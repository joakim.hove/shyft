#pragma once
/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <vector>
#include <map>
#include <stdexcept>
#include <string>
#include <functional>

#include <shyft/core/core_serialization.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/time/utctime_utilities.h>

#include <shyft/time_series/dd/geo_ts.h>
#include <shyft/time_series/dd/ats_vector.h>


/** @namespace geo contains the geo-extensions for the dtss
 *
 * @details
 * 
 * # Concept
 * We can think of the geo-ts-database as a 4 dimensonal space,
 * with a user-specified given unique name, 
 * where the data-type is a time-series.
 * 
 * The 'dimensions' are:
 * 
 * - 'v'  : the variable dimension, e.g. temperature, precipitation
 * - 'g'  : the geo spatial dimension (3d representative points in space)
 * - 'e'  : the ensemble dimension, we might have one or more 'versions'/ensembles of a forecast (they are variable correlated by same ensemble member)
 * - 't0' : when a time-series, usually forecast, starts, typically first time-point in the time-axis.
 * 
 * A `ts_db_config` defines the actual content of the dimensions above,
 * e.g.: 
 * 
 * - v=['temperature','preciptiation','relhum','wind_speed_2m']
 * - g=[point(1,1,1),point(1,100,1000)..,]
 * - e=[0..10]
 * - t0=[ '2020-01-01T00:00:00Z','2020-01-01T00:06:00Z'..]
 * 
 * An eval_args query resolves into a `slice` into  this 'space'.
 * e.g:
 *  - eval_args= {variables=['preciptation'],geo=polygon((0,0,0)..),e=[2,3],t0=['2020-01-01T00:06_00Z']}
 * could resolve into
 *  - slice = {v=[1],e=[2,3],g=[0,2],t=['2020-01-01T00:06_00Z']}
 * 
 * A `ts_id` is a precise index-location in this space.
 * 
 * ## Performance and speed
 *
 * The most imporant aspect of the geo-ts-db is to deliver geo-ts
 * at maximum (memory) speed for the use-cases relevant for
 * the shyft hydrology region-models.
 * To enable this, we provide caching of the time-series,
 * and we also provide both reading(evaluating geo-queries),
 * and writing more data to the geo-ts-db, with cache on write.
 * 
 * ## Backend and external formats.
 * 
 * The backend, providing the time-series storage, can
 * be in any form, including shyft-native time-series,
 * or netcdf archives, or even external services.
 * 
 * ## Current limitations
 * 
 * We have not yet enabled automagically resolving ordinary
 * ts-reads into the registered geo-databases.
 * This can however already be achieved using the dtss read python-callbacks,
 * and within those, resolve the geo://geodb/... into 
 * appropriate reads.
 * Note that since the geo://geodb/.. are cachable time-series,
 * they can participate in any server-side expressions.
 * 
 * ## Future expansions and ideas#
 * 
 * ### Serveside preprocessing using ts-expressions#
 * It is easy to extend the eval_args with time-series expression-templates,
 * that can be applied to the variables server-side.
 * e.g.
 *  pre_process['acc_precip']=TimeSeries('acc_precip').derivative()
 *  
 * ### Calling other dtss from geo-backend
 * If you use compute-intensive (e.g. concat, or grid-processing), you
 * can stack up several layers of dtss with geo-extensions, and
 * forward calls to multiple of those using pythonn threads.
 * 
 */
namespace shyft::dtss::geo {
    
using std::string;
using std::vector;
using std::map;
using std::int64_t;
using std::to_string;
using std::runtime_error;

using shyft::time_series::dd::apoint_ts;
using shyft::time_series::dd::ats_vector;
using shyft::time_series::dd::gta_t;
using shyft::core::utctime;
using shyft::core::no_utctime;
using shyft::core::utcperiod;
using shyft::core::to_seconds64;
using shyft::core::geo_point;
using shyft::time_series::dd::geo_ts; 
using shyft::time_series::dd::geo_ts_t0_var_ens_vector;//relization of geo_db
using shyft::time_series::dd::geo_ts_vector;

using ix_vector=vector<int64_t>;// to represent internal set of indexes into v,g,e dimensions(int64_t due to python mapping)

/** @brief A geo-query defines the epsg spec and polygon
 * 
 * @details In the context of the geo enabled dtss server,
 * the geo_query class helps us to set the
 * bounding-box, geo-graphic scope of the time-series.
 * We might want to provide a python interface to this that
 * ensures it is always constructed as a valid, immutable unit.
 * 
 */
struct query {
    int64_t epsg{0};          ///< the coordinate system epsg for these points
    vector<geo_point> polygon;///< polygon (we close if not closed)
    
    query()=default;
    query(int64_t epsg,vector<geo_point> const&gpv);
    
    bool operator==(query const&o) const {return epsg==o.epsg && polygon==o.polygon;}
    bool operator!=(query const&o) const {return !operator==(o);}
    x_serialize_decl();
};


/** @brief grid_spec: a minimal representation of a known points-3d in the terrain 
 *
 * @defails
 * We use it to describe a know grid, for example for 
 * arome-forecasts, or ec-forecasts.
 * 
 * We need a minimal,fast repr, and python exposures that
 * ensures constructed/manipulated elements are 
 * consistent/valid.
 * 
 * Notice that we have no requirement to the 'shape' of the grid, it's rather a set of points.
 */
struct grid_spec {
    int64_t epsg{0};         ///< as pr. usual, we always know the coord system
    vector<geo_point> points;///< we need x,y,z, the elevation is important, we assume no ordering,or grid-shape
    
    grid_spec()=default;
    grid_spec(int64_t epsg,vector<geo_point> const&gpv);
    
    bool operator==(grid_spec const&o) const {return epsg==o.epsg && points==o.points;}
    bool operator!=(grid_spec const&o) const {return !operator==(o);}
    
    /** @brief find the grid.points indicies of a geo_query
     * 
     * @param geo the geo-query
     * @return the indicies that are valid for lookup into grid.points identifying the position
     */
    ix_vector find_geo_match_ix(query const& geo) const;

    x_serialize_decl();
};

/** @brief eval_args 
 * @details
 * Describes scope for the geo_evaluate function,
 * in terms of:
 *  - the geo-ts database identifier
 *  - variables to extract, by names
 *  - ensemble members (list of ints)
 *  - temporal, using t0 from specified time-axis
 *  - spatial, using points for a polygon
 * and optionally 
 *  - the concat postprocessing with parameters
 */
struct eval_args {
    string  geo_ts_db_id;      ///< identifies the geo-ts-db, short, as 'arome', 'ec', as specified with server.add_geo_ts_db(cfg)
    vector<string>  variables; ///< 'temperature', 'precipitation' as in (almost) readable names. interpreted by the geo-ts-db backend routines, empty==all
    vector<int64_t>  ens;      ///< list of ensembles to return, empty=all, if specified >0
    gta_t  ta;                 ///< specifies the t0, and  .total_period().end is used as concatendated open-end fill-in length
    utctime ts_dt{0};          ///< read t0+ts_dt from each time-series, if 0, use the db-configured default
    query  geo_range;          ///< the spatial scope, as simple polygon
    utctime cc_dt0{0};         ///< concat lead-time
    bool concat{false};        ///< postprocess using concatenated forecast, returns 'one' concatenated forecast from many.
    
    eval_args()=default;
    eval_args(string const &geo_ts_db_id, vector<string> const & variables,vector<int64_t> const & ens,gta_t  const &ta,utctime ts_dt, query const & geo_range,bool concat,utctime cc_dt0);

    bool operator==(eval_args const&o) const {return ts_dt==o.ts_dt && geo_ts_db_id==o.geo_ts_db_id && variables==o.variables && ens==o.ens && ta==o.ta && geo_range==o.geo_range && cc_dt0==o.cc_dt0 && concat==o.concat;}
    bool operator!=(eval_args const&o) const {return !operator==(o);}
    x_serialize_decl();
};

/** @brief geo slice of the t0-var-ens-geo space
 * 
 * @details
 * We can consider geo ts_set as a precise specified slice of the 
 * t0-var-geo-ens 'space'.
 * 
 * It's the result of applying ts_db.compute(eval_args)
 * 
 */
struct slice {
    // the possible content of v,g,e are invariant when the geo_ts_db is configured,
    ix_vector v;///< variable indexes, 
    ix_vector g;///< geo-point indexes,
    ix_vector e;///<  ensemble indexes
    vector<utctime> t;///< t0 
    utctime ts_dt{0};///< read t0.. +ts_dt from each slice
    
    slice()=default;
    slice(ix_vector const&v,ix_vector const& g,ix_vector const&e, vector<utctime> const&t,utctime ts_dt);
    /** the size of the slice in terms of time-series */
    size_t size() const noexcept {return v.size()*g.size()*e.size()*t.size();}
    bool operator==(slice const&o) const {return v==o.v&& g==o.g && e==o.e && t==o.t && ts_dt==o.ts_dt;}
    bool operator!=(slice const&o) const {return !operator==(o);}
    /** set all to zero slize, keep memory */
    void size_to_zero() {v.resize(0);g.resize(0);e.resize(0);t.resize(0);}
};

namespace detail {
    
    /** geo imlementation specific helpers */
    
    /** geo::t0veg_ix represent index into the (t0,v,e,g) index space*/
    struct geo_ix {
        size_t t0;
        size_t v;//cfg.variable[v] -> gives the def.
        size_t e;//arg.ens[e] is the definition?
        size_t g;//cfg.grid.position[g], is the definition for geo-pos
        bool operator==(geo_ix const&o) const {return t0==o.t0&&v==o.v&&e==o.e&&g==o.g;}
        bool operator!=(geo_ix const&o) const {return !operator==(o);}
    };
    
    /** geo::ix_calc
     *
     * Helps transforming to and from (t0,v,e,g) index space to single linear index (i)
     */
     struct ix_calc {
        size_t n_t0{0};//t0_times[..]
        size_t n_v{0};//variable[v] -> gives the def.
        size_t n_e{0};//ens[e] is the definition
        size_t n_g{0};//grid.point[g], is the definition for geo-pos
        ix_calc()=default;
        
        ix_calc(size_t t0,size_t v,size_t e,size_t g):n_t0{t0},n_v{v},n_e{e},n_g{g} {}
        
        size_t size() const noexcept {return n_t0*n_v*n_e*n_g;}
        
        size_t flat(size_t  t0,size_t v,size_t e,size_t g) const noexcept {
            return n_v*n_e*n_g*t0+n_e*n_g*v + n_g*e + g;
        }
        
        void validate(size_t  t0,size_t v,size_t e,size_t g) const {
            if(t0>=n_t0) throw runtime_error("geo::matrix index t0 out of range "+to_string(n_t0)+" :" +to_string(t0));
            if(v>=n_v) throw runtime_error("geo::matrix index v out of range "+to_string(n_v)+" :" +to_string(v));
            if(e>=n_e) throw runtime_error("geo::matrix index e out of range "+to_string(n_e)+" :" +to_string(e));
            if(g>=n_g) throw runtime_error("geo::matrix index g out of range "+to_string(n_g)+" :" +to_string(g));
        }
        bool operator==(ix_calc const& o) const {return n_v==o.n_v && n_t0==o.n_t0 && n_e==o.n_e && n_g==o.n_g;}
        bool operator!=(ix_calc const& o) const {return !operator==(o);}
        
        geo_ix ix(size_t i) const noexcept {
            geo_ix r;
            auto x=lldiv(i,n_g);
            r.g= x.rem;
            x=lldiv(x.quot,n_e);
            r.e=x.rem;
            x=lldiv(x.quot,n_v);
            r.v=x.rem;
            r.t0=x.quot;
            return r;
        }
        x_serialize_decl();
    };
}

/** @brief ts_matrix 
 * 
 * @details
 * Keeps a dense view of (slice of) a geo ts db
 * as pure tsvector.
 * 
 * consider! replace impl with boost::multi_array (efficient, ready tested, allow slice/subviews etc iin c++)
 */
struct ts_matrix {
    detail::ix_calc shape;
    ats_vector tsv;

    ts_matrix()=default;
    ts_matrix(int n_t0,int n_v,int n_e,int n_g):shape{size_t(n_t0),size_t(n_v),size_t(n_e),size_t(n_g)} {
        tsv.resize(shape.size());
    }
    apoint_ts * _ts_pointer(int t,int v,int e,int g) {return &tsv[shape.flat(t,v,e,g)];}///< for internal access during geo-read
    apoint_ts const& _ts(int t,int v,int e,int g) const { return tsv[shape.flat(t,v,e,g)];}
    apoint_ts const& ts(int t,int v,int e,int g) const { shape.validate(t,v,e,g); return _ts(t,v,e,g);}
    
    void _set_ts(int t,int v,int e,int g,apoint_ts const &ts) {tsv[shape.flat(t,v,e,g)]=ts; }
    void set_ts(int t,int v,int e,int g,apoint_ts const &ts) {shape.validate(t,v,e,g);_set_ts(t,v,e,g,ts); }

    bool operator==(ts_matrix const &o) const {return shape==o.shape && tsv==o.tsv;}
    bool operator!=(ts_matrix const &o) const {return !operator==(o);}
    
    ts_matrix concatenate(utctime cc_dt,utctime fc_interval) const;
    x_serialize_decl();// needed because it's the geo_ts store value type
};

/** @brief geo_ts_matrix a 4 dim matrix of geo_ts type
 */
struct geo_ts_matrix {
    detail::ix_calc shape;
    geo_ts_vector tsv;

    geo_ts_matrix()=default;
    geo_ts_matrix(int n_t0,int n_v,int n_e,int n_g):shape{size_t(n_t0),size_t(n_v),size_t(n_e),size_t(n_g)} {
        tsv.resize(shape.size());
    }
    apoint_ts const& _ts(int t,int v,int e,int g) const { return tsv[shape.flat(t,v,e,g)].ts;}
    apoint_ts const& ts(int t,int v,int e,int g) const { shape.validate(t,v,e,g); return _ts(t,v,e,g);}
    
    void _set_ts(int t,int v,int e,int g,apoint_ts const &ts) {tsv[shape.flat(t,v,e,g)].ts=ts; }
    void set_ts(int t,int v,int e,int g,apoint_ts const &ts) {shape.validate(t,v,e,g);_set_ts(t,v,e,g,ts); }
    
    void _set_geo_point(int t,int v,int e,int g,geo_point const&p) {tsv[shape.flat(t,v,e,g)].mid_point=p;}
    void set_geo_point(int t,int v,int e,int g,geo_point const&p) {shape.validate(t,v,e,g);tsv[shape.flat(t,v,e,g)].mid_point=p;}
    
    geo_point const& _get_geo_point(int t,int v,int e,int g) const {return tsv[shape.flat(t,v,e,g)].mid_point;}
    geo_point const& get_geo_point(int t,int v,int e,int g) const {shape.validate(t,v,e,g);return _get_geo_point(t,v,e,g);}

    bool operator==(geo_ts_matrix const &o) const {return shape==o.shape && tsv==o.tsv;}
    bool operator!=(geo_ts_matrix const &o) const {return !operator==(o);}
    
    geo_ts_matrix concatenate(utctime cc_dt,utctime fc_dt) const;
    
    x_serialize_decl(); // needed because it is the dtss result of geo_evaluate
};


/** @brief A geo ts_db_config mapping time,geo to ts-url etc.
 * 
 * @details The purpose of this class is to enable us
 * to compute efficient, well defined, HPC ts-urls, based on 
 * a geo_query.
 * 
 * The class keeps a geo grid_spec, that covers the spatial
 * dimension and  fc_times keeps keeps the 
 * temporal dimension for the time where there
 * exists a 'forecast' (better term needed, it does not have 
 * to be a forecast,)
 * 
 * The user can register multiple geo-ts-databases, each with a unique name
 * 
 * , pr. usage (ready concatenated, or pr. region model)
 *    nea.(arome|ec|ec-ens).fc ... 
 *    nea.(arome|ec|ec-ens).cc .fx(nea.....fc) (as function, or materialized function)
 * 
 * About urls:
 *   shyft://<tsdb-dir>  used for shyft-internals
 *   geo:://<geo_ts_db>/.. used for geo-enabled storage
 * 
 */
struct ts_db_config {
    string prefix;            ///< the url prefix if we matrialize the geo-ts url, like the internal shyft:// or geo://
    string name;             ///< the shortest possible id of the geo_ts_db, e.g. 'a'='arome'
    string descr;            ///< nice to read description, no semantics
    grid_spec grid;          ///< the range/resolution of grid
    vector<utctime> t0_times;///< the available t0 times, the first time-point in ts, e.g. fc_times
    utctime dt{0};              ///< the time-span of each geo_ts,(e.g forecast), covered_period =[t0,t0 + dt>
    int64_t n_ensembles{0};  ///< number of available ensembles (1..n)
    vector<string> variables;///< known list of variables available

    ts_db_config()=default;
    //ts_db_config(ts_db_config const &)=default;
    
    ts_db_config(string const &prefix,string const&name,string const&descr,grid_spec const& grid,vector<utctime> const&t0_times,utctime dt,int64_t n_ensembles,vector<string> const&v);
    
    bool operator==(ts_db_config const&o) const {return prefix==o.prefix && name==o.name && descr==o.descr && grid==o.grid && t0_times==o.t0_times && dt==o.dt && n_ensembles==o.n_ensembles && variables==o.variables;}
    bool operator!=(ts_db_config const&o) const {return !operator==(o);}
    
    gta_t t0_time_axis() const ;
    
    /** get wanted t0, based on available t0, and concat mode
     * if concat == false then return t0
     * otherwise find closest available already existing stored t0 and return that
     * @param t0 the t0 time of the forecast series we are asking for
     * @param concat if false, then return t0, otherwise find leftmost existing forecast
     * @return t0 that will be used to identify the series in the 4-d time-variable-ens-geo grid
     */
    utctime get_associated_t0(utctime t0,bool concat) const;
  
    /** @brief find the grid.points indicies of a geo_query
     * 
     * @param geo the geo-query
     * @return the indicies that are valid for lookup into grid.points identifying the position
     */
    ix_vector find_geo_match_ix(query const& geo) const {
        return grid.find_geo_match_ix(geo);
    }

    /** @brief compute a geo::slice from eval_args */
    slice compute(eval_args const &ea) const;

    /** @brief create the structure to hold a geo::slice concrete result */
    ts_matrix create_ts_matrix(slice const& gx) const;
    ts_matrix create_ts_matrix_t0(size_t n_t0) const;///< create matix with n_t0 entries and  full v,e,g dimensions
    geo_ts_matrix create_geo_ts_matrix(slice const& gx) const;
    
    /** @brief compute geo bounding box for a slice */
    vector<geo_point> bounding_box(slice const& gx) const;

    /** @brief compute geo convex hull for a slice */
    vector<geo_point> convex_hull(slice const& gx) const;

    x_serialize_decl();
};
using ts_db_config_=std::shared_ptr<ts_db_config>;

/** represent a complete unique geo_ts_id using minimal size*/
struct ts_id {
    string geo_db;///< the geo_db key, keep it short.., so that we can associate it with the registered geo_db
    size_t v{0u};///< the variable index, from cfg.variables[v]
    size_t g{0u};///< the geo point index from cfg.grid.points[g];
    size_t e{0u};///< the ensemble index, in range 0..cfg.n_ensembles
    utctime t{no_utctime};///< the t0-time for the 'forecast'
    bool operator==(ts_id const& o) const noexcept {return geo_db==o.geo_db && v==o.v &&  g==o.g && e==o.e && t==o.t;}
    bool operator!=(ts_id const& o) const noexcept {return !operator==(o);}
    int64_t secs() const {return to_seconds64(t);} ///<we store up t seconds resolution.
};




/** @brief geo_read
 * 
 * @details
 * Returns a complete response, according to the parameters and geo_ts_db_config.
 * 
 * Thus you will need the 'question', parameters, to understand the
 * response.
 * 
 * E.g. if you ask for only   temperature, the var-dimension will be 1,
 *      note that ghe geo_scope is the only, not specified dimension.
 *      but it can be computed using the cfg and find the exact points that 
 *      are within  the geo-scope.
 * 
 */
using read_call_back_t = std::function<ts_matrix(
    ts_db_config_ const& cfg,     // provide keys/info for idx passed in next args
    slice const &g_slice          // slice to read
)>;

/** @brief geo_store
 *
 * @details
 * Function signature  for storing to a geo_ts_db.
 * Notice that we only support complete consistent
 * ts_matrix, where all the dimensions
 *  v,e,g must be consistent  with the database.
 * We support storing multiple t0's at once, as long as each v,e,g dimension is complete
 */
using store_call_back_t = std::function<void(
    ts_db_config_ cfg,// mutable..as pr. usual so that we now the index/variable-space
    ts_matrix const &v,// we require v to be a complete var.pos.ens.t0, we support overwrite/merge(concat)
    bool replace // replace or merge if already existing
)>;


    
}

x_serialize_export_key(shyft::dtss::geo::detail::ix_calc);
x_serialize_export_key(shyft::dtss::geo::geo_ts_matrix);
x_serialize_export_key(shyft::dtss::geo::ts_matrix);
x_serialize_export_key(shyft::dtss::geo::query);
x_serialize_export_key(shyft::dtss::geo::grid_spec);
x_serialize_export_key(shyft::dtss::geo::ts_db_config);
x_serialize_export_key(shyft::dtss::geo::eval_args);
