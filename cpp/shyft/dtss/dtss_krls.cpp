#include <shyft/dtss/dtss_krls.h>
#include <shyft/dtss/detail/krls_db.h>

namespace shyft::dtss {

using gts_t=krls_pred_db::gts_t;

krls_pred_db::krls_pred_db():impl{new krls_pred_db_impl()} {}
krls_pred_db::krls_pred_db(const std::string& root_dir,cb_fx cb):impl(new krls_pred_db_impl(root_dir,cb)) {
    
}
krls_pred_db::~krls_pred_db(){}

void krls_pred_db::save(const std::string & fn, const gts_t & ts, bool overwrite , const queries_t & queries , bool) {
    impl->save(fn,ts,overwrite,queries);
}

gts_t krls_pred_db::read(const std::string & fn, core::utcperiod period, const queries_t & queries ) {
    return impl->read(fn,period,queries);
}

void krls_pred_db::remove(const std::string & fn, const queries_t & ) {
    return impl->remove(fn);
}
ts_info krls_pred_db::get_ts_info(const std::string & fn, const queries_t &  ) { 
    return impl->get_ts_info(fn);
}
std::vector<ts_info> krls_pred_db::find(const std::string & match, const queries_t & ) {
    return impl->find(match);
}


/*  KRLS container API
    * ==================== */

void krls_pred_db::register_rbf_series(
    const std::string & fn, const std::string & source_url,  // series filename and source url
    const core::utcperiod & period,  // period to train
    const core::utctimespan dt, const ts::ts_point_fx point_fx,
    const std::size_t dict_size, const double tolerance,  // general parameters
    const double gamma,  // rbf kernel parameters
    const bool 
    ) {
    impl->register_rbf_series(fn,source_url,period,dt,point_fx,dict_size,tolerance,gamma);
}
void krls_pred_db::update_rbf_series(
    const std::string & fn,  // series filename
    const core::utcperiod & period,  // period to train
    const bool allow_gap_periods,
    const bool  
    ) {
    impl->update_rbf_series(fn,period,allow_gap_periods);
}

void krls_pred_db::move_predictor(
    const std::string & from_fn,
    const std::string & to_fn,
    const bool overwrite,
    const bool 
    ) {
    impl->move_predictor(from_fn,to_fn,overwrite);
}

gts_t krls_pred_db::predict_time_series(
    const std::string & fn,
    const gta_t & ta
    ) {
    return impl->predict_time_series(fn,ta);
}

}
