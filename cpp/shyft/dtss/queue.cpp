#include <shyft/dtss/queue.h>

namespace shyft::dtss::queue {
    using std::string;
    using std::shared_ptr;
    using std::make_shared;
    using time_series::dd::apoint_ts;
    using time_series::dd::ats_vector;
    using namespace core;

    vector<msg_info> tsv_queue::get_msg_infos() const {
        sl_ _{mx};
        vector<msg_info> r;r.reserve(all.size());
        for(auto const &m:all) r.push_back(m.second->info);
        return r;
    }

    msg_info tsv_queue::get_msg_info(string const&msg_id) const {
        sl_ _{mx};
        auto f=all.find(msg_id);
        if(f != all.end())
            return (*f).second->info;
        throw std::runtime_error("queue["+name+"].get_msg_info("+msg_id +") not found");
    }
    void tsv_queue::put(string const &msg_id,string const&descript, utctime ttl, ats_vector const&tsv) {
        sl_ _{mx};
        auto f=all.find(msg_id);
        if(f != all.end())
            throw std::runtime_error("queue["+name+"] already have a ("+msg_id +")! new items must have unique msg_id!");
        auto t_now=utctime_now();
        auto m= make_shared<tsv_msg>(msg_info{msg_id,descript,ttl,t_now},tsv);
        all[msg_id]=m;
        mq.push(m);
    }

    tsv_msg_ tsv_queue::try_get() {
        sl_ _{mx};
        if(!mq.empty()) {
            auto m=mq.front();
            mq.pop();
            m->info.fetched=utctime_now();
            return m;
        }
        return nullptr;
    }
    size_t tsv_queue::size() const {
        sl_ _{mx};
        return mq.size();
    }

    void tsv_queue::done(string const&msg_id, string const&diag ) {
        sl_ _{mx};
        auto f=all.find(msg_id);

        if(f == all.end())
            throw std::runtime_error("queue["+name+"].get_info("+msg_id +") not found");

        if ((*f).second->info.fetched == no_utctime)
            throw std::runtime_error("queue["+name+"].done("+msg_id +") attemted for element not yet fetched");

        (*f).second->info.done=utctime_now();
        (*f).second->info.diagnostics=diag;
    }

    void tsv_queue::reset() {
        sl_ _{mx};
        all.clear();
        mq={};//flush queue.
    }

    size_t tsv_queue::flush_done_items(bool keep_ttl_items, utctime x_now) {
        sl_ _{mx};
        size_t c{0};
        auto t_now=x_now!=no_utctime?x_now:utctime_now();// to compute eol
        auto m=all.begin(); // we are removing items from the container, so need by val iterator
        while(m!= all.end()) {
            if( m->second->info.done != no_utctime) {
                if(!keep_ttl_items || (keep_ttl_items && t_now>m->second->info.eol())) {
                    m=all.erase(m);// erase returns the next element in the map.
                    ++c;//count items removed
                    continue;
                }
            }
            ++m;
        }
        return c;
    }


    void q_mgr::add(string const&q_name) {
        sl_ _{mx};
        auto f=q.find(q_name);
        if(f != q.end())
            throw std::runtime_error("queue '"+q_name+"' already exists");
        q[q_name]=make_shared<tsv_queue>(q_name);
    }

    void q_mgr::remove(string const&q_name) {
        sl_ _{mx};
        auto f=q.find(q_name);
        if(f == q.end())
            throw std::runtime_error("queue '"+q_name+"' not found");
        q.erase(f);//consider test if q.empty()?
    }

    size_t q_mgr::size() const {
        sl_ _{mx};
        return q.size();
    }

    vector<string> q_mgr::queue_names() const {
        sl_ _{mx};
        vector<string> r;
        for(auto const&i:q) r.push_back(i.first);
        return r;
    }

    tsv_queue_ q_mgr::operator()(string const&q_name) const {
        return q_(q_name);
    }

    tsv_queue_ q_mgr::q_(std::string const&n)  const {
        sl_ _{mx};
        auto f=q.find(n);
        if(f== q.end())
            throw std::runtime_error("queue '"+n+"' not found");
        return f->second;
    }

}
