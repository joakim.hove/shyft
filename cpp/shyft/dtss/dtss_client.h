/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <cstdint>
#include <cstdio>
#include <string>
#include <vector>
#include <memory>

#include <dlib/iosockstream.h>
#include <dlib/misc_api.h>
#include <shyft/core/dlib_utils.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/gpoint_ts.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/geo_ts.h>

#include <shyft/dtss/time_series_info.h>
#include <shyft/dtss/dtss_cache.h>
#include <shyft/dtss/geo.h>
#include <shyft/dtss/queue_msg.h>

namespace shyft::dtss {

    using std::vector;
    using std::string;
    using std::unique_ptr;
    using std::make_unique;
    using std::max;

    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::core::utctimespan;
    using shyft::core::no_utctime;
    using shyft::core::calendar;
    using shyft::core::deltahours;
    using shyft::core::srv_connection;
    using gta_t = shyft::time_axis::generic_dt;
    using gts_t = shyft::time_series::point_ts<gta_t>;

    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::gpoint_ts;
    using shyft::time_series::dd::gts_t;
    using shyft::time_series::dd::aref_ts;
    using shyft::time_series::dd::geo_ts;

    // ========================================

    using ts_vector_t = shyft::time_series::dd::ats_vector;
    using ts_info_vector_t = vector<ts_info>;
    using id_vector_t = vector<string>;
    
    struct msync_read_result {
        ts_vector_t result;///< result of the slave read operation(have the lengt and order of the read request)
        vector<apoint_ts> updates;///< updates that we piggy-back to the read operation (might have zero length, note that they all are of real type ref_ts)
    };

    /** @brief a dtss client
     *
     * This class implements the client side functionality of the dtss client-server.
     *
     *
     */
    struct client {
        /** A client can connect to 1..n dtss and distribute the calculations
         * among these.
         * Precondition is that these servers are equally configured
         * for the operations performed, e.g. same containers etc.
         * (Alt: that they are just caching proxyies, slaves of the primary)
         * For write operations, the first connection in srv_con is used.
         */
        vector<srv_connection> srv_con;

        bool auto_connect{ true }; ///< if enabled, connections are made as needed, and kept short, otherwise externally managed.
        bool compress_expressions{ true };///< compress expressions to gain speed

        /** returns the current reconnenct count indicating number of failures
         * that was recovered using a new connection to server
         */
        size_t reconnect_count() const;
        
        /** construct client with single server */
        client(const string & host_port, bool auto_connect = true, int timeout_ms = 1000);
        
        /** construct client with connections to multiple servers */
        client(const vector<string> & host_ports, bool auto_connect, int timeout_ms);

    //-- connection managment
        void reopen(int timeout_ms = 1000);
        void close(int timeout_ms = 1000);

    //-- ts related functions
        ts_info_vector_t find(const string & search_expression);
        ts_info get_ts_info(const string & ts_url);
        vector<apoint_ts> percentiles(const ts_vector_t & tsv, utcperiod p, const gta_t & ta, const vector<int64_t> & percentile_spec, bool use_ts_cached_read, bool update_ts_cache);
        vector<apoint_ts> evaluate(const ts_vector_t & tsv, utcperiod p, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_result=utcperiod{});

        void store_ts(const ts_vector_t & tsv, bool overwrite_on_write, bool cache_on_write);
        void merge_store_ts(const ts_vector_t & tsv, bool cache_on_write);
        void remove(const string & name);

    //-- cache management and stats
        void cache_flush();
        cache_stats get_cache_stats();

    //-- master-slave sync related
        id_vector_t get_container_names();
        msync_read_result read(const id_vector_t & ts_urls, utcperiod p, bool use_ts_cached_read, bool subscribe);
        vector<apoint_ts> read_subscription();
        void unsubscribe(const id_vector_t & ts_urls);
        
    //-- geo related
        geo::geo_ts_matrix geo_evaluate(geo::eval_args const&ea,bool use_cache, bool update_cache);
        void geo_store(const string &geo_db_name,geo::ts_matrix const&tsm,bool replace, bool update_cache);
        vector<geo::ts_db_config_> get_geo_ts_db_info();
        void add_geo_ts_db(geo::ts_db_config_ const &gdb);
        void remove_geo_ts_db(const string & geo_db_name);
    //-- server version    
        string version();
    //-- queue related
        vector<string> q_list();///< provide a list of all available queues
        queue::msg_info q_msg_info(string const&q_name,string const&msg_id); 
        vector<queue::msg_info> q_msg_infos(string const&q_name);
        void q_put(string const &q_name,string const&msg_id, string const&descript, utctime ttl,ts_vector_t const&tsv);
        queue::tsv_msg_ q_get(string const &q_name, utctime max_wait);
        void q_ack(string const&q_name, string const&msg_id,string const&diag);
        size_t q_size(string const&q_name);
        void q_add(string const &q_name);
        void q_remove(string const &q_name);
        void q_maintain(string const&q_name,bool keep_ttl_items, bool flush_all=false);
        
    };

    // ========================================


} // shyft::dtss

