 /** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <string>
#include <vector>
#include <memory>
#include <map>
#include <shyft/time_series/point_ts.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::dtss {

    using std::vector;
    using std::map;
    using std::unique_ptr;
    using std::string;
    using std::size_t;

    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using gta_t = shyft::time_axis::generic_dt;
    using gts_t = shyft::time_series::point_ts<gta_t>;
    
    using queries_t = map<string, string>;

    /** @brief The abstract ts-db container interface 
     *
     * The dtss::server passes request to save/read/remove or find to instances
     * that implements this interface.
     * 
     * The queries_t allow that particular container to utilize user specified data along with the time-series id,
     * and kind of give some parameter possiblities that is utilized in the krls_db class.
     * 
     */
    struct its_db {
        virtual ~its_db() {}

        /** @brief Save a time-series to a file, *overwrite* any existing file with contents.
         *
         *
         * @param fn  'Path-name' to save the time-series at.
         * @param ts  Time-series to save.
         * @param win_thread_close  Only meaningfull on the Windows platform.
         *                          Use a deatached background thread to close the file.
         *                          Defaults to true.
         */
        virtual void save(const string& fn, const gts_t& ts, bool overwrite = true, const queries_t & queries = queries_t{}, bool = true)=0;

        /** read a ts from specified file */
        virtual gts_t read(const string& fn, utcperiod p, const queries_t & queries = queries_t{})=0;

        /** removes a ts from the container */
        virtual void remove(const string& fn, const queries_t & queries = queries_t{})=0;

        /** get minimal ts-information from specified fn */
        virtual ts_info get_ts_info(const string& fn, const queries_t & queries = queries_t{})=0;

        /** find all ts_info s that matches the specified re match string
         *
         * e.g.: match= 'hydmet_station/.*_id/temperature'
         *    would find all time-series /hydmet_station/xxx_id/temperature
         */
        virtual vector<ts_info> find(const string& match, const queries_t & queries = queries_t{})=0;
        
        /** return the root_dir string, if applicable, default to empty string */
        virtual string root_dir() const {return string("");}
        
        
        inline static const string reserved_extension{".cfg"};///< return reserved extension, like .cfg, that are disalloved for  ts file-names
    };

}
