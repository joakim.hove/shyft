#include <shyft/web_api/targetver.h>
#include <boost/asio/buffer.hpp>
#include <shyft/web_api/dtss_web_api.h>
#include <shyft/dtss/dtss.h>
#include <shyft/core/subscription.h>
#include <shyft/dtss/dtss_subscription.h>
#include <shyft/web_api/web_api_grammar.h>
#include <shyft/web_api/web_api_generator.h>


namespace shyft::web_api {
    using shyft::time_series::dd::apoint_ts;
    using namespace shyft::time_series::dd;
    using namespace shyft;
    using generator::atsv_generator;
    using generator::ts_info_vector_generator;
    using boost::spirit::karma::generate;
    using std::vector;
    using namespace shyft::dtss;
    using shyft::core::subscription::observer_base;
    using shyft::web_api::grammar::phrase_parser;
    namespace grammar {
        extern template struct web_request_grammar<const char*>;
    }
    using dtss::subscription::ts_expression_observer;
    using dtss::subscription::manager_;
    
    /** 
     * @brief customized expression observer
     * @details
     * We need to keep track of the wanted generation format for the ts-vectors, so
     * we add the ts_fmt field to the observer class that are used so that we
     * are able to repeat the same formatted emit on changes.
     * 
     */
    struct dtss_expression_observer:ts_expression_observer {
        template<class T, class F>
        dtss_expression_observer(manager_ const &sm, string const&  request_id,T &&tsv, F&&fx, bool ts_fmt )
        : ts_expression_observer{sm,request_id,std::forward<T>(tsv),std::forward<F>(fx)},ts_fmt{ts_fmt} {
        }

        bool ts_fmt{false};
    };
    /** @brief variant visitor dispatcher for messages
     *
     * So given type of message, generate the response for that message.
     */
    struct message_dispatch {
        using return_type=bg_work_result;
        dtss_server *srv{nullptr};//<< non-null dtss_server pointer.

        std::string safe_json_diag(std::string const&s) {
            std::string diag;
            auto bi=std::back_inserter(diag);
            generator::escaped_string_generator<decltype(bi)> esc_s;
            generate(bi,esc_s,s);
            return diag;
        }
        
        std::string gen_ok_response(std::string const&req_id, std::string diag) {
            return "{ \"request_id\" : \""+req_id + "\",\"diagnostics\": \""+ safe_json_diag(diag) +"\" }";
        }

        bg_work_result operator()(find_ts_request const&fts) {
            auto r=srv->do_find_ts(fts.find_pattern);
            std::string response=string("{\"request_id\":\"")+fts.request_id+string("\",\"result\":");
            auto  sink=std::back_inserter(response);
            if(!generate(sink,ts_info_vector_generator<decltype(sink)>(),r)) {
                response= "failed to generate response for " +fts.request_id;
            } else {
                response +="}";
            }
            return bg_work_result{response};
        }

        bg_work_result operator()(info_request const&ir) {
            return bg_work_result{gen_ok_response(ir.request_id,"not implemented")};
        }
        std::string gen_tsv_response(std::string const&req_id, ts_vector_t const &r,bool ts_fmt) {
            std::string response=string("{\"request_id\":\"")+req_id+string("\",\"tsv\":");
            auto  sink=std::back_inserter(response);
            if(!generate(sink,atsv_generator<decltype(sink)>(ts_fmt),r)) {
                response= "failed to genereate response for " +req_id;
            } else {
                response +="}";
            }
            return response;
        }

        bg_work_result operator()(read_ts_request const&rts) {
            ts_vector_t rtsv;
            for(auto const& sym_ts:rts.ts_ids)
                rtsv.emplace_back(apoint_ts(sym_ts));
            auto read_fx=[srv=this->srv,read_period=rts.read_period,cache=rts.cache,clip_period=rts.clip_period](ats_vector tsv)->ats_vector {
                return srv->do_evaluate_ts_vector(read_period,tsv,cache,false,clip_period);// use but do not update cache! make update explicit!
            };
            if(rts.subscribe) {
                auto ts_sub=make_shared<dtss_expression_observer>(srv->sm,rts.request_id,rtsv, read_fx,rts.ts_fmt);
                return bg_work_result{gen_tsv_response(rts.request_id,ts_sub->recompute_result(),rts.ts_fmt),ts_sub};
            } else {
                return bg_work_result{gen_tsv_response(rts.request_id,read_fx(rtsv),rts.ts_fmt)};
            }
        }


        bg_work_result operator()(average_ts_request const& rts) {
            ts_vector_t rtsv;
            for(auto const& sym_ts:rts.ts_ids)
                rtsv.emplace_back(apoint_ts(sym_ts));

            auto read_fx=[srv=this->srv,ta=rts.ta,read_period=rts.read_period,cache=rts.cache](ats_vector tsv)->ats_vector {
                auto raw=srv->do_evaluate_ts_vector(read_period,tsv,cache,false,utcperiod{});// use but do not update cache! make update explicit!
                return raw.average(ta);
            };
            if(rts.subscribe) {
                dtss::subscription::ts_expression_observer_ ts_sub{make_shared<dtss_expression_observer>(srv->sm,rts.request_id,rtsv, read_fx,rts.ts_fmt)};
                return bg_work_result{gen_tsv_response(rts.request_id,ts_sub->recompute_result(),rts.ts_fmt), ts_sub};
            } else {
                return bg_work_result{gen_tsv_response(rts.request_id,read_fx(rtsv),rts.ts_fmt)};
            }
        }

        bg_work_result operator()(percentile_ts_request const& rts) {
            ts_vector_t rtsv;
            for(auto const& sym_ts:rts.ts_ids)
                rtsv.emplace_back(apoint_ts(sym_ts));
            vector<int64_t> p;p.reserve(rts.percentiles.size());for(auto const i:rts.percentiles) p.emplace_back(i);
            auto read_fx=[srv=this->srv,ta=rts.ta, percentiles=p,read_period=rts.read_period,cache=rts.cache](ats_vector tsv)->ats_vector {
                return srv->do_evaluate_percentiles(read_period,tsv,ta,percentiles,cache,false);// use but do not update cache! make update explicit!
            };
            if(rts.subscribe) {
                dtss::subscription::ts_expression_observer_ ts_sub{ make_shared<dtss_expression_observer>(srv->sm,rts.request_id,rtsv, read_fx,rts.ts_fmt) };
                return bg_work_result{gen_tsv_response(rts.request_id,ts_sub->recompute_result(),rts.ts_fmt),ts_sub};
            } else {
                return bg_work_result{gen_tsv_response(rts.request_id,read_fx(rtsv),rts.ts_fmt)};
            }
        }

        bg_work_result operator()(store_ts_request const& r) {
            string diag;
            try {
                if(r.merge_store) {
                    srv->do_merge_store_ts(r.tsv,r.cache);
                } else {
                    srv->do_store_ts(r.tsv,r.recreate_ts,r.cache);
                }
            } catch(std::runtime_error const&re) {
                diag=re.what();
            }
            return bg_work_result{gen_ok_response(r.request_id,diag)};
        }

        bg_work_result operator()(unsubscribe_request const&r) {
            return bg_work_result{gen_ok_response(r.request_id,""),r.subscription_id};
        }

        //q-related
        // do repetitive patterns once, using template fx.
        template< template<class> class G, class Fx>
        bg_work_result make_response(Fx&&fx) {
            G<generator::generator_output_iterator> g_; // a generator fo the response
            string s;                                   // the string to keep the response
            auto sink=std::back_inserter(s);            // backinserter for the generator
            auto a=fx();                                // invoke the server call, get back result
            if(!generate(sink,g_,a)) {  // then generate the response, if fail, just throw(it is catched,and client gets it)
                throw std::runtime_error("failed to generate response");
            }
            return bg_work_result{s}; // success, just return the work result
        }

        bg_work_result operator()(q_list_request const&r ) {
            return make_response<generator::q_list_response_generator>(
                [&](){
                    return q_list_response {r.request_id,srv->queue_manager.queue_names()};
                }
            );
        }

        bg_work_result operator()(q_info_request const&r ) {
            return make_response<generator::q_info_response_generator>(
                [&](){
                    return q_info_response {r.request_id,srv->queue_manager(r.q_name)->get_msg_info(r.msg_id)};
                }
            );
        }

        bg_work_result operator()(q_infos_request const&r ) {
            return make_response<generator::q_infos_response_generator>(
                [&](){
                    return q_infos_response {r.request_id,srv->queue_manager(r.q_name)->get_msg_infos()};
                }
            );
        }

        bg_work_result operator()(q_size_request const&r ) {
            return make_response<generator::q_size_response_generator>(
                [&](){
                    return q_size_response {r.request_id,static_cast<int64_t>(srv->queue_manager(r.q_name)->size())};
                }
            );
        }

        bg_work_result operator()(q_maintain_request const&r ) {
            
            if(r.flush_all)
                srv->queue_manager(r.q_name)->reset();
            else
                srv->queue_manager(r.q_name)->flush_done_items(r.keep_ttl_items);    
            return bg_work_result{gen_ok_response(r.request_id,"")};
        }

        bg_work_result operator()(q_put_request const&r ) {
            srv->queue_manager(r.q_name)->put(r.msg_id,r.descript,r.ttl,r.tsv);
            return bg_work_result{gen_ok_response(r.request_id,"")};
        }

        bg_work_result operator()(q_get_request const&r ) {
            return make_response<generator::q_get_response_generator>([&](){
                    return q_get_response {r.request_id,srv->queue_manager(r.q_name)->try_get()};
                }
            );
        }

        bg_work_result operator()(q_done_request const&r ) {
            srv->queue_manager(r.q_name)->done(r.msg_id,r.diagnostics);
            return bg_work_result{gen_ok_response(r.request_id,"")};
        }

    };

    /** brute forece fuzzy request-id recovery */
    std::string recover_request_id(std::string const&request) {
        auto r_pos= request.find("request_id");
        if(r_pos==std::string::npos)return "";
        auto c_pos= request.find(":",r_pos);
        if(c_pos== std::string::npos) return "";
        auto q1_pos = request.find("\"", c_pos+1);
        if(q1_pos == std::string::npos) return "";
        auto q2_pos = request.find("\"", q1_pos+1);
        if(q2_pos==std::string::npos) return "";
        if(q2_pos-q1_pos >200) return ""; //give up,dont allow crap strings
        return request.substr(q1_pos+1,q2_pos-q1_pos-1);
    }
    
    bg_work_result request_handler::do_the_work(const std::string& request) {

        bg_work_result r;// as pr usual, we stash the result here,single return point
        grammar::web_request wr;// variant of any c++ type web-requests that we can work with.
        grammar::web_request_grammar<const char*> web_req_;// the grammar parser 
        message_dispatch msg_dispatch{srv};// the message dispatcher(needs srv to implement actions)
        bool ok_parse=false;
        std::string response;
        try {
           ok_parse=phrase_parser( request.c_str(),web_req_,wr);
            if(ok_parse) {
                r = boost::apply_visitor(msg_dispatch,wr);
            } else {
                r = bg_work_result{"not understood:" + request};
            }
        } catch (std::runtime_error const &re) {
            auto req_id= recover_request_id(request);
            if(req_id.size()) {//recovered request_id, try to respond with that subject
                r=bg_work_result{msg_dispatch.gen_ok_response(req_id,re.what())};
            } else {
                r = bg_work_result{string("request_parse:")+ re.what()};
            }
        }
        return r;
    }
    
    bg_work_result request_handler::do_subscription_work(shyft::core::subscription::observer_base_ const& sub_job) {
        if(sub_job->recalculate()) {
            auto sj=std::dynamic_pointer_cast<dtss_expression_observer>(sub_job);
            if(!sj) {
                return bg_work_result{message_dispatch{srv}.gen_ok_response(sub_job->request_id,"do_subscription_work: subscriptoin work of wrong type")};
            } else {
                return bg_work_result{message_dispatch{srv}.gen_tsv_response(sj->request_id,sj->published_view,sj->ts_fmt)};
            }
        } else {
            return bg_work_result{};// empty, no real change
        }
    }
}
