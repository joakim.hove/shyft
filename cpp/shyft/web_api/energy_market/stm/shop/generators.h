/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/web_api/web_api_generator.h>
#include <shyft/energy_market/stm/shop/shop_log_entry.h>


namespace shyft::web_api::generator::shop {
    namespace ka = boost::spirit::karma;
    namespace phx = boost::phoenix;

    constexpr char quote = '"';
    using boost::spirit::karma::generate;

    using ::shyft::web_api::generator::escaped_string_generator;
    using ::shyft::web_api::generator::utctime_generator;
    using ::shyft::energy_market::stm::shop::shop_log_entry;

    /** @brief generator class for shop_log_entry */
    template<class OutputIterator>
    struct shop_log_entry_generator : ka::grammar<OutputIterator, shop_log_entry()> {
        shop_log_entry_generator() : shop_log_entry_generator::base_type(pg) {
            pg = ka::lit('{')
                << ka::lit("\"time\":") << time_[ka::_1=phx::bind(&shop_log_entry::time, ka::_val)]
                << ka::lit(",\"severity\":") << quote << ka::string[ka::_1=phx::bind(map_severity, phx::bind(&shop_log_entry::severity, ka::_val))] << quote
                << ka::lit(",\"code\":") << ka::int_[ka::_1=phx::bind(&shop_log_entry::code, ka::_val)]
                << ka::lit(",\"message\":") << quote << esc_str_[ka::_1=phx::bind(&shop_log_entry::message, ka::_val)] << quote
                << ka::lit('}');
        }

        static string map_severity(int sev_code) {
            switch (sev_code) {
                case shop_log_entry::log_severity::information:
                    return "INFORMATION";
                case shop_log_entry::log_severity::diagnosis_information:
                    return "DIAGNOSIS INFORMATION";
                case shop_log_entry::log_severity::warning:
                    return "WARNING";
                case shop_log_entry::log_severity::diagnosis_warning:
                    return "DIAGNOSIS WARNING";
                case shop_log_entry::log_severity::error:
                    return "ERROR";
                case shop_log_entry::log_severity::diagnosis_error:
                    return "DIAGNOSIS ERROR";
                default:
                    return "UNDEFINED";
            }
        }

        ka::rule<OutputIterator, shop_log_entry()> pg;
        utctime_generator<OutputIterator> time_;
        escaped_string_generator<OutputIterator> esc_str_;
    };

}
