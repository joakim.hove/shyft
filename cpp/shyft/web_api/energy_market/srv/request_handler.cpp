#include <shyft/core/subscription.h>
#include <shyft/web_api/energy_market/srv/request_handler.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/generators/json_struct.h>

#include <shyft/web_api/energy_market/srv/generators.h>

#include <shyft/srv/server.h>
#include <shyft/srv/db.h>
#include <shyft/srv/srv_subscription.h>
#include <shyft/energy_market/stm/srv/task/server.h>


namespace shyft::web_api::energy_market::srv {
    using shyft::core::utcperiod;
    using shyft::web_api::grammar::phrase_parser;
    using shyft::web_api::grammar::request_grammar;
    using shyft::web_api::generator::emit;
    using shyft::web_api::generator::emit_object;
    using shyft::web_api::generator::emit_vector_fx;
    using shyft::srv::subscription::srv_observer;
    using shyft::srv::subscription::subscription_type;

    namespace ka = boost::spirit::karma;

    template <class Server>
    bool request_handler<Server>::handle_request(const request& req, bg_work_result& resp) {
        if (req.keyword == "get_model_infos") {
            return handle_get_model_infos_request(req.request_data, resp);
        } else if (req.keyword == "read_model") {
            return handle_read_model_request(req.request_data, resp);
        } else if (req.keyword == "update_model_info") {
            return handle_update_model_info_request(req.request_data, resp);
        } else if (req.keyword == "store_model") {
            return handle_store_model_request(req.request_data, resp);
        } else if (req.keyword == "remove_model") {
            return handle_remove_model_request(req.request_data, resp);
        } else if (req.keyword == "unsubscribe") {
            return handle_unsubscribe_request(req.request_data, resp);
        }
        return false;
    }

    template <class Server>
    bg_work_result request_handler<Server>::do_the_work(const string& input) {
        // 1. Parse the request
        // 2. Error handling: On success, do a switch on keyword,
        //      This last part is done using a virtual function and so can be extended in derived classes.
        // 3. Handle every case and emit a response string

        // 1. Parse the request:
        request arequest;
        request_grammar<const char*> request_;
        bool ok_parse = false;
        bg_work_result response;
        try {
            ok_parse = phrase_parser(input.c_str(), request_, arequest);
            if (ok_parse) {
                if (!handle_request(arequest, response)){
                    response.copy_response("Unknown keyword '" + arequest.keyword + "'");
                }
            } else {
                response = bg_work_result{"not understood: " + input};
            }
        } catch (std::runtime_error const& re) {
            response = bg_work_result{string("request_parse: ") + re.what()};
        }
        return response;
    }

    template <class Server>
    bg_work_result request_handler<Server>::do_subscription_work(observer_base_ const& o) {
        if (o->recalculate()) {
            auto so = std::dynamic_pointer_cast<srv_observer>(o);
            switch (so->st) {
                case subscription_type::MODEL_INFOS:
                    return bg_work_result{generate_model_infos_response(so->request_id, so->mids, so->per)};
                case subscription_type::READ_MODEL:
                    return bg_work_result{generate_read_model_response(so->request_id, so->mids[0])};
            }
            return bg_work_result{};
        } else {
            return bg_work_result{};
        }
    }

    template<class Server>
    string request_handler<Server>::generate_model_infos_response(string const& request_id, vector<int64_t> const& mids, utcperiod per) {
        using shyft::web_api::generator::model_info_generator;

        vector<model_info> model_infos;
        if (per.valid()) {
            model_infos = srv->db.get_model_infos(mids, per);
        } else {
            model_infos = srv->db.get_model_infos(mids);
        }
        string response = string(R"_({"request_id":")_") + request_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            model_info_generator<decltype(sink)> mi_;
            ka::generate(sink, '[' << -(mi_ % ',') << ']', model_infos);
        }
        *sink++ = '}';
        return response;
    }

    template<class Server>
    bool request_handler<Server>::handle_get_model_infos_request(const json& data, bg_work_result& resp) {
        auto req_id = data.required<string>("request_id");
        auto sub = boost::get_optional_value_or(data.optional<bool>("subscribe"), false);
        auto per = boost::get_optional_value_or(data.optional<utcperiod>("period"), utcperiod());
        auto tmids = boost::get_optional_value_or(data.optional<vector<int>>("mids"), vector<int>{});
        vector<int64_t> mids(tmids.begin(), tmids.end());
        if (sub) {
            auto subscription = std::make_shared<srv_observer>(srv->db.sm, req_id, mids, per,
                                                               subscription_type::MODEL_INFOS);
            subscription->recalculate();
            resp.subscription = std::move(subscription);
        }
        resp.copy_response(generate_model_infos_response(req_id, mids, per));
        return true;
    }

    template<class Server>
    string request_handler<Server>::generate_read_model_response(string const& request_id, int64_t mid) {
        // Get model from server:
        auto mdl = srv->db.read_model(mid);
        // Prepare response
        string response = string(R"_({"request_id":")_") + request_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            emit(sink, mdl);
        }
        *sink++ = '}';
        return response;
    }

    template<class Server>
    bool request_handler<Server>::handle_read_model_request(const json& data, bg_work_result& resp) {
        // Get required data:
        auto req_id = data.required<string>("request_id");
        int64_t model_id = data.required<int>("model_id");
        auto sub = boost::get_optional_value_or(data.optional<bool>("subscribe"), false);
        if (sub) {
            auto subscription = std::make_shared<srv_observer>(srv->db.sm, req_id, vector < int64_t > {model_id},
                                                               utcperiod(), subscription_type::READ_MODEL);
            subscription->recalculate();
            resp.subscription = std::move(subscription);
        }
        resp.copy_response(generate_read_model_response(req_id, model_id));
        return true;
    }

    template<class Server>
    bool request_handler<Server>::handle_update_model_info_request(const json& data, bg_work_result& resp) {
        // Get required data:
        auto req_id = data.required<string>("request_id");
        auto mi = data.required<model_info>("model_info");
        auto mid = mi.id;

        auto success = srv->db.update_model_info(mid, mi);
        // Prepare response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
           ka::generate(sink, ka::bool_, success);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    template <class Server>
    bool request_handler<Server>::handle_store_model_request(const json& data, bg_work_result& resp) {
        // Get required data:
        auto req_id = data.required<string>("request_id");
        auto mdl = std::make_shared<typename Server::DB_t::model_t>(data.required<typename Server::DB_t::model_t>("model"));
        auto mi = boost::get_optional_value_or(data.optional<model_info>("model_info"),
            model_info(mdl->id, mdl->name, mdl->created, mdl->json));
        mi.created = mdl->created;

        auto new_mid = srv->db.store_model(mdl, mi);

        // Prepare response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
           ka::generate(sink, ka::int_, new_mid);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    template <class Server>
    bool request_handler<Server>::handle_remove_model_request(const json& data, bg_work_result& resp) {
        // Get required data:
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("model_id");

        int res =srv->db.remove_model(mid);
        // Prepare response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            ka::generate(sink, ka::int_, res);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    template <class Server>
    bool request_handler<Server>::handle_unsubscribe_request(const json& data, bg_work_result& resp) {
        auto req_id = data.required<string>("request_id");
        auto unsub_id = data.required<string>("subscription_id");
        std::string response = "";
        auto sink = std::back_inserter(response);
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("request_id", req_id)
                .def("subscription_id", unsub_id)
                .def("diagnostics", string{});
        }
        resp.unsubscribe_id = unsub_id;
        resp.copy_response(response);
        return true;
    }

    template struct request_handler<shyft::energy_market::stm::srv::task::server>;
}
