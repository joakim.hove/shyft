#include <shyft/web_api/energy_market/srv/grammar.h>
#include <boost/optional/optional.hpp>

namespace shyft::web_api::grammar {
    using std::string;
    using shyft::core::no_utctime;

    
    template <class Iterator, class Skipper>
    model_info_grammar<Iterator, Skipper>::model_info_grammar():
        model_info_grammar::base_type(start, "model_info") {

        start = lit('{')
            >> lit("\"id\"") >> ':' >> int_[phx::bind(&model_info::id, _val) = _1] >> ','
            >> lit("\"name\"") >> ':' >> string_[phx::bind(&model_info::name, _val) = _1]
            >> (-(',' >> lit("\"created\"") >> ':' >> time_))
                [phx::bind(&model_info::created, _val) = phx::bind([](auto const&t){return t?*t:core::utctime_now();}, _1)]
            >> (-(',' >> lit("\"json\"") >> ':' >> string_))
                [phx::bind(&model_info::json, _val) = phx::bind(get_value_or<string>, _1, string(""))]
                >> lit('}');
        on_error<fail>(start, error_handler(_4, _3, _2));
    }
    template struct model_info_grammar<request_iterator_t, request_skipper_t>;
}
