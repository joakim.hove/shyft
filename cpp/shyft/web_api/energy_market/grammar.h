/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/mp/grammar.h>

#include <shyft/web_api/web_api_grammar.h>
#include <shyft/web_api/energy_market/srv/grammar.h>
#include <shyft/energy_market/constraints.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/web_api/json_struct.h>

#include <map>
#include <memory>

namespace shyft::web_api::grammar {
    using std::map;
    using std::shared_ptr;
    using std::pair;
    
    using ::shyft::web_api::energy_market::value_type;
    using ::shyft::web_api::energy_market::attribute_value_type;
    using ::shyft::web_api::energy_market::json;
    using ::shyft::web_api::energy_market::request;

    using shyft::energy_market::core::absolute_constraint;
    using shyft::energy_market::core::penalty_constraint;
    using shyft::energy_market::hydro_power::point;
    using shyft::energy_market::hydro_power::xy_point_curve;
    using shyft::energy_market::hydro_power::xy_point_curve_with_z;
    using shyft::energy_market::hydro_power::turbine_efficiency;
    using shyft::energy_market::hydro_power::turbine_description;


    /** Integer List Grammar **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct integer_list_grammar : public qi::grammar<Iterator, std::vector<int>(), Skipper> {
        integer_list_grammar();

        qi::rule<Iterator, vector<int>(), Skipper> start;
		qi::rule<Iterator, int(), Skipper> integer_ = int_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** Grammar for a point: **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct xy_point_grammar : public qi::grammar<Iterator, point(), Skipper> {
        xy_point_grammar();
        
        qi::rule<Iterator, point(), Skipper> start;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** Grammar for a list of points: **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct xy_point_list_grammar : public qi::grammar<Iterator, xy_point_curve(), Skipper> {
        xy_point_list_grammar();
        
        qi::rule<Iterator, xy_point_curve(), Skipper> start;
        xy_point_grammar<Iterator, Skipper> point_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** Grammar for xy_point_curve_with_z **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct xyz_curve_grammar : public qi::grammar<Iterator, xy_point_curve_with_z(), Skipper> {
        xyz_curve_grammar();
        
        qi::rule<Iterator, xy_point_curve_with_z(), Skipper> start;
        xy_point_list_grammar<Iterator, Skipper> xy_list_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** Grammar for a list of xyz curves: **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct xyz_list_grammar : public qi::grammar<Iterator, vector<xy_point_curve_with_z>(), Skipper> {
        xyz_list_grammar();
        
        qi::rule<Iterator, vector<xy_point_curve_with_z>(), Skipper> start;
        xyz_curve_grammar<Iterator, Skipper> curve_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** Grammar for turbine efficiency **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct turbine_efficiency_grammar : public qi::grammar<Iterator, turbine_efficiency(), Skipper> {
        turbine_efficiency_grammar();
        
        qi::rule<Iterator, turbine_efficiency(), Skipper> start;
        xyz_curve_grammar<Iterator, Skipper> xyz_curve_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** Grammar for turbine description **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct turbine_description_grammar : public qi::grammar<Iterator, turbine_description(), Skipper> {
        turbine_description_grammar();
        
        qi::rule<Iterator, turbine_description(), Skipper> start;
        turbine_efficiency_grammar<Iterator, Skipper> turbine_efficiency_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief Grammar for absolute constraint */
    template<class Iterator, class Skipper=qi::ascii::space_type>
    using absolute_constraint_grammar =
        shyft::mp::grammar::hana_struct_grammar<absolute_constraint, Iterator, Skipper>;

    /** @brief Grammar for penalty constraint */
    template<class Iterator, class Skipper=qi::ascii::space_type>
    using penalty_constraint_grammar =
        shyft::mp::grammar::hana_struct_grammar<penalty_constraint, Iterator, Skipper>;

    /** @brief Template class for parsing into shared_ptr< map<utctime, shared_ptr<T>> >
     * 
     * @tparam Iterator: Input iterator type,
     * @tparam T: Base type of range. That is wrapped in shared_ptr< map< utctime, shared_ptr< T > > >
     * @tparam TGrammar: Parser type for T
     * @tparam Skipper: What to omit in parsing.
     */
    template<typename Iterator, typename T, typename TGrammar, typename Skipper=qi::ascii::space_type>
    struct t_map_grammar : public qi::grammar<Iterator, shared_ptr< map<utctime, shared_ptr<T> > >(), Skipper> {
        using range_type = shared_ptr<T>;
        using map_type = map<utctime, range_type>;
        using value_type = shared_ptr< map_type >;
                
        t_map_grammar();
        
        qi::rule<Iterator, value_type(), Skipper> start;
        TGrammar range_;
        utctime_grammar<Iterator> time_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** Grammar for timestamped message (pair<utctime, string>) **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct t_str_grammar : public qi::grammar<Iterator, pair<utctime, string>(), Skipper> {
        t_str_grammar();

        qi::rule<Iterator, pair<utctime, string>(), Skipper> start;
        utctime_grammar<Iterator> time_;
        quoted_string_grammar<Iterator, Skipper> str_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };


    /** Grammar for attribute_value_type **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    using attribute_value_type_grammar =
        typename decltype(+shyft::mp::grammar::get_variant_grammar_from_types<Iterator, Skipper>(
            shyft::web_api::energy_market::stm_system_value_types
            ))::type;
    /*
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct attribute_value_type_grammar : public qi::grammar<Iterator, attribute_value_type(), Skipper> {
        attribute_value_type_grammar();
        
        qi::rule<Iterator, attribute_value_type(), Skipper> start;
        quoted_string_grammar<Iterator, Skipper> quoted_string_;
        apoint_ts_grammar<Iterator, Skipper> apoint_ts_;
        time_axis_grammar<Iterator, Skipper> time_axis_;
        t_str_grammar<Iterator, Skipper> t_str_;
        qi::rule<Iterator, vector<pair<utctime, string>>(), Skipper> t_str_list_;
        t_map_grammar<Iterator, xy_point_curve, xy_point_list_grammar<Iterator, Skipper>, Skipper> t_xy_rule;
        t_map_grammar<Iterator, vector<xy_point_curve_with_z>, xyz_list_grammar<Iterator, Skipper>, Skipper> t_xyz_curves_rule;
        t_map_grammar<Iterator, turbine_description, turbine_description_grammar<Iterator, Skipper>, Skipper> t_turbine_description_rule;
        absolute_constraint_grammar<Iterator, Skipper> absolute_constraint_;
        penalty_constraint_grammar<Iterator, Skipper> penalty_constraint_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    */
    
    /** Grammar for list of attrbitues **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct attribute_value_list_grammar : public qi::grammar<Iterator, vector<attribute_value_type>(), Skipper> {
        attribute_value_list_grammar();
        
        attribute_value_type_grammar<Iterator, Skipper> proxy_;
        qi::rule<Iterator, vector<attribute_value_type>(), Skipper> start;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** Grammar for JSON struct **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct json_grammar : public qi::grammar<Iterator, json(), Skipper> {
        json_grammar();

        qi::rule<Iterator, json(), Skipper> json_;
        qi::rule<Iterator, std::vector<json>(), Skipper> json_list_;
        qi::rule<Iterator, std::vector<std::vector<json>>(), Skipper> json_table_;
        qi::rule<Iterator, value_type(), Skipper> value_;
        qi::rule<Iterator, std::pair<std::string, value_type>(), Skipper> pair_;
        utcperiod_grammar<Iterator, Skipper> period_;
        time_axis_grammar<Iterator, Skipper> time_axis_;
        real_parser<double, strict_real_policies<double>> strict_double_;
        quoted_string_grammar<Iterator,Skipper> quoted_string_;
        qi::rule<Iterator, std::vector<std::string>(), Skipper> string_list_;
        integer_list_grammar<Iterator, Skipper> integer_list_;
        attribute_value_type_grammar<Iterator, Skipper> proxy_;
        attribute_value_list_grammar<Iterator, Skipper> proxy_list_;
        model_info_grammar<Iterator, Skipper> mi_;
        stm_case_grammar<Iterator, Skipper> stm_case_;
        stm_session_grammar<Iterator, Skipper> stm_session_;
        model_ref_grammar<Iterator, Skipper> model_ref_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct request_grammar : public qi::grammar<Iterator, request(), Skipper> {
        request_grammar();
        qi::rule<Iterator, std::string(), Skipper> keyword_;
        json_grammar<Iterator, Skipper> json_;
        qi::rule<Iterator, request(), Skipper> request_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    


    using request_iterator_em= const char*;
    using request_skipper_em= qi::ascii::space_type;
    // Basic value type parsers:
    extern template struct integer_list_grammar<request_iterator_em, request_skipper_em>;
    extern template struct xy_point_grammar<request_iterator_em, request_skipper_em>;
    extern template struct xy_point_list_grammar<request_iterator_em, request_skipper_em>;
    extern template struct xyz_curve_grammar<request_iterator_em, request_skipper_em>;
    extern template struct xyz_list_grammar<request_iterator_em, request_skipper_em>;
    extern template struct turbine_efficiency_grammar<request_iterator_em, request_skipper_em>;
    extern template struct turbine_description_grammar<request_iterator_em, request_skipper_em>;

    extern template struct t_str_grammar<request_iterator_em, request_skipper_em>;
    // Parsers for value types of attribute_valueibutes:
    extern template struct t_map_grammar<request_iterator_em, xy_point_curve, xy_point_list_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    extern template struct t_map_grammar<request_iterator_em, xy_point_curve_with_z, xyz_curve_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    extern template struct t_map_grammar<request_iterator_em, vector<xy_point_curve_with_z>, xyz_list_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    extern template struct t_map_grammar<request_iterator_em, turbine_description, turbine_description_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    //extern template struct attribute_value_type_grammar<request_iterator_em, request_skipper_em>;
    //extern template struct attribute_value_list_grammar<request_iterator_em, request_skipper_em>;
    
    
    using t_xy_grammar = t_map_grammar<request_iterator_em, xy_point_curve, xy_point_list_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    using t_xyz_grammar = t_map_grammar<request_iterator_em, xy_point_curve_with_z, xyz_curve_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    using t_xyz_list_grammar = t_map_grammar<request_iterator_em, vector<xy_point_curve_with_z>, xyz_list_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    using t_turbine_description_grammar = t_map_grammar<request_iterator_em, turbine_description, turbine_description_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    // Parsers for web_api request and json structure
    extern template struct json_grammar<request_iterator_em, request_skipper_em>;
    extern template struct request_grammar<request_iterator_em, request_skipper_em>;
}

