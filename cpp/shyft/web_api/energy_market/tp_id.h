#pragma once
#include <cstdint>
namespace shyft::web_api::grammar {
    using std::int64_t;
    /**
     * @brief simple type to hold type coded as char and its id, like U123 for unit
     *
     */
    struct tp_id {
        int tp{0}; ///< any int will do here
        int64_t id{0};///< the id could be 64bits, and ms c++ is 32bits on int, need to be specific
        bool operator==(tp_id const &o) const {return tp==o.tp && id==o.id;}
        bool operator!=(tp_id const &o) const {return !operator==(o);}
    };
}
