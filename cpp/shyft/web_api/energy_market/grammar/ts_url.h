/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/tp_id.h>
#include <functional>


namespace shyft::web_api::grammar {
    

    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct tp_id_grammar : public qi::grammar<Iterator, tp_id(), Skipper> {
        tp_id_grammar();
        qi::rule<Iterator, tp_id(), Skipper> g_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** @brief function type for the resolve the dstm url items to a ts
     * 
     * @param mid model id in the dstm
     * @param cpth one or more tp_id elements to navigate through the components
     * @param attr_id the dot separated path from the object as identified by the above two level adressing scheme
     *
     */    
    using path_resolver=std::function<shyft::time_series::dd::apoint_ts(const std::string& mid, vector<tp_id> const&pth, const std::string& attr_id)>;


    
    /** @brief grammar for reading a ts_url of type dstm://model_id/g_type g_id/comp_type comp_id.attr_id
     * and bind to the requested time series attribute using the supplied 
     */
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct dstm_path_grammar : public qi::grammar<Iterator, shyft::time_series::dd::apoint_ts(), Skipper> {
        dstm_path_grammar(path_resolver fx);

        tp_id_grammar< Iterator, Skipper> tp_id_;
        qi::rule<Iterator, std::string(), Skipper> mid_;
        qi::rule<Iterator, std::string(), Skipper> attr_id_;
        qi::rule<Iterator, shyft::time_series::dd::apoint_ts(), Skipper> ts_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    private:
        path_resolver resolver;
    };

    extern template struct dstm_path_grammar<request_iterator_em, request_skipper_em>;
    extern template struct tp_id_grammar<request_iterator_em, request_skipper_em>;
}
