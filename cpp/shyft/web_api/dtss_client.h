#pragma once
#include <string>
#include <memory>
#include <vector>
#include <functional>

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/dtss/queue_msg.h>

namespace shyft::web_api {
    using std::string;
    using std::vector;
    using shyft::time_series::dd::ats_vector;
    using namespace shyft::core;
    using namespace shyft::dtss::queue;
    using shyft::dtss::ts_info;


    struct dtss_client_impl;///<fwd for impl part

    /**
     * @brief A web-api dtss client
     * @details
     * This class implements the web-api protocol in c++ using boost.beast, boost.spirit/karma for request generation and parsing.
     * It's intended usage is in scenarios where the connection between the client and the server needs to be routed over http protocols.
     * For all other scenarios, the native raw socket dtss-client is superior in functionality and performance.
     *
     */
    struct dtss_client {

        /**
         * @brief constructs a dtss_client
         * @details
         * Constructs the object, keeping the supplied parameters, but do nothing else.
         * @param host_ip the host ip address that locates the server
         * @param port the ip port number where the web-api is served
         * @param auth empty, or a string like `Basic base64(user:pwd)` string
         */
        dtss_client(string const&host_ip,int port,string auth={});
        dtss_client()=delete;
        ~dtss_client();// We must define this so that unique_ptr<T> works, otherwise, the compiler need to know at this point the complete type of dtss_client_impl

        /**
         * @brief find time-series
         * @details
         * Starts a session, then executes find with regex pattern, e.g. `shyft://test/^abc[1-3]$` will match all series
         * in the `test` container starting with `abc` followed by exactly one single digit 1..3
         *
         * @param pattern A ts-url, with regular expression
         * @returns a vector of time-series info(ts_info) that matches the critera
         */
        vector<ts_info> find(string const & pattern) const;

        /**
         * @brief store time-series
         * @details
         * Starts a session, then store specified time-series using supplied arguments.
         *
         * @param tsv a vector of named time-series, e.g. apoint_ts(ts_url,apoint_ts{values})
         * @param recreate_ts if true, the existing time-series is wiped out and replaced with the new values, otherwise just change content of existing time-series
         * @param cache_on_write also add the time-series fragments to cache so that observers get it without reading disk
         */
        void store_ts(const ats_vector & tsv, bool recreate_ts, bool cache_on_write) const;

        /**
         * @brief read time-series
         * @details
         * Starts a session, then invoke read_ts with supplied arguments.
         *
         * @param ts_ids time-series identifiers/urls that are wanted
         * @param read_period the minimum period read from disk(if needed,otherwise cache is used)
         * @param use_ts_cached_read use cache if possible
         * @param clip_period the returned results are clipped, so that points outside this range is excluded(defaults to read_period)
         * @param fx callable that takes ats_vector const&, returns bool, if true, continue, if false then unsubscribe and close request
         * @returns the resulting read ts vector
         */
        ats_vector read_ts(vector<string>const &ts_ids, utcperiod read_period, bool use_ts_cached_read, utcperiod clip_period=utcperiod{}) const;

        /**
         * @brief subscribe to read_ts
         * @details
         * Starts a session, subscribing to the specified ts_ids, for the given period and other params.
         * As long as the callable  bool fx(ats_vector const&)  returns true,
         * subscription is active, and updates are forwarded to the fx.
         * If fx returns false, the subscription is cancelled, and the function returns.
         *
         * @param ts_ids time-series identifiers/urls that are wanted
         * @param read_period the minimum period read from disk(if needed,otherwise cache is used)
         * @param use_ts_cached_read use cache if possible
         * @param clip_period the returned results are clipped, so that points outside this range is excluded
         * @param fx callable that takes ats_vector const&, returns bool, if true, continue, if false then unsubscribe and close request
         * @param ft callable that is called every n ms (n=5ms), returns bool if timer is to continue, false if cancel timer/operation
         *
         */
        void subscribe_ts(vector<string>const &ts_ids, utcperiod read_period, bool use_ts_cached_read, utcperiod clip_period,std::function<bool(ats_vector const&)> const& fx, std::function<bool()> const &ft) const;

        /**
         * @brief remove a single ts
         * @details
         * Removes the specified ts-url, as in destroy/wipe out. never to be recovered.
         */
        // NOT YET impl. in the protocol void remove(const string & name) const;
        
        vector<string> q_list();///< provide a list of all available queues
        msg_info q_msg_info(string const&q_name,string const&msg_id); 
        vector<msg_info> q_msg_infos(string const&q_name);
        void q_put(string const &q_name,string const&msg_id, string const&descript, utctime ttl,ats_vector const&tsv);
        tsv_msg_ q_get(string const &q_name, utctime max_wait);
        void q_ack(string const&q_name, string const&msg_id,string const&diag);
        size_t q_size(string const&q_name);
        //NOT YET: void q_add(string const &q_name);
        //NOT YET: void q_remove(string const &q_name);
        void q_maintain(string const&q_name,bool keep_ttl_items, bool flush_all=false);

    private:
        std::unique_ptr<dtss_client_impl> impl;///< the impl, located in the .cpp file, since this uses spirit/karma.
    };
}
