/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/web_api/web_api_generator.h>

namespace shyft::web_api::generator {

    template<class OutputIterator>
    utcperiod_generator<OutputIterator>::utcperiod_generator()
        : utcperiod_generator::base_type(pg)
    {
        using ka::true_;
        using ka::bool_;
        using ka::_1;
        using ka::_val;
        static constexpr auto is_valid=[](utcperiod const&p) {return p.valid();};
        
        pg =
              (&bool_(true)[_1= phx::bind(is_valid,_val)]<< '['<< time_[_1=phx::bind(&utcperiod::start,_val)]<<','<<time_[_1=phx::bind(&utcperiod::end,_val)]<<']')
                       |
              (&true_ <<"null")
        ;
        pg.name("utcperiod");
    }


    template struct utcperiod_generator<generator_output_iterator>;
}
