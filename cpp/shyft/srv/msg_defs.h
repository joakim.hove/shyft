/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <cstdint>
#include <exception>
#include <dlib/server.h>
#include <dlib/iosockstream.h>
#include <shyft/core/dlib_utils.h>

namespace shyft::srv {
    using std::string;

    /** @brief  message-types
    *
    * The message types used for the wire-communication of ec::srv.
    *
    */
    struct message_type {
        using type = uint8_t;
        static constexpr type SERVER_EXCEPTION = 0;
        static constexpr type MODEL_INFO = 1;
        static constexpr type MODEL_INFO_UPDATE = 2;
        static constexpr type MODEL_STORE = 3;
        static constexpr type MODEL_READ = 4;
        static constexpr type MODEL_DELETE = 5;
        static constexpr type MODEL_READ_ARGS = 6;
        static constexpr type MODEL_INFO_FILTERED = 7;
    };

    using msg=shyft::core::msg_util<message_type>; ///< so that we get low-level functions adapted to our enum type
}


