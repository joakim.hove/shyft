/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <map>
#include <memory>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/id_base.h>

namespace shyft::energy_market::hydro_power {
    //fwd decl:
    struct hydro_power_system;
    typedef std::shared_ptr<hydro_power_system> hydro_power_system_;
}

namespace shyft::energy_market::market {

    using std::shared_ptr;
    using std::weak_ptr;
    using std::make_shared;
    using std::map;
    using std::string;
    using namespace hydro_power;
    using hydro_power::hydro_power_system_;
    // fwd:
    struct power_module;
    struct model;

    /** @brief Energy-market model-area
    *
    * The model-area is a simplified model of an electric market, where there are no
    * internal (or neglible) bottlenecs for energy exchange.
    * You can think of as if all consumers/producers where directly connected to the same
    * bus-bar.
    * 
    * The production-consumption and exhange with others areas needs to sum up to zero for
    * all points in time.
    * 
    * The actors within the model-areas are modeled as 'power_modules', and the exchange between
    * areas is represented using power_lines.
    * 
    * The EMPS model solves the market problem in several phases, and iterations between those.
    * The detailed_hydro model is proximate description of the hydro-power system, and is used
    * to verify that the model-area aggregated hydro model consumption/production is possible
    * to realize within the detailed-hydro model, given the constraint on exchange, inflow, etc.
    *
    * @note detailed_hydro is an optional feature, the model-area could just be driven of other energy-sources
    *
    */
    struct model_area:id_base {
        weak_ptr<model> mdl;
        map<int,shared_ptr<power_module>> power_modules;
        hydro_power_system_ detailed_hydro;
        model_area() =default;
        model_area(shared_ptr<model>const&mdl, int id, const string& area_name,const string& json="") :id_base {id,area_name,json,{}},mdl(mdl) {}
        shared_ptr<model> get_model() { return mdl.lock(); }
        bool operator==(const model_area &o)const;
        bool operator!=(const model_area &o) const { return !operator==(o); }
        bool operator <(const model_area & o) const { return id < o.id; }

        bool equal_structure( const model_area& b)const;

        hydro_power_system_ get_detailed_hydro() { return detailed_hydro;}
        void set_detailed_hydro(hydro_power_system_ hps) { detailed_hydro = hps; }

        x_serialize_decl();
    };
}

x_serialize_export_key(shyft::energy_market::market::model_area)
