#pragma once

#include <shyft/core/core_serialization.h>
#include <shyft/core/core_archive.h>


#include <shyft/energy_market/constraints.h>

#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/hydro_power/waterway.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/optimization_summary.h>

#include <shyft/energy_market/stm/srv/task/stm_task.h>

// stuff needed to ensure vector<T>, map<K,V> etc. are automagically serializable
#include <boost/serialization/vector.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/version.hpp>

#include <sstream>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <shyft/energy_market/constraints.h>
#include <shyft/mp.h>


namespace shyft::energy_market::stm {
    namespace hana = boost::hana;
    namespace mp = shyft::mp;

    template <class Archive,class T>
    void serialize_stm_attributes(T& o, Archive& ar) {
        hana::for_each(mp::leaf_accessors(hana::type_c<T>), [&](auto a){
            ar & shyft::core::core_nvp(mp::leaf_accessor_id_str(a), mp::leaf_access(o, a)); }
        );
    }
}
