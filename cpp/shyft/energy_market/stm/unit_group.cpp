#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/market.h>

namespace shyft::energy_market::stm {
    
    unit_group::unit_group():mdl{nullptr},group_type(0) {mk_url_fx(this);}
    unit_group::unit_group(stm_system* mdl):mdl{mdl},group_type(0) {mk_url_fx(this);}
    
    int64_t unit_group_member::id() const {return unit?unit->id:0;}
    namespace {
        string ug_tag(int64_t id) noexcept {
            return "/U"+std::to_string(id);
        }
    }
    void unit_group::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
        if (levels>0 && mdl) {
            mdl->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
        }
        if (!template_levels) {
            constexpr std::string_view a = "/U${unit_group_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto idstr=ug_tag(id);
            std::copy(std::begin(idstr),std::end(idstr),rbi);
        }
    }
    bool unit_group::operator==(const unit_group& other) const {
        if(this==&other) return true;//equal by addr.
        constexpr auto members_equal=[](auto const&a,auto const&b) {
            if(a.size()!=b.size())
                return false;
            for(size_t i=0;i<a.size();++i) {
                if(! (*a[i] == *b[i]))
                    return false;
            }
            return true;
        };
        
        return  hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use this that seems to be robust cross platform construct
            mp::leaf_accessors(hana::type_c<unit_group>),
            id_base::operator==(other),//initial value of the fold
            [this, &other](bool s, auto&& a) {
                return s?stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a)):false; // only evaluate equal if the fold state is still true
            }
        ) && (members_equal(members,other.members));
    }
    
    void unit_group::add_unit(unit_ const& u, apoint_ts const& active_ts) {
        
        for(auto const&x:members) {
            if (u.get()==x->unit.get()) { // consider just return silent, since it's already there, the intention is reached.
                throw std::runtime_error("unit_group: unit '"+x->unit->name +"' already  in the group");
            }
        }
        auto ugm=std::make_shared<unit_group_member>(this,u,active_ts);
        members.emplace_back(ugm);
        update_sum_expressions();
    }
    /** adding and removing units must update expressions, and ensure unique set */
    void unit_group::remove_unit(unit_ const&u) {
        auto f=std::find_if(std::begin(members),std::end(members),[&u](auto const& x) {return x->unit.get()==u.get();});
        if( f != std::end(members)) {
            members.erase(f);
        }
        update_sum_expressions();
    }
    
    /** @brief update the production and sum to express the units 
    * 
    *  @note That is not the ideal solution: better with compute on demand, maybe as a fx-expr..
    */
    void unit_group::update_sum_expressions() {
        using shyft::time_series::dd::ats_vector;
        constexpr auto mk_sum=[](auto const& members, auto &&fx_result)->apoint_ts {
            ats_vector p;
            for(auto const&m:members) {
                auto const& result=fx_result(m);
                if(result.ts) {
                    if(!m->active.ts) // if no active ts, then assume it's always active
                        p.emplace_back(result);
                    else
                        p.emplace_back(result*m->active);//use math and mask
                }
            }
            return p.size()>0?p.sum():apoint_ts{};// notice: empty ts if empty input
        };
        production = mk_sum(members,[](auto const&m){return m->unit->production.result;});
        flow =mk_sum(members,[](auto const&m){return m->unit->discharge.result;});
    }

    energy_market_area_ unit_group::get_energy_market_area() const {
        if(!mdl)
            return nullptr;//
        for (auto const& ema : mdl->market) {
            for (auto const& ug : ema->unit_groups) {
                if (ug.get() == this) {
                    return ema;
                }
            }
        }
        return nullptr;
    }

    std::vector<string>
    unit_group::all_urls(std::string const& prefix) const {
        std::vector<std::string> r;
        string pre=prefix+ug_tag(id)+".";
        hana::for_each(mp::leaf_accessor_map(hana::type_c<unit_group>), [&r,&pre] (auto p) {
            r.push_back(pre+hana::first(p).c_str());
        });
        return r;
    }
    
    //-- unit group member.
    unit_group_member::unit_group_member(){mk_url_fx(this);}
    unit_group_member::unit_group_member(unit_group *group,unit_ const&u,apoint_ts const& a):group{group},unit{u},active{a}{mk_url_fx(this);}

    bool unit_group_member::operator==(const unit_group_member& other) const {
        return  hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use this that seems to be robust cross platform construct
            mp::leaf_accessors(hana::type_c<unit_group_member>),
            true,//initial value of the fold
            [this, &other](bool s, auto&& a) {
                return s?stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a)):false; // only evaluate equal if the fold state is still true
            }
        ) && unit==other.unit;// require pointers to be equal in this case
    }
    
    void unit_group_member::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
        if (levels && group) {
            group->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
        }
        if (!template_levels) {
            constexpr std::string_view a = "/M${unit_group_member_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto idstr="/M"+ (unit?std::to_string(unit->id):std::string("?"));
            std::copy(std::begin(idstr),std::end(idstr),rbi);
        }
    }
    
     /** ref header file for docs
     */
    apoint_ts compute_unit_group_ts(
        std::vector<unit_group_> const &groups,
        unit_ const&u,
        apoint_ts run_ts, //  a ts filled with one's for all time-steps of interest
        int64_t group_type,
        std::map<int64_t,int64_t> const *id_map
    ) {
        apoint_ts result;// accumulate group-id at timesteps as pr. run_ts
        const double min_group=1.0;//  group-id has to be in range 1 .. 2**60
        const double max_group=std::pow(2.0,60);// arbitrary large range
        for(auto const&ug:groups) {
            if(ug->group_type!=group_type) continue;
            auto ug_id=ug->id;
            if(id_map) {// if supplied to translation
                auto f=id_map->find(ug_id);
                if(f == id_map->end())
                    throw std::runtime_error("unit group id-map must have complete mapping, failed to find "+ std::to_string(ug_id));
                ug_id= f->second;
            }
            for(auto const &m:ug->members) { // match member to u
                
                if(m->unit.get()==u.get()) { // got it!
                    apoint_ts c=!m->active? // empty means always active
                        run_ts:// all time-steps of interest is active, 1.0
                        m->active.use_time_axis_from(run_ts).inside(0.9,1.1,0.0,1.0,0.0);// resample to run_ts time-axis, 0 out when not active/nan
                    // notice that c now is spanning the run_ts time-axis range (maybe with some zeros filled in)

                    if(!result) { // first time contribution?
                        result=c*double(ug_id); // make result equal to the first contribution
                    } else { // we must check if there is conflicts with existing membership accumulated.
                        auto conflicts= (result.inside(min_group,max_group,0.0,1.0,0.0)+c).values();//make result 1| 0, add
                        for(size_t i=0;i<conflicts.size();++i) {
                            if (conflicts[i]>1.0+0.1) {
                                calendar utc;
                                throw std::runtime_error("unit group conflict for unit"+u->name +" at time " +utc.to_string(run_ts.time(i)));
                            }
                        }
                        result = result + c*double(ug_id); // multiply 1.0 with ug->id, so we get 0 or group_id, then add.
                    }
                }
            }
        }
        return result;
    }
}
