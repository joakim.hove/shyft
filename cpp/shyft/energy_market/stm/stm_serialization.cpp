#include <shyft/energy_market/stm/stm_serialization.h>
#include <shyft/core/boost_serialization_std_opt.h>

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif
// instantiate serialization templates here, ref. shyft expression-serialization for the ids_c  type
using namespace boost::serialization;
using shyft::energy_market::stm::serialize_stm_attributes;
using core_iarchive=boost::archive::binary_iarchive;
using core_oarchive=boost::archive::binary_oarchive;
using shyft::core::core_arch_flags;

/** SERIALIZATION FOR CONSTRAINTS **/
template <class Archive>
void shyft::energy_market::core::constraint_base::serialize(Archive & ar, const unsigned int /*version*/) {
//	ar
//	& make_nvp("limit", limit)
//	& make_nvp("flag", flag)
//	;
}

template <class Archive>
void shyft::energy_market::core::absolute_constraint::serialize(Archive & ar, const unsigned int /*version*/) {
	ar
	& make_nvp("super", base_object<super>(*this))
  	& make_nvp("limit", limit)
	& make_nvp("flag", flag)
	;
}

template <class Archive>
void shyft::energy_market::core::penalty_constraint::serialize(Archive & ar, const unsigned int /*version*/) {
	ar
	& make_nvp("super", base_object<super>(*this))
  	& make_nvp("limit", limit)
	& make_nvp("flag", flag)
	& make_nvp("cost", cost)
	& make_nvp("penalty", penalty)
	;
}

/****/

template<class Archive>
void shyft::energy_market::stm::reservoir::serialize(Archive & ar, const unsigned int /*version*/) {
    ar & make_nvp("super",base_object<super>(*this)) ;
    serialize_stm_attributes(*this, ar);
}

template <class Archive>
void shyft::energy_market::stm::reservoir_aggregate::serialize(Archive & ar, const unsigned int /*version*/) {
    ar
    & make_nvp("id_base",base_object<shyft::energy_market::id_base>(*this))
    & make_nvp("hps", hps)
    //& make_nvp("id", id)
    //& make_nvp("name", name)
    //& make_nvp("json", json)
    & make_nvp("reservoirs",reservoirs)
    ;
	serialize_stm_attributes(*this, ar);
}


template<class Archive>
void shyft::energy_market::stm::unit::serialize(Archive & ar, const unsigned int /*version*/) {
    ar & make_nvp("super",base_object<super>(*this));
    serialize_stm_attributes(*this, ar);
}

template<class Archive>
void shyft::energy_market::stm::power_plant::serialize(Archive & ar, const unsigned int /*version*/) {
    ar & make_nvp("super",base_object<super>(*this)) ;
    serialize_stm_attributes(*this, ar);
}

template<class Archive>
void shyft::energy_market::stm::catchment::serialize(Archive & ar, const unsigned int /*version*/) {
    ar & make_nvp("super",base_object<super>(*this)) ;
    serialize_stm_attributes(*this, ar);
}

template<class Archive>
void shyft::energy_market::stm::waterway::serialize(Archive & ar, const unsigned int /*version*/) {
    ar & make_nvp("super",base_object<super>(*this)) ;
    serialize_stm_attributes(*this, ar);
}

template<class Archive>
void shyft::energy_market::stm::gate::serialize(Archive & ar, const unsigned int /*version*/) {
    ar & make_nvp("super",base_object<super>(*this)) ;
    serialize_stm_attributes(*this, ar);
}

template<class Archive>
void shyft::energy_market::stm::stm_hps::serialize(Archive & ar, const unsigned int version) {
    ar
    & make_nvp("super",base_object<super>(*this)) // core hydro power goes here(core model,topology)
	& make_nvp("reservoir_aggregates",reservoir_aggregates); 
}


template<class Archive>
void shyft::energy_market::stm::run_parameters::serialize(Archive& ar, const unsigned int version) {
	ar
	& make_nvp("super", base_object<super>(*this))
	& make_nvp("mdl", mdl);
    serialize_stm_attributes(*this, ar);
}

template<class Archive>
void shyft::energy_market::stm::unit_group::serialize(Archive& ar, const unsigned int version) {
	ar
	& make_nvp("super", base_object<super>(*this))
	& make_nvp("mdl", mdl)
    & make_nvp("members",members)
    ;
    serialize_stm_attributes(*this, ar);
}

template<class Archive>
void shyft::energy_market::stm::unit_group_member::serialize(Archive& ar, const unsigned int version) {
	ar
	& make_nvp("group",group)
    & make_nvp("unit",unit)
    ;
    serialize_stm_attributes(*this, ar);
}

template<class Archive>
void shyft::energy_market::stm::stm_system::serialize(Archive& ar, const unsigned int version) {
    ar & make_nvp("super",base_object<super>(*this));
    serialize_stm_attributes(*this, ar);
}

template<class Archive>
void shyft::energy_market::stm::energy_market_area::serialize(Archive& ar, const unsigned int /*version*/) {
    ar
    & make_nvp("super",base_object<super>(*this))
    & make_nvp("sys",sys)//weak_ptr Hmm. did that really work ..
    & make_nvp("unit_groups", unit_groups)
    ;
    serialize_stm_attributes(*this, ar);
}



template<class Archive>
void shyft::energy_market::stm::srv::model_ref::serialize(Archive& ar, const unsigned int /*version*/) {
	ar & make_nvp("host", host)
	   & make_nvp("port_num", port_num)
	   & make_nvp("api_port_num", api_port_num)
	   & make_nvp("model_key", model_key)
	   & make_nvp("labels", labels)
	   ;
}

template<class Archive>
void shyft::energy_market::stm::srv::stm_case::serialize(Archive& ar, const unsigned int /*version*/) {
	ar & make_nvp("id", id)
	   & make_nvp("name", name)
	   & make_nvp("json", json)
	   & make_nvp("created", created)
	   & make_nvp("labels", labels)
	   & make_nvp("model_refs", model_refs)
	   ;
}

template<class Archive>
void shyft::energy_market::stm::srv::stm_task::serialize(Archive& ar, const unsigned int /*version*/) {
	ar & make_nvp("id", id)
	   & make_nvp("name", name)
	   & make_nvp("json", json)
	   & make_nvp("created", created)
	   & make_nvp("labels", labels)
	   & make_nvp("runs", cases)
	   & make_nvp("base_mdl", base_mdl)
	   & make_nvp("task_name", task_name)
	   ;
}

template<class Archive>
void shyft::energy_market::stm::optimization_summary::serialize(Archive& ar, const unsigned int /*version*/) {
    ar 
    & make_nvp("super",base_object<super>(*this)) 	
    & make_nvp("mdl", mdl)
    ;
    serialize_stm_attributes(*this, ar);
}

//
// 4. Then include the archive supported
//

// repeat template instance for each archive class
#define xxx_arch(T) x_serialize_archive(T,boost::archive::binary_oarchive,boost::archive::binary_iarchive)

xxx_arch(shyft::energy_market::stm::unit);
xxx_arch(shyft::energy_market::stm::unit_group);
xxx_arch(shyft::energy_market::stm::unit_group_member);
xxx_arch(shyft::energy_market::stm::power_plant);
xxx_arch(shyft::energy_market::stm::reservoir);
xxx_arch(shyft::energy_market::stm::reservoir_aggregate);
xxx_arch(shyft::energy_market::stm::catchment);
xxx_arch(shyft::energy_market::stm::waterway);
xxx_arch(shyft::energy_market::stm::gate);
xxx_arch(shyft::energy_market::stm::stm_hps);
xxx_arch(shyft::energy_market::stm::run_parameters);
xxx_arch(shyft::energy_market::stm::stm_system);
xxx_arch(shyft::energy_market::stm::energy_market_area);
xxx_arch(shyft::energy_market::stm::srv::model_ref);
xxx_arch(shyft::energy_market::stm::srv::stm_case);
xxx_arch(shyft::energy_market::stm::srv::stm_task);
xxx_arch(shyft::energy_market::stm::optimization_summary);

namespace shyft::energy_market::stm {

/** polymorphic types needed some pre-registration to stream
 * If you get 'unknown class' error while doing serialization,
 * this is the place.
 */
template <class Archive>
void register_types(Archive& a) {
    a.template register_type<reservoir>();
	a.template register_type<reservoir_aggregate>();
    a.template register_type<waterway>();
    a.template register_type<unit>();
    a.template register_type<unit_group>();
    a.template register_type<gate>();
	a.template register_type<power_plant>();
    a.template register_type<catchment>();
    a.template register_type<stm_hps>();
    //a.template register_type<run_parameters>();
    a.template register_type<stm_system>();
    a.template register_type<energy_market_area>();
    a.template register_type<optimization_summary>();
}


/**fx_to_blob simply serializes an object to a blob */
template <class T>
static string fx_to_blob(const shared_ptr<T>&s) {
    std::ostringstream xmls;
    {
        core_oarchive oa(xmls,core_arch_flags);
        register_types(oa);
        oa << boost::serialization::make_nvp("hps", s);
    }
    xmls.flush();
    return xmls.str();
}

/** fx_from_blob de-serializes a blob to a fully working object*/
template<class T>
static shared_ptr<T> fx_from_blob(const string &xmls) {
    shared_ptr<T> s;
    std::istringstream xmli(xmls); {
        //boost::archive::xml_iarchive ia(xmli);
        core_iarchive ia(xmli,core_arch_flags);
        register_types(ia);
        ia >> boost::serialization::make_nvp("hps", s);
	}
    return s;
}

// implementation for main classes, using the templates above

string  stm_hps::to_blob(const shared_ptr<stm_hps>&s) {return fx_to_blob<stm_hps>(s);}
shared_ptr<stm_hps> stm_hps::from_blob(const string &xmls) {return fx_from_blob<stm_hps>(xmls);}
string  stm_system::to_blob(const shared_ptr<stm_system>&s) { return fx_to_blob<stm_system>(s);}
shared_ptr<stm_system> stm_system::from_blob(const string &xmls) { return fx_from_blob<stm_system>(xmls);}
    
}


