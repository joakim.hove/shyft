#pragma once
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/attribute_types.h>


namespace shyft::energy_market::stm {
	using std::string;
    using std::vector;
	using std::shared_ptr;
	using std::weak_ptr;
	using std::make_shared;
	using std::static_pointer_cast;
	using shyft::time_series::dd::apoint_ts;
	using std::map;
	using shyft::core::utctime;

	struct stm_system;
    struct unit_group;
    using unit_group_=shared_ptr<unit_group>;

	/* @brief energy market area
	*
	* suited for the stm optimization, keeps attributes for an area where the price/load and
	* other parameters controlling the optimization problem can be kept
	*
	*
	*/
	struct energy_market_area:id_base {
		using super = id_base;
		energy_market_area();
		energy_market_area(int id, const string& name, const string&json, const stm_system_& sys);

		/** @brief generate an almost unique, url-like string for a market.
         *
         * @param rbi: back_inserter to store result
         * @param levels: how many levels of the url to include.
         * 		levels == 0 includes only this level. use level < 0 to include all levels.
         * @param placeholders: the last element of the vector states wethers to use the reservoir id
         * 		in the url or a placeholder. the remaining vector will be used in subsequent levels of the url.
         * 		if the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels=-1, int template_levels = -1) const;

        vector<unit_group_> unit_groups;
        /** set a unit group on the energy market area creating an association */
        void set_unit_group(const unit_group_& ug);
        /** get the unit group, if available */
        unit_group_ get_unit_group() const;
        /** disassociating a unit group with the energy market area */
        unit_group_ remove_unit_group();
        
		stm_system_ sys_() const { return sys.lock(); }
		stm_system__ sys; ///< reference up to the 'owning' optimization system.
        BOOST_HANA_DEFINE_STRUCT(energy_market_area,
            (apoint_ts, price),   ///< money/J (input)
            (apoint_ts, load),    ///< W (requirement)
            (apoint_ts, max_buy), ///< W (constraint)
            (apoint_ts, max_sale),///< W (constraint)
            (apoint_ts, buy),     ///< W (result)
            (apoint_ts, sale),    ///< W (result)
            (apoint_ts, production), ///< W (result)
            (apoint_ts, reserve_obligation_penalty) ///< Nok (result)
        );

		x_serialize_decl();
	};
	using energy_market_area_ = shared_ptr<energy_market_area>;


}

x_serialize_export_key(shyft::energy_market::stm::energy_market_area);
