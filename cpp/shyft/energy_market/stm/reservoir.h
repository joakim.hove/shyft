#pragma once
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/constraints.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/url_fx.h>

namespace shyft::energy_market::stm {
	using std::string;
	using std::shared_ptr;
    using std::dynamic_pointer_cast;
	using shyft::core::utctime;
	using core::absolute_constraint;
	using core::penalty_constraint;
	using shyft::time_series::dd::apoint_ts;
	
	using std::weak_ptr;
	using reservoir_aggregate__=weak_ptr<reservoir_aggregate>;

	struct reservoir:hydro_power::reservoir {
		using super=hydro_power::reservoir;

        /** @brief generate an almost unique, url-like string for a reservoir.
         *
         * @param rbi: back inserter to store result
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels=-1, int template_levels = -1) const;
        
		reservoir(int id, const string& name, const string&json, stm_hps_ &hps);
		reservoir() {mk_url_fx(this);};
		
        reservoir_aggregate__ rsv_aggregate;///< be weak ref.
        reservoir_aggregate_ rsv_aggregate_() const {return rsv_aggregate.lock();}//.lock();};

        bool operator==(const reservoir& other) const;
        bool operator!=(const reservoir& other) const { return !( *this == other); };

		//-- local structs fo

        struct level_ {
            struct constraint_ {
                url_fx_t url_fx;// needed to link up py wrapped url paths
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                    (apoint_ts,min),
                    (apoint_ts,max)
                );
            };
            url_fx_t url_fx;// needed by .url(...) to python exposure
            BOOST_HANA_DEFINE_STRUCT(level_,
                (apoint_ts,regulation_min), ///< masl 
                (apoint_ts,regulation_max), ///< masl
                (apoint_ts,realised),         ///< masl
                (apoint_ts,schedule),         ///< masl
                (apoint_ts,result),           ///< masl
                (constraint_,constraint)
            );
        };

        struct volume_  {
            struct constraint_ {
                struct tactical_ {
                    url_fx_t url_fx;// needed to link up py wrapped url paths
                    BOOST_HANA_DEFINE_STRUCT(tactical_,
                        (penalty_constraint,min),   ///< m3
                        (penalty_constraint,max)    ///< m3
                    );
                };
                url_fx_t url_fx;// needed to link up py wrapped url paths
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                    (apoint_ts,min),   ///< m3
                    (apoint_ts,max),   ///< m3
                    (tactical_, tactical)
                );
            };

            struct slack_ {
                url_fx_t url_fx;// needed to link up py wrapped url paths
                BOOST_HANA_DEFINE_STRUCT(slack_,
                    (apoint_ts, lower), ///< m3
                    (apoint_ts, upper)  ///< m3
                );
            };
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(volume_,
                (apoint_ts, static_max),  ///< m3
                (apoint_ts, schedule),    ///< m3
                (apoint_ts, realised),    ///< m3
                (apoint_ts, result),      ///< m3
                (apoint_ts, penalty), ///< m3
                (constraint_, constraint),
                (slack_, slack)
            );
        };

        struct inflow_ {
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(inflow_,
                (apoint_ts, schedule),   ///< m3/s
                (apoint_ts, realised),   ///< m3/s
                (apoint_ts, result)      ///< m3/s
            );
        };

        struct water_value_ {
            url_fx_t url_fx;// needed to link up py wrapped url paths
            struct result_ {
                url_fx_t url_fx;// needed to link up py wrapped url paths
                BOOST_HANA_DEFINE_STRUCT(result_,
                (apoint_ts, local_volume),   ///< money/m3
                (apoint_ts, global_volume),  ///< money/m3
                (apoint_ts, local_energy),   ///< money/joule
                (apoint_ts, end_value)       ///< money
                );
            };

            BOOST_HANA_DEFINE_STRUCT(water_value_,
                (apoint_ts, endpoint_desc),  ///< money/joule
                (result_, result)
            );
        };

        struct ramping_ {
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(ramping_, 
                (apoint_ts, level_down),
                (apoint_ts, level_up)
            );
        };
        //-- finally the top-level attributes:
        BOOST_HANA_DEFINE_STRUCT(reservoir,
            (t_xy_,volume_level_mapping),
            (level_, level),
            (volume_, volume),
            (inflow_, inflow),
            (ramping_, ramping), // it's really  level constraints that applies to simulation/optimization
            (water_value_, water_value)
            );
		x_serialize_decl();
	};
	using reservoir_=shared_ptr<reservoir>;


}

x_serialize_export_key(shyft::energy_market::stm::reservoir);
