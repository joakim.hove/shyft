#pragma once
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/url_fx.h>
#include <shyft/energy_market/id_base.h>

namespace shyft::energy_market::stm {
    using std::string;
    using std::shared_ptr;
    using std::dynamic_pointer_cast;
    using std::map;
    using shyft::core::utctime;
	using shyft::energy_market::id_base;
    using shyft::time_series::dd::apoint_ts;
	using shyft::energy_market::hydro_power::shared_from_me;

    struct reservoir_aggregate: id_base {		
        /** @brief generate an almost unique, url-like string for a power station.
         *
         * @param rbi: back inserter to store result
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels=-1, int template_levels = -1) const;
		reservoir_aggregate(int id,const string& name, const string& json="", stm_hps_ const&hps=nullptr);
        reservoir_aggregate();
		~reservoir_aggregate();
		
		vector<reservoir_> reservoirs;///< the reservoirs allocated to this object
		stm_hps_ hps_() const { return hps.lock(); }
        stm_hps__ hps; ///< reference up to the 'owning' hydro-power system.
        reservoir_aggregate_ shared_from_this()  const; ///< shared from this using hps.reservoir_aggregates shared pointers.

        static void add_reservoir(const reservoir_aggregate_&ra,const reservoir_&r);

        void remove_reservoir(const reservoir_&r);
        
        bool operator==(const reservoir_aggregate& other) const;
        bool operator!=(const reservoir_aggregate& other) const { return !( *this == other); };
		
        struct inflow_ {
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(inflow_,
                (apoint_ts, schedule),   ///< m3/s
                (apoint_ts, realised),   ///< m3/s
                (apoint_ts, result)      ///< m3/s
            );
        };
		
        struct volume_ {
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(volume_,
                (apoint_ts, static_max), ///< m3
                (apoint_ts, schedule),   ///< m3
                (apoint_ts, realised),   ///< m3
                (apoint_ts, result)      ///< m3
            );
        };
		
		BOOST_HANA_DEFINE_STRUCT(reservoir_aggregate,
            (inflow_, inflow),
            (volume_, volume)
        );
		

        x_serialize_decl();


	};
	using reservoir_aggregate_ = shared_ptr<reservoir_aggregate>;
}
x_serialize_export_key(shyft::energy_market::stm::reservoir_aggregate);

