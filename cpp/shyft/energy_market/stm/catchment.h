#pragma once
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/attribute_types.h>

namespace shyft::energy_market::stm {
	using std::string;
	using std::shared_ptr;
    using std::dynamic_pointer_cast;
	using std::map;
	using shyft::core::utctime;

    
	struct catchment:hydro_power::catchment {
		using super=hydro_power::catchment;

        /** @brief generate an almost unique, url-like string for a catchment
         *
         * @param rbi: back inserter to store result;
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels=-1, int template_levels = -1) const ;
		//using rds = hps_rds<catchment>; // sih: when needed

		catchment(int id, const string& name,const string&json, stm_hps_ &hps):super(id,name,json,hps) { }
		catchment()=default;
        BOOST_HANA_DEFINE_STRUCT(catchment,
            (apoint_ts,inflow_m3s)
        );
        bool operator==(const catchment& other) const;
        bool operator!=(const catchment& other) const { return !( *this == other); };

		x_serialize_decl();
	};
	using catchment_=shared_ptr<catchment>;
}

x_serialize_export_key(shyft::energy_market::stm::catchment);

