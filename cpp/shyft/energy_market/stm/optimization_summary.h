#pragma once
#include <string>
#include <boost/hana.hpp>

#include <shyft/energy_market/url_fx.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/time_series/common.h>

//#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {
    using shyft::nan;
    using std::back_insert_iterator;
    struct stm_system;//fwd
    struct optimization_summary : public id_base  {
        using super = id_base;
        stm_system * mdl{nullptr}; ///< _not_ owned ref to the owning model that is _required_ to outlive the scope of the run_parameters,  The pointer should be const, but the model can be modified.

        optimization_summary();
        void generate_url(back_insert_iterator<string>& rbi, int levels=-1, int template_levels = -1) const;
        optimization_summary& operator=(optimization_summary const &o);// need to take care of this
        struct reservoir_ {
            reservoir_()
            :end_value{nan},sum_ramping_penalty{nan},sum_limit_penalty{nan},end_limit_penalty{nan}
            {}
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(reservoir_,
                                     (double, end_value),
                                     (double, sum_ramping_penalty),
                                     (double, sum_limit_penalty),
                                     (double, end_limit_penalty)
                                     );
        };

        struct gate_ {
            gate_()
            :ramping_penalty{nan},discharge_cost{nan},discharge_constraint_penalty{nan},spill_cost{nan}
            {}
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(gate_,
                    (double, ramping_penalty),
                    (double, discharge_cost),
                    (double, discharge_constraint_penalty),
                    (double, spill_cost)
                    );
        };

        struct waterway_ {
            waterway_():vow_in_transit{nan},discharge_group_penalty{nan},sum_discharge_fee{nan}
            {}
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(waterway_,
                                     (double, vow_in_transit),
                                     (double, discharge_group_penalty),
                                     (double, sum_discharge_fee)
                                     );
        };

        struct spill_ {
            spill_():nonphysical{nan},physical{nan}
            {}
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(spill_,
                                     (double, nonphysical),
                                     (double, physical)
                                     );
        };

        struct bypass_ {
            bypass_():cost{nan}
            {}
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(bypass_,
                                     (double, cost)
                                     );

        };

        struct ramping_ {
            ramping_():ramping_penalty{nan}
            {}
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(ramping_,
                                     (double, ramping_penalty)
                                     );
        };

        struct reserve_ {
            reserve_()
            :violation_penalty{nan},sale_buy{nan},obligation_value{nan}
            {}
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(reserve_,
                                     (double, violation_penalty),
                                     (double, sale_buy),
                                     (double, obligation_value)
                                     );
        };

        struct unit_ {
            unit_()
            : startup_cost{nan},schedule_penalty{nan}
            {}
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(unit_,
                                     (double, startup_cost),
                                     (double, schedule_penalty)
                                     );
        };

        struct plant_ {
            plant_()
            :production_constraint_penalty{nan},discharge_constraint_penalty{nan},schedule_penalty{nan}
            {}
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(plant_,
                                     (double, production_constraint_penalty),
                                     (double, discharge_constraint_penalty),
                                     (double, schedule_penalty),
                                     (double, ramping_penalty)
                                     );
        };

        struct market_ {
            market_()
            :sum_sale_buy{nan},load_penalty{nan},load_value{nan}
            {}
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(market_,
                                     (double, sum_sale_buy),
                                     (double, load_penalty),
                                     (double, load_value)
                                     );
        };

        BOOST_HANA_DEFINE_STRUCT(optimization_summary,
                                 (reservoir_, reservoir),
                                 (waterway_, waterway),
                                 (gate_, gate),
                                 (spill_, spill),
                                 (bypass_, bypass),
                                 (ramping_, ramping),
                                 (reserve_, reserve),
                                 (unit_, unit),
                                 (plant_, plant),
                                 (market_, market),
                                 (double, total),
                                 (double, sum_penalties),
                                 (double, minor_penalties),
                                 (double, major_penalties),
                                 (double, grand_total)
                                 );
        /** @return all urls of form dstm://M123/O.total, i.e prefix/path, where prefix is like dstm://M123 
         */
        std::vector<string> all_urls(std::string const& prefix) const;
        x_serialize_decl();
    };
    using optimization_summary_=std::shared_ptr<optimization_summary>;
}
x_serialize_export_key(shyft::energy_market::stm::optimization_summary);
