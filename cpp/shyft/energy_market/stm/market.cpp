#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/unit_group.h>

namespace shyft::energy_market::stm {
    using std::make_shared;

    energy_market_area::energy_market_area(){mk_url_fx(this);};
    
    energy_market_area::energy_market_area(int id, const string& name, const string&json, const stm_system_& sys) 
        : super{ id, name,json,{}}, sys{ sys }  {mk_url_fx(this);}
        
    void energy_market_area::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels) const {
        if (levels>=0) {
            auto tmp = sys_();
            if (tmp) tmp->generate_url(rbi, levels-1, template_levels ? template_levels - 1 : template_levels);
        }
        if (!template_levels) {
            constexpr std::string_view a = "/m${market_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto a="/m"+std::to_string(id);
            std::copy(std::begin(a), std::end(a), rbi);
        }
    }

    void energy_market_area::set_unit_group(const unit_group_& ug){
        if(!ug) {
            if(unit_groups.size()) // assign a nullptr, means remove unit group.
                unit_groups.pop_back();//
            return;
        }
        if(ug->group_type != unit_group_type::production) {
            throw std::runtime_error("energy market price area unit group type was "+std::to_string(ug->group_type)
                                    + ", must be "+ std::to_string(unit_group_type::production));
        }
        if(ug->id <= 0 ) {
            throw std::runtime_error("energy market price area unit group id must be >0, name="+ug->name
                                    + ", supplied value was " +std::to_string(ug->id));
        }
        if (unit_groups.size() == 1) {
            unit_groups[0]=ug;
        } else {
            unit_groups.push_back(ug);
        }
    }

    unit_group_ energy_market_area::get_unit_group() const {
        return (unit_groups.size() == 0) ? nullptr : unit_groups[0];
    }

    unit_group_ energy_market_area::remove_unit_group() {
        if (unit_groups.size() == 0) {
            throw std::runtime_error("there is no unit group associated with this energy market area");
        }
        unit_group_ ug = unit_groups[0];
        unit_groups.clear();
        return ug;
    }
}
