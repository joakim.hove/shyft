#include <shyft/energy_market/stm/shop/shop_adapter.h>
#include <shyft/energy_market/stm/shop/shop_data.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/time_series/dd/qac_ts.h>

namespace shyft::energy_market::stm::shop {

    shop_reserve_group shop_adapter::to_shop(const unit_group& a) const {
        auto b = api.create<shop_reserve_group>(a.name);
        // a.group_type == would be fcr_n|d, affr,mfrr, frr, rr
        // and that selects what attribute to map the 
        // a.constraint(.limit,.flag,.cost , result .penalty)
        // into the shop_reserve_group property.
        // TODO: consider find common  group/properties, and collapse common reserve group into one group.
        //       it is unclear if this will change the optimisation rules.
        // 
        //
        //
        apoint_ts run_ts(time_axis,0.0,shyft::time_series::POINT_AVERAGE_VALUE);//a ts for entire time_axis with zeros
        auto z_fill=[&run_ts] (apoint_ts const &x) { // ensures zeros where nan/ not specified 
            if(!x || x.total_period().contains(run_ts.total_period())) 
                return x;// if empty or covering the run time-axis, return as-is
            using shyft::time_series::dd::qac_parameter;//otherwise, resample(could be average..) and replace nan with z0.
            qac_parameter nan_to_0;nan_to_0.constant_filler=0.0;nan_to_0.max_timespan=utctime(0);
            return x.use_time_axis_from(run_ts).quality_and_self_correction(nan_to_0);
        };

        if(a.group_type == unit_group_type::fcr_n_up ) {//
            set_optional(b.fcr_n_up_obligation,z_fill(a.obligation.schedule));
            set_optional(b.fcr_n_penalty_cost,a.obligation.cost);
        } else if(a.group_type == unit_group_type::fcr_n_down ) {//
            set_optional(b.fcr_n_down_obligation,z_fill(a.obligation.schedule));
            set_optional(b.fcr_n_penalty_cost,a.obligation.cost);
        } else if(a.group_type == unit_group_type::fcr_d_up ) {//
            set_optional(b.fcr_d_up_obligation,z_fill(a.obligation.schedule));
            set_optional(b.fcr_d_penalty_cost,a.obligation.cost);
// when supported in next shop version
//        } else if(a.group_type == unit_group_type::fcr_d_down ) {//
//            set_optional(b.fcr_d_down_obligation,z_fill(a.obligation.schedule));
//            set_optional(b.fcr_d_down_penalty_cost,a.obligation.cost);
        } else if(a.group_type == unit_group_type::afrr_up ) {//
            set_optional(b.frr_up_obligation,z_fill(a.obligation.schedule));
            set_optional(b.frr_penalty_cost,a.obligation.cost);
        } else if(a.group_type == unit_group_type::afrr_down ) {//
            set_optional(b.frr_down_obligation,z_fill(a.obligation.schedule));
            set_optional(b.frr_penalty_cost,a.obligation.cost);
        } else if(a.group_type == unit_group_type::rr_up ) {//
            set_optional(b.rr_up_obligation,z_fill(a.obligation.schedule));
            set_optional(b.rr_penalty_cost,a.obligation.cost);
        } else if(a.group_type == unit_group_type::rr_down ) {//
            set_optional(b.rr_down_obligation,z_fill(a.obligation.schedule));
            set_optional(b.rr_penalty_cost,a.obligation.cost);
        }//TODO: consider how to map mFRR up/down.
        return b;
    }

    shop_market shop_adapter::to_shop(const energy_market_area& a) const {
        auto b = api.create<shop_market>(a.name);
        b.prod_area=ema_prod_area_id++;//a.id;// this seems to work.prod_area++;
        // a lot of trouble in this area: std::cout<<"EMA "<<a.name<<", shop.id="<<b.id<<", prod_area="<<b.prod_area <<std::endl;
        if (valid(a.price)) {
            set(b.sale_price, a.price);
            set(b.buy_price, ((apoint_ts)a.price) + 0.0000001); // TODO: Currently same as sale-price with fixed small addition (0.1 NOK/MWH == 0,0000001 NOK/WH), should add as dedicated stm attribute?
        }
        set_optional(b.max_sale, a.max_sale);
        set_optional(b.max_buy, a.max_buy);
        set_optional(b.load, a.load);
        return b;
    }

    xy_point_curve_ shop_adapter::get_spill_description(const reservoir& a) const {
        waterway_ wtr = nullptr;
        for (auto& conn : a.downstreams) {
            if (conn.role == connection_role::flood) {
                wtr = std::dynamic_pointer_cast<waterway>(conn.target);
                break;
            }
        }
        if (wtr) {
            if (wtr->gates.size() == 1) {
                auto gt = std::dynamic_pointer_cast<gate>(wtr->gates[0]);
                if (valid(gt->flow_description)) {
                    auto xyz_list = get(gt->flow_description); // Return std::shared_ptr<vector<xyz_point_curve_list>>
                    if (xyz_list && xyz_list->size()) {
                        return std::make_shared<xy_point_curve>((*xyz_list)[0].xy_curve);
                    }
                }
            } else {
                // Error handling if not the correct number of gates
                return nullptr;
            }
        } else {
            // Error handling if no spill waterway was found
            return nullptr;
        }
        return nullptr;
    }

    shop_reservoir shop_adapter::to_shop(const reservoir& a) const {
        auto b = api.create<shop_reservoir>(a.name);
        double default_lrl = 0., default_hrl = 0., default_max_vol = 0.;
        shared_ptr<xy_point_curve> vmap;
        auto valid_vmap = [&vmap]() {return vmap && vmap->points.size()>1;};
        if (valid(a.volume_level_mapping)) { // Exists and valid at period.start
            vmap = get(a.volume_level_mapping);
            if (valid_vmap()) { // require 2 or more points to consider valid
                default_lrl = vmap->points.front().y;
                default_hrl = vmap->points.back().y; // if spill description this default value will be replaced
                default_max_vol = vmap->points.back().x;
                set(b.vol_head, vmap);
            }
        }
        auto v = get_spill_description(a);
        if (v != nullptr) {
            default_hrl = v->points.front().x;
            if(valid_vmap())
                default_max_vol = vmap->calculate_x(default_hrl);
            set(b.flow_descr, v);
        }
        set_required(b.lrl, a.level.regulation_min, default_lrl);
        set_required(b.hrl, a.level.regulation_max, default_hrl);
        set_required(b.max_vol, a.volume.static_max, default_max_vol);// for some reason flooding does not work unless max_vol/ hrl is set
        //set_optional(b.endpoint_desc_nok_mwh, a.endpoint_desc_currency_mwh); // NO: endpoint_desc_currency_mwh is time series in STM, but in Shop it is an XY where x value is not used.
        //set_optional(b.min_constr, a.level.constraint_min); // TODO: Not exposed in API (yet)!
        //set_optional(b.max_constr, a.level.constraint_max); // TODO: Not exposed in API (yet)!
        //set_optional(b.??, a.ramping.level_down); // TODO: Not exposed in API (yet)!
        //set_optional(b.??, a.ramping.level_up); // TODO: Not exposed in API (yet)!
        set_optional(b.inflow, a.inflow.schedule);
        set_optional(b.volume_schedule, a.volume.schedule);
        set_optional(b.min_vol_constr, a.volume.constraint.min);
        set_optional(b.max_vol_constr, a.volume.constraint.max);
        set_optional(b.level_schedule, a.level.schedule);
        set_optional(b.start_head, a.level.realised); // Set start reservoir scalar value in Shop from value at start of period in historical time series in stm
        return b;
    }

    shop_unit shop_adapter::to_shop(const unit& a) const {
        auto b = api.create<shop_unit>(a.name);
        double default_pmin = 0., default_pmax = 0.;
        apoint_ts run_ts(time_axis,0.0,shyft::time_series::POINT_AVERAGE_VALUE);//a ts for entire time_axis with zeros
        auto z_fill=[&run_ts] (apoint_ts const &x) { // ensures zeros where nan/ not specified 
            if(!x || x.total_period().contains(run_ts.total_period())) 
                return x;// if empty or covering the run time-axis, return as-is
            using shyft::time_series::dd::qac_parameter;//otherwise, resample(could be average..) and replace nan with z0.
            qac_parameter nan_to_0;nan_to_0.constant_filler=0.0;nan_to_0.max_timespan=utctime(0);
            return x.use_time_axis_from(run_ts).quality_and_self_correction(nan_to_0);
        };
        auto nan_fill=[&run_ts] (apoint_ts const &x) { // ensures zeros where nan/ not specified
            if(!x || x.total_period().contains(run_ts.total_period())) 
                return x;// if empty or covering the run time-axis, return as-is
            //using shyft::time_series::dd::qac_parameter;//otherwise, resample(could be average..) and replace nan with z0.
            //qac_parameter nan_to_0;nan_to_0.constant_filler=0.0;nan_to_0.max_timespan=utctime(0);
            return x.use_time_axis_from(run_ts);//.quality_and_self_correction(nan_to_0);
        };

        // Generator efficiency
        if (valid(a.generator_description)) { // Exists and valid at period.start
            auto v = get(a.generator_description);
            if (v != nullptr) {
                default_pmin = v->points.front().x;
                default_pmax = v->points.back().x;
                set(b.gen_eff_curve, v);
            }
        }
        // Min/max/nom prod
        // TODO: Not used on aggregate level for pelton turbines when specified with needle combinations,
        // then it is part of the needle combination instead, but perhaps it does not hurt to always set them?
        set_required(b.p_min, a.production.static_min, default_pmin);
        set_required(b.p_max, a.production.static_max, default_pmax);
        set_required(b.p_nom, a.production.static_max, default_pmax); // Nominal production is just same as maximum, we don't need it to be anything - but Shop requires it to be set!
        // Turbine efficiency
        if (valid(a.turbine_description)) { // Exists and valid at period.start
            auto v = get(a.turbine_description);
            if (v != nullptr && v->efficiencies.size() > 0) {
                if (v->efficiencies.size() < 2) {
                    // Only one efficiency: Set it as total turbine efficiency
                    b.turb_eff_curves = v->efficiencies.front().efficiency_curves;
                } else {
                    // More than one efficiency: Assume Pelton turbine, emit as needle combinations.
                    size_t i = 0;
                    for (auto& nc : v->efficiencies) {
                        auto b2 = api.create<shop_needle_combination>(a.name + std::to_string(++i));
                        b2.p_min = nc.production_min;
                        b2.p_max = nc.production_max;
                        b2.p_nom = nc.production_max; // Nominal production is just same as maximum, we don't need it to be anything - but Shop probably requires it to be set!
                        b2.turb_eff_curves = nc.efficiency_curves;
                        api.connect_generator_needle_combination(b.id, b2.id);
                    }
                }
            }
        }
        // Other attributes
        set_optional(b.min_p_constr, a.production.constraint.min);
        set_optional(b.max_p_constr, a.production.constraint.max);
        set_optional(b.min_q_constr, a.discharge.constraint.min);
        set_optional(b.max_q_constr, a.discharge.constraint.max);
        set_optional(b.max_q_limit_rsv_down, a.discharge.constraint.max_from_downstream_level);
        set_optional(b.startcost, a.cost.start);
        set_optional(b.stopcost, a.cost.stop);
        set_optional(b.maintenance_flag, a.unavailability);
        set_optional(b.priority, a.priority);
        set_optional(b.discharge_schedule, a.discharge.schedule);
        set_optional(b.startcost, a.cost.start);
        set_optional(b.stopcost, a.cost.stop);
        //--------------------------------------------------------
        // operational reserve attributes
        // notice that we currently do not emit the group membership time-series here
        // instead, we emit those along with the unit-group emitter.
        //    so first emit group-definitions with the requirement/constraint to be reached
        //       then  emit each unit member-ship time-series into the group for the respective
        //                  7 reserve types (fcr_n.up|down, fcr_d, frr.up|down, rr.up|down)
        if(exists(a.reserve.droop.schedule)) {
            set_optional(b.fixed_droop,z_fill(a.reserve.droop.schedule));
            // After tip from Ole Andreas/Tellef 2021.03.09: try to leave the flag, just pass on nans
            // and it could work, even better.
            // auto flag=a.reserve.droop.schedule.inside(0.5,1000.0,0.0, 1.0,0.0);// just set limits low..high enough for the practical usage.
            // set_optional(b.fixed_droop_flag,z_fill(flag));
        }
        set_optional(b.droop_min, a.reserve.droop.min);
        set_optional(b.droop_max, a.reserve.droop.max);
        set_optional(b.droop_cost, a.reserve.droop_cost);


        const auto set_opt_w_flag=[this,nan_fill](auto&& shop_v,auto && /* shop_f*/,auto &&shop_min,auto &&shop_max, auto && ts, auto&& ts_min,auto &&ts_max) {
            if(exists(ts)) {
                set_optional(shop_v,nan_fill(ts));//currently we do not need to worry about the flag, just pass nans
            }
            set_optional(shop_min,ts_min);// there was a comment in shop manual about schedule at the same time as 
            set_optional(shop_max,ts_max);// maybe with flag?
        };
        
        set_optional(b.p_fcr_min, z_fill(a.reserve.fcr_static_min));
        set_optional(b.p_fcr_max, z_fill(a.reserve.fcr_static_max));

        set_optional(b.fcr_mip_flag, a.reserve.fcr_mip);
        set_optional(b.p_rr_min, a.reserve.mfrr_static_min);
        
        set_opt_w_flag(b.fixed_droop,b.fixed_droop_flag,b.droop_min,b.droop_max,a.reserve.droop.schedule,a.reserve.droop.min,a.reserve.droop.max);

        set_opt_w_flag(b.fcr_n_up_schedule,b.fcr_n_up_schedule_flag,b.fcr_n_up_min,b.fcr_n_up_max,a.reserve.fcr_n.up.schedule,a.reserve.fcr_n.up.min,a.reserve.fcr_n.up.max);
        set_opt_w_flag(b.fcr_n_down_schedule,b.fcr_n_down_schedule_flag,b.fcr_n_down_min,b.fcr_n_down_max,a.reserve.fcr_n.down.schedule,a.reserve.fcr_n.down.min,a.reserve.fcr_n.down.max);

        set_opt_w_flag(b.fcr_d_up_schedule,b.fcr_d_up_schedule_flag,b.fcr_d_up_min,b.fcr_d_up_max,a.reserve.fcr_d.up.schedule,a.reserve.fcr_d.up.min,a.reserve.fcr_d.up.max);
        //TODO v14 support?: set_opt_w_flag(b.fcr_d_down_schedule,b.fcr_d_down_schedule_flag,b.fcr_d_down_min,b.fcr_d_down_max,a.reserve.fcr_d.down.schedule,a.reserve.fcr_d.down.min,a.reserve.fcr_d.down.max);

        // TODO: shop frr, is that aFRR and mFRR ? currently we map affr to frr
        set_opt_w_flag(b.frr_up_schedule,b.frr_up_schedule_flag,b.frr_up_min,b.frr_up_max,a.reserve.afrr.up.schedule,a.reserve.afrr.up.min,a.reserve.afrr.up.max);
        set_opt_w_flag(b.frr_down_schedule,b.frr_down_schedule_flag,b.frr_down_min,b.frr_down_max,a.reserve.afrr.down.schedule,a.reserve.afrr.down.min,a.reserve.afrr.down.max);

        set_opt_w_flag(b.rr_up_schedule,b.rr_up_schedule_flag,b.rr_up_min,b.rr_up_max,a.reserve.rr.up.schedule,a.reserve.rr.up.min,a.reserve.rr.up.max);
        set_opt_w_flag(b.rr_down_schedule,b.rr_down_schedule_flag,b.rr_down_min,b.rr_down_max,a.reserve.rr.down.schedule,a.reserve.rr.down.min,a.reserve.rr.down.max);


        // This can be controlled with the commited_flag
        // Case:
        // If commited_in is set to true (1), in the same interval as a schedule,
        // schedule should prevail over commited_in

        // After tip from Ole Andreas/Tellef 2021.03.09: try to leave the flag, just pass on nans
        // and it could work, even better.

        // production_schedule serves two roles in SHOP:
        //  - input to plan optimisation - planned production
        //  - input to inflow calculation - actual production

        // Require user to use either realised or schedule
        if (exists(a.production.schedule) || exists(a.production.commitment)) {
            set_optional(b.production_schedule, a.production.schedule);
            set_optional(b.committed_in, a.production.commitment);
        } else {
            set_optional(b.production_schedule, a.production.realised);
        }

        return b;
    }

    shop_power_plant shop_adapter::to_shop(const power_plant& a) const {
        auto b = api.create<shop_power_plant>(a.name);
        set_optional(b.outlet_line, a.outlet_level);
        set_optional(b.mip_flag, a.mip);
        set_optional(b.block_merge_tolerance, a.production.merge_tolerance);
        set_optional(b.power_ramping_up, a.production.ramping_up);
        set_optional(b.power_ramping_down, a.production.ramping_down);
        set_optional(b.discharge_ramping_up, a.discharge.ramping_up);
        set_optional(b.discharge_ramping_down, a.discharge.ramping_down);
        set_optional(b.min_p_constr, a.production.constraint_min);
        set_optional(b.max_p_constr, a.production.constraint_max);
        set_optional(b.min_q_constr, a.discharge.constraint_min);
        set_optional(b.max_q_constr, a.discharge.constraint_max);
        set_optional(b.production_schedule, a.production.schedule);
        set_optional(b.discharge_schedule, a.discharge.schedule);
        set_optional(b.intake_loss_from_bypass_flag, a.discharge.intake_loss_from_bypass_flag);
        //set_if(b.maintenance_flag, a->availability); // TODO: Not exposed in API (yet)!
        return b;
    }

    shop_tunnel shop_adapter::to_shop(const waterway& wtr) const {
        auto b = api.create<shop_tunnel>(wtr.name);
        set_optional(b.max_flow, wtr.discharge.static_max); // constraint on the flow in the river bed and so comply to all the parallel gates as a whole
        auto loss_coeff = valid_temporal(wtr.head_loss_coeff) ? get_temporal(wtr.head_loss_coeff, 0.0) : 0.0;
        set_optional(b.loss_factor, loss_coeff);
        set_optional(b.start_height, wtr.geometry.z0);
        set_optional(b.end_height, wtr.geometry.z1);
        set_optional(b.diameter, wtr.geometry.diameter);
        set_optional(b.length, wtr.geometry.length);
        if (wtr.gates.size() == 1) {
            if (auto gt = std::dynamic_pointer_cast<gate>(wtr.gates[0])) {
                set_optional(b.gate_adjustment_cost, gt->cost);
                set_optional(b.gate_opening_curve, gt->opening.constraint.positions);
                auto continuous_gate_flag = valid_temporal(gt->opening.constraint.continuous) ? get_temporal(gt->opening.constraint.continuous, 0.0) : 0.0;
                set(b.continuous_gate, continuous_gate_flag >= 0.5 ? 1 : 0);
                set_optional(b.gate_opening_schedule, gt->opening.schedule);
                set_optional(b.initial_opening, gt->opening.realised); // Set initial opening value in Shop from value at start of period in historical time series in stm
                set_optional(b.min_flow, gt->discharge.constraint.min);
                set_optional(b.max_flow, gt->discharge.constraint.max);
            }
        }
        return b;
    }



    shop_discharge_group shop_adapter::to_shop_discharge_group(const waterway& wtr) {

        auto b = api.create<shop_discharge_group>("dg_" + wtr.name);

        if(!!wtr.discharge.reference && (!!wtr.discharge.constraint.accumulated_max || !!wtr.discharge.constraint.accumulated_min) ) {
            auto dev0 = initial_acc_deviation(time_axis.total_period().start,wtr.discharge.realised,wtr.discharge.reference);
            set_required(b.weighted_discharge_m3s, wtr.discharge.reference);
            set_required(b.initial_deviation_mm3, dev0);

            if(exists(wtr.discharge.constraint.accumulated_max)) {
                set_required(b.max_accumulated_deviation_mm3_up,wtr.discharge.constraint.accumulated_max);
            }

            if(exists(wtr.discharge.constraint.accumulated_min)) {
                set_required(b.max_accumulated_deviation_mm3_down, wtr.discharge.constraint.accumulated_min);
            }
        }
        set_optional(b.penalty_cost_up_per_mm3, wtr.discharge.penalty.cost.accumulated_max);
        set_optional(b.penalty_cost_down_per_mm3, wtr.discharge.penalty.cost.accumulated_min);
        return b;
    }

    shop_gate shop_adapter::to_shop_gate(const waterway& wtr, gate const* gt) const {
        auto b = api.create<shop_gate>(gt?gt->name:wtr.name);
        set_optional(b.max_discharge, wtr.discharge.static_max); // constraint on the flow in the river bed and so comply to all the parallel gates as a whole
        //set_optional(b.shape_discharge, wtr.delay); // time delay on the flow in the river bed and so comply to all the parallel gates as a whole
        set_shop_time_delay(b, wtr.delay); // time delay on the flow in the river bed and so comply to all the parallel gates as a whole
        if (gt) {
            if (&wtr != std::dynamic_pointer_cast<waterway>(gt->wtr_()).get()) // Gate is assumed to be in given water route!
                throw std::runtime_error("Specified waterroute and gate are not connected"s);

            set_optional(b.max_discharge, gt->discharge.static_max);
            set_optional(b.min_flow, gt->discharge.constraint.min);
            set_optional(b.max_flow, gt->discharge.constraint.max);
            set_optional(b.functions_meter_m3s, gt->flow_description);// if flood gate, this is already set on the rsv, is it ok to emit?
            set_optional(b.functions_deltameter_m3s, gt->flow_description_delta_h);

            // Note that only one of 'schedule_percent' and 'schedule_m3s' can exist,
            // setting 'schedule_percent' will erase 'schedule_m3s' and vice versa.
            if (exists(gt->discharge.schedule) || exists(gt->opening.schedule)) {
                // assume plan is intended and set to planned values
                set_optional(b.schedule_percent, gt->opening.schedule);
                set_optional(b.schedule_m3s, gt->discharge.schedule);  // Last to be set and taking precendence if available
            } else {
                // provide realised production data for inflow calculation, if available
                set_optional(b.schedule_percent, gt->opening.realised);
                set_optional(b.schedule_m3s, gt->discharge.realised);  // Last to be set and taking precendence if available
            }
        }
        return b;
    }

    void shop_adapter::from_shop(energy_market_area& mkt, const shop_market& shop_mkt) {
        set_optional(mkt.buy, shop_mkt.buy);
        set_optional(mkt.sale, shop_mkt.sale);
        set_optional(mkt.reserve_obligation_penalty, shop_mkt.reserve_obligation_penalty);
        // Inflow calc
        set_optional(mkt.sale, shop_mkt.sim_sale);
    }

    void shop_adapter::from_shop(reservoir& rsv, const shop_reservoir& shop_rsv) {
        set_optional(rsv.volume.result, shop_rsv.storage);
        set_optional(rsv.level.result, shop_rsv.head);
        set_optional(rsv.volume.penalty, shop_rsv.penalty);

        // from inflow calculation
        set_optional(rsv.inflow.result, shop_rsv.sim_inflow);
        set_optional(rsv.volume.result, shop_rsv.sim_storage);
        set_optional(rsv.level.result, shop_rsv.sim_head);

        // water-values
        set_optional(rsv.water_value.result.local_energy, shop_rsv.local_incr_cost_nok_mwh);
        set_optional(rsv.water_value.result.local_volume, shop_rsv.local_incr_cost_nok_mm3);
        set_optional(rsv.water_value.result.global_volume, shop_rsv.global_incr_cost_nok_mm3);
        set_optional(rsv.water_value.result.end_value, shop_rsv.end_value);
    }

    void shop_adapter::from_shop(power_plant& pp, const shop_power_plant & shop_ps) {
        set_optional(pp.production.result, shop_ps.production);
        set_optional(pp.discharge.result, shop_ps.discharge);

        // from inflow calculation
        set_optional(pp.production.result, shop_ps.sim_production);
        set_optional(pp.discharge.result, shop_ps.sim_discharge);// * should be * sum of units..
        set_optional(pp.discharge.realised, shop_ps.sim_discharge);// * should be * sum of units..
    }

    void shop_adapter::from_shop(unit& agg, const shop_unit& shop_agg) {
        set_optional(agg.discharge.result, shop_agg.discharge);
        set_optional(agg.production.result, shop_agg.production);

        // from operational reserve (frequency control)
        set_optional(agg.reserve.fcr_n.up.result,shop_agg.fcr_n_up_delivery);
        set_optional(agg.reserve.fcr_n.down.result,shop_agg.fcr_n_down_delivery);
        set_optional(agg.reserve.fcr_d.up.result,shop_agg.fcr_d_up_delivery);
        set_optional(agg.reserve.afrr.up.result,shop_agg.frr_up_delivery);
        set_optional(agg.reserve.afrr.down.result,shop_agg.frr_down_delivery);
        set_optional(agg.reserve.rr.up.result,shop_agg.rr_up_delivery);
        set_optional(agg.reserve.rr.down.result,shop_agg.rr_down_delivery);
        set_optional(agg.reserve.frr.up.penalty, shop_agg.frr_up_schedule_penalty);
        set_optional(agg.reserve.fcr_n.up.penalty, shop_agg.fcr_n_up_schedule_penalty);
        set_optional(agg.reserve.fcr_n.down.penalty, shop_agg.fcr_n_down_schedule_penalty);
        set_optional(agg.reserve.droop.result, shop_agg.droop_result);

        // from inflow calculation
        set_optional(agg.production.result, shop_agg.sim_production);// ? usually a forcing variable..

        set_optional(agg.discharge.result, shop_agg.sim_discharge);//
        set_optional(agg.discharge.realised, shop_agg.sim_discharge);// most natural mapping
        set_optional(agg.effective_head, shop_agg.sim_eff_head);
    }

    void shop_adapter::from_shop(waterway& wtr, const shop_gate& shop_gt) {

        set_optional(wtr.discharge.result, shop_gt.discharge);

        // from inflow calculation
        set_optional(wtr.discharge.result, shop_gt.sim_discharge);
    }



    void shop_adapter::from_shop(waterway& wtr, const shop_discharge_group& shop_dg) {


        set_optional(wtr.discharge.result, shop_dg.actual_discharge_m3s);
        set_optional(wtr.discharge.penalty.result.accumulated_max, shop_dg.upper_penalty_mm3);
        set_optional(wtr.discharge.penalty.result.accumulated_min, shop_dg.lower_penalty_mm3);
        // debug : get back results
        //set_optional(wtr.discharge.constraint.accumulated_min, shop_dg.max_accumulated_deviation_mm3_down);
        //set_optional(wtr.discharge.constraint.accumulated_max, shop_dg.max_accumulated_deviation_mm3_up);
        //set_optional(wtr.discharge.schedule, shop_dg.accumulated_deviation_mm3);


    }

    void shop_adapter::from_shop(gate& gt, const shop_gate& shop_gt) {
        set_optional(gt.discharge.result, shop_gt.discharge);

        // from inflow calculation
        set_optional(gt.discharge.result, shop_gt.sim_discharge);

        // add to waterroute total
        if (const auto& wtr = dynamic_pointer_cast<waterway>(gt.wtr_())) {
            if (!wtr->discharge.result)
                wtr->discharge.result = gt.discharge.result;
            else
                wtr->discharge.result = wtr->discharge.result + gt.discharge.result;
        }
    }

    void shop_adapter::from_shop(waterway& wtr, const shop_tunnel& shop_tn) {
        set_optional(wtr.discharge.result, shop_tn.flow);
        if (wtr.gates.size() == 1) {
            if (auto gt = std::dynamic_pointer_cast<gate>(wtr.gates[0])) {
                set_optional(gt->opening.result, shop_tn.gate_opening);
            }
        }
    }


    void shop_adapter::from_shop(unit_group& a, const shop_reserve_group& b) {
        if(a.group_type == unit_group_type::fcr_n_up ) {//
            set_optional(a.obligation.penalty,b.fcr_n_up_violation);
            set_optional(a.obligation.result,b.fcr_n_up_slack);
        } else if(a.group_type == unit_group_type::fcr_n_down ) {//
            set_optional(a.obligation.penalty,b.fcr_n_down_violation);
            set_optional(a.obligation.result,b.fcr_n_down_slack);
        } else if(a.group_type == unit_group_type::fcr_d_up ) {//
            set_optional(a.obligation.penalty,b.fcr_d_up_violation);
            set_optional(a.obligation.result,b.fcr_d_up_slack);
        } else if(a.group_type == unit_group_type::afrr_up ) {//
            set_optional(a.obligation.penalty,b.frr_up_violation);
            set_optional(a.obligation.result,b.frr_up_slack);
        } else if(a.group_type == unit_group_type::afrr_down ) {//
            set_optional(a.obligation.penalty,b.frr_down_violation);
            set_optional(a.obligation.result,b.frr_down_slack);
        } else if(a.group_type == unit_group_type::rr_up ) {//
            set_optional(a.obligation.penalty,b.frr_up_violation);
            set_optional(a.obligation.result,b.frr_up_slack);
        } else if(a.group_type == unit_group_type::rr_down ) {//
            set_optional(a.obligation.penalty,b.frr_down_violation);
            set_optional(a.obligation.result,b.frr_down_slack);
        }//TODO: consider how to map mFRR up/down.
    }
    void shop_adapter::from_shop(optimization_summary& osm, const shop_objective& shop_obj) const {
        /* Note double set_optional on shop attrs containing sim_**
         * If shop is used for optimization, then the attr NOT containing sim_** will be set.
         * Else, if shop is used for simulation, the sim_** attr will be set. Hence we expose
         * the shop attr to stm model by context, how the model i runned by the user.
         **/
        // Top attrs(
        set_optional(osm.total, shop_obj.total);
        set_optional(osm.sum_penalties, shop_obj.sum_penalties);
        set_optional(osm.minor_penalties, shop_obj.minor_penalties);
        set_optional(osm.major_penalties, shop_obj.major_penalties);
        set_optional(osm.grand_total, shop_obj.grand_total);
        // Inflow calc
        set_optional(osm.grand_total, shop_obj.sim_grand_total);

        // Reservoir
        set_optional(osm.reservoir.sum_ramping_penalty, shop_obj.rsv_ramping_penalty);
        set_optional(osm.reservoir.sum_limit_penalty, shop_obj.rsv_penalty);
        set_optional(osm.reservoir.sum_limit_penalty, shop_obj.sim_rsv_penalty);
        set_optional(osm.reservoir.end_value, shop_obj.rsv_end_value);
        set_optional(osm.reservoir.end_value, shop_obj.sim_rsv_end_value);
        set_optional(osm.reservoir.end_limit_penalty, shop_obj.rsv_end_penalty);

        // Waterway
        set_optional(osm.waterway.vow_in_transit, shop_obj.vow_in_transit);
        set_optional(osm.waterway.sum_discharge_fee, shop_obj.sum_discharge_fee);
        set_optional(osm.waterway.discharge_group_penalty, shop_obj.discharge_group_penalty);

        // Gate
        set_optional(osm.gate.ramping_penalty, shop_obj.gate_ramping_penalty);
        set_optional(osm.gate.discharge_cost, shop_obj.gate_discharge_cost);
        set_optional(osm.gate.discharge_constraint_penalty, shop_obj.gate_q_constr_penalty);
        set_optional(osm.gate.spill_cost, shop_obj.gate_spill_cost);

        // Spill
        set_optional(osm.spill.nonphysical, shop_obj.nonphysical_spill_cost);
        set_optional(osm.spill.physical, shop_obj.physical_spill_cost);

        // Bypass
        set_optional(osm.bypass.cost, shop_obj.bypass_cost);

        // Ramping
        set_optional(osm.ramping.ramping_penalty, shop_obj.sum_ramping_penalty);

        // Reserve
        set_optional(osm.reserve.violation_penalty, shop_obj.reserve_violation_penalty);
        set_optional(osm.reserve.sale_buy, shop_obj.reserve_sale_buy);
        set_optional(osm.reserve.obligation_value, shop_obj.reserve_oblig_value);

        // Unit
        set_optional(osm.unit.startup_cost, shop_obj.startup_costs);
        set_optional(osm.unit.startup_cost, shop_obj.sim_startup_costs);
        set_optional(osm.unit.schedule_penalty, shop_obj.gen_schedule_penalty);

        // Plant
        set_optional(osm.plant.production_constraint_penalty, shop_obj.plant_p_constr_penalty);
        set_optional(osm.plant.discharge_constraint_penalty, shop_obj.plant_q_constr_penalty);
        set_optional(osm.plant.schedule_penalty, shop_obj.plant_schedule_penalty);
        set_optional(osm.plant.ramping_penalty, shop_obj.plant_ramping_penalty);

        // Market
        set_optional(osm.market.sum_sale_buy, shop_obj.market_sale_buy);
        set_optional(osm.market.sum_sale_buy, shop_obj.market_sale_buy);

        set_optional(osm.market.load_penalty, shop_obj.load_penalty);
        set_optional(osm.market.load_value, shop_obj.load_value);
        // Inflow calc
        set_optional(osm.market.sum_sale_buy, shop_obj.sim_market_sale_buy);
    }
}
