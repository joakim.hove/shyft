#pragma once
#include <functional>

namespace shyft::energy_market::stm::shop {

/**
 * Utility class with static function objects that can be hooked
 * onto programatically to receive callbacks from the core shop api.
 */
struct shop_log_hook {
    static std::function<void(const char*)> info;
    static std::function<void(const char*)> warning;
    static std::function<void(const char*)> error;
    static std::function<void()> exit;
};

}