/*
 * Integration of stm with shop api via proxy.
 */
#pragma once
#include <vector>
#include <memory>
#include <ostream>
#include <stdexcept>
#include <shyft/energy_market/stm/shop/shop_adapter.h>
#include <shyft/energy_market/stm/shop/shop_data.h>
#include <shyft/energy_market/stm/unit_group.h>

namespace shyft::energy_market::stm::shop {

using namespace shyft::energy_market;
using namespace shyft::energy_market::stm;
using shyft::core::to_seconds64;
using shyft::energy_market::id_base;

/** @brief shop object identifier
 *  @details 
 *  Utility class for uniqely identifying a Shop object (any of the shop::proxy::obj specializations).
 *  All object ids are only unique within same object type, so we must include the object type id as well.
 *  It plays the role of mapping shop-objects to stm-objects.
 *
 */
struct shop_object_id {
    using t_id_type = int;//decltype(shop_reservoir::t_id);
    using id_type = int;// decltype(shop_reservoir::id);
    t_id_type t_id; // shop object type id
    id_type id; // shop object index, as reported when object is created, unique within same object type
    shop_object_id() : t_id{ -1 }, id{ -1 } {}
    shop_object_id(t_id_type t_id, id_type id) : t_id{ t_id }, id{ id } {}
    template<int T> shop_object_id(const shop_object<T>& o) : t_id{ o.t_id }, id{ o.id } {}
    explicit operator bool() const { return t_id >= 0 && id >= 0; }
    bool operator!() const { return static_cast<bool>(*this) == false; }
    bool operator <(const shop_object_id &o) const { return t_id < o.t_id || (t_id == o.t_id && id < o.id); }
};

/** @brief shop-objects to stm objects mapping
 *  @details
 *  Utility class for keeping Shop objects and their connection to STM objects.
 *  Filled when converting an STM model before running Shop, and then used for putting results back from Shop objects into STM.
 *  Notice that in this context, all 'shop-objects' are keept by type-safe remote reference, that is shop {type-id,object-id},
 *  so they do not carry any data except for the precise definitions.
 */
struct shop_objects {
    // Main storage structure: Tuple with one map for each shop object type.
    template<class T> using map_type = std::map<const id_base*, T>; // Mapping from stm object pointer to shop object (recall, just the shop-type,shop-id)
    //using key_type = stm_object_id; //template<class T> using key_type = map_type<T>::key_type;
    std::tuple<
        map_type<shop_market>,
        map_type<shop_reservoir>,
        map_type<shop_power_plant>,
        map_type<shop_unit>,
        map_type<shop_tunnel>,
        map_type<shop_gate>,
        map_type<shop_reserve_group>,
        map_type<shop_objective>,
        map_type<shop_discharge_group>
    > maps;

    // Basic access functions, similar to std::map.
    // Since users of this object probably never reference it as a const, there are explicit
    // const variants of everything, similar to what std has for cbegin and cend (only).
    template<class T> using iterator = typename map_type<T>::iterator;
    template<class T> using const_iterator = typename map_type<T>::const_iterator;
    template<class T> map_type<T>& get() { return std::get<map_type<T>>(maps); }
    template<class T> const map_type<T>& get() const { return std::get<map_type<T>>(maps); }
    template<class T> const map_type<T>& cget() const { return get<T>(); }
    template<class T> typename map_type<T>::size_type size() const { return cget<T>().size(); }
    template<class T> iterator<T> begin() { return get<T>().begin(); }
    template<class T> const_iterator<T> begin() const { return cget<T>().cbegin(); }
    template<class T> const_iterator<T> cbegin() const { return begin<T>(); }
    template<class T> iterator<T> end() { return get<T>().end(); }
    template<class T> const_iterator<T> end() const { return cget<T>().cend(); }
    template<class T> const_iterator<T> cend() const { return end<T>(); }
    template<class T> iterator<T> find(const id_base* o) { return get<T>().find(o); }
    template<class T> const_iterator<T> find(const id_base* o) const { return cget<T>().find(o); }
    template<class T> const_iterator<T> cfind(const id_base* o) const { return find<T>(o); }

    // Getters for shop object of a given type corresponding to a given stm object.
    template<class T> T& get(const id_base* o) {
        if (!o) throw std::invalid_argument("stm object is nullptr");
        if (auto it = find<T>(o); it != end<T>())
            return it->second;
        throw std::out_of_range("shop object of specified type not found for given stm object");
    }
    template<class T> T& get(const id_base& o) { return get<T>(&o); }
    template<class T_shop, class T_stm> T_shop& get(const shared_ptr<T_stm>& o) { return get<T_shop>(o.get()); }

    template<class T> const T& get(const id_base* o) const {
        if (!o) throw std::invalid_argument("stm object is nullptr");
        if (const auto it = find<T>(o); it != end<T>())
            return it->second;
        throw std::out_of_range("shop object of specified type not found for given stm object");
    }
    template<class T> const T& get(const id_base& o) const { return get<T>(&o); }
    template<class T_shop, class T_stm> const T_shop& get(const shared_ptr<T_stm>& o) const { return get<T_shop>(o.get()); }

    template<class T> const T& cget(const id_base* o) const { return get<T>(o); }
    template<class T> const T& cget(const id_base& o) const { return get<T>(&o); }
    template<class T_shop, class T_stm> const T_shop& cget(const shared_ptr<T_stm>& o) const { return get<T_shop>(o.get()); }

    template<class T> T& operator()(const id_base* o) { return get<T>(o); }
    template<class T> T& operator()(const id_base& o) { return get<T>(&o); }
    template<class T_shop, class T_stm> T_shop& operator()(const shared_ptr<T_stm>& o) { return get<T_shop>(o.get()); }

    template<class T> const T& operator()(const id_base* o) const { return get<T>(o); }
    template<class T> const T& operator()(const id_base& o) const { return get<T>(&o); }
    template<class T_shop, class T_stm> const T_shop& operator()(const shared_ptr<T_stm>& o) const { return get<T_shop>(o.get()); }

    // Setters for shop object of a given type corresponding to a given stm object.
    template<class T> T& add(const id_base* o, T&& o_shop) {
        if (!o) throw std::invalid_argument("stm object is nullptr");
        auto p = get<T>().emplace(o, std::forward<T>(o_shop));
        if (p.second)
            return p.first->second;
        throw std::out_of_range("a shop object of specified type already exists for given stm object");
    }
    template<class T> T& add(const id_base& o_stm, T&& o_shop) {
        return add<T>(&o_stm, std::forward<T>(o_shop));
    }
    template<class T_shop, class T_stm> T_shop add(const shared_ptr<T_stm>& o_stm, T_shop&& o_shop) {
        return add<T_shop>(o_stm.get(), std::forward<T_shop>(o_shop));
    }

    template<class T> T& set(const id_base* o_stm, T&& o_shop) {
        if (!o_stm) throw std::invalid_argument("stm object is nullptr");
        return get<T>().insert_or_assign(o_stm, std::forward<T>(o_shop)).first->second;
    }
    template<class T> T& set(const id_base& o_stm, T&& o_shop) {
        return set<T>(&o_stm, std::forward<T>(o_shop));
    }
    template<class T_shop, class T_stm> T_shop set(const shared_ptr<T_stm>& o_stm, T_shop&& o_shop) {
        return set<T_shop>(o_stm.get(), std::forward<T_shop>(o_shop));
    }

    // Check if shop object corresponding to a given stm object exists.
    // Similar to id_of, which returns an shop_object_id that can be
    // tested with operator ! or converted to boolean to see if it is valid.
    // The result should per def. be identical, only difference would
    // be if an stm object was mapped to a shop object which corresponds
    // to an invalid shop_object_id, but that should never be the case!
    template<class T> bool exists(const id_base* o) const {
        if (!o) throw std::invalid_argument("stm object is nullptr");
        return find<T>(o) != end<T>();
    }
    template<class T> bool exists(const id_base& o) const { return exists<T>(&o); }
    template<class T_shop, class T_stm> bool exists(const shared_ptr<T_stm>& o) const { return exists<T_shop>(o.get()); }

    // Find id of shop object of a given type corresponding to a given stm object.
    template<class T> shop_object_id id_of(const id_base* o) const {
        if (!o) throw std::invalid_argument("stm object is nullptr");
        if (const auto it = find<T>(o); it != end<T>())
            return it->second;
        return {};
    }
    template<class T> shop_object_id id_of(const id_base& o) const { return id_of<T>(&o); }
    template<class T_shop, class T_stm> shop_object_id id_of(const shared_ptr<T_stm>& o) const { return id_of<T_shop>(o.get()); }

    // Find id of shop object corresponding to a given stm object, which in some cases can be one of several shop object types
    shop_object_id id_of(const energy_market_area* o) const { return id_of<shop_market>(o); }
    shop_object_id id_of(const unit_group* o) const { return id_of<shop_reserve_group>(o); }
    shop_object_id id_of(const reservoir* o) const { return id_of<shop_reservoir>(o); }
    shop_object_id id_of(const unit* o) const { return id_of<shop_unit>(o); }
    shop_object_id id_of(const waterway* o) const {
        auto id = id_of<shop_gate>(o);
        if (!id) id = id_of<shop_tunnel>(o);
        return id;
    }
    shop_object_id id_of(const gate* o) const { return id_of<shop_gate>(o); }
    //shop_object_id id_of(const id_base*) const { static_assert(false, "This energy market object type is not supported by shop emitter"); return {}; };

    template<class T, typename = std::enable_if_t<std::is_base_of<id_base, T>::value && !std::is_const<T>::value>>
    shop_object_id id_of(T* o) const { return id_of(const_cast<const T*>(o)); } // Avoid resolving non-const pointer to template overload id_of(const T&) with T=type*, leading to recursive expansion (type *const *const * ...)
    template<class T, typename = std::enable_if_t<std::is_base_of<id_base, T>::value>>
    shop_object_id id_of(const T& o) const { return id_of(&o); }
    template<class T, typename = std::enable_if_t<std::is_base_of<id_base, T>::value>>
    shop_object_id id_of(const shared_ptr<T>& o) const { return id_of(o.get()); }
};

/** @brief main interface and working class for the shop-stm interface
 * 
 *  @details
 *  This class provides main .to_shop(...) and .from_shop(..) using the shop_adapter class
 *  as a tool.
 */
struct shop_emitter {

    shop_api& api;///< non-owning ref to shop_api that have the type-safe io to shop
    shop_adapter& adapter; ///< non-owning ref to shop_adapter, to which we delegate some work

    shop_objects objects; ///< storage for all shop objects and their connection/mapping to STM objects

    shop_emitter(shop_api& api, shop_adapter& adapter) : api{api}, adapter{adapter} {}

    // Top level to/from shop

    void to_shop(const stm_system& stm);
    void from_shop(stm_system& stm);

    // Lower level utility functions, normally just used internally
    // from top level functions, but also called directly from tests etc.

    template<class T> void emit(const vector<shared_ptr<T>>& v) { for (const auto& i : v) emit(*i); }
    template<class T> void emit(const T& v) { objects.add(&v, adapter.to_shop(v)); }
    void emit(const stm_hps& hps);
    void emit(const vector<hydro_power::reservoir_>& reservoirs);
    void emit(const hydro_power::reservoir& reservoir);
    void emit(const hydro_power::power_plant& plant);

    template<class T> void emit(const T* o) {
        if (!o) throw std:: invalid_argument("object is nullptr");
        return emit(*o);
    }
    template<class T> void emit(T* o) { emit(const_cast<const T*>(o)); } // Avoid resolving non-const pointer to template overload to_shop(const T&) with T=type*, leading to recursive expansion (type *const *const * ...)
    template<class T> void emit(const shared_ptr<T>& o) { return emit(o.get()); }
    
    template<class F> void foreach_upstream_shop_source(const waterway& wtr, F func);

    void handle_reservoir_output(const reservoir& rsv);
    void handle_plant_input(const waterway& obj, shop_object_id shop_obj);
    void handle_plant_output(const shop_object_id shop_plant_obj, const waterway& tailrace);
    void handle_discharge_group(const waterway& wtr);
    void handle_time_delay(const waterway& wtr);

    bool is_tunnel(const waterway& wtr) const;
    static bool is_plant_outlet(const waterway& wtr);
    static bool is_plant_tailrace(const waterway& wtr);

    double get_tunnel_loss_coeff(const waterway& wtr) const;

    static waterway_ get_penstock(const unit& agg, bool always_inlet = false);
    static waterway_ get_tailrace(const unit& agg);
    static waterway_ get_tailrace(const power_plant& plant);
};

}
