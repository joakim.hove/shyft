#pragma once
#include <vector>
#include <memory>
#include <shyft/core/subscription.h>
#include <shyft/energy_market/a_wrap.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/srv/dstm/context.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/shop/api/shop_enums.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/shop/shop_log_entry.h>
#include <shyft/energy_market/stm/shop/api/shop_proxy.h>


namespace shyft::energy_market::stm::shop {
    using namespace shyft::energy_market::stm;
    using shyft::core::subscription::manager;
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using shyft::time_axis::generic_dt;
    using shyft::energy_market::proxy_attr;
    using srv::dstm::stm_system_context;

    enum class run_type {
        incremental,
        full
    };

    /** @brief Visitor class to update stm_system or subscriptions
     * at certain points of execution of shop.
     */
    class shop_visitor {
        stm_system_context& ctx; /// <- the context holding the model that SHOP will be run on.
        string prefix; /// <- Prefix used to get attribute <--> url mapping correct.
        vector<string> subs; /// <- A container with what observers should be updated. This must be done by a call to notify_changes.
        run_type rt = run_type::full; // Defaults to full

        template<class PA>
        void add_subscription(std::string a_name,PA& pa) {
            subs.push_back(proxy_attr(ctx.mdl->run_params,a_name,pa).url(prefix));
        }
        // +------------------------------------------+
        // | Handling of different types of commands  |
        // +------------------------------------------+
        void set_code(const string& option) {
            if (option == "full")
                rt = run_type::full;
            else if (option == "incremental")
                rt = run_type::incremental;
            else if (option == "head") {
                ctx.mdl->run_params.head_opt = true;
                add_subscription("head_opt",ctx.mdl->run_params.head_opt);
            }
        }

        /** @brief increase the n_<run_type>_runs attribute of references stm_system
         * based on what run type is currently active.
         * Observable to notify change of is also updated.
         *
         * @param n: Number to increase by.
         */
        void increase_runs(int n) {
            switch (rt) {
                case run_type::full :
                    ctx.mdl->run_params.n_full_runs = ctx.mdl->run_params.n_full_runs /*.value_or(0)*/ + n;
                    add_subscription("n_full_runs",ctx.mdl->run_params.n_full_runs);
                    return;
                case run_type::incremental :
                    ctx.mdl->run_params.n_inc_runs = ctx.mdl->run_params.n_inc_runs /*.value_or(0)*/ + n;
                    add_subscription("n_inc_runs",ctx.mdl->run_params.n_inc_runs);
                    return;
            }
        }



    public:
        shop_visitor(stm_system_context& ctx, const string& prefix): ctx(ctx), prefix{prefix} {
            // We assume the visitor is created before running SHOP optimization.
            // We therefore initialize all attributes:
            ctx.mdl->run_params.n_inc_runs = 0;
            ctx.mdl->run_params.n_full_runs = 0;
            ctx.mdl->run_params.head_opt = false;
        }

        /** @brief Function to call when updating the run_time_axis of the model.
         * Adds url of attribute to vector of observables that need to be updated.
         *
         * @param ta: mdl.run_time_axis will be set to this value.
         */
        void update_run_time_axis(generic_dt const& ta) {
            ctx.mdl->run_params.run_time_axis = ta;
            add_subscription("run_time_axis",ctx.mdl->run_params.run_time_axis);
        }

        /** @brief Extend the context's message list with new messages.
         *
         * @returns: true if new messages were added, false if input messages was empty
         */
        bool add_shop_log(const vector<shop_log_entry>& entries) {
             if (entries.size())
             {
                 ctx.add_shop_log( entries);
                 // TODO: With new messages added, we should notify change on this log.
                 return true;
             }
             return false;
        }
        /** @brief Update referenced stm_system based on a command (that has been, or will be executed.
         */
        void update_by_command(shop_command const& cmd) {
            if (cmd.keyword == "set" && cmd.specifier == "code") {
                if (cmd.options.size() > 0) set_code(cmd.options[0]);
            } else if (cmd.keyword == "start" && cmd.specifier == "sim") {
                if (cmd.objects.size() > 0) increase_runs(std::stoi(cmd.objects[0]));
            }
        }

        void notify_changes(manager& sm) const {
            sm.notify_change(subs);
        }
    };
    using shop_visitor_ = std::shared_ptr<shop_visitor>;
}
