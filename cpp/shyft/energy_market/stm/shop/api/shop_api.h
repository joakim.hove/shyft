#pragma once
// auto genereated files based on active version of Sintef SHOP api dll
#include "shop_proxy.h"
namespace shop {

using proxy::obj;
using proxy::rw;
using proxy::ro;
using namespace proxy::unit;

template<class A>
struct reservoir:obj<A,0> {
    using super=obj<A,0>;
    reservoir()=default;
    reservoir(A* s,int oid):super(s, oid) {}
    reservoir(const reservoir& o):super(o) {}
    reservoir(reservoir&& o):super(std::move(o)) {}
    reservoir& operator=(const reservoir& o) {
        super::operator=(o);
        return *this;
    }
    reservoir& operator=(reservoir&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<reservoir,4,int,no_unit,no_unit> cut_time{this}; // x[no_unit],y[no_unit]
    rw<reservoir,5,double,mm3,mm3> start_vol{this}; // x[mm3],y[mm3]
    rw<reservoir,6,double,meter,meter> start_head{this}; // x[meter],y[meter]
    rw<reservoir,7,double,mm3,mm3> max_vol{this}; // x[mm3],y[mm3]
    rw<reservoir,8,double,meter,meter> lrl{this}; // x[meter],y[meter]
    rw<reservoir,9,double,meter,meter> hrl{this}; // x[meter],y[meter]
    ro<reservoir,10,double,nok,nok> endpoint_penalty{this}; // x[nok],y[nok]
    rw<reservoir,12,typename A::_xy,mm3,meter> vol_head{this}; // x[mm3],y[meter]
    rw<reservoir,13,typename A::_xy,meter,m3_per_s> flow_descr{this}; // x[meter],y[m3_per_s]
    rw<reservoir,14,typename A::_xy,mm3,nok_per_mm3> endpoint_desc_nok_mm3{this}; // x[mm3],y[nok_per_mm3]
    rw<reservoir,15,typename A::_xy,mm3,nok_per_mwh> endpoint_desc_nok_mwh{this}; // x[mm3],y[nok_per_mwh]
    rw<reservoir,16,typename A::_xy,mm3,nok_per_mm3> watervalue_desc_nok_mm3{this}; // x[mm3],y[nok_per_mm3]
    rw<reservoir,18,vector<typename A::_xy>,percent,nok_per_mm3> reservoir_cut_coeffs{this}; // x[percent],y[nok_per_mm3]
    rw<reservoir,19,vector<typename A::_xy>,percent,nok_per_mm3> inflow_cut_coeffs{this}; // x[percent],y[nok_per_mm3]
    rw<reservoir,20,typename A::_txy,no_unit,m3_per_s> inflow{this}; // x[no_unit],y[m3_per_s]
    rw<reservoir,21,typename A::_txy,no_unit,m3_per_s> sim_inflow{this}; // x[no_unit],y[m3_per_s]
    rw<reservoir,22,typename A::_txy,no_unit,no_unit> inflow_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,23,typename A::_txy,no_unit,mm3> min_vol_constr{this}; // x[no_unit],y[mm3]
    rw<reservoir,24,typename A::_txy,no_unit,no_unit> min_vol_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,25,typename A::_txy,no_unit,mm3> max_vol_constr{this}; // x[no_unit],y[mm3]
    rw<reservoir,26,typename A::_txy,no_unit,no_unit> max_vol_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,27,typename A::_txy,no_unit,nok_per_mm3h> tactical_cost_min{this}; // x[no_unit],y[nok_per_mm3h]
    rw<reservoir,28,typename A::_txy,no_unit,no_unit> tactical_cost_min_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,29,typename A::_txy,no_unit,nok_per_mm3h> tactical_cost_max{this}; // x[no_unit],y[nok_per_mm3h]
    rw<reservoir,30,typename A::_txy,no_unit,no_unit> tactical_cost_max_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,31,typename A::_txy,no_unit,mm3> tactical_limit_min{this}; // x[no_unit],y[mm3]
    rw<reservoir,32,typename A::_txy,no_unit,no_unit> tactical_limit_min_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,33,typename A::_txy,no_unit,mm3> tactical_limit_max{this}; // x[no_unit],y[mm3]
    rw<reservoir,34,typename A::_txy,no_unit,no_unit> tactical_limit_max_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,35,typename A::_txy,no_unit,no_unit> overflow_mip_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,36,typename A::_txy,no_unit,mm3> schedule{this}; // x[no_unit],y[mm3]
    rw<reservoir,37,typename A::_txy,no_unit,no_unit> schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,38,typename A::_txy,no_unit,mm3> volume_schedule{this}; // x[no_unit],y[mm3]
    rw<reservoir,39,typename A::_txy,no_unit,meter> level_schedule{this}; // x[no_unit],y[meter]
    rw<reservoir,40,typename A::_txy,no_unit,mm3> upper_slack{this}; // x[no_unit],y[mm3]
    rw<reservoir,41,typename A::_txy,no_unit,mm3> lower_slack{this}; // x[no_unit],y[mm3]
    rw<reservoir,42,typename A::_txy,no_unit,meter> elevation_adjustment{this}; // x[no_unit],y[meter]
    ro<reservoir,43,typename A::_txy,no_unit,mm3> storage{this}; // x[no_unit],y[mm3]
    ro<reservoir,44,typename A::_txy,no_unit,mm3> sim_storage{this}; // x[no_unit],y[mm3]
    ro<reservoir,45,typename A::_txy,no_unit,meter> head{this}; // x[no_unit],y[meter]
    ro<reservoir,46,typename A::_txy,no_unit,meter> sim_head{this}; // x[no_unit],y[meter]
    ro<reservoir,47,typename A::_txy,no_unit,nok_per_mm3> global_incr_cost_nok_mm3{this}; // x[no_unit],y[nok_per_mm3]
    ro<reservoir,48,typename A::_txy,no_unit,nok_per_mm3> local_incr_cost_nok_mm3{this}; // x[no_unit],y[nok_per_mm3]
    ro<reservoir,49,typename A::_txy,no_unit,nok_per_mwh> local_incr_cost_nok_mwh{this}; // x[no_unit],y[nok_per_mwh]
    ro<reservoir,50,typename A::_txy,no_unit,mm3> penalty{this}; // x[no_unit],y[mm3]
    ro<reservoir,51,typename A::_txy,no_unit,nok> tactical_penalty_up{this}; // x[no_unit],y[nok]
    ro<reservoir,52,typename A::_txy,no_unit,nok> tactical_penalty_down{this}; // x[no_unit],y[nok]
    ro<reservoir,53,vector<typename A::_xy>,percent,nok_per_mm3> cut_output_coeffs_mm3{this}; // x[percent],y[nok_per_mm3]
    rw<reservoir,54,vector<typename A::_xy>,percent,nok_per_mm3> cut_input_coeffs_mm3{this}; // x[percent],y[nok_per_mm3]
    ro<reservoir,55,typename A::_txy,no_unit,nok> end_penalty{this}; // x[no_unit],y[nok]
    ro<reservoir,56,typename A::_txy,no_unit,nok> penalty_nok{this}; // x[no_unit],y[nok]
    ro<reservoir,57,typename A::_txy,no_unit,nok> vow_in_transit{this}; // x[no_unit],y[nok]
    ro<reservoir,58,typename A::_txy,no_unit,nok> tactical_penalty{this}; // x[no_unit],y[nok]
    ro<reservoir,59,typename A::_txy,no_unit,nok> end_value{this}; // x[no_unit],y[nok]
    ro<reservoir,60,typename A::_txy,no_unit,nok> change_in_end_value{this}; // x[no_unit],y[nok]
    rw<reservoir,61,double,no_unit,no_unit> latitude{this}; // x[no_unit],y[no_unit]
    rw<reservoir,62,double,no_unit,no_unit> longitude{this}; // x[no_unit],y[no_unit]
    ro<reservoir,63,int,no_unit,no_unit> added_to_network{this}; // x[no_unit],y[no_unit]
    ro<reservoir,64,int,no_unit,no_unit> network_no{this}; // x[no_unit],y[no_unit]
    rw<reservoir,65,int,no_unit,no_unit> head_opt{this}; // x[no_unit],y[no_unit]
    rw<reservoir,66,typename A::_txy,no_unit,nok_per_mm3> overflow_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<reservoir,67,typename A::_txy,no_unit,nok_per_mm3> overflow_cost_flag{this}; // x[no_unit],y[nok_per_mm3]
};
template<class A>
struct power_plant:obj<A,1> {
    using super=obj<A,1>;
    power_plant()=default;
    power_plant(A* s,int oid):super(s, oid) {}
    power_plant(const power_plant& o):super(o) {}
    power_plant(power_plant&& o):super(std::move(o)) {}
    power_plant& operator=(const power_plant& o) {
        super::operator=(o);
        return *this;
    }
    power_plant& operator=(power_plant&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<power_plant,71,int,no_unit,no_unit> time_delay{this}; // x[no_unit],y[no_unit]
    ro<power_plant,73,int,no_unit,no_unit> num_gen{this}; // x[no_unit],y[no_unit]
    ro<power_plant,74,int,no_unit,no_unit> num_pump{this}; // x[no_unit],y[no_unit]
    rw<power_plant,78,double,no_unit,meter> outlet_line{this}; // x[no_unit],y[meter]
    ro<power_plant,79,double,no_unit,kwh_per_mm3> prod_factor{this}; // x[no_unit],y[kwh_per_mm3]
    rw<power_plant,82,vector<double>,s2_per_m5,s2_per_m5> main_loss{this}; // x[s2_per_m5],y[s2_per_m5]
    rw<power_plant,83,vector<double>,s2_per_m5,s2_per_m5> penstock_loss{this}; // x[s2_per_m5],y[s2_per_m5]
    //--TODO: ro<power_plant,84,xyt,mw,m3_per_s> best_profit_q{this}; // x[mw],y[m3_per_s]
    //--TODO: ro<power_plant,85,xyt,mw,nok_per_mw> best_profit_mc{this}; // x[mw],y[nok_per_mw]
    //--TODO: ro<power_plant,86,xyt,mw,nok_per_mw> best_profit_ac{this}; // x[mw],y[nok_per_mw]
    //--TODO: ro<power_plant,87,xyt,mw,nok> best_profit_commitment_cost{this}; // x[mw],y[nok]
    rw<power_plant,88,vector<typename A::_xy>,m3_per_s,meter> tailrace_loss{this}; // x[m3_per_s],y[meter]
    rw<power_plant,89,vector<typename A::_xy>,m3_per_s,meter> intake_loss{this}; // x[m3_per_s],y[meter]
    rw<power_plant,90,typename A::_xy,hour,no_unit> shape_discharge{this}; // x[hour],y[no_unit]
    rw<power_plant,91,typename A::_xy,m3_per_s,nok_per_h_per_m3_per_s> discharge_cost_curve{this}; // x[m3_per_s],y[nok_per_h_per_m3_per_s]
    rw<power_plant,92,typename A::_txy,no_unit,no_unit> linear_startup_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,93,typename A::_txy,no_unit,no_unit> mip_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,94,typename A::_txy,no_unit,no_unit> tailrace_loss_from_bypass_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,95,typename A::_txy,no_unit,no_unit> intake_loss_from_bypass_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,96,typename A::_xy,meter,m3_per_s> max_q_limit_rsv_up{this}; // x[meter],y[m3_per_s]
    rw<power_plant,97,typename A::_xy,meter,m3_per_s> max_q_limit_rsv_down{this}; // x[meter],y[m3_per_s]
    rw<power_plant,98,typename A::_txy,no_unit,mw> min_p_constr{this}; // x[no_unit],y[mw]
    rw<power_plant,99,typename A::_txy,no_unit,mw> max_p_constr{this}; // x[no_unit],y[mw]
    rw<power_plant,100,typename A::_txy,no_unit,m3_per_s> min_q_constr{this}; // x[no_unit],y[m3_per_s]
    rw<power_plant,101,typename A::_txy,no_unit,m3_per_s> max_q_constr{this}; // x[no_unit],y[m3_per_s]
    rw<power_plant,102,typename A::_txy,no_unit,no_unit> min_p_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,103,typename A::_txy,no_unit,no_unit> max_p_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,104,typename A::_txy,no_unit,no_unit> min_q_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,105,typename A::_txy,no_unit,no_unit> max_q_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,106,typename A::_txy,no_unit,meter> intake_line{this}; // x[no_unit],y[meter]
    rw<power_plant,107,typename A::_txy,no_unit,mw> frr_up_min{this}; // x[no_unit],y[mw]
    rw<power_plant,108,typename A::_txy,no_unit,mw> frr_up_max{this}; // x[no_unit],y[mw]
    rw<power_plant,109,typename A::_txy,no_unit,mw> frr_down_min{this}; // x[no_unit],y[mw]
    rw<power_plant,110,typename A::_txy,no_unit,mw> frr_down_max{this}; // x[no_unit],y[mw]
    rw<power_plant,111,typename A::_txy,no_unit,mw> rr_up_min{this}; // x[no_unit],y[mw]
    rw<power_plant,112,typename A::_txy,no_unit,no_unit> frr_symmetric_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,113,typename A::_txy,no_unit,no_unit> distribute_production{this}; // x[no_unit],y[no_unit]
    rw<power_plant,114,typename A::_txy,no_unit,no_unit> discharge_group{this}; // x[no_unit],y[no_unit]
    rw<power_plant,115,typename A::_txy,no_unit,no_unit> bid_group{this}; // x[no_unit],y[no_unit]
    rw<power_plant,116,typename A::_txy,no_unit,no_unit> prod_area{this}; // x[no_unit],y[no_unit]
    rw<power_plant,117,typename A::_txy,no_unit,no_unit> prod_area_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,118,typename A::_txy,no_unit,delta_meter> tides{this}; // x[no_unit],y[delta_meter]
    rw<power_plant,119,typename A::_txy,no_unit,mw> block_merge_tolerance{this}; // x[no_unit],y[mw]
    rw<power_plant,120,typename A::_txy,no_unit,hour> block_generation_mwh{this}; // x[no_unit],y[hour]
    rw<power_plant,121,typename A::_txy,no_unit,hour> block_generation_m3s{this}; // x[no_unit],y[hour]
    rw<power_plant,122,typename A::_txy,no_unit,mw> production_schedule{this}; // x[no_unit],y[mw]
    rw<power_plant,123,typename A::_txy,no_unit,m3_per_s> discharge_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<power_plant,124,typename A::_txy,no_unit,mw> consumption_schedule{this}; // x[no_unit],y[mw]
    rw<power_plant,125,typename A::_txy,no_unit,m3_per_s> upflow_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<power_plant,126,typename A::_txy,no_unit,nok_per_mwh> feeding_fee{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_plant,127,typename A::_txy,no_unit,nok_per_mwh> production_fee{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_plant,128,typename A::_txy,no_unit,nok_per_mwh> consumption_fee{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_plant,129,typename A::_txy,no_unit,nok_per_mm3> discharge_fee{this}; // x[no_unit],y[nok_per_mm3]
    rw<power_plant,130,typename A::_txy,no_unit,no_unit> production_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,131,typename A::_txy,no_unit,no_unit> discharge_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,132,typename A::_txy,no_unit,no_unit> consumption_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,133,typename A::_txy,no_unit,no_unit> upflow_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,134,typename A::_txy,no_unit,no_unit> feeding_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,135,typename A::_txy,no_unit,no_unit> production_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,136,typename A::_txy,no_unit,no_unit> consumption_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,137,typename A::_txy,no_unit,no_unit> discharge_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,139,typename A::_txy,no_unit,no_unit> mc_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,140,typename A::_txy,no_unit,no_unit> mip_length{this}; // x[no_unit],y[no_unit]
    rw<power_plant,141,typename A::_txy,no_unit,no_unit> mip_length_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,142,typename A::_txy,no_unit,no_unit> min_p_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,143,typename A::_txy,no_unit,no_unit> max_p_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,144,typename A::_txy,no_unit,no_unit> min_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,145,typename A::_txy,no_unit,no_unit> max_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    ro<power_plant,146,typename A::_txy,no_unit,nok> min_p_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant,147,typename A::_txy,no_unit,nok> max_p_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant,148,typename A::_txy,no_unit,nok> min_q_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant,149,typename A::_txy,no_unit,nok> max_q_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant,150,typename A::_txy,no_unit,mw> production{this}; // x[no_unit],y[mw]
    ro<power_plant,151,typename A::_txy,no_unit,mw> sim_production{this}; // x[no_unit],y[mw]
    ro<power_plant,152,typename A::_txy,no_unit,mw> consumption{this}; // x[no_unit],y[mw]
    ro<power_plant,153,typename A::_txy,no_unit,m3_per_s> discharge{this}; // x[no_unit],y[m3_per_s]
    ro<power_plant,154,typename A::_txy,no_unit,m3_per_s> sim_discharge{this}; // x[no_unit],y[m3_per_s]
    ro<power_plant,155,typename A::_txy,no_unit,meter> eff_head{this}; // x[no_unit],y[meter]
    ro<power_plant,156,typename A::_txy,no_unit,meter> head_loss{this}; // x[no_unit],y[meter]
    ro<power_plant,157,typename A::_txy,no_unit,mw> prod_unbalance{this}; // x[no_unit],y[mw]
    ro<power_plant,158,typename A::_txy,no_unit,mw> cons_unbalance{this}; // x[no_unit],y[mw]
    ro<power_plant,159,typename A::_txy,no_unit,nok> schedule_up_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant,160,typename A::_txy,no_unit,nok> schedule_down_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant,162,typename A::_txy,no_unit,nok> p_constr_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant,163,typename A::_txy,no_unit,nok> q_constr_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant,164,typename A::_txy,no_unit,nok> schedule_penalty{this}; // x[no_unit],y[nok]
    rw<power_plant,165,double,no_unit,no_unit> latitude{this}; // x[no_unit],y[no_unit]
    rw<power_plant,166,double,no_unit,no_unit> longitude{this}; // x[no_unit],y[no_unit]
    rw<power_plant,167,typename A::_txy,no_unit,mw_hour> power_ramping_up{this}; // x[no_unit],y[mw_hour]
    rw<power_plant,168,typename A::_txy,no_unit,mw_hour> power_ramping_down{this}; // x[no_unit],y[mw_hour]
    rw<power_plant,169,typename A::_txy,no_unit,m3sec_hour> discharge_ramping_up{this}; // x[no_unit],y[m3sec_hour]
    rw<power_plant,170,typename A::_txy,no_unit,m3sec_hour> discharge_ramping_down{this}; // x[no_unit],y[m3sec_hour]
    rw<power_plant,171,typename A::_txy,no_unit,no_unit> bp_dyn_wv_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,172,typename A::_txy,no_unit,no_unit> production_group{this}; // x[no_unit],y[no_unit]
    ro<power_plant,173,typename A::_txy,no_unit,mw> ref_prod{this}; // x[no_unit],y[mw]
    rw<power_plant,176,int,no_unit,no_unit> equal_distribution{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<power_plant,177,int_array,no_unit,no_unit> gen_priority{this}; // x[no_unit],y[no_unit]
    rw<power_plant,178,double,no_unit,no_unit> less_distribution_eps{this}; // x[no_unit],y[no_unit]
    rw<power_plant,179,double,no_unit,no_unit> ownership{this}; // x[no_unit],y[no_unit]
    rw<power_plant,180,double,no_unit,nok> sched_penalty_cost_down{this}; // x[no_unit],y[nok]
    rw<power_plant,181,double,no_unit,nok> sched_penalty_cost_up{this}; // x[no_unit],y[nok]
    rw<power_plant,182,typename A::_txy,no_unit,no_unit> n_marg_points{this}; // x[no_unit],y[no_unit]
    rw<power_plant,183,typename A::_txy,no_unit,no_unit> n_seg_down{this}; // x[no_unit],y[no_unit]
    rw<power_plant,184,typename A::_txy,no_unit,no_unit> n_seg_up{this}; // x[no_unit],y[no_unit]
    rw<power_plant,185,typename A::_txy,no_unit,no_unit> n_mip_seg_down{this}; // x[no_unit],y[no_unit]
    rw<power_plant,186,typename A::_txy,no_unit,no_unit> n_mip_seg_up{this}; // x[no_unit],y[no_unit]
    rw<power_plant,187,typename A::_txy,no_unit,no_unit> dyn_pq_seg_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,188,typename A::_txy,no_unit,nok_per_mwh> min_p_penalty_cost{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_plant,189,typename A::_txy,no_unit,nok_per_mwh> max_p_penalty_cost{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_plant,190,typename A::_txy,no_unit,nok_per_mm3> min_q_penalty_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<power_plant,191,typename A::_txy,no_unit,nok_per_mm3> max_q_penalty_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<power_plant,192,typename A::_txy,no_unit,no_unit> min_p_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,193,typename A::_txy,no_unit,no_unit> max_p_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,194,typename A::_txy,no_unit,no_unit> min_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,195,typename A::_txy,no_unit,no_unit> max_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct unit:obj<A,2> {
    using super=obj<A,2>;
    unit()=default;
    unit(A* s,int oid):super(s, oid) {}
    unit(const unit& o):super(o) {}
    unit(unit&& o):super(std::move(o)) {}
    unit& operator=(const unit& o) {
        super::operator=(o);
        return *this;
    }
    unit& operator=(unit&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<unit,204,int,no_unit,no_unit> type{this}; // x[no_unit],y[no_unit]
    ro<unit,205,int,no_unit,no_unit> num_needle_comb{this}; // x[no_unit],y[no_unit]
    rw<unit,208,int,no_unit,no_unit> initial_state{this}; // x[no_unit],y[no_unit]
    rw<unit,209,int,no_unit,no_unit> penstock{this}; // x[no_unit],y[no_unit]
    rw<unit,210,double,no_unit,mw> p_min{this}; // x[no_unit],y[mw]
    rw<unit,211,double,no_unit,mw> p_max{this}; // x[no_unit],y[mw]
    rw<unit,212,double,no_unit,mw> p_nom{this}; // x[no_unit],y[mw]
    //--TODO: ro<unit,213,xyt,mw,nok_per_mw> best_profit_q{this}; // x[mw],y[nok_per_mw]
    //--TODO: ro<unit,214,xyt,mw,nok_per_mw> best_profit_p{this}; // x[mw],y[nok_per_mw]
    //--TODO: ro<unit,215,xyt,mw,m3_per_s> best_profit_dq_dp{this}; // x[mw],y[m3_per_s]
    //--TODO: ro<unit,216,xyt,mw,m3_per_s> best_profit_needle_comb{this}; // x[mw],y[m3_per_s]
    rw<unit,217,typename A::_xy,mw,percent> gen_eff_curve{this}; // x[mw],y[percent]
    rw<unit,218,vector<typename A::_xy>,m3_per_s,percent> turb_eff_curves{this}; // x[m3_per_s],y[percent]
    rw<unit,219,typename A::_xy,meter,m3_per_s> max_q_limit_rsv_down{this}; // x[meter],y[m3_per_s]
    rw<unit,220,vector<typename A::_xy>,m3_per_s,nok_per_h_per_m3_per_s> discharge_cost_curve{this}; // x[m3_per_s],y[nok_per_h_per_m3_per_s]
    rw<unit,221,typename A::_txy,no_unit,no_unit> maintenance_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,222,typename A::_txy,no_unit,mw> production_schedule{this}; // x[no_unit],y[mw]
    rw<unit,223,typename A::_txy,no_unit,no_unit> production_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,224,typename A::_txy,no_unit,m3_per_s> discharge_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<unit,225,typename A::_txy,no_unit,no_unit> discharge_schedule_flag{this}; // x[no_unit],y[no_unit]
    ro<unit,226,typename A::_txy,no_unit,mw> production_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,227,typename A::_txy,no_unit,mm3> discharge_schedule_penalty{this}; // x[no_unit],y[mm3]
    rw<unit,228,typename A::_txy,no_unit,no_unit> schedule_deviation_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,229,typename A::_txy,no_unit,no_unit> committed_in{this}; // x[no_unit],y[no_unit]
    rw<unit,230,typename A::_txy,no_unit,no_unit> committed_flag{this}; // x[no_unit],y[no_unit]
    ro<unit,231,typename A::_txy,no_unit,no_unit> committed_out{this}; // x[no_unit],y[no_unit]
    rw<unit,232,typename A::_txy,no_unit,mw> ref_production{this}; // x[no_unit],y[mw]
    rw<unit,233,typename A::_txy,no_unit,mw> min_p_constr{this}; // x[no_unit],y[mw]
    rw<unit,234,typename A::_txy,no_unit,mw> max_p_constr{this}; // x[no_unit],y[mw]
    rw<unit,235,typename A::_txy,no_unit,m3_per_s> min_q_constr{this}; // x[no_unit],y[m3_per_s]
    rw<unit,236,typename A::_txy,no_unit,m3_per_s> max_q_constr{this}; // x[no_unit],y[m3_per_s]
    rw<unit,237,typename A::_txy,no_unit,no_unit> min_p_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,238,typename A::_txy,no_unit,no_unit> max_p_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,239,typename A::_txy,no_unit,no_unit> min_q_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,240,typename A::_txy,no_unit,no_unit> max_q_constr_flag{this}; // x[no_unit],y[no_unit]
    ro<unit,241,typename A::_txy,no_unit,mw> production{this}; // x[no_unit],y[mw]
    ro<unit,242,typename A::_txy,no_unit,mw> solver_production{this}; // x[no_unit],y[mw]
    ro<unit,243,typename A::_txy,no_unit,mw> sim_production{this}; // x[no_unit],y[mw]
    ro<unit,244,typename A::_txy,no_unit,m3_per_s> discharge{this}; // x[no_unit],y[m3_per_s]
    ro<unit,245,typename A::_txy,no_unit,m3_per_s> sim_discharge{this}; // x[no_unit],y[m3_per_s]
    rw<unit,246,typename A::_txy,no_unit,nok> startcost{this}; // x[no_unit],y[nok]
    rw<unit,247,typename A::_txy,no_unit,nok> stopcost{this}; // x[no_unit],y[nok]
    rw<unit,248,typename A::_txy,no_unit,nok> reserve_ramping_cost_up{this}; // x[no_unit],y[nok]
    rw<unit,249,typename A::_txy,no_unit,nok> reserve_ramping_cost_down{this}; // x[no_unit],y[nok]
    rw<unit,250,typename A::_txy,no_unit,meter> upstream_min{this}; // x[no_unit],y[meter]
    rw<unit,251,typename A::_txy,no_unit,meter> downstream_max{this}; // x[no_unit],y[meter]
    rw<unit,252,typename A::_txy,no_unit,no_unit> commit_group{this}; // x[no_unit],y[no_unit]
    rw<unit,254,typename A::_txy,no_unit,no_unit> prod_area{this}; // x[no_unit],y[no_unit]
    rw<unit,256,typename A::_txy,no_unit,no_unit> priority{this}; // x[no_unit],y[no_unit]
    rw<unit,257,typename A::_txy,no_unit,no_unit> mc_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,258,typename A::_txy,no_unit,no_unit> fcr_mip_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,260,typename A::_txy,no_unit,mw> p_fcr_min{this}; // x[no_unit],y[mw]
    rw<unit,261,typename A::_txy,no_unit,mw> p_fcr_max{this}; // x[no_unit],y[mw]
    rw<unit,262,typename A::_txy,no_unit,mw> p_rr_min{this}; // x[no_unit],y[mw]
    rw<unit,263,typename A::_txy,no_unit,mw> frr_up_min{this}; // x[no_unit],y[mw]
    rw<unit,264,typename A::_txy,no_unit,mw> frr_up_max{this}; // x[no_unit],y[mw]
    rw<unit,265,typename A::_txy,no_unit,mw> frr_down_min{this}; // x[no_unit],y[mw]
    rw<unit,266,typename A::_txy,no_unit,mw> frr_down_max{this}; // x[no_unit],y[mw]
    rw<unit,267,typename A::_txy,no_unit,mw> fcr_n_up_min{this}; // x[no_unit],y[mw]
    rw<unit,268,typename A::_txy,no_unit,mw> fcr_n_up_max{this}; // x[no_unit],y[mw]
    rw<unit,269,typename A::_txy,no_unit,mw> fcr_n_down_min{this}; // x[no_unit],y[mw]
    rw<unit,270,typename A::_txy,no_unit,mw> fcr_n_down_max{this}; // x[no_unit],y[mw]
    rw<unit,271,typename A::_txy,no_unit,mw> fcr_d_up_min{this}; // x[no_unit],y[mw]
    rw<unit,272,typename A::_txy,no_unit,mw> fcr_d_up_max{this}; // x[no_unit],y[mw]
    rw<unit,273,typename A::_txy,no_unit,mw> rr_up_min{this}; // x[no_unit],y[mw]
    rw<unit,274,typename A::_txy,no_unit,mw> rr_up_max{this}; // x[no_unit],y[mw]
    rw<unit,275,typename A::_txy,no_unit,mw> rr_down_min{this}; // x[no_unit],y[mw]
    rw<unit,276,typename A::_txy,no_unit,mw> rr_down_max{this}; // x[no_unit],y[mw]
    rw<unit,277,typename A::_txy,no_unit,mw> fcr_n_up_schedule{this}; // x[no_unit],y[mw]
    rw<unit,278,typename A::_txy,no_unit,mw> fcr_n_down_schedule{this}; // x[no_unit],y[mw]
    rw<unit,279,typename A::_txy,no_unit,mw> fcr_d_up_schedule{this}; // x[no_unit],y[mw]
    rw<unit,280,typename A::_txy,no_unit,mw> frr_up_schedule{this}; // x[no_unit],y[mw]
    rw<unit,281,typename A::_txy,no_unit,mw> frr_down_schedule{this}; // x[no_unit],y[mw]
    rw<unit,282,typename A::_txy,no_unit,mw> rr_up_schedule{this}; // x[no_unit],y[mw]
    rw<unit,283,typename A::_txy,no_unit,mw> rr_down_schedule{this}; // x[no_unit],y[mw]
    rw<unit,284,typename A::_txy,no_unit,no_unit> fcr_n_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,285,typename A::_txy,no_unit,no_unit> fcr_n_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,286,typename A::_txy,no_unit,no_unit> fcr_d_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,287,typename A::_txy,no_unit,no_unit> frr_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,288,typename A::_txy,no_unit,no_unit> frr_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,289,typename A::_txy,no_unit,no_unit> rr_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,290,typename A::_txy,no_unit,no_unit> rr_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,291,typename A::_txy,no_unit,no_unit> fcr_n_up_group{this}; // x[no_unit],y[no_unit]
    rw<unit,292,typename A::_txy,no_unit,no_unit> fcr_n_down_group{this}; // x[no_unit],y[no_unit]
    rw<unit,293,typename A::_txy,no_unit,no_unit> fcr_d_up_group{this}; // x[no_unit],y[no_unit]
    rw<unit,294,typename A::_txy,no_unit,no_unit> frr_up_group{this}; // x[no_unit],y[no_unit]
    rw<unit,295,typename A::_txy,no_unit,no_unit> frr_down_group{this}; // x[no_unit],y[no_unit]
    rw<unit,296,typename A::_txy,no_unit,no_unit> rr_up_group{this}; // x[no_unit],y[no_unit]
    rw<unit,297,typename A::_txy,no_unit,no_unit> rr_down_group{this}; // x[no_unit],y[no_unit]
    ro<unit,298,typename A::_txy,no_unit,mw> fcr_n_up_delivery{this}; // x[no_unit],y[mw]
    ro<unit,299,typename A::_txy,no_unit,mw> fcr_n_down_delivery{this}; // x[no_unit],y[mw]
    ro<unit,300,typename A::_txy,no_unit,mw> fcr_d_up_delivery{this}; // x[no_unit],y[mw]
    ro<unit,301,typename A::_txy,no_unit,mw> frr_up_delivery{this}; // x[no_unit],y[mw]
    ro<unit,302,typename A::_txy,no_unit,mw> frr_down_delivery{this}; // x[no_unit],y[mw]
    ro<unit,303,typename A::_txy,no_unit,mw> rr_up_delivery{this}; // x[no_unit],y[mw]
    ro<unit,304,typename A::_txy,no_unit,mw> rr_down_delivery{this}; // x[no_unit],y[mw]
    ro<unit,305,typename A::_txy,no_unit,mw> fcr_n_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,306,typename A::_txy,no_unit,mw> fcr_n_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,307,typename A::_txy,no_unit,mw> fcr_d_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,308,typename A::_txy,no_unit,mw> frr_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,309,typename A::_txy,no_unit,mw> frr_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,310,typename A::_txy,no_unit,mw> rr_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,311,typename A::_txy,no_unit,mw> rr_down_schedule_penalty{this}; // x[no_unit],y[mw]
    rw<unit,312,typename A::_txy,no_unit,nok> droop_cost{this}; // x[no_unit],y[nok]
    rw<unit,313,typename A::_txy,no_unit,no_unit> fixed_droop{this}; // x[no_unit],y[no_unit]
    rw<unit,314,typename A::_txy,no_unit,no_unit> fixed_droop_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,315,typename A::_txy,no_unit,no_unit> droop_min{this}; // x[no_unit],y[no_unit]
    rw<unit,316,typename A::_txy,no_unit,no_unit> droop_max{this}; // x[no_unit],y[no_unit]
    ro<unit,317,typename A::_txy,no_unit,no_unit> droop_result{this}; // x[no_unit],y[no_unit]
    ro<unit,326,typename A::_txy,no_unit,nok> startup_cost_mip_objective{this}; // x[no_unit],y[nok]
    ro<unit,327,typename A::_txy,no_unit,nok> startup_cost_total_objective{this}; // x[no_unit],y[nok]
    ro<unit,328,typename A::_txy,no_unit,nok> schedule_penalty{this}; // x[no_unit],y[nok]
    ro<unit,329,typename A::_txy,no_unit,nok> discharge_fee_objective{this}; // x[no_unit],y[nok]
    ro<unit,330,typename A::_txy,no_unit,nok> feeding_fee_objective{this}; // x[no_unit],y[nok]
    ro<unit,331,typename A::_txy,no_unit,nok> market_income{this}; // x[no_unit],y[nok]
    ro<unit,332,typename A::_txy,no_unit,meter> sim_eff_head{this}; // x[no_unit],y[meter]
    rw<unit,333,int,no_unit,no_unit> affinity_eq_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,339,typename A::_txy,no_unit,no_unit> dyn_pq_seg_flag{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<unit,345,xyt,m3_per_s,mw> original_pq_curves{this}; // x[m3_per_s],y[mw]
    //--TODO: ro<unit,346,xyt,m3_per_s,mw> convex_pq_curves{this}; // x[m3_per_s],y[mw]
    //--TODO: ro<unit,347,xyt,m3_per_s,mw> final_pq_curves{this}; // x[m3_per_s],y[mw]
};
template<class A>
struct needle_combination:obj<A,3> {
    using super=obj<A,3>;
    needle_combination()=default;
    needle_combination(A* s,int oid):super(s, oid) {}
    needle_combination(const needle_combination& o):super(o) {}
    needle_combination(needle_combination&& o):super(std::move(o)) {}
    needle_combination& operator=(const needle_combination& o) {
        super::operator=(o);
        return *this;
    }
    needle_combination& operator=(needle_combination&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<needle_combination,351,double,no_unit,mw> p_max{this}; // x[no_unit],y[mw]
    rw<needle_combination,352,double,no_unit,mw> p_min{this}; // x[no_unit],y[mw]
    rw<needle_combination,353,double,no_unit,mw> p_nom{this}; // x[no_unit],y[mw]
    rw<needle_combination,354,vector<typename A::_xy>,m3_per_s,percent> turb_eff_curves{this}; // x[m3_per_s],y[percent]
    //--TODO: ro<needle_combination,355,xyt,m3_per_s,mw> original_pq_curves{this}; // x[m3_per_s],y[mw]
    //--TODO: ro<needle_combination,356,xyt,m3_per_s,mw> convex_pq_curves{this}; // x[m3_per_s],y[mw]
    //--TODO: ro<needle_combination,357,xyt,m3_per_s,mw> final_pq_curves{this}; // x[m3_per_s],y[mw]
};
template<class A>
struct pump:obj<A,4> {
    using super=obj<A,4>;
    pump()=default;
    pump(A* s,int oid):super(s, oid) {}
    pump(const pump& o):super(o) {}
    pump(pump&& o):super(std::move(o)) {}
    pump& operator=(const pump& o) {
        super::operator=(o);
        return *this;
    }
    pump& operator=(pump&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<pump,363,int,no_unit,no_unit> penstock{this}; // x[no_unit],y[no_unit]
    rw<pump,364,int,no_unit,no_unit> initial_state{this}; // x[no_unit],y[no_unit]
    rw<pump,366,double,no_unit,mw> p_max{this}; // x[no_unit],y[mw]
    rw<pump,367,double,no_unit,mw> p_min{this}; // x[no_unit],y[mw]
    rw<pump,368,double,no_unit,mw> p_nom{this}; // x[no_unit],y[mw]
    rw<pump,369,typename A::_xy,mw,percent> gen_eff_curve{this}; // x[mw],y[percent]
    rw<pump,370,vector<typename A::_xy>,m3_per_s,percent> turb_eff_curves{this}; // x[m3_per_s],y[percent]
    rw<pump,371,typename A::_txy,no_unit,no_unit> committed_in{this}; // x[no_unit],y[no_unit]
    rw<pump,372,typename A::_txy,no_unit,no_unit> committed_flag{this}; // x[no_unit],y[no_unit]
    ro<pump,373,typename A::_txy,no_unit,no_unit> committed_out{this}; // x[no_unit],y[no_unit]
    ro<pump,374,typename A::_txy,no_unit,mw> consumption{this}; // x[no_unit],y[mw]
    ro<pump,375,typename A::_txy,no_unit,m3_per_s> upflow{this}; // x[no_unit],y[m3_per_s]
    rw<pump,376,typename A::_txy,no_unit,no_unit> maintenance_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,377,typename A::_txy,no_unit,mw> consumption_schedule{this}; // x[no_unit],y[mw]
    rw<pump,378,typename A::_txy,no_unit,no_unit> consumption_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,379,typename A::_txy,no_unit,m3_per_s> upflow_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<pump,380,typename A::_txy,no_unit,no_unit> upflow_schedule_flag{this}; // x[no_unit],y[no_unit]
    ro<pump,381,typename A::_txy,no_unit,mw> consumption_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,382,typename A::_txy,no_unit,mm3> upflow_schedule_penalty{this}; // x[no_unit],y[mm3]
    rw<pump,383,typename A::_txy,no_unit,meter> upstream_max{this}; // x[no_unit],y[meter]
    rw<pump,384,typename A::_txy,no_unit,no_unit> upstream_max_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,385,typename A::_txy,no_unit,meter> downstream_min{this}; // x[no_unit],y[meter]
    rw<pump,386,typename A::_txy,no_unit,meter> downstream_min_flag{this}; // x[no_unit],y[meter]
    rw<pump,387,typename A::_txy,no_unit,nok> startcost{this}; // x[no_unit],y[nok]
    rw<pump,388,typename A::_txy,no_unit,nok> stopcost{this}; // x[no_unit],y[nok]
    rw<pump,389,typename A::_txy,no_unit,no_unit> commit_group{this}; // x[no_unit],y[no_unit]
    rw<pump,390,typename A::_txy,no_unit,nok> droop_cost{this}; // x[no_unit],y[nok]
    rw<pump,391,typename A::_txy,no_unit,no_unit> fixed_droop{this}; // x[no_unit],y[no_unit]
    rw<pump,392,typename A::_txy,no_unit,no_unit> fixed_droop_flag{this}; // x[no_unit],y[no_unit]
    ro<pump,393,typename A::_txy,no_unit,no_unit> droop_result{this}; // x[no_unit],y[no_unit]
    rw<pump,394,typename A::_txy,no_unit,no_unit> droop_min{this}; // x[no_unit],y[no_unit]
    rw<pump,395,typename A::_txy,no_unit,no_unit> droop_max{this}; // x[no_unit],y[no_unit]
    rw<pump,396,typename A::_txy,no_unit,mw> p_fcr_min{this}; // x[no_unit],y[mw]
    rw<pump,397,typename A::_txy,no_unit,mw> p_rr_min{this}; // x[no_unit],y[mw]
    rw<pump,398,typename A::_txy,no_unit,mw> p_fcr_max{this}; // x[no_unit],y[mw]
    rw<pump,399,typename A::_txy,no_unit,no_unit> fcr_mip_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,400,typename A::_txy,no_unit,mw> fcr_n_up_schedule{this}; // x[no_unit],y[mw]
    rw<pump,401,typename A::_txy,no_unit,mw> fcr_n_down_schedule{this}; // x[no_unit],y[mw]
    rw<pump,402,typename A::_txy,no_unit,mw> fcr_d_up_schedule{this}; // x[no_unit],y[mw]
    rw<pump,403,typename A::_txy,no_unit,mw> frr_up_schedule{this}; // x[no_unit],y[mw]
    rw<pump,404,typename A::_txy,no_unit,mw> frr_down_schedule{this}; // x[no_unit],y[mw]
    rw<pump,405,typename A::_txy,no_unit,mw> rr_up_schedule{this}; // x[no_unit],y[mw]
    rw<pump,406,typename A::_txy,no_unit,mw> rr_down_schedule{this}; // x[no_unit],y[mw]
    rw<pump,407,typename A::_txy,no_unit,no_unit> fcr_n_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,408,typename A::_txy,no_unit,no_unit> fcr_n_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,409,typename A::_txy,no_unit,no_unit> fcr_d_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,410,typename A::_txy,no_unit,no_unit> frr_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,411,typename A::_txy,no_unit,no_unit> frr_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,412,typename A::_txy,no_unit,no_unit> rr_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,413,typename A::_txy,no_unit,no_unit> rr_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,414,typename A::_txy,no_unit,no_unit> fcr_n_up_group{this}; // x[no_unit],y[no_unit]
    rw<pump,415,typename A::_txy,no_unit,no_unit> fcr_n_down_group{this}; // x[no_unit],y[no_unit]
    rw<pump,416,typename A::_txy,no_unit,no_unit> fcr_d_up_group{this}; // x[no_unit],y[no_unit]
    rw<pump,417,typename A::_txy,no_unit,no_unit> frr_up_group{this}; // x[no_unit],y[no_unit]
    rw<pump,418,typename A::_txy,no_unit,no_unit> frr_down_group{this}; // x[no_unit],y[no_unit]
    rw<pump,419,typename A::_txy,no_unit,no_unit> rr_up_group{this}; // x[no_unit],y[no_unit]
    rw<pump,420,typename A::_txy,no_unit,no_unit> rr_down_group{this}; // x[no_unit],y[no_unit]
    ro<pump,421,typename A::_txy,no_unit,mw> fcr_n_up_delivery{this}; // x[no_unit],y[mw]
    ro<pump,422,typename A::_txy,no_unit,mw> fcr_n_down_delivery{this}; // x[no_unit],y[mw]
    ro<pump,423,typename A::_txy,no_unit,mw> fcr_d_up_delivery{this}; // x[no_unit],y[mw]
    ro<pump,424,typename A::_txy,no_unit,mw> frr_up_delivery{this}; // x[no_unit],y[mw]
    ro<pump,425,typename A::_txy,no_unit,mw> frr_down_delivery{this}; // x[no_unit],y[mw]
    ro<pump,426,typename A::_txy,no_unit,mw> rr_up_delivery{this}; // x[no_unit],y[mw]
    ro<pump,427,typename A::_txy,no_unit,mw> rr_down_delivery{this}; // x[no_unit],y[mw]
    ro<pump,428,typename A::_txy,no_unit,mw> fcr_n_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,429,typename A::_txy,no_unit,mw> fcr_n_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,430,typename A::_txy,no_unit,mw> fcr_d_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,431,typename A::_txy,no_unit,mw> frr_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,432,typename A::_txy,no_unit,mw> frr_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,433,typename A::_txy,no_unit,mw> rr_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,434,typename A::_txy,no_unit,mw> rr_down_schedule_penalty{this}; // x[no_unit],y[mw]
    rw<pump,435,typename A::_txy,no_unit,nok_per_mw> reserve_ramping_cost_up{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,436,typename A::_txy,no_unit,nok_per_mw> reserve_ramping_cost_down{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,437,typename A::_txy,no_unit,mw> frr_up_min{this}; // x[no_unit],y[mw]
    rw<pump,438,typename A::_txy,no_unit,mw> frr_up_max{this}; // x[no_unit],y[mw]
    rw<pump,439,typename A::_txy,no_unit,mw> frr_down_min{this}; // x[no_unit],y[mw]
    rw<pump,440,typename A::_txy,no_unit,mw> frr_down_max{this}; // x[no_unit],y[mw]
    rw<pump,441,typename A::_txy,no_unit,mw> fcr_n_up_min{this}; // x[no_unit],y[mw]
    rw<pump,442,typename A::_txy,no_unit,mw> fcr_n_up_max{this}; // x[no_unit],y[mw]
    rw<pump,443,typename A::_txy,no_unit,mw> fcr_n_down_min{this}; // x[no_unit],y[mw]
    rw<pump,444,typename A::_txy,no_unit,mw> fcr_n_down_max{this}; // x[no_unit],y[mw]
    rw<pump,445,typename A::_txy,no_unit,mw> fcr_d_up_min{this}; // x[no_unit],y[mw]
    rw<pump,446,typename A::_txy,no_unit,mw> fcr_d_up_max{this}; // x[no_unit],y[mw]
    rw<pump,447,typename A::_txy,no_unit,mw> rr_up_min{this}; // x[no_unit],y[mw]
    rw<pump,448,typename A::_txy,no_unit,mw> rr_up_max{this}; // x[no_unit],y[mw]
    rw<pump,449,typename A::_txy,no_unit,mw> rr_down_min{this}; // x[no_unit],y[mw]
    rw<pump,450,typename A::_txy,no_unit,mw> rr_down_max{this}; // x[no_unit],y[mw]
    //--TODO: ro<pump,451,xyt,m3_per_s,mw> original_pq_curves{this}; // x[m3_per_s],y[mw]
    //--TODO: ro<pump,452,xyt,m3_per_s,mw> convex_pq_curves{this}; // x[m3_per_s],y[mw]
    //--TODO: ro<pump,453,xyt,m3_per_s,mw> final_pq_curves{this}; // x[m3_per_s],y[mw]
};
template<class A>
struct gate:obj<A,5> {
    using super=obj<A,5>;
    gate()=default;
    gate(A* s,int oid):super(s, oid) {}
    gate(const gate& o):super(o) {}
    gate(gate&& o):super(std::move(o)) {}
    gate& operator=(const gate& o) {
        super::operator=(o);
        return *this;
    }
    gate& operator=(gate&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<gate,456,int,no_unit,no_unit> type{this}; // x[no_unit],y[no_unit]
    rw<gate,458,int,no_unit,no_unit> time_delay{this}; // x[no_unit],y[no_unit]
    rw<gate,459,int,no_unit,no_unit> add_slack{this}; // x[no_unit],y[no_unit]
    rw<gate,460,double,no_unit,m3_per_s> max_discharge{this}; // x[no_unit],y[m3_per_s]
    rw<gate,463,typename A::_xy,hour,no_unit> shape_discharge{this}; // x[hour],y[no_unit]
    rw<gate,464,vector<typename A::_xy>,meter,m3_per_s> functions_meter_m3s{this}; // x[meter],y[m3_per_s]
    rw<gate,465,vector<typename A::_xy>,delta_meter,m3_per_s> functions_deltameter_m3s{this}; // x[delta_meter],y[m3_per_s]
    rw<gate,466,typename A::_txy,no_unit,m3_per_s> min_flow{this}; // x[no_unit],y[m3_per_s]
    rw<gate,467,typename A::_txy,no_unit,no_unit> min_flow_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,468,typename A::_txy,no_unit,m3_per_s> max_flow{this}; // x[no_unit],y[m3_per_s]
    rw<gate,469,typename A::_txy,no_unit,no_unit> max_flow_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,470,typename A::_txy,no_unit,m3_per_s> schedule_m3s{this}; // x[no_unit],y[m3_per_s]
    rw<gate,471,typename A::_txy,no_unit,percent> schedule_percent{this}; // x[no_unit],y[percent]
    rw<gate,472,typename A::_txy,no_unit,no_unit> schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,473,typename A::_txy,no_unit,no_unit> setting{this}; // x[no_unit],y[no_unit]
    rw<gate,474,typename A::_txy,no_unit,no_unit> setting_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,475,typename A::_txy,no_unit,nok_per_mm3> discharge_fee{this}; // x[no_unit],y[nok_per_mm3]
    rw<gate,476,typename A::_txy,no_unit,no_unit> discharge_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,477,typename A::_txy,no_unit,m3_per_s> block_merge_tolerance{this}; // x[no_unit],y[m3_per_s]
    rw<gate,478,typename A::_txy,no_unit,no_unit> discharge_group{this}; // x[no_unit],y[no_unit]
    rw<gate,479,typename A::_txy,no_unit,no_unit> min_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,480,typename A::_txy,no_unit,no_unit> max_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,481,typename A::_txy,no_unit,m3sec_hour> ramping_up{this}; // x[no_unit],y[m3sec_hour]
    rw<gate,482,typename A::_txy,no_unit,no_unit> ramping_up_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,483,typename A::_txy,no_unit,m3sec_hour> ramping_down{this}; // x[no_unit],y[m3sec_hour]
    rw<gate,484,typename A::_txy,no_unit,no_unit> ramping_down_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,485,typename A::_txy,no_unit,nok_per_m3_per_s> ramp_penalty_cost{this}; // x[no_unit],y[nok_per_m3_per_s]
    rw<gate,486,typename A::_txy,no_unit,no_unit> ramp_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    ro<gate,487,typename A::_txy,no_unit,nok> min_q_penalty{this}; // x[no_unit],y[nok]
    ro<gate,488,typename A::_txy,no_unit,nok> max_q_penalty{this}; // x[no_unit],y[nok]
    ro<gate,489,typename A::_txy,no_unit,m3_per_s> discharge{this}; // x[no_unit],y[m3_per_s]
    ro<gate,490,typename A::_txy,no_unit,m3_per_s> sim_discharge{this}; // x[no_unit],y[m3_per_s]
    rw<gate,491,double,meter,mm3> lin_rel_a{this}; // x[meter],y[mm3]
    rw<gate,492,double,no_unit,no_unit> lin_rel_b{this}; // x[no_unit],y[no_unit]
    rw<gate,493,typename A::_txy,no_unit,nok_per_mm3> max_q_penalty_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<gate,494,typename A::_txy,no_unit,nok_per_mm3> min_q_penalty_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<gate,495,typename A::_txy,no_unit,no_unit> max_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,496,typename A::_txy,no_unit,no_unit> min_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct thermal:obj<A,6> {
    using super=obj<A,6>;
    thermal()=default;
    thermal(A* s,int oid):super(s, oid) {}
    thermal(const thermal& o):super(o) {}
    thermal(thermal&& o):super(std::move(o)) {}
    thermal& operator=(const thermal& o) {
        super::operator=(o);
        return *this;
    }
    thermal& operator=(thermal&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
};
template<class A>
struct junction:obj<A,7> {
    using super=obj<A,7>;
    junction()=default;
    junction(A* s,int oid):super(s, oid) {}
    junction(const junction& o):super(o) {}
    junction(junction&& o):super(std::move(o)) {}
    junction& operator=(const junction& o) {
        super::operator=(o);
        return *this;
    }
    junction& operator=(junction&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<junction,512,int,no_unit,no_unit> junc_slack{this}; // x[no_unit],y[no_unit]
    rw<junction,513,double,no_unit,no_unit> altitude{this}; // x[no_unit],y[no_unit]
    ro<junction,514,typename A::_txy,no_unit,m3_per_s> tunnel_flow_1{this}; // x[no_unit],y[m3_per_s]
    ro<junction,515,typename A::_txy,no_unit,m3_per_s> tunnel_flow_2{this}; // x[no_unit],y[m3_per_s]
    ro<junction,516,typename A::_txy,no_unit,m3_per_s> sim_tunnel_flow_1{this}; // x[no_unit],y[m3_per_s]
    ro<junction,517,typename A::_txy,no_unit,m3_per_s> sim_tunnel_flow_2{this}; // x[no_unit],y[m3_per_s]
    rw<junction,518,double,no_unit,s2_per_m5> loss_factor_1{this}; // x[no_unit],y[s2_per_m5]
    rw<junction,519,double,no_unit,s2_per_m5> loss_factor_2{this}; // x[no_unit],y[s2_per_m5]
    rw<junction,520,typename A::_txy,no_unit,meter> min_pressure{this}; // x[no_unit],y[meter]
    ro<junction,521,typename A::_txy,no_unit,meter> pressure_height{this}; // x[no_unit],y[meter]
    ro<junction,522,typename A::_txy,no_unit,meter> sim_pressure_height{this}; // x[no_unit],y[meter]
    ro<junction,523,typename A::_txy,no_unit,nok> incr_cost{this}; // x[no_unit],y[nok]
    ro<junction,524,typename A::_txy,no_unit,nok> local_incr_cost{this}; // x[no_unit],y[nok]
};
template<class A>
struct junction_gate:obj<A,8> {
    using super=obj<A,8>;
    junction_gate()=default;
    junction_gate(A* s,int oid):super(s, oid) {}
    junction_gate(const junction_gate& o):super(o) {}
    junction_gate(junction_gate&& o):super(std::move(o)) {}
    junction_gate& operator=(const junction_gate& o) {
        super::operator=(o);
        return *this;
    }
    junction_gate& operator=(junction_gate&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<junction_gate,499,int,no_unit,no_unit> add_slack{this}; // x[no_unit],y[no_unit]
    rw<junction_gate,500,double,no_unit,meter> height_1{this}; // x[no_unit],y[meter]
    rw<junction_gate,501,double,no_unit,s2_per_m5> loss_factor_1{this}; // x[no_unit],y[s2_per_m5]
    rw<junction_gate,502,double,no_unit,s2_per_m5> loss_factor_2{this}; // x[no_unit],y[s2_per_m5]
    rw<junction_gate,503,typename A::_txy,no_unit,no_unit> schedule{this}; // x[no_unit],y[no_unit]
    ro<junction_gate,504,typename A::_txy,no_unit,meter> pressure_height{this}; // x[no_unit],y[meter]
    ro<junction_gate,505,typename A::_txy,no_unit,m3_per_s> tunnel_flow_1{this}; // x[no_unit],y[m3_per_s]
    ro<junction_gate,506,typename A::_txy,no_unit,m3_per_s> tunnel_flow_2{this}; // x[no_unit],y[m3_per_s]
    ro<junction_gate,507,typename A::_txy,no_unit,meter> tunnel_loss_1{this}; // x[no_unit],y[meter]
    ro<junction_gate,508,typename A::_txy,no_unit,meter> tunnel_loss_2{this}; // x[no_unit],y[meter]
};
template<class A>
struct creek_intake:obj<A,9> {
    using super=obj<A,9>;
    creek_intake()=default;
    creek_intake(A* s,int oid):super(s, oid) {}
    creek_intake(const creek_intake& o):super(o) {}
    creek_intake(creek_intake&& o):super(std::move(o)) {}
    creek_intake& operator=(const creek_intake& o) {
        super::operator=(o);
        return *this;
    }
    creek_intake& operator=(creek_intake&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<creek_intake,527,double,no_unit,meter> net_head{this}; // x[no_unit],y[meter]
    rw<creek_intake,528,double,no_unit,m3_per_s> max_inflow{this}; // x[no_unit],y[m3_per_s]
    rw<creek_intake,529,typename A::_txy,no_unit,m3_per_s> max_inflow_dynamic{this}; // x[no_unit],y[m3_per_s]
    rw<creek_intake,530,typename A::_txy,no_unit,m3_per_s> inflow{this}; // x[no_unit],y[m3_per_s]
    ro<creek_intake,531,typename A::_txy,no_unit,m3_per_s> sim_inflow{this}; // x[no_unit],y[m3_per_s]
    ro<creek_intake,532,typename A::_txy,no_unit,meter> sim_pressure_height{this}; // x[no_unit],y[meter]
    rw<creek_intake,533,typename A::_txy,no_unit,m3_per_s> inflow_percentage{this}; // x[no_unit],y[m3_per_s]
    rw<creek_intake,534,typename A::_txy,no_unit,nok_per_mm3> overflow_cost{this}; // x[no_unit],y[nok_per_mm3]
    ro<creek_intake,535,typename A::_txy,no_unit,no_unit> non_physical_overflow_flag{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct contract:obj<A,10> {
    using super=obj<A,10>;
    contract()=default;
    contract(A* s,int oid):super(s, oid) {}
    contract(const contract& o):super(o) {}
    contract(contract&& o):super(std::move(o)) {}
    contract& operator=(const contract& o) {
        super::operator=(o);
        return *this;
    }
    contract& operator=(contract&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
};
template<class A>
struct network:obj<A,11> {
    using super=obj<A,11>;
    network()=default;
    network(A* s,int oid):super(s, oid) {}
    network(const network& o):super(o) {}
    network(network&& o):super(std::move(o)) {}
    network& operator=(const network& o) {
        super::operator=(o);
        return *this;
    }
    network& operator=(network&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
};
template<class A>
struct market:obj<A,12> {
    using super=obj<A,12>;
    market()=default;
    market(A* s,int oid):super(s, oid) {}
    market(const market& o):super(o) {}
    market(market&& o):super(std::move(o)) {}
    market& operator=(const market& o) {
        super::operator=(o);
        return *this;
    }
    market& operator=(market&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<market,537,int,no_unit,no_unit> prod_area{this}; // x[no_unit],y[no_unit]
    rw<market,538,int,no_unit,no_unit> reserve_group{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<market,539,string,no_unit,no_unit> market_type{this}; // x[no_unit],y[no_unit]
    ro<market,540,typename A::_txy,no_unit,mw> buy{this}; // x[no_unit],y[mw]
    ro<market,541,typename A::_txy,no_unit,mw> sale{this}; // x[no_unit],y[mw]
    ro<market,542,typename A::_txy,no_unit,mw> sim_sale{this}; // x[no_unit],y[mw]
    rw<market,543,typename A::_txy,no_unit,mw> load{this}; // x[no_unit],y[mw]
    rw<market,544,typename A::_txy,no_unit,mw> max_buy{this}; // x[no_unit],y[mw]
    rw<market,545,typename A::_txy,no_unit,mw> max_sale{this}; // x[no_unit],y[mw]
    rw<market,546,typename A::_txy,no_unit,nok_per_mwh> buy_price{this}; // x[no_unit],y[nok_per_mwh]
    rw<market,547,typename A::_txy,no_unit,nok_per_mwh> sale_price{this}; // x[no_unit],y[nok_per_mwh]
    rw<market,548,typename A::_txy,no_unit,nok_per_mwh> buy_delta{this}; // x[no_unit],y[nok_per_mwh]
    rw<market,549,typename A::_txy,no_unit,nok_per_mwh> sale_delta{this}; // x[no_unit],y[nok_per_mwh]
    rw<market,550,typename A::_txy,no_unit,no_unit> bid_flag{this}; // x[no_unit],y[no_unit]
    rw<market,551,typename A::_txy,no_unit,no_unit> common_scenario{this}; // x[no_unit],y[no_unit]
    ro<market,552,typename A::_txy,no_unit,no_unit> reserve_obligation_penalty{this}; // x[no_unit],y[no_unit]
    ro<market,553,typename A::_txy,no_unit,no_unit> load_penalty{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct optimization:obj<A,13> {
    using super=obj<A,13>;
    optimization()=default;
    optimization(A* s,int oid):super(s, oid) {}
    optimization(const optimization& o):super(o) {}
    optimization(optimization&& o):super(std::move(o)) {}
    optimization& operator=(const optimization& o) {
        super::operator=(o);
        return *this;
    }
    optimization& operator=(optimization&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
};
template<class A>
struct reserve_group:obj<A,14> {
    using super=obj<A,14>;
    reserve_group()=default;
    reserve_group(A* s,int oid):super(s, oid) {}
    reserve_group(const reserve_group& o):super(o) {}
    reserve_group(reserve_group&& o):super(std::move(o)) {}
    reserve_group& operator=(const reserve_group& o) {
        super::operator=(o);
        return *this;
    }
    reserve_group& operator=(reserve_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<reserve_group,554,int,no_unit,no_unit> group_id{this}; // x[no_unit],y[no_unit]
    rw<reserve_group,555,typename A::_txy,no_unit,mw> fcr_n_up_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,556,typename A::_txy,no_unit,mw> fcr_n_down_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,557,typename A::_txy,no_unit,mw> fcr_d_up_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,558,typename A::_txy,no_unit,mw> frr_up_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,559,typename A::_txy,no_unit,mw> frr_down_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,560,typename A::_txy,no_unit,mw> rr_up_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,561,typename A::_txy,no_unit,mw> rr_down_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,562,typename A::_txy,no_unit,nok_per_mw> fcr_n_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<reserve_group,563,typename A::_txy,no_unit,nok_per_mw> fcr_d_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<reserve_group,564,typename A::_txy,no_unit,nok_per_mw> frr_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<reserve_group,565,typename A::_txy,no_unit,nok_per_mw> rr_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    ro<reserve_group,566,typename A::_txy,no_unit,mw> fcr_n_up_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,567,typename A::_txy,no_unit,mw> fcr_n_down_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,568,typename A::_txy,no_unit,mw> fcr_d_up_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,569,typename A::_txy,no_unit,mw> frr_up_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,570,typename A::_txy,no_unit,mw> frr_down_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,571,typename A::_txy,no_unit,mw> rr_up_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,572,typename A::_txy,no_unit,mw> rr_down_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,573,typename A::_txy,no_unit,mw> fcr_n_up_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,574,typename A::_txy,no_unit,mw> fcr_n_down_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,575,typename A::_txy,no_unit,mw> fcr_d_up_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,576,typename A::_txy,no_unit,mw> frr_up_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,577,typename A::_txy,no_unit,mw> frr_down_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,578,typename A::_txy,no_unit,mw> rr_up_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,579,typename A::_txy,no_unit,mw> rr_down_violation{this}; // x[no_unit],y[mw]
};
template<class A>
struct cut:obj<A,15> {
    using super=obj<A,15>;
    cut()=default;
    cut(A* s,int oid):super(s, oid) {}
    cut(const cut& o):super(o) {}
    cut(cut&& o):super(std::move(o)) {}
    cut& operator=(const cut& o) {
        super::operator=(o);
        return *this;
    }
    cut& operator=(cut&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
};
template<class A>
struct commit_group:obj<A,16> {
    using super=obj<A,16>;
    commit_group()=default;
    commit_group(A* s,int oid):super(s, oid) {}
    commit_group(const commit_group& o):super(o) {}
    commit_group(commit_group&& o):super(std::move(o)) {}
    commit_group& operator=(const commit_group& o) {
        super::operator=(o);
        return *this;
    }
    commit_group& operator=(commit_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<commit_group,599,int,no_unit,no_unit> group_id{this}; // x[no_unit],y[no_unit]
    rw<commit_group,600,typename A::_txy,no_unit,no_unit> exclude_group_when_committed{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct discharge_group:obj<A,17> {
    using super=obj<A,17>;
    discharge_group()=default;
    discharge_group(A* s,int oid):super(s, oid) {}
    discharge_group(const discharge_group& o):super(o) {}
    discharge_group(discharge_group&& o):super(std::move(o)) {}
    discharge_group& operator=(const discharge_group& o) {
        super::operator=(o);
        return *this;
    }
    discharge_group& operator=(discharge_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<discharge_group,580,int,no_unit,no_unit> group_id{this}; // x[no_unit],y[no_unit]
    rw<discharge_group,581,double,mm3,mm3> initial_deviation_mm3{this}; // x[mm3],y[mm3]
    rw<discharge_group,582,typename A::_txy,no_unit,mm3> max_accumulated_deviation_mm3_up{this}; // x[no_unit],y[mm3]
    rw<discharge_group,583,typename A::_txy,no_unit,mm3> max_accumulated_deviation_mm3_down{this}; // x[no_unit],y[mm3]
    rw<discharge_group,584,typename A::_txy,no_unit,m3_per_s> weighted_discharge_m3s{this}; // x[no_unit],y[m3_per_s]
    rw<discharge_group,585,typename A::_txy,no_unit,nok_per_mm3> penalty_cost_up_per_mm3{this}; // x[no_unit],y[nok_per_mm3]
    rw<discharge_group,586,typename A::_txy,no_unit,nok_per_mm3> penalty_cost_down_per_mm3{this}; // x[no_unit],y[nok_per_mm3]
    ro<discharge_group,587,typename A::_txy,no_unit,m3_per_s> actual_discharge_m3s{this}; // x[no_unit],y[m3_per_s]
    ro<discharge_group,588,typename A::_txy,no_unit,mm3> accumulated_deviation_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group,589,typename A::_txy,no_unit,mm3> upper_penalty_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group,590,typename A::_txy,no_unit,mm3> lower_penalty_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group,591,typename A::_txy,no_unit,mm3> upper_slack_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group,592,typename A::_txy,no_unit,mm3> lower_slack_mm3{this}; // x[no_unit],y[mm3]
};
template<class A>
struct production_group:obj<A,18> {
    using super=obj<A,18>;
    production_group()=default;
    production_group(A* s,int oid):super(s, oid) {}
    production_group(const production_group& o):super(o) {}
    production_group(production_group&& o):super(std::move(o)) {}
    production_group& operator=(const production_group& o) {
        super::operator=(o);
        return *this;
    }
    production_group& operator=(production_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<production_group,593,double,mwh,mwh> period_sum{this}; // x[mwh],y[mwh]
    rw<production_group,594,typename A::_txy,no_unit,mwh> period_flag{this}; // x[no_unit],y[mwh]
};
template<class A>
struct volume_constraint:obj<A,19> {
    using super=obj<A,19>;
    volume_constraint()=default;
    volume_constraint(A* s,int oid):super(s, oid) {}
    volume_constraint(const volume_constraint& o):super(o) {}
    volume_constraint(volume_constraint&& o):super(std::move(o)) {}
    volume_constraint& operator=(const volume_constraint& o) {
        super::operator=(o);
        return *this;
    }
    volume_constraint& operator=(volume_constraint&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<volume_constraint,595,typename A::_txy,no_unit,mm3> min_vol{this}; // x[no_unit],y[mm3]
    rw<volume_constraint,596,typename A::_txy,no_unit,mm3> max_vol{this}; // x[no_unit],y[mm3]
    rw<volume_constraint,597,typename A::_txy,no_unit,nok> min_vol_penalty{this}; // x[no_unit],y[nok]
    rw<volume_constraint,598,typename A::_txy,no_unit,nok> max_vol_penalty{this}; // x[no_unit],y[nok]
};
template<class A>
struct scenario:obj<A,20> {
    using super=obj<A,20>;
    scenario()=default;
    scenario(A* s,int oid):super(s, oid) {}
    scenario(const scenario& o):super(o) {}
    scenario(scenario&& o):super(std::move(o)) {}
    scenario& operator=(const scenario& o) {
        super::operator=(o);
        return *this;
    }
    scenario& operator=(scenario&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<scenario,602,int,no_unit,no_unit> scenario_id{this}; // x[no_unit],y[no_unit]
    rw<scenario,603,typename A::_txy,no_unit,no_unit> probability{this}; // x[no_unit],y[no_unit]
    rw<scenario,604,typename A::_txy,no_unit,no_unit> common_scenario{this}; // x[no_unit],y[no_unit]
    rw<scenario,605,typename A::_txy,no_unit,no_unit> common_history{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct objective:obj<A,21> {
    using super=obj<A,21>;
    objective()=default;
    objective(A* s,int oid):super(s, oid) {}
    objective(const objective& o):super(o) {}
    objective(objective&& o):super(std::move(o)) {}
    objective& operator=(const objective& o) {
        super::operator=(o);
        return *this;
    }
    objective& operator=(objective&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    //--TODO: ro<objective,606,string,no_unit,no_unit> solver_status{this}; // x[no_unit],y[no_unit]
    ro<objective,607,double,nok,nok> grand_total{this}; // x[nok],y[nok]
    ro<objective,608,double,nok,nok> sim_grand_total{this}; // x[nok],y[nok]
    ro<objective,609,double,nok,nok> total{this}; // x[nok],y[nok]
    ro<objective,610,double,nok,nok> rsv_end_value{this}; // x[nok],y[nok]
    ro<objective,611,double,nok,nok> sim_rsv_end_value{this}; // x[nok],y[nok]
    ro<objective,612,double,nok,nok> rsv_end_value_relative{this}; // x[nok],y[nok]
    ro<objective,613,double,nok,nok> market_sale_buy{this}; // x[nok],y[nok]
    ro<objective,614,double,nok,nok> sim_market_sale_buy{this}; // x[nok],y[nok]
    ro<objective,615,double,nok,nok> load_value{this}; // x[nok],y[nok]
    ro<objective,616,double,nok,nok> reserve_sale_buy{this}; // x[nok],y[nok]
    ro<objective,617,double,nok,nok> reserve_oblig_value{this}; // x[nok],y[nok]
    ro<objective,618,double,nok,nok> contract_value{this}; // x[nok],y[nok]
    ro<objective,619,double,nok,nok> startup_costs{this}; // x[nok],y[nok]
    ro<objective,620,double,nok,nok> sim_startup_costs{this}; // x[nok],y[nok]
    ro<objective,621,double,nok,nok> sum_penalties{this}; // x[nok],y[nok]
    ro<objective,622,double,nok,nok> minor_penalties{this}; // x[nok],y[nok]
    ro<objective,623,double,nok,nok> major_penalties{this}; // x[nok],y[nok]
    ro<objective,624,double,nok,nok> vow_in_transit{this}; // x[nok],y[nok]
    ro<objective,625,double,nok,nok> sum_feeding_fee{this}; // x[nok],y[nok]
    ro<objective,626,double,nok,nok> sum_discharge_fee{this}; // x[nok],y[nok]
    ro<objective,627,double,nok,nok> rsv_end_penalty{this}; // x[nok],y[nok]
    ro<objective,628,double,nok,nok> rsv_penalty{this}; // x[nok],y[nok]
    ro<objective,629,double,nok,nok> sim_rsv_penalty{this}; // x[nok],y[nok]
    ro<objective,630,double,nok,nok> load_penalty{this}; // x[nok],y[nok]
    ro<objective,631,double,nok,nok> group_time_period_penalty{this}; // x[nok],y[nok]
    ro<objective,632,double,nok,nok> group_time_step_penalty{this}; // x[nok],y[nok]
    ro<objective,633,double,nok,nok> sum_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,634,double,nok,nok> plant_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,635,double,nok,nok> rsv_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,636,double,nok,nok> gate_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,637,double,nok,nok> contract_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,638,double,nok,nok> group_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,639,double,nok,nok> discharge_group_penalty{this}; // x[nok],y[nok]
    ro<objective,640,double,nok,nok> common_decision_penalty{this}; // x[nok],y[nok]
    ro<objective,641,double,nok,nok> bidding_penalty{this}; // x[nok],y[nok]
    ro<objective,642,double,nok,nok> safe_mode_universal_penalty{this}; // x[nok],y[nok]
    ro<objective,643,double,nok,nok> gate_discharge_cost{this}; // x[nok],y[nok]
    ro<objective,644,double,nok,nok> bypass_cost{this}; // x[nok],y[nok]
    ro<objective,645,double,nok,nok> gate_spill_cost{this}; // x[nok],y[nok]
    ro<objective,646,double,nok,nok> physical_spill_cost{this}; // x[nok],y[nok]
    ro<objective,647,double,nok,nok> nonphysical_spill_cost{this}; // x[nok],y[nok]
    ro<objective,648,double,nok,nok> creek_spill_cost{this}; // x[nok],y[nok]
    ro<objective,649,double,nok,nok> creek_physical_spill_cost{this}; // x[nok],y[nok]
    ro<objective,650,double,nok,nok> creek_nonphysical_spill_cost{this}; // x[nok],y[nok]
    ro<objective,651,double,nok,nok> gate_slack_cost{this}; // x[nok],y[nok]
    ro<objective,652,double,nok,nok> junction_slack_cost{this}; // x[nok],y[nok]
    ro<objective,653,double,nok,nok> rsv_tactical_penalty{this}; // x[nok],y[nok]
    ro<objective,654,double,nok,nok> plant_p_constr_penalty{this}; // x[nok],y[nok]
    ro<objective,655,double,nok,nok> plant_q_constr_penalty{this}; // x[nok],y[nok]
    ro<objective,656,double,nok,nok> gate_q_constr_penalty{this}; // x[nok],y[nok]
    ro<objective,657,double,nok,nok> plant_schedule_penalty{this}; // x[nok],y[nok]
    ro<objective,658,double,nok,nok> gen_schedule_penalty{this}; // x[nok],y[nok]
    ro<objective,659,double,nok,nok> pump_schedule_penalty{this}; // x[nok],y[nok]
    ro<objective,660,double,nok,nok> reserve_violation_penalty{this}; // x[nok],y[nok]
    ro<objective,661,double,nok,nok> reserve_slack_cost{this}; // x[nok],y[nok]
    ro<objective,662,double,nok,nok> reserve_schedule_penalty{this}; // x[nok],y[nok]
};
template<class A>
struct bid_group:obj<A,22> {
    using super=obj<A,22>;
    bid_group()=default;
    bid_group(A* s,int oid):super(s, oid) {}
    bid_group(const bid_group& o):super(o) {}
    bid_group(bid_group&& o):super(std::move(o)) {}
    bid_group& operator=(const bid_group& o) {
        super::operator=(o);
        return *this;
    }
    bid_group& operator=(bid_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<bid_group,663,int,no_unit,no_unit> group_id{this}; // x[no_unit],y[no_unit]
    ro<bid_group,664,int,no_unit,no_unit> num_plants{this}; // x[no_unit],y[no_unit]
    ro<bid_group,666,int,no_unit,no_unit> price_dimension{this}; // x[no_unit],y[no_unit]
    ro<bid_group,667,int,no_unit,no_unit> time_dimension{this}; // x[no_unit],y[no_unit]
    ro<bid_group,668,int,no_unit,no_unit> bid_start_interval{this}; // x[no_unit],y[no_unit]
    ro<bid_group,669,int,no_unit,no_unit> bid_end_interval{this}; // x[no_unit],y[no_unit]
    ro<bid_group,673,double,no_unit,no_unit> reduction_cost{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<bid_group,675,xyt,nok_per_mwh,mwh> bid_curves{this}; // x[nok_per_mwh],y[mwh]
    ro<bid_group,676,typename A::_txy,nok_per_mwh,mwh> bid_penalty{this}; // x[nok_per_mwh],y[mwh]
};
template<class A>
struct cut_group:obj<A,23> {
    using super=obj<A,23>;
    cut_group()=default;
    cut_group(A* s,int oid):super(s, oid) {}
    cut_group(const cut_group& o):super(o) {}
    cut_group(cut_group&& o):super(std::move(o)) {}
    cut_group& operator=(const cut_group& o) {
        super::operator=(o);
        return *this;
    }
    cut_group& operator=(cut_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<cut_group,677,vector<typename A::_xy>,nok,nok> cut_rhs{this}; // x[nok],y[nok]
};
template<class A>
struct system:obj<A,24> {
    using super=obj<A,24>;
    system()=default;
    system(A* s,int oid):super(s, oid) {}
    system(const system& o):super(o) {}
    system(system&& o):super(std::move(o)) {}
    system& operator=(const system& o) {
        super::operator=(o);
        return *this;
    }
    system& operator=(system&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<system,678,vector<typename A::_xy>,nok,nok> cut_output_rhs{this}; // x[nok],y[nok]
    rw<system,679,vector<double>,nok,nok> cut_input_rhs{this}; // x[nok],y[nok]
    rw<system,680,int,no_unit,no_unit> cut_output_time{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct unit_combination:obj<A,25> {
    using super=obj<A,25>;
    unit_combination()=default;
    unit_combination(A* s,int oid):super(s, oid) {}
    unit_combination(const unit_combination& o):super(o) {}
    unit_combination(unit_combination&& o):super(std::move(o)) {}
    unit_combination& operator=(const unit_combination& o) {
        super::operator=(o);
        return *this;
    }
    unit_combination& operator=(unit_combination&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    //--TODO: ro<unit_combination,681,xyt,mw,m3_per_s> discharge{this}; // x[mw],y[m3_per_s]
    //--TODO: ro<unit_combination,682,xyt,mw,nok_per_mw> marginal_cost{this}; // x[mw],y[nok_per_mw]
    //--TODO: ro<unit_combination,683,xyt,m3_per_s,nok_per_mw> average_cost{this}; // x[m3_per_s],y[nok_per_mw]
};
template<class A>
struct tunnel:obj<A,26> {
    using super=obj<A,26>;
    tunnel()=default;
    tunnel(A* s,int oid):super(s, oid) {}
    tunnel(const tunnel& o):super(o) {}
    tunnel(tunnel&& o):super(std::move(o)) {}
    tunnel& operator=(const tunnel& o) {
        super::operator=(o);
        return *this;
    }
    tunnel& operator=(tunnel&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<tunnel,684,double,meter,meter> start_height{this}; // x[meter],y[meter]
    rw<tunnel,685,double,meter,meter> end_height{this}; // x[meter],y[meter]
    rw<tunnel,686,double,meter,meter> diameter{this}; // x[meter],y[meter]
    rw<tunnel,687,double,meter,meter> length{this}; // x[meter],y[meter]
    rw<tunnel,688,double,s2_per_m5,s2_per_m5> loss_factor{this}; // x[s2_per_m5],y[s2_per_m5]
    rw<tunnel,689,int,no_unit,no_unit> time_delay{this}; // x[no_unit],y[no_unit]
    rw<tunnel,690,typename A::_xy,no_unit,no_unit> gate_opening_curve{this}; // x[no_unit],y[no_unit]
    rw<tunnel,691,typename A::_txy,nok,nok> gate_adjustment_cost{this}; // x[nok],y[nok]
    rw<tunnel,692,typename A::_txy,no_unit,no_unit> gate_opening_schedule{this}; // x[no_unit],y[no_unit]
    rw<tunnel,693,double,no_unit,no_unit> initial_opening{this}; // x[no_unit],y[no_unit]
    rw<tunnel,694,int,no_unit,no_unit> continuous_gate{this}; // x[no_unit],y[no_unit]
    ro<tunnel,695,typename A::_txy,meter,meter> end_pressure{this}; // x[meter],y[meter]
    ro<tunnel,696,typename A::_txy,meter,meter> sim_end_pressure{this}; // x[meter],y[meter]
    ro<tunnel,697,typename A::_txy,m3_per_s,m3_per_s> flow{this}; // x[m3_per_s],y[m3_per_s]
    ro<tunnel,698,typename A::_txy,m3_per_s,m3_per_s> solver_flow{this}; // x[m3_per_s],y[m3_per_s]
    ro<tunnel,699,typename A::_txy,m3_per_s,m3_per_s> sim_flow{this}; // x[m3_per_s],y[m3_per_s]
    ro<tunnel,700,typename A::_txy,no_unit,no_unit> gate_opening{this}; // x[no_unit],y[no_unit]
    ro<tunnel,701,int,no_unit,no_unit> network_no{this}; // x[no_unit],y[no_unit]
    rw<tunnel,702,typename A::_txy,m3_per_s,m3_per_s> min_flow{this}; // x[m3_per_s],y[m3_per_s]
    rw<tunnel,703,typename A::_txy,m3_per_s,m3_per_s> max_flow{this}; // x[m3_per_s],y[m3_per_s]
    rw<tunnel,704,typename A::_txy,nok,nok> min_flow_penalty_cost{this}; // x[nok],y[nok]
    rw<tunnel,705,typename A::_txy,nok,nok> max_flow_penalty_cost{this}; // x[nok],y[nok]
};
template<class A>
struct interlock_constraint:obj<A,27> {
    using super=obj<A,27>;
    interlock_constraint()=default;
    interlock_constraint(A* s,int oid):super(s, oid) {}
    interlock_constraint(const interlock_constraint& o):super(o) {}
    interlock_constraint(interlock_constraint&& o):super(std::move(o)) {}
    interlock_constraint& operator=(const interlock_constraint& o) {
        super::operator=(o);
        return *this;
    }
    interlock_constraint& operator=(interlock_constraint&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<interlock_constraint,706,typename A::_txy,no_unit,no_unit> min_open{this}; // x[no_unit],y[no_unit]
    rw<interlock_constraint,707,typename A::_txy,no_unit,no_unit> max_open{this}; // x[no_unit],y[no_unit]
    rw<interlock_constraint,708,double,no_unit,no_unit> forward_switch_time{this}; // x[no_unit],y[no_unit]
    rw<interlock_constraint,709,double,no_unit,no_unit> backward_switch_time{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct flow_constraint:obj<A,28> {
    using super=obj<A,28>;
    flow_constraint()=default;
    flow_constraint(A* s,int oid):super(s, oid) {}
    flow_constraint(const flow_constraint& o):super(o) {}
    flow_constraint(flow_constraint&& o):super(std::move(o)) {}
    flow_constraint& operator=(const flow_constraint& o) {
        super::operator=(o);
        return *this;
    }
    flow_constraint& operator=(flow_constraint&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
};

}
