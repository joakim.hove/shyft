#pragma once
#include <ctime> //localtime
#include <iomanip> // put_time
#include <sstream> // stringstream>
#include <chrono>
#include <iostream>

namespace shyft::energy_market::stm::srv::dstm {

    /** @brief Configuration class for Dlib logger:
     */
    class server_log_hook {
    public:
        server_log_hook(){
            // Set up log directory or such
        }
        
        void log(const string& logger_name,
                 const dlib::log_level& ll,
                 const dlib::uint64 thread_id,
                 const char* message_to_log){
            // Log all messages to cout for now:
            auto t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
            std::stringstream ss;
            ss << std::put_time(std::localtime(&t), "%d/%m/%Y %H:%M:%S ");
            std::cout << ss.str() << ll << " [" << thread_id << "] " << logger_name << ": " << message_to_log << std::endl;
        }
    };
    
    inline void configure_logger(
                    server_log_hook& hook,
                    const dlib::log_level& ll){
        dlib::set_all_logging_levels(ll);
        dlib::set_all_logging_output_hooks(hook);
    }
}
