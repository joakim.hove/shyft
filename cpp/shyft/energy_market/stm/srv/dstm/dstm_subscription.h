#pragma once
#include <functional>
#include <shyft/core/subscription.h>
#include <shyft/dtss/dtss_subscription.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/a_wrap.h>
#include <shyft/web_api/json_struct.h>
#include <shyft/web_api/bg_work_result.h>
#include <shyft/web_api/energy_market/request_handler.h>
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::energy_market::stm::subscription {
    using std::string;
    
    using shyft::time_series::dd::ats_vector;
    using shyft::time_series::dd::ts_as;
    using shyft::time_series::dd::gpoint_ts;
    using shyft::time_series::dd::aref_ts;
    
    using shyft::core::subscription::observer_base;
    using shyft::dtss::subscription::ts_expression_observer_;
    using shyft::dtss::subscription::ts_expression_observer;
    using shyft::core::subscription::manager_;
    using shyft::core::subscription::observable_;
    using shyft::web_api::energy_market::json;
    using shyft::web_api::bg_work_result;
    
    using shyft::energy_market::stm::srv::dstm::server;    
    using shyft::energy_market::stm::stm_system_;
    using shyft::energy_market::stm::reservoir;
    using shyft::energy_market::stm::power_plant;
    using shyft::energy_market::stm::unit;
    using shyft::energy_market::stm::waterway;
    using shyft::energy_market::stm::catchment;
    using shyft::energy_market::proxy_attr;
    
    /** @brief observer class for attributes in an stm_system/stm_hps */
    struct proxy_attr_observer: observer_base {
       
        proxy_attr_observer(server *const srv, string const& request_id, json const& data, std::function<bg_work_result(json const&)>&& cb):
                observer_base(srv->sm, request_id), request_data{data}, response_cb{std::move(cb)} {
            mid = boost::get<string>(request_data.required("model_key"));
            // In the case that we use the dtss to keep subscriptions for time series:
            if (srv->dtss) {
                ts_sm = srv->dtss->sm;
            } else
                ts_sm = srv->sm;
        }
        
        virtual bool recalculate() override {
            auto updated = has_changed();
            published_version = terminal_version();
            return updated;
        }
        
        virtual int64_t terminal_version() const noexcept override {
            int64_t r = observer_base::terminal_version();
            for (auto& sub : ts_subs) r += sub->terminal_version();
            return r;
        }

        bg_work_result re_emit_response() const {
            return response_cb(request_data);
        }

        /** @brief Add an observable to terminals based on an attribute of a Struct.
         * Returns true if new subscription was added.
         *
         * @tparam Struct : The type of the owning object. E.g. reservoir, waterway &c.
         * @tparam LeafAccessor : The attribute to add subscription to. Here represented by the leaf accessor to it
         *  (nested member function pointers + attribute names) See shyft/mp.h
         */
        template<class Struct, class LeafAccessor>
        bool add_subscription(Struct const& t, LeafAccessor&& la) {
            using V = typename decltype(+mp::leaf_accessor_type(std::declval<LeafAccessor>()))::type;
            static const char* prefix="dstm://M";            // Initialize sub_id
            auto pa=proxy_attr(t, mp::leaf_accessor_id_str(la),mp::leaf_access(t,la));
            string sub_id=pa.url(prefix+mid);
            // Handling depending on value type of attribute (want dtss to handle time series)
            // Value type of attribute:
            
            if constexpr (std::is_same_v<V, apoint_ts>) {
                // Check that we are not already subscribing to it:
                auto it = std::find_if(ts_subs.begin(), ts_subs.end(),
                    [&sub_id](auto el) { return el->request_id == sub_id; });
                if (it == ts_subs.end()) {
                    dtss::ts_vector_t tsv;
                    apoint_ts const& ts = mp::leaf_access(t, la); // Get time-series value
                    if (!ts.ts) {
                        tsv.emplace_back(apoint_ts(sub_id));
                    } else if (ts_as<gpoint_ts>(ts.ts)) { // In the case gpoint_ts, all we have are values,
                        tsv.emplace_back(apoint_ts(sub_id, ts)); // so we just append the sub_id to the subscription to the dtss.
                    } else if (auto sts = ts_as<aref_ts>(ts.ts)) {
                        if (sts->needs_bind() && !(sts->id.rfind(prefix,0) == 0)) { // We are subscribing to an unbound series stored on the dtss
                            tsv.emplace_back(ts);// In this case, the attribute is considered void of values, and id starts with shyft://...
                        } else {
                            tsv.emplace_back(apoint_ts(sub_id, ts));// Here, the attribute time series has been bound, by e.g. evaluate_stm_system.
                        }
                    } else {
                        tsv.emplace_back(ts);// Everything else, like expressions.
                    }
                    auto new_sub = std::make_shared<ts_expression_observer>(ts_sm, sub_id, tsv, [](ats_vector)->ats_vector { return ats_vector{}; });
                    ts_subs.emplace_back(new_sub);
                    return true;
                } else {
                    return false;
                }
            } else { // The case that the value type is not time-series.
                auto subject = sm->add_subscription(sub_id)[0];
                // Check that it's not already part of terminals
                auto it = std::find_if(terminals.begin(), terminals.end(),
                    [&subject](auto el) { return el->id == subject->id; });
                if (it == terminals.end()) {
                    terminals.emplace_back(subject);
                    return true;
                } else {
                    return false;
                }
            }
        }

        // Member variables:
        manager_ ts_sm;             ///  subscription manager for time series
        vector<ts_expression_observer_> ts_subs; /// Subscriptions to time series, which should be handled by the dtss, if present
        json request_data;          /// The data containing what attributes, what components, what model to subscribe to.
        string mid;                 /// the model id we are working on (the unique, run-time/in-memory string known to client/server)
        std::function<bg_work_result(json const&)> response_cb; /// callback function to be called when value of observed is updated and response has to be re emitted.
    };
    using proxy_attr_observer_ = std::shared_ptr<proxy_attr_observer>;
}
