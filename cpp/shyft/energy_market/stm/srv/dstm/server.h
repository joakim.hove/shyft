#pragma once

#include <string>
#include <cstdint>
#include <exception>
#include <memory>
#include <mutex>
#include <shared_mutex>
#include <atomic>
#include <future>
#include <fstream>
#include <algorithm>
#include <functional>
#include <dlib/server.h>
#include <dlib/logger.h>

#include <boost/filesystem/fstream.hpp>

#include <shyft/core/fs_compat.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/market.h>

#include <shyft/energy_market/stm/srv/dstm/msg_defs.h>
#include <shyft/energy_market/stm/srv/dstm/context_enums.h>
#include <shyft/energy_market/stm/srv/dstm/compute_node.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/srv/model_info.h>

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/core/subscription.h>
#include <shyft/dtss/dtss.h>

#ifdef SHYFT_WITH_SHOP
#include <shyft/energy_market/stm/shop/shop_system.h>
#else
#include <shyft/energy_market/stm/shop/shop_log_entry.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#endif

namespace shyft::energy_market::stm::srv::dstm {

	using std::vector;
	using std::shared_ptr;
	using std::make_shared;
    using std::unique_ptr;
    using std::make_unique;
	using std::string;
	using std::to_string;
	using std::runtime_error;
	using std::mutex;
	using std::unique_lock;
    using std::shared_mutex;
    using std::shared_lock;
    using std::map;
    using std::atomic_uint;
	using shyft::core::utctime;
	using shyft::core::no_utctime;
    using shyft::core::utctime_now;
    using shyft::core::utcperiod;
	using shyft::core::subscription::manager;
    using shyft::core::subscription::manager_;

    //using shyft::dtss::standard_dtss_dispatcher;
    using ts_server = shyft::dtss::server;
    using shyft::dtss::ts_vector_t;
    using shyft::dtss::id_vector_t;

    using shyft::time_series::dd::ts_as_mutable;
	using shyft::time_series::dd::aref_ts;
	using shyft::time_series::dd::ts_bind_info;
    using shyft::time_series::dd::ats_vector;

	using shyft::energy_market::stm::stm_hps;
	using shyft::energy_market::stm::reservoir;
	using shyft::energy_market::stm::unit;
	using shyft::energy_market::stm::power_plant;
	using shyft::energy_market::stm::catchment;
	using shyft::energy_market::stm::waterway;
	using shyft::energy_market::stm::gate;
	using shyft::energy_market::stm::energy_market_area;

	using shyft::srv::model_info;
    using std::map;
    struct stm_system_context;
    using stm_system_context_=shared_ptr<stm_system_context>;
    
	using namespace shyft::energy_market::stm::shop;
	using fx_call_back_t = std::function<bool(string,string)>; ///< the type of callback provided, 1st arg model-id, 2nd arg fx-verb 

 
    /** @brief Class for custom handling of SIGSEGV signals.
     * We want to restrict the use of this as much as possible, but SHOP may try to access
     * invalid memory if we give it bad input, which our validation does not yet cover.
     * 
     * Set this as signal handler using
     * std::signal(SIGSEGV, shop_segfault_handler::sigsegv_handler);
     * And revert back to default with
     * std::signal(SIGSEGV, SIG_DLF);
     */
    struct shop_segfault_handler {
        shop_segfault_handler();
        ~shop_segfault_handler();
        // We want it movable, but not copyable:
        shop_segfault_handler(shop_segfault_handler const&) = delete;
        shop_segfault_handler & operator=(shop_segfault_handler const&) = delete;
        shop_segfault_handler(shop_segfault_handler&&) { curr_opt++; };
        shop_segfault_handler & operator=(shop_segfault_handler&&) = delete;
        
        thread_local static bool shop_call; /// Do we receive the SIGSEGV signal from SHOP?
        inline static bool sigsegv_received = false; ///< Have we encountered a SIGSEGV from SHOP previously?
        
        static void sigsegv_handler(int signum);
        
    private:
        inline static atomic_uint curr_opt = 0; ///< Current number of SHOP optimizations running.
    };
    
    struct client;
    using client_=shared_ptr<client>;
    
    class compute_node_manager;//fwd

    
	/** @brief a server for serializable (parts of) hydrology forecasting models,
	*
	* Currently using dlib server_iostream, 
	*/
	struct server : public dlib::server_iostream {

		server();
		server(server&&) = delete;
		server(const server&) = delete;
		server& operator=(const server&) = delete;
		server& operator=(server&&) = delete;
		~server();

		fx_call_back_t fx_cb;///< user specified callback that can be invoked using web-api fx(..) or python c.fx(..) typically run model
		mutex srv_mx;///< protect server context (use when changes to model_map)
		map<string, stm_system_context_> model_map;///< key=mid, val=context for a model

        const static dlib::logger slog; ///< server-side logger
        
        unique_ptr<ts_server> dtss; ///< Server for storing and handling time series and expressions related to models.
        manager_ sm=make_shared<manager>();///< the subscription_manager so that web-api can support observable expression-vectors
        unique_ptr<compute_node_manager> cn_mgr;///< if active, use dstm slaves

        /** @brief start the server in background, return the listening port used in case it was set unspecified */
		int start_server();

        /** @brief set up the dtss for the server */
        void setup_dtss();
        
        /** @brief When reading time series using the DTss, it will search in its own containers if the ts_url starts with shyft://.
         *  For ts_url's of the type dstm://model_key/hps_id/(R|W|U|P|...)component_id/attribute_id
         *  we use the following callbak function to resolve to time series.
         */
        ts_vector_t dtss_read_callback(const id_vector_t& ts_ids, utcperiod p);
        
        /** 
         * @brief Set the dtss to master/slave mode
         * @details This means that the embedded dtss server redirects all IO to a master dtss 
         * getting the real ts from the master 
         */
        void set_master(string ip, int port,double master_poll_time,size_t unsubscribe_min_threshold,double unsubscribe_max_delay);
        
        /** @brief add a container to the dtss */
        void add_container(const string& container_name, const string& root_dir);
        
		/** @brief get current api version */
		string do_get_version_info();

		/** @brief create a new model with id */
		bool do_create_model(string const& mid);

		/** @brief add existing model with id */
		bool do_add_model(string const& mid, stm_system_ mdl);

		/** @brief remove (free up mem etc) model by id */
		bool do_remove_model(string const& mid);

		/** @brief rename a model by id */
		bool do_rename_model(string old_mid, string new_mid);

		/** @brief clone existing model with id */
		bool do_clone_model(string const& old_mid, string new_mid);

		/** @brief get models, returns a string list with model identifiers */
		vector<string> do_get_model_ids() ;

		/** @brief get model infos. Returns a mapping from model identifiers to model_infos */
		map<string, model_info> do_get_model_infos();

        stm_system_ do_get_model(string const&mid);
        
		/** @brief get context, that keeps model with sync. primitives and context */
		stm_system_context_ do_get_context(string const& mid);

		void do_set_state(string const& mid, model_state const& state);

		model_state do_get_state(string const& mid);

        bool is_master() const;
        /**
         * @brief copy result time-series(etc) from src to dst model 
         * @details
         * Copy as if shop_api collect,
         * so same members, and possibly notifications.
         * Access to the dst model should be ensured
         * by the caller using context mutex etc..
         */
        void copy_results(stm_system_ const& src,stm_system_ const &dst);


        bool do_slave_optimize(const string& mid, const generic_dt& ta, const vector<shop_command>& cmd);
        vector<shop_log_entry> do_slave_get_log(const string& mid);
		/** @brief Get SHOP log for a model
         */
        vector<shop_log_entry> do_get_log(const string& mid);
		/** @brief start SHOP optimization on a model
		*/
		bool do_optimize(const string& mid, const generic_dt& ta, const vector<shop_command>& cmd);

        /** @brief optimization summary for stm system
		*/
        optimization_summary_ do_get_optimization_summary(const string &mid);

        
        /** @brief Evaluate any unbound time series of a model.
         * NOTE: This should only be called for clones of models stored on server.
         * Otherwise, the model will not need binding, which may lead to subsequent
         * requests to read attributes not yielding up-to-date values.
         */
        bool do_evaluate_model(const string& mid, utcperiod bind_period, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_period=utcperiod{});
        ts_vector_t evaluate_stm_system(stm_system const& mdl, utcperiod bind_period, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_period=utcperiod{});

        static bool stm_system_needs_bind(const stm_system& mdl);
        
        /**
         * @brief get_ts from model
         * @details
         * Given model id, and  ts-urls, resolve and return ts_vector
         * Used from python/c++ to read time-series attached to the model.
         * Similar to the web-api, using same underlying mechanisms.
         * @param mid the model id,the ts_urls should all refer to this model id
         * @param ts_urls, list of dstm://Mmid/path..
         * @return resolved list of time-series, 1 to 1 as for the request
         */
        ats_vector do_get_ts(const string &mid,const vector<string> &ts_urls);

        /** @brief set annotated time-series on the model
         *
         * @details
         * The `tsv` parameter should contain apoint_ts with, id and payload(the values).
         * It attaches/replaces the values to the attribute that is addressed.
         *
         * @param mid the model context identifier(all urls should address this)
         * @param tsv the time-series, with id, ts-urls, like dstm://Mmid
         */
        void do_set_ts(const string &mid, ats_vector const &tsv);

        /** @brief write lock model, set annotated time-series on the model
         *
         * @details
         * The `tsv` parameter should contain apoint_ts with, id and payload(the values).
         * It attaches/replaces the values to the attribute that is addressed.
         *
         * @param mid the model context identifier(all urls should address this)
         * @param tsv the time-series, with id, ts-urls, like dstm://Mmid
         */
        void do_lock_set_ts(const string &mid, ats_vector const &tsv);

        /**@brief do fx(arg1,arg2)
         * 
         * if the fx_cb exist, call it
         */
        bool do_fx(string mid, string action);
        void do_add_compute_node(string host_port);
        void do_remove_compute_node(string host_port);
        vector<compute_node> do_compute_node_info();

		/** @brief handle one client connection 
		*
		* Reads messages/requests from the clients,
		* - act and perform request,
		* - return response
		* for as long as the client keep the connection 
		* open.
		* 
		*/
		void on_connect(
			std::istream & in,
			std::ostream & out,
			const std::string & /*foreign_ip*/,
			const std::string & /*local_ip*/,
			unsigned short /*foreign_port*/,
			unsigned short /*local_port*/,
			dlib::uint64 /*connection_id*/
		);
	};

	//---------------------------------------------------
	// HELPER FUNCTIONS FOR CLONING MODELS WITH
	// REFERENCES TO OTHER ATTRIBUTES IN THE MODEL
	//---------------------------------------------------
	/** @brief Replace model-key part of aref_ts.id if
	 * the ID starts with dstm://
	 *
	 * returns true if replacement was made, false if either
	 * the underlying time series is not an aref_ts, or its
	 * ID doesn't start with dstm://M<model_key>
	 */
	extern bool replace_model_key_in_id(apoint_ts& ats, string const& new_mkey);

	/** @brief Rebind time series id of any time series type.
	 * If ts is an aref_ts: executes replace_model_key_in_id
	 * If ts is an expression, or has nonzero bind info: Goes through each time series in bind info
	 *  and executed replace_model_key_in_id.
	 * Else: Does nothing.
	 *
	 * @param ats: Time series to rebind
	 * @param new_mkey: New model_key to replace part of url.
	 * @return
	 */
	extern bool rebind_ts(apoint_ts& ats, string const& new_mkey); 


	/** @brief Rebind all time series in the stm_hps
	 *
	 */
	extern bool rebind_ts(stm_hps& hps, string const& new_mkey);

	extern bool rebind_ts(stm_system& mdl, string const& new_mkey);

}

