#pragma once

#include <string>
#include <cstdint>
#include <exception>

#include <dlib/iosockstream.h>

#include <shyft/energy_market/stm/srv/dstm/msg_defs.h>
#include <shyft/energy_market/stm/srv/dstm/context.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/srv/model_info.h>
#include <shyft/energy_market/stm/srv/dstm/compute_node.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/ats_vector.h>

#ifdef SHYFT_WITH_SHOP
#include <shyft/energy_market/stm/shop/shop_system.h>
#else
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/shop/shop_log_entry.h>
#endif

namespace shyft::energy_market::stm::srv::dstm {
	using std::vector;
    using std::map;
	using std::string;
	using std::string_view;
	using std::to_string;
	using std::runtime_error;
	using shyft::core::srv_connection;
	using shyft::core::scoped_connect;
	using shyft::core::do_io_with_repair_and_retry;
	using shyft::core::utctime;
	using shyft::core::no_utctime;
	using shyft::srv::model_info;
    using shyft::time_series::dd::ats_vector;
    using shyft::energy_market::stm::shop::shop_command;
	using shyft::energy_market::stm::shop::shop_log_entry;
	/**
     * @brief a client
	 *
	 *
	 * This class take care of message exchange to the remote server,
	 * using the supplied connection parameters.
	 *
	 * It implements the message protocol of the server, sending
	 * message-prefix, arguments, waiting for response
	 * deserialize the response and handle it back to the user.
	 *
	 * @see server
	 *
	 */
	struct client {
		srv_connection c;
		//client()=delete;
		client(string host_port,int timeout_ms=1000);

		string version_info() ;
		bool create_model(string const& mid);
		bool add_model(string const& mid, stm_system_ mdl);
		bool remove_model(string const&mid) ;
		bool rename_model(string const& old_mid, string const& new_mid);
		bool clone_model(string const& old_mid, string const& new_mid);
		vector<string> get_model_ids() ;
		map<string, model_info> get_model_infos() ;
		stm_system_ get_model(string const& mid) ;

		bool optimize(const string& mid, const generic_dt& ta, const vector<shop_command>& cmd);

		/** @brief get SHOP log for model.
         */
        vector<shop_log_entry> get_log(const string& mid);

        optimization_summary_ get_optimization_summary(const string &mid);

        /** @brief get state of a model:
         */
        model_state get_state(const string& mid);
        
        /** @brief set state of a model
         * 
         * @details it is not possible to set state running
         */
        void set_state(const string&mid, model_state x);
        
         /** @brief exeute fx(mid,fx_arg) on the server side
         */
        bool fx(const string& mid, const string& fx_arg);

        /** @brief evaluate any unbound time series attributes in a model.
         */
        bool evaluate_model(const string& mid, utcperiod bind_period, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_period=utcperiod{});
        
		/** @brief close, until needed again, the server connection
		*
		*/
		void close();

        /**
         * @brief get_ts from model
         * @details
         * Given model id, and  ts-urls, resolve and return ts_vector
         * Used from python/c++ to read time-series attached to the model.
         * Similar to the web-api, using same underlying mechanisms.
         * @param mid the model id,the ts_urls should all refer to this model id
         * @param ts_urls, list of dstm://Mmid/path..
         * @return resolved list of time-series, 1 to 1 as for the request
         */
        ats_vector get_ts(const string &mid,const vector<string> &ts_urls);

        /**
         * @brief set_ts into the model
         * @details
         * Given model id, and  ts-urls, resolve and set the supplied time-series
         * accoring to the supplied ts-urls. They should all refer to mid.
         * Used from python/c++ to set time-series attached to the model.
         * Similar to the web-api, using same underlying mechanisms,
         * including notify change to subscribers
         * @param mid the model id,the ts_urls should all refer to this model id
         * @param ts_urls, list of dstm://Mmid/path..
         *
         */
        void set_ts(const string &mid,const ats_vector &tsv);

        /**
         * @brief add compute node by host_port
         * @details
         * add the specified host:port to the server so that optimizations can be run
         * on the specified node. If the host:port is already added this is a no-op.
         * Note that if the host port in question is marked for removal(pending remove),
         * this mark is cleared(so that it's not removed).
         *
         * @param host_port formatted as a validhost:port address
         */
        void add_compute_node(string host_port);

        /**
         * @brief remove compute node by host_port
         * @details
         * If the server has the specified compute node in it's list it will be
         * removed. If it's not there, this is a noop.
         * Notice that if the specified compute node is doing computing, it will be
         * marked for removal and removed at first attempt.
         *
         * @param host_port formatted as a validhost:port address
         */
        void remove_compute_node(string host_port);
        /**
         * @brief compute-node info
         * @return
         * Returns the current state of the compute-node's related to the server.
         * If there is no compute-nodes added to the server, a zero length vector
         * is returned.
         */
        vector<compute_node> compute_node_info();

    };
}
