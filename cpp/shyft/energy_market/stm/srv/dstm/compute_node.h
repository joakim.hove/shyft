/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once

#include <string>
#include <unordered_map>
#include <mutex>
#include <boost/thread/sync_queue.hpp>
#include <shyft/time/utctime_utilities.h>


namespace shyft::energy_market::stm::srv::dstm {
using std::string;
using std::vector;
using namespace shyft::core;


/**
* @brief client side compute-node info and statistics
* @note
* Later we might add compute capablities(like optimize, simulate etc.).
*/
struct compute_node {
    string host_port;///<where to connect to the slave, like ip:portno, '192.168.1.2:10000'
    compute_node()=default;
    explicit compute_node(string host_port):host_port(host_port){}

    // statistics and nice to know through use
    size_t fail_count{0}; ///<keep track of # failures, updated by the user of this class.
    utctime last_contact{no_utctime};///< also keep track of last successful contact, updated by user of the compute_node struct
    string mid;///< model identifier last, or currently in progress, updated by the user of the compute_node struct
    utctime allocated_time{no_utctime};///< keep track of when it was allocated for compute, no_utctime if not in work. updated by the compute_node_manager
    utctime released_time{no_utctime};///< keep track of when it was released, set to no_utctime while allocated, updated by the compute_node_manager
    bool removed{false};///< indicate that this item is to be removed, at first opportunity.
    bool operator==(compute_node const&o) const {
        return host_port==o.host_port && fail_count == o.fail_count && last_contact == o.last_contact
        && mid == o.mid && allocated_time == o.allocated_time && released_time == o.released_time && removed == o.removed
        ;
    }
    bool operator!=(compute_node const&o) const { return !operator==(o);}
    bool empty() const {return host_port.size()==0;}
    x_serialize_decl();// to support query for statistics
};
using compute_node_=std::shared_ptr<compute_node>;

/**
* @brief compute_node_manager keeps a list of available compute-nodes
* @details
* The dstm master uses a list of available compute-nodes
* represented as the compute_node.
* When a dstm compute node is allocated to run a model,
* then the client handler is used to ensure that
* only one run pr/ client is executed at once.
* This ensures proper isolation of errors/seg-faults
* that we experience with current model algorithms.
* It also allow us to scale out/parallize the
* computations.
*/
class compute_node_manager {
    using queue=boost::sync_queue<compute_node_>;
    queue idle_nodes;///< list of available slaves
    std::unordered_map<string,compute_node_> all_nodes;///< all compute nodes, enforced unique by host_port
    mutable std::mutex mx;///< to ensure safe/robust access to node-manager maintained compute-nodes.
    public:
    compute_node_manager()=default;
    /**
    * @brief construct with initial set of nodes
    * @param compute_nodes vector of strings with 'host:port_number'
    */
    explicit compute_node_manager (std::vector<string> const& compute_nodes);

    /**
    * @brief add extra compute nodes
    * @param compute_nodes vector of strings with 'host:port_number'
    *
    */
    void add_compute_nodes(std::vector<string> const& compute_nodes);

    /**
    * @brief remove specified compute nodes
    * @param compute_nodes vector of strings with 'host:port_number'
    */
    void remove_compute_nodes(std::vector<string> const& compute_nodes);

    /** @brief allocate a compute_node for use
    * @details
    * before use: get a one available connect_info to use for dstm compute node run
    * when done use put to return it to the idle queue(use a scoped-lock-release class).
    * @return an exlusive compute node to be used during computation
    */
    compute_node_ get(string const&mid);

    /** @brief release a compute node after use */
    void put(compute_node_ const&ci);

    /** @brief safely register last contact time with a compute node */
    void register_contact(compute_node_ const& ci, utctime t) const;

    /** @brief safely register number of failures with a compute node */
    void register_failures(compute_node_ const& ci,size_t n_new_failures=1) const;

    /** @brief get statistics and diagnostics of the compute nodes */
    std::vector<compute_node> compute_nodes() const;
};

/**
 * @brief Scoped moveable compute node utility for RAII
 *
 * @details The purpose of this class is to ensure safe access to the compute node representation
 * and ensure that the remote resource is put back to queue when done. E.g. when this class goes out
 * of scope, the compute node manager is handed back the compute node shared ptr.
 */
struct scoped_compute_node {
    compute_node_manager & cn_mgr;///< ref to the manager that keeps the compute nodes.
    compute_node_ cn;///< the scoped locked compute node
    scoped_compute_node(const scoped_compute_node&)=delete;
    scoped_compute_node& operator=(const scoped_compute_node&)=delete;
    scoped_compute_node(scoped_compute_node&&)=default;

    scoped_compute_node(compute_node_manager& mgr,string const&mid):cn_mgr{mgr} {
        cn=cn_mgr.get(mid);
    }
    ~scoped_compute_node() {
        if(cn) /// important:: move will zero out cn, so this ensures it's really movable
            cn_mgr.put(cn);
    }
    /** @brief the client host port where the compute node can be reached */
    string client_host_port() const {return cn?cn->host_port:""; }

    /** @brief safely register time of contact with the compute node */
    void register_contact(utctime t) const {
        if(cn)
            cn_mgr.register_contact(cn,t);
    }

    /** @brief safely register one failure on the compute node */
    void register_failure() const {
        if(cn)
            cn_mgr.register_failures(cn,1);
    }
};

}
x_serialize_export_key(shyft::energy_market::stm::srv::dstm::compute_node);
