#pragma once
#include <string>
#include <memory>
#include <vector>
#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/optimization_summary.h>

namespace shyft::energy_market::stm {
    using std::shared_ptr;
    using std::weak_ptr;
    using std::vector;
    using std::string;
    using std::logic_error;
    using shyft::core::utctime;
    using shyft::core::utctimespan;
    using shyft::core::utcperiod;
    using shyft::time_axis::generic_dt;
    using std::static_pointer_cast;
 
    // --
    // fwd enough of the sub-components so that the top level is well defined.
    // this file should be possible to include from the sub-components,
    // so we have to forward decl. types here, 
    struct energy_market_area;
    using energy_market_area_=shared_ptr<energy_market_area>;
    struct reservoir;
    using reservoir_=shared_ptr<reservoir>;
    struct unit;
    using unit_=shared_ptr<unit>;
    struct unit_group;
    using unit_group_=shared_ptr<unit_group>;
    struct waterway;
    using waterway_=shared_ptr<waterway>;
    struct gate;
    using gate_=shared_ptr<gate>;
    struct catchment;
    using catchment_=shared_ptr<catchment>;
    struct power_plant;
    using power_plant_=shared_ptr<power_plant>;
    struct reservoir_aggregate;
    using reservoir_aggregate_=shared_ptr<reservoir_aggregate>;
    struct optimization_summary;
    using optimization_summary_ = shared_ptr<optimization_summary>;
    /** @brief stm hydro_power_system 
     *
     * Is the same as the core hydro_power_system, 
     * but with added attributes suitable for the stm.
     * 
     * The attributes are attached to the objects (as ususal),
     * but stored in the hps_ds, the indata-set for hydro power system (a bit more sophisitcated).
     * 
     */
    struct stm_hps:hydro_power::hydro_power_system {
        using super = hydro_power::hydro_power_system;

        stm_hps();
        stm_hps(int id, const string&name);
        static string  to_blob(const shared_ptr<stm_hps>&s);
        static shared_ptr<stm_hps> from_blob(const string &xmls);
		
		vector<reservoir_aggregate_> reservoir_aggregates;
		reservoir_aggregate_ find_reservoir_aggregate_by_name(const string& name) const;
		reservoir_aggregate_ find_reservoir_aggregate_by_id(int64_t id) const;
		
        bool operator==(const stm_hps& other) const;
        bool operator!=(const stm_hps& other) const { return !(*this==other); }

        /** @brief generate an almost unique, url-like string for a hydro_power_system.
         *
         * @param rbi: back inserter to store result
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels=-1, int template_levels = -1) const;
        x_serialize_decl();
    };
    using stm_hps_ = shared_ptr<stm_hps>;
    using stm_hps__ = weak_ptr<stm_hps>;


    struct stm_rule_exception:logic_error {
        stm_rule_exception(const string& why):logic_error(why){}
    };

    /** @brief A builder that ensure building rules are followed 
     * 
     * The idea here is to provid functions that build a system that
     * is verified as it is built, including uniqueue identifiers and naming
     * for each individual component.
     * 
     * Primary use is on the python-api -side, but it could be useful on
     * c++ side since it enforces one set of rules.
     */
    struct stm_hps_builder {
        stm_hps_ s;
        explicit stm_hps_builder(const stm_hps_&s):s{s}{}
        catchment_ create_catchment(int id,const string&name,const string &json);
      	reservoir_ create_reservoir(int id,const string&name,const string &json);
		reservoir_aggregate_ create_reservoir_aggregate(int id,const string&name,const string &json);
        unit_ create_unit(int id,const string&name,const string &json);
        waterway_ create_waterway(int id,const string&name,const string &json);
        gate_ create_gate(int id,const string&name,const string &json);
        power_plant_ create_power_plant(int id,const string&name,const string &json);
        waterway_ create_tunnel(int id,const string&name,const string &json) {return create_waterway(id,name,json);}
        waterway_ create_river(int id,const string&name,const string &json) {return create_waterway(id,name,json);}
    };

    /** @brief stm system
     *
     * Contains all needed components to describe a stm system.
     * This is basically the energy_market_model, but tuned to the
     * short term optimization processes and models.
     *
     *
     *
     *
     * 
     * The stm system model contains 
     * 
     *  * (stm) hydro-power-systems (one or more)
     *  * (energy) market-price-areas that contains the hydro-power-systems
     *  * other groups, or kind of market, like frequency or frequency-reserve markets
     *  
     */
    struct stm_system:id_base {
        using super=id_base;
        BOOST_HANA_DEFINE_STRUCT(stm_system,
            (vector<stm_hps_>,hps),
            (vector<energy_market_area_>,market),
            (run_parameters,run_params),
            (vector<unit_group_>,unit_groups),
            (optimization_summary_, summary)
        );

        stm_system();
        stm_system(int id,string name, string json);
        unit_group_ add_unit_group(int id,string name, string json, int group_type=0);
        void set_summary(optimization_summary_ const&x);
        static string  to_blob(const shared_ptr<stm_system>&s);
        static shared_ptr<stm_system> from_blob(const string &xmls);
        static shared_ptr<stm_system> clone_stm_system(const shared_ptr<stm_system>& s);

        /** @brief generate an almost unique, url-like string for an stm system.
         *
         * @param rbi: back inserter to store result
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels=-1, int template_levels = -1) const ;
        x_serialize_decl();
    };
    using stm_system_=shared_ptr<stm_system>;
    using stm_system__=weak_ptr<stm_system>;



}
x_serialize_export_key(shyft::energy_market::stm::stm_hps);
x_serialize_export_key(shyft::energy_market::stm::stm_system);

BOOST_CLASS_VERSION(shyft::energy_market::stm::stm_hps, 3);
BOOST_CLASS_VERSION(shyft::energy_market::stm::stm_system, 2);
