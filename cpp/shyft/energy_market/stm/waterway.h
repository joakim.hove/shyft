#pragma once
#include <map>
#include <string>
#include <stdexcept>

#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/apoint_ts.h>

#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/url_fx.h>

namespace shyft::energy_market::stm {

    using std::string;
    using std::shared_ptr;
    using std::runtime_error;
    using std::dynamic_pointer_cast;
    using shyft::core::utctime;
    using shyft::time_series::dd::apoint_ts;


    struct gate;
    using gate_ = shared_ptr<gate>;

    struct waterway;
    using waterway_ = shared_ptr<waterway>;

    /** @brief a water_route, tunnel or river 
        *
        */
    struct waterway:hydro_power::waterway {
        using super=hydro_power::waterway;

        /** @brief generate an almost unique, url-like string for a water route.
         *
         * @param rbi: back inserter to store result
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels=-1, int template_levels = -1) const;
        
        waterway(int id, const string& name, const string& json, stm_hps_ &hps):super(id,name,json,hps) {mk_url_fx(this);}
        waterway(){mk_url_fx(this);}

        bool operator==(const waterway& other) const;
        bool operator!=(const waterway& other) const { return !( *this == other); };

        // static gate_ add_gate(waterway_ const& w, int id, const string& name, const string& json);
        // static gate_ add_gate(waterway_ const& w, gate_ const& g);


        struct geometry_ {
            url_fx_t url_fx;// needed by .url(...) to python exposure
            BOOST_HANA_DEFINE_STRUCT(geometry_,
                (apoint_ts,length),  ///< m, the length of the waterway, the ts reflect that it can change
                (apoint_ts,diameter),///< m, the diameter of typically a tunnel
                (apoint_ts,z0),      ///< masl at first end
                (apoint_ts,z1)       ///< masl at second end
            );
        };

        struct discharge_ {

            struct constraint_ {
                url_fx_t url_fx;
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                                         (apoint_ts, min),
                                         (apoint_ts, max),
                                         (apoint_ts, ramping_up),
                                         (apoint_ts, ramping_down),
                                         (apoint_ts, accumulated_min),
                                         (apoint_ts, accumulated_max)
                );
            };

            struct penalty_ {

                struct cost_ {
                    url_fx_t url_fx;
                    BOOST_HANA_DEFINE_STRUCT(cost_,
                                             (apoint_ts, constraint_min),
                                             (apoint_ts, constraint_max),
                                             (apoint_ts, ramping_up),
                                             (apoint_ts, ramping_down),
                                             (apoint_ts, accumulated_min),
                                             (apoint_ts, accumulated_max)
                    );
                };

                struct result_ {
                    url_fx_t url_fx;
                    BOOST_HANA_DEFINE_STRUCT(result_,
                                             (apoint_ts, constraint_min),
                                             (apoint_ts, constraint_max),
                                             (apoint_ts, ramping_down),
                                             (apoint_ts, ramping_up),
                                             (apoint_ts, accumulated_min),
                                             (apoint_ts, accumulated_max)
                    );
                };

                url_fx_t url_fx;
                BOOST_HANA_DEFINE_STRUCT(penalty_,
                                         (cost_, cost),
                                         (result_, result)
                );
            };

            url_fx_t url_fx;// needed by .url(...) to python exposure
            BOOST_HANA_DEFINE_STRUCT(discharge_,
                (apoint_ts, schedule), // m3/s scheduled flow
                (apoint_ts, reference),// m3/s constraint reference, acc-dev, could be different from schedule
                (apoint_ts, static_max),// m3/s abs. value, both directions
                (apoint_ts, result),    // m3/s as in result from optimization/simulation
                (apoint_ts, realised),  // m3/s as in best estimated/measured/observed historical fact
                (constraint_, constraint),
                (penalty_, penalty)
            );
        };
    
        BOOST_HANA_DEFINE_STRUCT(waterway,
            (apoint_ts,head_loss_coeff),   ///< m/(m3/s)**2 head_loss = q*abs(q)*head_loss_coeff
            (t_xyz_list_,head_loss_func),  ///< head_loss= f(flow,delta_h) for complex head-loss, using table lookup
            (t_xy_, delay),
            (geometry_,geometry),
            (discharge_,discharge)
        );
        
        
        x_serialize_decl();
    };


    /** @brief Gate or,  Hatch, - controls flow into water-routes
    *
    * Gate's are active/passive elements of the hydro-power-system.
    * They can be controlled, some are binary (open/closed), typically tunnel-gates.
    * Gates for flood/bypass and be operated from closed up to full opening in steps,
    * to fulfil a certian plan or requirement.
    * Passive gates, are gates where the water flows when above certain level, typically for flooding
    * reservoirs etc.
    *
    * There can be several gates into a water-route, and they can be operated separately.
    * 
    * gate-plans (or requirements), can be positional, or by wanted flow.
    *  For positional gate-plans (cm, m, % of max opening etc.), this influences the gate-opening
    *  and the other factors, such as water-level up/down-stream determines the resulting flow.
    *  For flow-based gate-plan,(m3/s), - it's the other way around, the system would then
    *  try to figure out what gate-position (if any) that would give the wanted flow.
    *
    */
    struct gate : hydro_power::gate {
        using super = hydro_power::gate;

        /** @brief generate an almost unique, url-like string for a gate.
         *
         * @param rbi: back inserter to store result
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels=-1, int template_levels = -1) const;

        gate(int id, const string& name, const string& json);
        gate();

        bool operator==(const gate& other) const;
        bool operator!=(const gate& other) const { return !( *this == other); };

        struct opening_ {
            struct constraint_ {
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                                         (t_xy_,positions),     ///< x: arbitrary position / y: opening factor
                                         (apoint_ts,continuous) ///< x: time / y: 0 or 1
                );
                url_fx_t url_fx;// needed by .url(...) to python exposure
            };
            url_fx_t url_fx;// needed by .url(...) to python exposure
            BOOST_HANA_DEFINE_STRUCT(opening_,
                (apoint_ts,schedule),
                (apoint_ts,realised),
                (constraint_,constraint),
                (apoint_ts,result)
            );
        };

        struct discharge_ {
            struct constraint_ {
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                                         (apoint_ts,min), ///< m3/s
                                         (apoint_ts,max)  ///< m3/s
                );
                url_fx_t url_fx;// needed by .url(...) to python exposure
            };
            url_fx_t url_fx;// needed by .url(...) to python exposure
            BOOST_HANA_DEFINE_STRUCT(discharge_,
                (apoint_ts,static_max),
                (apoint_ts,schedule),
                (apoint_ts,realised),
                (constraint_,constraint),
                (apoint_ts,merge_tolerance), ///< m3/s, Max deviation in discharge between two timesteps
                (apoint_ts,result)
            );
        };

        //url_fx_t url_fx;// needed by .url(...) to python exposure
        BOOST_HANA_DEFINE_STRUCT(gate,
            (t_xyz_list_,flow_description),
            (t_xyz_list_,flow_description_delta_h),
            (apoint_ts,cost), ///< money
            (opening_,opening),
            (discharge_,discharge)
        );

        x_serialize_decl();
    };

    ///< ref impl. for details/context
    extern double initial_acc_deviation(utctime t0,const apoint_ts& actual, const apoint_ts& reference);
}

x_serialize_export_key(shyft::energy_market::stm::waterway);
x_serialize_export_key(shyft::energy_market::stm::gate);
