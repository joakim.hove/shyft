#pragma once
#include <vector>
#include <string>
#include <memory>
#include <map>
#include <type_traits>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/time_series/dd/apoint_ts.h>

#include <shyft/energy_market/constraints.h>

namespace shyft::energy_market::stm {
	using std::shared_ptr;
	using std::vector;
	using std::map;
	using std::string;
	using std::pair;

	using shyft::core::utctime;
	using shyft::core::utctimespan;
    using shyft::core::calendar;
	using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::POINT_AVERAGE_VALUE;
    using shyft::time_axis::generic_dt;

    using core::absolute_constraint;
    using core::penalty_constraint;

	using hydro_power::xy_point_curve_;
	using hydro_power::xyz_point_curve_;
    using xyz_point_curve_list=vector<hydro_power::xy_point_curve_with_z>;
    using xyz_point_curve_list_ = shared_ptr<xyz_point_curve_list>;
	using hydro_power::turbine_description_;

	template<typename T> using t_T = shared_ptr<map<utctime,T>>;///< all attributes are time-dependent (as in 'from that time, the value of this attribute should be...')
    
    //-- these are the basic types (in addition to the already existing value-types from time_series library)
	using t_xy_ = t_T<xy_point_curve_>; ///< given t: y=f(x)
	using t_xyz_ = t_T<xyz_point_curve_>; ///< given t: y=f(x,z)
	using t_xyz_list_ = t_T<xyz_point_curve_list_>;///< given t: give z, -> xy
	using t_turbine_description_ = t_T<turbine_description_>;

    // -- Comparison operators for above types (note that the operators are specific to this namespace)
    //    There could potentially be some issues because this == is called instead of std::operator==.
    //    More testing needed.
    //
    //    Attribute comparison could be simplified if some of the shared pointers can be dropped. 
    namespace impl {

        template<typename T, typename = void>
        struct has_equality : std::false_type {};

        template<typename T>
        struct has_equality<T, std::void_t<decltype(std::declval<T&>()==std::declval<T&>())>> : std::true_type {};

        template<typename T, typename=std::enable_if_t<has_equality<T>::value>>
        bool operator==(const std::shared_ptr<T>& lhs, const std::shared_ptr<T>& rhs) {
            // check ptr contents if both are valid
            return std::operator==(lhs, rhs) || ( (lhs && rhs) && (*lhs == *rhs) );
        }

        template<typename T, typename=std::enable_if_t<has_equality<T>::value>>
        bool operator!=(const std::shared_ptr<T>& lhs, const std::shared_ptr<T>& rhs) {
            return !operator==(lhs, rhs);
        }

        template<typename T, typename=std::enable_if_t<has_equality<T>::value>>
        bool operator==(const std::vector<std::shared_ptr<T>>& lhs, const std::vector<std::shared_ptr<T>>& rhs) {
            return (lhs.size() == rhs.size()) && std::equal(lhs.cbegin(), lhs.cend(), rhs.cbegin(),
                [](const auto& lhs, const auto& rhs) { return std::operator==(lhs, rhs) || ((lhs && rhs) && *lhs == *rhs); });
        }

        template<typename T, typename=std::enable_if_t<has_equality<T>::value>>
        bool operator!=(const std::vector<std::shared_ptr<T>>& lhs, const std::vector<std::shared_ptr<T>>& rhs) {
            return !operator==(lhs, rhs);
        }

        template<typename K, typename V, typename=std::enable_if_t<has_equality<V>::value>>
        bool operator==(const std::map<K, V>& lhs, const std::map<K, V>& rhs) {
            // check values with == as defined above
            return std::operator==(lhs, rhs) || ( (lhs.size() == rhs.size())
                && std::all_of(lhs.begin(), lhs.end(), [&rhs](const auto& a) { return rhs.count(a.first) && rhs.at(a.first) == a.second; })
            );
        }

        template<typename K, typename V, typename=std::enable_if_t<has_equality<V>::value>>
        bool operator!=(const std::map<K, V>& lhs, const std::map<K, V>& rhs) {
            return !operator==(lhs, rhs);
        }

    }

    // Comparison function stm attributes
    // TODO: We need to be able to compare unbound time series
    template<typename T>
    inline bool equal_attribute(const T& lhs, const T& rhs) {
        using impl::operator==;
        return lhs == rhs;
    }
}
