/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/em_utils.h>

namespace shyft::energy_market::hydro_power {

    waterway_ const& waterway::input_from( waterway_ const&me,waterway_ const& w) {
        connect(w->shared_from_this(), me->shared_from_this()); 
        return me; 
    }
            
    waterway_ const& waterway::input_from( waterway_ const& me,unit_ const& p) {
        connect(p->shared_from_this(), me->shared_from_this()); 
        return me; 
    }
            
    waterway_ const& waterway::input_from( waterway_ const& me, reservoir_ const& r, connection_role role) {
        connect(r->shared_from_this(),role,me->shared_from_this());
        return me; 
    }
            
    waterway_ const& waterway::input_from( waterway_ const& me, reservoir_ const& r) {
        connect(r->shared_from_this(),connection_role::main, me->shared_from_this());
        return me;
    }
            
    waterway_ const& waterway::output_to( waterway_ const& me, waterway_ const& w) {
        connect(me->shared_from_this(), w->shared_from_this());
        return me;
    }
            
    waterway_ const& waterway::output_to( waterway_ const& me, reservoir_ const& r) {
        connect(me->shared_from_this(), r->shared_from_this()); 
        return me; 
    }

    waterway_ const& waterway::output_to( waterway_ const& me, unit_ const& p) {
        connect(me->shared_from_this(), p->shared_from_this());
        return me;
    }

    connection_role waterway::upstream_role() {
        if (upstreams.size() == 0)
            throw std::runtime_error("Waterway has no upstream connections");
        auto upstream = upstreams[0].target;
        for (auto &connection : upstream->downstreams) {
            if (connection.target->id == id) {
                return connection.role;
            }
        }
        throw  std::runtime_error("Waterway has inconsistent upstream connnection");
    }
    
    bool waterway::operator==( waterway const& o)const {
        if(this==&o) return true;//equal by addr.
        if (!id_base::operator==(o))
            return false;

        return equal_structure(o) && std::is_permutation(begin(gates),end(gates),begin(o.gates),end(o.gates),[](auto const&ca,auto const&cb){return ca == cb || (ca&&cb && (*ca==*cb));});
    }
    waterway_ waterway::shared_from_this() const {
        auto hps=hps_();
        return hps?shared_from_me(this,hps->waterways):nullptr;
    }
    
    void waterway::add_gate( waterway_ const &ww, gate_ const& g) {
        auto w=ww->shared_from_this();
        if (g && g->wtr_()) {
            throw std::runtime_error("This gate is already part of '" + g->wtr_()->name + "', remove the gate from that oject first");
        }
        auto f = find(begin(w->gates), end(w->gates), g);
        if (f == end(w->gates)) {
            g->wtr = w; // uplink ref. here
            w->gates.push_back(g);
        }
    }
    void waterway::remove_gate_ptr(gate*g) {
        if (!g)return;
        auto f = find_if(begin(gates), end(gates), [g](const auto&x) {return x.get() == g; });
        if (f != end(gates)) {
            g->wtr.reset();// remove uplink ref. here.
            gates.erase(f);
        }
    }

    gate_ gate::shared_from_this() const {
        auto wtr = wtr_();
        return wtr ? shared_from_me(this, wtr->gates) : nullptr;
    }

    waterway::~waterway() {
        for (auto&g : gates) // remove uplink to this prior to vector delete
            g->wtr.reset();
    }

    gate::gate() = default;
    gate::~gate() {
    }

    gate::gate(int id, const string&name, const string&json)
        :id_base{ id, name, json,{}} {

    }
}
