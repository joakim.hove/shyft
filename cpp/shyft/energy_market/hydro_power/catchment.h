/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <memory>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/id_base.h>

namespace shyft::energy_market::hydro_power {

using std::string;
using std::shared_ptr;
using std::weak_ptr;
using std::make_shared;

//fwd:
struct hydro_power_system;
using hydro_power_system_=shared_ptr<hydro_power_system>;
using hydro_power_system__=weak_ptr<hydro_power_system>;

/** @brief A catchment in context stm
*
* A catchment in this context is a water-source, that injects water into reservoirs/creeks
* The catchment runoff [m3/s], is a f(t), projected to the reservoirs/creeks.
* In near future the runoff is known, but due to weather and catchment model uncertainity,
* the variations after a few days could be quite large.
* It is common to represent this uncertainity as a set of scenarios.
* Important to notice that the price-forecast is also influenced by the same set of scenarios in
* a hydro-dominated market. So take care to ensure that prices and inflow forecasts are synchronized.
*/
struct catchment:id_base {
	hydro_power_system__ hps;

	catchment() =default;
	catchment(int id,const string& name,const string&json, hydro_power_system_ a_hps ):id_base{id,name,json,{}},hps(a_hps) {}

	virtual ~catchment(); // Make catchment polymorphic

	//python access hydro_power_system
	hydro_power_system_ hps_() const { return hps.lock(); }
    void clear();
    bool equal_structure(const catchment&o) const{ return id==o.id;}
	bool operator==(const catchment& o) const;
	bool operator!=(const catchment&o) const { return !operator==(o); }
	x_serialize_decl();
};


typedef shared_ptr<catchment> catchment_;
typedef shared_ptr<catchment const> catchment_c;

}

x_serialize_export_key(shyft::energy_market::hydro_power::catchment);
