/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <stdexcept>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <boost/format.hpp>
#include <shyft/energy_market/em_utils.h>

namespace shyft::energy_market::hydro_power {

    using std::runtime_error;
    using std::dynamic_pointer_cast;

    unit_ const& unit::input_from(unit_ const&me, const waterway_& w) {
        connect(w->shared_from_this(), me->shared_from_this());
        return me;
    }
    unit_ const& unit::output_to(unit_ const&me, const waterway_& w) {
        connect(me->shared_from_this(), w->shared_from_this());
        return me;
    }
    bool unit::is_pump() {
        return name.find("pump") != std::string::npos;
    }
    waterway_ unit::downstream() const {
        return downstreams.size() ? dynamic_pointer_cast<waterway>(downstreams.front().target_()) : nullptr;
    }
    waterway_ unit::upstream() const {
        return upstreams.size() ? dynamic_pointer_cast<waterway>(upstreams.front().target_()) : nullptr;
    }

    unit_ unit::shared_from_this() const {
        auto hps=hps_();
        return hps?shared_from_me(this,hps->units):nullptr;
    }
    
    power_plant_ power_plant::shared_from_this() const {
        auto hps=hps_();
        return hps?shared_from_me(this,hps->power_plants):nullptr;
    }
    
    void power_plant::add_unit(const power_plant_& ps,const unit_ &a) {
        auto pp=ps->shared_from_this();
        if(a && pp && pp->hps_() == a->hps_()) {
            auto f = find(begin(pp->units), end(pp->units), a);
            if(f!=end(pp->units))
                throw runtime_error("unit  '" + a->name + "'already added to power-plant '"+pp->name);
            if(a->pwr_station_())
                throw runtime_error("unit  '" + a->name + "'already added to anoter power-plant '"+a->pwr_station_()->name);
            pp->units.push_back(a);
            a->pwr_station=pp;            
        } else {
            throw runtime_error("power_plant '"+string(ps ?ps->name:string("null"))
                   +"' and unit '"+string(a?a->name:string("null")) 
                   + "' must be non-null objects, and both belong to the same existing hydro-power-system :"
                + string(ps&&ps->hps_()?ps->hps_()->name:string("null")));
        }
    }
    void power_plant::remove_unit(const unit_& a) {
        auto f= find_if(begin(units),end(units),[&a](const auto&p)->bool {return a==p;});
        if(f !=end(units)) {
            (*f)->pwr_station.reset();
            units.erase(f);
        }
    }

    bool unit::operator==(unit const& o) const {
        return this==&o ||(id_base::operator==(o) && equal_structure(o));// same attributes and closure/neighbourhood
    }
    bool power_plant::operator==( power_plant const& o) const {
        if(this==&o)return true;
        if (!id_base::operator==(o))
            return false;
        return equal_structure(o);
    }
    bool power_plant::equal_structure(power_plant const&o) const {
        if(id != o.id) return false;// we insist on same object id to be structurally equal.
        //notice that we do not take the depth of units here, only that it points to same units
        // which correspond to the rule of limit to close neighbour objects
        return std::is_permutation(begin(units),end(units),begin(o.units),end(o.units)
            ,[](auto const&ca, auto const&cb) {return ca && cb && ca->id==cb->id;});
    }

    power_plant::~power_plant(){
        for(auto&a:units)
            if(a) a->pwr_station.reset();
    }

}
