/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/mp.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/url_fx.h>

namespace shyft::energy_market::core {
    using shyft::time_series::dd::apoint_ts;
    namespace hana=boost::hana;
    /** @brief Base class for data structure binding
     * data related to constraints together.
     */
    struct constraint_base {
        x_serialize_decl();
    };


    /** @brief Class for absolute constraint, where not satisfying the constraint incurs an infinite penalty
     *  This class requires no other data than that from constraint_base
     */
    struct absolute_constraint: constraint_base {
        using super=constraint_base;
        url_fx_t url_fx;// needed to link up py wrapped url paths
        BOOST_HANA_DEFINE_STRUCT(absolute_constraint,
            (apoint_ts,limit), ///< Time series for the threshold of the constraint (the limit for whether a value satisfies a constraint or not)
            (apoint_ts,flag) ///< Time series specifying when the constraint is active.
        );
        bool operator==(const absolute_constraint & o) const {
            return limit==o.limit &&  flag==o.flag ;
        }

        bool operator!=(const absolute_constraint & o) const {
            return !operator==(o);
        }
        absolute_constraint() = default;
        absolute_constraint(const apoint_ts& alimit, const apoint_ts& aflag):limit(alimit),flag(aflag) {}
        x_serialize_decl();
    };

    /** @brief Data class for a constraint, where violating the constraint incurs a finite cost.
     */
    struct penalty_constraint: constraint_base {
        using super = constraint_base;
        url_fx_t url_fx;// needed to link up py wrapped url paths
        BOOST_HANA_DEFINE_STRUCT(penalty_constraint,
            (apoint_ts,limit), ///< Time series for the threshold of the constraint (the limit for whether a value satisfies a constraint or not)
            (apoint_ts,flag),  ///< Time series specifying when the constraint is active.
            (apoint_ts,cost),  ///< the cost for violating the constraint [money]
            (apoint_ts,penalty)///< the penalty pr. unit violation [money/violation-unit]
        );

        penalty_constraint() = default;
        penalty_constraint(const apoint_ts& alimit, const apoint_ts& aflag,
            const apoint_ts& acost, const apoint_ts& apenalty):
            limit{alimit},flag{aflag}, cost{acost}, penalty{apenalty} {}

        bool operator==(const penalty_constraint & o) const {
            return limit==o.limit && flag==o.flag && cost == o.cost && penalty == o.penalty;
        }

        bool operator!=(const penalty_constraint & o) const {
            return !operator==(o);
        }

        x_serialize_decl();
    };

}

x_serialize_export_key(shyft::energy_market::core::constraint_base);
x_serialize_export_key(shyft::energy_market::core::absolute_constraint);
x_serialize_export_key(shyft::energy_market::core::penalty_constraint);
