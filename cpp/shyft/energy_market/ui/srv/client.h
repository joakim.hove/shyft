#pragma once
#include <string>
#include <memory>
#include <shyft/srv/msg_defs.h>
#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/srv/client.h>
#include <shyft/srv/model_info.h>
#include <shyft/core/core_archive.h>
#include <dlib/iosockstream.h>


namespace shyft::energy_market::ui::srv {
    using std::shared_ptr;
    using std::string;
    using std::to_string;

    using shyft::energy_market::ui::layout_info;
    using shyft::srv::model_info;
    using shyft::srv::message_type;
    using shyft::srv::msg;

    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using shyft::core::srv_connection;
    using shyft::core::scoped_connect;
    using shyft::core::do_io_with_repair_and_retry;

    struct config_client : shyft::srv::client<layout_info> {
        using super = shyft::srv::client<layout_info>;
        config_client() = delete;
        config_client(string host_port, int timeout_ms=1000): super{host_port, timeout_ms} {}
        /** @brief read model from server
         * If args and name are given, will try to generate new layout
         * if unable to find layout with provided  ID.
         *
         * @param mid: Model ID to read
         * @param name: Name of layout to generate
         * @param args: Arguments to generate layout (If not given, uses defaults)
         * @return read, or generated, layout.
         */
        shared_ptr<layout_info> read_model_with_args(int64_t mid, const string& layout_name, const string& args, bool store_layout = false) {
            scoped_connect sc(c);
            shared_ptr<layout_info> r;
            do_io_with_repair_and_retry(c, [&mid, &layout_name, &args, &r, store_layout](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::MODEL_READ_ARGS, io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid << layout_name << args << store_layout;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::MODEL_READ_ARGS) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response: ") + to_string((int)response_type));
                }
            });
            return r;
        }
    };
}
