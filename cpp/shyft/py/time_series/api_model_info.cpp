/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>

#include <shyft/srv/model_info.h>
namespace py=boost::python;

namespace expose {
    using shyft::srv::model_info;
    using namespace shyft::core;
    using std::string;
    using std::vector;

    void ex_model_info () {
            using py::self;
            py::class_<model_info>("ModelInfo",
                doc_intro("Provides model-information useful for selection and filtering")
            )
            .def(py::init<int64_t,string const&,utctime,string>(
                (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("created"),py::arg("json")=string{""}))
            )
            .def_readwrite("id",&model_info::id,"the unique model id, can be used to retrieve the real model")
            .def_readwrite("name",&model_info::name,"any useful name or description")
            .def_readwrite("created",&model_info::created,"the time of creation, or last modification of the model")
            .def_readwrite("json",&model_info::json,"a json formatted string to enable scripting and python to store more information")
            .def(self==self)
            .def(self!=self)
            ;
            using ModelInfoVector=vector<model_info>;
            py::class_<ModelInfoVector>("ModelInfoVector", "A strongly typed list, vector, of ModelInfo")
            .def(py::vector_indexing_suite<ModelInfoVector, true>())
            ;
    }
}
