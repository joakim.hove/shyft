/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>

#include  <shyft/hydrology/methods/radiation.h>
#include <armadillo>

namespace expose {

    void radiation() {
        using namespace shyft::core::radiation;
        using namespace boost::python;
        namespace py = boost::python;

        class_<parameter>("RadiationParameter")
            .def(init<double,double>(args("albedo","turbidity"),"a new object with specified parameters"))
            .def_readwrite("albedo",&parameter::albedo,"typical value 0.2")
            .def_readwrite("turbidity", &parameter::turbidity,"typical value 1.0")
            ;

        class_<response>("RadiationResponse")
            .def_readwrite("sw_cs_p",&response::sw_cs_p)
            .def_readwrite("sw_t",&response::sw_t)
            .def_readwrite("net_sw",&response::net_sw)
            .def_readwrite("net_lw",&response::net_lw)
            .def_readwrite("net",&response::net)
            .def_readwrite("ra",&response::ra)
            ;

        typedef calculator RadiationCalculator;
        class_<RadiationCalculator>("RadiationCalculator",
            doc_intro(
                "Radiation,R,\n"
                "\n"
                "reference ::\n\n"
                "    Allen, R. G.; Trezza, R. & Tasumi, M. Analytical integrated functions for daily solar radiation on slopes Agricultural and Forest Meteorology, 2006, 139, 55-73)\n"
                "    primitive implementation for calculating predicted clear-sky short-wave solar radiation for inclined surfaces\n"
                "\n\n"
                "This function is plain and simple, taking albedo and turbidity\n"
                "into the constructor and provides 2 functions:\n\n"
                " * net_radiation calculates predicted solar radiation (if no measured data available) or translates measured data into the slope plus adds the longwave radiation: instantaneously;\n"
                " * net_radiation_step calculates predicted solar radiation or/and translates measured horizontal radiation into sloped surface for the time period between tstart and tend plus adds the lw radiation\n"
                "\n"
                "Recommended usage is the net_radiation_step for 24h-steps; it was also tested with 1h and 3h steps"
            )
                ,no_init
            )
            .def(init<const parameter&>(args("param"),"create a calculator using supplied parameter"))
            .def("net_radiation_inst",&RadiationCalculator::net_radiation_inst,(py::arg("self"),py::arg("response"), py::arg("latitude"), py::arg("t"), py::arg("slope"), py::arg("aspect"), py::arg("temperature"), py::arg("rhumidity"), py::arg("elevation"),  py::arg("rsm")),
                     doc_intro("calculates net radiation using instantaneous method, updating response")
            )
            .def("net_radiation_step",&RadiationCalculator::net_radiation_step,(py::arg("self"),py::arg("response"), py::arg("latitude"), py::arg("t1"),py::arg("dt"), py::arg("slope"), py::arg("aspect"), py::arg("temperature"), py::arg("rhumidity"), py::arg("elevation"),  py::arg("rsm")),
                     doc_intro("calculates net radiation, updating response")
            )
            .def("net_radiation",&RadiationCalculator::net_radiation,(py::arg("self"),py::arg("response"), py::arg("latitude"), py::arg("t1"),py::arg("dt"), py::arg("slope"), py::arg("aspect"), py::arg("temperature"), py::arg("rhumidity"), py::arg("elevation"),  py::arg("rsm")),
                     doc_intro("calculates net radiation using either step or instantaneous method, updating response")
            )
            .def("net_radiation_step_asce_st",&RadiationCalculator::net_radiation_step_asce_st,(py::arg("self"),py::arg("response"), py::arg("latitude"), py::arg("t1"),py::arg("dt"), py::arg("slope"), py::arg("aspect"), py::arg("temperature"), py::arg("rhumidity"), py::arg("elevation"),  py::arg("rsm")),
                     doc_intro("calculates net radiation, updating response")
                )
            ;
    }
}


