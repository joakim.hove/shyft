/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include  <shyft/py/api/py_convertible.h>


#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/point_ts.h>
#include <shyft/hydrology/api/api.h>
#include <shyft/dtss/geo.h>  // looking for geo_ts that we need to support native

namespace expose {
    namespace py=boost::python;
    namespace sc=shyft::core;
    namespace ts=shyft::time_series;
	using ts::dd::ats_vector;
    using ts::dd::geo_ts;
    namespace ta=shyft::time_axis;
    namespace sa=shyft::api;
    using std::vector;
    using std::shared_ptr;
    using std::make_shared;

    template <class S>
    static std::vector<double> geo_tsv_values(std::shared_ptr<std::vector<S>> const &geo_tsv, sc::utctime t) {
        std::vector<double> r;
        if (geo_tsv) {
            r.reserve(geo_tsv->size());
            for (auto const &gts : *geo_tsv)
                r.push_back(gts.ts(t));
        }
        return r;
    }

    template<class T>
    static std::vector<T> create_from_geo_and_tsv(const std::vector<sc::geo_point>& gpv, const ats_vector& tsv) {
        if(gpv.size()!=tsv.size())
            throw std::runtime_error("list of geo-points and time-series must have equal length");
        std::vector<T> r;r.reserve(tsv.size());
        for(size_t i=0;i<tsv.size();++i)
            r.emplace_back(gpv[i],tsv[i]);
        return r;
    }

    /** simple extension to support construct from python list
     * Since py-api register convertible only works for non-shared ptr classes.
     */
    struct geo_tsv_extension {

        template <class T>
   		static shared_ptr<vector<T>> create_from_list(py::list tsl) {
			if(py::len(tsl)==0)
				return make_shared<vector<T>>();
			auto r = make_shared<vector<T>>();
			size_t n = py::len(tsl);
			r->reserve(py::len(tsl));
			for(size_t i=0;i<n;++i) {
				py::object oi = tsl[i];
				py::extract<T> xtract_gts(oi);
				if (xtract_gts.check()) {
					r->push_back(xtract_gts());
				} else {
                    py::extract<geo_ts> xgts(oi);
                    if(xgts.check()) {
                         r->emplace_back(T{xgts()});
                    } else {
                        throw std::runtime_error(std::string("failed to convert ") + std::to_string(i) + " element to xxxSource");
                    }
				}
			}
			return r;
        }
        
        template <class T>
        static shared_ptr<vector<T>> create_from_geo_tsv(vector<geo_ts> const&gtsv) {
			if(gtsv.size()==0)
				return make_shared<vector<T>>();
			auto r = make_shared<vector<T>>();
            r->reserve(gtsv.size());
            for(auto const & gts:gtsv)
                r->emplace_back(T{gts});
            return r;
        }
        
        template <class T> 
        static vector<geo_ts> to_geo_tsv(vector<T> const &tsv) {
            vector<geo_ts> r;r.reserve(tsv.size());
            for(auto const&ts:tsv)
                r.emplace_back(ts.get_geo_ts());
            return r;
        }
    };

    template<class T>
    static void GeoPointSourceX(const char *py_name,const char *py_vector,const char *py_doc) {
        py::class_<T,py::bases<sa::GeoPointSource>>(py_name,py_doc)
            .def(py::init<const sc::geo_point&,const sa::apoint_ts &>((py::arg("midpoint"),py::arg("ts"))))
            .def(py::init<const geo_ts&>( (py::arg("geo_ts"))  ) )
            ;
        typedef std::vector<T> TSourceVector;
        py::class_<TSourceVector,py::bases<>,std::shared_ptr<TSourceVector> > (py_vector)
            .def("__init__", py::make_constructor(&geo_tsv_extension::template create_from_list<T>,
				py::default_call_policies(),
               (py::arg("geo_ts_list"))
				),
				doc_intro("Construct from list")
			)
            .def("__init__", py::make_constructor(&geo_tsv_extension::template create_from_geo_tsv<T>,
				py::default_call_policies(),
               (py::arg("geo_ts_vector"))
				),
				doc_intro("Construct from a GeoTimeSeriesVector")
			)
            .def(py::vector_indexing_suite<TSourceVector>())
            .def(py::init<const TSourceVector&>(py::args("src"),"clone src"))
            .def("from_geo_and_ts_vector",&create_from_geo_and_tsv<T>,(py::arg("geo_points"), py::arg("tsv")),
                doc_intro("Create from a geo_points and corresponding ts-vectors")
                doc_parameters()
                doc_parameter("geo_points","GeoPointVector","the geo-points")
                doc_parameter("tsv","TsVector","the corresponding time-series located at corresponding geo-point")
                doc_returns("src_vector","SourceVector","a newly created geo-located vector of specified type")
            )
            .staticmethod("from_geo_and_ts_vector")
            .add_property("geo_tsvector",&geo_tsv_extension::template to_geo_tsv<T>,
                doc_intro("returns a GeoTimeSeriesVector containing GeoTimeSeries where the geo-points are copied, but time-series are by-reference")
            )
            .def(py::self==py::self)
            .def(py::self!=py::self)
            ;
        //not supported for shared_ptr, leaks mem etc: py_api::iterable_converter().from_python<TSourceVector>();

        py::def("compute_geo_ts_values_at_time", &geo_tsv_values<T>, py::args("geo_ts_vector", "t"),
            doc_intro("compute the ts-values of the GeoPointSourceVector type for the specified time t and return DoubleVector")
            doc_parameters()
            doc_parameter("geo_ts_vector", "GeoPointSourceVector", "Any kind of GeoPointSource vector")
            doc_parameter("t", "int", "timestamp in utc seconds since epoch")
            doc_returns("values", "DoubleValue", "List of extracted values at same size/position as the geo_ts_vector")
        );
    }

    static void GeoPointSource(void) {
        typedef std::vector<sa::GeoPointSource> GeoPointSourceVector;

        py::class_<sa::GeoPointSource>("GeoPointSource",
            "GeoPointSource contains common properties, functions\n"
            "for the point sources in Shyft.\n"
            "Typically it contains a GeoPoint (3d position), plus a time-series\n"
            )
            .def(py::init<const sc::geo_point&,const sa::apoint_ts&>( (py::arg("midpoint"),py::arg("ts"))  ) )
            .def(py::init<const geo_ts&>( (py::arg("geo_ts"))  ) )
            .def_readwrite("mid_point_",&sa::GeoPointSource::mid_point_,"reference to internal mid_point")
            .def("mid_point",&sa::GeoPointSource::mid_point,(py::arg("self")),"returns a copy of mid_point")
            // does not work  yet:.def_readwrite("ts",&sa::GeoPointSource::ts)
            .add_property("ts",&sa::GeoPointSource::get_ts,&sa::GeoPointSource::set_ts)
			.def_readwrite("uid",&sa::GeoPointSource::uid,"user specified identifier, string")
            .add_property("geo_ts",&sa::GeoPointSource::get_geo_ts,
                doc_intro("returns a GeoTimeSeries  where the geo-point are copied, but time-series are by-reference")
            )
            .def(py::self==py::self)
            .def(py::self!=py::self)
            
            ;

        py::class_<GeoPointSourceVector,py::bases<>,std::shared_ptr<GeoPointSourceVector> > ("GeoPointSourceVector")
            .def(py::vector_indexing_suite<GeoPointSourceVector>())
            .def(py::init<const GeoPointSourceVector&>(py::args("src"),"clone src"))
            .def(py::self==py::self)
            .def(py::self!=py::self)
            ;
        py::def("compute_geo_ts_values_at_time", &geo_tsv_values<sa::GeoPointSource>, py::args("geo_ts_vector", "t"),
            doc_intro("compute the ts-values of the GeoPointSourceVector for the specified time t and return DoubleVector")
            doc_parameters()
            doc_parameter("geo_ts_vector","GeoPointSourceVector","Any kind of GeoPointSource vector")
            doc_parameter("t","int","timestamp in utc seconds since epoch")
            doc_returns("values","DoubleValue","List of extracted values at same size/position as the geo_ts_vector")
            );

        //py::register_ptr_to_python<std::shared_ptr<sa::GeoPointSource>>();
        GeoPointSourceX<sa::TemperatureSource>("TemperatureSource","TemperatureSourceVector","geo located temperatures[deg Celcius]");
        GeoPointSourceX<sa::PrecipitationSource>("PrecipitationSource","PrecipitationSourceVector","geo located precipitation[mm/h]");
        GeoPointSourceX<sa::WindSpeedSource>("WindSpeedSource","WindSpeedSourceVector","geo located wind speeds[m/s]");
        GeoPointSourceX<sa::RelHumSource>("RelHumSource","RelHumSourceVector","geo located relative humidity[%rh], range 0..1");
        GeoPointSourceX<sa::RadiationSource>("RadiationSource","RadiationSourceVector","geo located radiation[W/m2]");
    }

    static void a_region_environment() {
        //SiH: Here I had trouble using def_readwrite(), the getter was not working as expected, the setter did the right thing
        //     work-around was to use add_property with explicit set_ get_ methods that returned  shared_ptr to vectors
        py::class_<sa::a_region_environment>("ARegionEnvironment","Contains all geo-located sources to be used by a Shyft core model")
            .add_property("temperature",&sa::a_region_environment::get_temperature,&sa::a_region_environment::set_temperature)
            .add_property("precipitation",&sa::a_region_environment::get_precipitation,&sa::a_region_environment::set_precipitation)
            .add_property("wind_speed",&sa::a_region_environment::get_wind_speed,&sa::a_region_environment::set_wind_speed)
            .add_property("rel_hum",&sa::a_region_environment::get_rel_hum,&sa::a_region_environment::set_rel_hum)
            .add_property("radiation",&sa::a_region_environment::get_radiation,&sa::a_region_environment::set_radiation)
            .def(py::self==py::self)
            .def(py::self!=py::self)
            .def("serialize",&sa::a_region_environment::serialize_to_bytes,(py::arg("self")),
                "convert ARegionEnvironment into a binary blob that later can be restored with the .deserialize(blob) method\n"
            )
            .def("deserialize",&sa::a_region_environment::deserialize_from_bytes,py::args("blob"),
               "convert a blob, as returned by .serialize() into a ARegionEnvironment"
            ).staticmethod("deserialize")
            ;
    }

    void region_environment() {
        GeoPointSource();
        a_region_environment();
    }
}
