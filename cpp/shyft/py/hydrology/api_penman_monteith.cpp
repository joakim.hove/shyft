/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>

#include  <shyft/hydrology/methods/penman_monteith.h>
#include <armadillo>

namespace expose {

    void penman_monteith() {
        using namespace shyft::core::penman_monteith;
        using namespace boost::python;
        namespace py = boost::python;

        class_<parameter>("PenmanMonteithParameter")
            .def(init<double,double,double,double,bool>(args("height_veg","height_ws","height_t", "rl","full_model"),"a new object with specified parameters"))
            .def_readwrite("height_veg", &parameter::height_t,"grass 0.15")
            .def_readwrite("height_ws", &parameter::height_ws,"typical value 2.0")
            .def_readwrite("height_t", &parameter::height_t,"typical value 2.0")
            .def_readwrite("rl", &parameter::rl,"stomatal resistance, for full PM only, 140")
            .def_readwrite("full_model", &parameter::full_model,"boolean to specify if full penman-monteith required, set to false by default, so it is asce-ewri standard")
            ;

        class_<response>("PenmanMonteithResponse")
            .def_readwrite("et_ref",&response::et_ref)
            .def_readwrite("soil_heat",&response::soil_heat)
            ;

        typedef calculator<parameter,response> PenmanMonteithCalculator;
        class_<PenmanMonteithCalculator>("PenmanMonteithCalculator",
            doc_intro(
                "Evapotranspiration model, Penman-Monteith equation, PM\n"
                "reference ::\n\n"
                "    ASCE-EWRI The ASCE Standardized Reference Evapotranspiration Equation Environmental and\n"
                "         * Water Resources Institute (EWRI) of the American Society of Civil Engineers Task Com- mittee on Standardization of\n"
                "         * Reference Evapotranspiration Calculation, ASCE, Washington, DC, Environmental and Water Resources Institute (EWRI) of\n"
                "         * the American Society of Civil Engineers Task Com- mittee on Standardization of Reference Evapotranspiration Calculation,\n"
                "         * ASCE, Washington, DC, 2005\n\n"
                "calculating reference evapotranspiration\n"
                "This function is plain and simple, taking albedo and turbidity\n"
                "into the constructor and provides function:\n"
                "reference_evapotranspiration;\n"
                 "[mm/s] units."
            )
            
                 ,no_init
                 
            )
            .def(init<const parameter&>(args("param"),"create a calculator using supplied parameter"))
            .def("reference_evapotranspiration",&PenmanMonteithCalculator::reference_evapotranspiration,(py::arg("self"),py::arg("response"), py::arg("dt"), py::arg("net_radiation"), py::arg("tempmax"), py::arg("tempmin"),py::arg("rhumidity"),  py::arg("elevation"),  py::arg("windspeed")),
                     doc_intro("calculates reference evapotranspiration (standard or full based on full_model param), updating response")
                     )
            ;
    }
}
