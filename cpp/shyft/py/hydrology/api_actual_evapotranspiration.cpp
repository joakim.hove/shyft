/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>

#include <shyft/hydrology/methods/actual_evapotranspiration.h>

namespace expose {
    using namespace shyft::core::actual_evapotranspiration;
    using namespace boost::python;
    namespace py=boost::python;

    void actual_evapotranspiration() {
        class_<parameter>("ActualEvapotranspirationParameter")
            .def(init<optional<double>>(args("ae_scale_factor"),"a new object with specified parameters"))
            .def_readwrite("ae_scale_factor",&parameter::ae_scale_factor,"typical value 1.5")
            ;
        class_<response>("ActualEvapotranspirationResponse")
            .def_readwrite("ae",&response::ae)
            ;
        def("ActualEvapotranspirationCalculate_step",
            calculate_step,
            (py::arg("water_level"),py::arg("potential_evapotranspiration"),py::arg("scale_factor"),py::arg("snow_fraction"),py::arg("dt")),
            doc_intro("actual_evapotranspiration calculates actual evapotranspiration, returning same unit as input pot.evap")
            doc_intro("based on supplied parameters")
            doc_parameters()
            doc_parameter("water_level","","[mm] water level eqvivalent in ground, ae goes to zero if ground is drying out")
            doc_parameter("potential_evapotranspiration","","[mm/x], x time-unit")
            doc_parameter("scale_factor","","typically 1.5")
            doc_parameter("snow_fraction",""," 0..1 there is to ae over snow surface, so snow_fraction 1.0, yields 0.0")
            doc_parameter("dt","","[s] timestep length, currently not part of the formula")
            doc_returns("actual evapotranspiration","","")
        );
    }
}
