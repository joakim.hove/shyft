/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/py/energy_market/py_model_client_server.h>
#include <shyft/srv/db.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/api/expose_vector.h>
#include <shyft/py/api/expose_str.h>

#include <boost/format.hpp>

namespace py=boost::python;

namespace expose {
    using namespace shyft::core;

    using shyft::time_series::dd::apoint_ts;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;
    using boost::format;

    template<>
    std::string str_(gcd_model const &o) {
        return (format("GeoCellDataModel(id=%1%,...)")%o.id).str();
    }

void def_gcd_model() {
    using gcd_model_=std::shared_ptr<gcd_model>;
    py::class_<gcd_model,py::bases<>,gcd_model_> m("GeoCellDataModel","A model identifier and a geo-cell-data vector");
    m
    .def_readwrite("id",&gcd_model::id)
    .def_readwrite("geo_cell_data",&gcd_model::gcd)
    .def(py::self==py::self)
    .def(py::self!=py::self)
    ;
    expose_str_repr(m);
    using gcd_model_vector=std::vector<gcd_model_>;
    py::class_<gcd_model_vector> mv("GeoCellDataModelVector","A strongly typed list of GeoCellDataModel's");
    mv.def(py::vector_indexing_suite<gcd_model_vector, true>());
    expose_str_repr(mv);
    detail::expose_eq_ne(mv);
    detail::expose_clone(mv);
//#expose_vector<std::shared_ptr<gcd_model>>("GeoCellDataModelVector",
//#                  "A strongly typed list of GeoCellDataModel's");
}

void gcd_client_server() {
    using model=gcd_model;
    using client_t = shyft::py::energy_market::py_client<shyft::srv::client<model>>;
    using srv_t = shyft::py::energy_market::py_server<shyft::srv::server<shyft::srv::db<model>>>;
    def_gcd_model();
    shyft::py::energy_market::expose_client<client_t>("GeoCellDataClient",
       "The client api for the shyft.hydrology geo_cell_data model service."
    );
    shyft::py::energy_market::expose_server<srv_t>("GeoCellDataServer",
        "The server-side component for the shyft.hydrology geo_cell_data model repository."
    );
}
}
