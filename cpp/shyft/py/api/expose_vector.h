/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <type_traits>
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/py_convertible.h>
#include <shyft/py/api/expose_str.h>

namespace expose {
    
namespace py=boost::python;

namespace detail {

    /* Check if a converter has been registered for a given C++ type in boost python's
     * global converter registry.
     * 
     * Checks to_python converter. There can only be one of these, any attempts to
     * register additional converters for same type will be rejected in runtime with a
     * warning ("to-Python converter for <type> already registered; second conversion
     * method ignored."). There can be multiple from_python converters, all tied to the
     * same C++ type registration as the to_python converter, but these will not be
     * considered here.
     */
    template<class T>
    static bool has_converter() {
        auto reg = py::converter::registry::query(py::type_id<T>());
        return reg && reg->m_to_python;
    }

    /* Basic expose of vector<T>, and converters for the C++ type.
     *
     * Registers global Python/C++ converters: The default to_python converter, created
     * automatically by the boost::python::class_ type creation, but also a custom
     * from_python converter, making it possible to create the C++ type from python
     * iterable types.
     * 
     * The converter registry is global, shared by all extension modules, and the
     * converters are tied to the C++ type, which means they will be applicable for any
     * exposed functions that accept this C++ type as an argument. This also means that
     * only a single set of converters should be created for each C++ type, so use
     * has_converter to check and then create_vector_class instead if already
     * registered.
     * 
     * Returns the boost python class_ type for further extension.
     * 
     * NOTE: The exposed vector type must be clonable (see expose_clone) for the
     * from_python converter to work (runtime error in Python if not).
     */
    template<class ValueType>
    static py::class_<std::vector<ValueType>>
    create_vector_class_and_converters(const char* name, const char* doc = nullptr) {
        py_api::iterable_converter().from_python<std::vector<ValueType>>();
        return py::class_<std::vector<ValueType>>(name, doc);
    }

    /* Basic expose of vector<T>.
     *
     * Suppresses registration of the default to_python converter by adding
     * boost::noncopyable argument on boost::python::class_ type creation, and skips
     * the explicit registration of the custom from_python converter.
     * 
     * If a vector of same type has been exposed with converters, these will apply to
     * all exposed types based on same C++ vector type - but all types must be clonable
     * (see expose_clone) for the from_python converter to work (runtime error in
     * Python if not).
     */
    template<class ValueType>
    static py::class_<std::vector<ValueType>, boost::noncopyable>
    create_vector_class(const char* name, const char* doc = nullptr) {
        return py::class_<std::vector<ValueType>, boost::noncopyable>(name, doc);
    }


    /* Helper function to define a clone method, a __init__ with an instance of same
     * type as argument, on exposed (vector) type.
     * 
     * NOTE: This is required to be able to create it from an iterable with the
     * from_python converter (see create_vector_class_and_converters).
     */
    template <class PyCls>
    static PyCls& expose_clone(PyCls& c) {
        return c.def(py::init<const typename PyCls::wrapped_type&>(py::args("clone"), "Create a clone."));
    }

    /* Helper function to define indexing capabilities to exposed vector, which
     * makes it appear as python list.
     * 
     * This exposes a set of methods: __len__, __getitem__, __setitem__, __delitem__,
     * __iter__, __contains__, append and extend.
     *
     * By default indexed elements have Python reference semantics and are returned by
     * proxy, this can be disabled by supplying true in the NoProxy template parameter
     * (e.g. if type already have reference semantics, such as shared_ptr).
     * 
     * NOTE: The wrapped vector's value type must have operator== implemented (compile
     * error if not).
     */
    template <class PyCls, bool NoProxy = false>
    static PyCls& expose_vector_indexing(PyCls& c) {
        return c.def(py::vector_indexing_suite<typename PyCls::wrapped_type, NoProxy>());
    }

    /* Helper function to define __eq__ and __ne__ on exposed (vector) type.
     *
     * NOTE: The wrapped vector's value type must have operator== implemented (compile
     * error if not).
     */
    template <class PyCls>
    static PyCls& expose_eq_ne(PyCls& c) {
        return c.def(py::self==py::self)
                .def(py::self!=py::self);
    }

    /* Helper function for adding a "complete" set of utility methods on an already
     * exposed vector type, common for the two different Python classes wrapping
     * vectors with or without to_python converter.
     */
    template <class PyCls>
    static PyCls& expose_vector_methods(PyCls& c, bool comparable) {
        expose_clone(c); // Expose __init__ for cloning. Required for it to be created from iterable by a from_python converter registered for the C++ type.
        expose_str_repr(c); // Expose __str__ and __repr__. Requires str_ function specialized for the vector's value_type (see expose_str.h).
        if (comparable) { // Conditional expose of features depending on operator== on wrapped type, and for wrapped vector it means also on vector's value_type.
            expose_vector_indexing(c); // Expose full set of methods that will make it appear as a regular python list.
            expose_eq_ne(c); // Expose __eq__ and __ne__.
        }
        return c;
    }
}

/** oneliner to expose vector<T>
 * 
 * Automatically handles registration of converters for the C++ type, in such a way
 * that the same type can be exposed multiple times (e.g. vector<string> can be
 * exposed from multiple extension modules).
 * 
 * Includes optional, but default, enabling of indexing capabilities, which makes
 * the exposed vector appear as a list in Python. This requires the C++ type to
 * implement operator==, which for vector also includes the vector's value_type.
 * The parameter comparable indicates if the C++ type does this, and if true
 * (the default) it will expose indexing methods as well as standard comparison
 * methods (__eq__ and __ne__).
 * 
 * @tparam ValueType the value type of the C++ vector to be exposed
 * @param name the name of the Python wrapper class
 * @param doc the optional docstring for the Python wrapper class
 * @param comparable if the vector, thus also the value type, implements operator==
 */
template<class ValueType>
static void expose_vector(const char* name, const char* doc = nullptr, bool comparable = true) {
    if (detail::has_converter<std::vector<ValueType>>()) {
        // Expose without converters since it already exists for the C++ type.
        //std::cout << "expose_vector(\"" << name << "\"): Without converters for C++ type " << typeid(ValueType).name() << " (to_python found in registry)\n";
        auto c = detail::create_vector_class<ValueType>(name, doc);
        detail::expose_vector_methods(c, comparable);
    } else {
        // Expose and create converters for the C++ type.
        //std::cout << "expose_vector(\"" << name << "\"): With converters for C++ type " << typeid(ValueType).name() << "\n";
        auto c = detail::create_vector_class_and_converters<ValueType>(name, doc);
        detail::expose_vector_methods(c, comparable);
    }
}

}
