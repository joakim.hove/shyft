#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/energy_market/py_object_ext.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/hydro_operations.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/graph/hps_graph_adaptor.h>
#include <shyft/energy_market/graph/graph_utilities.h>

namespace expose {
    namespace py=boost::python;
    using std::string;    
    void catchment_stuff() {
        using namespace boost::python;
        using namespace shyft::energy_market::hydro_power;
        class_<catchment, std::shared_ptr<catchment>, boost::noncopyable>(
            "Catchment",
            doc_intro("")
            )
            .def(init<int, const string&, const string&,hydro_power_system_ const&>(
                (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json"), py::arg("hps")), 
                doc_intro("tbd")
            )
            )
            .def_readwrite("id", &catchment::id)
            .def_readwrite("name", &catchment::name)
            .def_readwrite("json", &catchment::json)
            .def(self == self)
            .def(self != self)
            ;

        typedef std::vector<catchment_> CatchmentList;
        class_<CatchmentList>("CatchmentList")
            .def(vector_indexing_suite<CatchmentList, true>())
            ;
    }

    void water_route_stuff() {
        using namespace boost::python;
        using namespace shyft::energy_market::hydro_power;
        //note: we use pattern +[](shrd_ptr,..)->{} to fixup const issues with self (and maybe other args) passed as shrd_ptr const&(symptoms:weakptrs end up zero after return to python)
        class_<waterway, bases<hydro_component>, std::shared_ptr<waterway>, boost::noncopyable>("Waterway",
            doc_intro("The waterway can be a river or a tunnel, and connects the reservoirs, units(turbine).")
             )
            .def(init<int,const string&,const string&,hydro_power_system_ const&>((py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json"),py::arg("hps")), doc_intro("tbd")))
            .def_readonly("downstream",&waterway::downstream,doc_intro("returns downstream object(if any)"))
            .def_readonly("upstream",&waterway::upstream,doc_intro("returns upstream object(if any)"))
            .add_property("upstream_role", &waterway::upstream_role, doc_intro("the role the water way has relative to the component above"))
            .def("add_gate", +[](waterway_& ww, gate_& g)->void {waterway::add_gate(ww,g); },
                 (py::arg("self"),py::arg("gate")),doc_intro("add a gate to the waterway"))
            .def("remove_gate",&waterway::remove_gate,(py::arg("self"),py::arg("gate")),doc_intro("remove a gate from the waterway"))
            .def_readonly("gates",&waterway::gates,doc_intro("the gates attached to the inlet of the waterway"))

            .def("input_from",+[](waterway_ s,waterway_ o) {return waterway::input_from(s,o);},
                    (py::arg("self"), py::arg("other")),
                    doc_intro("Connect the input of this waterway to the output of the other waterway."))
            .def("input_from",+[](waterway_ s,unit_ u){return waterway::input_from(s,u);} ,
                    (py::arg("self"), py::arg("other")),
                    doc_intro("Connect the input of this waterway to the output of the unit."))
            .def("input_from", +[](waterway_ s, reservoir_ rsv, connection_role r) {return waterway::input_from(s,rsv,r);},
                    (py::arg("reservoir"), py::arg("other"), py::arg("role")=connection_role::main),
                    doc_intro("Connect the input of this waterway to the output of the reservoir, and assign the connection role."))
            .def("output_to", +[](waterway_ s, waterway_ w){return waterway::output_to(s,w);},
                    (py::arg("self"), py::arg("other")),
                    doc_intro("Connect the output of this waterway to the input of the other waterway."))
            .def("output_to", +[](waterway_ s, reservoir_ r) { return waterway::output_to(s,r);},
                    (py::arg("self"), py::arg("other")),
                    doc_intro("Connect the output of this waterway to the input of the reservoir."))
            .def("output_to", +[](waterway_ s, unit_ u) {return waterway::output_to(s,u);},
                    (py::arg("self"), py::arg("other")),
                    doc_intro("Connect the output of this waterway to the input of the unit."))
            .def(self==self)
            .def(self!=self)
        ;
        typedef std::vector<waterway_> WaterwayList;
        class_<WaterwayList>("WaterwayList")
            .def(vector_indexing_suite<WaterwayList, true>())
            ;

        class_<gate, bases<>, std::shared_ptr<gate>, boost::noncopyable>("Gate", 
            doc_intro(
            "A gate controls the amount of flow into the waterway by the gate-opening.\n"
            "In the case of tunnels, it's usually either closed or open.\n"
            "For reservoir flood-routes, the gate should be used to model the volume-flood characteristics.\n"
            "The resulting flow through a waterway is a function of many factors, most imporant:\n"
            " * gate opening and gate-characteristics\n"
            " * upstream water-level\n"
            " * downstrem water-level(in some-cases)\n"
            " * waterway properties(might be state dependent)\n"
            )
        )
        .def(init<int,const string&,const string&>((py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")),doc_intro("construct a new gate")))
        .def_readwrite("id",&gate::id,doc_intro("unique id for this gate"))
        .def_readwrite("name",&gate::name,doc_intro("name of the gate"))
        .def_readwrite("json",&gate::json,doc_intro("json for storing py-type data"))
        .add_property("water_route",&gate::wtr_,doc_intro("deprecated:use waterway"))
        .add_property("waterway",&gate::wtr_,doc_intro("ref. to the waterway where this gate controls the flow"))
        
        .def(self==self)
        .def(self!=self)
        ;
        typedef std::vector<gate_> GateList;
        class_<GateList>("GateList")
            .def(vector_indexing_suite<GateList, true>())
            ;
    }

    void reservoir_stuff() {
        using namespace shyft::energy_market::hydro_power;
        using py::self;
        py::class_<reservoir, py::bases<hydro_component>, std::shared_ptr<reservoir>, boost::noncopyable>("Reservoir", doc_intro(""),py::no_init)
            .def(py::init<int,const string&,const string&,hydro_power_system_ const&>((py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json"),py::arg("hps")), doc_intro("tbd")))
            .def("input_from",+[](reservoir_ s,waterway_ w){return reservoir::input_from(s,w);},(py::arg("self"),py::arg("other")),
                    doc_intro("Connect the input of the reservoir to the output of the waterway."))
            .def("output_to",+[] (reservoir_ s, waterway_ w, connection_role r) {return reservoir::output_to(s,w,r);},
                    (py::arg("self"), py::arg("other"), py::arg("role")=connection_role::main),
                    doc_intro("Connect the output of this reservoir to the input of the waterway, and assign the connection role"))
            //.def("output_to",+[] (reservoir_ &s, waterway_ const& w) {return reservoir::output_to(s,w,connection_role::main);},
            //        (py::arg("self"), py::arg("other")),
            //        doc_intro("Connect the output of this reservoir to the input of the waterway, and assign the connection role main."))
            .def(self == self)
            .def(self != self)
            ;

        typedef std::vector<reservoir_> ReservoirList;
        py::class_<ReservoirList>("ReservoirList")
            .def(py::vector_indexing_suite<ReservoirList, true>())
            ;
    }
    void power_plant_stuff() {
        using namespace boost::python;
        using namespace shyft::energy_market::hydro_power;
        using shyft::energy_market::id_base;
        
        class_<unit, bases<hydro_component>, std::shared_ptr<unit>, boost::noncopyable>(
            "Unit", 
            doc_intro(
                "An Unit consist of a turbine and a connected generator.\n"
                "The turbine is hydrologically connected to upstream tunnel and downstream tunell/river.\n"
                "The generator part is connected to the electrical grid through a busbar.\n"
                ""
                "In the long term models, the entire power plant is represented by a virtual unit that represents\n"
                "the total capability of the power-plant.\n"
                "\n"
                "The short-term detailed models, usually describes every aggratate up to a granularity that is\n"
                "relevant for the short-term optimization/simulation horizont.\n"
                "\n"
                "A power plant is a collection of one or more units that are natural to group into one power plant."
            )
            )
            .def(init<int,const string&,const string&,hydro_power_system_ const&>((py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json"),py::arg("hps")), doc_intro("tbd")))
            .add_property("downstream", &unit::downstream, doc_intro("returns downstream waterway(river/tunnel) object(if any)"))
            .add_property("upstream",&unit::upstream,doc_intro("returns upstream tunnel(water-route) object(if any)"))
            .add_property("power_station",&unit::pwr_station_,doc_intro("deprecated: use power_plant"))
            .add_property("power_plant",&unit::pwr_station_,doc_intro("return the hydro power plant associated with this unit"))
            .add_property("is_pump", &unit::is_pump, doc_intro("Returns true if the unit is a pump, otherwise, returns false"))
            .def("input_from", +[](unit_ s,waterway_ w){return unit::input_from(s,w);},
                    (py::arg("self"), py::arg("other")),
                    doc_intro("Connect the input of this unit to the output of the waterway."))
            .def("output_to", +[](unit_ s,waterway_ w){return unit::output_to(s,w);},
                    (py::arg("self"), py::arg("other")),
                    doc_intro("Connect the output of this unit to the input of the waterway."))
            .def(self == self)
            .def(self != self)
            ;
        
        typedef std::vector<unit_> UnitList;
        class_<UnitList>("UnitList")
            .def(vector_indexing_suite<UnitList, true>())
        ;
        
        class_<power_plant, bases<>, std::shared_ptr<power_plant>, boost::noncopyable>(
        "PowerPlant", 
        doc_intro(
            "A hydro power plant is the site/building that contains a number of units.\n"
            "The attributes of the power plant, are typically sum-requirement and/or operations that applies\n"
            "all of the units."
            "\n"
        )
        )
        .def(init<int,const string&,const string&,hydro_power_system_ const&>((py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json"),py::arg("hps")), doc_intro("tbd")))
        .add_property("hps", &power_plant::hps_, doc_intro("returns the hydro-power-system this component is a part of"))
        .def_readwrite("id", &power_plant::id, doc_intro("a system assigned unique id"))
        .def_readwrite("name", &power_plant::name, doc_intro("the name of the component, subject to unique-constraints"))
        .def_readwrite("json",&power_plant::json, doc_intro("some info as json"))
        .add_property("obj", &expose::py_object_ext<power_plant>::get_obj, &expose::py_object_ext<power_plant>::set_obj)
        .def_readwrite("units",&power_plant::units,doc_intro("associated units"))
        .def("add_unit",
             +[](power_plant_ pp, unit_ u)->void {power_plant::add_unit(pp,u); },// fixup const shrd ptr ref to python.
             (py::arg("self"),py::arg("unit")),doc_intro("add unit to plant"))
        .def("remove_unit",&power_plant::remove_unit,(py::arg("self"),py::arg("unit")),doc_intro("remove unit from plant"))
        // bw compat
        .def_readwrite("aggregates",&power_plant::units,doc_intro("deprecated: use units"))
        .def("add_aggregate",
             +[](power_plant_  pp, unit_ u)->void {power_plant::add_unit(pp,u); },// fixup const shrd ptr ref to python.
             (py::arg("self"),py::arg("aggregate")),doc_intro("deprecated:use add_unit"))
        .def("remove_aggregate",&power_plant::remove_unit,(py::arg("self"),py::arg("aggregate")),doc_intro("deprecated:use remove_unit"))
        .def(self == self)
        .def(self != self)
        ;
        typedef std::vector<power_plant_> PowerStationList;
        class_<PowerStationList>("PowerPlantList")
            .def(vector_indexing_suite<PowerStationList, true>())
        ;
        
    }

    void hydro_component_stuff() {
        using namespace boost::python;
        using namespace shyft::energy_market::hydro_power;
        enum_<connection_role>("ConnectionRole")
            .value("main", connection_role::main)
            .value("bypass", connection_role::bypass)
            .value("flood", connection_role::flood)
            .value("input", connection_role::input)
            .export_values()
        ;
        typedef hydro_connection HydroConnection;
        class_<HydroConnection,bases<>,HydroConnection,boost::noncopyable>("HydroConnection", 
            doc_intro(
                "A hydro connection is the connection object that relate one hydro component to another.\n"
                "A hydro component have zero or more hydro connections, contained in upstream and downstream lists.\n"
                "If you are using the hydro system builder, there will always be a mutual/two way connection.\n"
                "That is, if a reservoir connects downstream to a tunell (in role main), then the tunell will have\n"
                "a upstream connection pointing to the reservoir (as in role input)\n"
            ),
            no_init)
            //.def(init<connection_role,hydro_component_ const&>(args("a_role","a_target"),doc_intro("tbd")))
            .def_readwrite("role",&HydroConnection::role,doc_intro("role like main,bypass,flood,input"))
            .add_property("target",&HydroConnection::target_,doc_intro("target of the hydro-connection, Reservoir|Unit|Waterway"))
            .add_property("has_target",&HydroConnection::has_target,doc_intro("true if valid/available target"))
            ;
        typedef std::vector<HydroConnection> HydroConnectionList;
        class_<HydroConnectionList>("HydroConnectionList")
            .def(vector_indexing_suite<HydroConnectionList>())
        ;


        typedef hydro_component HydroComponent;
        class_<HydroComponent, bases<>, std::shared_ptr<HydroComponent>, boost::noncopyable>("HydroComponent",
            doc_intro("A hydro component keeps the common attributes and relational properties common for all components that can contain water")
            ,boost::python::no_init
            )
            .add_property("hps", &HydroComponent::hps_, doc_intro("returns the hydro-power-system this component is a part of"))
            .def_readwrite("id", &HydroComponent::id, doc_intro("a system assigned unique id"))
            .def_readwrite("name", &HydroComponent::name, doc_intro("the name of the component, subject to unique-constraints"))
            .def_readwrite("json",&HydroComponent::json, doc_intro("some info as json"))
            .def_readonly("upstreams", &HydroComponent::upstreams, doc_intro("list of hydro-components that are conceptually upstreams"))
            .def_readonly("downstreams", &HydroComponent::downstreams, doc_intro("list of hydro-components that are conceptually downstreams"))
            .add_property("obj", &py_object_ext<HydroComponent>::get_obj, &py_object_ext<HydroComponent>::set_obj, "a python object")
            .def("disconnect_from", &HydroComponent::disconnect_from, (py::arg("self"), py::arg("other")), doc_intro("disconnect from another component"))
            .def("equal_structure",&HydroComponent::equal_structure,(py::arg("self"),py::arg("other")),
                 doc_intro("Returns true if the `other` object have the same interconnections to the close neighbors as self.")
                 doc_intro("The neighbors are identified by their `.id` attribute, and they must appear in the same role to be considered equal.")
                 doc_intro("E.g. if for a reservoir, a waterway is in role flood for self, and in role bypass for other, they are different connections.")
                 doc_parameters()
                 doc_parameter("other","","the other object, of same type, hydro component, to compare.")
                 doc_returns("bool","","True if the other have same interconnections as self")
             )
            ;

		using HydroComponentList = std::vector<hydro_component_>;
		class_<HydroComponentList>("HydroComponentList")
			.def(vector_indexing_suite<HydroComponentList, true>())
			;
		
    }

    struct hps_ext {
        static std::vector<char> to_blob(const std::shared_ptr<shyft::energy_market::hydro_power::hydro_power_system>& hps) {
            auto s=shyft::energy_market::hydro_power::hydro_power_system::to_blob(hps);
            return std::vector<char>(s.begin(),s.end());
        }
        static std::shared_ptr<shyft::energy_market::hydro_power::hydro_power_system> from_blob(std::vector<char>&blob) {
            string s(blob.begin(),blob.end());
            return shyft::energy_market::hydro_power::hydro_power_system::from_blob(s);
        }
    };

    void hydro_power_system_stuff() {
        using namespace boost::python;
        using namespace shyft::energy_market::hydro_power;
        typedef hydro_power_system HydroPowerSystem;
        class_<HydroPowerSystem,bases<>,std::shared_ptr<HydroPowerSystem>,boost::noncopyable>("HydroPowerSystem", doc_intro(""),no_init)
            .def(init<string>(args("name"), doc_intro("creates an empty hydro power system with the specified name")))
            .def(init<int,string>((py::arg("id"),py::arg("name")),doc_intro("creates a an empty new hydro power system with specified id and name")))
            .def_readwrite("name", &HydroPowerSystem::name, doc_intro("The name of the hydro power system"))
            .def_readwrite("id", &HydroPowerSystem::id, doc_intro("The user-specified id, number [1..n>, that can identify this system from others"))
            .def_readwrite("created", &HydroPowerSystem::created, doc_intro("The time when this system was created(you should specify it when you create it)"))
            .def_readonly("water_routes",&HydroPowerSystem::waterways,"deprecated:use waterways")
            .def_readonly("waterways",&HydroPowerSystem::waterways,"all the waterways(tunells,rivers) of the system")
            .def_readonly("gates",&HydroPowerSystem::gates,"all the gates of the system")
            .def_readonly("aggregates",&HydroPowerSystem::units,"deprecated: use units")
            .def_readonly("units",&HydroPowerSystem::units,"all the hydropower units in this system")
            .def_readonly("reservoirs",&HydroPowerSystem::reservoirs,"all the reservoirs")
            .def_readonly("catchments",&HydroPowerSystem::catchments,"all the catchments for the system")
            .def_readonly("power_stations",&HydroPowerSystem::power_plants,"deprecated: use power_plant")
            .def_readonly("power_plants",&HydroPowerSystem::power_plants,"all power plants, each with references to its units")
            .add_property("model_area", &HydroPowerSystem::mdl_area_, 
                +[](HydroPowerSystem& hps, model_area_ &ma) {hps.set_mdl_area(ma); }, // Boost.Python can't handle pass by const-reference, so we make a thin wrapper.
                doc_intro("returns the model area this hydro-power-system is a part of")
                doc_see_also("ModelArea")
            )
            .add_property("obj", &py_object_ext<HydroPowerSystem>::get_obj, &py_object_ext<HydroPowerSystem>::set_obj, "a python object")
            .def("find_aggregate_by_name",&HydroPowerSystem::find_unit_by_name,(py::arg("self"),py::arg("name")),"deprecated: use find_unit_by_name")
            .def("find_unit_by_name",&HydroPowerSystem::find_unit_by_name,(py::arg("self"),py::arg("name")),"returns object that exactly  matches name")
            .def("find_reservoir_by_name", &HydroPowerSystem::find_reservoir_by_name, (py::arg("self"), py::arg("name")), "returns object that exactly  matches name")
            .def("find_water_route_by_name", &HydroPowerSystem::find_waterway_by_name, (py::arg("self"), py::arg("name")), "deprecated:use find_waterway_by_name")
            .def("find_waterway_by_name", &HydroPowerSystem::find_waterway_by_name, (py::arg("self"), py::arg("name")), "returns object that exactly  matches name")
            .def("find_gate_by_name", &HydroPowerSystem::find_gate_by_name, (py::arg("self"), py::arg("name")), "returns object that exactly  matches name")
            .def("find_power_station_by_name", &HydroPowerSystem::find_power_plant_by_name, (py::arg("self"), py::arg("name")), "deprecated:use find_power_plant_by_name")
            .def("find_power_plant_by_name", &HydroPowerSystem::find_power_plant_by_name, (py::arg("self"), py::arg("name")), "returns object that exactly  matches name")
            .def("find_aggregate_by_id", &HydroPowerSystem::find_unit_by_id, (py::arg("self"), py::arg("id")), "deprecated:use find_unit_by_id")
            .def("find_unit_by_id", &HydroPowerSystem::find_unit_by_id, (py::arg("self"), py::arg("id")), "returns object with specified id")
            .def("find_reservoir_by_id", &HydroPowerSystem::find_reservoir_by_id, (py::arg("self"), py::arg("id")), "returns object with specified id")
            .def("find_water_route_by_id", &HydroPowerSystem::find_waterway_by_id, (py::arg("self"), py::arg("id")), "deprecated:use find_waterway_by_id")
            .def("find_waterway_by_id", &HydroPowerSystem::find_waterway_by_id, (py::arg("self"), py::arg("id")), "returns object with specified id")
            .def("find_gate_by_id", &HydroPowerSystem::find_gate_by_id, (py::arg("self"), py::arg("id")), "returns object with specified id")
            .def("find_power_station_by_id", &HydroPowerSystem::find_power_plant_by_id, (py::arg("self"), py::arg("id")), "deprecated:use find_power_plant_by_id")
            .def("find_power_plant_by_id", &HydroPowerSystem::find_power_plant_by_id, (py::arg("self"), py::arg("id")), "returns object with specified id")
            .def("equal_structure",&HydroPowerSystem::equal_structure,
                 (py::arg("self"),py::arg("other_hps")),
                 doc_intro("returns true if equal structure of identified objects, using the .id, but not comparing .name, .attributes etc., to the other"))
            .def("equal_content",&HydroPowerSystem::equal_content,
                 (py::arg("self"),py::arg("other_hps")),
                 doc_intro("returns true if alle the content of the hps are equal, same as the equal == operator, except that .id, .name .created at the top level is not compared")
            )
            .def(py::self == py::self)
            .def(py::self != py::self)
            .def("to_blob_ref", &hps_ext::to_blob,args("me"),
                doc_intro("serialize the model into an blob")
                doc_returns("blob", "string", "blob-serialized version of the model")
                doc_see_also("from_blob")
            ).staticmethod("to_blob_ref")
            .def("from_blob", &hps_ext::from_blob, args("blob_string"),
                doc_intro("constructs a model from a blob_string previously created by the to_blob method")
                doc_parameters()
                doc_parameter("blob_string", "string", "blob-formatted representation of the model, as create by the to_blob method")
            ).staticmethod("from_blob")
            ;
        
        class_<hydro_power_system_builder>("HydroPowerSystemBuilder", doc_intro("class to support building hydro-power-systems save and easy"), no_init)
            .def(init<hydro_power_system_&>(args("hydro_power_system")))
            .def("create_river", &hydro_power_system_builder::create_river, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("create and add river to the system"))
            .def("create_tunnel", &hydro_power_system_builder::create_tunnel, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("create and add river to the system"))
            .def("create_water_route", &hydro_power_system_builder::create_waterway, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("deprecated:use create_waterway"))
            .def("create_waterway", &hydro_power_system_builder::create_waterway, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("create and add river to the system"))
            .def("create_gate", &hydro_power_system_builder::create_gate, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("create and add a gate to the system"))
            .def("create_aggregate", &hydro_power_system_builder::create_unit, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("deprecated:use create_unit"))
            .def("create_unit", &hydro_power_system_builder::create_unit, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("creates a new unit with the specified parameters"))
            .def("create_reservoir", &hydro_power_system_builder::create_reservoir, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("creates and adds a reservoir to the system"))
            .def("create_power_station", &hydro_power_system_builder::create_power_plant, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("deprecated: use create_power_plant"))
            .def("create_power_plant", &hydro_power_system_builder::create_power_plant, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("creates and adds a power plant to the system"))
            .def("create_catchment", &hydro_power_system_builder::create_catchment, (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")), doc_intro("create and add catchmment to the system"))
            ;
            
        using HydroPowerSystemDict = std::map<string, std::shared_ptr<HydroPowerSystem>>;
        class_<HydroPowerSystemDict>("HydroPowerSystemDict",
                                     doc_intro("A dict of HydroPowerSystem, the key-value is the watercourse-name"))
            .def(map_indexing_suite<HydroPowerSystemDict, true>())
            .def(init<const HydroPowerSystemDict&>(args("clone_me")))
        ;

        using HydroPowerSystemList = std::vector<hydro_power_system_>;
        class_<HydroPowerSystemList>("HydroPowerSystemList")
            .def(vector_indexing_suite<HydroPowerSystemList, true>())
        ;
        
    }

    void hydro_graph_stuff() {
        using namespace shyft::energy_market::hydro_power;
        using namespace shyft::energy_market::graph;

	py::def("upstream_reservoirs", upstream_reservoirs,(py::arg("component"), py::arg("max_dist")=0), doc_intro("Find reservoirs upstream from component"));
	py::def("downstream_reservoirs", downstream_reservoirs,(py::arg("component"), py::arg("max_dist")=0), doc_intro("Find reservoirs upstream from component"));
	py::def("upstream_units", upstream_units,(py::arg("component"), py::arg("max_dist")=0), doc_intro("Find reservoirs upstream from component"));
	py::def("downstream_units", downstream_units,(py::arg("component"), py::arg("max_dist")=0), doc_intro("Find reservoirs upstream from component"));
        
    }
    
    void hydro_operations_stuff() {
        using namespace boost::python;
        using namespace shyft::energy_market::hydro_power;
        using hgt = hydro_operations; // alias hgt = HydroGraphTraversal
        using chc = const hydro_component_;
        using hc_vec = std::vector<hydro_component_>;

        class_<hgt, bases<>, std::shared_ptr<hgt>, boost::noncopyable>("HydroGraphTraversal", doc_intro("A collection of hydro operations"), boost::python::no_init)
            .def<void(*)(hc_vec&, connection_role)>("path_to_ocean", &hgt::path_to_ocean, doc_intro("finds path to ocean for a given hydro component"))
            .def<void(*)(hc_vec&)>("path_to_ocean",&hgt::path_to_ocean, doc_intro("finds path to ocean for a given hydro component")).staticmethod("path_to_ocean")
            .def<hc_vec(*)(chc&, connection_role)>("get_path_to_ocean",&hgt::get_path_to_ocean, doc_intro("finds path to ocean for a given hydro component"))
            .def<hc_vec(*)(chc&)>("get_path_to_ocean",&hgt::get_path_to_ocean, doc_intro("finds path to ocean for a given hydro component")).staticmethod("get_path_to_ocean")
            .def<hc_vec(*)(chc&, chc&, connection_role)>("get_path_between",&hgt::get_path_between, doc_intro("finds path between two hydro components"))
            .def<hc_vec(*)(chc&, chc&)>("get_path_between",&hgt::get_path_between, doc_intro("finds path between two hydro components")).staticmethod("get_path_between")
            .def<bool(*)(chc&, chc&, connection_role)>("is_connected",&hgt::is_connected, doc_intro("finds whether two hydro components are connected"))
            .def<bool(*)(chc&, chc&)>("is_connected",&hgt::is_connected, doc_intro("finds whether two hydro components are connected")).staticmethod("is_connected")
            .def("extract_water_courses", &hgt::extract_water_courses, args("hps"), doc_intro("extracts the sub-hydro system from a given hydro system")).staticmethod("extract_water_courses")
            ;
    }

    void all_of_hydro_power_system() {
        // call each expose f here
        catchment_stuff();
        hydro_component_stuff();
        water_route_stuff();
        reservoir_stuff();
        power_plant_stuff();
        hydro_power_system_stuff();
        hydro_operations_stuff();
        hydro_graph_stuff();
    }
}
