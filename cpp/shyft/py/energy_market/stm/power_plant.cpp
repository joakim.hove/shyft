/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <boost/format.hpp>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;

    using boost::format;

    template<> string str_(stm::power_plant const& o) {
        return (format("PowerPlant(id=%1%, name=%2%)")
            %str_(o.id)
            %str_(o.name)
        ).str();
    }

    template<> string str_(stm::power_plant::discharge_ const& o) {
        return (format("_PowerPlantDischarge(schedule=%1%, result=%2%, constraint_min=%3%, constraint_max=%4%, ramping_up=%5%)")
            %str_(o.schedule)
            %str_(o.result)
            %str_(o.constraint_min)
            % str_(o.constraint_max)
            % str_(o.ramping_up)
        ).str();
    }

    template<> string str_(stm::power_plant::production_ const& o) {
        return (format("_PowerPlantProduction(schedule=%1%, constraint_min=%2%, constraint_max=%3%, result=%4%)")
            %str_(o.schedule)
            %str_(o.constraint_min)
            %str_(o.constraint_max)
            %str_(o.result)
        ).str();
    }

    void stm_power_plant() {
        auto p=py::class_<
            stm::power_plant,
            py::bases<hydro_power::power_plant>,
            shared_ptr<stm::power_plant>,
            boost::noncopyable
        >("PowerPlant", 
            doc_intro("A power plant keeping stm units.")
            doc_details("After creating the units, create the power plant,\n"
                        "and add the units to the power plant.")
            , py::no_init);
        p
            .def(py::init<int, const string&, const string&, stm::stm_hps_ &>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("hps")),
                "Create power plant with unique id and name for a hydro power system."))


            .def_readonly("discharge", &stm::power_plant::discharge, "Discharge attributes")
            .def_readonly("production", &stm::power_plant::production, "Production attributes")
            .add_property("tag", +[](const stm::power_plant& self){return url_tag(self);}, "url tag")
            .def("add_unit",
                +[](stm::power_plant_& pp, stm::unit_& u)->void {hydro_power::power_plant::add_unit(pp,u); },// fixup const shrd ptr ref to python.
                (py::arg("self"),py::arg("unit")),doc_intro("add unit to plant"))

            .def("__eq__", &stm::power_plant::operator==)
            .def("__ne__", &stm::power_plant::operator!=)

            .def("flattened_attributes", +[](stm::power_plant& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes")
        ;
        expose_str_repr(p);
        add_proxy_property(p,"outlet_level", stm::power_plant,outlet_level, "Outlet level, time-dependent attribute.")
        add_proxy_property(p,"mip", stm::power_plant,mip, "Mip, time series.")
        add_proxy_property(p,"unavailability", stm::power_plant,unavailability, "Unavailability, time series.")


        {
            py::scope pp_scope=p;
            auto  pd=py::class_<stm::power_plant::discharge_, boost::noncopyable>("_Discharge", py::no_init);
            expose_str_repr(pd);
            
            _add_proxy_property(pd,"constraint_min", stm::power_plant::discharge_,constraint_min, "Discharge minimum restriction, time series.")
            _add_proxy_property(pd,"constraint_max", stm::power_plant::discharge_,constraint_max, "Discharge maximum restriction, time series.")
            _add_proxy_property(pd,"schedule", stm::power_plant::discharge_,schedule, "Discharge schedule, time series.")
            _add_proxy_property(pd,"result", stm::power_plant::discharge_,result, "Discharge result, time series.")
            _add_proxy_property(pd,"realised", stm::power_plant::discharge_,realised, "Discharge realised, usually related/or defined as unit.discharge.realised.")
            _add_proxy_property(pd,"upstream_level_constraint", stm::power_plant::discharge_,upstream_level_constraint, "Max discharge limited by upstream water level")
            _add_proxy_property(pd,"downstream_level_constraint", stm::power_plant::discharge_,downstream_level_constraint, "Max discharge limited by downstream water lever")
            _add_proxy_property(pd,"intake_loss_from_bypass_flag", stm::power_plant::discharge_,intake_loss_from_bypass_flag, "Headloss of intake affected by bypass discharge")
            _add_proxy_property(pd,"ramping_up", stm::power_plant::discharge_,ramping_up, "Constraint on increasing discharge, time-dependent attribute.")
            _add_proxy_property(pd,"ramping_down", stm::power_plant::discharge_,ramping_down, "Constraint on decreasing discharge, time-dependent attribute.")



            auto pp=py::class_<stm::power_plant::production_, boost::noncopyable>("_Production", py::no_init);
            expose_str_repr(pp);
            _add_proxy_property(pp,"constraint_min", stm::power_plant::production_,constraint_min, "Production minimum restriction, time series.")
            _add_proxy_property(pp,"constraint_max", stm::power_plant::production_,constraint_max, "Production maximum restriction, time series.")
            _add_proxy_property(pp,"schedule", stm::power_plant::production_,schedule, "Production schedule, time series.")
            _add_proxy_property(pp,"realised", stm::power_plant::production_,realised, "Production realised, usually related to sum of unit.production.realised.")
            _add_proxy_property(pp,"merge_tolerance", stm::power_plant::production_,merge_tolerance, "Nax deviation in production, time-dependent attribute.")
            _add_proxy_property(pp,"ramping_up", stm::power_plant::production_,ramping_up, "Constraint on increasing production, time-dependent attribute.")
            _add_proxy_property(pp,"ramping_down", stm::power_plant::production_,ramping_down, "Constraint on decreasing production, time-dependent attribute.")
            _add_proxy_property(pp,"result", stm::power_plant::production_,result, "Production result, time series.")

        }
    }
}
