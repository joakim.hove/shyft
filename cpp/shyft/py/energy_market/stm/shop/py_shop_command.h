#pragma once
#include <string>
#include <vector>
#include <boost/format.hpp>
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_str.h>
#include <shyft/py/api/expose_vector.h>
#include <shyft/energy_market/stm/shop/shop_command.h>

namespace expose {

template<> std::string str_(const shyft::energy_market::stm::shop::shop_command& o) {
    return (boost::format("ShopCommand(%1%)")
        %str_((std::string)o)
    ).str();
}

static void expose_shop_command() {

    using std::vector;
    using std::string;
    using namespace boost::python;
    using shyft::energy_market::stm::shop::shop_command;

    auto x=class_<
        shop_command,
        bases<>
        //shared_ptr<shop_command>,
        //boost::noncopyable
    >("ShopCommand",
        doc_intro("A shop command, which can later be sent to the shop core.")
        doc_details(
            "In the shop core a command can be described as a string with syntax:\n"
            "\"<keyword> <specifier> [/<opt> [/<opt>...]] [[<obj>] [<obj>...]]\".\n"
            "The keyword is a general word that specifies the kind of action, for\n"
            "example 'set', 'save' or 'return'.\n"
            "The specifier identifies what data will be affected by the command,\n"
            "and it is unique for every command, for example 'method', or 'ramping'.\n"
            "Commands can accept one or several pre-set options to further specify\n"
            "the command. These always start with a forward slash, consist of one\n"
            "word only, and if more than one the order is important.\n"
            "Some commands also needs input objects, usually an integer, floating\n"
            "point, or string value.")
        , no_init)
        .def(init<const string&>(
            (arg("command")),
            "Create from a single string in shop command language syntax."))
        .def(init<const string&, const string&, const vector<string>&, const vector<string>&>(
            (arg("keyword"), arg("specifier"), arg("options"), arg("objects")),
            "Create from individual components."))
        .def(self == self)
        .def(self != self)

        .def_readwrite("keyword", &shop_command::keyword, "keyword of the command")
        .def_readwrite("specifier", &shop_command::specifier, "specifier of the command")
        .def_readwrite("options", &shop_command::options, "list of options")
        .def_readwrite("objects", &shop_command::objects, "list of objects")

        .def("set_method_primal"                   , &shop_command::set_method_primal                                                    , "Shop command string \"set method /primal\"."                        ).staticmethod("set_method_primal")
        .def("set_method_dual"                     , &shop_command::set_method_dual                                                      , "Shop command string \"set method /dual\"."                          ).staticmethod("set_method_dual")
        .def("set_method_baropt"                   , &shop_command::set_method_baropt                                                    , "Shop command string \"set method /baropt\"."                        ).staticmethod("set_method_baropt")
        .def("set_method_hydbaropt"                , &shop_command::set_method_hydbaropt                                                 , "Shop command string \"set method /hydbaropt\"."                     ).staticmethod("set_method_hydbaropt")
        .def("set_method_netprimal"                , &shop_command::set_method_netprimal                                                 , "Shop command string \"set method /netprimal\"."                     ).staticmethod("set_method_netprimal")
        .def("set_method_netdual"                  , &shop_command::set_method_netdual                                                   , "Shop command string \"set method /netdual\"."                       ).staticmethod("set_method_netdual")
        .def("set_code_full"                       , &shop_command::set_code_full                                                        , "Shop command string \"set code /full\"."                            ).staticmethod("set_code_full")
        .def("set_code_incremental"                , &shop_command::set_code_incremental                                                 , "Shop command string \"set code /incremental\"."                     ).staticmethod("set_code_incremental")
        .def("set_code_head"                       , &shop_command::set_code_head                                                        , "Shop command string \"set code /head\"."                            ).staticmethod("set_code_head")
        .def("set_password"                        , &shop_command::set_password                        , (arg("key"), arg("value"))     , "Shop command string \"set password <value>\"."                      ).staticmethod("set_password")
        .def("start_sim"                           , &shop_command::start_sim                           , (arg("iterations"))            , "Shop command string \"start sim <iterations>\"."                    ).staticmethod("start_sim")
        .def("start_shopsim"                       , &shop_command::start_shopsim                                                        , "Shop command string \"start shopsim\"."                             ).staticmethod("start_shopsim")
        .def<shop_command(*)()>("log_file"         , &shop_command::log_file                                                             , "Shop command string \"log file\"."                                  )//.staticmethod("log_file")
        .def<shop_command(*)(string)>("log_file"   , &shop_command::log_file                            , (arg("filename"))              , "Shop command string \"log file <filename>\"."                       )//.staticmethod("log_file")
        .def("log_file_lp"                         , &shop_command::log_file_lp                         , (arg("filename"))              , "Shop command string \"log file /lp <filename>\"."                   ).staticmethod("log_file")
        .def("set_xmllog"                          , &shop_command::set_xmllog                          , (arg("on"))                    , "Shop command string \"set xmllog /on|/off\"."                       ).staticmethod("set_xmllog")
        .def("return_simres"                       , &shop_command::return_simres                       , (arg("filename"))              , "Shop command string \"return simres <filename>\"."                  ).staticmethod("return_simres")
        .def("return_simres_gen"                   , &shop_command::return_simres_gen                   , (arg("filename"))              , "Shop command string \"return simres /gen <filename>\"."             ).staticmethod("return_simres_gen")
        .def("save_series"                         , &shop_command::save_series                         , (arg("filename"))              , "Shop command string \"save series <filename>\"."                    ).staticmethod("save_series")
        .def("save_xmlseries"                      , &shop_command::save_xmlseries                      , (arg("filename"))              , "Shop command string \"save xmlseries <filename>\"."                 ).staticmethod("save_xmlseries")
        .def("print_model"                         , &shop_command::print_model                         , (arg("filename"))              , "Shop command string \"print model <filename>\"."                    ).staticmethod("print_model")
        .def("return_scenario_result_table"        , &shop_command::return_scenario_result_table        , (arg("filename"))              , "Shop command string \"return scenario_result_table <filename>\"."   ).staticmethod("return_scenario_result_table")
        .def("save_tunnelloss"                     , &shop_command::save_tunnelloss                                                      , "Shop command string \"save tunnelloss\"."                           ).staticmethod("save_tunnelloss")
        .def("save_shopsimseries"                  , &shop_command::save_shopsimseries                  , (arg("filename"))              , "Shop command string \"save shopsimseries <filename>\"."             ).staticmethod("save_shopsimseries")
        .def("return_shopsimres"                   , &shop_command::return_shopsimres                   , (arg("filename"))              , "Shop command string \"return shopsimres <filename>\"."              ).staticmethod("return_shopsimres")
        .def("return_shopsimres_gen"               , &shop_command::return_shopsimres_gen               , (arg("filename"))              , "Shop command string \"return shopsimres /gen <filename>\"."         ).staticmethod("return_shopsimres_gen")
        .def("save_xmlshopsimseries"               , &shop_command::save_xmlshopsimseries               , (arg("filename"))              , "Shop command string \"save xmlshopsimseries <filename>\"."          ).staticmethod("save_xmlshopsimseries")
        .def("print_pqcurves_all"                  , &shop_command::print_pqcurves_all                                                   , "Shop command string \"print pqcurves /all\"."                       ).staticmethod("print_pqcurves_all")
        .def("print_pqcurves_result"               , &shop_command::print_pqcurves_result                                                , "Shop command string \"print pqcurves /result\"."                    ).staticmethod("print_pqcurves_result")
        .def("print_pqcurves_input"                , &shop_command::print_pqcurves_input                                                 , "Shop command string \"print pqcurves /input\"."                     ).staticmethod("print_pqcurves_input")
        .def("penalty_flag_all"                    , &shop_command::penalty_flag_all                    , (arg("on"))                    , "Shop command string \"penalty flag /all /on|/off\"."                ).staticmethod("penalty_flag_all")
        .def("penalty_flag_reservoir_ramping"      , &shop_command::penalty_flag_reservoir_ramping      , (arg("on"))                    , "Shop command string \"penalty flag /on|/off /reservoir /ramping\"." ).staticmethod("penalty_flag_reservoir_ramping")
        .def("penalty_flag_reservoir_endpoint"     , &shop_command::penalty_flag_reservoir_endpoint     , (arg("on"))                    , "Shop command string \"penalty flag /on|/off /reservoir /endpoint\".").staticmethod("penalty_flag_reservoir_endpoint")
        .def("penalty_flag_load"                   , &shop_command::penalty_flag_load                   , (arg("on"))                    , "Shop command string \"penalty flag /on|/off /load\"."               ).staticmethod("penalty_flag_load")
        .def("penalty_flag_powerlimit"             , &shop_command::penalty_flag_powerlimit             , (arg("on"))                    , "Shop command string \"penalty flag /on|/off /powerlimit\"."         ).staticmethod("penalty_flag_powerlimit")
        .def("penalty_flag_discharge"              , &shop_command::penalty_flag_discharge              , (arg("on"))                    , "Shop command string \"penalty flag /on|/off /discharge\"."          ).staticmethod("penalty_flag_discharge")
        .def("penalty_flag_plant_min_p_con"        , &shop_command::penalty_flag_plant_min_p_con        , (arg("on"))                    , "Shop command string \"penalty flag /on|/off /plant /min_p_con\"."   ).staticmethod("penalty_flag_plant_min_p_con")
        .def("penalty_flag_plant_max_p_con"        , &shop_command::penalty_flag_plant_max_p_con        , (arg("on"))                    , "Shop command string \"penalty flag /on|/off /plant /max_p_con\"."   ).staticmethod("penalty_flag_plant_max_p_con")
        .def("penalty_flag_plant_min_q_con"        , &shop_command::penalty_flag_plant_min_q_con        , (arg("on"))                    , "Shop command string \"penalty flag /on|/off /plant /min_q_con\"."   ).staticmethod("penalty_flag_plant_min_q_con")
        .def("penalty_flag_plant_max_q_con"        , &shop_command::penalty_flag_plant_max_q_con        , (arg("on"))                    , "Shop command string \"penalty flag /on|/off /plant /max_q_con\"."   ).staticmethod("penalty_flag_plant_max_q_con")
        .def("penalty_flag_plant_schedule"         , &shop_command::penalty_flag_plant_schedule         , (arg("on"))                    , "Shop command string \"penalty flag /on|/off /plant /schedule\"."    ).staticmethod("penalty_flag_plant_schedule")
        .def("penalty_flag_gate_min_q_con"         , &shop_command::penalty_flag_gate_min_q_con         , (arg("on"))                    , "Shop command string \"penalty flag /on|/off /gate /min_q_con\"."    ).staticmethod("penalty_flag_gate_min_q_con")
        .def("penalty_flag_gate_max_q_con"         , &shop_command::penalty_flag_gate_max_q_con         , (arg("on"))                    , "Shop command string \"penalty flag /on|/off /gate /max_q_con\"."    ).staticmethod("penalty_flag_gate_max_q_con")
        .def("penalty_flag_gate_ramping"           , &shop_command::penalty_flag_gate_ramping           , (arg("on"))                    , "Shop command string \"penalty flag /on|/off /gate /ramping\"."      ).staticmethod("penalty_flag_gate_ramping")
        .def("penalty_cost_all"                    , &shop_command::penalty_cost_all                    , (arg("value"))                 , "Shop command string \"penalty cost /all <value>\"."                 ).staticmethod("penalty_cost_all")
        .def("penalty_cost_reservoir_ramping"      , &shop_command::penalty_cost_reservoir_ramping      , (arg("value"))                 , "Shop command string \"penalty cost /reservoir /ramping <value>\"."  ).staticmethod("penalty_cost_reservoir_ramping")
        .def("penalty_cost_reservoir_endpoint"     , &shop_command::penalty_cost_reservoir_endpoint     , (arg("value"))                 , "Shop command string \"penalty cost /reservoir /endpoint <value>\"." ).staticmethod("penalty_cost_reservoir_endpoint")
        .def("penalty_cost_load"                   , &shop_command::penalty_cost_load                   , (arg("value"))                 , "Shop command string \"penalty cost /load <value>\"."                ).staticmethod("penalty_cost_load")
        .def("penalty_cost_powerlimit"             , &shop_command::penalty_cost_powerlimit             , (arg("value"))                 , "Shop command string \"penalty cost /powerlimit <value>\"."          ).staticmethod("penalty_cost_powerlimit")
        .def("penalty_cost_discharge"              , &shop_command::penalty_cost_discharge              , (arg("value"))                 , "Shop command string \"penalty cost /discharge <value>\"."           ).staticmethod("penalty_cost_discharge")
        .def("penalty_cost_gate_ramping"           , &shop_command::penalty_cost_gate_ramping           , (arg("value"))                 , "Shop command string \"penalty cost /gate /ramping <value>\"."       ).staticmethod("penalty_cost_gate_ramping")
        .def("penalty_cost_overflow"               , &shop_command::penalty_cost_overflow               , (arg("value"))                 , "Shop command string \"penalty cost /overflow <value>\"."            ).staticmethod("penalty_cost_overflow")
        .def("penalty_cost_overflow_time_adjust"   , &shop_command::penalty_cost_overflow_time_adjust   , (arg("value"))                 , "Shop command string \"penalty cost /overflow_time_adjust <value>\".").staticmethod("penalty_cost_overflow_time_adjust")
        .def("penalty_cost_reserve"                , &shop_command::penalty_cost_reserve                , (arg("value"))                 , "Shop command string \"penalty cost /reserve <value>\"."             ).staticmethod("penalty_cost_reserve")
        .def("penalty_cost_soft_p_penalty"         , &shop_command::penalty_cost_soft_p_penalty         , (arg("value"))                 , "Shop command string \"penalty cost /soft_p_penalty <value>\"."      ).staticmethod("penalty_cost_soft_p_penalty")
        .def("penalty_cost_soft_q_penalty"         , &shop_command::penalty_cost_soft_q_penalty         , (arg("value"))                 , "Shop command string \"penalty cost /soft_q_penalty <value>\"."      ).staticmethod("penalty_cost_soft_q_penalty")
        .def("set_newgate"                         , &shop_command::set_newgate                         , (arg("on"))                    , "Shop command string \"set newgate /on|/off\"."                      ).staticmethod("set_newgate")
        .def("set_ramping"                         , &shop_command::set_ramping                         , (arg("mode"))                  , "Shop command string \"set ramping <value>\"."                       ).staticmethod("set_ramping")
        .def("set_mipgap"                          , &shop_command::set_mipgap                          , (arg("absolute"), arg("value")), "Shop command string \"set mipgap /absolute|/relative <value>\"."    ).staticmethod("set_mipgap")
        .def("set_nseg_all"                        , &shop_command::set_nseg_all                        , (arg("value"))                 , "Shop command string \"set nseg /all <value>\"."                     ).staticmethod("set_nseg_all")
        .def("set_nseg_up"                         , &shop_command::set_nseg_up                         , (arg("value"))                 , "Shop command string \"set nseg /up <value>\"."                      ).staticmethod("set_nseg_up")
        .def("set_nseg_down"                       , &shop_command::set_nseg_down                       , (arg("value"))                 , "Shop command string \"set nseg /down <value>\"."                    ).staticmethod("set_nseg_down")
        .def("set_dyn_seg_on"                      , &shop_command::set_dyn_seg_on                                                       , "Shop command string \"set dyn_seg /on\"."                           ).staticmethod("set_dyn_seg_on")
        .def("set_dyn_seg_incr"                    , &shop_command::set_dyn_seg_incr                                                     , "Shop command string \"set dyn_seg /incr\"."                         ).staticmethod("set_dyn_seg_incr")
        .def("set_dyn_seg_mip"                     , &shop_command::set_dyn_seg_mip                                                      , "Shop command string \"set dyn_seg /mip\"."                          ).staticmethod("set_dyn_seg_mip")
        .def("set_max_num_threads"                 , &shop_command::set_max_num_threads                 , (arg("value"))                 , "Shop command string \"set max_num_threads <value>\"."               ).staticmethod("set_max_num_threads")
        .def("set_parallel_mode_auto"              , &shop_command::set_parallel_mode_auto                                               , "Shop command string \"set parallel_mode /auto\"."                   ).staticmethod("set_parallel_mode_auto")
        .def("set_parallel_mode_deterministic"     , &shop_command::set_parallel_mode_deterministic                                      , "Shop command string \"set parallel_mode /deterministic\"."          ).staticmethod("set_parallel_mode_deterministic")
        .def("set_parallel_mode_opportunistic"     , &shop_command::set_parallel_mode_opportunistic                                      , "Shop command string \"set parallel_mode /opportunistic\"."          ).staticmethod("set_parallel_mode_opportunistic")
        .def("set_capacity_all"                    , &shop_command::set_capacity_all                    , (arg("value"))                 , "Shop command string \"set capacity /all <value>\"."                 ).staticmethod("set_capacity_all")
        .def("set_capacity_gate"                   , &shop_command::set_capacity_gate                   , (arg("value"))                 , "Shop command string \"set capacity /gate <value>\"."                ).staticmethod("set_capacity_gate")
        .def("set_capacity_bypass"                 , &shop_command::set_capacity_bypass                 , (arg("value"))                 , "Shop command string \"set capacity /bypass <value>\"."              ).staticmethod("set_capacity_bypass")
        .def("set_capacity_spill"                  , &shop_command::set_capacity_spill                  , (arg("value"))                 , "Shop command string \"set capacity /spill <value>\"."               ).staticmethod("set_capacity_spill")
        .def("set_headopt_feedback"                , &shop_command::set_headopt_feedback                , (arg("value"))                 , "Shop command string \"set headopt_feedback <value>\"."              ).staticmethod("set_headopt_feedback")
        .def("set_timelimit"                       , &shop_command::set_timelimit                       , (arg("value"))                 , "Shop command string \"set timelimit <value>\"."                     ).staticmethod("set_timelimit")
        .def("set_dyn_flex_mip"                    , &shop_command::set_dyn_flex_mip                    , (arg("value"))                 , "Shop command string \"set dyn_flex_mip <value>\"."                  ).staticmethod("set_dyn_flex_mip")
        .def("set_universal_mip_on"                , &shop_command::set_universal_mip_on                                                 , "Shop command string \"set universal_mip /on\"."                     ).staticmethod("set_universal_mip_on")
        .def("set_universal_mip_off"               , &shop_command::set_universal_mip_off                                                , "Shop command string \"set universal_mip /off\"."                    ).staticmethod("set_universal_mip_off")
        .def("set_universal_mip_not_set"           , &shop_command::set_universal_mip_not_set                                            , "Shop command string \"set universal_mip /not_set\"."                ).staticmethod("set_universal_mip_not_set")
        .def("set_merge_on"                        , &shop_command::set_merge_on                                                         , "Shop command string \"set merge /on\"."                             ).staticmethod("set_merge_on")
        .def("set_merge_off"                       , &shop_command::set_merge_off                                                        , "Shop command string \"set merge /off\"."                            ).staticmethod("set_merge_off")
        .def("set_merge_stop"                      , &shop_command::set_merge_stop                                                       , "Shop command string \"set merge /stop\"."                           ).staticmethod("set_merge_stop")
        .def("set_com_dec_period"                  , &shop_command::set_com_dec_period                  , (arg("value"))                 , "Shop command string \"set com_dec_period <value>\"."                ).staticmethod("set_com_dec_period")
        .def("set_time_delay_unit_hour"            , &shop_command::set_time_delay_unit_hour                                             , "Shop command string \"set time_delay_unit hour\"."                  ).staticmethod("set_time_delay_unit_hour")
        .def("set_time_delay_unit_minute"          , &shop_command::set_time_delay_unit_minute                                           , "Shop command string \"set time_delay_unit minute\"."                ).staticmethod("set_time_delay_unit_minute")
        .def("set_time_delay_unit_time_step_length", &shop_command::set_time_delay_unit_time_step_length                                 , "Shop command string \"set time_delay_unit time_step_length\"."      ).staticmethod("set_time_delay_unit_time_step_length")
        .def("set_power_head_optimization"         , &shop_command::set_power_head_optimization         , (arg("on"))                    , "Shop command string \"set power_head_optimization /on|/off\"."      ).staticmethod("set_power_head_optimization")
        .def("set_bypass_loss"                     , &shop_command::set_bypass_loss                     , (arg("on"))                    , "Shop command string \"set bypass_loss /on|/off\"."                  ).staticmethod("set_bypass_loss")
        .def("set_reserve_slack_cost"              , &shop_command::set_reserve_slack_cost              , (arg("value"))                 , "Shop command string \"set reserve_slack_cost <value>\"."            ).staticmethod("set_reserve_slack_cost")
        .def("set_fcr_n_band"                      , &shop_command::set_fcr_n_band                      , (arg("value"))                 , "Shop command string \"set fcr_n_band <value>\"."                    ).staticmethod("set_fcr_n_band")
        .def("set_fcr_d_band"                      , &shop_command::set_fcr_d_band                      , (arg("value"))                 , "Shop command string \"set fcr_d_band <value>\"."                    ).staticmethod("set_fcr_d_band")
        .def("set_fcr_n_equality"                  , &shop_command::set_fcr_n_equality                  , (arg("value"))                 , "Shop command string \"set fcr_n_equality <value>\"."                ).staticmethod("set_fcr_n_equality")
        .def("set_reserve_ramping_cost"            , &shop_command::set_reserve_ramping_cost            , (arg("value"))                 , "Shop command string \"set reserve_ramping_cost <value>\"."          ).staticmethod("set_reserve_ramping_cost")
        .def("set_gen_turn_off_limit"              , &shop_command::set_gen_turn_off_limit              , (arg("value"))                 , "Shop command string \"set gen_turn_off_limit <value>\"."            ).staticmethod("set_gen_turn_off_limit")
    ;

    expose_str_repr(x);

    expose_vector<shop_command>("ShopCommandList", "A strongly typed list of ShopCommand.");
}

}
