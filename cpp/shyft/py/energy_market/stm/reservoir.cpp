/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <boost/format.hpp>


namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::time_series::dd::apoint_ts;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;

    using boost::format;

    template<> string str_(stm::reservoir::level_::constraint_ const& o) {
        return (format("_Constraint(min=%1%, max=%2%)")
            %str_(o.min)
            %str_(o.max)
        ).str();
    }

    template<> string str_(stm::reservoir::volume_::constraint_::tactical_ const& o) {
        return (format("_Tactical(min=%1%, max=%2%)")
            %str_(o.min)
            %str_(o.max)
        ).str();
    }

    template<> string str_(stm::reservoir::volume_::constraint_ const& o) {
        return (format("_Constraint(min=%1%, max=%2%, tactical=%3%)")
            %str_(o.min)
            %str_(o.max)
            %str_(o.tactical)
        ).str();
    }

    template<> string str_(stm::reservoir::volume_::slack_ const& o) {
        return (format("_Slack(lower=%1%, uppper=%2%)")
            %str_(o.lower)
            %str_(o.upper)
        ).str();
    }

    template<> string str_(stm::reservoir::volume_ const& o) {
        return (format("_Volume(static_max=%1%, schedule=%2%, realised= %3%, result=%4%, constaint=%5%, slack=%6%)")
            %str_(o.static_max)
            %str_(o.schedule)
            %str_(o.realised)
            %str_(o.result)
            %str_(o.constraint)
            %str_(o.slack)
        ).str();
    }

    template<> string str_(stm::reservoir::level_ const& o) {
        return (format("_Level(regulation_min=%1%, regulation_max=%2%, realised=%3%, schedule=%4%, result=%5%, constraint=%6%)")
            %str_(o.regulation_min)
            %str_(o.regulation_max)
            %str_(o.realised)
            %str_(o.schedule)
            %str_(o.result)
            %str_(o.constraint)
        ).str();
    }

    template<> string str_(stm::reservoir::ramping_ const& o) {
        return (format("_Ramping(level_down=%1%, level_up=%2%)")
            %str_(o.level_down)
            %str_(o.level_up)
        ).str();
    }

    template<> string str_(stm::reservoir::inflow_ const& o) {
        return (format("_Inflow(schedule=%1%, realised=%2%, result=%3%)")
            %str_(o.schedule)
            %str_(o.realised)
            %str_(o.result)
        ).str();
    }

    template<> string str_(stm::reservoir::water_value_::result_ const& o) {
        return (format("_Result(local_volume=%1%, global_volume=%2%, local_energy=%3%)")
                %str_(o.local_volume)
                %str_(o.global_volume)
                %str_(o.local_energy)
        ).str();
    }

    template<> string str_(stm::reservoir::water_value_ const& o) {
        return (format("_Water_value(endpoint_desc=%1%, result=%2%)")
                %str_(o.endpoint_desc)
                %str_(o.result)
        ).str();
    }


    template<> string str_(stm::reservoir const& o) {
        return (format("Reservoir(id=%1%, name=%2%)")
            %str_(o.id)
            %str_(o.name)
        ).str();
    }

    void stm_reservoir() {
        auto r=py::class_<
            stm::reservoir,
            py::bases<hydro_power::reservoir>,
            shared_ptr<stm::reservoir>, 
            boost::noncopyable
        >("Reservoir","Stm reservoir.",py::no_init);
        r
            .def(py::init<int, const string&,const string&, stm::stm_hps_ &>(
                (py::arg("uid"),py::arg("name"),py::arg("json"),py::arg("hps")),
                "Create reservoir with unique id and name for a hydro power system."))


            .add_property("level", &stm::reservoir::level, "Level attributes")
            .add_property("tag", +[](const stm::reservoir& self){return url_tag(self);}, "url tag")
            .def_readonly("volume", &stm::reservoir::volume, "Volume attributes")
            .def_readonly("inflow", &stm::reservoir::inflow, "Inflow attributes")
            .def_readonly("ramping", &stm::reservoir::ramping, "Ramping attributes")
            .def_readonly("water_value", &stm::reservoir::water_value, "Water-value attributes")

            .def("__eq__", &stm::reservoir::operator==)
            .def("__ne__", &stm::reservoir::operator!=)

            .def("flattened_attributes", +[](stm::reservoir& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes")
            ;
        expose_str_repr(r);
        add_proxy_property(r,"volume_level_mapping", stm::reservoir,volume_level_mapping, "Volume capacity description, time-dependent x=[masl], y=[m3]")

        using ReservoirList=std::vector<stm::reservoir_>;
        auto rlist=py::class_<ReservoirList>("ReservoirList", "A strongly typed list of Reservoirs.");
        rlist
                .def(py::vector_indexing_suite<ReservoirList, true>())
                .def("__init__",
                     construct_from<ReservoirList>(py::default_call_policies(),(py::arg("reservoirs"))),
                     "construct from list"
                )
        ;

        {
            py::scope scope_r=r;
            auto rl=py::class_<stm::reservoir::level_, py::bases<>, boost::noncopyable>("_Level", py::no_init);
            rl
                .def_readonly("constraint", &stm::reservoir::level_::constraint, "Level constraint attributes")
            ;
            expose_str_repr(rl);
            _add_proxy_property(rl,"regulation_min", stm::reservoir::level_,regulation_min, "[masl] Lowest regulated water level , time-dependent attribute.")
            _add_proxy_property(rl,"regulation_max", stm::reservoir::level_,regulation_max, "[masl] Highest regulated water level , time-dependent attribute.")
            _add_proxy_property(rl,"realised", stm::reservoir::level_,realised, "[masl] Historical water level, time_series")
            _add_proxy_property(rl,"schedule", stm::reservoir::level_,schedule, "[masl] Level schedule, time_series")
            _add_proxy_property(rl,"result", stm::reservoir::level_,result, "[masl] Level results, time_series")

            auto rv=py::class_<stm::reservoir::volume_, py::bases<>, boost::noncopyable>("_Volume", py::no_init);
            rv
                .def_readonly("constraint", &stm::reservoir::volume_::constraint, "Volume constraint attributes")
                .def_readonly("slack", &stm::reservoir::volume_::slack, "Slack attributes")
            ;
            expose_str_repr(rv);
            _add_proxy_property(rv,"static_max", stm::reservoir::volume_,static_max, "[m3] Maximum regulated volume, time-dependent attribute.")
            _add_proxy_property(rv,"realised", stm::reservoir::volume_,realised, "[m3] Historical volume, time-series")
            _add_proxy_property(rv,"schedule", stm::reservoir::volume_,schedule, "[m3] Volume schedule, time-series")
            _add_proxy_property(rv,"result", stm::reservoir::volume_,result, "[m3] Volume results, time series.")
            _add_proxy_property(rv,"penalty", stm::reservoir::volume_,penalty, "[m3] Volume penalty, time series.")

            auto rwv=py::class_<stm::reservoir::water_value_, py::bases<>, boost::noncopyable>("_WaterValue", py::no_init);
            rwv
                    .def_readonly("result", &stm::reservoir::water_value_::result, "Optimizer detailed watervalue results")
                    ;
            expose_str_repr(rwv);

            _add_proxy_property(rwv,"endpoint_desc", stm::reservoir::water_value_,endpoint_desc, "[money/joule] Fixed water-value endpoint .")
            {
                py::scope scope_rwv=rwv;
                auto rwvr=py::class_<stm::reservoir::water_value_::result_, py::bases<>, boost::noncopyable>("_Result", py::no_init);
                _add_proxy_property(rwvr,"local_volume", stm::reservoir::water_value_::result_,local_volume, "[money/m3] Resulting water-value.")
                _add_proxy_property(rwvr,"global_volume", stm::reservoir::water_value_::result_,global_volume, "[money/m3] Resulting water-value.")
                _add_proxy_property(rwvr,"local_energy", stm::reservoir::water_value_::result_,local_energy, "[money/joule] Resulting water-value.")
                _add_proxy_property(rwvr,"end_value", stm::reservoir::water_value_::result_,end_value, "[money] Resulting water-value at the endpoint.")
                expose_str_repr(rwvr);

            }
            auto ri=py::class_<stm::reservoir::inflow_, py::bases<>, boost::noncopyable>("_Inflow", py::no_init);
            expose_str_repr(ri);
            _add_proxy_property(ri,"schedule", stm::reservoir::inflow_,schedule, "[m3/s] Inflow input , time series.")
            _add_proxy_property(ri,"realised", stm::reservoir::inflow_,realised, "[m3/s] Historical inflow , time series.")
            _add_proxy_property(ri,"result", stm::reservoir::inflow_,result, "[m3/s]  Inflow result , time series.")


            auto rr=py::class_<stm::reservoir::ramping_, py::bases<>, boost::noncopyable>("_Ramping", py::no_init);
            expose_str_repr(rr);
            _add_proxy_property(rr,"level_down", stm::reservoir::ramping_,level_down, "[m/s] Max level change down , time series.")
            _add_proxy_property(rr,"level_up", stm::reservoir::ramping_,level_up, "[m/s] Max level change up , time series.")


            {
                py::scope rl_scope=rl;
                auto rlc=py::class_<stm::reservoir::level_::constraint_, py::bases<>, boost::noncopyable>("_Constraints", py::no_init);
                expose_str_repr(rlc);
                _add_proxy_property(rlc,"min", stm::reservoir::level_::constraint_,min, "[masl] Level constraint minimum, time_series")
                _add_proxy_property(rlc,"max", stm::reservoir::level_::constraint_,max, "[masl] Level constraint maximum, time_series")
            }
            
            {
                py::scope rv_scope=rv;
                auto rvc=py::class_<stm::reservoir::volume_::constraint_, py::bases<>, boost::noncopyable>("_Constraints", py::no_init);
                rvc.def_readonly("tactical", &stm::reservoir::volume_::constraint_::tactical, "Tactical volume constraint attributes");
                expose_str_repr(rvc);
                _add_proxy_property(rvc,"min", stm::reservoir::volume_::constraint_,min, "[m3] Volume constraint minimum, time-series")
                _add_proxy_property(rvc,"max", stm::reservoir::volume_::constraint_,max, "[m3] Volume constraint maximum, time-series")
                
                {
                    py::scope rvc_scope=rvc;
                    auto rvct=py::class_<stm::reservoir::volume_::constraint_::tactical_,py::bases<>, boost::noncopyable>("_Tactical", py::no_init);
                    rvct.def_readonly("min", &stm::reservoir::volume_::constraint_::tactical_::min, "[masl],[money] Tactical minimum volume constraint, PenaltyConstraint");
                    rvct.def_readonly("max", &stm::reservoir::volume_::constraint_::tactical_::max, "[masl],[money] Tactical maximum volume constraint, PenaltyConstraint");
                    expose_str_repr(rvct);
                }

                auto rvs=py::class_<stm::reservoir::volume_::slack_,py::bases<>, boost::noncopyable>("_Slack", py::no_init);
                expose_str_repr(rvs);
                _add_proxy_property(rvs,"lower", stm::reservoir::volume_::slack_,lower,"[m3] Lower volume slack, time-series")
                _add_proxy_property(rvs,"upper", stm::reservoir::volume_::slack_,upper, "[m3] Upper volume slack, time-series")
            }
        }
   }
}
