/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>

#include <boost/format.hpp>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::core::calendar;
    using shyft::time_axis::generic_dt;
    using shyft::time_series::dd::apoint_ts;
    using shyft::energy_market::core::absolute_constraint;
    using shyft::energy_market::core::penalty_constraint;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;

    using boost::format;

    template<class T>
    void py_t_value(const char *tp_name) {
        //string (*str_f)(typename T::element_type const&)=&py_string_of_value_type;
        auto c=py::class_<typename T::element_type,py::bases<>,T>(tp_name,
            doc_intro("Time variable value-type.")
            doc_details(
                "Implemented as a sorted map of tuple (time,value) items.\n"
                "\n"
                "You can assign/replace a new item using:\n"
                ">>> m = t_double_() # create a time-dependent map-type\n"
                ">>> m[time('2018-01-01T00:00:00Z')] = 3.23\n"
                "\n"
                "And iterate over the tuple like this:\n"
                ">>> for i in m:\n"
                ">>>    print(i.key(),i.data())\n"
                "\n"
                "To make a copy of a time-dependent variable, use:\n"
                ">>> m_clone=t_double_(m) # pass in the object to clone in the constructor")
            );
        c
            .def(py::map_indexing_suite<typename T::element_type,true>())
            .def(py::init<const typename T::element_type&>((py::arg("clone")),"create a copy of the object to clone"))
            // implement __call__ to evaluate for time, similar to time series
            .def("__call__", +[](const T& m, const utctime& t) -> typename T::element_type::mapped_type {
                    auto it = std::find_if(m->rbegin(), m->rend(), [&t](const auto& v) -> bool { return v.first <= t; } );
                    return it != m->rend() ? it->second : nullptr;
                },
                (py::args("self"), py::arg("time")),
                doc_intro("Find value for a given time.")
            )
        ;
        expose_str_repr(c);
        // this is what we want:
        // user can create a t_value as t_value({})
        //py_api::iterable_converter().from_python<T>();
    }
 
    void stm_basic_attributes() {
        // expose basic types here
        py_t_value<stm::t_xy_>("t_xy");
        py_t_value<stm::t_xyz_>("t_xyz");
        py_t_value<stm::t_xyz_list_>("t_xyz_list");
        py_t_value<stm::t_turbine_description_>("t_turbine_description");
        auto ac=py::class_<
            absolute_constraint,
            py::bases<>,// absolute_constraint>,
            boost::noncopyable
        >("AbsoluteConstraint",
            doc_intro("A grouping of time series related to an absolute constraint (i.e. infinite cost)"), py::no_init);
        
        _add_proxy_property(ac,"limit", absolute_constraint,limit, "The threshold related to the constraint")
        _add_proxy_property(ac,"flag", absolute_constraint,flag, "Flag indicating whether the constraint is active or not")
        ;
        expose_str_repr(ac);

        auto pc=py::class_<
            penalty_constraint,
            py::bases<>,// shared_ptr<penalty_constraint>,
            boost::noncopyable
        >("PenaltyConstraint",
            doc_intro("A grouping of time-series related to a constraint with a penalty cost"),
            py::no_init);
        
        _add_proxy_property(pc,"limit", penalty_constraint,limit, "The threshold related to the constraint")
        _add_proxy_property(pc,"flag", penalty_constraint,flag, "Flag indicating whether the constraint is active or not")
        _add_proxy_property(pc,"cost", penalty_constraint,cost, "The cost of violating the constraint")
        _add_proxy_property(pc,"penalty", penalty_constraint,penalty, "Incurred cost of violating the constraint")
        
        expose_str_repr(pc);
        
        def_a_wrap<apoint_ts>("_ts");
        def_a_wrap<stm::t_turbine_description_>("_turbine_description");
        def_a_wrap<stm::t_xy_>("_t_xy_");
        def_a_wrap<stm::t_xyz_>("_t_xyz");
        def_a_wrap<stm::t_xyz_list_>("_t_xy_z_list");
        //def_a_wrap<uint16_t>("u16");
        //def_a_wrap<stm::penalty_constraint>("_penalty_constraint");
    }
}
