/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/periodic.h>

namespace shyft::time_series::dd {// dd= dynamic_dispatch version of the time_series library, aiming at python api
struct apoint_ts;
/** @brief periodic_ts is used for providing ts periodic values over a time-axis
*
*/
struct periodic_ts : ipoint_ts {
    typedef shyft::time_series::periodic_ts<gta_t> pts_t;
    pts_t ts;

    periodic_ts(const pts_t &pts):ts(pts) {}
    periodic_ts(const vector<double>& pattern, utctimespan dt, const gta_t& ta) : ts(pattern, dt, ta) {}
    periodic_ts(const vector<double>& pattern, utctimespan dt, utctime pattern_t0,const gta_t& ta) : ts(pattern, dt,pattern_t0,ta) {}
    periodic_ts(const periodic_ts& c) : ts(c.ts) {}
    periodic_ts(periodic_ts&& c) : ts(move(c.ts)) {}
    periodic_ts& operator=(const periodic_ts& c) {
        if (this != &c) {
            ts = c.ts;
        }
        return *this;
    }
    periodic_ts& operator=(periodic_ts&& c) {
        ts = move(c.ts);
        return *this;
    }
    periodic_ts()=default;
    // implement ipoint_ts contract
    ts_point_fx point_interpretation() const override { return ts_point_fx::POINT_AVERAGE_VALUE; }
    void set_point_interpretation(ts_point_fx) override { ; }
    const gta_t& time_axis() const override { return ts.ta; }
    utcperiod total_period() const override { return ts.ta.total_period(); }
    size_t index_of(utctime t) const override { return ts.index_of(t); }
    size_t size() const override { return ts.ta.size(); }
    utctime time(size_t i) const override { return ts.ta.time(i); }
    double value(size_t i) const override { return ts.value(i); }
    double value_at(utctime t) const override { return value(index_of(t)); }
    vector<double> values() const override { return ts.values(); }
    bool needs_bind() const override { return false;}// this is a terminal node, no bind needed
    void do_bind()  override {}
    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const& shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx&ctx) const override;
    string stringify() const override;
    x_serialize_decl();
};


}
x_serialize_export_key(shyft::time_series::dd::periodic_ts);
