/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>


namespace shyft::time_series::dd {// dd= dynamic_dispatch version of the time_series library, aiming at python api

struct apoint_ts;

/** @brief The fast_krls_ts is used for providing krls-filtered ts values over a time-axis, which is based on partitioned time-axes.
*
*
* The @ref ts_point_fx is always POINT_INSTANT_VALUE for the result ts.
*
* @note TODO Write notes here.
* @note WARNING: This file is still in production, not operating yet.
*
*/
struct fast_krls_ts :ipoint_ts {
    const gta_t ta;
    const size_t partition_size;
    const size_t partition_halfoverlap;
    // useful constructors
    fast_krls_ts(gta_t const & ta, size_t partition_size, size_t partition_halfoverlap,time_t krls_dt,double gamma,double tol,size_t krls_sz):ta{ta},partition_size{partition_size},partition_halfoverlap{partition_halfoverlap},krls_dt{krls_dt},gamma{gamma},tol{tol},krls_sz{krls_sz} {}
    
    
    struct split_ta {

        int i{0};// positon in the generator sequence, or actually any state state

        const gta_t ta;
        const size_t partition_size;
        const size_t partition_halfoverlap;

        int n= ta.size()<partition_size ?ta.size() : partition_size;
        int parti_halfoverlap = partition_halfoverlap>partition_size/2 ?partition_size/2 : partition_halfoverlap; // if the total overlap is larger than n_max, then the last time-axis could exceed the end of the total time-axis if ta.size()/n is not dividable without rest.
        int n_ta = ta.size()/n; // number of time-axis

        split_ta(gta_t const & ta, size_t partition_size, size_t partition_halfoverlap):ta{ta},partition_size{partition_size},partition_halfoverlap{partition_halfoverlap} {}//construct.
        int next() {return i++;}// advance sequence

        //first entry in tuple -> i0 = i*n - (i>0 ? parti_halfoverlap :0);// figure out where to start
        //second entry in tuple -> n0 = i+1==n_ta && n+2*parti_halfoverlap<ta.size()?ta.size()-i0 : n_ta==1?n : i == 0?n + parti_halfoverlap : i0+2*n <= ta.size()?n + 2*parti_halfoverlap : ta.size()-i0; // here are four conditions to ensure (1) to reach the end-bound of ta and make last segment(i+1=n_ta) larger than n, (2) the size of ta equal to size of tas.front() if n_ta==1, (3) the first overlap being same size as all other overlaps, and (4) to stop inside bounds and make last segment not smaller than n.
        std::tuple<int,int> value() const {return std::make_tuple(i*n - (i>0 ? parti_halfoverlap :0), i+1==n_ta && n+2*parti_halfoverlap<ta.size()?ta.size()-(i*n - (i>0 ? parti_halfoverlap :0)) : n_ta==1?n : i == 0?n + parti_halfoverlap : (i*n - (i>0 ? parti_halfoverlap :0))+2*n <= ta.size()?n + 2*parti_halfoverlap : ta.size()-(i*n - (i>0 ? parti_halfoverlap :0)));} // return current value
        operator bool () const {return i<n_ta;} // stop condition
    };

    ats_vector v;// we stash up the partitions in this vector. // TODO What is the type of TsVector in cpp?
    for(split_ta g(ta,n_steps_in_partition,n_steps_partition_overlap);g;g.next()) {
        auto [start,steps] = g.value();
        gta_t tax = ta.slice(start, steps);
        apoint_ts ax = a.average(tax).krls_interpolation(krls_dt,gamma,tol,krls_sz); // TODO The variable 'a' is the actual time series that the method is applied to.
        v.push_back(ax);
    }
    auto c=v.forecast_merge(dt*n_steps_partition_overlap,n_steps_in_partition*dt);
    return c;
    x_serialize_decl();

};

}
x_serialize_export_key(shyft::time_series::dd::fast_krls_ts);

