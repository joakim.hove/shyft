/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <memory>
#include <utility>
#include <map>
#include <array>

#include <shyft/core/core_serialization.h>

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/point_ts.h>

namespace shyft::time_series::dd {// dd= dynamic_dispatch version of the time_series library, aiming at python api
using namespace shyft::core;
using std::shared_ptr;
using std::make_shared;
using std::vector;
using std::string;
using std::runtime_error;
using std::size_t;
using std::move;
using std::forward;
using std::map;

/**
    time-series math to be exposed to python

    This provide functionality like

    a = TimeSeries(..)
    b = TimeSeries(..)
    c = a + 3*b
    d = max(c,0.0)

    implementation strategy

    provide a type apoint_ts, that always appears as a time-series.

    It could however represent either a
        point_ts<time_axis:generic_dt>
    or an expression
        like a abin_op_ts( lhs,op,rhs)

    Then we provide operators:
    apoint_ts bin_op a_point_ts
    and
    double bin_op a_points_ts
*/

/** @brief generic time-axis

    Using the time_axis library and concept directly.
    This time-axis is generic, but currently only dense-types
    fixed_dt (fastest)
    calendar_dt( quite fast, but with calendar semantics)
    point_dt ( a point at start of each interval, plus the end point of the last interval, could give performance hits in some scenarios)

*/
using gta_t=time_axis::generic_dt;
using gts_t=point_ts<gta_t>;
using rts_t=point_ts<time_axis::fixed_dt>;
using intv_t=vector<int64_t>; ///< vector<int64_t> to ensure expose to python works same linux and win
struct ipoint_ts;

/** @brief ts-expression evalutation context
*
* @detail The purpose of this class is to establish an evaulation context,
* that keeps track of time-series during evaluation of a direct represented
* time-series expression, so that :
*
*  a) same series/expression is evaluated only once
*
*  b) that an expression tree nodes  is evaluated reducing each of it's siblings into
*      a concrete time-series (no expression), recursively,  so such that the tree
*     collapses into a concrete representation of its final values.
*
* The eval_ctx is used mutable in the ipoint_ts.evaluate(..) method,
* to ref_count/register results.
*
* It's primary use is in the tsv.flatten algo, executed multithreaded by the dtss-server'
*
*/
struct eval_ctx {
    map<ipoint_ts const *,shared_ptr<const ipoint_ts>> evaluated; ///< map of node pointer to its evaluated gpoint_ts
    map<ipoint_ts const *,size_t> ref_count; ///< ref count of node, so that we know which results we need to keep

    size_t ref_counting(ipoint_ts const* ts_this) {
        return  ++ref_count[ts_this];
    }

    bool is_evaluated(ipoint_ts const* ts_this) const {return evaluated.find(ts_this)!=evaluated.end();}

    void  register_ts(ipoint_ts const *ts_this,shared_ptr<const ipoint_ts> const& expr_result) {
        if(ref_count[ts_this]>1) // only register ts with ref-count > 1, minimze use of memory
            evaluated[ts_this]=expr_result;
    }
    /** @brief evaluates ts-argument 
    *
    * Compute the result of the ts-argument, and returns a  shared_ptr<ipoint_ts> that is
    * the concrete evaluated result.
    * @param ts the ref to the expression( could be a terminal as well)  to be evaluated
    * @returns a shared_ptr<ipoint_ts> that promise to be of gpoint_ts.
    */
    shared_ptr<const ipoint_ts> evaluate(shared_ptr<const ipoint_ts> const &ts);
};

/** @brief A virtual abstract interface (thus the prefix i) base for point_ts
*
* There are three defining properties of a time-series:
*
* 1. The @ref time_axis provided by the time_axis() method
*    or direct time-axis information as provided by the
*    total_period(),size(), time(i), index_of(t) methods
*
* 2. The values, provided by values(),
*    or any of the value(i),value_at(utctime t) methods.
*
* 3. The @ref ts_point_fx
*    that determines how the points should be projected to f(t)
* 
* Some background for performance, scalability, threading, algorithms(ODR) considerations
* 
* 1. We would like value(i), value_at(t) to be :
*     [X] fast and minimial, as in reqursive partial compute arount parameters i,t
*     [X] const-ness, so that we can safely multi-thread it
* 
* 2. We would like bulk-compute, like .values(), and most important .evalute(ctx) to be :
*     [X] fast and efficient, as in recursive depth first, imploding the computational three
*     [X] const-ness, for threading the computations (eval_ctx helps here to keep thread-local context)
* 
* 3. Algorithms should be defined one place
* 
* Requirement 1 2 and 3 is partly contradicting, since writing fast algo for (2), means we
* need to 'get rid of' typical one-time costly functions.
* E.g. time-axis.index_of, common-sub-expressions only evaluated once etc.
*  -and the fact that we need to implode the computation tree to make bulk compute
* as efficient as we need.
* 
* While imploding the computational tree, each computation node should be computed using
* the static-dispatch (direct memory access, inplace operations etc.).
* .. so we need algorithms that are agnostic to 'ts'-interface
* .. at the same time, when we invoke with with static-dispatched, direct-memory-access
* .. we need to give the c++ compiler the chance to deliver the ultimate code.
* 
* The terminal nodes (ts-constants), are indeed point_ts with time-axis = time_axis::generic_dt,
* or alternatively bound place-holders of the same (when bound they hold a gts_t).
* 
* Due to shared-owner-ship as well as interfaces to python, we extensively use shared_ptr
* for this.
* So while algorithms in (1) rely on virtual dispatch and ipoint_ts, and will be fast
* since the use-pattern is like a few values (usually).
* The algorithms in (2), need a completely different approach when it comes to 
* 
* [X] accessing the time-axis (unwrap from generic_dt to its real-representation, fixed_dt etc.)
* and then using the optimized unchecked methods ._time(i) , .period(i)
* [X] accessing the values (as in direct access to vector<double> )
* 
* When bulk-computing the expression-tree, the result of siblings expressions of a node (e.g. bin-op), 
* can be one of:
*   a) const reference to terminal-node(a constant or bound constant ts) (gpoint_ts ->gts_t)
*   b) const reference to a common sub-expression result (gpoint_ts -> gts_t)
*   c) a temporary (mutable reference to) result (gpoint_ts -> gts_t)

* In case (c), we _can_ do inplace computations, which is faster, and less memory expensive
* for case (a) and (b), we should enforce const'ness through types.
*
* The eval_ctx is the thread-context we use to perform bulk-computations, it keeps the
* thread-local context,(one for each computational thread).
*/
struct ipoint_ts {
    typedef gta_t ta_t;// time-axis type
    ipoint_ts() =default; // ease boost serialization
    virtual ~ipoint_ts()=default;

    /** @brief clone/copy the expr ts
    * 
    * Make a smart copy of the ts, (could be expression) such that:
    * 
    *  - unbound parts of an expression tree is copied
    *  - bound parts is just refererenced
    * 
    * e.g.: 
    *   - if ts.needs_bind() then construct a copy of ts
    *   - terminals (e.g. gpoint_ts) should throw.
    * for a binary_op: if only lhs.needs_bind(), then copy lhs, inc-ref rhs. etc.
    * 
    */
    virtual shared_ptr<ipoint_ts> clone_expr() const=0;
    
    virtual ts_point_fx point_interpretation() const =0;
    virtual void set_point_interpretation(ts_point_fx point_interpretation) =0;

    /** @return Returns the effective time-axis of the timeseries
    */
    virtual const gta_t& time_axis() const =0;

    /** @return the total period of time_axis(), same as time_axis().total_period()
    */
    virtual utcperiod total_period() const=0;

    /** @return the index_of(utctime t), using time_axis().index_of(t) @ref time_axis::fixed_dt
    */
    virtual size_t index_of(utctime t) const=0; ///< we might want index_of(t,ix-hint..)

    /** @return number of points that descr. y=f(t) on t ::= period
    */
    virtual size_t size() const=0;

    /** @return time_axis().time(i), the start of the i'th period in the time-axis
    */
    virtual utctime time(size_t i) const=0;

    /** @return the value at the i'th period of the time-axis
    */
    virtual double value(size_t i) const=0;

    /** @return the f(t) at the specified time t, @note if t outside total_period() nan is returned
    */
    virtual double value_at(utctime t) const =0;

    /** @return the values, one for each of the time_axis() intervals as a vector
    */
    virtual vector<double> values() const =0;

    /** @return a human readable string of the time-series, or expression*/
    virtual string stringify() const =0;

    /** for internal computation and expression trees, we need to know *if*
    * there are unbound symbolic ts in the chain of this ts
    * We know that a point-ts never do not need a binding, but
    * all others are expressions of ts, and could potentially
    * needs a bind (if it has an unbound symbolic ts in it's siblings)
    */
    virtual bool needs_bind() const =0;


    /** propagate do_bind to expression tree siblings(if any)
    * then do any needed binding stuff needed for the specific class impl.
    */
    virtual void do_bind()=0;

    /** @brief evaluate/flatten the time-series to a concrete time-seres
    *
    * If the time-series is an expression, evaluate/flatten the expression
    * using depth first.
    * The eval_ctx keeps a list of already evaluated ipoint_ts (this), so first do a lookup
    * using ctx.already_don(this) and if true return ctx.evaluated[this],
    * else to a deept first of the dependents, then with those inplace
    *  clone it self to a new, with those new its as dependents,
    *  then evaluate .values(), and create resulting time-series.
    * @param ctx the evaluation context
    * @param shared_this shared_ptr of this, needed for zero-copy multi-ref'd terminals
    *
    */
    virtual shared_ptr<const ipoint_ts> evaluate(eval_ctx& ctx, shared_ptr<const ipoint_ts> const& shared_this) const=0;
    
    /** @brief prepare evaluate
    *
    * Currently this involve ctx.register_ts(this_ts), and then pass on to it's siblings
    * e.g. for bin-op, pass on rhs.prepare(ctx), lhs.prepare(ctx)
    */
    virtual void prepare(eval_ctx&ctx) const=0;
    // to be removed:
    point get(size_t i) const {return point(time(i),value(i));}
    x_serialize_decl();
};
using ipoint_ts_=shared_ptr<ipoint_ts>;
using ipoint_ts_c = shared_ptr<const ipoint_ts>;

//-- utilities and shorthands to ensure we can keep things const by default, and mutable by explicit calls --

using ipoint_ts_ref = ipoint_ts_c;///< use const T for references inside clases, then wref(ts) to get a write-able handle

/** get a mutable shared ptr<ipoint_ts> from  shared_ptr<const ipoint_ts> effectively a shorthand for const pointer-cast, inc-ref is called! */
inline shared_ptr<ipoint_ts> wref(shared_ptr<const ipoint_ts>&ts) noexcept {return std::const_pointer_cast<ipoint_ts>(ts);}

/** de-reference a and return a mutable reference of the shared_ptr<const ipoint_ts>, e.g. a const cast, no inc-ref */
inline ipoint_ts& dref(shared_ptr<const ipoint_ts>&ts) noexcept {return *const_cast<ipoint_ts*>(ts.get());}

/** @brief ts_as<some_ts_type> -> some_ts_type const *
* @detail returns a pointer
* 
*/
template <class T> 
inline T const * ts_as( shared_ptr<const ipoint_ts> const&p) noexcept{
    ipoint_ts const *ts=p.get();
    if(ts) return dynamic_cast<T const*>(ts);
    return nullptr;
}

template <class T> 
inline T * ts_as_mutable( shared_ptr<const ipoint_ts> const&p) noexcept{
    ipoint_ts const *ts=p.get();
    if(ts) return dynamic_cast<T *>(const_cast<ipoint_ts*>(ts));//OBS! intentionally, get a mutable pointer
    return nullptr;
}


inline ipoint_ts_ref eval_ctx::evaluate(ipoint_ts_ref const &ts) {
    if(!ts) return nullptr;
    return ts->evaluate(*this,ts);
}
/** @brief Enumerates fill policies for time-axis extension.
*/
enum extend_ts_fill_policy:int8_t {
    EPF_NAN,   /**< Fill any gap between the time-axes with NaN. */
    EPF_LAST,  /**< At a gap, keep the last time-axis value through a gap. */
    EPF_FILL,  /**< Fill any gap between the time-axes with a given value. */
};

/** @brief Enumerates split policies for time-axis extension.
    */
enum extend_ts_split_policy:int8_t {
    EPS_LHS_LAST,   /**< Split at the last value of the lhs ts. */
    EPS_RHS_FIRST,  /**< Split at the first value of the rhs ts. */
    EPS_VALUE,      /**< Split at a given time-value. */
};

/** @brief derivative_method
 * 
 * Enum to select how to compute the derivative of time-series
 */
enum class derivative_method:int8_t {
    default_diff,
    forward_diff,
    backward_diff,
    center_diff
};

/** returns true if a are similar to b, including both is nan */
inline bool nan_equal(double a, double b, double abs_e) {
    if(!std::isfinite(a) && !std::isfinite(b)) return true;
    return fabs(a-b) <= abs_e;
}

}
namespace shyft::time_series {
    inline size_t hint_based_search(const dd::ipoint_ts& source, const utcperiod& p, size_t i) {
        return source.time_axis().index_of(p.start, i);
    }
}
x_serialize_export_key(shyft::time_series::dd::ipoint_ts);
