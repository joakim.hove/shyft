/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/gpoint_ts.h>

namespace shyft::time_series::dd {// dd= dynamic_dispatch version of the time_series library, aiming at python api
struct apoint_ts;



/** @brief ats_vector represents a list of time-series, support math-operations.
*
* Supports handling and math operations for vectors of time-series.
* Especially convinient in python due to compact notation and speed.
*/
typedef vector<apoint_ts> ats_vec;
struct ats_vector:ats_vec  {  // inheritance from vector, to get most parts for free
    // constructor stuff that needs to be complete for boost::python
    ats_vector()=default;
    ats_vector(ats_vec const&c):ats_vec(c) {}
    ats_vector(ats_vec&& c):ats_vec(move(c)) {}
    ats_vector(ats_vector const&c):ats_vec(c) {}
    explicit ats_vector(size_t sz):ats_vec(sz) {}
    ats_vector(std::initializer_list<apoint_ts> l):ats_vec(l){}
    ats_vector(ats_vector&&c):ats_vec(move(c)) {}
    ats_vector& operator=(ats_vector const&c) {
        if(this !=&c) {
            ats_vec::operator=(c);
        }
        return *this;
    }
    ats_vector& operator=(ats_vector&&c) {
        ats_vec::operator=(c);
        return *this;
    }
    //-- minimal iterator support in order to expose it as vector
    template <class vec_iter>
    ats_vector(vec_iter&&  b,vec_iter&&  e):ats_vec(forward<vec_iter>(b),forward<vec_iter>(e)) {}
    auto begin() {return ats_vec::begin();}
    auto begin() const {return ats_vec::begin();}
    auto end() {return ats_vec::end();}
    auto end() const {return ats_vec::end();}
    void reserve(size_t x) {ats_vec::reserve(x);}
    apoint_ts& operator()(size_t i) {return *(begin()+i);}
    apoint_ts const & operator()(size_t i) const {return *(begin()+i);}
    /** support operator! bool  to let an empty tsv  evaluate to */
    bool operator !() const { // can't expose it as op, due to math promotion
        return !(size() > 0);
    }
    bool operator==(ats_vector const&o) const;
    bool operator!=(ats_vector const&o) const {return !operator==(o);}
    vector<double> values_at_time(utctime t) const {
        vector<double> r;r.reserve(size());
        for (auto const &ts : *this ) r.push_back(ts(t));
        return r;
    }
    vector<double> values_at_time_i(int64_t t) const {
        return values_at_time(seconds(t));
    }
    ats_vector percentiles(gta_t const &ta,intv_t const& percentile_list) const;
    ats_vector percentiles_f(time_axis::fixed_dt const&ta,intv_t const& percentile_list) const {
        return percentiles(gta_t(ta),percentile_list);
    }

    /**
     * @brief min..max value range for the specified period
     * @details computes the functional,f(t), min/max range in the supplied period.
     * The purpose of the function is to provide help for determine plot-axis,
     * for display of time-series, thus best effort is done to provide usable
     * information back.
     * @param p gives the time-range for the function
     *
     * @return vector where 1st element is min, 2nd is max.
     */
    vector<double> value_range(utcperiod p) const;

    ats_vector slice(intv_t const& slice_spec) const {
        if(slice_spec.size()==0) {
            return ats_vector(*this);// just a clone of this
        } else {
            ats_vector r;for(auto ix:slice_spec) r.push_back(begin()[ix]);
            return r;
        }
    }

    ats_vector extend_ts(
        apoint_ts const & ta,
        extend_ts_split_policy split_policy, extend_ts_fill_policy fill_policy,
        utctime split_at, double fill_value
    ) const {
        ats_vector r; r.reserve(this->size());
        for ( auto const & ts : *this )
            r.push_back(ts.extend(ta, split_policy, fill_policy, split_at, fill_value));
        return r;
    }
    ats_vector extend_vec(
        ats_vector const & ts_vec,
        extend_ts_split_policy split_policy, extend_ts_fill_policy fill_policy,
        utctime split_at, double fill_value
    ) const {
        if ( this->size() != ts_vec.size() ) throw runtime_error("vector size mismatch, must be of the same size");
        ats_vector r; r.reserve(this->size());
        auto lhs_it = this->cbegin(); auto rhs_it = ts_vec.cbegin();
        while ( lhs_it != this->cend() ) {
            r.push_back(lhs_it->extend(*rhs_it, split_policy, fill_policy, split_at, fill_value));
            lhs_it++; rhs_it++;
        }
        return r;
    }

    ats_vector abs() const {
        ats_vector r; r.reserve(size()); for (auto const &ts : *this) r.push_back(ts.abs()); return r;
    }
    ats_vector average(gta_t const&ta) const {
        ats_vector r;r.reserve(size());for(auto const &ts:*this) r.push_back(ts.average(ta)); return r;
    }
    ats_vector statistics(gta_t const&ta,int64_t p) const {
        ats_vector r;r.reserve(size());for(auto const &ts:*this) r.push_back(ts.statistics(ta,p)); return r;
    }
    ats_vector integral(gta_t const&ta) const {
        ats_vector r;r.reserve(size());for(auto const &ts:*this) r.push_back(ts.integral(ta)); return r;
    }
    ats_vector accumulate(gta_t const&ta) const {
        ats_vector r;r.reserve(size());for(auto const &ts:*this) r.push_back(ts.accumulate(ta)); return r;
    }
    ats_vector derivative(derivative_method dm) const {
        ats_vector r;r.reserve(size());for(auto const &ts:*this) r.push_back(ts.derivative(dm)); return r;
    }
    ats_vector time_shift(utctimespan delta_t) const {
        ats_vector r;r.reserve(size());for(auto const &ts:*this) r.push_back(ts.time_shift(delta_t)); return r;
    }
    ats_vector use_time_axis_from(const apoint_ts&o) const {
        ats_vector r;r.reserve(size());for(auto const &ts:*this) r.push_back(ts.use_time_axis_from(o)); return r;
    }
    ats_vector min(double x) const {
        ats_vector r;r.reserve(size());for (auto const &ts : *this) r.push_back(ts.min(x)); return r;
    }
    ats_vector max(double x) const {
        ats_vector r;r.reserve(size());for (auto const &ts : *this) r.push_back(ts.max(x)); return r;
    }
    ats_vector min(apoint_ts const& x) const {
        ats_vector r;r.reserve(size());for (auto const &ts : *this) r.push_back(ts.min(x)); return r;
    }
    ats_vector max(apoint_ts const& x) const {
        ats_vector r;r.reserve(size());for (auto const &ts : *this) r.push_back(ts.max(x)); return r;
    }
    ats_vector min(ats_vector const& x) const;
    ats_vector max(ats_vector const& x) const;

    ats_vector pow(double x) const {ats_vector r;r.reserve(size());for (auto const &ts : *this) r.push_back(ts.pow(x)); return r;}
    ats_vector pow(apoint_ts const& x) const {ats_vector r;r.reserve(size());for (auto const &ts : *this) r.push_back(ts.pow(x)); return r;}
    ats_vector pow(ats_vector const& x) const;
    ats_vector repeat(gta_t const&ta) const {ats_vector r;r.reserve(size());for (auto const &ts : *this) r.push_back(ts.repeat(ta)); return r;}
    ats_vector log() const {
        ats_vector r;
        r.reserve(size());
        for (auto const & ts : *this) {
            r.push_back(ts.log());
        }
        return r;
    }

    apoint_ts forecast_merge(utctimespan lead_time,utctimespan fc_interval) const;
    apoint_ts sum() const;
    ats_vector average_slice(utctimespan t0_offset,utctimespan dt, int n) const ;
    double nash_sutcliffe(apoint_ts const &obs,utctimespan t0_offset,utctimespan dt, int n) const ;
    ats_vector inside(double min_v,double max_v,double nan_v, double inside_v, double outside_v) const;
    ats_vector transform_spline(const std::vector<double>& knots, const std::vector<double>& coeff, std::size_t degree) const;
    ats_vector transform(const xy_point_curve& xy, interpolation_scheme scheme) const;
    ats_vector clone_expr() const {
        ats_vector r;
        r.reserve(size());
        for(auto const&ts:*this) r.push_back(ts.clone_expr());
        return r;
    }
    x_serialize_decl();
};
// quantile-mapping
ats_vector quantile_map_forecast(vector<ats_vector> const & forecast_set,vector<double> const& set_weights,ats_vector const& historical_data,gta_t const&time_axis,utctime interpolation_start, utctime interpolation_end=no_utctime, bool interpolated_quantiles=false);
// multiply operators
ats_vector operator*(ats_vector const &a,double b);
ats_vector operator*(double a,ats_vector const &b);
ats_vector operator*(ats_vector const &a,ats_vector const& b);
ats_vector operator*(ats_vector::value_type const &a,ats_vector const& b);
ats_vector operator*(ats_vector const& b,ats_vector::value_type const &a);


// divide operators
ats_vector operator/(ats_vector const &a,double b);
ats_vector operator/(double a,ats_vector const &b);
ats_vector operator/(ats_vector const &a,ats_vector const& b);
ats_vector operator/(ats_vector::value_type const &a,ats_vector const& b);
ats_vector operator/(ats_vector const& b,ats_vector::value_type const &a);

// add operators
ats_vector operator+(ats_vector const &a,double b);
ats_vector operator+(double a,ats_vector const &b);
ats_vector operator+(ats_vector const &a,ats_vector const& b);
ats_vector operator+(ats_vector::value_type const &a,ats_vector const& b);
ats_vector operator+(ats_vector const& b,ats_vector::value_type const &a);

// sub operators
ats_vector operator-(const ats_vector& a);

ats_vector operator-(ats_vector const &a,double b);
ats_vector operator-(double a,ats_vector const &b);
ats_vector operator-(ats_vector const &a,ats_vector const& b);
ats_vector operator-(ats_vector::value_type const &a,ats_vector const& b);
ats_vector operator-(ats_vector const& b,ats_vector::value_type const &a);

// max-min func overloads (2x!)
ats_vector min(ats_vector const &a, double b);
ats_vector min(double b, ats_vector const &a);
ats_vector min(ats_vector const &a, apoint_ts const& b);
ats_vector min(apoint_ts const &b, ats_vector const& a);
ats_vector min(ats_vector const &a, ats_vector const &b);

ats_vector max(ats_vector const &a, double b);
ats_vector max(double b, ats_vector const &a);
ats_vector max(ats_vector const &a, apoint_ts const & b);
ats_vector max(apoint_ts const &b, ats_vector const &a);
ats_vector max(ats_vector const &a, ats_vector const & b);

ats_vector pow(ats_vector const &a, double b);
ats_vector pow(double b, ats_vector const &a);
ats_vector pow(ats_vector const &a, apoint_ts const & b);
ats_vector pow(apoint_ts const &b, ats_vector const &a);
ats_vector pow(ats_vector const &a, ats_vector const & b);

ats_vector log(ats_vector const & a);
    ///< percentiles, need to include several forms of time_axis for python
vector<apoint_ts> percentiles(const vector<apoint_ts>& ts_list,const gta_t & ta,const intv_t& percentiles);
vector<apoint_ts> percentiles(const vector<apoint_ts>& ts_list,const time_axis::fixed_dt & ta,const intv_t& percentiles);

/** @brief clip all time-series in a tsvector to specified clip_to_period
*
*  @see clip_to_period for apoint_ts for spec/algo and context (dtss server, before serialize stage)
*/
ats_vector clip_to_period(ats_vector const& tsv, utcperiod p);


}
x_serialize_export_key(shyft::time_series::dd::ats_vector);
