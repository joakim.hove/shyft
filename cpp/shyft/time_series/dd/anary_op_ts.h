/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/fx_merge.h>

namespace shyft::time_series::dd {// dd= dynamic_dispatch version of the time_series library, aiming at python api

/** @brief The n-ary operation for type ts op ts op ... ts.
*
* This is the "multi argument" companion of abin_op_ts. The n-ary operation is lazy just as bin_op, and
* only keep the reference to the list of args that are of the @ref apoint_ts type.
*
* The operation is of @ref nary_op_t, and "merge" is currently the only implemented
* operation. This operation will merge the args using forecast_merge_fx
*
* As per definition this class implements the @ref ipoint_ts interface, and
* the time-axis of type @ref gta_t is currently computed in the constructor.
* This could take some cpu if the time-axis is of type point_dt, so we could
* consider working some more on the internal algorithms to avoid this.
*
* The @ref ts_point_fx is taken from the first args, but can be overridden by the user.
*
*/
enum struct nary_op_t : int64_t {
    OP_NONE,
    OP_MERGE,
    OP_ADD,
};

void find_ts_bind_info(ipoint_ts_ref const &its, std::vector<ts_bind_info>&r); // TODO, this is ugly, but nary_op_t::find_ts_bind_info currently needs it

struct anary_op_ts:ipoint_ts {
    vector<apoint_ts> args;
    nary_op_t op=nary_op_t::OP_NONE;
    gta_t ta;
    ts_point_fx fx_policy=POINT_AVERAGE_VALUE;
    utctimespan lead_time{0};
    utctimespan fc_interval{seconds(6*60*60)};
    bool bound=false;

    ts_point_fx point_interpretation() const override {
        return fx_policy;
    }
    void set_point_interpretation(ts_point_fx x) override {
        fx_policy=x;
    }

    void local_do_bind() ;

    anary_op_ts();
    anary_op_ts(const vector<apoint_ts> &args,nary_op_t op, utctimespan lead_time = seconds(0), utctimespan fc_interval = seconds(6*60*60));
private:
    anary_op_ts(vector<apoint_ts> args, nary_op_t op,gta_t const& ta);
public:
    void bind_check() const {
        if(!bound)
            throw runtime_error("attempting to use unbound timeseries, context anary_op_ts");
    }
    utcperiod total_period() const override {
        return time_axis().total_period();
    }
    gta_t const& time_axis() const override {
        bind_check();
        return ta;
    }// combine lhs,rhs
    size_t index_of(utctime t) const override {
        return time_axis().index_of(t);
    }
    size_t size() const override {
        return time_axis().size();
    }// use the combined ta.size();
    utctime time( size_t i) const override {
        return time_axis().time(i);
    } // return combined ta.time(i)
    double value_at(utctime t) const override;
    double value(size_t i) const override;// return op( lhs(t), rhs(t)) ..
    std::vector<double> values() const override;
    bool needs_bind() const override;
    void do_bind() override ;
    ipoint_ts_ref evaluate ( eval_ctx& c, const ipoint_ts_ref& ) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx&ctx) const override;
    void find_ts_bind_info(vector<ts_bind_info>& r) const {
        for(auto const & a: args) shyft::time_series::dd::find_ts_bind_info(a.ts, r);
    }
    string stringify() const override;
    x_serialize_decl();

};

}
x_serialize_export_key(shyft::time_series::dd::anary_op_ts);
