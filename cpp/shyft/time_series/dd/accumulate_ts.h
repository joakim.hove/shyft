/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/fx_average.h>

namespace shyft::time_series::dd {// dd= dynamic_dispatch version of the time_series library, aiming at python api

struct apoint_ts;

/** @brief The accumulate_ts is used for providing accumulated(integrated) ts values over a time-axis
*
* Given a any ts, concrete, or an expression, provide the true accumulated values,
* defined as area under non-nan values of the f(t) curve,
* on the intervals points as provided by the specified time-axis.
*
* The value at the i'th point of the time-axis is given by:
*
*   integral of f(t) dt from t0 to ti ,
*
*   where t0 is time_axis.period(0).start, and ti=time_axis.period(i).start
*
* using the f(t) interpretation of the supplied ts (linear or stair case).
*
* @note The value at t=t0 is 0.0 (by definition)
* @note The value of t outside ta.total_period() is nan
*
* The @ref ts_point_fx is always POINT_INSTANT_VALUE for the result ts.
*
* @note if a nan-value intervals are excluded from the integral and time-computations.
*       E.g. let's say half the interval is nan, then the true average is computed for
*       the other half of the interval.
*
*/
struct accumulate_ts :ipoint_ts {
    gta_t ta;
    ipoint_ts_ref ts;
    // useful constructors
    accumulate_ts(gta_t&& ta, const apoint_ts& ats) :ta(move(ta)), ts(ats.ts) {}
    accumulate_ts(gta_t&& ta, apoint_ts&& ats) :ta(move(ta)), ts(move(ats.ts)) {}
    accumulate_ts(const gta_t& ta, apoint_ts&& ats) :ta(ta), ts(move(ats.ts)) {}
    accumulate_ts(const gta_t& ta, const apoint_ts& ats) :ta(ta), ts(ats.ts) {}
    accumulate_ts(const gta_t& ta, ipoint_ts_ref const& ts) :ta(ta), ts(ts) {}
    accumulate_ts(gta_t&& ta, ipoint_ts_ref const& ts) :ta(move(ta)), ts(ts) {}
    // std copy ct and assign
    accumulate_ts()=default;
    // implement ipoint_ts contract:
    ts_point_fx point_interpretation() const override { return ts_point_fx::POINT_INSTANT_VALUE; }
    void set_point_interpretation(ts_point_fx /*point_interpretation*/) override { ; }// we could throw here..
    const gta_t& time_axis() const override { return ta; }
    utcperiod total_period() const override { return ta.total_period(); }
    size_t index_of(utctime t) const override { return ta.index_of(t); }
    size_t size() const override { return ta.size(); }
    utctime time(size_t i) const override { return ta.time(i); };
    double value(size_t i) const override {
        if (i>ta.size())
            return nan;
        if (i == 0)// by definition,0.0 at i=0
            return 0.0;
        size_t ix_hint = 0;// assume almost fixed delta-t.
        utctimespan tsum;
        double x= accumulate_value(*ts, utcperiod(ta.time(0), ta.time(i)), ix_hint, tsum, ts->point_interpretation() == ts_point_fx::POINT_INSTANT_VALUE);
        return isfinite(x)?x:0.0;
    }
    double value_at(utctime t) const override {
        // return true accumulated value at t
        if (!ta.total_period().contains(t))
            return nan;
        if (t == ta.time(0))
            return 0.0; // by definition
        utctimespan tsum;
        size_t ix_hint = 0;
        double x= accumulate_value(*ts, utcperiod(ta.time(0), t), ix_hint, tsum, ts->point_interpretation() == ts_point_fx::POINT_INSTANT_VALUE);// also note: average of non-nan areas !;
        return isfinite(x)?x:0.0;
    }
    vector<double> values() const override {
        utctime tsum;
        size_t ix_hint = 0;
        double a=0.0;
        vector<double> r; r.reserve(ta.size());
        auto lin= ts->point_interpretation() == ts_point_fx::POINT_INSTANT_VALUE;
        for(size_t i=0;i<ta.size();++i) {
            r.push_back(a);
            auto x=accumulate_value(*ts, ta.period(i), ix_hint, tsum,lin);
            if(isfinite(x)) a+=x;
        }
        return r;
    }
    bool needs_bind() const override { return ts->needs_bind();}
    void do_bind() override {dref(ts).do_bind();}
    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx&ctx) const override;
    // to help the average function, return the i'th point of the underlying timeseries
    //point get(size_t i) const {return point(ts->time(i),ts->value(i));}
    string stringify() const override;
    x_serialize_decl();

};

}
x_serialize_export_key(shyft::time_series::dd::accumulate_ts);
