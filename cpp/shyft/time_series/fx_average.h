/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/common.h>

namespace shyft::time_series {

    using std::vector;
    using std::isfinite;
    using std::max;
    using std::min;
    using std::string;
    using core::utcperiod;
    using core::utctime;
    using core::utctimespan;
    using core::to_seconds;

    // compute in ticks is faster than seconds
    // area needs to be scaled down to units of seconds
    // we use _a_seconds for that, and _to_ticks to
    inline double _a_seconds(const double& a) {return a/utctime::period::den;}
    inline int64_t _to_ticks(const utctime&t) {return t.count();}

    /** @brief compute non_nan_integral|average for a linear interpreted time-series
      *
      * For each interval in the specified time-axis (continuous)
      * compute the non-nan integral|average in the interval.
      *
      * The points in the supplied time-series is interpreted according
      * to it's ts_point_fx policy.
      *
      *  #) stair_case|POINT_AVERAGE_VALUE:
      *     it represents the average value of the interval
      *     so it's value
      *       f(t) = c for t in [start..end>
      *
      *     f(t) is nan for all points outside ts.total_period()
      *
      *  #) linear| POINT_INSTANT_VALUE:
      *     the point represent the value at the time specified
      *     and f(t) is *linear between* points:
      *
      *       f(t) = a*t + b, for t in [start..end>
      *              where a,b describes the straight line
      *              between points (start,f(start)) (end,f(end))
      *              where we require:
      *               1. a finite left-hand value of f(start)
      *               2. a finite right-hand value of f(end)
      *
      *     first-point, last-point and nan-point considerations:
      *
      *     from the above definition of interval values:
      *     f(t) is nan before the first non-nan point (t,v).
      *     f(t) is nan immediately after the last non-nan point(t,v)
      *     f(t) *left-hand value* is nan if the following point is nan
      *
      *     note: from this follows that you need two-points to have
      *           a non-nan value for linear type of integral
      *
      *     e.g
      *
      *      ^
      *      |      o  x
      *      |    /       o
      *      |  o          \
      *      |              o
      *      ...[---].....[-].......>
      *
      *      The f(t) contributes to the
      *      non-nan integral values only
      *      for where it is defined.
      *      Also note that the left-hand side values at the end of
      *      intervals, or last point, is considered as nan.
      *
      *  Performance consideration/intentions:
      *  this algorithm will call
      *   ts.index_of() just once
      *   and ts.get(i) just once for each point i needed.
      *
      * @tparam TA is the time-axis type to compute values for
      *        The required signatures is:
      *
      *      #) .size() ->size_t
      *      #) .period(size_t i) -> utcperiod
      *
      * @tparam TS is the point-source that we are integrating/averaging
      *        The required signature is:
      *
      *      #) .size() ->size_t : number of points in the point source
      *      #) .index_of(utctime t)->size_t: left-hand index of point at t
      *      #) .get(size_t i) ->point: time,value of the i'th point
      *
      *
      * @param ta the time-axis to compute values for
      * @param ts the point source to use for computation
      * @param avg if true compute the average else compute integral
      * @return a vector<double>[ta.size()] with computed values, integral or true average
     */

    template < class TA,class TS>
    vector<double> accumulate_linear(const TA&ta, const TS& ts, bool avg) {
        vector<double> r(ta.size(),nan);
        // all good reasons for quitting early goes here:
        if(    ta.size()==0
            || ts.size() < 2 // needs two points, otherwise ->nan
            || ts.time(0) >= ta.total_period().end  // entirely after ->nan
            || ts.time(ts.size()-1) <= ta.total_period().start) // entirely before ->nan
            return r;
        // s= start point in our algorithm
        size_t s=ts.index_of(ta.period(0).start);
        if(s == string::npos) // ts might start after our time-axis begin
            s=0;// no prob. then we start where it begins.
        point s_p{ts.get(s)};
        bool s_finite{isfinite(s_p.v)};
        // e = end point for a partition in our algorithm
        size_t e{0};
        point e_p;
        bool e_finite{false};
        const size_t n=ts.size();
        double a{0};// the constants for line
        double b{0};// f(t) = a*t + b, computed only when needed

        for(size_t i=0;i<ta.size();++i) {
            double area{0.0}; // integral of non-nan f(x), area
            int64_t t_sum{0};  // sum of non-nan time-axis
            const auto p {ta.period(i)};

            //---- find first finite point of a partition
            search_s_finite: // we loop here from advance_e_point if we hit a nan
                while(!s_finite) {
                    ++s;
                    if(s+1>=n) {// we are out of points searching for non-nan
                        if(t_sum)
                            r[i] = avg? area/t_sum: _a_seconds(area);
                        return r;//-> we are completely done
                    }
                    s_p=ts.get(s); // we need only value here.. could optimize
                    s_finite= isfinite(s_p.v);
                }
                // ok! here we got one finite point, possibly with one more after

            if(s_p.t >= p.end) { // are we out of this interval? skip to next ta.period
                if(t_sum)
                    r[i] = avg? area/t_sum: _a_seconds(area);// stash this result if any
                continue;
            }

            //---- find end-point of a partition
                if(e != s+1) { // only if needed!
            advance_e_point: // we loop into this point at the end of compute partition below
                    e= s+1;// get next point from s
                    if(e==n) {// we are at the end, and left-value of s is nan
                        if(t_sum)
                            r[i] = avg? area/t_sum: _a_seconds(area);//stash result if any
                        return r;// -> we are completely done
                    }
                    e_p = ts.get(e);
                    e_finite = isfinite(e_p.v);
                    if(e_finite) {// yahoo! two points, we can then..
                        // compute equation for the line f(t) = a*t + b
                        // given points s_p and e_p
                        a = (e_p.v - s_p.v)/_to_ticks(e_p.t - s_p.t);
                        b = s_p.v - a*_to_ticks(s_p.t);
                    } else {
                        s=e;// got nan, restart search s_finite
                        s_finite=false;//
                        goto search_s_finite;
                    }
                }

            //compute_partition: we got a valid partition, defined by two points
                auto s_t = max(s_p.t,p.start);// clip to interval p
                auto e_t = min(e_p.t,p.end); // recall that the points can be anywhere
                // then compute non-nan area and non-nan t_sum
                const int64_t dt{ _to_ticks(e_t-s_t)};
                area +=  (0.5*a*_to_ticks((s_t+e_t)) + b)*dt;// avg.value * dt
                t_sum += dt;
                if(e_p.t >= p.end) { // are we done in this time-step ?
                    r[i] = avg? area/t_sum: _a_seconds(area);// stash result
                    continue; // using same s, as it could extend into next p
                }
                // else advance start to next point, that is; the current end-point
                s_p=e_p;
                s=e;
                goto advance_e_point;
        }
        return r;
    }

    template < class TA,class TS>
    vector<double> accumulate_stair_case(const TA&ta, const TS& ts, bool avg) {
        vector<double> r(ta.size(),nan);
        // all good reasons for quitting early goes here:
        if(    ta.size()==0
            || ts.size() ==0 // needs at least one point, otherwise ->nan
            || ts.time(0) >= ta.total_period().end  // entirely after ->nan
            || ts.total_period().end <= ta.total_period().start) // entirely before ->nan
            return r;
        const utcperiod tp{ts.total_period()};
        // s= start point in our algorithm
        size_t s=ts.index_of(ta.period(0).start);
        if(s == string::npos) // ts might start after our time-axis begin
            s=0;// no prob. then we start where it begins.
        point s_p{ts.get(s)};
        bool s_finite{isfinite(s_p.v)};
        // e = end point for a partition in our algorithm
        point e_p;
        bool e_finite{false};
        const size_t n=ts.size();

        for(size_t i=0;i<ta.size();++i) {
            double area{0.0}; // integral of non-nan f(x), area
            int64_t t_sum{0};  // sum of non-nan time-axis
            const auto p {ta.period(i)};

            //---- find first finite point of a partition
            search_s_finite:
                while(!s_finite) {
                    ++s;
                    if(s>=n) {// we are out of points searching for non-nan
                        if(t_sum)
                            r[i] = avg? area/t_sum: _a_seconds(area);
                        return r;//-> we are completely done
                    }
                    s_p=ts.get(s); // we need only value here.. could optimize
                    s_finite= isfinite(s_p.v);
                }
                // ok! here we got one finite point, possibly with one more after

            if(s_p.t >= p.end) { // are we out of this interval? skip to next ta.period
                if(t_sum)
                    r[i] = avg? area/t_sum:_a_seconds( area);// stash this result if any
                continue;
            }
            //---- find end-point of a partition
            find_partition_end:
                if(s+1<n) {
                    e_p = ts.get(s+1);
                    e_finite = isfinite(e_p.v);
                } else {
                    e_p.t = tp.end;// total-period end
                    e_finite=false;
                }
            //compute_partition: we got a valid partition, defined by two points
            auto s_t = max(s_p.t,p.start);// clip to interval p
            auto e_t = min(e_p.t,p.end); // recall that the points can be anywhere
            int64_t dt{_to_ticks(e_t-s_t)};
            area +=  s_p.v*dt;
            t_sum += dt;
            if ( e_p.t <= p.end && s+1 <n) {// should&can we advance s
                s_p=e_p;
                s_finite=e_finite;
                ++s;
                if(e_p.t == p.end) {
                    r[i] = avg? area/t_sum:_a_seconds(area);// stash result
                    continue;// skip to next interval
                }
                if(s_finite)
                    goto find_partition_end;
                else
                    goto search_s_finite;
            }
            // keep s, next interval.
            r[i] = avg? area/t_sum:_a_seconds( area);// stash result

            if(s+1>=n && p.end >= tp.end)
               return r;// finito
        }
        return r;
    }

    /** @brief hint_based search to eliminate binary-search in irregular time-point series.
     *
     *  utilizing the fact that most access are periods, sequential, so the average_xxx functions
     *  can utilize this, and keep the returned index as a hint for the next request for average_xxx
     *  value.
     * @tparam S a point ts source, must have .get(i) ->point, and .size(), and .index_of(t)->size_t
     * @param source a point ts source as described above
     * @param p utcperiod for which we search a start point <= p.start
     * @param i the start-of-search hint, could be -1, then ts.index_of(p.start) is used to figure out the ix.
     * @return lowerbound index or npos if not found
     * @note We should specialize this for sources with computed time-axis to improve speed
     */
    template<class S>
    size_t hint_based_search(const S& source, const utcperiod& p, size_t i) {
        const size_t n = source.size();
        if (n == 0)
            return std::string::npos;
        if (i != std::string::npos && i<n) { // hint-based search logic goes here:
            const size_t max_directional_search = 5; // +-5 just a guess for what is a reasonable try upward/downward
            auto ti = source.get(i).t;
            if (ti == p.start) { // direct hit and extreme luck ?
                return i; // just use it !
            } else if (ti < p.start) { // do a local search upward to see if we find the spot we search for
                if (i == n - 1) return i;// unless we are at the end (n-1), try to search upward
                size_t i_max = std::min(i + max_directional_search, n);
                while (++i < i_max) {
                    ti = source.get(i).t;
                    if (ti < p.start )
                        continue;
                    return  ti > p.start? i - 1 : i;// we either got one to far, or direct-hit
                }
                return (i < n) ? source.index_of(p.start):n-1; // either local search failed->bsearch etc., or we are at the end -> n-1
            } else if (ti > p.start) { // do a local search downwards from last index, maybe we are lucky
                if (i == 0) // if we are at the beginning, just return npos (no left-bound index found)
                    return 0;//std::string::npos;
                size_t i_min =  (i - std::min(i, max_directional_search));
                do {
                    ti = source.get(--i).t;//notice that i> 0 before we start due to if(i==0) above(needed due to unsigned i!)
                    if ( ti > p.start)
                        continue;
                    return i; // we found the lower left bound (either exact, or less p.start)
                } while (i > i_min);
                return i>0? source.index_of(p.start): std::string::npos; // i is >0, there is a hope to find the index using index_of, otherwise, no left lower bound
            }
        }
        return source.index_of(p.start);// no hint given, just use binary search to establish the start.
    }


    /** @brief accumulate_value
     *
     * @details
    * This function provides a projection/interpretation of the values of a point source on to a time-axis as provided.
    * This includes interpolation and true average, linear between points
    * and nan-handling semantics.
    * In addition the Accessor allows fast sequential access to these values
    * using clever caching of the last position used in the underlying
    * point source. The time axis and point source can be of any type
    * as listed above as long as the basic type requirement as described below
    * are satisfied.
    * @tparam S point source, must provide:
    *  -# .size() const               -> number of points in the source
    *  -# .index_of(utctime tx) const -> return lower bound index or -1 for the supplied tx
    *  -# .get(size_t i) const        -> return the i'th point  (t,v)
    * @param source of type S
    * @param p         the period [start,end) on time-axis, the range where we will accumulate/integrate the f(t)
    * @param last_idx  position of the last time point used on the source, updated after each call.
    * @param tsum      the sum of time under non-nan areas of the curve
    * @param linear    interpret points as linear between, if set to false, use stair-case start of step def
    * @param strict_linear_between enforces strict linear between points, needs two-points to form a line, no +oo|rhs-nan extensions
    * @return the area under the non-nan areas of the curve, specified by tsum reference-parameter
    *
    * @note to be replaced by the faster accumulate functions above
    */
    template <class S>
    double accumulate_value(const S& source, const utcperiod& p, size_t& last_idx, utctimespan& tsum,bool linear = true,bool strict_linear_between=true) {
        const size_t n = source.size();
        const bool extrapolate_flat = !linear || (linear && !strict_linear_between);
        if (n == 0) // early exit if possible
            return nan;
        size_t i = hint_based_search(source, p, last_idx);  // use last_idx as hint, allowing sequential periodic average to execute at high speed(no binary searches)
        point l;// Left point
        bool l_finite = false;

        if (i == std::string::npos) { // this might be a case
            //  source starts after p.start
            // or
            //  source has no period that contans p.start
            //  - that is:
            i = 0;
            last_idx = 0;// we update the hint to the left-most possible index
            if (strict_linear_between) {// investigate if we are after end
                l = source.get(i++);
                l_finite = std::isfinite(l.v);
                if (!p.contains(l.t))
                    return nan;
            }

        }

        double area = 0.0;  // Integrated area over the non-nan parts of the time-axis
        tsum = utctimespan{0}; // length of non-nan f(t) time-axis
        while (true) { //there are two exit criteria: no more points, or we pass the period end.
            if (!l_finite) {//search for 'point' anchor phase
                l = source.get(i++);
                l_finite = std::isfinite(l.v);
                if (i == n) { // exit condition
                    if (l_finite && l.t < p.end) {//give contribution
                        if (extrapolate_flat) {
                            utctimespan dt = p.end - std::max(p.start, l.t);
                            tsum += dt;
                            area += to_seconds(dt)* l.v; // extrapolate value flat
                        }
                    }
                    break;//done
                }
                if (l.t >= p.end) {//also exit condition, if the point time is after period we search, then no anchor to be found
                    break;//done
                }
            } else { // got point anchor l, search for right point/end
                point r = source.get(i++);// r is guaranteed > p.start due to hint-based search
                bool r_finite = std::isfinite(r.v);
                utcperiod px(std::max(l.t, p.start), std::min(r.t, p.end));
                utctimespan dt = px.timespan();


                // now add area contribution for l..r
                if (linear && r_finite) {
                    double a = (r.v - l.v) / to_seconds((r.t - l.t));
                    double b = r.v - a*to_seconds(r.t);
                    area += to_seconds(dt) * (0.5*a*to_seconds(px.start+px.end) + b);
                    tsum += dt;
                } else { // flat contribution from l  max(l.t,p.start) until max time of r.t|p.end
                    if (extrapolate_flat) {
                        area += l.v*to_seconds(dt);
                        tsum += dt;
                    }
                }
                if (i == n) { // exit condition: final value in sequence, then we need to finish, but
                    if (r_finite && r.t < p.end) {// add area contribution from r and out to the end of p
                        if (extrapolate_flat) {
                            dt = p.end - r.t;
                            tsum += dt;
                            area += to_seconds(dt)* r.v; // extrapolate value flat
                        }
                    }
                    break;//done
                }
                if (r.t >= p.end)
                    break;//also exit condition, done
                l_finite = r_finite;
                l = r;
            }
        }
        last_idx = i - 1;
        return tsum.count() ? area : nan;
    }

   /** @brief average_value provides a projection/interpretation
     * of the values of a pointsource on to a time-axis as provided.
     * This includes interpolation and true average, linear between points
     * and nan-handling semantics.
     * In addition the Accessor allows fast sequential access to these values
     * using clever caching of the last position used in the underlying
     * point source. The time axis and point source can be of any type
     * as listed above as long as the basic type requirement as described below
     * are satisfied.
     * @tparam S point source, must provide:
     *  -# .size() const               -> number of points in the source
     *  -# .index_of(utctime tx) const -> return lower bound index or -1 for the supplied tx
     *  -# .get(size_t i) const        -> return the i'th point  (t,v)
     * @param source of type S
     * @param p the period [start,end) on time-axis
     * @param last_idx in/out, position of the last time point used on the source, updated after each call.
     * @param linear how to interpret the points, if true, use linear between points specification
     * @return double, the value at the as true average of the specified period
     *
     * @note to be replaced by the faster accumulate functions above
     */
    template <class S>
    inline double average_value(const S& source, const utcperiod& p, size_t& last_idx,bool linear=true) {
        utctimespan tsum{0};
        double area = accumulate_value(source, p, last_idx, tsum, linear);// just forward the call to the accumulate function
        return tsum.count()>0?area/to_seconds(tsum):nan;
    }


} // shyft.time_series
