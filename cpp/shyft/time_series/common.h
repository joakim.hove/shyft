/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cmath>
#include <type_traits>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/math_utilities.h>

#ifndef M_PI
# define M_PI           3.14159265358979323846  /* pi */
#endif

namespace shyft {
    const double nan = std::numeric_limits<double>::quiet_NaN();
}

namespace shyft::time_series {
    using std::shared_ptr;
    using std::isfinite;
    using std::enable_if;
    using std::enable_if_t;

    using core::utctime;
    using core::utctime_0;
    constexpr double EPS=1e-12; ///< used some places for comparison to equality, \ref point

    /** @brief simply a point identified by utctime t and value v */
    struct point {
        utctime t;
        double v;
        point(utctime t=utctime_0, double v=0.0) : t(t), v(v) { /* Do nothing */}
    };

    /** @brief point a and b are considered equal if same time t and value-diff less than EPS
    */
    inline bool operator==(const point &a,const point &b)  {return (a.t==b.t) && std::fabs(a.v-b.v)< EPS;}

        /** @brief Enumerates how points are mapped to f(t)
     *
     * If there is a point_ts, this determines how we would draw f(t).
     * Average values are typically staircase-start-of step, a constant over the interval
     * for which the average is computed.
     * State-in-time values are typically POINT_INSTANT_VALUES; and we could as an approximation
     * draw a straight line between the points.
     */
    enum ts_point_fx:int8_t {
        POINT_INSTANT_VALUE, ///< the point value represents the value at the specific time (or centered around that time),typically linear accessor
        POINT_AVERAGE_VALUE///< the point value represents the average of the interval, typically stair-case start of step accessor

    };

    inline ts_point_fx result_policy(ts_point_fx a, ts_point_fx b) {
        return a==ts_point_fx::POINT_INSTANT_VALUE || b==ts_point_fx::POINT_INSTANT_VALUE?ts_point_fx::POINT_INSTANT_VALUE:ts_point_fx::POINT_AVERAGE_VALUE;
    }

    //--- to avoid duplicating algorithms in classes where the stored references are
    // either by shared_ptr, or by value, we use template function:
    //  to make shared_ptr<T> appear equal to T
    template<typename T> struct is_shared_ptr {static const bool value=false;};
    template<typename T> struct is_shared_ptr<shared_ptr<T>> {static const bool value=true;};

    /** is_ts<T> is a place holder to support specific dispatch for time-series types
     * default is false
     */
    template<class T> struct is_ts {static const bool value=false;};

    /** needs_bind<T> is a default place holder to support specific dispatch for
     * time-series or expressions that might have symbolic references.
     * We use this to deduce compile-time if an expression needs a bind
     * cycle before being evaluated. That is; that the ref_ts get their values
     * set by a bind-operation.
     * By default, this is true for all types, except point_ts that we know do not need bind
     * - all other need to turn it by specializing needs_bind similar as for point_ts
     * @see ref_ts
     */
    template<class T> struct needs_bind {static const bool value=true;};

    /** Resolves compiletime to dispatch runtime calls to needs_bind where supported.
     * Additionally allows for querying if a type supports needs_bind.
     */
    template <
        class T, typename = void
    > struct needs_bind_dispatcher : std::false_type
    {
        static bool needs_bind(const T& ) {
            return false;
        }
    };

    template <class T> struct needs_bind_dispatcher<
        T, std::enable_if_t<std::is_same<decltype(std::declval<T>().needs_bind()), bool>::value>
    > : std::true_type
    {
        static bool needs_bind(const T& t) {
            return t.needs_bind();
        }
    };

    template<class T> bool e_needs_bind(T const& t) { return needs_bind_dispatcher<T>::needs_bind(t); }// run-time check on state, default false

    /** @brief d_ref function to d_ref object or shared_ptr
     *
     * The T d_ref(T) template to return a ref or const ref to T, if T is shared_ptr<T> or just T
     */
    template<class U> const U& d_ref(const std::shared_ptr<U>& p) { return *p; }
    template<class U> const U& d_ref(const U& u) { return u; }
    template<class U> U& d_ref(std::shared_ptr<U>& p) { return *p; }
    template<class U> U& d_ref(U& u) { return u; }


    ///< @brief d_ref_t template to rip out T of shared_ptr<T> or T if T specified
    template <class T, class P = void >
    struct d_ref_t { };

    template <class T >
    struct d_ref_t<T,typename enable_if<is_shared_ptr<T>::value>::type> {
        typedef typename T::element_type type;
    };
    template <class T >
    struct d_ref_t<T,typename enable_if<!is_shared_ptr<T>::value>::type > {
        typedef T type;
    };

}
