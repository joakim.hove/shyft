/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/time_series/common.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::time_series {
    using namespace shyft::core;
    using std::string;
    using std::vector;
    using std::runtime_error;
    using boost::math::epsilon_difference;

    class rating_curve_segment {

    public:
        double lower;  ///< Lower level for this segment, considered valid indefinitly after.
        double a;  ///< Rating-curve parameter.
        double b;  ///< Rating-curve parameter.
        double c;  ///< Rating-curve parameter.

    public:  // con/de-struction, copy & move
        rating_curve_segment()
            : lower{ 0. }, a{ 0. }, b{ 0. }, c{ 0. } { }
        rating_curve_segment(double lower, double a, double b, double c)
            : lower{ lower }, a{ a }, b{ b }, c{ c } { }
        ~rating_curve_segment() = default;
        // -----
        rating_curve_segment(const rating_curve_segment &) = default;
        rating_curve_segment & operator=(const rating_curve_segment &) = default;
        // -----
        rating_curve_segment(rating_curve_segment &&) = default;
        rating_curve_segment & operator=(rating_curve_segment &&) = default;

    public:  // api
        /** Return whether the segment is valid for the level.
         * Validity is based on whether level is less than lower.
         */
        bool valid(double level) const {
            return lower <= level;
        }
        bool operator==(const rating_curve_segment& o) const {
            return epsilon_difference(lower,o.lower)<2 && epsilon_difference(a,o.a)<2 && epsilon_difference(b,o.b) && epsilon_difference(c,o.c);
        }
        bool operator!=(const rating_curve_segment&o) const { return !operator==(o); }
        // -----
        /** Compute the flow from a water level.
         * Does _not_ check if `h` is valid according to `lower`.
         */
        double flow(double level) const {
            return a * std::pow(level - b, c);
        }
        /** Compute the flow for a list of water levels.
        * Does _not_ check if the water levels are valid according to `lower`.
        */
        std::vector<double> flow(
            const std::vector<double> & levels,
            std::size_t i0 = 0u,
            std::size_t iN = std::numeric_limits<std::size_t>::max()
        ) const {
            std::vector<double> flow;
            flow.reserve(levels.size());
            for (std::size_t i = i0, idx_end = std::min(levels.size(), iN); i < idx_end; ++i) {
                flow.emplace_back(a * std::pow(levels[i] - b, c));
            }
            return flow;
        }
        // -----
        operator std::string() {
            std::string ret{ "rating_curve_segment{ " };
            ret += "lower=" + std::to_string(lower) + " a=" + std::to_string(a) + " b=" +std::to_string(b) +" c=" + std::to_string(c) + " }";
            return ret;
        }
        // -----
        /** Compare two rating-curve segments according to their lower value.
         */
        bool operator< (const rating_curve_segment & other) const {
            return this->lower < other.lower;
        }
        /** Compare a rating-curve segment to a value interpreted as a level value.
        */
        bool operator< (double value) const {
            return this->lower < value;
        }

        x_serialize_decl();
    };

    class rating_curve_function {
        std::vector<rating_curve_segment> segments;  // invariant: This is kept sorted ascending on el.lower!

    public:
        rating_curve_function() = default;
        rating_curve_function(const std::vector<rating_curve_segment> & segment_vector, bool sorted = false)
            : segments{ segment_vector }
        {
            if ( ! sorted )
                std::sort(segments.begin(), segments.end());
        }
        rating_curve_function(std::vector<rating_curve_segment> && segment_vector, bool sorted = false)
            : segments{ std::move(segment_vector) }
        {
            if ( ! sorted)
                std::sort(segments.begin(), segments.end());
        }
        template <class InputIt>
        rating_curve_function(InputIt first, InputIt last, bool sorted = false)
            : segments{ first, last }
        {
            if ( ! sorted)
                std::sort(segments.begin(), segments.end());
        }
        ~rating_curve_function() = default;
        // -----
        rating_curve_function(const rating_curve_function &) = default;
        rating_curve_function & operator=(const rating_curve_function &) = default;
        // -----
        rating_curve_function(rating_curve_function &&) = default;
        rating_curve_function & operator=(rating_curve_function &&) = default;

    public:  // api
        decltype(segments)::const_iterator cbegin() const {
            return segments.cbegin();
        }
        decltype(segments)::const_iterator cend() const {
            return segments.cend();
        }
        std::size_t size() const {
            return segments.size();
        }
        // -----
        operator std::string() {
            std::string ret{ "rating_curve_function{" };
            for ( auto & it : segments )
                ret += " " + static_cast<std::string>(it) + ",";
            ret += " }";
            return ret;
        }
        // -----
        void add_segment(double lower, double a, double b, double c) {
            add_segment(rating_curve_segment(lower, a, b, c));
        }
        void add_segment(rating_curve_segment && seg) {
            segments.emplace(
                std::upper_bound(segments.begin(), segments.end(), seg),
                std::forward<rating_curve_segment>(seg) );
        }
        void add_segment(const rating_curve_segment & seg) {
            segments.emplace(std::upper_bound(segments.begin(), segments.end(), seg), seg );
        }
        // -----
        /** Compute the flow at a specific level.
         */
        double flow(const double level) const {
            if ( segments.size() == 0 )
                throw std::runtime_error("no rating-curve segments");

            // assume segments is sorted ascending on el.lower
            auto it = std::lower_bound(segments.cbegin(), segments.cend(), level);
            if ( it != segments.cend() && level == it->lower ) {
                return it->flow(level);
            } else if ( it != segments.cbegin() ) {  // not at begining? -> compute with prev curve
                return (it - 1)->flow(level);
            } else {  // before first segment -> no flow
                return nan;
            }
        }
        /** Compute the flow for all values in a vector.
         */
        vector<double> flow(const vector<double> & levels) const {
            return flow(levels.cbegin(), levels.cend());
        }
        /** Compute the flow for all values from a iterator.
        */
        template <typename InputIt>
        vector<double> flow(InputIt first, InputIt last) const {
            if ( segments.size() == 0 )
                throw std::runtime_error("no rating-curve segments");

            std::size_t count = std::distance(first, last);

            if ( count == 0 )
                return std::vector<double>{};

            std::vector<double> flow;
            flow.reserve(count);
            for ( auto lvl = first; lvl != last; ++lvl ) {
                auto it = std::lower_bound(segments.cbegin(), segments.cend(), *lvl);
                if ( it != segments.cend() && *lvl == it->lower ) {
                    flow.emplace_back(it->flow(*lvl));
                } else if ( it != segments.cbegin() ) {  // not at begining? -> compute with prev curve
                    flow.emplace_back((*(it - 1)).flow(*lvl));
                } else {  // before first segment -> no flow
                    flow.emplace_back(nan);
                }
            }

            return flow;
        }

        x_serialize_decl();
    };

    class rating_curve_parameters {
        std::map<utctime, rating_curve_function> curves;

    public:  // con/de-struction
        rating_curve_parameters() = default;
        /** Instanciate a new rating-curve parameter block from
        * a iterator yielding `std::pair< [const] utctime, rating_curve_function >`.
        */
        template <typename InputIt>
        rating_curve_parameters(InputIt first, InputIt last)
            : curves{ first, last } { }
        explicit rating_curve_parameters(const std::vector<std::pair<utctime, rating_curve_function>> & curves)
            : rating_curve_parameters{ curves.cbegin(), curves.cend() } { }
        // -----
        ~rating_curve_parameters() = default;
        // -----
        rating_curve_parameters(const rating_curve_parameters &) = default;
        rating_curve_parameters & operator= (const rating_curve_parameters &) = default;
        // -----
        rating_curve_parameters(rating_curve_parameters &&) = default;
        rating_curve_parameters & operator= (rating_curve_parameters &&) = default;

    public:  // api
        decltype(curves)::const_iterator cbegin() const {
            return curves.cbegin();
        }
        decltype(curves)::const_iterator cend() const {
            return curves.cend();
        }			// -----
        operator std::string() {
            std::string ret{ "rating_curve_parameters{"};
            calendar utc;
            for ( auto & it : curves )
                ret += " " + utc.to_string(it.first) + ": [ " + static_cast<std::string>(it.second) + " ],";
            ret += " }";
            return ret;
        }
        // -----
        rating_curve_parameters& add_curve(utctime t, rating_curve_function && rcf) {
            curves.emplace(t, std::forward<rating_curve_function>(rcf));
            return *this;
        }
        rating_curve_parameters& add_curve(utctime t, const rating_curve_function & rcf) {
            curves.emplace(t, rcf);
            return *this;
        }
        // -----
        /** Apply the rating-curve pack at a specific time. */
        double flow(utctime t, double level) const {
            using curve_vt = decltype(curves)::value_type;
            auto it = std::lower_bound(
                curves.cbegin(), curves.cend(), t,
                [](curve_vt lhs, utctime rhs) -> bool { return lhs.first < rhs; }
            );
            if ( it == curves.cbegin() && it->first > t ) {
                return nan;
            } else if ( it == curves.cend() || it->first > t ) {
                // last curve valid indefinitly
                it--;
            }
            return it->second.flow(level);
        }
        /** Apply the rating-curve pack on a time-series. */
        template <typename TA>
        std::vector<double> flow(const TA & ta) const {
            auto it = curves.cbegin();
            if ( it == curves.cend() || it->first >= ta.total_period().end ) {  // no curves...
                return std::vector<double>(ta.size(), nan);
            }
            std::vector<double> flow;
            flow.reserve(ta.size());

            // determine start and pad with nan
            std::size_t i;
            if ( it->first > ta.time(0u) ) {
                i = ta.index_of(it->first);
                for ( std::size_t j = 0u; j < i; ++j ) {
                    flow.emplace_back(nan);
                }
            } else {
                i = 0u;
            }

            auto it_next = it;  // peeking iterator
            it_next++;
            for ( std::size_t dim = ta.size(); i < dim; ++i ) {
                utctime t = ta.time(i);
                double val = ta.value(i);

                if ( it_next != curves.cend() && it_next->first <= t ) {
                    // advance both iterators
                    it++; it_next++;
                }

                flow.emplace_back(it->second.flow(val));
            }

            return flow;
        }

        x_serialize_decl();
    };

}
