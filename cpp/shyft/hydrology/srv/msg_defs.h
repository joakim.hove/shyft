/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <cstdint>
#include <exception>
#include <shyft/core/dlib_utils.h>

namespace shyft::hydrology::srv  {

    /** @brief drms message-types
     *
     * The message types used for the wire-communication of drms
     *
     */
    struct message_type {
        using type = uint8_t;
        static constexpr type SERVER_EXCEPTION = 0;
        static constexpr type VERSION_INFO = 1;
        static constexpr type CREATE_MODEL = 2;
        static constexpr type SET_STATE = 3;
        static constexpr type GET_STATE = 4;
        static constexpr type RUN_INTERPOLATION = 5;
        static constexpr type RUN_CELLS = 6;
        static constexpr type ADJUST_Q = 7;
        static constexpr type GET_DISCHARGE = 8;
        static constexpr type GET_TEMPERATURE = 9;
        static constexpr type GET_PRECIPITATION = 10;
        static constexpr type GET_SNOW_SWE = 11;
        static constexpr type GET_CHARGE = 12;
        static constexpr type GET_SNOW_SCA = 13;
        static constexpr type GET_RADIATION = 14;
        static constexpr type GET_WIND_SPEED= 15;
        static constexpr type GET_REL_HUM = 16;
        static constexpr type SET_REGION_PARAMETER = 17;
        static constexpr type SET_CATCHMENT_PARAMETER = 18;
        static constexpr type SET_CATCHMENT_CALCULATION_FILTER = 19;
        static constexpr type GET_MODEL_IDS = 20;
        static constexpr type REMOVE_MODEL = 21;
        static constexpr type RENAME_MODEL = 22;
        static constexpr type CLONE_MODEL = 23;
        static constexpr type COPY_MODEL = 24;
        static constexpr type REVERT_TO_INITIAL_STATE = 25;
        static constexpr type SET_STATE_COLLECTION = 26;
        static constexpr type SET_SNOW_SCA_SWE_COLLECTION = 27;
        static constexpr type IS_CELL_ENV_TS_OK = 28;
        static constexpr type IS_CALCULATED = 29;
        static constexpr type SET_INITIAL_STATE = 30;
        static constexpr type GET_TIME_AXIS=31;
        static constexpr type START_CALIBRATION=32;
        static constexpr type CHECK_CALIBRATION=33;
        static constexpr type CANCEL_CALIBRATION=34;
        static constexpr type GET_REGION_PARAMETER = 35;
        static constexpr type GET_CATCHMENT_PARAMETER=36;
        static constexpr type REMOVE_CATCHMENT_PARAMETER=37;
        static constexpr type HAS_CATCHMENT_PARAMETER=38;
        static constexpr type GET_GEOCELLDATA=39;
        static constexpr type GET_REGION_ENV=40;
        static constexpr type GET_INTERPOLATION_PARAMETER=41;
        static constexpr type SET_DESCRIPTION=42;
        static constexpr type GET_DESCRIPTION=43;
        
        //-- new drsm messages goes here
        //  like GET_TIME_AXIS etc.
    };

    /** @brief  adapt low-level and message-type handling from the core/dblib_utils.h */
     using msg=shyft::core::msg_util<message_type>; 
}
