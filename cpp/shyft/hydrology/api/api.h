/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <utility>
#include <memory>
#include <stdexcept>


/**
 * This file contains mostly typedefs and some few helper classes to
 * provide a python friendly header file to the api.python project.
 *
 *
 */

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/time_series/point_ts.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/hydrology/model_calibration.h>
#include <shyft/hydrology/spatial/bayesian_kriging.h>
#include <shyft/hydrology/spatial/inverse_distance.h>

#include <shyft/hydrology/stacks/pt_gs_k_cell_model.h>
#include <shyft/hydrology/stacks/r_pm_gs_k_cell_model.h>
#include <shyft/hydrology/stacks/r_pt_gs_k_cell_model.h>
#include <shyft/hydrology/stacks/pt_hs_k_cell_model.h>
#include <shyft/hydrology/stacks/pt_st_k_cell_model.h>
#include <shyft/hydrology/stacks/pt_ss_k_cell_model.h>
#include <shyft/hydrology/stacks/pt_hps_k_cell_model.h>
#include <shyft/hydrology/stacks/hbv_stack_cell_model.h>

#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/geo_ts.h>

namespace shyft::api {
  using namespace shyft::core;
  using std::string;
  using std::vector;
  using std::map;
  using std::shared_ptr;
  using std::runtime_error;
  using std::invalid_argument;
  
  using shyft::time_series::dd::apoint_ts;
  using shyft::time_series::dd::ats_vector;
  using shyft::time_series::dd::geo_ts;
  using shyft::core::stat_scope;

    /** @brief TsFactor provides time-series creation function using supplied primitives like vector of double, start, delta-t, n etc.
     */
    struct TsFactory {

        apoint_ts
        create_point_ts(int n, utctime tStart, utctimespan dt,
                        const std::vector<double>& values,
                        ts_point_fx interpretation=POINT_INSTANT_VALUE){
            return apoint_ts( time_axis::fixed_dt(tStart,dt, n), values, interpretation);
        }


        apoint_ts
        create_time_point_ts(utcperiod period, const std::vector<utctime>& times,
                             const std::vector<double>& values,
                             ts_point_fx interpretation=POINT_INSTANT_VALUE) {
            if (times.size() == values.size() + 1) {
                return apoint_ts( time_axis::point_dt(times), values, interpretation);
            } else if (times.size() == values.size()) {
                auto tx(times);
                tx.push_back(period.end > times.back()?period.end:times.back() + utctimespan(1));
                return apoint_ts( time_axis::point_dt(tx), values, interpretation);
            } else {
                throw std::runtime_error("create_time_point_ts times and values arrays must have corresponding count");
            }
        }
    };


    /** @brief GeoPointSource contains common properties, functions
     * for the geo-point located time-series.
     * Typically it contains a GeoPoint (3d position), plus a timeseries
     */
    struct GeoPointSource {
        GeoPointSource() =default;
        GeoPointSource(const geo_point& midpoint, const apoint_ts& ts)
          : mid_point_(midpoint), ts(ts) {}
        GeoPointSource(const geo_ts&gts):mid_point_(gts.mid_point),ts(gts.ts){}
        virtual ~GeoPointSource() {}
        typedef apoint_ts ts_t;
        typedef geo_point geo_point_t;

        geo_point mid_point_;
        apoint_ts ts;
		string uid;///< user-defined id, for external mapping,
        // boost python fixes for attributes and shared_ptr
        apoint_ts get_ts()  {return ts;}
        void set_ts(apoint_ts x) {ts=x;}
        bool is_equal(const GeoPointSource& x) const {return  uid==x.uid && mid_point_==x.mid_point_ && ts == x.ts;}
        geo_point mid_point() const { return mid_point_; }
        bool operator==(const GeoPointSource&x) const {return is_equal(x);}
        bool operator!=(const GeoPointSource&o) const {return !operator==(o);}
        geo_ts get_geo_ts() const {return geo_ts{mid_point_,ts};}
        x_serialize_decl();
    };

    struct TemperatureSource : GeoPointSource {
        TemperatureSource()=default;
        TemperatureSource(const geo_point& p, const apoint_ts& ts)
         : GeoPointSource(p, ts) {}
        TemperatureSource(geo_ts const&gts):GeoPointSource(gts){}
        const apoint_ts& temperatures() const { return ts; }
		void set_temperature(size_t ix, double v) { ts.set(ix, v); } //btk dst compliant signature, used during btk-interpolation in gridpp exposure
		void set_value(size_t ix, double v) { ts.set(ix, v); }
        bool operator==(const TemperatureSource& x) const {return is_equal(x);}
        bool operator!=(const TemperatureSource&o) const {return !operator==(o);}
        x_serialize_decl();
    };

    struct PrecipitationSource : GeoPointSource {
        PrecipitationSource()=default;
        PrecipitationSource(const geo_point& p, const apoint_ts& ts)
         : GeoPointSource(p, ts) {}
        PrecipitationSource(geo_ts const&gts):GeoPointSource(gts){}
        const apoint_ts& precipitations() const { return ts; }
		void set_value(size_t ix, double v) { ts.set(ix, v); }
		bool operator==(const PrecipitationSource& x) const {return is_equal(x);}
		bool operator!=(const PrecipitationSource&o) const {return !operator==(o);}
		x_serialize_decl();
    };

    struct WindSpeedSource : GeoPointSource {
        WindSpeedSource()=default;
        WindSpeedSource(const geo_point& p, const apoint_ts& ts)
         : GeoPointSource(p, ts) {}
        WindSpeedSource(geo_ts const&gts):GeoPointSource(gts){}
        bool operator==(const WindSpeedSource& x) const {return is_equal(x);}
		bool operator!=(const WindSpeedSource&o) const {return !operator==(o);}
        x_serialize_decl();
    };

    struct RelHumSource : GeoPointSource {
        RelHumSource()=default;
        RelHumSource(const geo_point& p, const apoint_ts& ts)
         : GeoPointSource(p, ts) {}
        RelHumSource(geo_ts const&gts):GeoPointSource(gts){} 
        bool operator==(const RelHumSource& x) const {return is_equal(x);}
		bool operator!=(const RelHumSource&o) const {return !operator==(o);}
        x_serialize_decl();
    };

    struct RadiationSource : GeoPointSource {
        RadiationSource()=default;
        RadiationSource(const geo_point& p, const apoint_ts& ts)
         : GeoPointSource(p, ts) {}
        RadiationSource(geo_ts const&gts):GeoPointSource(gts){} 
        bool operator==(const RadiationSource& x) const {return is_equal(x);}
        x_serialize_decl();
    };

    struct a_region_environment {
            typedef PrecipitationSource precipitation_t;
            typedef TemperatureSource temperature_t;
            typedef RadiationSource radiation_t;
            typedef RelHumSource rel_hum_t;
            typedef WindSpeedSource wind_speed_t;
            /** make vectors non nullptr by  default */
            a_region_environment() {
                temperature=make_shared<vector<TemperatureSource>>();
                precipitation=make_shared<vector<PrecipitationSource>>();
                radiation=make_shared<vector<RadiationSource>>();
                rel_hum=make_shared<vector<RelHumSource>>();
                wind_speed=make_shared<vector<WindSpeedSource>>();
            }
            shared_ptr<vector<TemperatureSource>>   temperature;
            shared_ptr<vector<PrecipitationSource>> precipitation;
            shared_ptr<vector<RadiationSource>>     radiation;
            shared_ptr<vector<WindSpeedSource>>     wind_speed;
            shared_ptr<vector<RelHumSource>>        rel_hum;
            bool operator==(a_region_environment const&o) const;
            bool operator!=(a_region_environment const &o) const {return !operator==(o);}
            // our boost python needs these methods to get properties straight (most likely it can be fixed by other means but..)
            shared_ptr<vector<TemperatureSource>> get_temperature() {return temperature;}
            void set_temperature(shared_ptr<vector<TemperatureSource>> x) {temperature=x;}
            shared_ptr<vector<PrecipitationSource>> get_precipitation() {return precipitation;}
            void set_precipitation(shared_ptr<vector<PrecipitationSource>> x) {precipitation=x;}
            shared_ptr<vector<RadiationSource>> get_radiation() {return radiation;}
            void set_radiation(shared_ptr<vector<RadiationSource>> x) {radiation=x;}
            shared_ptr<vector<WindSpeedSource>> get_wind_speed() {return wind_speed;}
            void set_wind_speed(shared_ptr<vector<WindSpeedSource>> x) {wind_speed=x;}
            shared_ptr<vector<RelHumSource>> get_rel_hum() {return rel_hum;}
            void set_rel_hum(shared_ptr<vector<RelHumSource>> x) {rel_hum=x;}
            vector<char> serialize_to_bytes() const;
            static a_region_environment deserialize_from_bytes(const vector<char>&ss);
            x_serialize_decl();
    };

    typedef shyft::time_series::point_ts<time_axis::fixed_dt> result_ts_t;
    typedef std::shared_ptr<result_ts_t> result_ts_t_;
    using core::cell_statistics;
    using cids_t=std::vector<int64_t>;
 

	/**@brief geo_cell_data_io provide fast conversion to-from string
	 *
	 * In the context of orchestration/repository, we found that it could be useful to
     * cache information as retrieved from a GIS system
     *
     * This only makes sense to keep as long as we need it for performance reasons
     *
	 */
	struct geo_cell_data_io {
        static size_t size() {return 11;}// number of doubles to store a gcd
        static size_t tin_size() {return 15;}// number of doubles to store a TIN gcd
        static void push_to_vector(vector<double>&v,const geo_cell_data& gcd ){
            v.push_back(gcd.mid_point().x);
            v.push_back(gcd.mid_point().y);
            v.push_back(gcd.mid_point().z);
            v.push_back(gcd.area());
            v.push_back(int(gcd.catchment_id()));
            v.push_back(gcd.radiation_slope_factor());
            v.push_back(gcd.land_type_fractions_info().glacier());
            v.push_back(gcd.land_type_fractions_info().lake());
            v.push_back(gcd.land_type_fractions_info().reservoir());
            v.push_back(gcd.land_type_fractions_info().forest());
            v.push_back(gcd.land_type_fractions_info().unspecified());// not really needed, since it can be computed from 1- theothers
        }
        static vector<double> to_vector(const geo_cell_data& gcd) {
            vector<double> v;v.reserve(11);
            push_to_vector(v,gcd);
            return v;
        }
        static geo_cell_data from_raw_vector(const double *v) {
            land_type_fractions ltf; ltf.set_fractions(v[6],v[7],v[8],v[9]);
            //                               x    y    z    a    cid       rsl
            return geo_cell_data(geo_point(v[0],v[1],v[2]),v[3],int(v[4]),v[5],ltf);
        }
        static geo_cell_data from_vector(const vector<double>&v) {
            if(v.size()!= size())
                throw std::invalid_argument("geo_cell_data_io::from_vector: size of vector must be equal to geo_cell_data_io::size()");
            return from_raw_vector(v.data());

        }
        // TIN -- in case it is useful.
        static geo_cell_data from_raw_vector_to_tin(const double *v) {
            // the v layout should be like this, all double
            //  012  345 678    9          10  11,12,13,14
            //   p1   p2    p3   epsg_id cid   ltf 
            land_type_fractions ltf; ltf.set_fractions(v[11],v[12],v[13],v[14]); //automagically figure out monocell or not
            return geo_cell_data(geo_point(v[0],v[1],v[2]),geo_point(v[3],v[4],v[5]),geo_point(v[6],v[7],v[8]),int64_t(v[9]),(v[10]),ltf);
        }

    };
 
}

namespace shyft::core {
using shyft::api::a_region_environment;
///---------------------------
//  Expose the stacks as external templates here:
extern template class region_model<pt_gs_k::cell_complete_response_t,a_region_environment>;
extern template class region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>;
        
extern template class region_model<pt_ss_k::cell_complete_response_t,a_region_environment>;
extern template class region_model<pt_ss_k::cell_discharge_response_t,a_region_environment>;

extern template class region_model<pt_hs_k::cell_complete_response_t,a_region_environment>;
extern template class region_model<pt_hs_k::cell_discharge_response_t,a_region_environment>;

extern template class region_model<pt_hps_k::cell_complete_response_t,a_region_environment>;
extern template class region_model<pt_hps_k::cell_discharge_response_t,a_region_environment>;

extern template class region_model<r_pm_gs_k::cell_complete_response_t,a_region_environment>;
extern template class region_model<r_pm_gs_k::cell_discharge_response_t,a_region_environment>;
        
extern template class region_model<pt_st_k::cell_complete_response_t,a_region_environment>;
extern template class region_model<pt_st_k::cell_discharge_response_t,a_region_environment>;
        
extern template class region_model<r_pt_gs_k::cell_complete_response_t,a_region_environment>;
extern template class region_model<r_pt_gs_k::cell_discharge_response_t,a_region_environment>;

extern template class region_model<hbv_stack::cell_complete_response_t,a_region_environment>;
extern template class region_model<hbv_stack::cell_discharge_response_t,a_region_environment>;

}

x_serialize_export_key(shyft::api::GeoPointSource);
x_serialize_export_key(shyft::api::TemperatureSource);
x_serialize_export_key(shyft::api::PrecipitationSource);
x_serialize_export_key(shyft::api::RadiationSource);
x_serialize_export_key(shyft::api::WindSpeedSource);
x_serialize_export_key(shyft::api::RelHumSource);
x_serialize_export_key(shyft::api::a_region_environment);
