/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/hydrology/api/api_pch.h>
#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/api/api_state.h>
#include <shyft/hydrology/api/api_statistics.h>

namespace shyft::api {
    namespace {
        template <class T> 
        bool is_p_vec_equal(shared_ptr<vector<T>> const &a,shared_ptr<vector<T>> const &b) {
            if(!a && !b) 
                return true;
            if(a && b) 
                return *a == *b;
            return false;
        }
    }
    
    bool a_region_environment::operator==(a_region_environment const&o) const {
        return is_p_vec_equal(temperature,o.temperature)
         && is_p_vec_equal(precipitation,o.precipitation)
         && is_p_vec_equal(radiation,o.radiation)
         && is_p_vec_equal(wind_speed,o.wind_speed)
         && is_p_vec_equal(rel_hum,o.rel_hum)
         ;
    }

}
namespace shyft::core {
using shyft::api::a_region_environment;
///---------------------------
//  Expose the stacks as external templates here:
template class region_model<pt_gs_k::cell_complete_response_t,a_region_environment>;
template class region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>;
 
template class region_model<pt_ss_k::cell_complete_response_t,a_region_environment>;
template class region_model<pt_ss_k::cell_discharge_response_t,a_region_environment>;

template class region_model<pt_hs_k::cell_complete_response_t,a_region_environment>;
template class region_model<pt_hs_k::cell_discharge_response_t,a_region_environment>;

template class region_model<pt_hps_k::cell_complete_response_t,a_region_environment>;
template class region_model<pt_hps_k::cell_discharge_response_t,a_region_environment>;

template class region_model<r_pm_gs_k::cell_complete_response_t,a_region_environment>;
template class region_model<r_pm_gs_k::cell_discharge_response_t,a_region_environment>;
 
template class region_model<pt_st_k::cell_complete_response_t,a_region_environment>;
template class region_model<pt_st_k::cell_discharge_response_t,a_region_environment>;
 
template class region_model<r_pt_gs_k::cell_complete_response_t,a_region_environment>;
template class region_model<r_pt_gs_k::cell_discharge_response_t,a_region_environment>;

template class region_model<hbv_stack::cell_complete_response_t,a_region_environment>;
template class region_model<hbv_stack::cell_discharge_response_t,a_region_environment>;

}

namespace shyft::api {

    
// instantiate templates one place here
template struct state_io_handler<hbv_stack::cell_complete_response_t>;
template struct state_io_handler<hbv_stack::cell_discharge_response_t>;
template struct state_io_handler<pt_gs_k::cell_complete_response_t>;
template struct state_io_handler<pt_gs_k::cell_discharge_response_t>;
template struct state_io_handler<r_pm_gs_k::cell_complete_response_t>;
template struct state_io_handler<r_pm_gs_k::cell_discharge_response_t>;
template struct state_io_handler<r_pt_gs_k::cell_complete_response_t>;
template struct state_io_handler<r_pt_gs_k::cell_discharge_response_t>;
template struct state_io_handler<pt_ss_k::cell_complete_response_t>;
template struct state_io_handler<pt_ss_k::cell_discharge_response_t>;
template struct state_io_handler<pt_hs_k::cell_complete_response_t>;
template struct state_io_handler<pt_hs_k::cell_discharge_response_t>;
template struct state_io_handler<pt_st_k::cell_complete_response_t>;
template struct state_io_handler<pt_st_k::cell_discharge_response_t>;
template struct state_io_handler<pt_hps_k::cell_complete_response_t>;
template struct state_io_handler<pt_hps_k::cell_discharge_response_t>;

#define cell_template_instantiate(C) \
    template struct C<hbv_stack::cell_complete_response_t>;template struct C<hbv_stack::cell_discharge_response_t>;\
    template struct C<pt_gs_k::cell_complete_response_t>;  template struct C<pt_gs_k::cell_discharge_response_t>;\
    template struct C<r_pm_gs_k::cell_complete_response_t>;template struct C<r_pm_gs_k::cell_discharge_response_t>;\
    template struct C<r_pt_gs_k::cell_complete_response_t>;template struct C<r_pt_gs_k::cell_discharge_response_t>;\
    template struct C<pt_ss_k::cell_complete_response_t>;  template struct C<pt_ss_k::cell_discharge_response_t>;\
    template struct C<pt_hs_k::cell_complete_response_t>;  template struct C<pt_hs_k::cell_discharge_response_t>;\
    template struct C<pt_st_k::cell_complete_response_t>;  template struct C<pt_st_k::cell_discharge_response_t>;\
    template struct C<pt_hps_k::cell_complete_response_t>; template struct C<pt_hps_k::cell_discharge_response_t>

#define cell_template_instantiate2(C) \
    template struct C<pt_gs_k::cell_complete_response_t>;  \
    template struct C<r_pm_gs_k::cell_complete_response_t>;\
    template struct C<r_pt_gs_k::cell_complete_response_t>;\
    template struct C<pt_ss_k::cell_complete_response_t>;  \
    template struct C<pt_hs_k::cell_complete_response_t>;  \
    template struct C<pt_st_k::cell_complete_response_t>;  \
    template struct C<pt_hps_k::cell_complete_response_t>; 
    
    
#define r_template_instantiate(C) \
    template struct C<r_pm_gs_k::cell_complete_response_t>;\
    template struct C<r_pt_gs_k::cell_complete_response_t>;
    
#define st_template_instantiate(C) \
    template struct C<pt_st_k::cell_complete_response_t>;  
    
#define ss_template_instantiate(C) \
    template struct C<pt_ss_k::cell_complete_response_t>;  

#define hs_template_instantiate(C) \
    template struct C<hbv_stack::cell_complete_response_t>;\
    template struct C<pt_hs_k::cell_complete_response_t>;  
    
#define hps_template_instantiate(C) \
    template struct C<pt_hps_k::cell_complete_response_t>; 

    
#define pm_template_instantiate(C) \
    template struct C<r_pm_gs_k::cell_complete_response_t>;\

    
#define pt_template_instantiate(C) \
    template struct C<pt_gs_k::cell_complete_response_t>;  \
    template struct C<r_pt_gs_k::cell_complete_response_t>;\
    template struct C<pt_ss_k::cell_complete_response_t>;  \
    template struct C<pt_hs_k::cell_complete_response_t>;  \
    template struct C<pt_st_k::cell_complete_response_t>;  \
    template struct C<pt_hps_k::cell_complete_response_t>; 

    
#define gs_template_instantiate(C) \
    template struct C<pt_gs_k::cell_complete_response_t>;  \
    template struct C<r_pm_gs_k::cell_complete_response_t>;\
    template struct C<r_pt_gs_k::cell_complete_response_t>;

#define hbv_template_instantiate(C) \
    template struct C<hbv_stack::cell_complete_response_t>;\
    
#define k_template_instantiate(C) \
    template struct C<pt_gs_k::cell_complete_response_t>;  \
    template struct C<r_pm_gs_k::cell_complete_response_t>;\
    template struct C<r_pt_gs_k::cell_complete_response_t>;\
    template struct C<pt_ss_k::cell_complete_response_t>;  \
    template struct C<pt_hs_k::cell_complete_response_t>;  \
    template struct C<pt_st_k::cell_complete_response_t>;  \
    template struct C<pt_hps_k::cell_complete_response_t>; 

    
cell_template_instantiate(basic_cell_statistics);
k_template_instantiate(kirchner_cell_state_statistics);

hbv_template_instantiate(hbv_soil_cell_state_statistics);
hbv_template_instantiate(hbv_tank_cell_state_statistics);

gs_template_instantiate(gamma_snow_cell_state_statistics);
gs_template_instantiate(gamma_snow_cell_response_statistics);

ss_template_instantiate(skaugen_cell_state_statistics);
ss_template_instantiate(skaugen_cell_response_statistics);

hs_template_instantiate(hbv_snow_cell_state_statistics);

hs_template_instantiate(hbv_snow_cell_response_statistics);

st_template_instantiate(snow_tiles_cell_response_statistics);
st_template_instantiate(snow_tiles_cell_state_statistics);

hps_template_instantiate(hbv_physical_snow_cell_state_statistics);

hps_template_instantiate(hbv_physical_snow_cell_response_statistics);

pt_template_instantiate(priestley_taylor_cell_response_statistics);
pm_template_instantiate(penman_monteith_cell_response_statistics);
r_template_instantiate(radiation_cell_response_statistics);

hbv_template_instantiate(hbv_soil_cell_response_statistics);

cell_template_instantiate2(actual_evapotranspiration_cell_response_statistics);

hbv_template_instantiate(hbv_actual_evapotranspiration_cell_response_statistics);

}
