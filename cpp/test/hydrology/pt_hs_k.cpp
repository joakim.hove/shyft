
#include "test_pch.h"
#include <shyft/hydrology/stacks/pt_hs_k.h>
#include <shyft/hydrology/cell_model.h>
#include <shyft/hydrology/stacks/pt_hs_k_cell_model.h>
#include "hydro_mocks.h"
#include <shyft/time_series/point_ts.h>
#include <shyft/time/utctime_utilities.h>
#include <energy_market/serialize_loop.h>

// Some typedefs for clarity
using namespace shyft::core;
using namespace shyft::time_series;
using namespace shyft::core::pt_hs_k;

using namespace shyfttest::mock;
using namespace shyfttest;

namespace pt = shyft::core::priestley_taylor;
namespace hs = shyft::core::hbv_snow;
namespace gm = shyft::core::glacier_melt;
namespace kr = shyft::core::kirchner;
namespace ae = shyft::core::actual_evapotranspiration;
namespace pc = shyft::core::precipitation_correction;
namespace ta = shyft::time_axis;
typedef TSPointTarget<ta::point_dt> catchment_t;

namespace shyfttest {
    namespace mock {
        // need specialization for pthsk_response_t above
        template<> template<>
        void ResponseCollector<ta::fixed_dt>::collect<response>(size_t idx, const response& response) {
            _snow_output.set(idx, response.snow.outflow);
        }
        template <> template <>
        void DischargeCollector<ta::fixed_dt>::collect<response>(size_t idx, const response& response) {
            // q_avg is given in mm, so compute the totals
            avg_discharge.set(idx, destination_area*response.kirchner.q_avg / 1000.0 / 3600.0);
        }
    };
}; // End namespace shyfttest

namespace test {
    template <class T>
    void test_serialize() {
        T a;
        T b= test::serialize_loop(a);
        CHECK_EQ(a,b);
    }
}

TEST_SUITE("pt_hs_k") {
TEST_CASE("test_call_stack") {
    xpts_t temp;
    xpts_t prec;
    xpts_t rel_hum;
    xpts_t wind_speed;
    xpts_t radiation;

    calendar cal;
    utctime t0 = cal.time(YMDhms(2014, 8, 1, 0, 0, 0));
    size_t n_ts_points = 3*24;
    utctimespan dt  = deltahours(1);
    utctime t1 = t0 + n_ts_points*dt;
    shyfttest::create_time_series(temp, prec, rel_hum, wind_speed, radiation, t0, dt, n_ts_points);

    auto model_dt = deltahours(24);
    vector<utctime> times;
    for (utctime i=t0; i <= t1; i += model_dt)
        times.emplace_back(i);
    ta::fixed_dt time_axis(t0, dt, n_ts_points);
	ta::fixed_dt state_time_axis(t0, dt, n_ts_points + 1);
    // Initialize parameters
    std::vector<double> s = {1.0, 1.0, 1.0, 1.0, 1.0}; // Zero cv distribution of snow (i.e. even)
    std::vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
    pt::parameter pt_param;
    hs::parameter snow_param(s, a);
    ae::parameter ae_param;
    kr::parameter k_param;
    pc::parameter p_corr_param;

    // Initialize the state vectors
    kr::state kirchner_state {5.0};
    hs::state snow_state(10.0, 0.5);
    // should work without..snow_state.distribute(snow_param);
    // Initialize response
    response run_response;

    // Initialize collectors
    shyfttest::mock::ResponseCollector<ta::fixed_dt> response_collector(1000*1000, time_axis);
    shyfttest::mock::StateCollector<ta::fixed_dt> state_collector(state_time_axis);

    state state {snow_state, kirchner_state};
    parameter parameter(pt_param, snow_param, ae_param, k_param, p_corr_param);
    geo_cell_data geo_cell_data;
    pt_hs_k::run<direct_accessor, response>(geo_cell_data, parameter, time_axis,0,0, temp,
                                              prec, wind_speed, rel_hum, radiation, state,
                                              state_collector, response_collector);

    auto snow_swe = response_collector.snow_swe();
    for (size_t i = 0; i < snow_swe.size(); ++i)
        TS_ASSERT(std::isfinite(snow_swe.get(i).v) && snow_swe.get(i).v >= 0);
}

TEST_CASE("basic_serialize") {
    SUBCASE("pt") { test::test_serialize<pt::parameter>();}
    SUBCASE("hs") { test::test_serialize<hs::parameter>();}
    SUBCASE("kr") { test::test_serialize<kr::parameter>();}
    SUBCASE("ae") { test::test_serialize<ae::parameter>();}
    SUBCASE("pc") { test::test_serialize<pc::parameter>();}
}

TEST_CASE("test_parameter_serialize") {
    pt::parameter pt_param;
    hs::parameter hs_param;
    ae::parameter ae_param;
    kr::parameter k_param;
    pc::parameter p_corr_param;
    parameter original{pt_param, hs_param, ae_param, k_param, p_corr_param};
    auto restored = test::serialize_loop(original);
    CHECK_EQ(original, restored);
}

TEST_CASE("pt_hs_k_lake_reservoir_response") {
    calendar cal;
    utctime t0 = cal.time(2014, 8, 1, 0, 0, 0);
    utctimespan dt=deltahours(1);
    const int n=50;// need to run some steps to observe kirchner response
    ta::fixed_dt tax(t0,dt,n);
	ta::fixed_dt tax_state(t0, dt, n + 1);
    pt::parameter pt_param;
    hs::parameter gs_param;
    ae::parameter ae_param;
    kr::parameter k_param;
    pc::parameter p_corr_param;
    parameter parameter{pt_param, gs_param, ae_param, k_param, p_corr_param};

    pts_t temp(tax,-15.0,POINT_AVERAGE_VALUE);// freezing cold
    pts_t prec(tax,3.0,POINT_AVERAGE_VALUE);prec.set(0,0.0);// rain except 1.step we use to get initial kirchner response
    pts_t rel_hum(tax,0.8,POINT_AVERAGE_VALUE);
    pts_t wind_speed(tax,2.0,POINT_AVERAGE_VALUE);
    pts_t radiation(tax,300.0,POINT_AVERAGE_VALUE);

    kr::state kirchner_state{1};// 1 mm
    hs::state gs_state;
    //gs_state.lwc=100.0;
    //gs_state.acc_melt=100.0;

    state s0{gs_state, kirchner_state};// need a equal state for the second run
    state s1{gs_state, kirchner_state};

    pt_hs_k::state_collector sc;
    pt_hs_k::all_response_collector rc;
    const double cell_area=1000*1000;
    sc.collect_state=true;
    sc.initialize(tax_state,0,0,cell_area);
    rc.initialize(tax,0,0,cell_area);
    geo_cell_data gcd(geo_point(1000,1000,100));
    land_type_fractions ltf(0.1,0.2,0.3,0.0,0.4);// 0.1 glacier, 0.2 lake, 0.3 reservoir , 0.4 unspec
    gcd.set_land_type_fractions(ltf);

    parameter.msp.reservoir_direct_response_fraction=0.0;// all rsv goes to kirchner
    pt_hs_k::run<direct_accessor,pt_hs_k::response>(gcd,parameter,tax,0,0,temp,prec,wind_speed,rel_hum,radiation,s0,sc,rc);
    CHECK_EQ(rc.avg_discharge.value(0), doctest::Approx(0.266).epsilon(0.01)); // first with 0 precip, nothing should happen
    CHECK_EQ(rc.avg_discharge.value(n-1), doctest::Approx( 0.5*mmh_to_m3s(prec.value(1),cell_area)).epsilon(0.01));

    parameter.msp.reservoir_direct_response_fraction=1.0;// all rsv goes directly to output, lake goes to kirchner
    sc.initialize(tax_state,0,0,cell_area);
    rc.initialize(tax,0,0,cell_area);
    pt_hs_k::run<direct_accessor,pt_hs_k::response>(gcd,parameter,tax,0,0,temp,prec,wind_speed,rel_hum,radiation,s1,sc,rc);
    CHECK_EQ(rc.avg_discharge.value(0), doctest::Approx(0.266).epsilon(0.01)); // first with 0 precip, nothing should happen
    auto expected_1 = 
          0.266 // estimate of kirchner output from 1st step.
        + 0.3*mmh_to_m3s(prec.value(1)-rc.pe_output.value(1),cell_area); // estimate of direct response for 1st timestep
    CHECK_EQ(rc.avg_discharge.value(1), doctest::Approx(expected_1).epsilon(0.05)); // precip on rsv direct effect
    //-- verify snow storage on non lake/rsv only
    CHECK_EQ(sc.snow_swe.value(0),doctest::Approx(0.0).epsilon(0.0001));// linear, and 0.0 first point
    CHECK_EQ(sc.snow_swe.value(1),doctest::Approx(0.0).epsilon(0.0001));// linear, and 0.0 second point
    CHECK_EQ(sc.snow_swe.value(2),doctest::Approx(1.5).epsilon(0.0001));// linear, and 1.5  mm, 3.0/2 mm third  point
    CHECK_EQ(rc.snow_swe.value(0),doctest::Approx(0.0).epsilon(0.0001));// stair, avg,, and 0.0 first interval
    CHECK_EQ(rc.snow_swe.value(1),doctest::Approx(1.5).epsilon(0.0001));// stair avg, 1.5mm  second interval
    CHECK_EQ(rc.snow_swe.value(2),doctest::Approx(3.0).epsilon(0.0001));// stair avg, 3.0mm  third interval

    auto expected_2 = 
         0.2*mmh_to_m3s(prec.value(1)-rc.ae_output.value(1),cell_area) // because this is on lake(frozen, but pressed out)
        +0.3*mmh_to_m3s(prec.value(1)-rc.pe_output.value(1),cell_area);// this is direct response
    CHECK_EQ(rc.avg_discharge.value(n-1), doctest::Approx(expected_2 ).epsilon(0.01));
    //-----------------------------------------
    //-- now verify that if we start melting
    // (1) it melts as usual from snow-coverable fraction
    // (2) when the snow has melted to an area less than the glacier, 
    //     we start getting a non-zero glacier-melt response.
    //     
    // Arrange the test: 
    // Improve this: the sca flips from 0.5 to zero in one step, need a scew distribution that
    // uncover partial
    parameter.hs.cx=2.0;// melt out faster
    parameter.hs.set_snow_redistribution_factors(vector<double>{0.1,0.2,0.3,0.4,0.5});
    s1.snow.distribute(parameter.hs);//make more shape for snow distribution
    temp.fill_range(20.0,0,n);// hot 20 deg/C that force melt
    prec.fill_range(0.0,0,n);// no rain, just summer
    sc.initialize(tax_state,0,0,cell_area);// ensure we nan out response and state
    rc.initialize(tax,0,0,cell_area);
    // Act: run usint state s1 (should have a lot of snow in it)
    pt_hs_k::run<direct_accessor,pt_hs_k::response>(gcd,parameter,tax,0,0,temp,prec,wind_speed,rel_hum,radiation,s1,sc,rc);
    // Assert: 
    FAST_CHECK_GT(rc.snow_swe.value(0),rc.snow_swe.value(n-1));
    FAST_CHECK_GT(rc.snow_sca.value(0),rc.snow_sca.value(n-1));
    FAST_CHECK_LT(rc.snow_sca.value(n-1),ltf.snow_storage());
    for(size_t i=0;i<n;++i) { // loop over steps and verify that there are no glacier response until uncovered bare glacier
        auto sca_cell=  rc.snow_sca.value(i)*ltf.snow_storage();
        //MESSAGE("sca "<<i<<":"<<sca_cell<<" swe:"<<rc.snow_swe.value(i)<<" gm="<<rc.glacier_melt.value(i));
        if(sca_cell >= ltf.glacier()) { // glacier is covered by snow, no melt
            FAST_CHECK_EQ(rc.glacier_melt.value(i),doctest::Approx(0.0));
        } else {// parts of the glacier is now exposed, so it should start melting 
            FAST_CHECK_GT(rc.glacier_melt.value(i),0.0);
        }
    }  
}
}
