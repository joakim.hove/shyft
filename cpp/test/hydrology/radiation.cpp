#include "test_pch.h"
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/point_ts.h>
#include <armadillo>
#include <vector>
#include <chrono>
#include <boost/math/constants/constants.hpp>
#include <shyft/hydrology/methods/radiation.h>
#include <shyft/hydrology/hydro_functions.h>
#include <cmath>
#include <random>
#include <tuple>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/srs/epsg.hpp>
#include <boost/geometry/srs/projection.hpp>
#include <boost/math/constants/constants.hpp>

namespace shyft::test {
    class trapezoidal_average {
    private:
        double area = 0.0;
        double f_a = 0.0;; // Left hand side of next integration subinterval
        double t_start = 0.0; // Start of integration period
        double t_a = 0.0; // Left hand side time of next integration subinterval
    public:
        explicit trapezoidal_average() {}

        /** \brief initialize must be called to reset states before being used during ode integration.
         */
        void initialize(double f0, double t_start) {
            this->f_a = f0;
            this->t_start = t_start;
            t_a = t_start;
            area = 0.0;
        }

        /** \brief Add contribution to average using a simple trapezoidal rule
         *
         * See: http://en.wikipedia.org/wiki/Numerical_integration
         */
        void add(double f, double t) {
            area += 0.5*(f_a + f)*(t - t_a);
            f_a = f;
            t_a = t;
        }

        double result() const {
            /*std::cout<<" times "<<(t_a-t_start)<<std::endl;*/
            return area/(t_a - t_start);
        }
    };

}
namespace boost::geometry::traits {
    // dress up shyft::core::geo_point as a boost geometry  2D point in this context
    // notice that tag-based dispatch require us to do this in boost::geometry::traits (the dressing room!)
    template<> struct tag<shyft::core::geo_point> {using type= point_tag;};
    template<> struct coordinate_type<shyft::core::geo_point> {using type=double;};
    template<> struct coordinate_system<shyft::core::geo_point> {using type=cs::cartesian;};
    template<> struct dimension<shyft::core::geo_point>:boost::mpl::int_<2> {};
    template<> struct access<shyft::core::geo_point,0> {
        static double get(shyft::core::geo_point const&p) {return p.x;}
        static void set(shyft::core::geo_point & p, double value){p.x=value;}
    };
    template<> struct access<shyft::core::geo_point,1> {
        static double get(shyft::core::geo_point const&p) {return p.y;}
        static void set(shyft::core::geo_point & p, double value){p.y=value;}
    };
    
}
namespace test::boost_proj {
    namespace bg= boost::geometry;
    using bg::degree;
    using bg::cs::cartesian;
    using bg::cs::geographic;
    using bg::distance;
    using bg::srs::projection;
    using bg::srs::proj4;
    using bg::srs::epsg;
    using bg::model::point;
    
    using point_ll=point<double, 2,geographic<degree> >;
    using point_xy=shyft::core::geo_point;//point<double, 2,cartesian>;

    void do_test_proj4(){
        point_ll pt_ll(1, 1);
        point_ll pt_ll2(0, 0);
        point_xy pt_xy(0, 0);
        projection<> prj = proj4("+proj=tmerc +ellps=WGS84 +units=m");
        prj.forward(pt_ll, pt_xy);
        prj.inverse(pt_xy, pt_ll2);
        FAST_CHECK_LE(distance(pt_ll,pt_ll2),1e-8);
    }
    
    void do_test_epsg() {
        projection<> prj(epsg(32632));// UTM 32N, approx norway, cartesian x,y
        point_ll pt_ll(10.6367397,59.9151928);// lilleaker oslo, long,lat epsg 4326, wgs84
        point_xy pt_expected(591520.915557,6643097.688005);
        point_xy pt_xy(0, 0);
        prj.forward(pt_ll,pt_xy);
        auto dxy= distance(pt_xy,pt_expected);
        FAST_CHECK_LE(dxy,0.1);
        std::cout<<" epsg 32632 : pt_xy:"<< bg::get<0>(pt_xy)<<","<<bg::get<1>(pt_xy)<<" dist:"<<dxy<<std::endl;
        point_ll pt_r;
        prj.inverse(pt_xy,pt_r);
        auto dll=distance(pt_r,pt_ll);
        FAST_CHECK_LE(dll,1e-7);
        std::cout<<" epsg 4346 : long,lat "<< bg::get<0>(pt_r)<<","<<bg::get<1>(pt_r)<<"dist(ll):"<<dll<<std::endl;
        
    }
    
}

#define TEST_SECTION(x)

TEST_SUITE("radiation") {
    using shyft::core::radiation::parameter;
    using shyft::core::radiation::response;
    using shyft::core::radiation::calculator;
//    using shyft::core::radiation::surface_normal;
    using shyft::core::calendar;
    using shyft::core::utctime;
    using shyft::test::trapezoidal_average;
    bool verbose= getenv("SHYFT_VERBOSE")!=nullptr;
    //bool verbose = true;
    // test basics: creation, etc

    // Data to compare with Eugene, OR, p.64, fig.1b
    // all numbers are taken from picture, so very much approximate
    // basically, I take 3 points in the year: june, january and december, teh more comprehensive test is done on python side
    calendar utc_cal;
    utctime ta_june = utc_cal.time(2002, 06, 21, 00, 00, 0, 0);
    utctime ta_december = utc_cal.time(2002, 12, 21, 00, 00, 0, 0);;
    utctime ta_january = utc_cal.time(2002, 01, 1, 00, 00, 0, 0);;
    double eu_rahor_june = 500.0;
    double eu_rs_june = 370.0;
    double eu_ra45s_june = 390.0;
    double eu_rs45s_june = 320.0;
    double eu_rahor_january = 130.0;
    double eu_rs_january = 78.0;
    double eu_ra45s_january = 380.0;
    double eu_rs45s_january = 186.0;
    double eu_rahor_december = 130.0;
    double eu_rs_december = 78.0;
    double eu_ra45s_december = 380.0;
    double eu_rs45s_december = 180.0;
    double eu_ra90s_june = 110.0;
    double eu_rs90s_june = 108.0;
    double eu_ra90s_january = 410.0;
    double eu_rs90s_january = 186.0;
    double eu_ra90s_december = 410.0;
    double eu_rs90s_december = 180.0;
    double eu_ra90n_june = 102.5;
    double eu_rs90n_june = 62.0;
    double eu_ra90n_january = 0.0;
    double eu_rs90n_january = 11.8;
    double eu_ra90n_december = 0.0;
    double eu_rs90n_december = 11.5;
    // location and weather
    double eu_lat = 44.0;
    double temperature = 20; //[deg Celcius]
    double rhumidity = 50; // [%]
    double eu_elevation = 150; //[m]
    // to compare 2 methods outputs
    double eu_inst_hor_june_rs = 0;
    double eu_inst_hor_dec_rs = 0;
    double eu_inst_hor_jan_rs = 0;



    TEST_CASE("boost_proj") {
        test::boost_proj::do_test_proj4();
        test::boost_proj::do_test_epsg();
    }
    TEST_CASE("test_sunrise_and_set") {
        double latitude = 71.8;
        parameter p;
        p.albedo = 0.2;
        p.turbidity = 1.0;
        calculator rad(p);
        // polar day
        utctime t=utc_cal.time(2002, 06, 21, 00, 00, 0, 0);
        double omegas = rad.horizontal_sunrise(latitude, t);
        CHECK_EQ(omegas, doctest::Approx(3.14).epsilon(0.05));
        // polar night
        t=utc_cal.time(2002, 01, 21, 00, 00, 0, 0);
        omegas = rad.horizontal_sunrise(latitude, t);
        CHECK_EQ(omegas, doctest::Approx(0.0).epsilon(0.05));
    }
    TEST_CASE("test_radiation_equal_operator") {
        double albedo = 0.25;
        double turbidity = 1.0;
        double al=0.34;
        double bl=0.14;
        double ac=1.35;
        double bc=0.35;
        double as = 1.25;
        double bs = 0.25;
        
        parameter p(albedo, turbidity, al, bl, ac, bc, as, bs);
        parameter p1(albedo+1.0, turbidity, al, bl, ac, bc, as, bs);
        parameter p2(albedo, turbidity+1.0, al, bl, ac, bc, as, bs);
        parameter p3(albedo, turbidity, al+1.0, bl, ac, bc, as, bs);
        parameter p4(albedo, turbidity, al, bl+1.0, ac, bc, as, bs);
        parameter p5(albedo, turbidity, al, bl, ac+1.0, bc, as, bs);
        parameter p6(albedo, turbidity, al, bl, ac, bc+1.0, as, bs);
        parameter p7(albedo, turbidity, al, bl, ac, bc, as+1.0, bs);
        parameter p8(albedo, turbidity, al, bl, ac, bc, as, bs+1.0);
        
        TS_ASSERT(p != p1);
        TS_ASSERT(p != p2);
        TS_ASSERT(p != p3);
        TS_ASSERT(p != p4);
        TS_ASSERT(p != p5);
        TS_ASSERT(p != p6);
        TS_ASSERT(p != p7);
        TS_ASSERT(p != p8);
        
        p1.albedo = albedo;
        p2.turbidity = turbidity;
        p3.al = al;
        p4.bl = bl;
        p5.ac = ac;
        p6.bc = bc;
        p7.as = as;
        p8.bs = bs;
        
        TS_ASSERT(p == p1);
        TS_ASSERT(p == p2);
        TS_ASSERT(p == p3);
        TS_ASSERT(p == p4);
        TS_ASSERT(p == p5);
        TS_ASSERT(p == p6);
        TS_ASSERT(p == p7);
        TS_ASSERT(p == p8);
    }

    TEST_CASE("Instantaneous_method") {
        parameter p;
        response r;
        p.albedo = 0.2;
        p.turbidity = 1.0;
        utctime t;
        SUBCASE("Horizontal_surface") {
            calculator rad(p);
            // checking for horizontal surface Eugene, OR, p.64, fig.1b
            double slope = 0.0;
            double aspect = 0.0;
            std::uniform_real_distribution<double> ur(150.0, 390.0);
            std::default_random_engine gen;
            double generated_rsm = ur(
                    gen); // random generator for measured radiation, we assume it same per each test run
            trapezoidal_average av_rahor;
            trapezoidal_average av_ra;
            trapezoidal_average av_rs;
            trapezoidal_average av_rso;
            SUBCASE("June_h") {
                //ta = utc_cal.time(2002, 06, 21, 00, 00, 0, 0);
                //rad.pnet_sw(r, lat, ta, surface_normal, 20.0, 50.0, 150.0);
                rad.net_radiation_inst(r, eu_lat, ta_june, slope, aspect, temperature, rhumidity, eu_elevation,
                                       generated_rsm);
                av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
                av_ra.initialize(rad.ra_radiation(), 0.0);
                av_rs.initialize(r.sw_cs_p, 0.0);
                av_rso.initialize(r.sw_t, 0.0);
                for (int h = 1; h <= 23; ++h) {
                    if (h<24){
                    t = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                     }
                    else {t = utc_cal.time(2002, 06, 22, 0, 00, 0, 0);} // becasue calendar doesn't allow 24 h

                    rad.net_radiation_inst(r, eu_lat, t, slope, aspect, temperature, rhumidity, eu_elevation,
                                           generated_rsm);
                    av_rahor.add(rad.ra_radiation_hor(), h);
                    av_ra.add(rad.ra_radiation(), h);
                    av_rs.add(r.sw_cs_p, h);
                    av_rso.add(r.sw_t, h);
                }
                        CHECK_EQ(av_ra.result(), doctest::Approx(eu_rahor_june).epsilon(0.05));
                        CHECK_EQ(av_rahor.result(), doctest::Approx(av_ra.result()).epsilon(0.05));
                        CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs_june).epsilon(0.05));
                        CHECK_EQ(av_rso.result(), doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose) {
                    std::cout << "---------- June, inst, hor ------- " << std::endl;
                    std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                    std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                    std::cout << "rso (translated): " << av_rso.result() << std::endl;
                    std::cout << "rsm generated: " << generated_rsm << std::endl;
                }

            }
            SUBCASE("January_h") {
                //ta = utc_cal.time(2002, 01, 1, 00, 00, 0, 0); // January
                rad.net_radiation_inst(r, eu_lat, ta_january, slope, aspect, temperature, rhumidity, eu_elevation,
                                       generated_rsm);
                av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
                av_ra.initialize(rad.ra_radiation(), 0.0);
                av_rs.initialize(r.sw_cs_p, 0.0);
                av_rso.initialize(r.sw_t, 0.0);
                for (int h = 1; h <= 23; ++h) {
                    if (h<24) {
                        t = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // January
                    }
                    else {t = utc_cal.time(2002, 01, 2, 0, 00, 0, 0);}
                    rad.net_radiation_inst(r, eu_lat, t, slope, aspect, temperature, rhumidity, eu_elevation,
                                           generated_rsm);
                    av_rahor.add(rad.ra_radiation_hor(), h);
                    av_ra.add(rad.ra_radiation(), h);
                    av_rs.add(r.sw_cs_p, h);
                    av_rso.add(r.sw_t, h);
                }
                        CHECK_EQ(av_ra.result(), doctest::Approx(eu_rahor_january).epsilon(0.05));
                        CHECK_EQ(av_rahor.result(), doctest::Approx(av_ra.result()).epsilon(0.05));
                        CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs_january).epsilon(0.05));
                        CHECK_EQ(av_rso.result(), doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose) {
                    std::cout << "---------- January, inst, hor ------- " << std::endl;
                    std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                    std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                    std::cout << "rso (translated): " << av_rs.result() << std::endl;
                    std::cout << "rsm generated: " << generated_rsm << std::endl;
                }

            }
            SUBCASE("December_h") {
                //ta = utc_cal.time(2002, 12, 21, 00, 00, 0, 0);// December
                rad.net_radiation_inst(r, eu_lat, ta_december, slope, aspect, temperature, rhumidity, eu_elevation,
                                       generated_rsm);
                av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
                av_ra.initialize(rad.ra_radiation(), 0.0);
                av_rs.initialize(r.sw_cs_p, 0.0);
                av_rso.initialize(r.sw_t, 0.0);
                for (int h = 1; h <= 23; ++h) {
                    if (h<24) {
                        t = utc_cal.time(2002, 12, 21, h, 00, 0, 0); // January
                    }
                    else {t = utc_cal.time(2002, 12, 22,0, 00, 0, 0);}
                    rad.net_radiation_inst(r, eu_lat, t, slope, aspect, temperature, rhumidity, eu_elevation,
                                           generated_rsm);
                    av_rahor.add(rad.ra_radiation_hor(), h);
                    av_ra.add(rad.ra_radiation(), h);
                    av_rs.add(r.sw_cs_p, h);
                    av_rso.add(r.sw_t, h);
                }
                        CHECK_EQ(av_ra.result(), doctest::Approx(eu_rahor_december).epsilon(0.05));
                        CHECK_EQ(av_rahor.result(), doctest::Approx(av_ra.result()).epsilon(0.05));
                        CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs_december).epsilon(0.05));
                        CHECK_EQ(av_rso.result(), doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose) {
                    std::cout << "---------- December, inst, hor ------- " << std::endl;
                    std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                    std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                    std::cout << "rso (translated): " << av_rs.result() << std::endl;
                    std::cout << "rsm generated: " << generated_rsm << std::endl;
                }

            }
        }
        SUBCASE("check_solar_radiation_slope_45s_inst") {
            calculator rad(p);
            // checking for horizontal surface Eugene, OR, p.64, fig.1d
            // 24h  average radiation
            double slope = 45;//*shyft::core::radiation::pi/180; // 45 S
            double aspect = 0.0;//*shyft::core::radiation::pi/180;// facing south
            trapezoidal_average av_rahor;
            trapezoidal_average av_ra;
            trapezoidal_average av_rs;
            trapezoidal_average av_rso;
            std::uniform_real_distribution<double> ur(100.0, 390.0);
            std::default_random_engine gen;
            double generated_rsm = ur(gen); // random generator for measured radiation, we assume it same per each test run
            SUBCASE("June_45s") {
                // ta = utc_cal.time(2002, 06, 21, 00, 00, 0, 0);
                rad.net_radiation_inst(r, eu_lat, ta_june, slope, aspect, temperature, rhumidity, eu_elevation,
                                       generated_rsm);
                av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
                av_ra.initialize(rad.ra_radiation(), 0.0);
                av_rs.initialize(r.sw_cs_p, 0.0);
                av_rso.initialize(r.sw_t, 0.0);
                for (int h = 1; h <= 23; ++h) {
                    if (h<24) {
                        t = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                    }
                    else {t = utc_cal.time(2002, 06, 22, 0, 00, 0, 0);}
                    rad.net_radiation_inst(r, eu_lat, t, slope, aspect, temperature, rhumidity, eu_elevation,
                                           generated_rsm);
                    av_rahor.add(rad.ra_radiation_hor(), h);
                    av_ra.add(rad.ra_radiation(), h);
                    av_rs.add(r.sw_cs_p, h);
                    av_rso.add(r.sw_t, h);
                }
                        CHECK_EQ(av_ra.result(), doctest::Approx(eu_ra45s_june).epsilon(0.05));
                        CHECK_EQ(av_rahor.result(), doctest::Approx(eu_rahor_june).epsilon(0.05));
                        CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs45s_june).epsilon(0.05));
                //CHECK_EQ(av_rso.result(), doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose) {
                    std::cout << "---------- June, inst, 45s ------- " << std::endl;
                    std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                    std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                    std::cout << "rso (translated): " << av_rso.result() << std::endl;
                    std::cout << "rsm generated: " << generated_rsm << std::endl;
                    std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
                    std::cout << "sun_set: " << rad.sun_set() << std::endl;
                }
            }
            SUBCASE("January_45s") {
                //ta = utc_cal.time(2002, 01, 1, 00, 00, 0, 0); // January
                rad.net_radiation_inst(r, eu_lat, ta_january, slope, aspect, temperature, rhumidity, eu_elevation,
                                       generated_rsm);
                av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
                av_ra.initialize(rad.ra_radiation(), 0.0);
                av_rs.initialize(r.sw_cs_p, 0.0);
                av_rso.initialize(r.sw_t, 0.0);
                for (int h = 1; h <= 23; ++h) {
                    if (h<24) {
                        t = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // January
                    }
                    else{t = utc_cal.time(2002, 01, 2, 0, 00, 0, 0);}
                    rad.net_radiation_inst(r, eu_lat, t, slope, aspect, temperature, rhumidity, eu_elevation,
                                           generated_rsm);
                    av_rahor.add(rad.ra_radiation_hor(), h);
                    av_ra.add(rad.ra_radiation(), h);
                    av_rs.add(r.sw_cs_p, h);
                    av_rso.add(r.sw_t, h);
                }
                        CHECK_EQ(av_ra.result(), doctest::Approx(eu_ra45s_january).epsilon(0.05));
                        CHECK_EQ(av_rahor.result(), doctest::Approx(eu_rahor_january).epsilon(0.05));
                        CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs45s_january).epsilon(0.05));
                //CHECK_EQ(av_rso.result(), doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose) {
                    std::cout << "---------- January, inst, 45s ------- " << std::endl;
                    std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                    std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                    std::cout << "rso (translated): " << av_rso.result() << std::endl;
                    std::cout << "rsm generated: " << generated_rsm << std::endl;
                    std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
                    std::cout << "sun_set: " << rad.sun_set() << std::endl;
                }
            }
            SUBCASE("December_45s") {
                //ta = utc_cal.time(2002, 12, 12, 00, 00, 0, 0); // December
                rad.net_radiation_inst(r, eu_lat, ta_december, slope, aspect, temperature, rhumidity, eu_elevation,
                                       generated_rsm);
                av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
                av_ra.initialize(rad.ra_radiation(), 0.0);
                av_rs.initialize(r.sw_cs_p, 0.0);
                av_rso.initialize(r.sw_t, 0.0);
                for (int h = 1; h <= 23; ++h) {
                    if (h<24) {
                        t = utc_cal.time(2002, 12, 21, h, 00, 0, 0); // June
                    }
                    else{t = utc_cal.time(2002, 12, 22, 0, 00, 0, 0);}
                    rad.net_radiation_inst(r, eu_lat, t, slope, aspect, temperature, rhumidity, eu_elevation,
                                           generated_rsm);
                    av_rahor.add(rad.ra_radiation_hor(), h);
                    av_ra.add(rad.ra_radiation(), h);
                    av_rs.add(r.sw_cs_p, h);
                    av_rso.add(r.sw_t, h);
                }
                        CHECK_EQ(av_ra.result(), doctest::Approx(eu_ra45s_december).epsilon(0.05));
                        CHECK_EQ(av_rahor.result(), doctest::Approx(eu_rahor_december).epsilon(0.05));
                        CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs45s_december).epsilon(0.05));
                //CHECK_EQ(av_rso.result(), doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose) {
                    std::cout << "---------- December, inst, 45s ------- " << std::endl;
                    std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                    std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                    std::cout << "rso (translated): " << av_rso.result() << std::endl;
                    std::cout << "rsm generated: " << generated_rsm << std::endl;
                    std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
                    std::cout << "sun_set: " << rad.sun_set() << std::endl;
                }
            }
        }
        SUBCASE("check_solar_radiation_slope_90s_inst"){
            parameter p;
            response r;
            p.albedo = 0.05;
            p.turbidity = 1.0;
            calculator rad(p);
            calendar utc_cal;
            utctime t;
            // checking for horizontal surface Eugene, OR, p.64, fig.1d
            // 24h  average radiation
            double slope = 90;
            double aspect = 0.0;

            trapezoidal_average av_rahor;
            trapezoidal_average av_ra;
            trapezoidal_average av_rs;
            trapezoidal_average av_rso;
            std::uniform_real_distribution<double> ur(0.0, 250.0);
            std::default_random_engine gen;
            double generated_rsm = ur(gen);
            SUBCASE("June_90s") {
                rad.net_radiation_inst(r, eu_lat, ta_june, slope, aspect, temperature, rhumidity, eu_elevation,
                                       generated_rsm);
                av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
                av_ra.initialize(rad.ra_radiation(), 0.0);
                av_rs.initialize(r.sw_cs_p, 0.0);
                av_rso.initialize(r.sw_t, 0.0);
                for (int h = 1; h <= 23; ++h) {
                    if (h<24) {
                        t = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                    }
                    else{t = utc_cal.time(2002, 06, 22, 0, 00, 0, 0);}
                    rad.net_radiation_inst(r, eu_lat, t, slope, aspect, temperature, rhumidity, eu_elevation,
                                           generated_rsm);
                    av_rahor.add(rad.ra_radiation_hor(), h);
                    av_ra.add(rad.ra_radiation(), h);
                    av_rs.add(r.sw_cs_p, h);
                    av_rso.add(r.sw_t, h);
                }
                CHECK_EQ(av_ra.result(), doctest::Approx(eu_ra90s_june).epsilon(0.05));
                CHECK_EQ(av_rahor.result(), doctest::Approx(eu_rahor_june).epsilon(0.05));
                CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs90s_june).epsilon(0.05));
                //CHECK_EQ(av_rso.result(), doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose) {
                    std::cout << "---------- June, inst, 90s ------- " << std::endl;
                    std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                    std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                    std::cout << "rso (translated): " << av_rso.result() << std::endl;
                    std::cout << "rsm generated: " << generated_rsm << std::endl;
                    std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
                    std::cout << "sun_set: " << rad.sun_set() << std::endl;
                }
            }
            SUBCASE("January_90s") {
                rad.net_radiation_inst(r, eu_lat, ta_january, slope, aspect, temperature, rhumidity, eu_elevation,
                                       generated_rsm);
                av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
                av_ra.initialize(rad.ra_radiation(), 0.0);
                av_rs.initialize(r.sw_cs_p, 0.0);
                av_rso.initialize(r.sw_t, 0.0);
                for (int h = 1; h <= 23; ++h) {
                    if (h<24) {
                        t = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // January
                    }
                    else{t = utc_cal.time(2002, 01, 2, 0, 00, 0, 0);}
                    rad.net_radiation_inst(r, eu_lat, t, slope, aspect, temperature, rhumidity, eu_elevation,
                                           generated_rsm);
                    av_rahor.add(rad.ra_radiation_hor(), h);
                    av_ra.add(rad.ra_radiation(), h);
                    av_rs.add(r.sw_cs_p, h);
                    av_rso.add(r.sw_t, h);
                }
                CHECK_EQ(av_ra.result(), doctest::Approx(eu_ra90s_january).epsilon(0.05));
                CHECK_EQ(av_rahor.result(), doctest::Approx(eu_rahor_january).epsilon(0.05));
                CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs90s_january).epsilon(0.05));
                //CHECK_EQ(av_rso.result(), doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose) {
                    std::cout << "---------- January, inst, 90s ------- " << std::endl;
                    std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                    std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                    std::cout << "rso (translated): " << av_rso.result() << std::endl;
                    std::cout << "rsm generated: " << generated_rsm << std::endl;
                    std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
                    std::cout << "sun_set: " << rad.sun_set() << std::endl;
                }
            }
            SUBCASE("December_90s") {
                rad.net_radiation_inst(r, eu_lat, ta_december, slope, aspect, temperature, rhumidity, eu_elevation,
                                       generated_rsm);
                av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
                av_ra.initialize(rad.ra_radiation(), 0.0);
                av_rs.initialize(r.sw_cs_p, 0.0);
                av_rso.initialize(r.sw_t, 0.0);
                for (int h = 1; h <= 23; ++h) {
                    if (h<24) {
                        t = utc_cal.time(2002, 12, 21, h, 00, 0, 0); // December
                    }
                    else{t = utc_cal.time(2002, 12, 22, 0, 00, 0, 0);}
                    rad.net_radiation_inst(r, eu_lat, t, slope, aspect, temperature, rhumidity, eu_elevation,
                                           generated_rsm);
                    av_rahor.add(rad.ra_radiation_hor(), h);
                    av_ra.add(rad.ra_radiation(), h);
                    av_rs.add(r.sw_cs_p, h);
                    av_rso.add(r.sw_t, h);
                }
                CHECK_EQ(av_ra.result(), doctest::Approx(eu_ra90s_december).epsilon(0.05));
                CHECK_EQ(av_rahor.result(), doctest::Approx(eu_rahor_december).epsilon(0.05));
                CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs90s_december).epsilon(0.05));
                //CHECK_EQ(av_rso.result(), doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose) {
                    std::cout << "---------- December, inst, 90s ------- " << std::endl;
                    std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                    std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                    std::cout << "rso (translated): " << av_rso.result() << std::endl;
                    std::cout << "rsm generated: " << generated_rsm << std::endl;
                    std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
                    std::cout << "sun_set: " << rad.sun_set() << std::endl;
                }
            }
        }
        SUBCASE("check_solar_radiation_slope_90n_inst") {
            parameter p;
            response r;
            p.albedo = 0.05;
            p.turbidity = 1.0;
            calculator rad(p);
            calendar utc_cal;
            utctime t;
            // checking for horizontal surface Eugene, OR, p.64, fig.1d
            // 24h  average radiation
            double slope = 90;
            double aspect = 180.0;

            trapezoidal_average av_rahor;
            trapezoidal_average av_ra;
            trapezoidal_average av_rs;
            trapezoidal_average av_rso;
            std::uniform_real_distribution<double> ur(0.0, 100.0);
            std::default_random_engine gen;
            double generated_rsm = ur(gen);
            SUBCASE("June_90n") {
                rad.net_radiation_inst(r, eu_lat, ta_june, slope, aspect, temperature, rhumidity, eu_elevation,
                                       generated_rsm);
                av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
                av_ra.initialize(rad.ra_radiation(), 0.0);
                av_rs.initialize(r.sw_cs_p, 0.0);
                av_rso.initialize(r.sw_t, 0.0);
                for (int h = 1; h <= 23; ++h) {
                    if (h < 24) {
                        t = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                    } else { t = utc_cal.time(2002, 06, 22, 0, 00, 0, 0); }
                    rad.net_radiation_inst(r, eu_lat, t, slope, aspect, temperature, rhumidity, eu_elevation,
                                           generated_rsm);
                    av_rahor.add(rad.ra_radiation_hor(), h);
                    av_ra.add(rad.ra_radiation(), h);
                    av_rs.add(r.sw_cs_p, h);
                    av_rso.add(r.sw_t, h);
                }
                        CHECK_EQ(av_ra.result(), doctest::Approx(eu_ra90n_june).epsilon(0.05));
                        CHECK_EQ(av_rahor.result(), doctest::Approx(eu_rahor_june).epsilon(0.05));
                        CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs90n_june).epsilon(0.05));
                //CHECK_EQ(av_rso.result(), doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose) {
                    std::cout << "---------- June, inst, 90n ------- " << std::endl;
                    std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                    std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                    std::cout << "rso (translated): " << av_rso.result() << std::endl;
                    std::cout << "rsm generated: " << generated_rsm << std::endl;
                    std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
                    std::cout << "sun_set: " << rad.sun_set() << std::endl;
                }
            }
            SUBCASE("January_90n") {
                rad.net_radiation_inst(r, eu_lat, ta_january, slope, aspect, temperature, rhumidity, eu_elevation,
                                       generated_rsm);
                av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
                av_ra.initialize(rad.ra_radiation(), 0.0);
                av_rs.initialize(r.sw_cs_p, 0.0);
                av_rso.initialize(r.sw_t, 0.0);
                for (int h = 1; h <= 23; ++h) {
                    if (h < 24) {
                        t = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // January
                    } else { t = utc_cal.time(2002, 01, 2, 0, 00, 0, 0); }
                    rad.net_radiation_inst(r, eu_lat, t, slope, aspect, temperature, rhumidity, eu_elevation,
                                           generated_rsm);
                    av_rahor.add(rad.ra_radiation_hor(), h);
                    av_ra.add(rad.ra_radiation(), h);
                    av_rs.add(r.sw_cs_p, h);
                    av_rso.add(r.sw_t, h);
                }
                        CHECK_EQ(av_ra.result(), doctest::Approx(eu_ra90n_january).epsilon(0.05));
                        CHECK_EQ(av_rahor.result(), doctest::Approx(eu_rahor_january).epsilon(0.05));
                        CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs90n_january).epsilon(0.05));
                //CHECK_EQ(av_rso.result(), doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose) {
                    std::cout << "---------- January, inst, 90n ------- " << std::endl;
                    std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                    std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                    std::cout << "rso (translated): " << av_rso.result() << std::endl;
                    std::cout << "rsm generated: " << generated_rsm << std::endl;
                    std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
                    std::cout << "sun_set: " << rad.sun_set() << std::endl;
                }
            }
            SUBCASE("December_90n") {
                rad.net_radiation_inst(r, eu_lat, ta_december, slope, aspect, temperature, rhumidity, eu_elevation,
                                       generated_rsm);
                av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
                av_ra.initialize(rad.ra_radiation(), 0.0);
                av_rs.initialize(r.sw_cs_p, 0.0);
                av_rso.initialize(r.sw_t, 0.0);
                for (int h = 1; h <= 23; ++h) {
                    if (h < 24) {
                        t = utc_cal.time(2002, 12, 21, h, 00, 0, 0); // December
                    } else { t = utc_cal.time(2002, 12, 22, 0, 00, 0, 0); }
                    rad.net_radiation_inst(r, eu_lat, t, slope, aspect, temperature, rhumidity, eu_elevation,
                                           generated_rsm);
                    av_rahor.add(rad.ra_radiation_hor(), h);
                    av_ra.add(rad.ra_radiation(), h);
                    av_rs.add(r.sw_cs_p, h);
                    av_rso.add(r.sw_t, h);
                }
                        CHECK_EQ(av_ra.result(), doctest::Approx(eu_ra90n_december).epsilon(0.05));
                        CHECK_EQ(av_rahor.result(), doctest::Approx(eu_rahor_december).epsilon(0.05));
                        CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs90n_december).epsilon(0.05));
                //CHECK_EQ(av_rso.result(), doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose) {
                    std::cout << "---------- December, inst, 90n ------- " << std::endl;
                    std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                    std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                    std::cout << "rso (translated): " << av_rso.result() << std::endl;
                    std::cout << "rsm generated: " << generated_rsm << std::endl;
                    std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
                    std::cout << "sun_set: " << rad.sun_set() << std::endl;
                }
            }
        }
    }

    TEST_CASE("Step_method"){
        parameter p;
        response r;
        p.albedo = 0.2;
        p.turbidity = 1.0;
        utctime t1;
        auto dt = shyft::core::deltahours(1);
        SUBCASE("Horizontal_surface"){
            calculator rad(p);
            // checking for horizontal surface Eugene, OR, p.64, fig.1b
            double slope = 0.0;
            double aspect = 0.0;
            std::uniform_real_distribution<double> ur(150.0, 390.0);
            std::default_random_engine gen;
            double generated_rsm = ur(gen); // random generator for measured radiation, we assume it same per each test run
            SUBCASE("June") {
                double rastep = 0.0;
                double rsstep = 0.0;
                double rsostep = 0.0;
                for (int h = 0; h < 24; ++h) {
                    t1 = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                    rad.net_radiation_step(r, eu_lat, t1,dt, slope, aspect, temperature, rhumidity, eu_elevation,generated_rsm/23);
                    rastep+=r.ra;
                    rsstep+=r.sw_cs_p;
                    rsostep+=r.sw_t;
                }
                CHECK_EQ(rastep, doctest::Approx(eu_rahor_june).epsilon(0.05));
                CHECK_EQ(rsstep, doctest::Approx(eu_rs_june).epsilon(0.05));
                CHECK_EQ(rsostep, doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose){
                    std::cout << "---------- June, step, hor ------- " << std::endl;
                    std::cout << "ra: " << rastep<<std::endl;
                    std::cout << "rs (predicted): " << rsstep<<std::endl;
                    std::cout << "rso (translated): " << rsostep<<std::endl;
                    std::cout << "rsm generated: " << generated_rsm<<std::endl;
                }
            }
            SUBCASE("January") {
                double rastep = 0.0;
                double rsstep = 0.0;
                double rsostep = 0.0;
                for (int h = 0; h < 24; ++h) {
                    t1 = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // January
                    rad.net_radiation_step(r, eu_lat, t1,dt, slope, aspect, temperature, rhumidity, eu_elevation,generated_rsm/23);
                    rastep+=r.ra;
                    rsstep+=r.sw_cs_p;
                    rsostep+=r.sw_t;
                }
                CHECK_EQ(rastep, doctest::Approx(eu_rahor_january).epsilon(0.05));
                CHECK_EQ(rsstep, doctest::Approx(eu_rs_january).epsilon(0.05));
                CHECK_EQ(rsostep, doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose){
                    std::cout << "---------- January, step, hor ------- " << std::endl;
                    std::cout << "ra: " << rastep<<std::endl;
                    std::cout << "rs (predicted): " << rsstep<<std::endl;
                    std::cout << "rso (translated): " << rsostep<<std::endl;
                    std::cout << "rsm generated: " << generated_rsm<<std::endl;
                }

            }
            SUBCASE("December") {
                double rastep = 0.0;
                double rsostep = 0.0;
                double rsstep = 0.0;
                for (int h = 0; h < 24; ++h) {
                    t1 = utc_cal.time(2002, 12, 21, h, 00, 0, 0); // June
                    rad.net_radiation_step(r, eu_lat, t1,dt, slope, aspect, temperature, rhumidity, eu_elevation,generated_rsm/23);
                    rastep+=r.ra;
                    rsstep+=r.sw_cs_p;
                    rsostep+=r.sw_t;
                }
                CHECK_EQ(rastep, doctest::Approx(eu_rahor_december).epsilon(0.05));
                CHECK_EQ(rsstep, doctest::Approx(eu_rs_december).epsilon(0.05));
                CHECK_EQ(rsostep, doctest::Approx(generated_rsm).epsilon(0.05));
                if (verbose){
                    std::cout << "---------- December, step, hor ------- " << std::endl;
                    std::cout << "ra: " << rastep<<std::endl;
                    std::cout << "rs (predicted): " << rsstep<<std::endl;
                    std::cout << "rso (translated): " << rsostep<<std::endl;
                    std::cout << "rsm generated: " << generated_rsm<<std::endl;
                }

            }

        }
        SUBCASE("check_solar_radiation_45s_step"){
            calculator rad(p);
            // checking for 45s surface Eugene, OR, p.66, fig.3d
            double slope = 45.0;
            double aspect = 0.0;
            std::uniform_real_distribution<double> ur(100.0, 390.0);
            std::default_random_engine gen;
            double generated_rsm = ur(gen); // random generator for measured radiation, we assume it same per each test run
            SUBCASE("June_45s") {
                double rastep = 0.0;
                double rsostep = 0.0;
                double rsstep = 0.0;
                for (int h = 0; h < 24; ++h) {
                    t1 = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                    rad.net_radiation_step(r, eu_lat, t1,dt, slope, aspect, temperature, rhumidity, eu_elevation,generated_rsm/23);
                    rastep+=r.ra;
                    rsstep+=r.sw_cs_p;
                    rsostep+=r.sw_t;
                }
                CHECK_EQ(rastep, doctest::Approx(eu_ra45s_june).epsilon(0.05));
                CHECK_EQ(rsstep, doctest::Approx(eu_rs45s_june).epsilon(0.05));
                if (verbose){
                    std::cout << "---------- June, step, 45s ------- " << std::endl;
                    std::cout << "ra: " << rastep<<std::endl;
                    std::cout << "rs (predicted): " << rsstep<<std::endl;
                    std::cout << "rso (translated): " << rsostep<<std::endl;
                    std::cout << "rsm generated: " << generated_rsm<<std::endl;
                }
            }
            SUBCASE("January_45s") {
                double rastep = 0.0;
                double rsostep = 0.0;
                double rsstep = 0.0;
                for (int h = 0; h < 24; ++h) {
                    t1 = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // December
                    rad.net_radiation_step(r, eu_lat, t1,dt, slope, aspect, temperature, rhumidity, eu_elevation,generated_rsm/23);
                    rastep+=r.ra;
                    rsstep+=r.sw_cs_p;
                    rsostep+=r.sw_t;
                }
                CHECK_EQ(rastep, doctest::Approx(eu_ra45s_january).epsilon(0.05));
                CHECK_EQ(rsstep, doctest::Approx(eu_rs45s_january).epsilon(0.05));
                if (verbose){
                    std::cout << "---------- January, step, 45s ------- " << std::endl;
                    std::cout << "ra: " << rastep<<std::endl;
                    std::cout << "rs (predicted): " << rsstep<<std::endl;
                    std::cout << "rso (translated): " << rsostep<<std::endl;
                    std::cout << "rsm generated: " << generated_rsm<<std::endl;
                }
            }
            SUBCASE("December_45s") {
                double rastep = 0.0;
                double rsostep = 0.0;
                double rsstep = 0.0;
                for (int h = 0; h < 24; ++h) {
                    t1 = utc_cal.time(2002, 12, 21, h, 00, 0, 0); // December
                    rad.net_radiation_step(r, eu_lat, t1,dt, slope, aspect, temperature, rhumidity, eu_elevation,generated_rsm/23);
                    rastep+=r.ra;
                    rsstep+=r.sw_cs_p;
                    rsostep+=r.sw_t;
                }
                CHECK_EQ(rastep, doctest::Approx(eu_ra45s_december).epsilon(0.05));
                CHECK_EQ(rsstep, doctest::Approx(eu_rs45s_december).epsilon(0.05));
                if (verbose){
                    std::cout << "---------- December, step, 45s ------- " << std::endl;
                    std::cout << "ra: " << rastep<<std::endl;
                    std::cout << "rs (predicted): " << rsstep<<std::endl;
                    std::cout << "rso (translated): " << rsostep<<std::endl;
                    std::cout << "rsm generated: " << generated_rsm<<std::endl;
                };
            }
        }
        SUBCASE("check_solar_radiation_90s_step"){
            parameter p;
            response r;
            p.albedo = 0.05;
            p.turbidity = 1.0;
            calculator rad(p);
            // checking for horizontal surface Eugene, OR, p.64, fig.1b
            double slope = 90.0;
            double aspect = 0.0;
            std::uniform_real_distribution<double> ur(0.0, 250.0);
            std::default_random_engine gen;
            double generated_rsm = ur(gen); // random generator for measured radiation, we assume it same per each test run
            SUBCASE("June_90s") {
                double rastep = 0.0;
                double rsostep = 0.0;
                double rsstep = 0.0;
                for (int h = 0; h < 24; ++h) {
                    t1 = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                    rad.net_radiation_step(r, eu_lat, t1,dt, slope, aspect, temperature, rhumidity, eu_elevation,generated_rsm/23);
                    rastep+=r.ra;
                    rsstep+=r.sw_cs_p;
                    rsostep+=r.sw_t;
                }
                CHECK_EQ(rastep, doctest::Approx(eu_ra90s_june).epsilon(0.05));
                CHECK_EQ(rsstep, doctest::Approx(eu_rs90s_june).epsilon(0.05));
                if (verbose){
                    std::cout << "---------- June, step, 90s ------- " << std::endl;
                    std::cout << "ra: " << rastep<<std::endl;
                    std::cout << "rs (predicted): " << rsstep<<std::endl;
                    std::cout << "rso (translated): " << rsostep<<std::endl;
                    std::cout << "rsm generated: " << generated_rsm<<std::endl;
                };
            }
            SUBCASE("January_90s") {
                double rastep = 0.0;
                double rsstep = 0.0;
                double rsostep = 0.0;
                for (int h = 0; h < 24; ++h) {
                    t1 = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // January
                    rad.net_radiation_step(r, eu_lat, t1,dt, slope, aspect, temperature, rhumidity, eu_elevation,generated_rsm/23);
                    rastep+=r.ra;
                    rsstep+=r.sw_cs_p;
                    rsostep+=r.sw_t;
                }
                CHECK_EQ(rastep, doctest::Approx(eu_ra90s_january).epsilon(0.05));
                CHECK_EQ(rsstep, doctest::Approx(eu_rs90s_january).epsilon(0.05));
                if (verbose){
                    std::cout << "---------- January, step, 90s ------- " << std::endl;
                    std::cout << "ra: " << rastep<<std::endl;
                    std::cout << "rs (predicted): " << rsstep<<std::endl;
                    std::cout << "rso (translated): " << rsostep<<std::endl;
                    std::cout << "rsm generated: " << generated_rsm<<std::endl;
                };
            }
            SUBCASE("December_90s") {
                double rastep = 0.0;
                double rsostep = 0.0;
                double rsstep = 0.0;
                for (int h = 0; h < 24; ++h) {
                    t1 = utc_cal.time(2002, 12, 21, h, 00, 0, 0); // December
                    rad.net_radiation_step(r, eu_lat, t1,dt, slope, aspect, temperature, rhumidity, eu_elevation,generated_rsm/23);
                    rastep+=r.ra;
                    rsstep+=r.sw_cs_p;
                    rsostep+=r.sw_t;
                }
                CHECK_EQ(rastep, doctest::Approx(eu_ra90s_december).epsilon(0.05));
                CHECK_EQ(rsstep, doctest::Approx(eu_rs90s_december).epsilon(0.05));
                if (verbose){
                    std::cout << "---------- December, step, 90s ------- " << std::endl;
                    std::cout << "ra: " << rastep<<std::endl;
                    std::cout << "rs (predicted): " << rsstep<<std::endl;
                    std::cout << "rso (translated): " << rsostep<<std::endl;
                    std::cout << "rsm generated: " << generated_rsm<<std::endl;
                };
            }
        }
        SUBCASE("check_solar_radiation_90n_step"){
            parameter p;
            response r;
            p.albedo = 0.05;
            p.turbidity = 1.0;
            calculator rad(p);
            // checking for horizontal surface Eugene, OR, p.64, fig.1b
            double slope = 90.0;
            double aspect = 180.0;
            std::uniform_real_distribution<double> ur(0.0, 100.0);
            std::default_random_engine gen;
            double generated_rsm = ur(gen); // random generator for measured radiation, we assume it same per each test run
            SUBCASE("June_90n") {
                double rastep = 0.0;
                double rsostep = 0.0;
                double rsstep = 0.0;
                for (int h = 0; h < 24; ++h) {
                    t1 = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                    rad.net_radiation_step(r, eu_lat, t1,dt, slope, aspect, temperature, rhumidity, eu_elevation,generated_rsm/23);
                    rastep+=r.ra;
                    rsstep+=r.sw_cs_p;
                    rsostep+=r.sw_t;
                }
                        CHECK_EQ(rastep, doctest::Approx(eu_ra90n_june).epsilon(0.05));
                        CHECK_EQ(rsstep, doctest::Approx(eu_rs90n_june).epsilon(0.05));
                if (verbose){
                    std::cout << "---------- June, step, 90n ------- " << std::endl;
                    std::cout << "ra: " << rastep<<std::endl;
                    std::cout << "rs (predicted): " << rsstep<<std::endl;
                    std::cout << "rso (translated): " << rsostep<<std::endl;
                    std::cout << "rsm generated: " << generated_rsm<<std::endl;
                };
            }
            SUBCASE("January_90n") {
                double rastep = 0.0;
                double rsstep = 0.0;
                double rsostep = 0.0;
                for (int h = 0; h < 24; ++h) {
                    t1 = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // January
                    rad.net_radiation_step(r, eu_lat, t1,dt, slope, aspect, temperature, rhumidity, eu_elevation,generated_rsm/23);
                    rastep+=r.ra;
                    rsstep+=r.sw_cs_p;
                    rsostep+=r.sw_t;
                }
                        CHECK_EQ(rastep, doctest::Approx(eu_ra90n_january).epsilon(0.05));
                        CHECK_EQ(rsstep, doctest::Approx(eu_rs90n_january).epsilon(0.05));
                if (verbose){
                    std::cout << "---------- January, step, 90n ------- " << std::endl;
                    std::cout << "ra: " << rastep<<std::endl;
                    std::cout << "rs (predicted): " << rsstep<<std::endl;
                    std::cout << "rso (translated): " << rsostep<<std::endl;
                    std::cout << "rsm generated: " << generated_rsm<<std::endl;
                };
            }
            SUBCASE("December_90n") {
                double rastep = 0.0;
                double rsostep = 0.0;
                double rsstep = 0.0;
                for (int h = 0; h < 24; ++h) {
                    t1 = utc_cal.time(2002, 12, 21, h, 00, 0, 0); // December
                    rad.net_radiation_step(r, eu_lat, t1,dt, slope, aspect, temperature, rhumidity, eu_elevation,generated_rsm/23);
                    rastep+=r.ra;
                    rsstep+=r.sw_cs_p;
                    rsostep+=r.sw_t;
                }
                        CHECK_EQ(rastep, doctest::Approx(eu_ra90n_december).epsilon(0.05));
                        CHECK_EQ(rsstep, doctest::Approx(eu_rs90n_december).epsilon(0.05));
                if (verbose){
                    std::cout << "---------- December, step, 90n ------- " << std::endl;
                    std::cout << "ra: " << rastep<<std::endl;
                    std::cout << "rs (predicted): " << rsstep<<std::endl;
                    std::cout << "rso (translated): " << rsostep<<std::endl;
                    std::cout << "rsm generated: " << generated_rsm<<std::endl;
                };
            }
        }

    } // end of Test Case
    TEST_CASE("Automatic_method"){
        parameter p;
        p.albedo = 0.2;
        p.turbidity = 1.0;
        utctime t1;
        utctime t_inst;
        auto dt_step1h = shyft::core::deltahours(1);
        auto dt_step3h = shyft::core::deltahours(3);
        auto dt_step6h = shyft::core::deltahours(6);
        auto dt_step12h = shyft::core::deltahours(12);
        auto dt_step24h = shyft::core::deltahours(24);

        // checking for horizontal surface Eugene, OR, p.64, fig.1b

        // 1-h
        double rsm[24]={0, 0, 0, 0, 0, 10, 20, 30, 60, 70, 80, 90, 180, 90, 80, 70, 60, 30, 20, 10, 0, 0, 0, 0};
        //3h average
        double rsm_3h[8]={};
        double rsm_avg=0;
        for (int k = 0; k<8;k++) {
            for (int i = 3*k; i < (3+3*k); i ++) {
                rsm_avg += rsm[i];
            }
            rsm_3h[k] = rsm_avg/3;
            rsm_avg=0;
        }
        //6h average
        double rsm_6h[4]={};
        for (int k = 0; k<4;k++) {
            for (int i = 6*k; i < (6+6*k); i ++) {
                rsm_avg += rsm[i];
            }
            rsm_6h[k] = rsm_avg/6;
            rsm_avg=0;
        }
        //12h average
        double rsm_12h[2]={};
        for (int k = 0; k<2;k++) {
            for (int i = 12*k; i < (12+12*k); i ++) {
                rsm_avg += rsm[i];
            }
            rsm_12h[k] = rsm_avg/12;
            rsm_avg=0;
        }
        //24h-average
        double rsm_24h={};
        for (int i = 0; i < 24; i ++) {
            rsm_avg += rsm[i];
        }
        rsm_24h = rsm_avg/24;
        rsm_avg=0;
        SUBCASE("Horizontal_June") {
            response r_inst;
            response r_step3h;
            response r_step12h;
            calculator rad_inst(p);
            calculator rad_step1h(p);
            calculator rad_step3h(p);
            calculator rad_step12h(p);
            double slope = 0.0;
            double aspect = 0.0;
            double rastep_3h = 0.0;
            double rsstep_3h = 0.0;
            double rsostep_3h = 0.0;
            int counter = 0;
            for (int h = 0; h < 24; h+=3) {
                t1 = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                rad_step3h.net_radiation(r_step3h, eu_lat, t1,dt_step3h, slope, aspect, temperature, rhumidity, eu_elevation,rsm_3h[counter]);
                rastep_3h+=r_step3h.ra;
                rsstep_3h+=r_step3h.sw_cs_p;
                rsostep_3h+=r_step3h.sw_t;
                counter++;
            }
            CHECK_EQ(rastep_3h, doctest::Approx(eu_rahor_june).epsilon(0.1));
            CHECK_EQ(rsstep_3h, doctest::Approx(eu_rs_june).epsilon(0.1));
            CHECK_EQ(rsostep_3h/8, doctest::Approx(rsm_24h).epsilon(0.1));
            if (verbose){
                std::cout << "---------- June, automatic, 3h-step, hor ------- " << std::endl;
                std::cout << "ra: " << rastep_3h<<std::endl;
                std::cout << "rs (predicted): " << rsstep_3h<<std::endl;
                std::cout << "rso (translated): " << rsostep_3h<<std::endl;
                std::cout << "rsm 24h average: " << rsm_24h<<std::endl;
            }
            double rastep_12h = 0.0;
            double rsstep_12h = 0.0;
            double rsostep_12h = 0.0;
            counter = 0;
            for (int h = 0; h < 24; h+=12) {
                t1 = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                rad_step12h.net_radiation(r_step12h, eu_lat, t1,dt_step12h, slope, aspect, temperature, rhumidity, eu_elevation,rsm_12h[counter]);
                rastep_12h+=r_step12h.ra;
                rsstep_12h+=r_step12h.sw_cs_p;
                rsostep_12h+=r_step12h.sw_t;
                counter++;
            }
                    CHECK_EQ(rastep_12h, doctest::Approx(eu_rahor_june).epsilon(0.1));
                    CHECK_EQ(rsstep_12h, doctest::Approx(eu_rs_june).epsilon(0.1));
                    CHECK_EQ(rsostep_12h/2, doctest::Approx(rsm_24h).epsilon(0.1));
            if (verbose){
                std::cout << "---------- June, automatic, 12h-step, hor ------- " << std::endl;
                std::cout << "ra: " << rastep_12h<<std::endl;
                std::cout << "rs (predicted): " << rsstep_12h<<std::endl;
                std::cout << "rso (translated): " << rsostep_12h<<std::endl;
                std::cout << "rsm 24h: " << rsm_24h<<std::endl;
            }
            trapezoidal_average av_rahor;
            trapezoidal_average av_ra;
            trapezoidal_average av_rs;
            trapezoidal_average av_rso;
            rad_inst.net_radiation(r_inst, eu_lat, ta_june, dt_step1h, slope, aspect, temperature, rhumidity, eu_elevation,
                                       rsm[0]);
            av_rahor.initialize(rad_inst.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad_inst.ra_radiation(), 0.0);
            av_rs.initialize(r_inst.sw_cs_p, 0.0);
            av_rso.initialize(r_inst.sw_t, 0.0);
            for (int h = 1; h <= 23; ++h) {
                if (h<24){
                    t_inst = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                }
                else {t_inst = utc_cal.time(2002, 06, 22, 0, 00, 0, 0);} // becasue calendar doesn't allow 24 h

                rad_inst.net_radiation(r_inst, eu_lat, t_inst,dt_step1h, slope, aspect, temperature, rhumidity, eu_elevation,
                                           rsm[h]);
                av_rahor.add(rad_inst.ra_radiation_hor(), h);
                av_ra.add(rad_inst.ra_radiation(), h);
                av_rs.add(r_inst.sw_cs_p, h);
                av_rso.add(r_inst.sw_t, h);
                }
            CHECK_EQ(av_ra.result(), doctest::Approx(eu_rahor_june).epsilon(0.1));
            CHECK_EQ(av_rahor.result(), doctest::Approx(av_ra.result()).epsilon(0.1));
            CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs_june).epsilon(0.1));
            CHECK_EQ(av_rso.result(), doctest::Approx(rsm_24h).epsilon(0.1));
            if (verbose) {
                std::cout << "---------- June, automatic inst, hor ------- " << std::endl;
                std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                std::cout << "rso (translated): " << av_rso.result() << std::endl;
                std::cout << "rsm generated: " << rsm_24h << std::endl;
            }
                    CHECK_EQ(rastep_12h, doctest::Approx(rastep_3h).epsilon(0.1));
                    CHECK_EQ(rsstep_12h, doctest::Approx(rsstep_3h).epsilon(0.1));
                    CHECK_EQ(rastep_12h, doctest::Approx(av_ra.result()).epsilon(0.1));
                    CHECK_EQ(rsstep_12h, doctest::Approx(av_rs.result()).epsilon(0.1));
        }
        SUBCASE("January_45s") {
            response r_inst;
            response r_step3h;
            response r_step6h;
            response r_step12h;
            response r_step24h;
            calculator rad_inst(p);
            calculator rad_step1h(p);
            calculator rad_step3h(p);
            calculator rad_step6h(p);
            calculator rad_step12h(p);
            calculator rad_step24h(p);
            const double deg2rad = boost::math::constants::pi<double>()/180.0;
            double slope = 45.0*deg2rad; // the new method automatically convert radians to degrees
            double aspect = 180.0*deg2rad; // this conversion also is done inside new method: Generally, North = 0 deg aspect and South = 180 deg aspect,
            // but radiation method has different notation, so we do a conversion
            double rastep_3h = 0.0;
            double rsstep_3h = 0.0;
            double rsostep_3h = 0.0;
            int counter = 0;
            for (int h = 0; h < 24; h+=3) {
                t1 = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // January
                rad_step3h.net_radiation(r_step3h, eu_lat, t1,dt_step3h, slope, aspect, temperature, rhumidity, eu_elevation,rsm_3h[counter]);
                rastep_3h+=r_step3h.ra;
                rsstep_3h+=r_step3h.sw_cs_p;
                rsostep_3h+=r_step3h.sw_t;
                counter++;
            }
                    CHECK_EQ(rastep_3h, doctest::Approx(eu_ra45s_january).epsilon(0.1));
                    CHECK_EQ(rsstep_3h, doctest::Approx(eu_rs45s_january).epsilon(0.1));

            if (verbose){
                std::cout << "---------- January, automatic, 3h-step, 45s ------- " << std::endl;
                std::cout << "ra: " << rastep_3h<<std::endl;
                std::cout << "rs (predicted): " << rsstep_3h<<std::endl;
                std::cout << "rso (translated): " << rsostep_3h/8<<std::endl;
                std::cout << "rsm generated: " << rsm_24h<<std::endl;
            }
            double rastep_6h = 0.0;
            double rsstep_6h = 0.0;
            double rsostep_6h = 0.0;
            counter = 0;
            for (int h = 0; h < 24; h+=6) {
                t1 = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // January
                rad_step6h.net_radiation(r_step6h, eu_lat, t1,dt_step6h, slope, aspect, temperature, rhumidity, eu_elevation,rsm_6h[counter]);
                rastep_6h+=r_step6h.ra;
                rsstep_6h+=r_step6h.sw_cs_p;
                rsostep_6h+=r_step6h.sw_t;
                counter++;
            }
                    CHECK_EQ(rastep_6h, doctest::Approx(eu_ra45s_january).epsilon(0.1));
                    CHECK_EQ(rsstep_6h, doctest::Approx(eu_rs45s_january).epsilon(0.1));

            if (verbose){
                std::cout << "---------- January, automatic, 6h-step, 45s ------- " << std::endl;
                std::cout << "ra: " << rastep_6h<<std::endl;
                std::cout << "rs (predicted): " << rsstep_6h<<std::endl;
                std::cout << "rso (translated): " << rsostep_6h/4<<std::endl;
                std::cout << "rsm generated: " << rsm_24h<<std::endl;
            }
            double rastep_12h = 0.0;
            double rsstep_12h = 0.0;
            double rsostep_12h = 0.0;
            counter = 0;
            for (int h = 0; h < 24; h+=12) {
                t1 = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // January
                rad_step12h.net_radiation(r_step12h, eu_lat, t1,dt_step12h, slope, aspect, temperature, rhumidity, eu_elevation,rsm_12h[counter]);
                rastep_12h+=r_step12h.ra;
                rsstep_12h+=r_step12h.sw_cs_p;
                rsostep_12h+=r_step12h.sw_t;
                counter++;
            }
                    CHECK_EQ(rastep_12h, doctest::Approx(eu_ra45s_january).epsilon(0.1));
                    CHECK_EQ(rsstep_12h, doctest::Approx(eu_rs45s_january).epsilon(0.1));
            if (verbose){
                std::cout << "---------- January, automatic, 12h-step, 45s ------- " << std::endl;
                std::cout << "ra: " << rastep_12h<<std::endl;
                std::cout << "rs (predicted): " << rsstep_12h<<std::endl;
                std::cout << "rso (translated): " << rsostep_12h/2<<std::endl;
                std::cout << "rsm generated: " << rsm_24h<<std::endl;
            }
            double rastep_24h = 0.0;
            double rsstep_24h = 0.0;
            double rsostep_24h = 0.0;
            int h = 0;
            t1 = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // January
            rad_step24h.net_radiation(r_step24h, eu_lat, t1,dt_step24h, slope, aspect, temperature, rhumidity, eu_elevation,rsm_24h);
            rastep_24h+=r_step24h.ra;
            rsstep_24h+=r_step24h.sw_cs_p;
            rsostep_24h+=r_step24h.sw_t;
                    CHECK_EQ(rastep_24h, doctest::Approx(eu_ra45s_january).epsilon(0.1));
                    CHECK_EQ(rsstep_24h, doctest::Approx(eu_rs45s_january).epsilon(0.1));
            if (verbose){
                std::cout << "---------- January, automatic, 24h-step, 45s ------- " << std::endl;
                std::cout << "ra: " << rastep_24h<<std::endl;
                std::cout << "rs (predicted): " << rsstep_24h<<std::endl;
                std::cout << "rso (translated): " << rsostep_24h<<std::endl;
                std::cout << "rsm generated: " << rsm_24h<<std::endl;
            }
            trapezoidal_average av_rahor;
            trapezoidal_average av_ra;
            trapezoidal_average av_rs;
            trapezoidal_average av_rso;
            rad_inst.net_radiation(r_inst, eu_lat, ta_january, dt_step1h, slope, aspect, temperature, rhumidity, eu_elevation,
                                   rsm[0]);
            av_rahor.initialize(rad_inst.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad_inst.ra_radiation(), 0.0);
            av_rs.initialize(r_inst.sw_cs_p, 0.0);
            av_rso.initialize(r_inst.sw_t, 0.0);
            for (int h = 1; h <= 23; ++h) {
                if (h<24){
                    t_inst = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // January
                }
                else {t_inst = utc_cal.time(2002, 01, 2, 0, 00, 0, 0);} // becasue calendar doesn't allow 24 h

                rad_inst.net_radiation(r_inst, eu_lat, t_inst,dt_step1h, slope, aspect, temperature, rhumidity, eu_elevation,
                                       rsm[h]);
                av_rahor.add(rad_inst.ra_radiation_hor(), h);
                av_ra.add(rad_inst.ra_radiation(), h);
                av_rs.add(r_inst.sw_cs_p, h);
                av_rso.add(r_inst.sw_t, h);
            }
                    CHECK_EQ(av_ra.result(), doctest::Approx(eu_ra45s_january).epsilon(0.1));
                    CHECK_EQ(av_rs.result(), doctest::Approx(eu_rs45s_january).epsilon(0.1));
            if (verbose) {
                std::cout << "---------- January, automatic inst, 45s ------- " << std::endl;
                std::cout << "ra (extraterrestrial): " << av_ra.result() << std::endl;
                std::cout << "rs (predicted): " << av_rs.result() << std::endl;
                std::cout << "rso (translated): " << av_rso.result() << std::endl;
                std::cout << "rsm generated: " << rsm_24h << std::endl;
            }
            // all methods should output results within expected tolerance
                    CHECK_EQ(rastep_6h, doctest::Approx(rastep_3h).epsilon(0.1));
                    CHECK_EQ(rastep_12h, doctest::Approx(rastep_6h).epsilon(0.1));
                    CHECK_EQ(rastep_24h, doctest::Approx(rastep_12h).epsilon(0.1));
                    CHECK_EQ(rastep_24h, doctest::Approx(av_ra.result()).epsilon(0.1));

                    CHECK_EQ(rsstep_6h, doctest::Approx(rsstep_3h).epsilon(0.1));
                    CHECK_EQ(rsstep_12h, doctest::Approx(rsstep_6h).epsilon(0.1));
                    CHECK_EQ(rsstep_24h, doctest::Approx(rsstep_12h).epsilon(0.1));
                    CHECK_EQ(rsstep_24h, doctest::Approx(av_rs.result()).epsilon(0.1));

                    // This is not actually supposed to be equal,
                    // as while applying translation routine we might get some changes in coefficients related to inclinations
//                    CHECK_EQ(rsostep_6h/4, doctest::Approx(rsostep_3h/8).epsilon(0.1));
//                    CHECK_EQ(rsostep_12h/2, doctest::Approx(rsostep_6h/4).epsilon(0.1));
//                    CHECK_EQ(rsostep_24h, doctest::Approx(rsostep_12h/2).epsilon(0.1));
                    //CHECK_EQ(rsostep_24h, doctest::Approx(av_rso.result()).epsilon(0.2));
                    CHECK_EQ(rsostep_3h/8, doctest::Approx(av_rso.result()).epsilon(0.2));

        }
    }
////
} // end of Test Suit
