#include "test_pch.h"
#include <shyft/web_api/bg_work_result.h>

using namespace shyft::web_api;
TEST_SUITE("web_api_auth") {
    TEST_CASE("server_auth") {
        server_auth a;
        FAST_CHECK_EQ(a.needed(),false);
        FAST_CHECK_EQ(a.valid("any"),true);//because needed is false
        FAST_CHECK_EQ(a.tokens().size(),0u);
        a.add(vector<string>{"B1","B2","B2"});
        FAST_CHECK_EQ(a.tokens().size(),2u);
        FAST_CHECK_EQ(a.valid("not this time"),false);
        FAST_CHECK_EQ(a.valid("B1"),true);
        FAST_CHECK_EQ(a.valid("B2"),true);
        a.remove(vector<string>{"B1"});
        FAST_CHECK_EQ(a.valid("B1"),false);
        FAST_CHECK_EQ(a.valid("B2"),true);
        FAST_CHECK_EQ(a.tokens(),vector<string> {"B2"});
    }
}
