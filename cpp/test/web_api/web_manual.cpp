#include "test_pch.h"
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/adapt_adt.hpp>
#include <boost/spirit/include/support_adapt_adt_attributes.hpp>

#include <boost/spirit/include/karma.hpp>
#include <boost/iterator/iterator_facade.hpp>

#include <sstream>
#include <string_view>
#include <shyft/time/utctime_utilities.h>
#include <shyft/dtss/dtss.h>
#include <shyft/web_api/dtss_web_api.h>
#include <shyft/web_api/web_api_grammar.h>

static dlib::logger dlog("web_manual.log");


TEST_SUITE("web") {
TEST_CASE("web_manual") {
    // only to ease manual testing c++ server from python, running valgrind etc.
    if(getenv("SHYFT_WS_START")) {
        dlog<<dlib::LINFO<<"Starting beast ";
        std::string ipf_local_only("127.0.0.1");
        std::string if_any("0.0.0.0");
        std::string root=getenv("SHYFT_WS_START");
        auto doc_root=std::make_shared<std::string>(root);
        int threads=2;
        int port=8081;
        dlog<<dlib::LINFO<<"Attempting to start shyft web_api on "<< root<<" using port "<<port;
        shyft::web_api::dtss_server srv;
        srv.add_container("test",root+"/dtss");
        shyft::web_api::request_handler rh;rh.srv=&srv;
        shyft::web_api::start_web_server(rh, if_any,port,doc_root,threads,threads);
        dlog<<dlib::LINFO<<"Done with web-api server";
    }
}
}
