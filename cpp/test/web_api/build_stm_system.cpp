#include "build_stm_system.h"
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/market.h>

namespace test::web_api {
    using namespace  shyft::time_axis;
    using namespace shyft::time_series;
    
    stm_hps_ create_stm_hps(int id,string name) {
        using shyft::energy_market::hydro_power::connect;
        using shyft::energy_market::hydro_power::connection_role;
        /*
        Demonstrates how to build an in memory representation of  stm HydroPowerSystem
        using part of the blåsjø/ulla-førre systems
        */
        auto sorland = make_shared<stm_hps>(id,name);
        stm_hps_builder hps(sorland);
        auto blasjo = hps.create_reservoir(1,"blåsjø",string("json{max_vol:3200Mm3"));
        auto saurdal = hps.create_unit(2,"saurdal","json{max_eff:600MW}");
        auto tunx = hps.create_tunnel(12,"blåsjø-saurdal","json{}");
        connect(tunx)
            .input_from(blasjo)
            .output_to(saurdal);
        auto sandsavatn = hps.create_reservoir(3,"sandsvatn", "reservoir_data(560.0, 605.0, 230.0)");
        auto lauvastolsvatn = hps.create_reservoir(4,"lauvastølsvatn", "reservoir_data(590.0, 605.0, 8.3)");
        auto kvilldal1 = hps.create_unit(51,"kvilldal_1","{Ek:1.3,outlet_level_masl:70");
        auto kvilldal2 = hps.create_unit(52,"kvilldal_2","{Ek:1.3,outlet_level_masl:70");
        auto t_kvilldal = hps.create_tunnel(510,"kvilldal hovedtunnel", "{alpha:0.000053}");
        auto t_kvill_penstock_1 = hps.create_tunnel(5101,"kvilldal_1_penstock","json{}");
        auto t_kvill_penstock_2 = hps.create_tunnel(5102,"kvilldal_2_penstock","json{}");
        auto t_saur_kvill = hps.create_tunnel(6,"saurdal-kvilldal-hoved-tunnel","json{}");
        auto t_sandsa_kvill = hps.create_tunnel(7,"sandsavatn-til-kvilldal", "json{}");
        auto t_lauvas_kvill = hps.create_tunnel(8,"lauvastølsvatn-til-kvilldal", "json{}");
        auto g = hps.create_gate(1, "L1", "");
        shyft::energy_market::stm::waterway::add_gate(t_lauvas_kvill, g);
        g->discharge.schedule=apoint_ts("shyft://stm/rid/oid/aid");
        connect(saurdal).output_to(t_saur_kvill);
        connect(t_saur_kvill).output_to(t_kvilldal);
        connect(t_kvilldal).output_to(t_kvill_penstock_1);
        connect(t_kvill_penstock_1).output_to(kvilldal1);
        connect(t_kvilldal).output_to(t_kvill_penstock_2);
        connect(t_kvill_penstock_2).output_to(kvilldal2);

        connect(t_sandsa_kvill).input_from(sandsavatn).output_to(t_kvilldal);
        connect(t_lauvas_kvill).input_from(lauvastolsvatn).output_to(t_kvilldal);

        auto vassbotvatn = hps.create_reservoir(9,"vassbotvatn", "json{}");
        auto stoelsdal_pumpe = hps.create_unit(10,"stølsdal pumpe","json{}");
        auto above_vassbotvatn = hps.create_tunnel(11,"stølsdals kraftstasjon(pumpe) til vassbotvatn det pumpes fra", "json{}");
        connect(above_vassbotvatn)
            .input_from(stoelsdal_pumpe);
        connect(vassbotvatn).input_from(above_vassbotvatn);
        auto tun_sandsa_stolsdal = hps.create_tunnel(120, "fra sandsvatn til stølsdal pump", "json{}");
        connect(tun_sandsa_stolsdal).output_to(stoelsdal_pumpe);
        connect(sandsavatn).output_to(tun_sandsa_stolsdal, connection_role::main);

        auto suldalsvatn = hps.create_reservoir(13,"suldalsvatn","json{}");
        auto hylen = hps.create_unit(14,"hylen","json{}");

        connect(hps.create_river(15,"fra kvilldal til suldalsvatn", "json{}"))
            .input_from(kvilldal1)
            .input_from(kvilldal2)
            .output_to(suldalsvatn);

        connect(hps.create_tunnel(16,"hylen-tunnel", "json{}"))
            .input_from(suldalsvatn)
            .output_to(hylen);

        auto havet = hps.create_reservoir(17, "havet","json{}");

        connect(hps.create_river(18,"utløp hylen","json{}"))
            .input_from(hylen)
            .output_to(havet);

        connect(hps.create_river(19,"bypass suldal til havet","json{}"))
            .input_from(suldalsvatn, connection_role::bypass)
            .output_to(havet);

        connect(hps.create_river(20,"flom suldal til havet","json{}"))
            .input_from(suldalsvatn, connection_role::flood)
            .output_to(havet);
        return sorland;
    }

    
    stm_hps_ create_simple_hps(int id, string name) {
        using shyft::energy_market::hydro_power::connect;
        using shyft::energy_market::hydro_power::connection_role;

        auto simple = make_shared<stm_hps>(id,name);
        stm_hps_builder hps(simple);
        auto r = hps.create_reservoir(1,"simple_res", "");
        auto u = hps.create_unit(1, "simple_unit", "stuff");
        auto tun = dynamic_pointer_cast<waterway>(hps.create_tunnel(1, "r->u", ""));
        auto gt=hps.create_gate(1,"gt1","");
        waterway::add_gate(tun,gt);
        connect(tun).input_from(r)
                    .output_to(u);
        auto pp = hps.create_power_plant(2,"simple_pp", "");
        pp->add_unit(pp,u);

        //We'll also dress the model up with some time-series:
        utctimespan dt{deltahours(1)};
        fixed_dt ta{_t(0),dt,6};
        constexpr ts_point_fx linear{POINT_INSTANT_VALUE},stair_case{POINT_AVERAGE_VALUE};
        // Setting some attributes with a dstm:// url
        r->inflow.schedule          = apoint_ts(ta, vector<double>{1.0, 2.0, shyft::nan, 4.0, 3.0, 6.0}, linear);
        gt->discharge.result        = apoint_ts(ta,vector<double>{20.0,21.0,22.0,23.0,24.0,19.0},stair_case); 
        r->level.result             = apoint_ts(ta, 1.0, linear);
        u->discharge.constraint.min = apoint_ts(ta, 0.87, linear);
        u->production.result        = apoint_ts(ta, vector<double>{1.0, 1.2, 1.3, 1.4, 1.5, 1.6}, linear);
        tun->discharge.result       = apoint_ts(ta, 5.0, linear);

        return simple;
    }

    stm_system_ create_stm_system(int id, string name, string json) {
        auto mdl = make_shared<stm_system>(id, name, json);
        mdl->hps.push_back(create_stm_hps());
        return mdl;
    }

    stm_system_ create_simple_system(int id, string name) {
        auto mdl = make_shared<stm_system>(id, name, "");
        mdl->hps.push_back(create_simple_hps());
        return mdl;
    }
}

