#pragma once
#include <shyft/web_api/targetver.h>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>
#include <csignal>

#include <shyft/dtss/dtss.h>
#include <shyft/web_api/dtss_web_api.h>
#include <test/test_utils.h>



namespace test {

using std::vector;
using std::string;
using std::string_view;

using namespace shyft::dtss;
using namespace shyft::core;
using namespace shyft;

/** test server for web_api */
struct test_server:server {
    web_api::request_handler bg_server;///< handle web-api requests
    mutable std::recursive_mutex x_srv;//protect the below stuff.
    std::future<int> web_srv;///< mutex,

    test_server();
    ~test_server();
    int start_web_api(string host_ip,int port,string doc_root,int fg_threads,int bg_threads);
    bool web_api_running() const;
    void stop_web_api() ;

    //-- when needed, we can impl. callbacks to deal with non shyft:// urls
    ts_vector_t test_read_cb(vector<string> const&,utcperiod const&);
    vector<ts_info> test_find_cb(string );
    void test_store_cb(ts_vector_t const& );
};


}
