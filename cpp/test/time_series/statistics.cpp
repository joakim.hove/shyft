#include "test_pch.h"

#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/statistics_ts.h>
#include <shyft/time_series/fx_statistics.h>

#include <shyft/time_series/dd/ats_vector.h>
#include <vector>
#include <math.h>

using std::make_shared;

namespace shyft::time_series {
    
template <class TS,class TA> 
point_ts<TA> ts_percentile(TS const & ts, TA const& ta,int64_t p){
    return point_ts<TA>(ta,ts_percentile_values(ts,ta,p),ts.point_interpretation());
}

}



TEST_SUITE("time_series") {
    using namespace shyft::core;
    using namespace shyft::time_series;
    using ta_t=shyft::time_axis::generic_dt;
    using ts_t=point_ts<ta_t>;
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::statistics_ts;
    using shyft::nan;
    TEST_CASE("ts_statistics") {
        ta_t ta{utctime(0),utctime(10),10};
        ts_t ts{ta,vector<double>{1,2,3,4,5,6,7,8,9,10},ts_point_fx::POINT_AVERAGE_VALUE};
        auto r=ts_percentile(ts,ta,50);
        FAST_CHECK_EQ(r,ts);
        FAST_CHECK_EQ(ts,ts_percentile(ts,ta,statistics_property::AVERAGE));
        FAST_CHECK_EQ(ts,ts_percentile(ts,ta,statistics_property::MIN_EXTREME));
        FAST_CHECK_EQ(ts,ts_percentile(ts,ta,statistics_property::MAX_EXTREME));
        ta_t tb{ta.time(0),ta.total_period().timespan(),1};
        auto rb=ts_percentile(ts,tb,statistics_property::AVERAGE);
        FAST_CHECK_EQ(rb.time_axis(),tb);
        FAST_CHECK_EQ(rb.value(0),doctest::Approx(5.5));
        ts.set(1,nan);
        auto rc=ts_percentile(ts,ta,50);
        FAST_CHECK_EQ(rc,ts);
        SUBCASE("resable_partial_with_larger_dt") {
            ta_t tx(utctime(10),utctime(20),2);
            auto rx=ts_percentile(ts,tx,statistics_property::AVERAGE);
            auto expected_rx=ts_t{tx,vector<double>{3,4.5},ts_point_fx::POINT_AVERAGE_VALUE};
            FAST_CHECK_EQ(rx,expected_rx);
        }
        SUBCASE("resample_partial_with_smaller_dt") {
            ta_t tf(utctime(0),utctime(2),11);// 0.. 12
            for(auto p: vector<int64_t>{statistics_property::AVERAGE,statistics_property::MAX_EXTREME,statistics_property::MIN_EXTREME,20}) {
                auto rf=ts_percentile(ts,tf,p);
                //                0  1   2   3  4   5   6   7   8   9  10
                //                0  2   4   6  8  10  12  14  16  18  20 22
                vector<double> ef{1,nan,nan,nan,nan,nan,nan,nan,nan,nan,3};
                FAST_CHECK_UNARY(nan_equal(rf.values(),ef));
            }
        }
        SUBCASE("resample_with_larger_total_period_P50") {
            ta_t tx(utctime(-20),utctime(20),7);
            auto rx=ts_percentile(ts,tx,50);
            vector<double>ex{nan,1,3.5,5.5,7.5,9.5,nan};
            // for(size_t i=0;i<rx.size();++i)  std::cout<<i<<":"<< ex[i]<<" vs " << rx.value(i)<<"\n";
            FAST_CHECK_UNARY(nan_equal(rx.values(),ex));
        }
        SUBCASE("resample_with_larger_total_period_AVG") {
            ta_t tx(utctime(-20),utctime(20),7);
            auto rx=ts_percentile(ts,tx,statistics_property::AVERAGE);
            vector<double>ex{nan,1,3.5,5.5,7.5,9.5,nan};
            // for(size_t i=0;i<rx.size();++i)  std::cout<<i<<":"<< ex[i]<<" vs " << rx.value(i)<<"\n";
            FAST_CHECK_UNARY(nan_equal(rx.values(),ex));
        }
        SUBCASE("resample_with_larger_total_period_MIN") {
            ta_t tx(utctime(-20),utctime(20),7);
            auto rx=ts_percentile(ts,tx,statistics_property::MIN_EXTREME);
            vector<double>ex{nan,1,3,5,7,9,nan};
            // for(size_t i=0;i<rx.size();++i)  std::cout<<i<<":"<< ex[i]<<" vs " << rx.value(i)<<"\n";
            FAST_CHECK_UNARY(nan_equal(rx.values(),ex));
        }
        SUBCASE("resample_with_larger_total_period_MAX") {
            ta_t tx(utctime(-20),utctime(20),7);
            auto rx=ts_percentile(ts,tx,statistics_property::MAX_EXTREME);
            vector<double>ex{nan,1,4,6,8,10,nan};
            //for(size_t i=0;i<rx.size();++i)  std::cout<<i<<":"<< ex[i]<<" vs " << rx.value(i)<<"\n";
            FAST_CHECK_UNARY(nan_equal(rx.values(),ex));
        }
        SUBCASE("resample_with_non_disjoint_period_before") {
            ta_t tx(utctime(-20),utctime(20),1);
            auto rx=ts_percentile(ts,tx,50);
            vector<double>ex{nan};
            // for(size_t i=0;i<rx.size();++i)  std::cout<<i<<":"<< ex[i]<<" vs " << rx.value(i)<<"\n";
            FAST_CHECK_UNARY(nan_equal(rx.values(),ex));
        }
        SUBCASE("resample_with_non_disjoint_period_after") {
            ta_t tx(utctime(100),utctime(20),1);
            auto rx=ts_percentile(ts,tx,50);
            vector<double>ex{nan};
            // for(size_t i=0;i<rx.size();++i)  std::cout<<i<<":"<< ex[i]<<" vs " << rx.value(i)<<"\n";
            FAST_CHECK_UNARY(nan_equal(rx.values(),ex));
        }
        SUBCASE("ts_percentile_invalid percentile") {
            ta_t tx(utctime(100),utctime(20),1);
            auto rx=ts_percentile(ts,tx,50);
            CHECK_THROWS_AS(ts_percentile(ts,tx,-2),runtime_error);
        }
        
    }
    TEST_CASE("calculate_percentile_excel_method_pre_sorted") {
        vector<double> v{1,2,3,4,5,6,7,8,9,10};
        vector<int64_t> p{0,35,55,100};
        vector<double> e{1.0,4.15,5.95,10};
        for(size_t i=0;i<p.size();++i)
            FAST_CHECK_EQ(calculate_percentile_excel_method_pre_sorted(v,p[i]),doctest::Approx(e[i]));
        
    }

    
    TEST_CASE("ts_statistics_perf") {
        size_t n=1000000;
        std::srand(to_seconds64(utctime_now())); // use current time as seed for random generator
        ta_t ta{utctime(0),utctime(10),n};
        vector<double> v;v.reserve(ta.size());
        for(size_t i=0;i<ta.size();++i) {
            v.push_back(std::rand());
        }
        ts_t ts{ta,v,ts_point_fx::POINT_AVERAGE_VALUE};
        size_t nm=n/10;
        ta_t ta_stat{utctime(0),utctime(10*nm),n/nm};
        for(auto p:vector<int64_t>{statistics_property::MIN_EXTREME,statistics_property::AVERAGE,50}) {
            auto t0=utctime_now();
            auto r=ts_percentile_values(ts,ta_stat,p);
            auto t1=utctime_now();
            auto t_used=to_seconds(t1-t0);
            MESSAGE("ts_statistics perf "<<p<<" using "<<n<<" src points reduced to "<<r.size()<<" points, used  "<<t_used<<"s ->"<<n/t_used/1e5<<" mill pts/s");
            FAST_CHECK_EQ(r.size(),ta_stat.size());
        }
    }
    TEST_CASE("ts_statistics_basic") {
        ta_t ta{utctime(0),utctime(10),10};
        ta_t tx{utctime(0),utctime(20),5};
        //ts_t ts{ta,vector<double>{1,2,3,4,5,6,7,8,9,10},ts_point_fx::POINT_AVERAGE_VALUE};
        apoint_ts ts{ta,vector<double>{1,2,3,4,5,6,7,8,9,10},ts_point_fx::POINT_AVERAGE_VALUE};
        apoint_ts r(make_shared<const statistics_ts>(ts,tx,50));
        auto rv=r.values();
        vector<double> ev{1.5,3.5,5.5,7.5,9.5};
        FAST_CHECK_UNARY(nan_equal(rv,ev));
        FAST_CHECK_EQ(r.time_axis(),tx);
        FAST_CHECK_EQ(r.value(0),doctest::Approx(ev[0]));
        FAST_CHECK_EQ(r(utctime(45)), doctest::Approx(ev[2]) );
        FAST_CHECK_UNARY(!std::isfinite(r(utctime(-10))));
        FAST_CHECK_UNARY(!std::isfinite(r(utctime(10000))));
        FAST_CHECK_UNARY(!std::isfinite(r.value(1000)));
        FAST_CHECK_EQ(r.point_interpretation(),ts_point_fx::POINT_AVERAGE_VALUE);
        FAST_CHECK_EQ(r.time_axis(),tx);
        r.set_point_interpretation(ts_point_fx::POINT_INSTANT_VALUE);
        FAST_CHECK_EQ(r.point_interpretation(),ts_point_fx::POINT_INSTANT_VALUE);
        FAST_CHECK_EQ(r.time(0),tx.time(0));
        FAST_CHECK_EQ(r.index_of(tx.time(0)),0);
        FAST_CHECK_EQ(r.total_period(),tx.total_period());

    }

    TEST_CASE("fx_extract_statistics") {
        ta_t ta{utctime(0),seconds(10),10};
        ts_t ts{ta,vector<double>{1,2,3,4,5,6,7,8,9,10},ts_point_fx::POINT_AVERAGE_VALUE};
        constexpr auto tap=[](utcperiod p){return ta_t{p.start,p.timespan(),1};};//make ta from period with one step
        auto v1=extract_statistics(ts,tap(utcperiod{ta.time(0)+seconds(5),ta.time(4)}),nan_min);
        auto v2=extract_statistics(ts,tap(utcperiod{ta.time(1)+seconds(5),ta.time(4)}),nan_max);
        FAST_CHECK_EQ(v1[0],doctest::Approx(2.0));// 2.0 is the first sample within the interval, 1.0 is to the left
        FAST_CHECK_EQ(v2[0],doctest::Approx(4.0));

    }
}

TEST_SUITE("tsv_value_range") {
    using namespace shyft::core;
    using namespace shyft::time_series;

    using ta_t=shyft::time_axis::generic_dt;
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::statistics_ts;
    using shyft::time_series::dd::ats_vector;
    using shyft::nan;

    TEST_CASE("tsv_empty") {
        ats_vector tsv{vector<apoint_ts>{}};
        utcperiod p{utctime(0), utctime(10)};

        auto value_range = tsv.value_range(p);
        FAST_CHECK_EQ(isnan(value_range[0]), true);
        FAST_CHECK_EQ(isnan(value_range[1]), true);
    }
    TEST_CASE("tsv_1_empty") {
        ta_t ta{utctime(0), utctime(3), 3};
        apoint_ts ts{ta, vector<double>{1., 5., 9.}, ts_point_fx::POINT_AVERAGE_VALUE};
        apoint_ts ts_empty{};
        apoint_ts ts2{ta, vector<double>{1., 5., 10.}, ts_point_fx::POINT_AVERAGE_VALUE};
        ats_vector tsv2{ts, ts_empty, ts2};
        utcperiod p{utctime(0), utctime(10)};
        auto value_range = tsv2.value_range(p);
        FAST_CHECK_EQ(value_range[0], doctest::Approx(1.));
        FAST_CHECK_EQ(value_range[1], doctest::Approx(10.));
    }

    TEST_CASE("invalid_period") {
        ats_vector tsv{vector<apoint_ts>{}};
        utcperiod p{utctime(10), utctime(0)};
        CHECK_THROWS_AS(tsv.value_range(p), std::runtime_error);
    }


    TEST_CASE("basic_1_point") {
        ta_t ta{utctime(0), utctime(10), 1};
        utcperiod p{utctime(0), utctime(10)};

        apoint_ts ts{ta, vector<double>{5.5}, ts_point_fx::POINT_AVERAGE_VALUE};
        ats_vector tsv{ts};

        auto value_range = tsv.value_range(p);
        FAST_CHECK_EQ(value_range[0], doctest::Approx(5.5));
        FAST_CHECK_EQ(value_range[1], doctest::Approx(5.5));
        }
    TEST_CASE("basic_1_point_outside_range") {
        ta_t ta{utctime(0), utctime(10), 1};
        utcperiod p{utctime(20), utctime(30)};

        apoint_ts ts{ta, vector<double>{5.5}, ts_point_fx::POINT_AVERAGE_VALUE};
        ats_vector tsv{ts};

        auto value_range = tsv.value_range(p);
        FAST_CHECK_EQ(isnan(value_range[0]), true);
        FAST_CHECK_EQ(isnan(value_range[1]), true);
    }
    TEST_CASE("basic_1_point_only_nan_input") {
        ta_t ta{utctime(0), utctime(10), 1};
        utcperiod p{utctime(0), utctime(10)};

        apoint_ts ts{ta, vector<double>{nan}, ts_point_fx::POINT_AVERAGE_VALUE};
        ats_vector tsv{ts};

        auto value_range = tsv.value_range(p);
        FAST_CHECK_EQ(isnan(value_range[0]), true);
        FAST_CHECK_EQ(isnan(value_range[1]), true);
    }

    ta_t ta{utctime(0), utctime(10), 5};
    apoint_ts ts{ta, vector<double>{1, 2, 3, 4, nan}, ts_point_fx::POINT_AVERAGE_VALUE};

    TEST_CASE("tsv1val") {
        utcperiod p{utctime(10), utctime(50)};

        ats_vector tsv{ts};

        auto value_range = tsv.value_range(p);
        CHECK(value_range[0] == doctest::Approx( 2.));
        CHECK(value_range[1] == doctest::Approx( 4. ));
    }
    TEST_CASE("tsv_1_entry_period_start_shifted_by_dt_halv") {
        ta_t ta{utctime(0), utctime(10), 5};
        utcperiod p{utctime(15), utctime(50)};

        apoint_ts ts{ta, vector<double>{1, 2, 3, 4, nan}, ts_point_fx::POINT_AVERAGE_VALUE};
        ats_vector tsv{ts};

        auto value_range = tsv.value_range(p);
        CHECK(value_range[0]==doctest::Approx( 2.));
        CHECK(value_range[1]==doctest::Approx( 4.));
        auto vr=tsv.value_range({utctime{-10},utctime{4}});
        CHECK(vr[0]==doctest::Approx(1.0));
        CHECK(vr[1]==doctest::Approx(1.0));
        vr=tsv.value_range({utctime{-10},utctime{-1}}); //entirely before
        CHECK(isnan(vr[0]));
        CHECK(isnan(vr[1]));
        vr=tsv.value_range({utctime{45},utctime{60}}); //overlap out at end
        CHECK(isnan(vr[0]));
        CHECK(isnan(vr[1]));
        vr=tsv.value_range({utctime{30},utctime{60}});//picks up the 4.0 value
        CHECK(vr[0]==doctest::Approx(4.0));
        CHECK(vr[1]==doctest::Approx(4.0));
    }
    TEST_CASE("tsv_with_several_entries") {
        ta_t ta2{utctime(0), utctime(20), 2};
        apoint_ts ts2{ta2, vector<double>{1, 6}, ts_point_fx::POINT_AVERAGE_VALUE};
        ats_vector tsv{ts,ts2};

        utcperiod p{utctime(25), utctime(50)};
        auto value_range = tsv.value_range(p);
        CHECK(value_range[0] == doctest::Approx( 3.));
        CHECK(value_range[1] == doctest::Approx( 6.));
    }

    TEST_CASE("tsv_contains_unbound") {
        auto ts_unbound = apoint_ts("test");
        ats_vector tsv{ts, ts_unbound};
        utcperiod p{utctime(25), utctime(50)};
        CHECK_THROWS_AS(tsv.value_range(p), std::runtime_error);
    }


}
