#include "test_pch.h"
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/anary_op_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/fx_merge.h>
#include <shyft/time_series/bin_op_eval.h>
#include <shyft/time_series/dd/compute_ts_vector.h>

using std::make_shared;
using std::vector;
using namespace shyft;
using namespace shyft::core;
using namespace shyft::time_series;
using namespace shyft::time_series::dd;
using std::plus;

namespace nary_test {
    
    template<class TS>
    static vector<TS> make_ts_vector_of_type(size_t n, utctime t0, utctimespan fc_interval, utctimespan arome_dt, size_t fc_size, ts_point_fx pfx=ts_point_fx::POINT_AVERAGE_VALUE) {
        vector<TS> r;
        for(size_t i = 0; i < n; ++i) {
            r.emplace_back(gta_t(t0+i*fc_interval, arome_dt, fc_size), vector<double>(fc_size, static_cast<double>(i)), pfx);
        }
        return r;
    }
    vector<apoint_ts> make_arome_forecast(size_t n, utctime t0, utctimespan fc_interval, utctimespan arome_dt, size_t fc_size) {
        return make_ts_vector_of_type<apoint_ts>(n,t0,fc_interval,arome_dt,fc_size);
    }
    
}


TEST_SUITE("ts_nary") {
    TEST_CASE("nary_ts_basic") {
        
        utctimespan fc_interval = seconds(3600)*6;
        anary_op_ts a{
            nary_test::make_arome_forecast(6, utctime(0), fc_interval, seconds(3600), 36),
            nary_op_t::OP_MERGE,
            seconds(0), // Merge from start of forecast
            fc_interval // Merge until start of next forecast
        };
        CHECK_EQ(false, a.needs_bind()); // Verify that the time axis has been built

        CHECK_EQ(a.time_axis().size(), 5*6 + 36);

        const std::vector<double>& v = a.values();
        CHECK_EQ(v.size(), 5*6 + 36);
    }
    
    TEST_CASE("nary_ts_unbound") {
        auto arome_forecasts = nary_test::make_arome_forecast(6, utctime(0), seconds(3600)*6, seconds(3600), 36);
        
        // Replace first forecast with symbolic ts
        apoint_ts ats("shyft://container/x");
        arome_forecasts[0] = ats.average(gta_t{utctime(0), deltahours(1), 36});
        CHECK_UNARY(ats.needs_bind());
        
        anary_op_ts a{arome_forecasts, nary_op_t::OP_MERGE};
        CHECK_UNARY(a.needs_bind());

        std::vector<ts_bind_info> ts_bi;
        a.find_ts_bind_info(ts_bi);
        CHECK_EQ(ts_bi.size(), 1);
        
        ts_bi[0].ts.bind(arome_forecasts[1]); // Attach some data to unbound element
        a.do_bind(); // Resolve all binds, compute time axis
        CHECK_EQ(a.needs_bind(), false);
    }
    
    TEST_CASE("nary_ts_performance_single") {

        // One year forecast for a single variable at a single point
        auto fc = nary_test::make_arome_forecast(180, utctime(0), seconds(3600)*6, seconds(3600), 36);
        
        eval_ctx c;
        
        auto t0 = timing::now();
        anary_op_ts a{fc, nary_op_t::OP_MERGE};
        a.prepare(c);
        a.evaluate(c, nullptr);
        auto t1 = timing::now();
        MESSAGE( "nary_ts benchmark single merge " << double(elapsed_us(t0, t1)) << " us");
        
        CHECK_EQ(a.values().size(), 179*6 + 36);
    }
    
    TEST_CASE("nary_ts_performance_multi") {

        const size_t n = 1000*4; // 1000 points for 4 variables
        std::vector<std::vector<apoint_ts>> forecasts;
        forecasts.reserve(n);
        // Make n half-year forecasts
        for(size_t i = 0; i < n; ++i) forecasts.emplace_back(nary_test::make_arome_forecast(180, utctime(0), seconds(3600)*6, seconds(3600), 36));

        auto t0 = timing::now();

        vector<apoint_ts> e;
        for(size_t i = 0; i < n; ++i) {
            e.emplace_back(make_shared<const anary_op_ts>(forecasts[i], nary_op_t::OP_MERGE));
        }

        auto t1 = timing::now();
        auto r = deflate_ts_vector<gts_t>(std::move(e));
        auto t2 = timing::now();

        MESSAGE("nary_ts benchmark multiple tax " << double(elapsed_ms(t0, t1)) << " ms" );
        MESSAGE("nary_ts benchmark multiple values " << double(elapsed_ms(t1, t2)) << " ms" );
        MESSAGE("nary_ts benchmark multiple total " << double(elapsed_ms(t0, t2)) << " ms" );
    }
    
    TEST_CASE("test_forecast_merge") {
        ats_vector forecasts(nary_test::make_arome_forecast(6, utctime(0), seconds(3600)*6, seconds(3600), 36));
        auto merged = forecasts.forecast_merge(utctime(0), seconds(3600)*6);
        TS_ASSERT_EQUALS(merged.size(), 5*6 + 36);
    }
    
    TEST_CASE("test_nary_add") {
        const size_t n_ts = 10; // 100 tim
        const size_t n=100;
        const size_t npts=24*365*3;
        std::vector<ats_vector> forecasts;
        forecasts.reserve(n);
        // Make n half-year forecasts
        for(size_t i = 0; i < n_ts; ++i)
            forecasts.emplace_back(nary_test::make_arome_forecast(n, utctime(0), seconds(0), seconds(3600), npts));
        auto t0 = timing::now();

        vector<apoint_ts> e;
        for(size_t i = 0; i < n_ts; ++i) {
            e.emplace_back(forecasts[i].sum());
        }
        auto t1 = timing::now();
        auto r = deflate_ts_vector<gts_t>(std::move(e));
        auto t2 = timing::now();
        auto w=nary_test::make_ts_vector_of_type<apoint_ts>(n_ts, seconds(0),seconds(0), seconds(3601), npts,ts_point_fx::POINT_AVERAGE_VALUE);
        vector<apoint_ts> f;
        std::vector<ats_vector> mixed_forecasts;
        for(size_t i=0;i<n_ts;++i) {
            mixed_forecasts.emplace_back(nary_test::make_arome_forecast(n, utctime(0), seconds(0), seconds(3600), npts));
            mixed_forecasts[i].insert(begin(mixed_forecasts[i]),w[i]);// make it difficult, no time-axis will match.
            f.emplace_back(mixed_forecasts[i].sum());
        }
        auto t3=timing::now();
        auto q = deflate_ts_vector<gts_t>(std::move(f));
        auto t4 = timing::now();
        
        MESSAGE("n_ts"<<n_ts<<" xn="<<n<<" x npts= "<< npts );
        MESSAGE("nary_ts benchmark multiple tax " << double(elapsed_ms(t0, t1)) << " ms" );
        MESSAGE("nary_ts benchmark multiple values " << double(elapsed_ms(t1, t2)) << " ms" );
        MESSAGE("nary_ts benchmark multiple total " << double(elapsed_ms(t0, t2)) << " ms");
        MESSAGE("nary_ts benchmark multiple total " << (n_ts*n*npts)/double(elapsed_ms(t0, t2))/1000.0 << " mill pts/s");
        MESSAGE("nary_ts benchmark worstcase "<<double(elapsed_ms(t3, t4)) << " ms" );
        MESSAGE("nary_ts benchmark worstcase total " << ((n_ts+1)*n*npts)/double(elapsed_ms(t3, t4))/1000.0 << " mill pts/s");
    }
    TEST_CASE("test_nary_add_expression") {
        const size_t n=20;
        const size_t npts=24;
        utctime t0{seconds{0}};
        utctimespan dt{seconds{10}};
        ats_vector fc;
        for(size_t i=0;i<n;++i)
            fc.emplace_back(2.0*apoint_ts(gta_t{t0,dt,npts},double(1.0),ts_point_fx::POINT_AVERAGE_VALUE));
        auto expr=fc.sum();
        auto r = expr.evaluate();
        for(size_t i=0;i<r.size();++i)
            FAST_CHECK_EQ(r.value(i),doctest::Approx(2.0*double(n)));
    }

    TEST_CASE("new_bin_op_speed") {
        const size_t n_ts=100;
        const size_t n_pts=24*365*3;
        const auto pfx=ts_point_fx::POINT_AVERAGE_VALUE;
        auto v=nary_test::make_ts_vector_of_type<point_ts<gta_t>>(n_ts, seconds(0),seconds(0), seconds(3600), n_pts,pfx);
        auto w=nary_test::make_ts_vector_of_type<point_ts<gta_t>>(1, seconds(0),seconds(0), seconds(3601), n_pts,pfx);
        auto t0=utctime_now();
        auto r=w[0];
        for(size_t i=1;i<v.size();++i) {
            r=bin_op_eval<point_ts<gta_t>>(r,plus<double>(),v[i]);
        }
        auto t1=utctime_now();
        auto t_used=to_seconds(t1-t0);
        MESSAGE("new: n_ts"<<n_ts<<" x npts= "<< n_pts <<" : "<<t_used<<"perf:"<< (n_ts*n_pts)/t_used/1e6<<" m pts/s");        
    }
    TEST_CASE("old_bin_op_speed") {
        const size_t n_ts=100;
        const size_t n_pts=24*365*3;
        const auto pfx=ts_point_fx::POINT_AVERAGE_VALUE;
        auto av=nary_test::make_ts_vector_of_type<apoint_ts>(n_ts, seconds(0),seconds(0), seconds(3600), n_pts,pfx);
        auto aw=nary_test::make_ts_vector_of_type<apoint_ts>(1, seconds(0),seconds(0), seconds(3601), n_pts,pfx);
        auto t0=utctime_now();
        apoint_ts av_sum=aw[0];
        for(size_t i=0;i<av.size();++i) {
            av_sum = av_sum + av[i];
        }
        auto av_sum_evaluated=av_sum.evaluate();
        auto t1=utctime_now();
        auto t_used=to_seconds(t1-t0);
        MESSAGE("old: n_ts"<<n_ts<<" x npts= "<< n_pts <<" : "<<t_used<<"perf:"<< (n_ts*n_pts)/t_used/1e6<<" m pts/s");
    }

}
