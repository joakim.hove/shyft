#include "test_pch.h"
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/repeat_ts.h>
using std::make_shared;
namespace test::play_ground  {
    using namespace shyft::time_axis;
    using namespace shyft::core;
    using std::vector;
#if 0
    // play_ground algorithms for fast iterations, 
    /** return the i'th value of repeated time-series, using callable Fi to get out the value once index is computed */
    template <class Fi>
    inline double compute_ts_value(size_t i, generic_dt const& ta,generic_dt const &r , generic_dt const &src, Fi&& fi) {
            auto t=ta.time(i);
            auto t0 = r.gt==generic_dt::CALENDAR? r.c.cal->trim(src.time(0),r.c.dt):utctime_floor(src.time(0),r.f.dt); // fast compute, invariant.
            auto rp=r.index_of(t);//repeat period index, fast computed,since r is calendar_dt or fixed_dt
            auto tb=r.time(rp);// start of repeat period., fast computed, ... 
            auto t_src= t0+ (t-tb);//offset into repeat-pattern.
            auto src_ix=src.index_of(t_src); // will return n_pos for t_src < src.t0, -kind of ok. and also npos for t_src > src.end, which is also ok, since we plan nan-for both.
            return src_ix!=std::string::npos?fi(src_ix):shyft::nan;
    }
    
    /** return the value at time t, possibly interpolated from neighbour points if linear type */
    template <class Fi>
    inline double compute_ts_value(utctime t, generic_dt const& ta,generic_dt const &r , generic_dt const &src, bool linear, Fi&& fi) {
            // looks repeated, but we need t0,rp,tb
            auto t0 = r.gt==generic_dt::CALENDAR? r.c.cal->trim(src.time(0),r.c.dt):utctime_floor(src.time(0),r.f.dt); // fast compute, invariant.
            auto rp=r.index_of(t);//repeat period index, fast computed,since r is calendar_dt or fixed_dt
            auto tb=r.time(rp);// start of repeat period., fast computed, ... 
            auto t_src= t0+ (t-tb);//offset into repeat-pattern.
            auto src_ix= src.index_of(t_src); // will return n_pos for t_src < src.t0, -kind of ok. and also npos for t_src > src.end, which is also ok, since we plan nan-for both.
            auto v1 = src_ix!=std::string::npos?fi(src_ix):shyft::nan;
            if(!linear) return v1;
            if(!isfinite(v1)) return v1;// nan because lhs is nan
            // get next point of src.. hmm. could wrap.
            if(src_ix+1 < src.size()) {// we are lucky, just interpolated to next point
                auto v2= fi(src_ix+1);
                auto t2 = src.time(src_ix+1);
                auto t1= src.time(src_ix);
                return v1 + to_seconds(t_src-t1)*(v2-v1)/to_seconds(t2-t1);//interpolate
            } else { // we are at the end of src ts. If total period ends before next period, this is a nan, otherwise, wrap around take first value.
                if( tb + (src.total_period().end - t0 ) >= r.period(rp).end ) { // the repeat ts do have enough data for period,  try to wrap to first value.hmm.
                    if(src.time(0)== t0) {
                        auto v2= fi(0);// then interpolate. but what if there is a gap between t0 and the first value of the src ts. (always wrap,or always nan..)
                        auto t2 =t0+(r.period(rp).end - tb);// yes, this point exactly
                        auto t1= src.time(src_ix);
                        return v1 + to_seconds(t_src-t1)*(v2-v1)/to_seconds(t2-t1);//interpolate
                    } // else wrap around into a nan-area.
                }
                return shyft::nan;
            }
                
    }
#endif
    // ease test-cases
    template <class N>
    inline utctime _t(N x) {return from_seconds(x);} // short-hand
    inline utctime _t(const char*x) {return create_from_iso8601_string(x);}

}



using namespace shyft::core;
using namespace shyft::time_axis;
using namespace test::play_ground;
using shyft::time_series::dd::apoint_ts;
using namespace shyft::time_series;

TEST_SUITE("time_axis") {
    
    TEST_CASE("repeat_none_and_exceptions") {
        generic_dt e{};
        generic_dt p{vector<utctime>{_t(0),_t(1)},_t(2)};
        generic_dt a(_t(0),_t(10),10);
        auto ta1=repeat_time_axis(e,a);
        auto ta2=repeat_time_axis(a,e);
        CHECK_EQ(ta1.size(),0u);
        CHECK_EQ(ta2.size(),0u);
        CHECK_THROWS_AS(repeat_time_axis(a,p),std::runtime_error);
    }
    
    TEST_CASE("repeat_fixed_dt") {
        generic_dt src(_t(10),_t(1),10);
        generic_dt rep(_t(0),_t(10),10);
        auto ta= repeat_time_axis(src,rep);
        REQUIRE_EQ(ta.size(),10*10u);
        CHECK_EQ(ta.total_period(),rep.total_period());
        for(size_t i=0;i<ta.size();++i) {
            CHECK_EQ(ta.time(i),_t(i));
        }
    }
    TEST_CASE("repeat_fixed_dt_pattern_any_where") {
        generic_dt src(_t(10000),_t(1),10);// pattern is outside the repeat period time-axis, should also work ok
        generic_dt rep(_t(0),_t(10),10);
        auto ta= repeat_time_axis(src,rep);
        REQUIRE_EQ(ta.size(),10*10u);
        CHECK_EQ(ta.total_period(),rep.total_period());
        for(size_t i=0;i<ta.size();++i) {
            CHECK_EQ(ta.time(i),_t(i));
        }
    }
    
    TEST_CASE("repeat_calendar_365_dt") {
        generic_dt src(_t("2015-01-01T00:00:00Z"),calendar::DAY,365);
        auto utc=make_shared<calendar>();
        generic_dt rep(utc,_t("2000-01-01T00:00:00Z"),calendar::YEAR,10);
        auto ta= repeat_time_axis(src,rep);
        // expected_size for the time-axis is actually 3(leap-years!) *366 + 7*365 (taken care of by calendar diff_units)
        auto expected_size =utc->diff_units(rep.total_period().start,rep.total_period().end,calendar::DAY);
        REQUIRE_EQ(ta.size(), expected_size);
        CHECK_EQ(ta.total_period(),rep.total_period());
    }
    TEST_CASE("repeat_calendar_366_dt") {
        generic_dt src(_t("2015-01-01T00:00:00Z"),calendar::DAY,366);
        auto utc=make_shared<calendar>();
        generic_dt rep(utc,_t("2000-01-01T00:00:00Z"),calendar::YEAR,10);
        auto ta= repeat_time_axis(src,rep);
        // expected_size for the time-axis is actually 3(leap-years!) *366 + 7*365 (taken care of by calendar diff_units)
        auto expected_size =utc->diff_units(rep.total_period().start,rep.total_period().end,calendar::DAY);
        REQUIRE_EQ(ta.size(), expected_size);
        CHECK_EQ(ta.total_period(),rep.total_period());
    }

    TEST_CASE("repeat_with_nan_gap_both_sides") {
        generic_dt src(_t(1),_t(1),8);// nan 1...1 nan, repeated 
        generic_dt rep(_t(0),_t(10),3);// every 10 sec
        auto ta= repeat_time_axis(src,rep);
        REQUIRE_EQ(ta.size(),10*3u);
        CHECK_EQ(ta.total_period(),rep.total_period());
        for(size_t i=0;i<ta.size();++i) {
            CHECK_EQ(ta.time(i),_t(i));
        }
    }
    TEST_CASE("repeat_with_nan_gap_left") {
        generic_dt src(_t(1),_t(1),9);// nan 1...1 , repeated 
        generic_dt rep(_t(0),_t(10),3);// every 10 sec
        auto ta= repeat_time_axis(src,rep);
        REQUIRE_EQ(ta.size(),10*3u);
        CHECK_EQ(ta.total_period(),rep.total_period());
        for(size_t i=0;i<ta.size();++i) {
            CHECK_EQ(ta.time(i),_t(i));
        }
    }

    TEST_CASE("repeat_with_nan_gap_right") {
        generic_dt src(_t(0),_t(1),9);// 1...1 nan, repeated 
        generic_dt rep(_t(0),_t(10),3);// every 10 sec
        auto ta= repeat_time_axis(src,rep);
        REQUIRE_EQ(ta.size(),10*3u);
        CHECK_EQ(ta.total_period(),rep.total_period());
        for(size_t i=0;i<ta.size();++i) {
            CHECK_EQ(ta.time(i),_t(i));
        }
    }
}
    //-- real-ts testing.
TEST_SUITE("time_series") {
    TEST_CASE("ts_repeat_fixed_dt") {
        apoint_ts a(generic_dt(_t(10),_t(1),10),vector<double>{0,1,2,3,4,5,6,7,8,9},POINT_AVERAGE_VALUE);
        apoint_ts b(generic_dt(_t(10),_t(1),10),vector<double>{0,1,2,3,4,5,6,7,8,9},POINT_INSTANT_VALUE);
        generic_dt ta_rep(_t(0),_t(10),10);
        auto rep_a= a.repeat(ta_rep);
        auto rep_b= b.repeat(ta_rep);
        auto const& ta=a.time_axis();
        for(size_t i=0;i<ta.size();++i) {
            auto vi=rep_a.value(i);
            auto vt=rep_a(ta.time(i));
            auto vl=rep_b(ta.time(i)+_t(0.5));
            CHECK_EQ(vi,doctest::Approx(double(i%10)));//verify repeated sequence 0..10
            CHECK_EQ(vt,doctest::Approx(double(i%10)));//verify repeated sequence 0..10
            CHECK_EQ(vl,doctest::Approx( 0.5*(double(i%10)+double((i+1)%10))    ));//verify repeated sequence 0.5..8.5..4.5
        }
    }

    TEST_CASE("ts_repeat_with_nan_gap_both_sides") {
        apoint_ts a(generic_dt(_t(1),_t(1),8),vector<double>{0,1,2,3,4,5,6,7},POINT_AVERAGE_VALUE );// nan 1...1 nan, repeated 
        auto rep= a.repeat(generic_dt(_t(0),_t(10),3));// every 10 sec
        auto const& ta= rep.time_axis();
        REQUIRE_EQ(ta.size(),10*3u);
        CHECK_EQ(ta.total_period(),rep.total_period());
        for(size_t i=0;i<ta.size();++i) {
            auto vi=rep.value(i);
            auto vt=rep(ta.time(i));
            size_t ix= i%10;
            double v= ix==0 || ix==9? shyft::nan: double(ix-1); // nan 0..7 nan
            if(!isfinite(v)) {
                CHECK_EQ(false,isfinite(vi));
                CHECK_EQ(false,isfinite(vt));
            } else {
                CHECK_EQ(vi,doctest::Approx(v));
                CHECK_EQ(vt,doctest::Approx(v));
            }
        }
    }
    
    TEST_CASE("ts_repeat_with_nan_gap_left") {
        apoint_ts a(generic_dt(_t(1),_t(1),9),vector<double>{0,1,2,3,4,5,6,7,8},POINT_AVERAGE_VALUE );// nan 1...1 , repeated 
        auto rep=a.repeat(generic_dt(_t(0),_t(10),3));// every 10 sec
        auto const& ta= rep.time_axis();
        REQUIRE_EQ(ta.size(),10*3u);
        CHECK_EQ(ta.total_period(),rep.total_period());
        for(size_t i=0;i<ta.size();++i) {
            auto vi=rep.value(i);
            auto vt=rep(ta.time(i));
            size_t ix= i%10;
            double v= ix==0 ? shyft::nan: double(ix-1); // nan 0..8
            if(!isfinite(v)) {
                CHECK_EQ(false,isfinite(vi));
                CHECK_EQ(false,isfinite(vt));
            } else {
                CHECK_EQ(vi,doctest::Approx(v));
                CHECK_EQ(vt,doctest::Approx(v));
            }
        }
    }

    TEST_CASE("ts_repeat_with_nan_gap_right") {
        apoint_ts a (generic_dt(_t(0),_t(1),9),vector<double>{0,1,2,3,4,5,6,7,8},POINT_AVERAGE_VALUE );
        auto  rep=a.repeat(generic_dt(_t(0),_t(10),3));// every 10 sec
        auto const &ta= rep.time_axis();
        REQUIRE_EQ(ta.size(),10*3u);
        for(size_t i=0;i<ta.size();++i) {
            auto vi=rep.value(i);
            auto vt=rep(ta.time(i));
            size_t ix= i%10;
            double v= ix==9 ? shyft::nan: double(ix); // 0..8 nan
            if(!isfinite(v)) {
                CHECK_EQ(false,isfinite(vi));
                CHECK_EQ(false,isfinite(vt));
            } else {
                CHECK_EQ(vi,doctest::Approx(v));
                CHECK_EQ(vt,doctest::Approx(v));
            }
        }
    }
    
    TEST_CASE("ts_repeat_calendar_365_dt") {
        vector<double> va;for(size_t i=0;i<365;++i) va.push_back(double(i+1));// day-numbers
        apoint_ts a(generic_dt(_t("2015-01-01T00:00:00Z"),calendar::DAY,365),va,POINT_AVERAGE_VALUE);
        auto utc=make_shared<calendar>();
        generic_dt ta_rep(utc,_t("2000-01-01T00:00:00Z"),calendar::YEAR,10);
        auto rep=a.repeat(ta_rep);
        auto const &ta= rep.time_axis();
        for(size_t i=0;i<ta.size();++i) {
            auto vi=rep.value(i);
            auto vt=rep(ta.time(i));
            auto doy= utc->day_of_year(ta.time(i));
            double v = double(doy);
            if(doy == 366) {
                CHECK_EQ(false,isfinite(vi));
                CHECK_EQ(false,isfinite(vt));
            } else {
                CHECK_EQ(vi,doctest::Approx(v));
                CHECK_EQ(vt,doctest::Approx(v));
            }
        }
    }
    TEST_CASE("ts_repeat_calendar_366_dt") {
        vector<double> va;for(size_t i=0;i<366;++i) va.push_back(double(i+1));// day-numbers
        apoint_ts a(generic_dt(_t("2015-01-01T00:00:00Z"),calendar::DAY,366),va,POINT_AVERAGE_VALUE);
        auto utc=make_shared<calendar>();
        generic_dt ta_rep(utc,_t("2000-01-01T00:00:00Z"),calendar::YEAR,10);
        auto rep=a.repeat(ta_rep);
        auto const &ta= rep.time_axis();
        for(size_t i=0;i<ta.size();++i) {
            auto vi=rep.value(i);
            auto vt=rep(ta.time(i));
            auto doy= utc->day_of_year(ta.time(i));
            double v = double(doy);
            CHECK_EQ(vi,doctest::Approx(v));
            CHECK_EQ(vt,doctest::Approx(v));
        }
    }
    
    TEST_CASE("ts_repeat_bind") {
        apoint_ts a("shyft://container/x");
        auto utc=make_shared<calendar>();
        generic_dt ta_rep(utc,_t("2000-01-01T00:00:00Z"),calendar::YEAR,10);
        auto rep=a.repeat(ta_rep);
        CHECK_EQ(true, rep.needs_bind());
        //auto bi=
        vector<double> va;for(size_t i=0;i<366;++i) va.push_back(double(i+1));// day-numbers
        apoint_ts b(generic_dt(_t("2015-01-01T00:00:00Z"),calendar::DAY,366),va,POINT_AVERAGE_VALUE);
        auto bi=rep.find_ts_bind_info();
        REQUIRE_EQ(bi.size(),1);
        bi[0].ts.bind(b);
        rep.do_bind();
        CHECK_EQ(false,rep.needs_bind());
   }
    TEST_CASE("ts_repeat_speed") {
        vector<double> va;for(size_t i=0;i<366;++i) va.push_back(double(i+1));// day-numbers
        apoint_ts a(generic_dt(_t("2015-01-01T00:00:00Z"),calendar::DAY,366),va,POINT_AVERAGE_VALUE);
        auto utc=make_shared<calendar>();
        generic_dt ta_rep(utc,_t("1931-01-01T00:00:00Z"),calendar::YEAR,100);
        auto t0=utctime_now();
        size_t n=10;
        double s=0.0;
        for(size_t i=0;i<n;++i) {
            auto rep=a.repeat(ta_rep);// time-axis get constructed here
            auto e = rep.evaluate();
            s+=e.value(0);
        }
        auto t1=utctime_now();
        MESSAGE("summarized value(0):"<<s);
        WARN(double(to_seconds(t1-t0)/n) < 0.1); //ok, its like 1 ms for each iteration above on 4GHz i7
        //std::cout<<"repeat-ts used "<<to_seconds(t1-t0)/n<<" seconds for 100 year rep one year evals, sum first val all iterations is"<<s<<"\n";
   }

}
