#include <doctest/doctest.h>
#include <shyft/energy_market/ui/srv/server.h>
#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/energy_market/ui/srv/client.h>
#include <memory>
#include <test/test_utils.h>

using std::to_string;
using std::string;
using shyft::core::utctime;
using shyft::core::utctime_now;
using shyft::core::to_seconds64;

namespace test {
    string generate_json(const string& layout_name, const string& /* args*/) {
        if (layout_name == "test")
            return R"_({"name":"generated_layout", "widget": {"misc": "data"}})_";
        else
            return "";
    }
}
TEST_SUITE("config_server") {
    using shyft::srv::model_info;
    using shyft::energy_market::ui::layout_info;
    using shyft::energy_market::ui::srv::config_server;
    using shyft::energy_market::ui::srv::config_client;
    using shyft::srv::client;
    using std::shared_ptr;
    using std::make_shared;


    TEST_CASE("serverio") {
        // set up a server:
        auto dirname = "ui.srv.cfg.test.serverio" + to_string(to_seconds64(utctime_now()));
        test::utils::temp_dir tmpdir(dirname.c_str());
        config_server s(tmpdir.string());
        string host_ip = "127.0.0.1";
        s.set_listening_ip(host_ip);
        auto s_port = s.start_server();
        CHECK_GE(s_port, 1024);
        CHECK_EQ(s_port, s.get_listening_port());

        // Add a model
        auto li = make_shared<layout_info>(2, "laye oeuitte", "{misc.}");
        model_info mi(li->id, li->name, utctime_now(), "");
        s.db.store_model(li, mi);
        CHECK_EQ(s.db.find_max_model_id(),li->id);

        auto mis = s.db.get_model_infos({});
        CHECK_EQ(mis.size(), 1);

        // Read a model
        auto li2 = s.db.read_model(li->id);
        CHECK_EQ(*li, *li2);

        // Remove the model:
        s.db.remove_model(li->id);
        mis = s.db.get_model_infos({});
        CHECK_EQ(mis.size(), 0);
        // Clean up
        s.clear();
    }

    TEST_CASE("server-client") {
        // Set up a server and client
        auto dirname = "ui.srv.cfg.test.server-client" + to_string(to_seconds64(utctime_now()));
        test::utils::temp_dir tmpdir(dirname.c_str());
        config_server s(tmpdir.string());
        string host_ip = "127.0.0.1";
        s.set_listening_ip(host_ip);
        auto s_port = s.start_server();
        CHECK_GE(s_port, 1024);
        MESSAGE("client connecting");
        config_client c{host_ip + ":" + to_string(s_port)};

        MESSAGE("getting model infos");

        // 1. Preliminary check
        auto mis = c.get_model_infos({});
        CHECK_EQ(mis.size(), 0);
        MESSAGE("create add mdl");

        // 2. Create and add one model
        auto li = make_shared<layout_info>(1, "initial layout", "{misc.}");
        model_info mi(li->id, li->name, utctime_now(), "");
        c.store_model(li, mi);
        MESSAGE("model stored, reading modelinfos again");
        mis = c.get_model_infos({});
        MESSAGE("Worked out!, now read model back");
        CHECK_EQ(mis.size(), 1);
        CHECK_EQ(mis[0], mi);

        // 3. Read model
        auto li2 = c.read_model(li->id);
        CHECK_EQ(*li, *li2);
        MESSAGE("// 4.1 Check that it throws if no callback is set:");

        // 4. Read model not yet present (uses callback)
        // 4.1 Check that it throws if no callback is set:
        CHECK_THROWS_AS(c.read_model_with_args(0, "test", "{misc. args}̋"), std::runtime_error);
        // 4.2 With callback set: generate empty string
        s.set_read_cb(test::generate_json);
        MESSAGE("4.2 With callback set: generate empty string");

        CHECK_THROWS_AS(c.read_model_with_args(0, "test_not_ok", ""), std::runtime_error);
        // 4.3 Read model with valid callback
        MESSAGE("// 4.3 Read model with valid callback");
        auto li3 = c.read_model_with_args(0, "test", "{misc. args}", true);
        CHECK_EQ(li3->id, 2);
        CHECK_EQ(li3->json, s.get_read_cb()("test", "not doing anything, currently"));
        mis = c.get_model_infos({});
        CHECK_EQ(mis.size(), 2);
        auto li4 = c.read_model_with_args(0, "test", "{misc. args}", false);
        CHECK_EQ(li4->id, -1);
        CHECK_EQ(li4->name, li3->name);
        CHECK_EQ(li4->json, li3->json);
        CHECK_EQ(mis.size(), 2);
        // Cleanup
        c.close();
        s.clear();
    }
}
