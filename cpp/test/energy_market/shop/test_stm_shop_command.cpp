#include "doctest/doctest.h"
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <string>

TEST_SUITE("stm_shop_command") {

TEST_CASE("shop command")
{
    // NB: Using 6 decimal places for any floating point values in strings, because currently the
    // shop_command is using std::to_string to convert the double value into string and it seems
    // to always use 6 decimals without option to specify anything else.

    using namespace std::string_literals;
    using shyft::energy_market::stm::shop::shop_command;

    SUBCASE("keyword only")
    {
        auto command_string = "quit"s;
        shop_command cmd{ command_string };
        CHECK(cmd.keyword == command_string);
        CHECK(cmd.specifier.empty());
        CHECK(cmd.options.size() == 0);
        CHECK(cmd.objects.size() == 0);
        CHECK(static_cast<std::string>(cmd) == command_string);
    }
    SUBCASE("keyword and specifier")
    {
        const char* const command_string = "save tunnelloss";
        shop_command cmd{ command_string };
        CHECK(cmd.keyword == "save"s);
        CHECK(cmd.specifier == "tunnelloss"s);
        CHECK(cmd.options.size() == 0);
        CHECK(cmd.objects.size() == 0);
        CHECK((std::string)cmd == command_string);
        shop_command cmd2 = shop_command::save_tunnelloss();
        CHECK(cmd == cmd2);
        CHECK((std::string)cmd == (std::string)cmd2);
    }
    SUBCASE("keyword specifier and one option")
    {
        const char* const command_string = "set method /dual";
        shop_command cmd{ command_string };
        CHECK(cmd.keyword == "set"s);
        CHECK(cmd.specifier == "method"s);
        CHECK(cmd.options.size() == 1);
        if (cmd.options.size() == 1) {
            CHECK(cmd.options.front() == "dual"s);
        }
        CHECK(cmd.objects.size() == 0);
        CHECK((std::string)cmd == command_string);
        shop_command cmd2 = shop_command::set_method_dual();
        CHECK(cmd == cmd2);
        CHECK((std::string)cmd == (std::string)cmd2);
    }
    SUBCASE("keyword specifier and string value")
    {
        const char* const command_string = "log file filename.ext";
        shop_command cmd{ command_string };
        CHECK(cmd.keyword == "log"s);
        CHECK(cmd.specifier == "file"s);
        CHECK(cmd.options.size() == 0);
        CHECK(cmd.objects.size() == 1);
        if (cmd.objects.size() == 1) {
            CHECK(cmd.objects.front() == "filename.ext"s);
        }
        CHECK((std::string)cmd == command_string);
        shop_command cmd2 = shop_command::log_file("filename.ext");
        CHECK(cmd == cmd2);
        CHECK((std::string)cmd == (std::string)cmd2);
    }
    SUBCASE("keyword specifier and int value")
    {
        const char* const command_string = "set max_num_threads 8";
        shop_command cmd{ command_string };
        CHECK(cmd.keyword == "set"s);
        CHECK(cmd.specifier == "max_num_threads"s);
        CHECK(cmd.options.size() == 0);
        CHECK(cmd.objects.size() == 1);
        if (cmd.objects.size() == 1) {
            CHECK(cmd.objects.front() == "8"s);
        }
        CHECK((std::string)cmd == command_string);
        shop_command cmd2 = shop_command::set_max_num_threads(8);
        CHECK(cmd == cmd2);
        CHECK((std::string)cmd == (std::string)cmd2);
    }
    SUBCASE("keyword specifier and double value")
    {
        const char* const command_string = "set fcr_n_band 0.400000";
        shop_command cmd{ command_string };
        CHECK(cmd.keyword == "set"s);
        CHECK(cmd.specifier == "fcr_n_band"s);
        CHECK(cmd.options.size() == 0);
        CHECK(cmd.objects.size() == 1);
        if (cmd.objects.size() == 1) {
            CHECK(cmd.objects.front() == "0.400000"s);
        }
        CHECK((std::string)cmd == command_string);
        shop_command cmd2 = shop_command::set_fcr_n_band(0.4);
        CHECK(cmd == cmd2);
        CHECK((std::string)cmd == (std::string)cmd2);
    }
    SUBCASE("keyword specifier option and string value")
    {
        const char* const command_string = "return simres /gen filename.ext";
        shop_command cmd{ command_string };
        CHECK(cmd.keyword == "return"s);
        CHECK(cmd.specifier == "simres"s);
        CHECK(cmd.options.size() == 1);
        if (cmd.options.size() == 1) {
            CHECK(cmd.options.front() == "gen"s);
        }
        CHECK(cmd.objects.size() == 1);
        if (cmd.objects.size() == 1) {
            CHECK(cmd.objects.front() == "filename.ext"s);
        }
        CHECK((std::string)cmd == command_string);
        shop_command cmd2 = shop_command::return_simres_gen("filename.ext");
        CHECK(cmd == cmd2);
        CHECK((std::string)cmd == (std::string)cmd2);
    }
    SUBCASE("keyword specifier option and double value")
    {
        const char* const command_string = "set mipgap /relative 0.300000";
        shop_command cmd{ command_string };
        CHECK(cmd.keyword == "set"s);
        CHECK(cmd.specifier == "mipgap"s);
        CHECK(cmd.options.size() == 1);
        if (cmd.options.size() == 1) {
            CHECK(cmd.options.front() == "relative"s);
        }
        CHECK(cmd.objects.size() == 1);
        if (cmd.objects.size() == 1) {
            CHECK(cmd.objects.front() == "0.300000"s);
        }
        CHECK((std::string)cmd == command_string);
        shop_command cmd2 = shop_command::set_mipgap(false, 0.3);
        CHECK(cmd == cmd2);
        CHECK((std::string)cmd == (std::string)cmd2);
    }
    SUBCASE("keyword specifier and three options")
    {
        const char* const command_string = "penalty flag /on /reservoir /ramping";
        shop_command cmd{ command_string };
        CHECK(cmd.keyword == "penalty"s);
        CHECK(cmd.specifier == "flag"s);
        CHECK(cmd.options.size() == 3);
        if (cmd.options.size() == 3) {
            CHECK(cmd.options[0] == "on"s);
            CHECK(cmd.options[1] == "reservoir"s);
            CHECK(cmd.options[2] == "ramping"s);
        }
        CHECK(cmd.objects.size() == 0);
        CHECK((std::string)cmd == command_string);
        shop_command cmd2 = shop_command::penalty_flag_reservoir_ramping(true);
        CHECK(cmd == cmd2);
        CHECK((std::string)cmd == (std::string)cmd2);
    }
    SUBCASE("keyword specifier two options and double value")
    {
        const char* const input_command_string = "\t penalty   cost   /reservoir \t /ramping    123.500000   ";
        const char* const output_command_string = "penalty cost /reservoir /ramping 123.500000";
        shop_command cmd{ input_command_string };
        CHECK(cmd.keyword == "penalty"s);
        CHECK(cmd.specifier == "cost"s);
        CHECK(cmd.options.size() == 2);
        if (cmd.options.size() == 2) {
            CHECK(cmd.options[0] == "reservoir"s);
            CHECK(cmd.options[1] == "ramping"s);
        }
        CHECK(cmd.objects.size() == 1);
        if (cmd.objects.size() == 1) {
            CHECK(cmd.objects[0] == "123.500000"s);
        }
        CHECK((std::string)cmd == output_command_string);
        shop_command cmd2 = shop_command::penalty_cost_reservoir_ramping(123.50);
        CHECK(cmd == cmd2);
        CHECK((std::string)cmd == (std::string)cmd2);
    }
    SUBCASE("multiple objects")
    {
        const char* const command_string = "the quick brown fox jumps over the lazy dog";
        shop_command cmd{ command_string };
        CHECK(cmd.keyword == "the"s);
        CHECK(cmd.specifier == "quick"s);
        CHECK(cmd.options.size() == 0);
        CHECK(cmd.objects.size() == 7);
        if (cmd.objects.size() == 7) {
            CHECK(cmd.objects[0] == "brown"s);
            CHECK(cmd.objects[1] == "fox"s);
            CHECK(cmd.objects[2] == "jumps"s);
            CHECK(cmd.objects[3] == "over"s);
            CHECK(cmd.objects[4] == "the"s);
            CHECK(cmd.objects[5] == "lazy"s);
            CHECK(cmd.objects[6] == "dog"s);
        }
        CHECK((std::string)cmd == command_string);
    }
    SUBCASE("parse strict examples")
    {
        // Explicitely using parse_strict with some arguments that are handled
        // differently from parse_lenient (tested with same arguments in next subcase).
        {
            shop_command cmd;
            CHECK_THROWS(cmd.parse_strict("The /quick" ));
            CHECK_THROWS(cmd.parse_strict("The /quick /brown"));
            CHECK_THROWS(cmd.parse_strict("The /quick /brown /fox"));
            CHECK_THROWS(cmd.parse_strict("The quick /brown fox /jumps"));
            CHECK_THROWS(cmd.parse_strict("The quick brown /fox"));
            CHECK_THROWS(cmd.parse_strict("The quick brown /fox /jumps"));
            CHECK_THROWS(cmd.parse_strict("print mc_curves mc.xml 12 24 /up /down")); // From shop documentation: Objects before options
        }
        {
            shop_command cmd{ "print mc_curves /up /down mc.xml 12 24" }; // Alternative variant with options before objects
            CHECK(cmd.keyword == "print"s);
            CHECK(cmd.specifier == "mc_curves"s);
            CHECK(cmd.options.size() == 2);
            if (cmd.options.size() == 2) {
                CHECK(cmd.options[0] == "up"s);
                CHECK(cmd.options[1] == "down"s);
            }
            CHECK(cmd.objects.size() == 3);
            if (cmd.objects.size() == 3) {
                CHECK(cmd.objects[0] == "mc.xml"s);
                CHECK(cmd.objects[1] == "12"s);
                CHECK(cmd.objects[2] == "24"s);
            }
        }
        {
            shop_command cmd;
            CHECK_THROWS(cmd.parse_strict("print mc_curves mc.xml /up 12 /down 24")); // Alternative variant with mixed order of options before objects
            CHECK_THROWS(cmd.parse_strict("print mc_curves mc.xml/up/down 12 24")); // Alternative variant with no space before options
        }
    }
    SUBCASE("parse lenient examples")
    {
        // Explicitely using parse_lenient with some arguments that are handled
        // differently from parse_strict (tested with same arguments in previous subcase).
        {
            shop_command cmd;
            cmd.parse_lenient("The /quick");
            CHECK(cmd.keyword == "The"s);
            CHECK(cmd.specifier.empty());
            CHECK(cmd.objects.size() == 0);
            CHECK(cmd.options.size() == 1);
            if (cmd.options.size() == 1) {
                CHECK(cmd.options[0] == "quick"s);
            }
        }
        {
            shop_command cmd;
            cmd.parse_lenient("The /quick /brown");
            CHECK(cmd.keyword == "The"s);
            CHECK(cmd.specifier.empty());
            CHECK(cmd.objects.size() == 0);
            CHECK(cmd.options.size() == 2);
            if (cmd.options.size() == 2) {
                CHECK(cmd.options[0] == "quick"s);
                CHECK(cmd.options[1] == "brown"s);
            }
        }
        {
            shop_command cmd;
            cmd.parse_lenient("The /quick /brown /fox");
            CHECK(cmd.keyword == "The"s);
            CHECK(cmd.specifier.empty());
            CHECK(cmd.objects.size() == 0);
            CHECK(cmd.options.size() == 3);
            if (cmd.options.size() == 3) {
                CHECK(cmd.options[0] == "quick"s);
                CHECK(cmd.options[1] == "brown"s);
                CHECK(cmd.options[2] == "fox"s);
            }
        }
        {
            shop_command cmd;
            cmd.parse_lenient("The quick /brown fox /jumps");
            CHECK(cmd.keyword == "The"s);
            CHECK(cmd.specifier == "quick"s);
            CHECK(cmd.objects.size() == 1);
            if (cmd.objects.size() == 1) {
                CHECK(cmd.objects[0] == "fox"s);
            }
            CHECK(cmd.options.size() == 2);
            if (cmd.options.size() == 2) {
                CHECK(cmd.options[0] == "brown"s);
                CHECK(cmd.options[1] == "jumps"s);
            }
        }
        {
            shop_command cmd;
            cmd.parse_lenient("The quick brown /fox");
            CHECK(cmd.keyword == "The"s);
            CHECK(cmd.specifier == "quick"s);
            CHECK(cmd.objects.size() == 1);
            if (cmd.objects.size() == 1) {
                CHECK(cmd.objects[0] == "brown"s);
            }
            CHECK(cmd.options.size() == 1);
            if (cmd.options.size() == 1) {
                CHECK(cmd.options[0] == "fox"s);
            }
        }
        {
            shop_command cmd;
            cmd.parse_lenient("The quick brown /fox /jumps");
            CHECK(cmd.keyword == "The"s);
            CHECK(cmd.specifier == "quick"s);
            CHECK(cmd.objects.size() == 1);
            if (cmd.objects.size() == 1) {
                CHECK(cmd.objects[0] == "brown"s);
            }
            CHECK(cmd.options.size() == 2);
            if (cmd.options.size() == 2) {
                CHECK(cmd.options[0] == "fox"s);
                CHECK(cmd.options[1] == "jumps"s);
            }
        }

        auto check_print_mc_curves = [](const shop_command& cmd){
            CHECK(cmd.keyword == "print"s);
            CHECK(cmd.specifier == "mc_curves"s);
            CHECK(cmd.options.size() == 2);
            if (cmd.options.size() == 2) {
                CHECK(cmd.options[0] == "up"s);
                CHECK(cmd.options[1] == "down"s);
            }
            CHECK(cmd.objects.size() == 3);
            if (cmd.objects.size() == 3) {
                CHECK(cmd.objects[0] == "mc.xml"s);
                CHECK(cmd.objects[1] == "12"s);
                CHECK(cmd.objects[2] == "24"s);
            }
            CHECK(static_cast<std::string>(cmd) == "print mc_curves /up /down mc.xml 12 24"s);
        };
        {
            // From shop documentation: Objects before options
            shop_command cmd;
            cmd.parse_lenient("print mc_curves mc.xml 12 24 /up /down");
            check_print_mc_curves(cmd);
        }
        {
            // Alternative variant with options before objects
            shop_command cmd;
            cmd.parse_lenient("print mc_curves /up /down mc.xml 12 24");
            check_print_mc_curves(cmd);
        }
        {
            // Alternative variant with mixed order of options before objects
            shop_command cmd;
            cmd.parse_lenient("print mc_curves mc.xml /up 12 /down 24");
            check_print_mc_curves(cmd);
        }
        {
            // Alternative variant with no space before options
            shop_command cmd;
            cmd.parse_lenient("print mc_curves mc.xml/up/down 12 24");
            check_print_mc_curves(cmd);
        }
    }
}

}
