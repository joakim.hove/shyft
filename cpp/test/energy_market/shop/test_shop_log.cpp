#include <doctest/doctest.h>
#include "model_simple.h"
#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/energy_market/stm/shop/shop_log_entry.h>
#include <shyft/energy_market/stm/srv/dstm/context.h>
#include <shyft/energy_market/stm/srv/dstm/context_enums.h>

using shyft::energy_market::stm::shop::shop_system;
using shyft::energy_market::stm::shop::shop_log_entry;
using shyft::energy_market::stm::srv::dstm::stm_system_context;
using shyft::energy_market::stm::srv::dstm::model_state;
using std::vector;
using std::make_shared;

static const bool SKIP_OPTIMIZE = false;
//static const bool shop_log_files = false;
static const bool write_file_commands = false;
static const bool shop_console_output = false;


void print_log(const std::vector<shop_log_entry>& msgs) {
    for (auto const& msg: msgs) {
        switch (msg.severity) {
            case shop_log_entry::log_severity::information:
                std::cout << "INFORMATION";
                break;
            case shop_log_entry::log_severity::diagnosis_information:
                std::cout << "DIAGNOSIS INFORMATION";
                break;
            case shop_log_entry::log_severity::warning:
                std::cout << "WARNING";
                break;
            case shop_log_entry::log_severity::diagnosis_warning:
                std::cout << "DIAGNOSIS WARNING";
                break;
            case shop_log_entry::log_severity::error:
                std::cout << "ERROR";
                break;
            case shop_log_entry::log_severity::diagnosis_error:
                std::cout << "DIAGNOSIS ERROR";
                break;
        }
        std::cout << " [" << msg.code << "]:\t" << msg.message + "\n";
    }
}
TEST_SUITE("stm_shop_log") {
    const bool always_inlet_tunnels = false;
    const bool use_defaults = false;
    const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    const auto dt = shyft::core::deltahours(1);
    const size_t n_t = (t_end - t_begin)/dt;
    const shyft::time_axis::generic_dt ta(t_begin, dt, n_t);

    TEST_CASE("get_shop_log_buffer"
        * doctest::description("usage of shop_system::get_log_buffer")
        * doctest::skip(SKIP_OPTIMIZE))
    {
        // Set up system to optimize and expected solution
        auto stm = build_simple_model(t_begin, t_end, dt, always_inlet_tunnels, use_defaults);
        auto rstm = build_simple_model(t_begin, t_end, dt, always_inlet_tunnels, use_defaults, 1, true);
        // Create shop system
        shop_system shop(ta);

        // 0. Check that log is empty:
        vector<shop_log_entry> messages = shop.get_log_buffer();
        CHECK_EQ(messages.size(), 0);
        // Do optimization:
        const auto commands = optimization_commands(1, write_file_commands);
        shop.set_logging_to_stdstreams(shop_console_output);
        shop.set_logging_to_files(write_file_commands);
        shop.emit(*stm);
        // 1. Check log after emit:
        vector<shop_log_entry> new_messages = shop.get_log_buffer();
        messages.insert(messages.end(), new_messages.begin(), new_messages.end());
        CHECK_EQ(messages.size(), 0);
        shop.command(commands);
        // 2. Check log after commands:
        new_messages = shop.get_log_buffer();
        messages.insert(messages.end(), new_messages.begin(), new_messages.end());
        size_t N = messages.size();
        CHECK_GE(N, 0);
        CHECK_EQ(shop.get_log_buffer().size(), 0);

        shop.collect(*stm);
        // 3. Check log after collecting results
        CHECK_EQ(shop.get_log_buffer().size(), 0);

        check_results(stm, rstm, t_begin, t_end, dt);
        std::cout << "LOG MESSAGES:\n";
        print_log(messages);
    }

    TEST_CASE("get_shop_log_buffer_context"
        * doctest::description("usage of shop_system::visitor to handle logging")
        * doctest::skip(SKIP_OPTIMIZE))
    {
        // Set up system context and expected solution
        auto stm = build_simple_model(t_begin, t_end, dt, always_inlet_tunnels, use_defaults);
        auto ctx = make_shared<stm_system_context>(model_state::idle, stm);
        auto rstm = build_simple_model(t_begin, t_end, dt, always_inlet_tunnels, use_defaults, 1, true);
        // Create shop system
        shop_system shop(ta, ctx.get());

        // 0. Check that log is empty:
        CHECK_EQ(ctx->shop_log_size(), 0);
        // Do optimization:
        const auto commands = optimization_commands(1, write_file_commands);
        shop.set_logging_to_stdstreams(shop_console_output);
        shop.set_logging_to_files(write_file_commands);
        shop.emit(*stm);
        // 1. Check log after emit: (should be nothing, it seems)
        CHECK_FALSE(shop.vis->add_shop_log(shop.get_log_buffer()));
        shop.command(commands);
        // 2. Check log after commands:
        size_t N = ctx->shop_log_size();
        CHECK_GE(N, 0);
        CHECK_EQ(shop.get_log_buffer().size(), 0);

        shop.collect(*stm);
        // 3. Check log after collecting results
        CHECK_FALSE(shop.vis->add_shop_log(shop.get_log_buffer()));

        check_results(stm, rstm, t_begin, t_end, dt);
    }
}
