#include <doctest/doctest.h>
#define NEEDS_OPTIMIZATION_COMMANDS
#include "model_simple.h"

// For debug output from tests:
// - define DEBUG_TESTS (uncomment)
// - modify debug flags within the "#ifdef DEBUG_TESTS" section only
// This will issue a message in the build output, to hopefully remember not to commit with debug flags enabled.
//#define DEBUG_TESTS
#ifdef DEBUG_TESTS
#pragma message("*** Remember to undefine DEBUG_TESTS before commit ***")
static const bool shop_console_output = false;
static const bool shop_log_files = false;
static const bool write_file_commands = false; // Should the test write log and results to file for manual inspection?
static const bool SKIP_OPTIMIZE = false;
#else
static const bool shop_console_output = false;
static const bool shop_log_files = false;
static const bool write_file_commands = false; // Should the test write log and results to file for manual inspection?
static const bool SKIP_OPTIMIZE = false;
#endif

using shyft::time_axis::generic_dt;

TEST_SUITE("stm_shop_optimize") {

    const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    const auto t_step = shyft::core::deltahours(1);
    const size_t n_step=18;
    const generic_dt time_axis{t_begin,t_step,n_step};
    
    const shyft::core::utcperiod t_period{ time_axis.total_period()};
    const auto t_end = t_period.end;

    static std::size_t run_id = 1; // Unique number, to not overwrite files from other tests when write_file_commands==true.

TEST_CASE("optimize simple model without inlet"
    * doctest::description("building and optmimizing simple model without inlet segment: aggregate-penstock-maintunnel-reservoir")
    * doctest::skip(SKIP_OPTIMIZE))
{
    const bool always_inlet_tunnels = false;
    const bool use_defaults = false;
        
    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true); // Model with expected results
    // Run optimization
    // Alt 1: Fixed res
    shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id, write_file_commands), shop_console_output, shop_log_files);
    // Alt 2: Dynamic res
    //auto n_steps = (size_t)((t_end - t_begin) / t_step);
    //shyft::time_axis::generic_dt ta{ t_begin, t_step, n_steps };
    //stm->optimize(ta, options);
    // Compare results:
    check_results(stm, rstm, t_begin, t_end, t_step);
}

TEST_CASE("optimize simple model with inlet"
    * doctest::description("building and optmimizing simple model with inlet segment: aggregate-inlet-penstock-maintunnel-reservoir")
    * doctest::skip(SKIP_OPTIMIZE))
{
    const bool always_inlet_tunnels = false;
    const bool use_defaults = false;
    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1);
    auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true);
    shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id, write_file_commands), shop_console_output, shop_log_files);
    check_results(stm, rstm, t_begin, t_end, t_step);
}

TEST_CASE("optimize simple model with discharge group"
          * doctest::description("building and optmimizing simple model without explicit values for lrl/hrl/maxvol/p_min/p_max/p_nom")
          * doctest::skip(SKIP_OPTIMIZE))
{
    using namespace shyft::energy_market;
    const bool always_inlet_tunnels = false;
    const bool use_defaults = true;
    bool verbose=false;// true to view ouput from operational reserve  run etc.

    // Build models
    auto stm = build_simple_model_discharge_group(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    auto rstm = build_simple_model_discharge_group(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);

    // Fetch stm objects connected to shop objects
    auto rsv_ = std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(stm->hps.front()->find_reservoir_by_name("reservoir"));
    auto unit_ = std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(stm->hps.front()->find_unit_by_name("aggregate"));
    auto flood_river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(stm->hps.front()->find_waterway_by_name("waterroute flood river"));
    auto bypass_river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(stm->hps.front()->find_waterway_by_name("waterroute bypass"));
    auto bypass_gate_ = std::dynamic_pointer_cast<shyft::energy_market::stm::gate>(bypass_river_->gates.front());
    auto river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(stm->hps.front()->find_waterway_by_name("waterroute river"));

    // Decorate connection with attrs to enforce discharge group
    river_->discharge.reference = make_constant_ts(t_begin, t_end, 0);
    river_->discharge.constraint.accumulated_max = make_constant_ts(t_begin, t_end, 1e6);
    river_->discharge.penalty.cost.accumulated_max = make_constant_ts(t_begin, t_end, 10);
    //
    // Set buypass schedule, to check if river_ gets input from both production and bypass
    bypass_gate_->discharge.schedule = make_constant_ts(t_begin, t_end, 10);

    generic_dt time_axis{t_begin,t_step,n_step};
    generic_dt time_axis_accumulate{t_begin,t_step,n_step + 1};
    shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id, write_file_commands), shop_console_output, shop_log_files);
    shyft::energy_market::stm::shop::shop_system::optimize(*rstm, time_axis, optimization_commands(run_id, write_file_commands), shop_console_output, shop_log_files);
    check_results_with_discharge_group(stm, rstm, t_begin, t_end, t_step);

    // Helper lambda to print results
    auto show_ts=[verbose](string title,apoint_ts const&a,double scale, std::ostream&os){
        if(!verbose) return;
        os<<title;
        if(!!a) {
            for(auto i=0u;i<a.size();++i) {
                if(i) os<<", ";
                os.width(4);os.precision(0);
                os<<std::fixed<< a.value(i)/scale;
            }
        } else os<<"<empty>";
        os<<std::endl;
    };

    // Observer river_ object, connecting bypass and wtr from unit, contributes to discharge.result
    show_ts(rsv_->name+".volume.result=",rsv_->volume.result,1e6,std::cout);
    show_ts(rsv_->name+".level.result=",rsv_->level.result,1,std::cout);
    show_ts(unit_->name+".production.result=",unit_->production.result, 1,std::cout);
    show_ts(unit_->name+".discharge.result=",unit_->discharge.result, 1,std::cout);
    show_ts(unit_->name+".discharge.result.accumulate=",unit_->discharge.result.accumulate(time_axis_accumulate), 1,std::cout);
    show_ts(flood_river_->name+".discharge.result=",flood_river_->discharge.result,1,std::cout);
    show_ts(bypass_river_->name+".discharge.result=",bypass_river_->discharge.result,1,std::cout);
    show_ts(bypass_gate_->name+".discharge.result=",bypass_gate_->discharge.result,1,std::cout);
    show_ts(river_->name+".discharge.result=",river_->discharge.result,1,std::cout);
    show_ts(river_->name+".discharge.result.accumulate=",river_->discharge.result.accumulate(time_axis_accumulate),1,std::cout);

}

TEST_CASE("optimize_model_w_2_units"
    * doctest::description("building and optmimizing model with 2 units")
    * doctest::skip(SKIP_OPTIMIZE))
{
    using namespace shyft::energy_market;
    const bool always_inlet_tunnels = false;
    const bool use_defaults = false;
    auto stm = build_simple_model_n_units(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, 3);
    auto log_handler = std::make_shared<shyft::energy_market::stm::shop::shop_memory_logger>();

    shyft::energy_market::stm::shop::shop_system shop{ time_axis };
    shop.install_logger(log_handler);
    shop.set_logging_to_stdstreams(shop_console_output);
    shop.set_logging_to_files(shop_log_files);
    shop.emit(*stm);
    auto cmds=optimization_commands(run_id, write_file_commands);
    cmds.insert(cmds.begin(),stm::shop::shop_command::set_universal_mip_on());// let shop find the ideal combination
    shop.command(cmds);
    shop.collect(*stm);
    //shop.export_topology();
    bool verbose=false;// true to view ouput from operational reserve  run etc.
    auto show_ts=[verbose](string title,apoint_ts const&a,double scale, std::ostream&os){
      if(!verbose) return;
      os<<title;
      if(!!a) {
        for(auto i=0u;i<a.size();++i) {
            if(i) os<<", ";
            os.width(4);os.precision(0);
            os<<std::fixed<< a.value(i)/scale;
        }
      } else os<<"<empty>";
      os<<std::endl;
    };
    apoint_ts run_ts(time_axis,1.0,shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    for(auto const & u_:stm->hps.front()->units) {
        auto u=std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(u_);
        show_ts("unit "+u->name +": MW=",u->production.result,1e6,std::cout);
    }
    for(auto const & u_:stm->hps.front()->units) {
        auto u=std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(u_);
        show_ts(u->name+".fcr_n.up=",u->reserve.fcr_n.up.result,1e6,std::cout);
        show_ts(u->name+".fcr_n.dn=",u->reserve.fcr_n.down.result,1e6,std::cout);
        show_ts(u->name+".afrr.up =",u->reserve.afrr.up.result,1e6,std::cout);
        show_ts(u->name+".afrr.dn =",u->reserve.afrr.down.result,1e6,std::cout);
        auto mbr_ts= compute_unit_group_ts(stm->unit_groups,u,run_ts,shyft::energy_market::stm::unit_group_type::fcr_n_up);
        show_ts(u->name+".fcr_n.up.unit_group= ",mbr_ts,1.0,std::cout);
        auto afrr_up_ts= compute_unit_group_ts(stm->unit_groups,u,run_ts,shyft::energy_market::stm::unit_group_type::afrr_up);
        show_ts(u->name+".afrr.up.unit_group= ",afrr_up_ts,1.0,std::cout);
    }
    for(auto const &ug: stm->unit_groups) {
        show_ts(ug->name+".obligation.penalty:",ug->obligation.penalty,1e6,std::cout);
        show_ts(ug->name+".obligation.result:",ug->obligation.result,1e6,std::cout);
    }

    for(auto const &r_: stm->hps.front()->reservoirs) {
        auto r=std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(r_);
        show_ts(r->name+".volume.result",r->volume.result, 1e6, std::cout);
        show_ts(r->name+".water_value.result.local_energy",r->water_value.result.local_energy, 1, std::cout);
        show_ts(r->name+".water_value.result.local_volume",r->water_value.result.local_volume, 1, std::cout);
        show_ts(r->name+".water_value.result.global_volume",r->water_value.result.global_volume,1, std::cout);
    }

    int regression_severity_sum=0;
    for(auto const &msg:log_handler->messages) {
        regression_severity_sum += msg.severity;
        if(verbose) std::cout<<msg.severity<<":"<<msg.message;
    }
    if(verbose) std::cout<<std::endl;
    FAST_CHECK_EQ(regression_severity_sum,6*shyft::energy_market::stm::shop::shop_log_entry::log_severity::warning);// on a fresh case, this is number of warnings

   //TODO: Now add unit-group, and 
   //      set RR aFRR etc. requirements to the groups.
   // then re-rerun the model and observe that the OPT tries
   // to satisfy the requirements.
    REQUIRE(log_handler->messages.size() > 0);

}

TEST_CASE("optimize_model_unit_group_market_area_reference")
{
    // TODO: Implement
}

TEST_CASE("optimize simple model with defaults"
    * doctest::description("building and optmimizing simple model without explicit values for lrl/hrl/maxvol/p_min/p_max/p_nom")
    * doctest::skip(SKIP_OPTIMIZE))
{
    const bool always_inlet_tunnels = false;
    const bool use_defaults = true;
    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id, write_file_commands), shop_console_output, shop_log_files);
}

TEST_CASE("optimize with log handler"
    * doctest::description("building and optmimizing simple model with log handler attached")
    * doctest::skip(SKIP_OPTIMIZE))
{
    const bool always_inlet_tunnels = false;
    const bool use_defaults = true;
    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);

    //struct test_stm_shop_log_handler : shop_stream_logger {
    //	//std::ostringstream stream;
    //	test_stm_shop_log_handler() : shop_stream_logger{ "test_stm_shop_log_handler", std::cout } { enter(); }
    //	~test_stm_shop_log_handler() { exit(); }
    //};
    //auto log_handler = std::make_shared<test_stm_shop_log_handler>();

    auto log_handler = std::make_shared<shyft::energy_market::stm::shop::shop_memory_logger>();

    //shyft::energy_market::stm::shop::shop_system::optimize(stm, t_begin, t_end, t_step, optimization_commands(run_id, write_file_commands), silent);

    shyft::energy_market::stm::shop::shop_system shop{ time_axis };
    shop.install_logger(log_handler);
    shop.set_logging_to_stdstreams(shop_console_output);
    shop.set_logging_to_files(shop_log_files);
    shop.emit(*stm);
    shop.command(optimization_commands(run_id, write_file_commands));
    shop.collect(*stm);
    //shop.export_topology();

    REQUIRE(log_handler->messages.size() > 0);

#if 0
    // For debugging:
    size_t i = 0;
    for (const auto& msg : log_handler->messages) {
        std::cout << i++ << " [" << (int)msg.severity << "]:" << msg.message;
}
#endif

#if _WIN32
    REQUIRE(log_handler->messages.size() == 283); // 283 as of 2021.06.17 (v13.5.5.g); 288 with new tunnel modelling; 273 as of 2021.03.16 (v13.5.4.e) with SHOP_OPEN and SHOP_TUNNEL license; 271 in Shop API version 13.2.1.d, 157 (!!) in version 13.1.1.d, 269 in previous versions.
    CHECK(log_handler->messages[36].severity == shyft::energy_market::stm::shop::shop_log_entry::log_severity::information);
    CHECK(log_handler->messages[36].message.rfind("Overview SHOP environment variables", 1) != std::string::npos);
    CHECK(log_handler->messages[112].message.rfind("Full description. No curve fitting.", 0) != std::string::npos);
    CHECK(log_handler->messages[115].message.rfind("1. iteration.", 0) != std::string::npos);
    CHECK(log_handler->messages[142].message.rfind("2. iteration.", 0) != std::string::npos);
    CHECK(log_handler->messages[170].message.rfind("3. iteration.", 0) != std::string::npos);
    CHECK(log_handler->messages[195].message.rfind("Incremental description.", 0) != std::string::npos);
    CHECK(log_handler->messages[198].message.rfind("1. iteration.", 0) != std::string::npos);
    CHECK(log_handler->messages[227].message.rfind("2. iteration.", 0) != std::string::npos);
    CHECK(log_handler->messages[257].message.rfind("3. iteration.", 0) != std::string::npos);
    CHECK(log_handler->messages[282].message.rfind("Time used in CPLEX", 0) != std::string::npos);
#else
    // Linux version 13.1.2.c with Cplex 12.8.0 includes 6 additional messages, one "Calling CPXcheckprob" for each iteration.
    REQUIRE(log_handler->messages.size() > 100 );
    // CHECK(log_handler->messages[0].severity == shop_log_entry::info);
    // CHECK(log_handler->messages[0].message.rfind("Overview SHOP environment variables",1) != std::string::npos);
    // CHECK(log_handler->messages[2].message.rfind("Full description. No curve fitting.", 0) != std::string::npos);
    // CHECK(log_handler->messages[5].message.rfind("1. iteration.", 0) != std::string::npos);
    // CHECK(log_handler->messages[32].message.rfind("2. iteration.", 0) != std::string::npos);
    // CHECK(log_handler->messages[59].message.rfind("3. iteration.", 0) != std::string::npos);
    // CHECK(log_handler->messages[83].message.rfind("Incremental description.", 0) != std::string::npos);
    // CHECK(log_handler->messages[86].message.rfind("1. iteration.", 0) != std::string::npos);
    // CHECK(log_handler->messages[113].message.rfind("2. iteration.", 0) != std::string::npos);
    // CHECK(log_handler->messages[140].message.rfind("3. iteration.", 0) != std::string::npos);
    // CHECK(log_handler->messages[162].message.rfind("Time used in CPLEX", 0) != std::string::npos);
#endif
}

}
