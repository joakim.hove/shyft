#pragma once
#include <cmath>
#include <cfloat>
#include <climits>
#include <shop_lib_interface.h> // External Shop library
#include <doctest/doctest.h> // Doctest testing framework

struct model_builder
{
    ShopSystem* shop;
    model_builder(ShopSystem* shop) : shop(shop) {};

    static constexpr int n_scen = 1; // We don't test with multiple scenarios, so always just one!
    void SetGetCompareInt(const int object_ix, const char*  attribute_, int v, bool check_exists) const
    {
        shop_attr_str attribute;strcpy(attribute,attribute_);
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute);
        if (check_exists)
            CHECK(!ShopAttributeExists(shop, object_ix, attr_ix));
        CHECK(ShopCheckAttributeToSet(shop, attr_ix));
        CHECK(ShopSetIntAttribute(shop, object_ix, attr_ix, v));
        if (check_exists)
            CHECK(ShopAttributeExists(shop, object_ix, attr_ix));
        CHECK(ShopCheckAttributeToGet(shop, attr_ix));
        int v_read = INT_MIN;
        ShopGetIntAttribute(shop, object_ix, attr_ix, v_read);
        CHECK(v_read == v);
    }
    void SetGetCompareDouble(const int object_ix, const char * attribute_, double v, bool check_exists) const
    {
        shop_attr_str attribute;strcpy(attribute,attribute_);
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute);
        if (check_exists)
            CHECK(!ShopAttributeExists(shop, object_ix, attr_ix));
        CHECK(ShopCheckAttributeToSet(shop, attr_ix));
        CHECK(ShopSetDoubleAttribute(shop, object_ix, attr_ix, v));
        if (check_exists)
            CHECK(ShopAttributeExists(shop, object_ix, attr_ix));
        CHECK(ShopCheckAttributeToGet(shop, attr_ix));
        double v_read = -DBL_MAX;
        ShopGetDoubleAttribute(shop, object_ix, attr_ix, v_read);
        CHECK(v_read == v);
    }
    template<typename VArray>
    void SetGetCompareDoubleArray(const int object_ix, const char* attribute_, const int n, VArray v, bool check_exists) const
    {
        shop_attr_str attribute;strcpy(attribute,attribute_);
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute);
        if (check_exists)
            CHECK(!ShopAttributeExists(shop, object_ix, attr_ix));
        CHECK(ShopCheckAttributeToSet(shop, attr_ix));
        double* v_ptr = &v[0]; // Type of argument v may already be double*, but in case it is std::array<double,n> or std::vector<double> we need to make it a double* type compatible with the Shop API.
        CHECK(ShopSetDoubleArrayAttribute(shop, object_ix, attr_ix, n, v_ptr));
        if (check_exists)
            CHECK(ShopAttributeExists(shop, object_ix, attr_ix));
        CHECK(ShopCheckAttributeToGet(shop, attr_ix));
        int n_get = INT_MIN;
        CHECK(ShopGetDoubleArrayLength(shop, object_ix, attr_ix, n_get));
        CHECK(n_get == n);
        double* v_get = new double[n_get];
        n_get = INT_MIN;
        CHECK(ShopGetDoubleArrayAttribute(shop, object_ix, attr_ix, n_get, v_get));
        CHECK(n_get == n);
        for (int i=0;i<n_get;++i) {
            CHECK(v[i] == v_get[i]);
        }
        delete[] v_get;
    }
    template<typename XArray, typename YArray>
    void SetGetCompareXY(const int object_ix, const char*  attribute_, double ref, const int n, XArray x, YArray y, bool check_exists) const
    {
        shop_attr_str attribute;strcpy(attribute,attribute_);
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute);
        if (check_exists)
            CHECK(!ShopAttributeExists(shop, object_ix, attr_ix));
        CHECK(ShopCheckAttributeToSet(shop, attr_ix));
        double* x_ptr = &x[0]; // Type of argument x may already be double*, but in case it is std::array<double,n> or std::vector<double> we need to make it a double* type compatible with the Shop API.
        double* y_ptr = &y[0]; // Type of argument y may already be double*, but in case it is std::array<double,n> or std::vector<double> we need to make it a double* type compatible with the Shop API.
        CHECK(ShopSetXyAttribute(shop, object_ix, attr_ix, ref, n, x_ptr, y_ptr));
        if (check_exists)
            CHECK(ShopAttributeExists(shop, object_ix, attr_ix));
        CHECK(ShopCheckAttributeToGet(shop, attr_ix));
        int n_get = INT_MIN;
        CHECK(ShopGetXyAttributeNPoints(shop, object_ix, attr_ix, n_get));
        CHECK(n_get == n);
        n_get = INT_MIN;
        double ref_get = -DBL_MAX;
        double* x_get = new double[n];
        double* y_get = new double[n];
        CHECK(ShopGetXyAttribute(shop, object_ix, attr_ix, ref_get, n_get, x_get, y_get));
        CHECK(n_get == n);
        CHECK(ref_get == ref);
        for (int i=0;i<n_get;++i) {
            CHECK(x[i] == x_get[i]);
            CHECK(y[i] == y_get[i]);
        }
        delete[] x_get;
        delete[] y_get;
    }
    template<typename RefArray, typename NPointsArray, typename XArray, typename YArray>
    void SetGetCompareXYArray(const int object_ix, const char*  attribute_, const int n_dimensions,
        RefArray ref, NPointsArray n_points, XArray x, YArray y, bool check_exists) const
    {
        shop_attr_str attribute;strcpy(attribute,attribute_);
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute);
        if (check_exists)
            CHECK(!ShopAttributeExists(shop, object_ix, attr_ix));
        CHECK(ShopCheckAttributeToSet(shop, attr_ix));
        double* ref_ptr = &ref[0]; // Type of argument ref may already be double*, but in case it is std::array<double,n> or std::vector<double> we need to make it a double* type compatible with the Shop API.
        int* n_points_ptr = &n_points[0]; // Type of argument n_points may already be int*, but in case it is std::array<int,n> or std::vector<int> we need to make it a int* type compatible with the Shop API.
        double* x_ptr = &x[0][0];
        double** x_pptr = &x_ptr; // Type of argument x may already be double**, but in case it is std::array<std::array<double,m>,n> or std::vector<std::vector<double>> we need to make it a double** type compatible with the Shop API.
        double* y_ptr = &y[0][0];
        double** y_pptr = &y_ptr; // Type of argument x may already be double**, but in case it is std::array<std::array<double,m>,n> or std::vector<std::vector<double>> we need to make it a double** type compatible with the Shop API.
        CHECK(ShopSetXyArrayAttribute(shop, object_ix, attr_ix, n_dimensions, ref_ptr, n_points_ptr, x_pptr, y_pptr));
        if (check_exists)
            CHECK(ShopAttributeExists(shop, object_ix, attr_ix));
        CHECK(ShopCheckAttributeToGet(shop, attr_ix));
        int n_dimensions_get = INT_MIN;
        int n_points_max_get = INT_MIN;
        CHECK(ShopGetXyArrayDimensions(shop, object_ix, attr_ix, n_dimensions_get, n_points_max_get));
        CHECK(n_dimensions_get == n_dimensions);
        for (int i=0;i<n_dimensions;++i){
            CHECK(n_points[i] <= n_points_max_get);
        }
        double* ref_get = new double[n_dimensions_get];
        int* n_points_get = new int[n_dimensions_get];
        double** x_get = new double*[n_dimensions];
        double** y_get = new double*[n_dimensions];
        for (int i=0;i<n_dimensions;++i){
            x_get[i] = new double[n_points_max_get];
            y_get[i] = new double[n_points_max_get];
        }
        CHECK(ShopGetXyArrayAttribute(shop, object_ix, attr_ix, n_dimensions_get, ref_get, n_points_get, x_get, y_get));
        for (int i=0;i<n_dimensions;++i) {
            CHECK(ref_get[i] == ref[i]);
            CHECK(n_points_get[i] == n_points[i]);
            for (int j=0;j<n_points_get[i];++j) {
                CHECK(x[i][j] == x_get[i][j]);
                CHECK(y[i][j] == y_get[i][j]);
            }
        }
        delete[] ref_get;
        delete[] n_points_get;
        for (int i=0;i<n_dimensions;++i) {
            delete[] x_get[i];
            delete[] y_get[i];
        }
        delete[] x_get;
        delete[] y_get;
    }
    template<typename TArray, typename YArray>
    void SetGetCompareTimeSeries(const int object_ix, const char*  start_time_, const int n_time_steps,
        const char* attribute_, const int n_points, TArray t, YArray y, bool check_exists) const
    {
        // Argument type of auto parameters t and y must be some kind of arrays: t an array of ints, y a 2D array of doubles.
        // For example y can be declared as any of the following:
        //    double y[n][m];
        //    double** y;
        //    std::array<std::array<double,m>,n>;
        //    std::vector<std::vector<double>>;
        shop_attr_str attribute;strcpy(attribute,attribute_);
        shop_time_str start_time;strcpy(start_time,start_time_);
        constexpr int n_scen = 1; // We don't test with multiple scenarios, so always just one!
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute);
        if (check_exists)
            CHECK(!ShopAttributeExists(shop, object_ix, attr_ix));
        // Set as compressed break point data
        CHECK(ShopCheckAttributeToSet(shop, attr_ix));
        int* t_ptr = &t[0]; // Type of argument t may already be int*, but in case it is std::array<int,n> or std::vector<int> we need to make it a int* type compatible with the Shop API.
        double* y_ptr = &y[0][0];
        double** y_pptr = &y_ptr; // Type of argument y may already be double**, but in case it is std::array<std::array<double,m>,n> or std::vector<std::vector<double>> we need to make it a double** type compatible with the Shop API.
        CHECK(ShopSetTxyAttribute(shop, object_ix, attr_ix, start_time, n_points, n_scen, t_ptr, y_pptr));
        if (check_exists)
            CHECK(ShopAttributeExists(shop, object_ix, attr_ix));
        // Get as expanded complete series
        CHECK(ShopCheckAttributeToGet(shop, attr_ix));
        char start_time_get[18];
        int n_scen_get, n_points_get;
        CHECK(ShopGetTxyAttributeDimensions(shop, object_ix, attr_ix, start_time_get, n_points_get, n_scen_get));
        CHECK(strncmp(start_time_get, start_time, strlen(start_time)) == 0);
        CHECK(n_scen_get == n_scen);
        CHECK(n_points_get == n_time_steps); // NB: We get expanded, so one point for each time steps regardless of what we did set!
        int* t_get = new int[n_time_steps];
        double* y_get[n_scen];
        y_get[0] = new double[n_time_steps];
        double** y_get_pptr = &y_get[0];
        CHECK(ShopGetTxyAttribute(shop, object_ix, attr_ix, n_points_get, n_scen_get, t_get, y_get_pptr));
        // Verify that we really have gotten an expanded time axis
        for (int i=0;i<n_points_get;++i) {
            CHECK(t_get[i]==i);
        }
        // Compare the expanded time series data we got with the compressed break point data we set
        for (int get_i=0,set_i=0;get_i<n_points_get;++get_i){
            if (set_i<n_points-1) {
                if (get_i>=t[set_i+1]) {
                    ++set_i;
                }
            }
            if (std::isnan(y[0][set_i])) {
                CHECK(y_get[0][get_i] == 1e40); // Shop returns magic number 1e40 in points where we have set std::numeric_limits<double>::quiet_NaN()
            } else {
                CHECK(y_get[0][get_i]==y[0][set_i]);
            }
        }
        delete[] t_get;
        delete[] y_get[0];
    }
    template<typename TArray, typename YArray>
    void GetTimeSeries(const int object_ix, char* const /*start_time*/, const int n_time_steps,
        char* const attribute, TArray t, YArray y, bool check_exists) const
    {
        constexpr int n_scen = 1; // We don't test with multiple scenarios, so always just one!
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute);
        if (check_exists)
            CHECK(ShopAttributeExists(shop, object_ix, attr_ix));
        // Get as expanded complete series
        CHECK(ShopCheckAttributeToGet(shop, attr_ix));
        //TODO: ShopGetTxyAttributeDimensions crashes!
        //int n_scen_get, n_points_get;
        //CHECK(ShopGetTxyAttributeDimensions(shop, object_ix, attr_ix, start_time, n_points_get, n_scen_get));
        //CHECK(n_scen_get == n_scen);
        //CHECK(n_points_get == n_time_steps); // NB: We get expanded, so one point for each time steps regardless of what we did set!
        int* t_ptr = &t[0]; // Type of argument t may already be int*, but in case it is std::array<int,n> or std::vector<int> we need to make it a int* type compatible with the Shop API.
        double* y_ptr = &y[0][0];
        double** y_pptr = &y_ptr; // Type of argument y may already be double**, but in case it is std::array<std::array<double,m>,n> or std::vector<std::vector<double>> we need to make it a double** type compatible with the Shop API.
        CHECK(ShopGetTxyAttribute(shop, object_ix, attr_ix, n_time_steps, n_scen, t_ptr, y_pptr));
        // Verify that we really have gotten an expanded time axis
        for (int i=0;i<n_time_steps;++i) {
            CHECK(t[i]==i);
        }
    }
    template<typename TArray, typename YArray>
    void GetCheckResultSeries(const int object_ix, const char* start_time_, const int n_time_steps, const char* attribute_,
        const int n, TArray t, YArray y, bool check_exists) const
    {
        shop_time_str start_time;strcpy(start_time,start_time_);
        shop_attr_str attribute;strcpy(attribute,attribute_);
        // Get expanded result time series
        constexpr int n_scen = 1; // We don't test with multiple scenarios, so always just one!
        int* t_actual = new int[n_time_steps];
        double* y_actual[n_scen];
        y_actual[0] = new double[n_time_steps];
        double** y_actual_pptr = &y_actual[0];
        GetTimeSeries(object_ix, start_time, n_time_steps, attribute, t_actual, y_actual_pptr, check_exists);
        // Check the value at specific points
        for (int i=0;i<n;++i) {
            CHECK(t_actual[t[i]] == t[i]); // We assume that the result time series is expanded (and if not the current test have already been marked as failed since GetTimeSeries checks that t[i]==i for every value of i)
            CHECK(y_actual[0][t[i]] == doctest::Approx(y[0][i]));
        }
        delete[] t_actual;
        delete[] y_actual[0];
    };

};
