#include "doctest/doctest.h"
#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/time/utctime_utilities.h>
#include <memory>
#include <vector>
#include <string>

// For debug output from tests:
// - define DEBUG_TESTS (uncomment)
// - modify debug flags within the "#ifdef DEBUG_TESTS" section only
// This will issue a message in the build output, to hopefully remember not to commit with debug flags enabled.
//#define DEBUG_TESTS
#ifdef DEBUG_TESTS
#pragma message("*** Remember to undefine DEBUG_TESTS before commit ***")
static const bool shop_console_output = false;
static const bool shop_log_files = false;
#else
static const bool shop_console_output = false;
static const bool shop_log_files = false;
#endif

TEST_SUITE("stm_shop_adapter_basics")
{

TEST_CASE("getters")
{
    using std::string;
    using namespace std::string_literals;
    using namespace shyft::energy_market;
    using namespace shyft::energy_market::stm;
    using namespace shyft::energy_market::stm::shop;
    using shyft::core::utctime;
    using shyft::core::utcperiod;

    const shyft::core::calendar calendar("Europe/Oslo");
    const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    const auto make_constant_ts = [&t_begin, &t_end](double value) { return apoint_ts(shyft::time_series::dd::gta_t(t_begin, t_end - t_begin, 1), value, POINT_AVERAGE_VALUE); };

    SUBCASE("basic")
    {
        const double v_dbl = 1.23;
        CHECK(shop_adapter::exists(v_dbl));
        CHECK(shop_adapter::valid(v_dbl));
        CHECK(shop_adapter::get(v_dbl) == v_dbl);
        const int v_int = 123;
        CHECK(shop_adapter::exists(v_int));
        CHECK(shop_adapter::valid(v_int));
        CHECK(shop_adapter::get(v_int) == v_int);
        const utctime v_time = shyft::core::utctime_now();
        CHECK(shop_adapter::exists(v_time));
        CHECK(shop_adapter::valid(v_time));
        CHECK(shop_adapter::get(v_time) == v_time);
        const auto v_ts = make_constant_ts(100.0);
        CHECK(shop_adapter::exists(v_ts));
        CHECK(shop_adapter::valid(v_ts));
        CHECK(shop_adapter::get(v_ts) == v_ts);
    }
    SUBCASE("temporal ts")
    {
        const double ts_value = 100.0;
        const auto ts_empty = apoint_ts();
        CHECK(shop_adapter::exists(ts_empty) == false);
        CHECK(shop_adapter::valid(ts_empty) == false); // Note: Treated as a regular non-temporal ts value in this case
        CHECK(shop_adapter::valid_temporal(ts_empty, t_begin) == false); // Temporal value not valid at specified time
        CHECK_THROWS(shop_adapter::get(ts_empty, t_begin)); // Attribute is empty
        const auto ts_const = make_constant_ts(ts_value);
        CHECK(shop_adapter::exists(ts_const) == true);
        CHECK(shop_adapter::valid(ts_const) == true); // Note: Valid, because it is treated as a regular non-temporal ts value in this case
        CHECK(shop_adapter::valid_temporal(ts_const, t_begin) == true);
        CHECK(shop_adapter::get(ts_const, t_begin) == ts_value);
        auto t = calendar.add(t_end, calendar.SECOND, -1);
        CHECK(shop_adapter::valid_temporal(ts_const, t) == true);
        CHECK(shop_adapter::get(ts_const, t) == ts_value);
        t = calendar.add(t_begin, calendar.SECOND, -1);
        CHECK(shop_adapter::valid_temporal(ts_const, t) == false);
        CHECK_THROWS(shop_adapter::get(ts_const, t)); // Attribute is not valid at t
        CHECK(shop_adapter::valid_temporal(ts_const, t_end) == false);
        CHECK_THROWS(shop_adapter::get(ts_const, t_end)); // Attribute is not valid at t
    }
    SUBCASE("temporal xy")
    {
        const auto txy = make_shared<map<utctime, xy_point_curve_>>();
        CHECK(shop_adapter::exists(txy) == false);
        CHECK(shop_adapter::valid_temporal(txy, t_begin) == false);
        CHECK_THROWS(shop_adapter::get(txy, t_begin)); // Attribute is empty
        const auto xy = make_shared<hydro_power::xy_point_curve>(
            std::vector<double>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
            std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 });
        txy.get()->emplace(t_begin, xy);
        CHECK(shop_adapter::exists(txy) == true);
        CHECK(shop_adapter::valid_temporal(txy, t_begin) == true);
        CHECK(shop_adapter::get(txy, t_begin) == xy);
        auto t = calendar.add(t_end, calendar.SECOND, -1);
        CHECK(shop_adapter::valid_temporal(txy, t) == true);
        CHECK(shop_adapter::get(txy, t) == xy);
        t = calendar.add(t_begin, calendar.SECOND, -1);
        CHECK(shop_adapter::valid_temporal(txy, t) == false);
        CHECK_THROWS(shop_adapter::get(txy, t)); // Attribute is not valid at t
        CHECK(shop_adapter::valid_temporal(txy, t_end) == true);
        CHECK(shop_adapter::get(txy, t_end) == xy); // Note: In contrast to temporal ts, this is valid at t_end and beyond since the validity period is open ended!
    }
    SUBCASE("stm attribute")
    {
        int id = 1;
        stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
        stm_hps_builder builder(hps);
        reservoir_ rsv_stm = builder.create_reservoir(id++, "reservoir"s, ""s);

        // stm ts attribute
        CHECK(shop_adapter::exists(rsv_stm->inflow.schedule) == false);
        CHECK(shop_adapter::valid(rsv_stm->inflow.schedule) == false); // Note: Treated as a regular non-temporal ts value in this case
        const double ts_value = 100.0;
        const auto ts_empty = apoint_ts();
        rsv_stm->inflow.schedule = ts_empty;
        CHECK(shop_adapter::exists(rsv_stm->inflow.schedule) == false);
        CHECK(shop_adapter::valid(rsv_stm->inflow.schedule) == false); // Note: Treated as a regular non-temporal ts value in this case
        CHECK(shop_adapter::get(rsv_stm->inflow.schedule) == ts_empty);
        const auto ts_const = make_constant_ts(ts_value);
        rsv_stm->inflow.schedule = ts_const;
        CHECK(shop_adapter::exists(rsv_stm->inflow.schedule) == true);
        CHECK(shop_adapter::valid(rsv_stm->inflow.schedule) == true); // Note: Valid, because it is treated as a regular non-temporal ts value in this case
        CHECK(shop_adapter::get(rsv_stm->inflow.schedule) == ts_const);

        // stm temporal ts attribute
        rsv_stm->level.regulation_max = ts_empty;
        CHECK(shop_adapter::get(rsv_stm->level.regulation_max) == ts_empty);
        CHECK(shop_adapter::exists(rsv_stm->level.regulation_max) == false);
        CHECK(shop_adapter::valid(rsv_stm->level.regulation_max) == false); // Note: Treated as a regular non-temporal ts value in this case
        CHECK(shop_adapter::valid_temporal(rsv_stm->level.regulation_max, t_begin) == false);
        CHECK_THROWS(shop_adapter::get(rsv_stm->level.regulation_max, t_begin)); // Attribute is empty
        rsv_stm->level.regulation_max = ts_const;
        CHECK(shop_adapter::exists(rsv_stm->level.regulation_max) == true);
        CHECK(shop_adapter::valid(rsv_stm->level.regulation_max) == true); // Note: Valid, because it is treated as a regular non-temporal ts value in this case
        CHECK(shop_adapter::valid_temporal(rsv_stm->level.regulation_max, t_begin) == true);
        CHECK(shop_adapter::get(rsv_stm->level.regulation_max, t_begin) == ts_value);
        auto t = calendar.add(t_end, calendar.SECOND, -1);
        CHECK(shop_adapter::valid_temporal(rsv_stm->level.regulation_max, t) == true);
        CHECK(shop_adapter::get(rsv_stm->level.regulation_max, t) == ts_value);
        t = calendar.add(t_begin, calendar.SECOND, -1);
        CHECK(shop_adapter::valid_temporal(rsv_stm->level.regulation_max, t) == false);
        CHECK_THROWS(shop_adapter::get(rsv_stm->level.regulation_max, t)); // Attribute is not valid at t
        CHECK(shop_adapter::valid_temporal(rsv_stm->level.regulation_max, t_end) == false);
        CHECK_THROWS(shop_adapter::get(rsv_stm->level.regulation_max, t_end)); // Attribute is not valid at t

        // stm temporal xy attribute
        rsv_stm->volume_level_mapping = make_shared<map<utctime, xy_point_curve_>>();
        CHECK_THROWS(shop_adapter::get(rsv_stm->volume_level_mapping, t_begin)); // Attribute is empty
        const auto xy = make_shared<hydro_power::xy_point_curve>(
            std::vector<double>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
            std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 });
        rsv_stm->volume_level_mapping.get()->emplace(t_begin, xy);
        CHECK(shop_adapter::valid_temporal(rsv_stm->volume_level_mapping, t_begin) == true);
        CHECK(shop_adapter::get(rsv_stm->volume_level_mapping, t_begin) == xy);
        t = calendar.add(t_end, calendar.SECOND, -1);
        CHECK(shop_adapter::valid_temporal(rsv_stm->volume_level_mapping, t) == true);
        CHECK(shop_adapter::get(rsv_stm->volume_level_mapping, t) == xy);
        t = calendar.add(t_begin, calendar.SECOND, -1);
        CHECK(shop_adapter::valid_temporal(rsv_stm->volume_level_mapping, t) == false);
        CHECK_THROWS(shop_adapter::get(rsv_stm->volume_level_mapping, t)); // Attribute is not valid at t
        CHECK(shop_adapter::valid_temporal(rsv_stm->volume_level_mapping, t_end) == true);
        CHECK(shop_adapter::get(rsv_stm->volume_level_mapping, t_end) == xy);  // Note: In contrast to temporal ts, this is valid at t_end and beyond since the validity period is open ended!
    }
}

TEST_CASE("setters")
{
    using std::string;
    using namespace std::string_literals;
    using namespace shyft::energy_market;
    using namespace shyft::energy_market::stm;
    using namespace shyft::energy_market::stm::shop;
    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::core::utctimespan;
    using shyft::core::to_seconds64;
    using shyft::time_axis::point_dt;
    using shyft::core::deltahours;
    using shyft::core::deltaminutes;


    const shyft::core::calendar calendar("Europe/Oslo");
    const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    const auto t_step = shyft::core::deltahours(1);
    const size_t n_step=18;
    const shyft::time_axis::generic_dt ta{t_begin,t_step,n_step};

    const utcperiod t_period{ ta.total_period()};
    const auto t_end = t_period.end;
    const auto make_constant_ts = [](utcperiod t_period, double value) { return apoint_ts(shyft::time_series::dd::gta_t(t_period.start, t_period.timespan(), 1), value, POINT_AVERAGE_VALUE); };
    const auto make_ts = [&t_period, &make_constant_ts](double value) { return make_constant_ts(t_period, value); };


    int id = 1;

    stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    reservoir_ rsv_stm = builder.create_reservoir(id++, "reservoir"s, ""s);
    shop_system api{ ta };
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    //auto rsv_shop = api.api.create<shop_reservoir>(rsv_stm->name);

    SUBCASE("basic")
    {
        const double v_dbl_src = 1.23;
        double v_dbl_dst = -999.0;
        CHECK(api.adapter.valid_to_set(v_dbl_dst, v_dbl_src));
        CHECK(api.adapter.set(v_dbl_dst, v_dbl_src) == v_dbl_dst);
        CHECK(v_dbl_dst == v_dbl_src);
        const int v_int_src = 123;
        int v_int_dst = -999;
        CHECK(api.adapter.valid_to_set(v_int_dst, v_int_src));
        CHECK(api.adapter.set(v_int_dst, v_int_src) == v_int_dst);
        CHECK(v_int_dst == v_int_src);
        const utctime v_time_src = shyft::core::utctime_now();
        utctime v_time_dst = shyft::core::no_utctime;
        CHECK(api.adapter.valid_to_set(v_time_dst, v_time_src));
        CHECK(api.adapter.set(v_time_dst, v_time_src) == v_time_dst);
        CHECK(v_time_dst == v_time_src);
        const double ts_value = 100.0;
        const auto v_ts_src = make_ts(ts_value);
        auto v_ts_dst = apoint_ts();
        CHECK(api.adapter.valid_to_set(v_ts_dst, v_ts_src));
        CHECK(api.adapter.set(v_ts_dst, v_ts_src) == v_ts_dst);
        CHECK(v_ts_dst == v_ts_src);
        v_dbl_dst = -999.0;
        CHECK(api.adapter.valid_to_set(v_dbl_dst, v_ts_src));
        CHECK(api.adapter.set(v_dbl_dst, v_ts_src) == v_dbl_dst);
        CHECK(v_dbl_dst == ts_value);
    }

    SUBCASE("basic stm attribute not exists")
    {
        const double v_init = -999.0;
        double v = v_init;
        CHECK(api.adapter.valid_to_set(v, rsv_stm->level.regulation_min) == false);
        CHECK_THROWS(api.adapter.set(v, rsv_stm->level.regulation_min)); // Throws: Attempt to read not-yet-set attribute for object
        api.adapter.set_if(v, rsv_stm->level.regulation_min); // Does nothing, and not throwing, because attribute does not exist
        CHECK(v == v_init); // Still unchanged
        CHECK(api.adapter.set_or(v, rsv_stm->level.regulation_min, 1.23) == v); // Sets default because attribute does not exist
        CHECK(v == 1.23);
        v = v_init;
        CHECK(api.adapter.set_optional(v, rsv_stm->level.regulation_min) == v); // Does not throw, does nothing, because attribute does not exist
        CHECK(v == v_init); // Still unchanged
        v = v_init;
        CHECK(api.adapter.set_required(v, rsv_stm->level.regulation_min, 1.23) == v); // Sets default value because attribute does not exist
        CHECK(v == 1.23); // New default value set
    }

    SUBCASE("stm attribute exists and valid")
    {
        const double ts_value = 100.0;
        rsv_stm->level.regulation_min = make_ts(ts_value);
        const double v_init = -999.0;
        double v = v_init;
        CHECK(api.adapter.valid_to_set(v, rsv_stm->level.regulation_min) == true);
        CHECK(api.adapter.set(v, rsv_stm->level.regulation_min) == v);
        CHECK(v == ts_value);
        v = v_init;
        CHECK(api.adapter.set_if(v, rsv_stm->level.regulation_min) == v);
        CHECK(v == ts_value);
        v = v_init;
        CHECK(api.adapter.set_or(v, rsv_stm->level.regulation_min, 1.23) == v);
        CHECK(v == ts_value);
        v = v_init;
        CHECK(api.adapter.set_optional(v, rsv_stm->level.regulation_min) == v);
        CHECK(v == ts_value);
        v = v_init;
        CHECK(api.adapter.set_required(v, rsv_stm->level.regulation_min) == v);
        CHECK(v == ts_value);
        v = v_init;
    }

    SUBCASE("stm attribute exists but not valid")
    {
        const double ts_value = 100.0;
        rsv_stm->level.regulation_min = make_constant_ts({t_end, calendar.add(t_end, calendar.DAY, 1)}, ts_value); // Temporal value that is valid only after the api period
        const double v_init = -999.0;
        double v = v_init;
        CHECK(api.adapter.valid_to_set(v, rsv_stm->level.regulation_min) == false);
        CHECK_THROWS(api.adapter.set(v, rsv_stm->level.regulation_min)); // Throws: Attribute is not valid at t
        CHECK(v == v_init);
        CHECK_THROWS(api.adapter.set_if(v, rsv_stm->level.regulation_min)); // Throws: Attribute is not valid at t
        CHECK(v == v_init);
        v = v_init;
        CHECK(api.adapter.set_if(v, rsv_stm->level.regulation_min, 1.23) == v); // Set 1.23 since there is no valid value (not throwing since attribute exists)
        CHECK(v == 1.23);
        v = v_init;
        CHECK_THROWS(api.adapter.set_or(v, rsv_stm->level.regulation_min, 1.23)); // Throws: Attribute is not valid at t
        CHECK(v == v_init);
        v = v_init;
        CHECK(api.adapter.set_or(v, rsv_stm->level.regulation_min, 1.23, 4.56) == v); // Set 4.56 since there is no valid value (not setting 1.23 since attribute exists)
        CHECK(v == 4.56);
        v = v_init;
        CHECK(api.adapter.set_optional(v, rsv_stm->level.regulation_min) == v); // Does nothing since attribute is not valid at t
        CHECK(v == v_init);
        v = v_init;
        CHECK(api.adapter.set_optional(v, rsv_stm->level.regulation_min, 1.23) == v); // Does nothing since attribute is not valid at t (not setting 1.23 since attribute exists)
        CHECK(v == v_init);
        v = v_init;
        CHECK(api.adapter.set_optional(v, rsv_stm->level.regulation_min, 1.23, 4.56) == v); // Set 4.56 since there is no valid value (not setting 1.23 since attribute exists)
        CHECK(v == 4.56);
        v = v_init;
        CHECK_THROWS(api.adapter.set_required(v, rsv_stm->level.regulation_min));  // Throws: Attribute is not valid at t
        CHECK(v == v_init);
        v = v_init;
        CHECK(api.adapter.set_required(v, rsv_stm->level.regulation_min, 1.23) == v); // Sets 1.23 since attribute exists but is not valid at t, and no additional default is given
        CHECK(v == 1.23);
        v = v_init;
        CHECK(api.adapter.set_required(v, rsv_stm->level.regulation_min, 1.23, 4.56) == v); // Sets 4.56 since attribute exists but is not valid at t, not setting 1.23 since this is now only for the not exists case
        CHECK(v == 4.56);
        v = v_init;
    }

    SUBCASE("stm attribute exists valid nan")
    {
        const double ts_value = shyft::nan;
        rsv_stm->level.regulation_min = make_ts(ts_value);
        const double v_init = -999.0;
        double v = v_init;
        CHECK(api.adapter.valid_to_set(v, rsv_stm->level.regulation_min) == true);
        CHECK(api.adapter.set_or(v, rsv_stm->level.regulation_min, 1.23, 4.56, 7.89) == v); // Set 7.89 since there is a valid value but it is nan
        CHECK(v == 7.89);
        v = v_init;
        CHECK(api.adapter.set_optional(v, rsv_stm->level.regulation_min, 1.23, 4.56, 7.89) == v); // Set 7.89 since there is a valid value but it is nan
        CHECK(v == 7.89);
        v = v_init;
        CHECK(api.adapter.set_required(v, rsv_stm->level.regulation_min, 1.23, 4.56, 7.89) == v); // Set 7.89 since there is a valid value but it is nan
        CHECK(v == 7.89);
        v = v_init;
    }

} // TEST_CASE
} // TEST_SUITE

TEST_SUITE("stm_shop_adapter_to_shop")
{
    // Aggregation of tests checking that 'adapter::to_shop' functions creates SHOP objects that
    // has SHOP attributes set as expected.

    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::gta_t;
    using shyft::time_axis::fixed_dt;
    using shyft::time_series::POINT_AVERAGE_VALUE;
    using namespace shyft::energy_market::stm;
    using namespace shyft::energy_market::stm::shop;

    using shyft::energy_market::hydro_power::point;
    using xyz_list = std::vector<xy_point_curve_with_z>;
    using t_xyz_list = std::map<utctime, std::shared_ptr<xyz_list>>;

    const auto t_step = shyft::core::deltahours(1);
    const gta_t time_axis{shyft::core::create_from_iso8601_string("2020-01-01T00:00:00Z"),t_step,24};
    const utcperiod t_period=time_axis.total_period();

    auto create_constant_t_double = [] (double val) -> apoint_ts {
        auto dt = t_period.end - t_period.start + shyft::core::deltahours(1);
        auto ta = fixed_dt(t_period.start, dt, 1);
        return apoint_ts(ta, val, POINT_AVERAGE_VALUE);
    };

    auto create_constant_ts = [] (double val) -> apoint_ts {
        int num_step = (t_period.end - t_period.start) / t_step;
        auto ta = gta_t(t_period.start, t_step, num_step);
        return apoint_ts(ta, val, POINT_AVERAGE_VALUE);
    };

    auto create_constant_t_xy = [] (double x, double y) -> t_xy_ {
        auto m = make_shared<t_xy_::element_type>();
        auto xy = std::make_shared<xy_point_curve>();
        xy->points.emplace_back(x, y);
        m->emplace(t_period.start, xy);
        return m;
    };

    auto create_t_xy = [] (std::initializer_list<std::pair<double, double>> v) -> t_xy_ {
        auto m = make_shared<t_xy_::element_type>();
        auto xy = std::make_shared<xy_point_curve>();
        for (auto it = v.begin(); it != v.end(); ++it)
            xy->points.emplace_back(it->first, it->second);
        m->emplace(t_period.start, xy);
        return m;
    };

    auto create_constant_t_xyz = [] (double x, double y, double z) -> t_xyz_list_ {
        auto m = make_shared<t_xyz_list_::element_type>();
        xy_point_curve xy {{point{x, y}}};
        auto xyz = std::make_shared<std::vector<xy_point_curve_with_z>>();
        xyz->emplace_back(xy, z);
        m->emplace(t_period.start, xyz);
        return m;
    };

    auto create_shop_system = [] (stm_hps_ hps) -> shared_ptr<shop_system> {
        // wrap the hps in a dummy stm system
        auto mdl = make_shared<stm_system>(0, "dummy_stm_system"s, ""s);
        auto mkt = make_shared<energy_market_area>(0, "dummy_stm_market"s, ""s, mdl);
        mdl->hps.push_back(hps);
        mdl->market.push_back(mkt);

        // creating the shop system calls ShopInit
        auto s = make_shared<shop_system>(time_axis);
        return s;
    };

    TEST_CASE("shop_gate")
    {

        SUBCASE("shop_gate_from_stm_waterway_no_attributes")
        {
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);

            CHECK_FALSE(sg.max_discharge.exists());
        }

        SUBCASE("shop_gate_from_stm_waterway_basic_attributes")
        {
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            wtr->discharge.static_max = create_constant_t_double(10.0);
            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);

            CHECK(sg.max_discharge.exists());
        }

        // NOTE: It seems that SHOP api fails does not correcctly evaluate ShopAttributeExists
        // for these attributes:
        //   - schedule_m3s
        //   - schedule_percent
        //   - functions_meter_m3s
        SUBCASE("shop_gate_from_stm_waterway_with_gate_sets_basic_attributes")
        {
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto gt = builder.create_gate(2, "gate name", ""s);
            waterway::add_gate(wtr, gt);

            wtr->discharge.static_max = create_constant_t_double(10.0);
            gt->flow_description = create_constant_t_xyz(10.0, 20.0, 30.0);

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, gt.get());

            CHECK(sg.max_discharge.exists());
            CHECK(sg.functions_meter_m3s.get().size());  // Check exist does not work
        }

        SUBCASE("shop_gate_from_stm_waterway_with_gate_sets_basic_attributes_with_deltameters")
        {
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto gt = builder.create_gate(2, "gate name", ""s);
            waterway::add_gate(wtr, gt);

            wtr->discharge.static_max = create_constant_t_double(10.0);
            gt->flow_description_delta_h = create_constant_t_xyz(0.0, -10.0, 50.0);

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, gt.get());

            CHECK(sg.max_discharge.exists());
            CHECK(sg.functions_deltameter_m3s.get().size());  // Check exist does not work
        }


        SUBCASE("shop_gate_from_stm_waterway_with_gate_optimise_sets_discharge_schedule")
        {
            // Check that if both 'discharge.schedule' and 'opening.schedule' STM attributes
            // are set, then the 'schedule_m3s' SHOP attribute is set on the SHOP gate.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto gt = builder.create_gate(2, "gate name", ""s);
            waterway::add_gate(wtr, gt);

            gt->discharge.schedule = create_constant_ts(10.0);
            gt->opening.schedule = create_constant_ts(100.0);  // should be ignored by adapter, discharge takes precedence

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, gt.get());

            CHECK(sg.schedule_m3s.get().size());  // Check exist does not work
        }

        SUBCASE("shop_gate_from_stm_waterway_with_gate_sets_opening_schedule")
        {
            // Check that if only 'opening.schedule' STM attribute is set, then
            // the 'schedule_percent' SHOP attribute is set on the SHOP gate.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto gt = builder.create_gate(2, "gate name", ""s);
            waterway::add_gate(wtr, gt);

            gt->opening.schedule = create_constant_ts(100.0);
            gt->discharge.realised = create_constant_ts(10.0);  // should be ignored by adapter, schedule takes precedence

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, gt.get());

            CHECK(sg.schedule_percent.get().size());  // Check exist does not work
        }

        SUBCASE("shop_gate_from_stm_waterway_with_gate_optimise_sets_discharge_realised")
        {
            // Check that if both 'discharge.realised' and 'opening.realised' STM attributes
            // are set, then the 'schedule_m3s' SHOP attribute is set on the SHOP gate.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto gt = builder.create_gate(2, "gate name", ""s);
            waterway::add_gate(wtr, gt);

            gt->discharge.realised = create_constant_ts(10.0);
            gt->opening.realised = create_constant_ts(100.0);

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, gt.get());

            CHECK(sg.schedule_m3s.get().size());  // Check exist does not work
        }

        SUBCASE("shop_gate_from_stm_waterway_with_gate_sets_opening_realised")
        {
            // Check that if only 'opening.realised' STM attribute is set, then
            // the 'schedule_percent' SHOP attribute is set on the SHOP gate.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto gt = builder.create_gate(2, "gate name", ""s);
            waterway::add_gate(wtr, gt);

            gt->opening.realised = create_constant_ts(100.0);

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, gt.get());

            CHECK(sg.schedule_percent.get().size());  // exists does not work
        }

        SUBCASE("shop_gate_from_stm_waterway_and_gate_not_connected_throws")
        {
            // Check that 'to_shop_gate' throws if the gate does not belong to the waterway.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto gt = builder.create_gate(1, "gate name", ""s);  // not linked to waterway

            auto shop_sys = create_shop_system(hps);
            CHECK_THROWS(shop_sys->adapter.to_shop_gate(*wtr, gt.get()));
        }
    } // TESTCASE

    TEST_CASE("shop_gate_delay")
    {
        // Note: Time delay unit assumed to be the finest possible fixed resolution,
        // currently minutes, instead of the default which is according to timestep length!
        // Configured by shop_system!
        const utctime time_delay_unit = shyft::core::utctime_from_seconds64(60);

        SUBCASE("shop_gate_no_delay")
        {
            // No delay (base test)
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK_FALSE(sg.shape_discharge.exists());
        }
        SUBCASE("shop_gate_delay_single_point_x_is_zero")
        {
            // 1 point X==0.0 means no time delay.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            wtr->delay = create_constant_t_xy(0.0, 1.0);

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK_FALSE(sg.shape_discharge.exists());
        }
        SUBCASE("shop_gate_delay_two_points_first_x_and_y_is_zero")
        {
            // 2 points where X(0)==0.0 and Y(0)==0.0 means constant time delay in
            // format valid as shape_discharge, so adapter could map it to either
            // of them - currently time_delay.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto delay_secs = shyft::core::utctime_from_seconds64(3600);
            wtr->delay = create_t_xy({
                {0.0, 0.0},
                {shyft::core::to_seconds(delay_secs), 1.0}
            });

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);
            CHECK(sg.time_delay.exists());
            CHECK_EQ(sg.time_delay.get(), delay_secs/time_delay_unit);
            CHECK_FALSE(sg.shape_discharge.exists());
        }
        SUBCASE("shop_gate_delay_two_points_first_x_but_not_y_is_zero")
        {
            // 2 points where X(0)==0.0 and Y(0)>0.0, means some amount of water
            // comes without delay and rest with constant delay, format is valid
            // as shape_discharge.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto delay_secs = shyft::core::utctime_from_seconds64(3600);
            wtr->delay = create_t_xy({
                {0.0, 0.5},
                {shyft::core::to_seconds(delay_secs), 1.0}
            });

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK(sg.shape_discharge.exists());
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points.size(), 2);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].x, 0.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].y, 0.5);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].x, shyft::core::to_seconds(delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].y, 1.0);
        }
        SUBCASE("shop_gate_delay_single_point_x_is_not_zero")
        {
            // 1 point X>0.0 means constant time delay in format invalid as
            // shape_discharge but mapped to time_delay by adapter.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto delay_secs = shyft::core::utctime_from_seconds64(3600);
            wtr->delay = create_constant_t_xy(shyft::core::to_seconds(delay_secs), 1.0);

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);
            CHECK(sg.time_delay.exists());
            CHECK_EQ(sg.time_delay.get(), delay_secs/time_delay_unit);
            CHECK_FALSE(sg.shape_discharge.exists());
        }

        SUBCASE("shop_gate_delay_two_points_first_x_is_not_zero")
        {
            // 2 points where X(0)>0.0 means constant time delay in format invalid as
            // shape_discharge but mapped to time delay (offset) + shape discharge (wave)
            // by adapter.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto offset_secs = shyft::core::utctime_from_seconds64(3600);
            auto delay_secs = shyft::core::utctime_from_seconds64(7200);
            wtr->delay = create_t_xy({
                {shyft::core::to_seconds(offset_secs), 0.5},
                {shyft::core::to_seconds(delay_secs), 1.0}
            });

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);
            CHECK(sg.time_delay.exists());
            CHECK_EQ(sg.time_delay.get(), offset_secs/time_delay_unit);
            CHECK(sg.shape_discharge.exists());
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points.size(), 2);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].x, 0.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].y, 0.5);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].x, shyft::core::to_seconds(delay_secs-offset_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].y, 1.0);
        }
        SUBCASE("shop_gate_delay_multi_point_standard_wave_shape")
        {
            // Standard shape_discharge (wave).
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto base_delay_secs = shyft::core::utctime_from_seconds64(3600);
            wtr->delay = create_t_xy({
                {0.0, 0.0},
                {shyft::core::to_seconds(2*base_delay_secs), 1.0},
                {shyft::core::to_seconds(4*base_delay_secs), 2.0},
                {shyft::core::to_seconds(6*base_delay_secs), 2.0},
                {shyft::core::to_seconds(8*base_delay_secs), 1.0}
            });

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK(sg.shape_discharge.exists());
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points.size(), 5);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].x, 0.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].y, 0.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].x, shyft::core::to_seconds(2*base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].y, 1.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[2].x, shyft::core::to_seconds(4*base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[2].y, 2.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[3].x, shyft::core::to_seconds(6*base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[3].y, 2.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[4].x, shyft::core::to_seconds(8*base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[4].y, 1.0);
        }

        SUBCASE("shop_gate_delay_set_constant_when_constant_already_set")
        {
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);

            // Start by creating gate object without delay
            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK_FALSE(sg.shape_discharge.exists());

            // Set constant delay and update: Result should be only time_delay attribute.
            auto constant_delay_secs = shyft::core::utctime_from_seconds64(3600);
            wtr->delay = create_t_xy({
                {0.0, 0.0},
                {shyft::core::to_seconds(constant_delay_secs), 1.0}
            });
            shop_sys->adapter.set_shop_time_delay(sg, wtr->delay);
            CHECK(sg.time_delay.exists());
            CHECK_EQ(sg.time_delay.get(), constant_delay_secs/time_delay_unit);
            CHECK_FALSE(sg.shape_discharge.exists());

            // Set a different constant and try to update
            auto shape_base_delay_secs = shyft::core::utctime_from_seconds64(3600);
            wtr->delay = create_t_xy({
                {0.0, 0.0},
                {shyft::core::to_seconds(2*constant_delay_secs), 1.0}
            });
            // Should be no change, since it was already set
            shop_sys->adapter.set_shop_time_delay(sg, wtr->delay);
            CHECK(sg.time_delay.exists());
            CHECK_EQ(sg.time_delay.get(), constant_delay_secs/time_delay_unit);
            CHECK_FALSE(sg.shape_discharge.exists());
        }

        SUBCASE("shop_gate_delay_set_shape_when_constant_already_set")
        {
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);

            // Start by creating gate object without delay
            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK_FALSE(sg.shape_discharge.exists());

            // Set constant delay and update: Result should be only time_delay attribute.
            auto constant_delay_secs = shyft::core::utctime_from_seconds64(3600);
            wtr->delay = create_t_xy({
                {0.0, 0.0},
                {shyft::core::to_seconds(constant_delay_secs), 1.0}
            });
            shop_sys->adapter.set_shop_time_delay(sg, wtr->delay);
            CHECK(sg.time_delay.exists());
            CHECK_EQ(sg.time_delay.get(), constant_delay_secs/time_delay_unit);
            CHECK_FALSE(sg.shape_discharge.exists());

            // Set shape discharge and try to update
            auto shape_base_delay_secs = shyft::core::utctime_from_seconds64(3600);
            wtr->delay = create_t_xy({
                {0.0, 0.0},
                {shyft::core::to_seconds(2*shape_base_delay_secs), 1.0},
                {shyft::core::to_seconds(4*shape_base_delay_secs), 2.0},
                {shyft::core::to_seconds(6*shape_base_delay_secs), 2.0},
                {shyft::core::to_seconds(8*shape_base_delay_secs), 1.0}
            });
            // Should be no change, since it was already set
            shop_sys->adapter.set_shop_time_delay(sg, wtr->delay);
            CHECK(sg.time_delay.exists());
            CHECK_EQ(sg.time_delay.get(), constant_delay_secs/time_delay_unit);
            CHECK_FALSE(sg.shape_discharge.exists());
        }

        SUBCASE("shop_gate_delay_set_constant_when_shape_already_set")
        {
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);

            // Start by creating gate object without delay
            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK_FALSE(sg.shape_discharge.exists());

            // Set shape delay and update: Result should be only shape_discharge attribute.
            auto shape_base_delay_secs = shyft::core::utctime_from_seconds64(3600);
            wtr->delay = create_t_xy({
                {0.0, 0.0},
                {shyft::core::to_seconds(2*shape_base_delay_secs), 1.0},
                {shyft::core::to_seconds(4*shape_base_delay_secs), 2.0},
                {shyft::core::to_seconds(6*shape_base_delay_secs), 2.0},
                {shyft::core::to_seconds(8*shape_base_delay_secs), 1.0}
            });
            shop_sys->adapter.set_shop_time_delay(sg, wtr->delay);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK(sg.shape_discharge.exists());

            // Set constant and try to update
            auto constant_delay_secs = shyft::core::utctime_from_seconds64(3600);
            wtr->delay = create_t_xy({
                {0.0, 0.0},
                {shyft::core::to_seconds(constant_delay_secs), 1.0}
            });
            // Should be no change, since it was already set
            shop_sys->adapter.set_shop_time_delay(sg, wtr->delay);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK(sg.shape_discharge.exists());
        }

        SUBCASE("shop_gate_delay_set_shape_when_shape_already_set")
        {
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);

            // Start by creating gate object without delay
            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK_FALSE(sg.shape_discharge.exists());

            // Set shape delay and update: Result should be only shape_discharge attribute.
            auto shape_base_delay_secs = shyft::core::utctime_from_seconds64(3600);
            wtr->delay = create_t_xy({
                {0.0, 0.0},
                {shyft::core::to_seconds(2*shape_base_delay_secs), 1.0},
                {shyft::core::to_seconds(4*shape_base_delay_secs), 2.0},
                {shyft::core::to_seconds(6*shape_base_delay_secs), 2.0},
                {shyft::core::to_seconds(8*shape_base_delay_secs), 1.0}
            });
            shop_sys->adapter.set_shop_time_delay(sg, wtr->delay);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK(sg.shape_discharge.exists());
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points.size(), 5);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].x, 0.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].y, 0.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].x, shyft::core::to_seconds(2*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].y, 1.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[2].x, shyft::core::to_seconds(4*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[2].y, 2.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[3].x, shyft::core::to_seconds(6*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[3].y, 2.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[4].x, shyft::core::to_seconds(8*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[4].y, 1.0);

            // Set shape discharge and try to update
            wtr->delay = create_t_xy({
                {0.0, 0.0},
                {shyft::core::to_seconds(2*2*shape_base_delay_secs), 2*1.0},
                {shyft::core::to_seconds(2*4*shape_base_delay_secs), 2*2.0},
                {shyft::core::to_seconds(2*6*shape_base_delay_secs), 2*2.0},
                {shyft::core::to_seconds(2*8*shape_base_delay_secs), 2*1.0}
            });
            // Should be no change, since it was already set
            shop_sys->adapter.set_shop_time_delay(sg, wtr->delay);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK(sg.shape_discharge.exists());
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points.size(), 5);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].x, 0.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].y, 0.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].x, shyft::core::to_seconds(2*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].y, 1.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[2].x, shyft::core::to_seconds(4*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[2].y, 2.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[3].x, shyft::core::to_seconds(6*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[3].y, 2.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[4].x, shyft::core::to_seconds(8*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[4].y, 1.0);
        }
    }

#if 0
// TODO: Test for configurable overwrite/replace of existing time delay,
//       but currently not supported...
        SUBCASE("shop_gate_delay_overwrite")
        {
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);

            // Start by creating gate object without delay
            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK_FALSE(sg.shape_discharge.exists());

            // Set constant delay and update: Result should be only time_delay attribute.
            bool overwrite = false; // No overwrite, but should be set since nothing set before
            auto constant_delay_secs = shyft::core::utctime_from_seconds64(3600);
            wtr->delay = create_t_xy({
                {0.0, 0.0},
                {shyft::core::to_seconds(constant_delay_secs), 1.0}
            });
            shop_sys->adapter.set_shop_time_delay(sg, wtr->delay, overwrite);
            CHECK(sg.time_delay.exists());
            CHECK_EQ(sg.time_delay.get(), constant_delay_secs/time_delay_unit);
            CHECK_FALSE(sg.shape_discharge.exists());

            // Set shape discharge and update
            auto shape_base_delay_secs = shyft::core::utctime_from_seconds64(3600);
            wtr->delay = create_t_xy({
                {0.0, 0.0},
                {shyft::core::to_seconds(2*shape_base_delay_secs), 1.0},
                {shyft::core::to_seconds(4*shape_base_delay_secs), 2.0},
                {shyft::core::to_seconds(6*shape_base_delay_secs), 2.0},
                {shyft::core::to_seconds(8*shape_base_delay_secs), 1.0}
            });
            // Without overwrite: No change
            overwrite = false;
            shop_sys->adapter.set_shop_time_delay(sg, wtr->delay, overwrite);
            CHECK(sg.time_delay.exists());
            CHECK_EQ(sg.time_delay.get(), constant_delay_secs/time_delay_unit);
            CHECK_FALSE(sg.shape_discharge.exists());
            // With overwrite: Result should be only shape_discharge attribute,
            // previous time_delay attribute removed.
            overwrite = true;
            shop_sys->adapter.set_shop_time_delay(sg, wtr->delay, overwrite);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK(sg.shape_discharge.exists());
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points.size(), 5);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].x, 0.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].y, 0.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].x, shyft::core::to_seconds(2*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].y, 1.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[2].x, shyft::core::to_seconds(4*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[2].y, 2.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[3].x, shyft::core::to_seconds(6*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[3].y, 2.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[4].x, shyft::core::to_seconds(8*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[4].y, 1.0);

            // Set constant delay and update again to see that a previous shape_discharge
            // is removed and replaced with time_delay only.
            wtr->delay = create_t_xy({
                {0.0, 0.0},
                {shyft::core::to_seconds(constant_delay_secs), 1.0}
            });
            // Without overwrite: No change
            overwrite = false;
            shop_sys->adapter.set_shop_time_delay(sg, wtr->delay, overwrite);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK(sg.shape_discharge.exists());
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points.size(), 5);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].x, 0.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].y, 0.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].x, shyft::core::to_seconds(2*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].y, 1.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[2].x, shyft::core::to_seconds(4*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[2].y, 2.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[3].x, shyft::core::to_seconds(6*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[3].y, 2.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[4].x, shyft::core::to_seconds(8*shape_base_delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[4].y, 1.0);
            // With overwrite: Result should be only time_delay attribute.
            overwrite = true;
            shop_sys->adapter.set_shop_time_delay(sg, wtr->delay, overwrite);
            CHECK(sg.time_delay.exists());
            CHECK_EQ(sg.time_delay.get(), constant_delay_secs/time_delay_unit);
            CHECK_FALSE(sg.shape_discharge.exists());
        }
    }
#endif

    TEST_CASE("shop_reservoir")
    {
        SUBCASE("shop_reservoir_gets_spill_flow_description_from_gate")
        {
            // Check that spill flow description ('flow_descr' in SHOP)  is set correctly
            // on the SHOP reservoir created by the adapter.
            auto hps = make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto rsv = builder.create_reservoir(0, "reservoir"s, ""s);

            // spillway information is on waterway/gate in stm
            auto spill_wtr = builder.create_waterway(1, "spillway"s, ""s);
            connect(spill_wtr).input_from(rsv, connection_role::flood);
            auto spill_gt = builder.create_gate(2, "spillway gate"s, ""s);
            waterway::add_gate(spill_wtr, spill_gt);
            spill_gt->flow_description = create_constant_t_xyz(10.0, 20.0, 0.0);

            auto shop_sys = create_shop_system(hps);
            shop_reservoir sr = shop_sys->adapter.to_shop(*rsv);

            CHECK(sr.flow_descr.exists());
        }
    } // TEST_CASE

    TEST_CASE("shop_unit")
    {
        SUBCASE("optional_remains_unset") {
            // Check that optional attributes are unset on shop object when unset on stm object
            auto hps = make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto unit = builder.create_unit(0, "unit"s, ""s);

            auto shop_sys = create_shop_system(hps);
            shop_unit su = shop_sys->adapter.to_shop(*unit);

            CHECK_FALSE(su.priority.exists());
            CHECK_FALSE(su.fcr_mip_flag.exists());
            CHECK_FALSE(su.p_rr_min.exists());
            CHECK_FALSE(su.max_q_limit_rsv_down.exists());
        }

        SUBCASE("passes_priority")
        {
            // Check that the adapter passes the priority value
            auto hps = make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto unit = builder.create_unit(0, "unit"s, ""s);

            unit->priority = create_constant_ts(11.0);

            auto shop_sys = create_shop_system(hps);
            shop_unit su = shop_sys->adapter.to_shop(*unit);

            CHECK(su.priority.exists());
            CHECK_EQ(su.priority.get().value(0), 11.0);
        }

        SUBCASE("passes_cfr_mip")
        {
            // Check that the adapter passes the priority value
            auto hps = make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto unit = builder.create_unit(0, "unit"s, ""s);

            unit->reserve.fcr_mip = create_constant_ts(12.0);

            auto shop_sys = create_shop_system(hps);
            shop_unit su = shop_sys->adapter.to_shop(*unit);

            CHECK(su.fcr_mip_flag.exists());
            CHECK_EQ(su.fcr_mip_flag.get().value(0), 12.0);
        }

        SUBCASE("passes_mfrr_static_min")
        {
            // Check that the adapter passes the priority value
            auto hps = make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto unit = builder.create_unit(0, "unit"s, ""s);

            unit->reserve.mfrr_static_min = create_constant_ts(13.0);

            auto shop_sys = create_shop_system(hps);
            shop_unit su = shop_sys->adapter.to_shop(*unit);

            CHECK(su.p_rr_min.exists());
            CHECK_EQ(su.p_rr_min.get().value(0), 13.0);
        }

        SUBCASE("passes_constraint.max_from_downstream_level")
        {
            // Check that the adapter passes the priority value
            auto hps = make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto unit = builder.create_unit(0, "unit"s, ""s);

            auto hq = create_constant_t_xy(101.0, 42.0);
            unit->discharge.constraint.max_from_downstream_level = hq;

            auto shop_sys = create_shop_system(hps);
            shop_unit su = shop_sys->adapter.to_shop(*unit);

            CHECK(su.max_q_limit_rsv_down.exists());
            CHECK_EQ(su.max_q_limit_rsv_down.get().xy_curve.points[0], point{101.0, 42.0});
        }

    } // TEST_CASE

    TEST_CASE("shop_plant")
    {
        SUBCASE("optional_remains_unset") {
            // Check that optional attributes are unset on shop object when unset on stm object
            auto hps = make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto plant = builder.create_power_plant(0, "plant"s, ""s);

            auto shop_sys = create_shop_system(hps);
            shop_power_plant sp = shop_sys->adapter.to_shop(*plant);

            CHECK_FALSE(sp.discharge_ramping_up.exists());
            CHECK_FALSE(sp.discharge_ramping_down.exists());
            CHECK_FALSE(sp.power_ramping_up.exists());
            CHECK_FALSE(sp.power_ramping_down.exists());
        }

        SUBCASE("passes_discharge.ramping_up")
        {
            // check that the adapter passes the priority value
            auto hps = make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto plant = builder.create_power_plant(0, "plant"s, ""s);

            plant->discharge.ramping_up = create_constant_ts(11.0);

            auto shop_sys = create_shop_system(hps);
            shop_power_plant sp = shop_sys->adapter.to_shop(*plant);

            CHECK(sp.discharge_ramping_up.exists());
            CHECK_EQ(sp.discharge_ramping_up.get().value(0), 11.0);
        }

        SUBCASE("passes_discharge.ramping_down")
        {
            // check that the adapter passes the priority value
            auto hps = make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto plant = builder.create_power_plant(0, "plant"s, ""s);

            plant->discharge.ramping_down = create_constant_ts(12.0);

            auto shop_sys = create_shop_system(hps);
            shop_power_plant sp = shop_sys->adapter.to_shop(*plant);

            CHECK(sp.discharge_ramping_down.exists());
            CHECK_EQ(sp.discharge_ramping_down.get().value(0), 12.0);
        }

        SUBCASE("passes_production.ramping_up")
        {
            // check that the adapter passes the priority value
            auto hps = make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto plant = builder.create_power_plant(0, "plant"s, ""s);

            plant->production.ramping_up = create_constant_ts(13.0);

            auto shop_sys = create_shop_system(hps);
            shop_power_plant sp = shop_sys->adapter.to_shop(*plant);

            CHECK(sp.power_ramping_up.exists());
            CHECK_EQ(sp.power_ramping_up.get().value(0), 13.0);
        }

        SUBCASE("passes_production.ramping_down")
        {
            // check that the adapter passes the priority value
            auto hps = make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto plant = builder.create_power_plant(0, "plant"s, ""s);

            plant->production.ramping_down = create_constant_ts(13.0);

            auto shop_sys = create_shop_system(hps);
            shop_power_plant sp = shop_sys->adapter.to_shop(*plant);

            CHECK(sp.power_ramping_down.exists());
            CHECK_EQ(sp.power_ramping_down.get().value(0), 13.0);
        }

    } // TEST_CASE
} // TEST_SUITE

TEST_SUITE("stm_shop_adapter_from_shop")
{
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::gta_t;
    using shyft::time_axis::fixed_dt;
    using shyft::time_series::POINT_AVERAGE_VALUE;
    using namespace shyft::energy_market::stm;
    using namespace shyft::energy_market::stm::shop;

    using shyft::energy_market::hydro_power::point;
    using xyz_list = std::vector<xy_point_curve_with_z>;
    using t_xyz_list = std::map<utctime, std::shared_ptr<xyz_list>>;

    const auto t_step = shyft::core::deltahours(1);
    const gta_t time_axis{shyft::core::create_from_iso8601_string("2020-01-01T00:00:00Z"),t_step,24};
    const utcperiod t_period=time_axis.total_period();

    auto create_constant_t_double = [] (double val) -> apoint_ts {
        auto dt = t_period.end - t_period.start + shyft::core::deltahours(1);
        auto ta = fixed_dt(t_period.start, dt, 1);
        return apoint_ts(ta, val, POINT_AVERAGE_VALUE);
    };

    auto create_constant_ts = [] (double val) -> apoint_ts {
        int num_step = (t_period.end - t_period.start) / t_step;
        auto ta = gta_t(t_period.start, t_step, num_step);
        return apoint_ts(ta, val, POINT_AVERAGE_VALUE);
    };

    auto create_constant_t_xy = [] (double x, double y) -> t_xy_ {
        auto m = make_shared<t_xy_::element_type>();
        auto xy = std::make_shared<xy_point_curve>();
        xy->points.emplace_back(x, y);
        m->emplace(t_period.start, xy);
        return m;
    };

    auto create_t_xy = [] (std::initializer_list<std::pair<double, double>> v) -> t_xy_ {
        auto m = make_shared<t_xy_::element_type>();
        auto xy = std::make_shared<xy_point_curve>();
        for (auto it = v.begin(); it != v.end(); ++it)
            xy->points.emplace_back(it->first, it->second);
        m->emplace(t_period.start, std::move(xy));
        return m;
    };

    auto create_constant_t_xyz = [] (double x, double y, double z) -> t_xyz_list_ {
        auto m = make_shared<t_xyz_list_::element_type>();
        xy_point_curve xy {{point{x, y}}};
        auto xyz = std::make_shared<std::vector<xy_point_curve_with_z>>();
        xyz->emplace_back(xy, z);
        m->emplace(t_period.start, xyz);
        return m;
    };

    auto create_shop_system = [] (stm_hps_ hps) -> shared_ptr<shop_system> {
        // wrap the hps in a dummy stm system
        auto mdl = make_shared<stm_system>(0, "dummy_stm_system"s, ""s);
        auto mkt = make_shared<energy_market_area>(0, "dummy_stm_market"s, ""s, mdl);
        mdl->hps.push_back(hps);
        mdl->market.push_back(mkt);

        // creating the shop system calls ShopInit
        auto s = make_shared<shop_system>(time_axis);
        return s;
    };

    TEST_CASE("basic_types_from_shop")
    {
        SUBCASE("double_from_shop")
        {
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto shop_sys = create_shop_system(hps);
            auto wtr = builder.create_waterway(1, "waterway 1"s, ""s);
            wtr->discharge.static_max = create_constant_ts(10.0);
            shop_tunnel st = shop_sys->adapter.to_shop(*wtr);

            // Create new waterway and read back the attribute into that
            auto wtr2 = builder.create_waterway(2, "waterway 2"s, ""s);
            shop_sys->adapter.set_optional(wtr2->discharge.static_max, st.max_flow);
            CHECK_EQ(wtr->discharge.static_max, wtr2->discharge.static_max);
        }
        SUBCASE("ts_from_shop")
        {
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto shop_sys = create_shop_system(hps);
            auto rsv = builder.create_reservoir(1, "rsv 1"s, ""s);
            rsv->inflow.schedule = create_constant_ts(10.0);
            shop_reservoir sr = shop_sys->adapter.to_shop(*rsv);

            // Create new reservoir and read back the attribute into that
            auto rsv2 = builder.create_reservoir(2, "rsv 2"s, ""s);
            shop_sys->adapter.set_optional(rsv2->inflow.schedule, sr.inflow);
            CHECK_EQ(rsv->inflow.schedule, rsv2->inflow.schedule);
        }
        SUBCASE("xy_from_shop")
        {
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto delay_secs = shyft::core::utctime_from_seconds64(3600);
            auto shop_sys = create_shop_system(hps);
            auto wtr = builder.create_waterway(1, "waterway 1"s, ""s);
            wtr->delay = create_t_xy({
                {0.0, 0.5},
                {shyft::core::to_seconds(delay_secs), 1.0}
            });
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, nullptr);
            CHECK_FALSE(sg.time_delay.exists());
            CHECK(sg.shape_discharge.exists());
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points.size(), 2);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].x, 0.0);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[0].y, 0.5);
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].x, shyft::core::to_seconds(delay_secs));
            CHECK_EQ(sg.shape_discharge.get().xy_curve.points[1].y, 1.0);

            // Create new waterway and read back the attribute into that
            auto wtr2 = builder.create_waterway(2, "waterway 2"s, ""s);
            shop_sys->adapter.set_optional(wtr2->delay, sg.shape_discharge);
            CHECK_FALSE(wtr2->delay->empty());
            CHECK_EQ(wtr2->delay->begin()->first, t_period.start);
            CHECK_EQ(wtr2->delay->begin()->second->points.size(), 2);
            CHECK_EQ(wtr2->delay->begin()->second->points[0].x, 0.0);
            CHECK_EQ(wtr2->delay->begin()->second->points[0].y, 0.5);
            CHECK_EQ(wtr2->delay->begin()->second->points[1].x, shyft::core::to_seconds(delay_secs));
            CHECK_EQ(wtr2->delay->begin()->second->points[1].y, 1.0);
        }
        // SUBCASE("xyt_from_shop"): TODO, only output parameters in shop, so must run optimize to test?
    }
}
