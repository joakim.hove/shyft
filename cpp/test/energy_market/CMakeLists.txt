# CMake configuration for tests


add_executable(test_energy_market cpp_test.cpp
    mp/test_hana_struct.cpp
    mp/test_type_set_variants.cpp
    mp/test_mp_grammar.cpp
    stm/test_stm_srv.cpp
    stm/test_stm_proxy_url.cpp
    stm/test_task_classes.cpp
    stm/test_stm_attributes.cpp
    stm/test_stm_reservoir.cpp
    stm/test_stm_waterway.cpp
    stm/test_stm_unit.cpp
    stm/test_stm_system.cpp
    stm/test_stm_system_context.cpp
    stm/test_stm_system_clone.cpp
    stm/test_task_server_web_api.cpp
    stm/test_stm_srv_grammar.cpp
    stm/test_stm_generators.cpp
    stm/test_task_server.cpp
    stm/test_stm_serialisation.cpp
    stm/test_stm_unit_group.cpp
    stm/test_dstm_ts_url.cpp
    stm/build_test_system.cpp
    stm/test_compute_node.cpp
    graph/test_hps_graph.cpp
    graph/test_graph_utilities.cpp
    test_complete_models.cpp
    test_hydro_power_system.cpp
    test_hydro_operations.cpp
    test_waterway.cpp
    test_ltm_core.cpp
    test_xy.cpp
    test_turb_eff_descr.cpp
    test_srv.cpp
    test_pyobject.cpp
)
set_target_properties(test_energy_market PROPERTIES INSTALL_RPATH "$ORIGIN/../../shyft/lib")
target_include_directories(test_energy_market BEFORE PRIVATE ${CMAKE_SOURCE_DIR}/cpp/test/energy_market)
target_link_libraries(
    test_energy_market 
    PRIVATE shyft_private_flags doctest::doctest em_model_core stm_core shyft_core
    PUBLIC shyft_public_flags
    )
if(APPLE)
    target_link_libraries(test_energy_market PRIVATE "dl")
elseif(NOT MSVC)
    target_link_libraries(test_energy_market PRIVATE "dl" "stdc++fs")
endif()

add_custom_command(OUTPUT junit.xml
  COMMAND $<TARGET_FILE:test_energy_market> -r=junit -o=junit.xml
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  DEPENDS test_energy_market
)

add_custom_target(test_energy_market_junit DEPENDS junit.xml)

add_custom_target(test_energy_market_console
  COMMAND $<TARGET_FILE:test_energy_market> 
  DEPENDS test_energy_market
)

include(doctest)
doctest_discover_tests(test_energy_market)
