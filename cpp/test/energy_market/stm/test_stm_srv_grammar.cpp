#include <test/test_pch.h>
#include <shyft/web_api/energy_market/srv/grammar.h>
#include <shyft/srv/model_info.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include <test/web_api/test_parser.h>

#include <iostream>
#include <sstream>

using shyft::web_api::grammar::model_info_grammar;
using shyft::web_api::grammar::model_ref_grammar;
using shyft::web_api::grammar::stm_case_grammar;
using shyft::web_api::grammar::stm_session_grammar;

using shyft::srv::model_info;
using shyft::energy_market::stm::srv::model_ref;
using shyft::energy_market::stm::srv::stm_case;
using shyft::energy_market::stm::srv::stm_task;

using shyft::core::no_utctime;
using shyft::core::utctime;
using shyft::core::from_seconds;
using shyft::core::utctime_now;

TEST_SUITE("stm_grammar") {

    TEST_CASE("model_info") {
        model_info_grammar<const char*> mi_;
        model_info mi;
        auto t0 = utctime_now();

        // With all defaults:
        CHECK_EQ(test::phrase_parser(R"_(
            {
                "id": 1,
                "name": "test_mi"
            })_", mi_, mi), true);
        model_info exp_mi(1, "test_mi", t0, "");
        CHECK_EQ(mi.id, exp_mi.id);
        CHECK_EQ(mi.name, exp_mi.name);
        CHECK_GE(mi.created, exp_mi.created);
        CHECK_EQ(mi.json, exp_mi.json);

        // With created, no json:
        CHECK_EQ(test::phrase_parser(
            R"_({
                "id": 2,
                "name": "test_mi2",
                "created": 5
            })_",
            mi_, mi), true);
        CHECK_EQ(mi, model_info(2, "test_mi2", from_seconds(5), ""));

        // With json, not created
        CHECK_EQ(test::phrase_parser(
            R"_({
                "id": 3,
                "name": "test_mi3",
                "json": "{misc.}"
            })_",
            mi_, mi), true);
        exp_mi = model_info(3, "test_mi3", t0, "{misc.}");
        CHECK_EQ(mi.id, exp_mi.id);
        CHECK_EQ(mi.name, exp_mi.name);
        CHECK_GE(mi.created, exp_mi.created);
        CHECK_EQ(mi.json, exp_mi.json);

        // Both json and created:
        CHECK_EQ(test::phrase_parser(
            R"_({
                "id": 4,
                "name": "test_mi4",
                "created": 10,
                "json": "{misc.}"
            })_",
            mi_, mi), true);
        CHECK_EQ(mi, model_info(4, "test_mi4", from_seconds(10), "{misc.}"));
    }

    TEST_CASE("model_ref") {
        model_ref_grammar<const char*> ri_;
        model_ref ri;

        CHECK_EQ(true, test::phrase_parser(
            R"_({
                "host": "testhost",
                "port_num": 123,
                "api_port_num": 456,
                "model_key": "test_model"
            })_",
            ri_, ri)
        );
        CHECK_EQ(ri, model_ref("testhost", 123, 456, "test_model"));
    }

    TEST_CASE("stm_case") {
        stm_case_grammar<const char*> case_;
        stm_case acase;
        CHECK_EQ(acase.created,no_utctime);
        CHECK_EQ(acase.id,0);
        CHECK_EQ(acase.name.size(),0);
        
        auto t0 = utctime_now();
        // All defaults:
        CHECK_EQ(true, test::phrase_parser(
            R"_({
                "id": 1,
                "name": "case1"
            })_",
            case_, acase)
        );
        CHECK_EQ(acase, stm_case(1, "case1", acase.created));

        CHECK_GE(acase.created,t0); // To make sure that the case was created just about now.

        // With json:
        CHECK_EQ(true, test::phrase_parser(
            R"_({
                "id": 2,
                "name": "case2",
                "json": "{misc.}"
            })_",
            case_, acase)
        );
        CHECK_EQ(acase, stm_case(2, "case2", acase.created, "{misc.}"));
        CHECK_GE(acase.created, t0);

        // With labels:
        CHECK_EQ(true, test::phrase_parser(
            R"_({
                "id": 3,
                "name": "case3",
                "labels": ["test", "testlabel"]
            })_",
            case_, acase)
        );
        CHECK_EQ(acase, stm_case(3, "case3", acase.created, "", {"test", "testlabel"}));
        CHECK_GE(acase.created, t0);

        // With model_refs:
        CHECK_EQ(true, test::phrase_parser(
            R"_({
                "id": 4,
                "name": "case4",
                "model_refs":  [
                    {"host": "testhost", "port_num": 123, "api_port_num": 456, "model_key": "key1"},
                    {"host": "testhost", "port_num": 456, "api_port_num": 789, "model_key": "key2"}
                ]
            })_",
            case_, acase)
        );
        CHECK_EQ(acase, stm_case(4, "case4", acase.created, "", {}, {std::make_shared<model_ref>("testhost", 123, 456, "key1"), std::make_shared<model_ref>("testhost", 456, 789, "key2")}));
        CHECK_GE(acase.created, t0);

        // With all of it:
        CHECK_EQ(true, test::phrase_parser(
            R"_({
                "id": 5,
                "name": "case5",
                "created": 10,
                "json": "{misc.}",
                "labels": ["test", "testlabel"],
                "model_refs":  [
                    {"host": "testhost", "port_num": 123, "api_port_num": 456, "model_key": "key1"},
                    {"host": "testhost", "port_num": 456, "api_port_num": 789, "model_key": "key2"}
                ]
            })_",
            case_, acase)
        );
        CHECK_EQ(acase, stm_case(5, "case5", from_seconds(10), "{misc.}", {"test", "testlabel"},
                               {std::make_shared<model_ref>("testhost", 123, 456, "key1"), std::make_shared<model_ref>("testhost", 456, 789, "key2")}));

        // Lastly, a fail:
        CHECK_EQ(false, test::phrase_parser(
            R"_({
                "id": 1
            })_",
            case_, acase)
        );

    }

    TEST_CASE("stm_session") {
        stm_session_grammar<const char*> session_;
        stm_task session, expected;
        auto t0 = utctime_now();

        // 0. All defaults:
        CHECK_EQ(true, test::phrase_parser(
            R"_({
                "id": 1,
                "name": "testsession"
            })_",
            session_, session)
            );
        expected = stm_task(1, "testsession", t0);
        CHECK_EQ(session.id, expected.id);
        CHECK_EQ(session.name, expected.name);
        CHECK_GE(session.created, expected.created);

        // 1. Added json:
        CHECK_EQ(true, test::phrase_parser(
            R"_({
                "id": 1,
                "name": "testsession",
                "json": "{misc.}"
            })_",
            session_, session));
        CHECK_EQ(session.json, "{misc.}");

        // 2. With some labels:
        CHECK_EQ(true, test::phrase_parser(
            R"_({
                "id": 1,
                "name": "testsession",
                "labels": ["label1", "label2"]
            })_",
            session_, session));
        CHECK_EQ(session.labels.size(), 2);
        CHECK_EQ(session.labels[0], "label1");
        CHECK_EQ(session.labels[1], "label2");

        // 3. With cases attached:
        CHECK_EQ(true, test::phrase_parser(
            R"_({
                "id": 1,
                "name": "testsession",
                "cases": [
                    {
                        "id": 2,
                        "name": "testcase",
                        "created": 0.0,
                        "labels": ["test", "attached"]
                    }
                ]
            })_",
            session_, session));
        CHECK_EQ(session.cases.size(),1);
        CHECK_EQ(*(session.cases[0]), stm_case(2, "testcase", from_seconds(0), "", {"test", "attached"}));

        // 4. With reference to base_model:
        CHECK_EQ(true, test::phrase_parser(
            R"_({
                "id": 1,
                "name": "testsession",
                "base_model": {
                    "host": "testhost",
                    "port_num": 123,
                    "api_port_num": 456,
                    "model_key": "testmdl"
                }
            })_",
            session_, session));
        CHECK_EQ(session.base_mdl, model_ref("testhost", 123, 456, "testmdl"));

        // 5. With task name:
        CHECK_EQ(true, test::phrase_parser(
            R"_({
                "id": 1,
                "name": "testsession",
                "task_name": "atask"
            })_",
            session_, session));
        CHECK_EQ(session.task_name, "atask");
    }
}
