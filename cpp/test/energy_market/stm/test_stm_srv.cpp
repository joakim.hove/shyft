#include <doctest/doctest.h>
#include "build_test_system.h"
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/ts_url_generator.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/market.h>

#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <shyft/core/core_archive.h>
#include <shyft/energy_market/a_wrap.h>

#include <future>
#include <test/test_utils.h>

using namespace shyft::core;
using namespace shyft::energy_market::stm::srv::dstm;
using std::string;
using std::to_string;
using shyft::time_series::dd::apoint_ts;
using std::shared_ptr;
using std::make_shared;
using shyft::energy_market::stm::stm_system;
using shyft::energy_market::stm::energy_market_area;
using shyft::energy_market::stm::stm_hps;
using shyft::energy_market::stm::power_plant;
using shyft::energy_market::stm::unit;
using shyft::energy_market::stm::reservoir;
using shyft::energy_market::stm::stm_hps_builder;

using shyft::energy_market::hydro_power::connect;
using shyft::energy_market::hydro_power::connection_role;
using std::stringstream;
using test::utils::temp_dir;
using shyft::energy_market::a_wrap;
using shyft::energy_market::proxy_attr;

namespace {
template <class T>
static T serialize_loop(const T& o, int c_a_flags = core_arch_flags) {
	stringstream xmls;
	core_oarchive oa(xmls, c_a_flags);
	oa << core_nvp("o", o);
	xmls.flush();
	core_iarchive ia(xmls, c_a_flags);
	T o2;
	ia>>core_nvp("o", o2);
	return o2;
}

// simple utility to make ts url for one time-series
#define mk_ts_url(prefix,o,attr) proxy_attr(o,#attr,o.attr).url(prefix)


}
TEST_SUITE("stm_srv") {
TEST_CASE("stm_srv_basics") {
    dlib::set_all_logging_levels(dlib::LNONE);
    using shyft::time_series::dd::apoint_ts;
    server s;
    s.set_listening_ip("127.0.0.1");
    auto port_no = s.start_server();
    REQUIRE_GT(port_no,0);// require vs. test.abort this part of test if we fail here
    try {
        auto  host_port = string("localhost:") + to_string(port_no);
        client c(host_port);

        // get version info
        auto result = c.version_info();
        CHECK_EQ(result, s.do_get_version_info());
        // get model ids
        auto mids=c.get_model_ids();
        CHECK_EQ(mids.size(),0); // it should be 0 models to start with

        // create model server-side
        CHECK_EQ(c.create_model("m1"), true);
        CHECK_THROWS_AS(c.create_model("m1"),std::runtime_error); // attempting create with name already in use
        CHECK_EQ(s.model_map.size(), 1);

        // get model ids
        mids=c.get_model_ids();
        REQUIRE_EQ(mids.size(),1);
        CHECK_EQ(mids[0],"m1");

        // Get model infos
        auto mifs = c.get_model_infos();
        CHECK_EQ(mifs.size(), 1);
        auto mif = mifs["m1"];
        CHECK_EQ(mif.id, 0);
        CHECK_EQ(mif.name, "");
        CHECK_EQ(mif.json, "");

        // rename model
        CHECK_THROWS_AS(c.rename_model("m2","m0"), std::runtime_error); // attempting rename non-existent model
        CHECK_THROWS_AS(c.rename_model("m1","m1"), std::runtime_error); // attempting rename to name already in use
        CHECK_EQ(c.rename_model("m1","m0"), true);
        mids=c.get_model_ids();
        CHECK_EQ(mids.size(),1);
        CHECK_EQ(mids[0],"m0");

        // get model
        CHECK_THROWS_AS(c.get_model("m2"), std::runtime_error); // attempting get non-existent model
        CHECK_THROWS_AS(c.get_model("m1"), std::runtime_error); // attempting get non-existent model
        auto mdl = c.get_model("m0");

        // add model created client-side
        auto mdl2 = make_shared<stm_system>();
        {
            mdl2->market.push_back(make_shared<energy_market_area>(1, "test market", "", mdl2));
            auto hps = make_shared<stm_hps>(1, "test hps");
            stm_hps_builder builder(hps);
            auto rsv = builder.create_reservoir(2, "test rsv", "");
            auto unit = builder.create_unit(3, "test unit", "");
            auto plant = builder.create_power_plant(4, "test plant", "");
            auto tun = builder.create_tunnel(5, "rsv-unit", "");
            power_plant::add_unit(plant, unit);
            connect(tun)
                .input_from(rsv)
                .output_to(unit);
            mdl2->hps.push_back(hps);
        }
        CHECK_EQ(c.add_model("m1",mdl2), true);
        CHECK_EQ(s.model_map.size(), 2);

        // remove model
        CHECK_THROWS_AS(c.remove_model("m2"), std::runtime_error); // attempting remove non-existent model
        CHECK_EQ(c.remove_model("m0"), true); // remove model m0
        mids = c.get_model_ids();
        CHECK_EQ(mids.size(), 1);

        // clone model
        CHECK_THROWS_AS(c.clone_model("m0","m3"), std::runtime_error); // attempting clone non-existent model
        CHECK_THROWS_AS(c.clone_model("m1","m1"), std::runtime_error); // attempting clone to existing name
        CHECK_EQ(c.clone_model("m1","m1c"), true); // clone model m1 to m1c
        mids=c.get_model_ids();
        CHECK_EQ(mids.size(),2);
        auto mdl1 = c.get_model("m1");
        auto mdl1c = c.get_model("m1c");
        CHECK_EQ(mdl1->hps.size(),1);
        CHECK_EQ(mdl1c->hps.size(),1);
        CHECK_UNARY(mdl1->hps.front()->equal_structure(*mdl1c->hps.front()));
        // check callback fx
        // (with none on the server side)
        CHECK_EQ(c.fx("m1","optimize_this"),false);// we should have false when no cb set.
        // then rig a callback
        size_t cb_count=0;
        s.fx_cb = [&cb_count](string mid,string arg)->bool {
            cb_count++;
            return mid=="m1" && arg=="optimize_this"; // verify we got the correct args.
        };
        CHECK_EQ(c.fx("m1","optimize_this"),true);// we should have false when no cb set.
        CHECK_EQ(cb_count,1);// really, it's done
        s.fx_cb=nullptr;// ensure that lambda capture is zeroed out before terminating test.
        // Get state:
        CHECK_THROWS_AS(c.get_state("m3"), std::runtime_error); // attempting to get state of nonexistent model
        auto state = c.get_state("m1");
        CHECK_EQ(state, model_state::idle);
        // remove models
        CHECK_THROWS_AS(c.remove_model("m0"), std::runtime_error); // attempting remove non-existent model
        CHECK_EQ(c.remove_model("m1"), true); // remove model m1
        mids = c.get_model_ids();
        CHECK_EQ(mids.size(), 1);
        CHECK_THROWS_AS(c.remove_model("m1"), std::runtime_error); // attempting remove non-existent model
        CHECK_EQ(c.remove_model("m1c"), true); // remove model m1c
        mids = c.get_model_ids();
        CHECK_EQ(mids.size(), 0);

        c.close();
        s.clear();
    } catch (exception const&ex) {
        DOCTEST_MESSAGE(ex.what());
        CHECK_EQ(true,false);
        s.clear();
    }
}

TEST_CASE("dstm_stress") {
	dlib::set_all_logging_levels(dlib::LNONE);
    using shyft::time_series::dd::apoint_ts;
    server s;
    s.set_listening_ip("127.0.0.1");
    auto port_no = s.start_server();
    REQUIRE_GT(port_no,0);// require vs. test.abort this part of test if we fail here
	const int n_connects=100;
    try {
        auto  host_port = string("localhost:") + to_string(port_no);
        client c(host_port);

        // get version info
        auto result = c.version_info();
        CHECK_EQ(result, s.do_get_version_info());

        // get model ids
        auto mids=c.get_model_ids();
        CHECK_EQ(mids.size(),0); // it should be 0 models to start with

        // create model server-side
        CHECK_EQ(c.create_model("m1"), true);
        CHECK_THROWS_AS(c.create_model("m1"),std::runtime_error); // attempting create with name already in use
        CHECK_EQ(s.model_map.size(), 1);
        for (size_t i=0; i<n_connects; ++i) {
        	mids = c.get_model_ids();
        	CHECK_EQ(1, mids.size());
        }
        s.clear();
    } catch (exception const& ex) {
		DOCTEST_MESSAGE(ex.what());
		CHECK_EQ(true, false);
		s.clear();
    }

}

TEST_CASE("dstm_stress_evaluate") {
    dlib::set_all_logging_levels(dlib::LNONE);
    using shyft::time_series::dd::apoint_ts;
    server s;
    s.set_listening_ip("127.0.0.1");

    temp_dir tmpdir{"dstm_evaluate.test."};
    s.add_container("test", (tmpdir / "ts").string());
    auto port_no = s.start_server();
    REQUIRE_GT(port_no,0);// require vs. test.abort this part of test if we fail here
    const int n_connects=20;
    try {
        auto  host_port = string("localhost:") + to_string(port_no);
        //client c(host_port);

        for (size_t i=0; i<n_connects; ++i) {
            s.do_add_model("m" + to_string(i), test::create_simple_system_with_dtss(*(s.dtss)));
        }
        CHECK_EQ(s.model_map.size(), n_connects);
        // Do several evaluates in paralell:
        vector<std::future<bool>> res;

        utcperiod p(0, 2400);
        for (size_t i=0; i<n_connects; ++i) {
            res.emplace_back(std::async(std::launch::async,
                [port_no, &p, i]()->bool {
                    auto t = p.start;
                    auto host_port = string("localhost:") + to_string(port_no);
                    client c(host_port);
                    string mid = "m" + to_string(i);
                    c.evaluate_model(mid, p, false, false);
                    auto mdl = c.get_model(mid);
                    auto hps = mdl->hps[0];
                    auto rsv = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_id(1));
                    CHECK_EQ(rsv->level.regulation_min(t), doctest::Approx(1.0));
                    CHECK_EQ(rsv->level.regulation_max(t), doctest::Approx(2.0));
                    CHECK_EQ(rsv->volume.static_max(t), doctest::Approx(3.0));
                    CHECK_EQ(rsv->volume.result(t), doctest::Approx(2.5));

                    auto u = std::dynamic_pointer_cast<unit>(hps->find_unit_by_id(1));
                    CHECK_EQ(u->cost.start(t), doctest::Approx(4.0));
                    CHECK_EQ(u->cost.stop(t), doctest::Approx(5.0));
                    CHECK_EQ(u->production.result(t), doctest::Approx(20.0));

                    auto pp = std::dynamic_pointer_cast<power_plant>(hps->find_power_plant_by_id(2));
                    CHECK_EQ(pp->mip(t), doctest::Approx(6.0));
                    CHECK_EQ(pp->production.schedule(t), doctest::Approx(20.5));

                    auto market = mdl->market[0];
                    CHECK_EQ(market->price(t), doctest::Approx(7.0));
                    CHECK_EQ(market->sale(t), doctest::Approx(8.0));
                    CHECK_EQ(market->load(t), doctest::Approx(164.0));
                    return true;
            }));
        }
        CHECK_EQ(res.size(), n_connects);

        // Check that each thread exits successfully:
        for (size_t i=0; i<n_connects; ++i) {
            CHECK(res[i].get());
        }
        s.clear();
    } catch (exception const& ex) {
        DOCTEST_MESSAGE(ex.what());
        CHECK_EQ(true, false);
        s.clear();
    }
}

TEST_CASE("dstm_get_set_ts") {
    dlib::set_all_logging_levels(dlib::LNONE);
    using shyft::time_series::dd::apoint_ts;
    using ta_t=shyft::time_axis::generic_dt;
    using shyft::time_series::ts_point_fx;

    server s;
    s.set_listening_ip("127.0.0.1");

    temp_dir tmpdir{"dstm_get_set_ts.test."};
    s.add_container("test", (tmpdir / "ts").string());
    auto port_no = s.start_server();
    REQUIRE_GT(port_no,0);// require vs. test.abort this part of test if we fail here
    try {
        auto  host_port = string("localhost:") + to_string(port_no);
        client c(host_port);
        string mid{"m1"};
        s.do_add_model(mid, test::create_simple_system_with_dtss(*(s.dtss)));
        utcperiod p(0, 2400);
        c.evaluate_model(mid, p, false, false);// ensure we have evaluated time-series(would it also work with unbound?)
        auto mdl = c.get_model(mid);
        auto hps = mdl->hps[0];
        auto rsv = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_id(1));
        auto u = std::dynamic_pointer_cast<unit>(hps->find_unit_by_id(1));
        //auto pp = std::dynamic_pointer_cast<power_plant>(hps->find_power_plant_by_id(2));
        auto market = mdl->market[0];

        ///-- get_ts.
        vector<string> ts_urls;

        auto mdl_prefix="dstm://M"+mid;
        ts_urls.push_back(mk_ts_url(mdl_prefix,rsv->volume,result));
        ts_urls.push_back(mk_ts_url(mdl_prefix,u->production,result));
        ts_urls.push_back(mk_ts_url(mdl_prefix,(*market),sale));
        for(auto url:ts_urls)
            MESSAGE("url:"<<url);
        //CHECK_EQ("dstm://Mm1/H1/R1.volume.result",ts_urls.front());
        // ACT: get specified list of ts-urls, from different levels of the model
        auto rts=c.get_ts(mid,ts_urls);
        // ASSERT that these values are the same as whats really stored on the model
        CHECK_EQ(rts.size(),ts_urls.size());
        CHECK_EQ(rts[0],rsv->volume.result);
        CHECK_EQ(rts[1],u->production.result);
        CHECK_EQ(rts[2],market->sale);
        // Verify the url_generator for the result ts.(to be exposed later)

        auto all_result_ts_urls=ts_url_generator(mdl_prefix,*mdl);
        //for(auto url:all_result_ts_urls)
        //    MESSAGE("url:"<<url);
        rts=c.get_ts(mid,all_result_ts_urls);
        CHECK_EQ(rts.size(),42);
        //------------------------------------------------------------
        //
        //-- now that we know get_ts work, let us test set_ts as well:
        //
        // lets empty one of the series, so that we subscribe on an empty ts.
        //MESSAGE("setting ts to empty");
        vector<string> empty_ts_urls;empty_ts_urls.push_back(ts_urls[0]);
        ats_vector empty_ts; empty_ts.push_back(apoint_ts(ts_urls[0]) );
        c.set_ts(mid,empty_ts);
        //MESSAGE("now its empty");
        rts=c.get_ts(mid,ts_urls);
        CHECK_EQ(rts[0].needs_bind(),true);
        CHECK_EQ(rts[0].id(),empty_ts_urls[0]);
        
        auto subs=s.dtss->sm->add_subscriptions(ts_urls);
        //MESSAGE("added subs");
        
        auto sum_subs=0;
        for(auto const&sub:subs)
            sum_subs += sub->v;
        ats_vector sts;
        ta_t ta{from_seconds(0),from_seconds(10),5};
        apoint_ts rsv_volume_result{ta,{1.0,2.0,3.0,4.0,5.0},ts_point_fx::POINT_AVERAGE_VALUE};
        apoint_ts u_production_result{ta,{1.0,2.2,3.0,2.0,5.0},ts_point_fx::POINT_AVERAGE_VALUE};
        apoint_ts market_sale{ta,{1.1,2.0,3.3,4.0,5.0},ts_point_fx::POINT_AVERAGE_VALUE};
        sts.push_back(apoint_ts{ts_urls[0],rsv_volume_result});
        sts.push_back(apoint_ts{ts_urls[1],u_production_result});
        sts.push_back(apoint_ts{ts_urls[2],market_sale});
        c.set_ts(mid,sts);
        // prove two things:
        // (1) values in set_ts is updated
        // (2) that notification/subscription was updated so that web-ui get the changes
        rts=c.get_ts(mid,ts_urls);
        CHECK_EQ(rts.size(),ts_urls.size());
        CHECK_EQ(rts[0],rsv_volume_result);
        CHECK_EQ(rts[1],u_production_result);
        CHECK_EQ(rts[2],market_sale);
        auto sum_subs2=0;
        for(auto const&sub:subs) sum_subs2 += sub->v;
        CHECK_EQ(sum_subs2-sum_subs,3);
        s.dtss->sm->remove_subscriptions(subs);
        subs.clear();

        c.close();
        s.clear();
    } catch (exception const& ex) {
        MESSAGE("Failed with exception:: "<<ex.what());
        CHECK_EQ(true, false);
        s.clear();
    }
}


}
