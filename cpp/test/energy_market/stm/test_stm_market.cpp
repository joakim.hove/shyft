#include <doctest/doctest.h>

#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit_group.h>

using namespace shyft::energy_market::stm;
using std::make_shared;

TEST_SUITE("stm_energy_market_area") {

    TEST_CASE("energy_market_area_construct") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto ema=make_shared<energy_market_area>(1,"ema","{}", sys);
        FAST_CHECK_UNARY( ema);
        FAST_CHECK_EQ(ema->id,1);
        FAST_CHECK_EQ(ema->name,"ema");
        FAST_CHECK_EQ(ema->json,"{}");

    }
    TEST_CASE("energy_market_area_set_unit_group") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto ema=make_shared<energy_market_area>(1,"ema","{}", sys);
        auto ug1 = sys->add_unit_group(1,"ug1","{}");
        ema->set_unit_group(ug1);
        FAST_CHECK_EQ(ema->unit_groups[0], ug1);
        auto ug2 = sys->add_unit_group(2,"ug2","{}");
        CHECK_THROWS_AS(ema->set_unit_group(ug2), std::runtime_error);
    }
    TEST_CASE("energy_market_area_get_unit_group") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto ema=make_shared<energy_market_area>(1,"ema","{}", sys);
        auto ug1 = sys->add_unit_group(1,"ug1","{}");
        auto ug = ema->get_unit_group();
        FAST_CHECK_EQ(ug, nullptr);
        ema->set_unit_group(ug1);
        auto ug2 = ema->get_unit_group();
        FAST_CHECK_EQ(ug2, ug1);
        FAST_CHECK_EQ(ema->unit_groups.size(),1u);
    }
    TEST_CASE("energy_market_area_remove_group") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto ema=make_shared<energy_market_area>(1,"ema","{}", sys);
        auto ug1 = sys->add_unit_group(1,"ug1","{}");
        CHECK_THROWS_AS(ema->remove_unit_group(), std::runtime_error);
        ema->set_unit_group(ug1);
        auto ug = ema->remove_unit_group();
        FAST_CHECK_EQ(ug, ug1);
        FAST_CHECK_EQ(ema->unit_groups.size(),0u);
    }
}
