#include <doctest/doctest.h>
#include <shyft/mp.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/stm_serialization.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <test/energy_market/stm/utilities.h>

namespace hana = boost::hana;

namespace doctest {

    // template specialization to generate readable error messages
    template<> struct StringMaker<std::vector<const char*>>
    {
        static String convert(const std::vector<const char*>& vec)
        {
            std::ostringstream oss;
            oss << "[ ";
            std::copy(vec.begin(), vec.end(), std::ostream_iterator<const char*>(oss, " "));
            oss << "]";
            return oss.str().c_str();
        }
    };
}

namespace test_stm_serialization {

    namespace hana = boost::hana;
    namespace mp = shyft::mp;
    namespace sts = shyft::time_series;
    namespace stm = shyft::energy_market::stm;

    using stm::impl::operator==; // enable by-value comparison of stm types
    using shyft::energy_market::hydro_power::xy_point_curve;
    using shyft::energy_market::hydro_power::xy_point_curve_with_z;
    using shyft::energy_market::hydro_power::turbine_efficiency;
    using shyft::energy_market::hydro_power::turbine_description;
    using stm::xyz_point_curve_list;
    using shyft::core::utctime;
    using shyft::time_axis::fixed_dt;
    using shyft::time_axis::generic_dt;
    using shyft::time_axis::generic_dt;
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::POINT_INSTANT_VALUE;

    template<typename T>
    auto to_blob(std::shared_ptr<T>& t) -> std::string {
        std::ostringstream oss;
        boost::archive::binary_oarchive archive(oss);

        stm::serialize_stm_attributes(*t, archive);
        return oss.str();
    }

    template<typename T>
    auto from_blob(std::string istring) -> std::shared_ptr<T> {
        std::istringstream iss(istring);
        boost::archive::binary_iarchive archive(iss);

        auto t = std::make_shared<T>();
        stm::serialize_stm_attributes(*t, archive);
        return t;
    }

    template<typename T>
    auto serialize_deserialize(std::shared_ptr<T>& t) -> std::shared_ptr<T> {
        return from_blob<T>(to_blob(t));
    }

    auto create_fx_log = [] () {
        std::vector<std::pair<utctime, std::string>> log;
        log.emplace_back(11, "test");
        return log;
    };

    auto create_time_axis_generic_dt = [] () {
        return generic_dt {1000, 4600, 8200};
    };

    auto create_hps_vector = [] () {
        std::vector<std::shared_ptr<stm::stm_hps>> hps_vec;
        hps_vec.push_back(std::make_shared<stm::stm_hps>(1, "hps_1"));
        hps_vec.push_back(std::make_shared<stm::stm_hps>(2, "hps_2"));
        return hps_vec;
    };

    auto create_market_vector = [] () {
        std::vector<std::shared_ptr<stm::energy_market_area>> markets;
        markets.push_back(std::make_shared<stm::energy_market_area>());
        markets.back()->id = 2;
        markets.back()->name = "test_market";
        return markets;
    };

    auto create_run_parameters = [] () -> std::shared_ptr<stm::run_parameters> {
        auto rp = std::make_shared<stm::run_parameters>();
        rp->n_inc_runs = 1;
        rp->n_full_runs = 1;
        return rp;
    };
    auto create_unit_groups=[]()->std::vector<stm::unit_group_> {
        std::vector<stm::unit_group_> r;
        r.push_back(std::make_shared<stm::unit_group>());
        r.back()->id=1;
        r.back()->name="UG1";
        return r;
    };

    // To automate the tests we need a non-default constructed instances of the value types
    // used in the attribute structs.
    auto values = hana::make_tuple(
        (uint16_t) 5,
        (bool) true,

        test::create_t_double(11, 42.0),
        test::create_t_xy(12, std::vector<double>{1, 2}),
        test::create_t_xyz(13, std::vector<double>{1, 2, 3}),
        test::create_t_turbine_description(14, std::vector<double>{1, 2, 3}),

        create_fx_log(),
        create_time_axis_generic_dt(),
        create_hps_vector(),
        create_market_vector(),
        create_run_parameters(),
        create_unit_groups()
    );

    const auto type_value_map = hana::fuse(hana::make_map)(
        hana::transform(hana::zip(hana::transform(values, hana::typeid_), values),
                        hana::fuse(hana::make_pair))
    );


    // assign to each struct leaf the value of the matching type.
    template<typename T>
    void assign_values(T& t) {
        auto key = hana::compose(mp::accessor_ptr_type, mp::leaf_accessor);
        auto assign_val = [&] (auto a) {
            auto v = hana::find(type_value_map, key(a));
            if constexpr(!hana::is_nothing(v))
                mp::leaf_access(t, a) = v.value();
        };
        hana::for_each(mp::leaf_accessors(hana::type_c<T>), assign_val);
    }


    // list ids of attributes that are equal between o1 and o2.
    template<typename T>
    std::vector<const char*> equal_attrs(T& o1, T& o2) {
        std::vector<const char*> res;
        hana::for_each(mp::leaf_accessors(hana::type_c<T>),
            [&o1, &o2, &res](auto a) {
                auto v1 = mp::leaf_access(o1, a);
                auto v2 = mp::leaf_access(o2, a);
                if ( stm::equal_attribute(v1, v2) )
                    res.push_back(mp::leaf_accessor_id_str(a));
            }
        );
        return res;
    }

    // list ids of attributes that are different between o1 and o2.
    template<typename T>
    std::vector<const char*> not_equal_attrs(T& o1, T& o2) {
        std::vector<const char*> res;
        hana::for_each(mp::leaf_accessors(hana::type_c<T>),
            [&o1, &o2, &res](auto a) {
                auto v1 = mp::leaf_access(o1, a);
                auto v2 = mp::leaf_access(o2, a);
                if ( !stm::equal_attribute(v1, v2) )
                    res.push_back(mp::leaf_accessor_id_str(a));
            }
        );
        return res;
    }

    // identify attributes we have not handled (missing from the values-tuple).
    template<typename T>
    std::vector<const char*> unhandled_attrs() {
        auto o1 = std::make_shared<T>(); 
        auto o2 = std::make_shared<T>();
        assign_values(*o1);
        return equal_attrs(*o1, *o2);
    };

    // Custom template for stm_system, due too optimization_summary
    // is created in constructors, hence not a nullptr
    template<>
    std::vector<const char*> unhandled_attrs<stm::stm_system>() {
        auto o1 = std::make_shared<stm::stm_system>();
        auto o2 = std::make_shared<stm::stm_system>();
        o2->summary = nullptr;
        assign_values(*o1);
        return equal_attrs(*o1, *o2);
    }

    // identify attributes that did not pass the serialize-deserialize cycle.
    template<typename T>
    std::vector<const char*> unserialized_attrs() {
        auto o1 = std::make_shared<T>(); 
        assign_values(*o1);
        auto o2 = serialize_deserialize(o1);
        return not_equal_attrs(*o1, *o2);
    };

    const auto empty_list = std::vector<const char*> {};


TEST_SUITE("test_stm_serialisation") {
    // For each struct, we want to test that all its attributes survive
    // a serialization-deserialization cycle in a mostly automated way.
    //
    // The for each object we:
    //  - test that we can assign to each leaf member a non-default value. 
    //  - test that attributes are the same after passing the object
    //    through a serialize-deserialize cycle.
    //
    // The non-default values are required in order to make a meaningful
    // comparison between the original and the deserialized object.
    //
    // The assigned values are taken from a tuple according to their type.
    // When adding a new attribute to an object make sure that its value
    // type is found in the tuple of known value types.


    // stm hydro components

    TEST_CASE("stm_reservoir") {
        CHECK_EQ(unhandled_attrs<stm::reservoir>(), empty_list);
        CHECK_EQ(unserialized_attrs<stm::reservoir>(), empty_list);
    }
	
	TEST_CASE("stm_reservoir_aggregate") {
        CHECK_EQ(unhandled_attrs<stm::reservoir_aggregate>(), empty_list);
        CHECK_EQ(unserialized_attrs<stm::reservoir_aggregate>(), empty_list);
    }

    TEST_CASE("stm_unit") {
        CHECK_EQ(unhandled_attrs<stm::unit>(), empty_list);
        CHECK_EQ(unserialized_attrs<stm::unit>(), empty_list);

    }

    TEST_CASE("stm_power_plant") {
        CHECK_EQ(unhandled_attrs<stm::power_plant>(), empty_list);
        CHECK_EQ(unserialized_attrs<stm::power_plant>(), empty_list);

    }

    TEST_CASE("stm_waterway") {
        CHECK_EQ(unhandled_attrs<stm::waterway>(), empty_list);
        CHECK_EQ(unserialized_attrs<stm::waterway>(), empty_list);

    }

    TEST_CASE("stm_gate") {
        CHECK_EQ(unhandled_attrs<stm::gate>(), empty_list);
        CHECK_EQ(unserialized_attrs<stm::gate>(), empty_list);

    }

    //

    TEST_CASE("stm_system") {
        CHECK_EQ(unhandled_attrs<stm::stm_system>(), empty_list);
        CHECK_EQ(unserialized_attrs<stm::stm_system>(), empty_list);
    }

    TEST_CASE("run_parameters") {
        CHECK_EQ(unhandled_attrs<stm::run_parameters>(), empty_list);
        CHECK_EQ(unserialized_attrs<stm::run_parameters>(), empty_list);

        // no check for attribute 'mdl', since it is pointer to unowned object
    }

} // end test suite
} // end namespace
