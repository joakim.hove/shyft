#include "build_test_system.h"
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/market.h>

#include <shyft/dtss/dtss.h>

namespace test {
    using namespace shyft::energy_market::stm;
    using namespace shyft::core;
    using namespace shyft::time_series;
    using namespace shyft::time_axis;
    using std::vector;
    using std::shared_ptr;
    using std::make_shared;
    


    stm_hps_ create_stm_hps(int id,string name) {
        using shyft::energy_market::hydro_power::connect;
        using shyft::energy_market::hydro_power::connection_role;
        /*
        Demonstrates how to build an in memory representation of  stm HydroPowerSystem
        using part of the blåsjø/ulla-førre systems
        */
        auto sorland = make_shared<stm_hps>(id,name);
        stm_hps_builder hps(sorland);
        auto blasjo1 = hps.create_reservoir(1,"blåsjø1",string("json{max_vol:3200Mm3}"));
        auto blasjo2 = hps.create_reservoir(21,"blåsjø2",string("json{}"));
        auto blasjo = hps.create_reservoir_aggregate(22,"blåsjø",string("json{}"));
        reservoir_aggregate::add_reservoir(blasjo, blasjo1);
        reservoir_aggregate::add_reservoir(blasjo, blasjo2);
        auto saurdal = hps.create_unit(2,"saurdal","json{max_eff:600MW}");
        auto tunx = hps.create_tunnel(12,"blåsjø-saurdal","json{}");
        connect(tunx)
            .input_from(blasjo1)
            .output_to(saurdal);
        auto sandsavatn = hps.create_reservoir(3,"sandsvatn", "reservoir_data(560.0, 605.0, 230.0)");
        auto lauvastolsvatn = hps.create_reservoir(4,"lauvastølsvatn", "reservoir_data(590.0, 605.0, 8.3)");
        auto kvilldal1 = hps.create_unit(51,"kvilldal_1","{Ek:1.3,outlet_level_masl:70");
        auto kvilldal2 = hps.create_unit(52,"kvilldal_2","{Ek:1.3,outlet_level_masl:70");
        auto t_kvilldal = hps.create_tunnel(510,"kvilldal hovedtunnel", "{alpha:0.000053}");
        auto t_kvill_penstock_1 = hps.create_tunnel(5101,"kvilldal_1_penstock","json{}");
        auto t_kvill_penstock_2 = hps.create_tunnel(5102,"kvilldal_2_penstock","json{}");
        auto t_saur_kvill = hps.create_tunnel(6,"saurdal-kvilldal-hoved-tunnel","json{}");
        auto t_sandsa_kvill = hps.create_tunnel(7,"sandsavatn-til-kvilldal", "json{}");
        auto t_lauvas_kvill = hps.create_tunnel(8,"lauvastølsvatn-til-kvilldal", "json{}");
        auto g = hps.create_gate(1, "L1", "");
        shyft::energy_market::stm::waterway::add_gate(t_lauvas_kvill, g);
        g->discharge.schedule=apoint_ts("shyft://stm/rid/oid/aid");
        connect(saurdal).output_to(t_saur_kvill);
        connect(t_saur_kvill).output_to(t_kvilldal);
        connect(t_kvilldal).output_to(t_kvill_penstock_1);
        connect(t_kvill_penstock_1).output_to(kvilldal1);
        connect(t_kvilldal).output_to(t_kvill_penstock_2);
        connect(t_kvill_penstock_2).output_to(kvilldal2);

        connect(t_sandsa_kvill).input_from(sandsavatn).output_to(t_kvilldal);
        connect(t_lauvas_kvill).input_from(lauvastolsvatn).output_to(t_kvilldal);

        auto vassbotvatn = hps.create_reservoir(9,"vassbotvatn", "json{}");
        auto stoelsdal_pumpe = hps.create_unit(10,"stølsdal pumpe","json{}");
        auto above_vassbotvatn = hps.create_tunnel(11,"stølsdals kraftstasjon(pumpe) til vassbotvatn det pumpes fra", "json{}");
        connect(above_vassbotvatn)
            .input_from(stoelsdal_pumpe);
        connect(vassbotvatn).input_from(above_vassbotvatn);
        auto tun_sandsa_stolsdal = hps.create_tunnel(120, "fra sandsvatn til stølsdal pump", "json{}");
        connect(tun_sandsa_stolsdal).output_to(stoelsdal_pumpe);
        connect(sandsavatn).output_to(tun_sandsa_stolsdal, connection_role::main);

        auto suldalsvatn = hps.create_reservoir(13,"suldalsvatn","json{}");
        auto hylen = hps.create_unit(14,"hylen","json{}");

        connect(hps.create_river(15,"fra kvilldal til suldalsvatn", "json{}"))
            .input_from(kvilldal1)
            .input_from(kvilldal2)
            .output_to(suldalsvatn);

        connect(hps.create_tunnel(16,"hylen-tunnel", "json{}"))
            .input_from(suldalsvatn)
            .output_to(hylen);

        auto havet = hps.create_reservoir(17, "havet","json{}");

        connect(hps.create_river(18,"utløp hylen","json{}"))
            .input_from(hylen)
            .output_to(havet);

        connect(hps.create_river(19,"bypass suldal til havet","json{}"))
            .input_from(suldalsvatn, connection_role::bypass)
            .output_to(havet);

        connect(hps.create_river(20,"flom suldal til havet","json{}"))
            .input_from(suldalsvatn, connection_role::flood)
            .output_to(havet);
        return sorland;
    }

    stm_hps_ create_simple_hps(int id, string name) {
        using shyft::energy_market::hydro_power::connect;
        using shyft::energy_market::hydro_power::connection_role;
        
        auto simple = make_shared<stm_hps>(id,name);
        stm_hps_builder hps(simple);
        
        auto r = hps.create_reservoir(1,"simple_res", "");
        auto u = hps.create_unit(1, "simple_unit", "stuff");
        auto tun = hps.create_tunnel(1, "r->u", "");
        auto pp = hps.create_power_plant(2,"simple_pp", "");
        auto g= hps.create_gate(1,"gate","");
        auto ra= hps.create_reservoir_aggregate(2,"ra","");
        auto ca= hps.create_catchment(1,"catchment","{}");

        connect(tun).input_from(r)
                    .output_to(u);
        pp->add_unit(pp,u);
        waterway::add_gate(tun,g);
        reservoir_aggregate::add_reservoir(ra,r);
        
        //We'll also dress the model up with some time-series:
        utctimespan dt{deltahours(1)};
        fixed_dt ta{_t(0),dt,6};
        ts_point_fx linear{POINT_INSTANT_VALUE};
        
        r->inflow.schedule = apoint_ts(ta,vector<double>{1.0,2.0,shyft::nan,4.0,3.0,6.0},linear);
        r->level.result = apoint_ts(ta, 1.0, linear);
        r->volume.result = apoint_ts("shyft://test/r.volume.result");
        
        ra->volume.schedule=apoint_ts("shyft://test/ra.volume.schedule");
        
        ca->inflow_m3s = apoint_ts("shyft://test/ca.inflow_m3s");

        u->discharge.constraint.min = apoint_ts(ta, 0.87, linear);
        u->production.result = apoint_ts(ta, vector<double>{1.0, 1.2, 1.3, 1.4, 1.5, 1.6}, linear);
        
        
        pp->production.schedule= apoint_ts("shyft://test/pp.production.schedule");
        
        tun->discharge.result = apoint_ts(ta, 5.0, linear);
        tun->discharge.static_max = apoint_ts("shyft://test/w.discharge.static_max");
        
        g->discharge.result=apoint_ts("shyft://test/g.discharge.result");
        return simple;
    }

    stm_system_ create_stm_system(int id, string name, string json) {
        auto mdl = make_shared<stm_system>(id, name, json);
        mdl->hps.push_back(create_stm_hps());
        return mdl;
    }

    stm_system_ create_simple_system(int id, string name) {
        auto mdl = make_shared<stm_system>(id, name, "");
        mdl->hps.push_back(create_simple_hps());
        auto market = make_shared<energy_market_area>(2, "market", "", mdl);
        mdl->market.push_back(market);
        market->price = apoint_ts("shyft://test/market.price");
        auto ug= mdl->add_unit_group(3,"ug","");
        apoint_ts act("shyft://test/ug/m.active");
        ug->add_unit(std::dynamic_pointer_cast<unit>(mdl->hps.front()->units.front()),act);
        ug->obligation.schedule=apoint_ts("shyft://test/ug.obligation.schedule");
        return mdl;
    }

    stm_system_ create_simple_system_with_dtss(shyft::dtss::server& dtss, int id, string name) {
        auto mdl = make_shared<stm_system>(id, name, "");
        mdl->hps.push_back(create_simple_hps());
        auto market = make_shared<energy_market_area>(2, "market", "", mdl);
        mdl->market.push_back(market);
        // Add some time series with expressions
        shyft::dtss::ts_vector_t tsv;
        utctime t0, tN;
        t0 = _t(0);
        tN = _t(2400);
        shyft::dtss::gta_t ta(t0, tN, 24);
        ts_point_fx average{POINT_AVERAGE_VALUE};
        // 1. HPS:
        auto hps = mdl->hps[0];
        // 1.1 Reservoir:
        auto rsv = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_id(1));
        apoint_ts rsv_lrl(ta, 1.0, average);
        tsv.emplace_back(apoint_ts{"shyft://test/rsv_lrl", rsv_lrl});
        rsv->level.regulation_min = apoint_ts{"shyft://test/rsv_lrl"};

        apoint_ts rsv_hrl(ta, 2.0, average);
        tsv.emplace_back(apoint_ts{"shyft://test/rsv_hrl", rsv_hrl});
        rsv->level.regulation_max = apoint_ts{"shyft://test/rsv_hrl"};

        apoint_ts rsv_max_vol(ta, 3.0, average);
        tsv.emplace_back(apoint_ts{"shyft://test/rsv_max_vol", rsv_max_vol});
        rsv->volume.static_max = apoint_ts{"shyft://test/rsv_max_vol"};

        // An expression
        rsv->volume.result = 0.5*rsv->volume.static_max + rsv->level.regulation_min;
        // 1.2 Unit:
        auto u = std::dynamic_pointer_cast<unit>(hps->find_unit_by_id(1));

        apoint_ts u_cost_start(ta, 4.0, average);
        tsv.emplace_back(apoint_ts{"shyft://test/u_cost_start", u_cost_start});
        u->cost.start = apoint_ts{"shyft://test/u_cost_start"};

        apoint_ts u_cost_stop(ta, 5.0, average);
        tsv.emplace_back(apoint_ts{"shyft://test/u_cost_stop", u_cost_stop});
        u->cost.stop = apoint_ts{"shyft://test/u_cost_stop"};
        u->production.result = u->cost.start * u->cost.stop;

        // 1.3 Power plant:
        auto pp = std::dynamic_pointer_cast<power_plant>(hps->find_power_plant_by_id(2));
        apoint_ts pp_mip(ta, 6.0, average);
        tsv.emplace_back(apoint_ts{"shyft://test/pp_mip", pp_mip});
        pp->mip = apoint_ts{"shyft://test/pp_mip"};
        pp->production.schedule = u->production.result + 0.5;

        // 1.4 waterway:
        auto ww = std::dynamic_pointer_cast<waterway>(hps->find_waterway_by_id(1));
        apoint_ts d_max(ta, 6.0, average);
        tsv.emplace_back(ww->discharge.static_max.id(), d_max);
        // 1.5 gate:
        auto gt = std::dynamic_pointer_cast<gate>(hps->find_gate_by_id(1));
        apoint_ts g_flow(ta, 6.0, average);
        tsv.emplace_back(gt->discharge.result.id(), g_flow);

        // 1.6 reservoir reservoir_aggregate
        auto ra = std::dynamic_pointer_cast<reservoir_aggregate>(hps->find_reservoir_aggregate_by_id(2));
        apoint_ts ra_vol_schedule(ta, 6.0, average);
        tsv.emplace_back(ra->volume.schedule.id(), ra_vol_schedule);

        // 1.7 catchments
        auto ca = std::dynamic_pointer_cast<catchment>(hps->find_catchment_by_id(1));
        apoint_ts ca_inflow(ta, 6.0, average);
        tsv.emplace_back(ca->inflow_m3s.id(), ca_inflow);

        // 2. Market
        apoint_ts m_price(ta, 7.0, average);
        tsv.emplace_back(apoint_ts{"shyft://test/m_price", m_price});
        market->price = apoint_ts{"shyft://test/m_price"};

        apoint_ts m_sale(ta, 8.0, average);
        tsv.emplace_back(apoint_ts{"shyft://test/m_sale", m_sale});
        market->sale = apoint_ts{"shyft://test/m_sale"};
        market->load = pp->production.schedule * market->sale;

        // Store and return
        dtss.do_store_ts(tsv, true, true);
        return mdl;
    }
}
