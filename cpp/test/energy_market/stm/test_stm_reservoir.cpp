#include <string>
#include <doctest/doctest.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <boost/format.hpp>
#include <shyft/energy_market/a_wrap.h>

#include <test/energy_market/stm/utilities.h>

using namespace std;
using namespace shyft::energy_market::stm;
using boost::format;
namespace dont_use_this_now {
    using shyft::energy_market::url_fx_t;
    // for testing out the a_wrap class without the py dependency.
    template <typename A>
    struct a_wrap {
        url_fx_t url_fx; ///< the callable url_fx, provided by the python attribute expose of the a_wrap<T>.. 
        std::string a_name;///< the visible attribute name in python.
        A& a;///< attribute object reference, to location within the  owning object(might be nested aggregate in e.g. unit,)
        
        a_wrap()=delete;///< don't allow default construct.
        a_wrap(url_fx_t &&url_fx,std::string a_name,A& a):url_fx{url_fx},a_name{a_name},a{a} {}
        a_wrap(const a_wrap &c):url_fx{c.url_fx},a_name{c.a_name},a{c.a} {}
        a_wrap ( a_wrap&&c):url_fx{std::move(c.url_fx)},a_name{c.a_name},a{c.a} {}
        
        
        /** @brief generate an almost unique, url-like string for a proxy exposed attribute.
         * 
         * @param prefix: What the resulting string will start with
         * @param levels: How many levels of the url to include.
         *      levels== 0 includes only this level. (Use levels < 0 to get all levels)
         * @param placeholders: The last element of the vector states whether to use the attribute ID or place_holder
         *  "${attr_id}". The remaining vector will be used in subsequent levels of the url.
         *  If the vector is empty, the function defaults to not using placeholder in the url
         */
        std::string url(std::string prefix="",int levels=-1,int template_levels=-1) const {
            std::string s;
            auto sbi=std::back_inserter(s);
            std::copy(begin(prefix),end(prefix),sbi);
            url_fx(sbi,levels,template_levels,"");
            std::string attr_part= template_levels==0?std::string("${attr_id}"):a_name;
            return (format("%1%.%2%")%s%attr_part).str(); 
        }

        /** @brief exits returns true if the attribute is kind of non-null, false otherwise */
        bool exists() const {
            if constexpr(std::is_same<A,apoint_ts>::value) {
                return a.ts.get()?true:false;
            } else { // just assume it's a shared-ptr, compiler will tell you if not
              return a.get()?true:false;  
            }
        }
        /** @brief remove() nullfies the attribute, so that after call .exists() ==false */
        void remove() {
            if constexpr(std::is_same<A,apoint_ts>::value) {
                a=apoint_ts();
            } else {
                a.reset(); // just assume it's a shared-ptr, compiler will tell you if not
            }
        }
        
        /** @brief equal operator, propagate to the attribute type */
        bool operator==( a_wrap const&b) const { 
            if constexpr(std::is_same<A,apoint_ts>::value) {
                return a==b.a;
            } else {
                return (!a && !b.a) || (a&&b.a && *a == *b.a);// ... we could utilize stm::equal.. here.. 
            }
        }
        bool operator!=( a_wrap const&other) const {return !operator==(other);};
    };
}

using shyft::time_series::dd::apoint_ts;
using shyft::energy_market::sbi_t;
using shyft::energy_market::a_wrap;
using namespace shyft::energy_market::stm;// enable equal operatos
using builder = shyft::energy_market::stm::stm_hps_builder;

TEST_SUITE("stm_reservoir"){
	TEST_CASE("stm_reservoir_basics") {
        const auto name{ "res_one"s };
        auto shp=make_shared<stm_hps>(1,"sys");
        stm_hps_builder b(shp);
        string json_string("some json");
        const auto r = b.create_reservoir(1, name,json_string);
        CHECK_EQ(r->id, 1);
        CHECK_EQ(r->name, name);
        CHECK_EQ(r->json,json_string);
        CHECK_THROWS_AS(b.create_reservoir(1,name+"a",""),stm_rule_exception);
        CHECK_THROWS_AS(b.create_reservoir(2,name,""),stm_rule_exception);
        string s;
        sbi_t sbi(s);
        r->inflow.url_fx(sbi,-1,-1,".schedule");
        CHECK_EQ(s,"/H1/R1.inflow.schedule");

        // verify a_wrap at primary and secondary-nested levels
        a_wrap<apoint_ts> ris(
            [o=&r->inflow](sbi_t& so,int l, int tl,string_view sv)->void {
               o->url_fx(so,l,tl,sv);
            },
            "schedule",
            r->inflow.schedule
        );

        CHECK_EQ(ris.url("",0,-1),".inflow.schedule");
        CHECK_EQ(ris.url("",0,0),".${attr_id}");
        CHECK_EQ(ris.url("",0,1),".inflow.schedule");

        CHECK_EQ(ris.url("",1,-1),"/R1.inflow.schedule");
        CHECK_EQ(ris.url("",1,0),"/R${rsv_id}.${attr_id}");
        CHECK_EQ(ris.url("",1,1),"/R${rsv_id}.inflow.schedule");
        CHECK_EQ(ris.url("",1,2),"/R1.inflow.schedule");

        CHECK_EQ(ris.url("",2,-1),"/H1/R1.inflow.schedule");
        CHECK_EQ(ris.url("",2,0),"/H${hps_id}/R${rsv_id}.${attr_id}");
        CHECK_EQ(ris.url("",2,1),"/H${hps_id}/R${rsv_id}.inflow.schedule");
        CHECK_EQ(ris.url("",2,2),"/H${hps_id}/R1.inflow.schedule");
        CHECK_EQ(ris.url("",2,3),"/H1/R1.inflow.schedule");
        string a_name="water_value.endpoint_desc";
        string a_id="${attr_id}";
        a_wrap<apoint_ts> res(
            [o=r,a_name,a_id](sbi_t& so,int l, int tl,string_view /*sv*/)->void {
                if(l)
                    o->generate_url(so,l-1,tl>0?tl-1:tl);
            },
            a_name,
            r->water_value.endpoint_desc
        );
        CHECK_EQ(res.url("",0,-1),".water_value.endpoint_desc");
        CHECK_EQ(res.url("",0,0),".${attr_id}");
        CHECK_EQ(res.url("",0,1),".water_value.endpoint_desc");

        CHECK_EQ(res.url("",1,-1),"/R1.water_value.endpoint_desc");
        CHECK_EQ(res.url("",1,0),"/R${rsv_id}.${attr_id}");
        CHECK_EQ(res.url("",1,1),"/R${rsv_id}.water_value.endpoint_desc");
        CHECK_EQ(res.url("",1,2),"/R1.water_value.endpoint_desc");
        r->volume_level_mapping=make_shared<t_xy_::element_type>();
        auto level_volume = make_shared<xy_point_curve_::element_type>();
        using shyft::energy_market::hydro_power::point;
        using shyft::core::from_seconds;
        (*level_volume).points.push_back(point(1000,0));
        (*level_volume).points.push_back(point(1010,10));
        (*r->volume_level_mapping)[from_seconds(0)]= level_volume;
        auto blob=stm_hps::to_blob(shp);
        auto o=stm_hps::from_blob(blob);
        auto r2=std::dynamic_pointer_cast<reservoir>(o->reservoirs[0]);
        CHECK_EQ(r2->id,r->id);
        CHECK_EQ(true,r2->volume_level_mapping.get()!=nullptr);
        auto const& m1=*r2->volume_level_mapping;
        auto const& m2=*r->volume_level_mapping;
        CHECK_EQ(m1.size(),m2.size());
        for(auto const&kv:m2) {
            auto f=m1.find(kv.first);
            if(f==m1.end()) {
                MESSAGE("Failed to find key");
                CHECK(f==m1.end());
                continue;
            }
            if( *(*f).second != (*kv.second) ) {
                MESSAGE("Different values..");
                CHECK(false);//
            }
        }
    }

    TEST_CASE("stm_reservoir_equality") {

        // Test that stm::reservoir::operator== checks stm attributes
        auto hps_1 = make_shared<stm_hps>(1, "test_hps_1");
        auto hps_2 = make_shared<stm_hps>(2, "test_hps_2");

        auto rsv_1 = builder(hps_1).create_reservoir(11, "test", "");
        auto rsv_2 = builder(hps_2).create_reservoir(11, "test", "");

        using shyft::energy_market::hydro_power::xy_point_curve;

        // All attributes left at default, objects should be equal
        CHECK_EQ(*rsv_1, *rsv_2);

        // Add some attributes to first object, no longer equal
        rsv_1->level.regulation_min = test::create_t_double(1, 101.0);
        rsv_1->volume_level_mapping = test::create_t_xy(1, {0., 100.0, 1., 101.0});
        CHECK_NE(*rsv_1, *rsv_2);

        // Add attributes to other object, now equal again
        rsv_2->level.regulation_min = test::create_t_double(1, 101.0);
        rsv_2->volume_level_mapping = test::create_t_xy(1, {0., 100.0, 1., 101.0});
        CHECK_EQ(*rsv_1, *rsv_2);
    }
    }

TEST_SUITE("stm_reservoir_aggregate"){
    TEST_CASE("stm_reservoir_aggregate_equality") {
        // Test that stm::reservoir_agrgegate::operator== checks stm attributes
        auto hps_1 = make_shared<stm_hps>(1, "test_hps_1");
        auto hps_2 = make_shared<stm_hps>(2, "test_hps_2");

        auto ra_1 = builder(hps_1).create_reservoir_aggregate(11, "test", "");
        auto ra_2 = builder(hps_2).create_reservoir_aggregate(11, "test", "");

        auto r_1 = builder(hps_1).create_reservoir(14, "test", "");
        auto r_2 = builder(hps_2).create_reservoir(14, "test", "");

        // All attributes left at default, objects should be equal
        CHECK_EQ(*ra_1, *ra_2);

        // Add reservoir to one reservoir aggregate, no longer equal
        reservoir_aggregate::add_reservoir(ra_1, r_1);
        CHECK_NE(*ra_1, *ra_2);

        // Add reservoir to the other reservoir aggregate, equal again
        reservoir_aggregate::add_reservoir(ra_2, r_2);
        CHECK_EQ(*ra_1, *ra_2);

        // Add some attributes to first object, no longer equal
        ra_1->volume.static_max = test::create_t_double(1, 0.01);
		CHECK_NE(*ra_1, *ra_2);

        // Add attributes to other object, now equal again
        ra_2->volume.static_max = test::create_t_double(1, 0.01);
        CHECK_EQ(*ra_1, *ra_2);

        // Add reservoir attribute to first object, no longer equal
        r_1->inflow.schedule = test::create_t_double(1, 10.0);
        CHECK_NE(*ra_1, *ra_2);

        // Add reservoir attribute to other object, now equal again
        r_2->inflow.schedule = test::create_t_double(1, 10.0);
        CHECK_EQ(*ra_1, *ra_2);
    }
	
    TEST_CASE("stm_reservoir_aggregate_basics") {
        const auto name{ "res_one"s };
        auto shp=make_shared<stm_hps>(1,"sys");
        stm_hps_builder b(shp);
        string json_string("some json");
        const auto ra = b.create_reservoir_aggregate(1, name,json_string);
        // Check that attributes were correctly initialized
        CHECK_EQ(ra->id, 1);
        CHECK_EQ(ra->name, name);
        CHECK_EQ(ra->json,json_string);
        // Test that adding reservoir aggregate with same id throws
        CHECK_THROWS_AS(b.create_reservoir_aggregate(1,name+"a",""),stm_rule_exception);
        // Test that adding reservoir aggregate with same name throws
        CHECK_THROWS_AS(b.create_reservoir_aggregate(2,name,""),stm_rule_exception);

        // Test url
        string s;
        sbi_t sbi(s);
        ra->inflow.url_fx(sbi,-1,-1,".schedule");
        CHECK_EQ(s,"/H1/A1.inflow.schedule");
        
        // verify a_wrap at secondary-nested levels
        a_wrap<apoint_ts> ris(
            [o=&ra->inflow](sbi_t& so,int l, int tl,string_view sv)->void {
               o->url_fx(so,l,tl,sv); 
            },
            "schedule",
            ra->inflow.schedule
        );

        CHECK_EQ(ris.url("",0,-1),".inflow.schedule");
        CHECK_EQ(ris.url("",0,0),".${attr_id}");
        CHECK_EQ(ris.url("",0,1),".inflow.schedule");

        CHECK_EQ(ris.url("",1,-1),"/A1.inflow.schedule");
        CHECK_EQ(ris.url("",1,0),"/A${rsv_agg_id}.${attr_id}");
        CHECK_EQ(ris.url("",1,1),"/A${rsv_agg_id}.inflow.schedule");
        CHECK_EQ(ris.url("",1,2),"/A1.inflow.schedule");
        
        CHECK_EQ(ris.url("",2,-1),"/H1/A1.inflow.schedule");
        CHECK_EQ(ris.url("",2,0),"/H${hps_id}/A${rsv_agg_id}.${attr_id}");
        CHECK_EQ(ris.url("",2,1),"/H${hps_id}/A${rsv_agg_id}.inflow.schedule");
        CHECK_EQ(ris.url("",2,2),"/H${hps_id}/A1.inflow.schedule");
        CHECK_EQ(ris.url("",2,3),"/H1/A1.inflow.schedule");

		ra->volume.static_max = test::create_t_double(1, 0.01);
        auto blob=stm_hps::to_blob(shp);
        auto o=stm_hps::from_blob(blob);
        auto ra2=std::dynamic_pointer_cast<reservoir_aggregate>(o->reservoir_aggregates[0]);
        CHECK_EQ(*ra2,*ra);

        // Test for shared_from_this function
        CHECK_EQ(ra, ra->shared_from_this());
        // Test that returns nullptr hps is missing
        auto ra3 = make_shared<reservoir_aggregate>(3, "ra3", "", nullptr);
        CHECK_EQ(nullptr, ra3->shared_from_this());
        // Test that returns nullptr if object is missing in the hps
        auto ra4 = make_shared<reservoir_aggregate>(4, "ra4", "", shp);
        CHECK_EQ(nullptr, ra4->shared_from_this());

        const auto r = b.create_reservoir(2, "r",json_string);

        // Tests for add_reservoir function
        //Test successful add
        reservoir_aggregate::add_reservoir(ra, r);
        CHECK(ra->reservoirs.size() == 1);
        CHECK(ra->reservoirs[0] == r);
        CHECK(ra == r->rsv_aggregate_());

        //Test add fails if reservoir already added
        CHECK_THROWS_AS(reservoir_aggregate::add_reservoir(ra, r), runtime_error);

        //Test add fails if reservoir aggregate misses hps
        auto r2 = make_shared<reservoir>(2, "r2", "", shp);
        CHECK_THROWS_AS(reservoir_aggregate::add_reservoir(ra3, r2), runtime_error);

        // Test that adding fails if the reservoir aggregate is missing from the hps
        const auto r3 = b.create_reservoir(3, "r3",json_string);
        CHECK_THROWS_AS(reservoir_aggregate::add_reservoir(ra4, r3), runtime_error);

        //Test that adding fails if reservoir already added to another reservoir_aggregate
        const auto ra5 = b.create_reservoir_aggregate(5, "ra5",json_string);
        CHECK_THROWS_AS(reservoir_aggregate::add_reservoir(ra5, r), runtime_error);

        //Test remove_reservoir function
        ra->remove_reservoir(r);
        CHECK(ra->reservoirs.size() == 0);
        CHECK(nullptr == r->rsv_aggregate_());

        //Test that removing from empty reservoir aggregate does nothing
        ra->remove_reservoir(r);
        CHECK(ra->reservoirs.size() == 0);

        //Test that adding a reservoir back works
        reservoir_aggregate::add_reservoir(ra, r);
        CHECK(ra->reservoirs.size() == 1);
        CHECK(ra->reservoirs[0] == r);
        CHECK(ra == r->rsv_aggregate_());


} // end TEST_CASE


} // end TEST_SUITE
