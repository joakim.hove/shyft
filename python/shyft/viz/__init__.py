from ..hydrology.viz import *
import warnings
warnings.warn("shyft.viz module is deprecated, use shyft.hydrology.viz instead", DeprecationWarning,
              stacklevel=2)