from ..time_series import *
from ..hydrology import *
# Deprecated: This module is for bw compat only
import warnings
warnings.warn("shyft.api module is deprecated, use shyft.hydrology instead", DeprecationWarning,
              stacklevel=2)