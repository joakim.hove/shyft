from ..hydrology.repository import *
import warnings
warnings.warn("shyft.repository module is deprecated, use shyft.hydrology.repository instead", DeprecationWarning,
              stacklevel=2)
