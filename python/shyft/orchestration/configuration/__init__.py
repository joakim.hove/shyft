from ...hydrology.orchestration.configuration import *
import warnings
warnings.warn("shyft.orchestration.configuration module is deprecated, use shyft.hydrology.orchestration.configuration instead", DeprecationWarning,
              stacklevel=2)