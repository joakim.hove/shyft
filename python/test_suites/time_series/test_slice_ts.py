import numpy as np
import pytest
from shyft.time_series import TsVector, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE, Calendar


def test_ts_slice():

    # Test data
    values = np.linspace(1, 4, 4)
    dt = 3600
    times = [0.0] + [dt*v for v in values]

    # Make three TimeSeries with with the same data in three different TimeAxes
    tsv = TsVector()
    tsv.append(TimeSeries(ta=TimeAxis(times[0], dt, len(values)),
                          values=values, point_fx=POINT_AVERAGE_VALUE))
    tsv.append(TimeSeries(ta=TimeAxis(Calendar(), times[0], dt, len(values)),
                          values=values, point_fx=POINT_AVERAGE_VALUE))
    tsv.append(TimeSeries(ta=TimeAxis(times),
                          values=values, point_fx=POINT_AVERAGE_VALUE))

    for ts in tsv:

        # Make all possible valid slices of all timeseries
        for start in range(len(values)):
            for n in range(1, len(values) - start + 1):
                sliced = ts.slice(start, n)

                # We should have a sliced TimeSeriew with n elements and identical point interpretation
                assert len(sliced) == n
                assert len(sliced.time_axis) == n
                assert sliced.point_interpretation() == ts.point_interpretation()

                # Verify n identical time_points and values
                for i in range(n):
                    assert sliced.value(i) == ts.value(start + i)
                    assert sliced.time_axis.time(i) == ts.time_axis.time(start + i)

                # Verify last time point
                last_ix = start + n
                end = ts.time_axis.total_period().end
                if last_ix < len(ts):
                    end = ts.time_axis.time(last_ix)
                assert sliced.time_axis.total_period().end == end

        # Then verify that invalid slices fail
        with pytest.raises(RuntimeError):
            ts.slice(-10, 1)  # Start before beginning
        with pytest.raises(RuntimeError):
            ts.slice(10, 1)  # Start after end
        with pytest.raises(RuntimeError):
            ts.slice(0, 10)  # Request too many items
        with pytest.raises(RuntimeError):
            ts.slice(0, -10)  # Request too few items
        with pytest.raises(RuntimeError):
            ts.slice(0, 0)  # Request no items
