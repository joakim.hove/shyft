from shyft.time_series import TimeSeries, TimeAxis, TsVector, DoubleVector, POINT_INSTANT_VALUE
import numpy as np


def test_basic_case():
    ta = TimeAxis(0, 10, 6)
    to = TimeAxis(10, 20, 3)
    va = DoubleVector([0, 10, 20, 30, 40.0, 50.0])
    a = TimeSeries(ta, va, POINT_INSTANT_VALUE)
    o = TimeSeries(to, fill_value=0.0, point_fx=POINT_INSTANT_VALUE)  # point-ip should not matter
    r = a.use_time_axis_from(o)
    assert r is not None
    assert r.time_axis == o.time_axis
    ev = DoubleVector([10.0, 30.0, 50.0]).to_numpy()
    assert np.allclose(r.values.to_numpy(), ev)
    assert r.point_interpretation() == a.point_interpretation()
    tsv = TsVector([a, 2.0*a])
    rv = tsv.use_time_axis_from(o)
    for x in rv:
        assert x.time_axis == o.time_axis
    assert np.allclose(rv[0].values.to_numpy(), ev)
    assert np.allclose(rv[1].values.to_numpy(), 2.0*ev)
