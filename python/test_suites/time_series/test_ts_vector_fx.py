import math
from functools import reduce
from operator import add

import numpy as np
from numpy.testing import assert_array_almost_equal

from shyft.time_series import (ts_vector_values_at_time, TimeSeries, point_interpretation_policy, IntVector, TimeAxis, min, max)

from shyft.time_series import (Calendar, utctime_now, DoubleVector, TsVector,
                               POINT_INSTANT_VALUE, POINT_AVERAGE_VALUE,
                               TimeAxisFixedDeltaT, TimeAxisByPoints,
                               TsPoint, TsFixed, TsFactory, UtcTimeVector, Int64Vector, AverageAccessorTs,
                               deltahours, deltaminutes,time)


class TimeSeriesFixture:
    """    common stuff for ts tests
    """

    def __init__(self):
        self.c = Calendar()
        self.d = deltahours(1)
        self.n = 24
        self.t = self.c.trim(utctime_now(), self.d)
        self.ta = TimeAxisFixedDeltaT(self.t, self.d, self.n)


def test_a_time_series_vector():
    c = Calendar()
    t0 = c.time(2018, 7, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxisFixedDeltaT(t0, dt, n)

    a = TimeSeries(ta=ta, fill_value=3.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    b = TimeSeries(ta=ta, fill_value=2.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    c = TimeSeries(ta=ta, fill_value=10.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    v = TsVector()
    v.append(a)
    v.append(b)

    assert len(v) == 2
    assert round(abs(v[0].value(0) - 3.0), 7) == 0, "expect first ts to be 3.0"
    aa = TimeSeries(ta=a.time_axis, values=a.values,
                    point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)  # copy construct (really copy the values!)
    a.fill(1.0)
    assert round(abs(v[0].value(0) - 1.0), 7) == 0, "expect first ts to be 1.0, because the vector keeps a reference "
    assert round(abs(aa.value(0) - 3.0), 7) == 0

    vt = v.values_at(t0).to_numpy()
    assert len(vt) == len(v)
    v1 = v[0:1]
    assert len(v1) == 1
    assert round(abs(v1[0].value(0) - 1.0), 7) == 0
    v_clone = TsVector(v)
    assert len(v_clone) == len(v)
    del v_clone[-1]
    assert len(v_clone) == 1
    assert len(v) == 2
    v_slice_all = v.slice(IntVector())
    v_slice_1 = v.slice(IntVector([1]))
    v_slice_12 = v.slice(IntVector([0, 1]))
    assert len(v_slice_all) == 2
    assert len(v_slice_1) == 1
    assert round(abs(v_slice_1[0].value(0) - 2.0), 7) == 0
    assert len(v_slice_12) == 2
    assert round(abs(v_slice_12[0].value(0) - 1.0), 7) == 0

    # multiplication by scalar
    v_x_2a = v*2.0
    v_x_2b = 2.0*v
    for i in range(len(v)):
        assert round(abs(v_x_2a[i].value(0) - 2*v[i].value(0)), 7) == 0
        assert round(abs(v_x_2b[i].value(0) - 2*v[i].value(0)), 7) == 0

    # division by scalar
    v_d_a = v/3.0
    v_d_b = 3.0/v
    for i in range(len(v)):
        assert round(abs(v_d_a[i].value(0) - v[i].value(0)/3.0), 7) == 0
        assert round(abs(v_d_b[i].value(0) - 3.0/v[i].value(0)), 7) == 0

    # addition by scalar
    v_a_a = v + 3.0
    v_a_b = 3.0 + v
    for i in range(len(v)):
        assert round(abs(v_a_a[i].value(0) - (v[i].value(0) + 3.0)), 7) == 0
        assert round(abs(v_a_b[i].value(0) - (3.0 + v[i].value(0))), 7) == 0

    # sub by scalar
    v_s_a = v - 3.0
    v_s_b = 3.0 - v
    for i in range(len(v)):
        assert round(abs(v_s_a[i].value(0) - (v[i].value(0) - 3.0)), 7) == 0
        assert round(abs(v_s_b[i].value(0) - (3.0 - v[i].value(0))), 7) == 0

    # multiplication vector by ts
    v_x_ts = v*c
    ts_x_v = c*v
    for i in range(len(v)):
        assert round(abs(v_x_ts[i].value(0) - v[i].value(0)*c.value(0)), 7) == 0
        assert round(abs(ts_x_v[i].value(0) - c.value(0)*v[i].value(0)), 7) == 0

    # division vector by ts
    v_d_ts = v/c
    ts_d_v = c/v
    for i in range(len(v)):
        assert round(abs(v_d_ts[i].value(0) - v[i].value(0)/c.value(0)), 7) == 0
        assert round(abs(ts_d_v[i].value(0) - c.value(0)/v[i].value(0)), 7) == 0

    # add vector by ts
    v_a_ts = v + c
    ts_a_v = c + v
    for i in range(len(v)):
        assert round(abs(v_a_ts[i].value(0) - (v[i].value(0) + c.value(0))), 7) == 0
        assert round(abs(ts_a_v[i].value(0) - (c.value(0) + v[i].value(0))), 7) == 0

    # sub vector by ts
    v_s_ts = v - c
    ts_s_v = c - v
    for i in range(len(v)):
        assert round(abs(v_s_ts[i].value(0) - (v[i].value(0) - c.value(0))), 7) == 0
        assert round(abs(ts_s_v[i].value(0) - (c.value(0) - v[i].value(0))), 7) == 0

    # vector mult vector
    va = v
    vb = 2.0*v

    v_m_v = va*vb
    assert len(v_m_v) == len(va)
    for i in range(len(va)):
        assert round(abs(v_m_v[i].value(0) - va[i].value(0)*vb[i].value(0)), 7) == 0

    # vector div vector
    v_d_v = va/vb
    assert len(v_d_v) == len(va)
    for i in range(len(va)):
        assert round(abs(v_d_v[i].value(0) - va[i].value(0)/vb[i].value(0)), 7) == 0

    # vector add vector
    v_a_v = va + vb
    assert len(v_a_v) == len(va)
    for i in range(len(va)):
        assert round(abs(v_a_v[i].value(0) - (va[i].value(0) + vb[i].value(0))), 7) == 0

    # vector sub vector
    v_s_v = va - vb
    assert len(v_s_v) == len(va)
    for i in range(len(va)):
        assert round(abs(v_s_v[i].value(0) - (va[i].value(0) - vb[i].value(0))), 7) == 0

    # vector unary minus
    v_u = - va
    assert len(v_u) == len(va)
    for i in range(len(va)):
        assert round(abs(v_u[i].value(0) - (-va[i].value(0))), 7) == 0

    # integral functions, just to verify exposure works, and one value is according to spec.
    ta2 = TimeAxis(t0, dt*24, n//24)
    v_avg = v.average(ta2)
    v_int = v.integral(ta2)
    v_acc = v.accumulate(ta2)
    v_sft = v.time_shift(dt*24)
    assert v_avg is not None
    assert v_int is not None
    assert v_acc is not None
    assert v_sft is not None
    assert round(abs(v_avg[0].value(0) - 1.0), 7) == 0
    assert round(abs(v_int[0].value(0) - 86400.0), 7) == 0
    assert round(abs(v_acc[0].value(0) - 0.0), 7) == 0
    assert v_sft[0].time(0) == (t0 + dt*24)

    # min/max functions
    min_v_double = va.min(-1000.0)
    max_v_double = va.max(1000.0)
    assert round(abs(min_v_double[0].value(0) - (-1000.0)), 7) == 0
    assert round(abs(max_v_double[0].value(0) - (+1000.0)), 7) == 0
    min_v_double = min(va, -1000.0)
    max_v_double = max(va, +1000.0)
    assert round(abs(min_v_double[0].value(0) - (-1000.0)), 7) == 0
    assert round(abs(max_v_double[0].value(0) - (+1000.0)), 7) == 0
    # c = 10.0
    c1000 = 100.0*c
    min_v_double = va.min(-c1000)
    max_v_double = va.max(c1000)
    assert round(abs(min_v_double[0].value(0) - (-c1000.value(0))), 7) == 0
    assert round(abs(max_v_double[0].value(0) - (c1000.value(0))), 7) == 0
    min_v_double = min(va, -c1000)
    max_v_double = max(va, c1000)
    assert round(abs(min_v_double[0].value(0) - (-c1000.value(0))), 7) == 0
    assert round(abs(max_v_double[0].value(0) - (c1000.value(0))), 7) == 0

    v1000 = va*1000.0
    min_v_double = va.min(-v1000)
    max_v_double = va.max(v1000)
    assert round(abs(min_v_double[0].value(0) - (-v1000[0].value(0))), 7) == 0
    assert round(abs(max_v_double[0].value(0) - (v1000[0].value(0))), 7) == 0
    min_v_double = min(va, -v1000)
    max_v_double = max(va, v1000)
    assert round(abs(min_v_double[0].value(0) - (-v1000[0].value(0))), 7) == 0
    assert round(abs(max_v_double[0].value(0) - (v1000[0].value(0))), 7) == 0

    # finally, test that exception is raised if we try to multiply two unequal sized vectors

    try:
        x = v_clone*va
        assert False, 'We expected exception for unequal sized ts-vector op'
    except RuntimeError as re:
        pass

    # also test that empty vector + vector -> vector etc.
    va_2 = va + TsVector()
    va_3 = TsVector() + va
    va_4 = va - TsVector()
    va_5 = TsVector() - va
    va_x = TsVector() + TsVector()
    assert len(va_2) == len(va)
    assert len(va_3) == len(va)
    assert len(va_4) == len(va)
    assert len(va_5) == len(va)
    assert (not va_x) == True
    assert (not va_2) == False
    va_2_ok = False
    va_x_ok = True
    if va_2:
        va_2_ok = True
    if va_x:
        va_x_ok = False
    assert va_2_ok
    assert va_x_ok


def test_extend_vector_of_timeseries():
    t0 = utctime_now()
    dt = deltahours(1)
    n = 512

    tsvector = TsVector()

    ta = TimeAxisFixedDeltaT(t0 + 3*n*dt, dt, 2*n)

    tsvector.push_back(TimeSeries(
        ta=TimeAxisFixedDeltaT(t0, dt, 2*n),
        fill_value=1.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE))
    tsvector.push_back(TimeSeries(
        ta=TimeAxisFixedDeltaT(t0 + 2*n*dt, dt, 2*n),
        fill_value=2.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE))

    extension = TimeSeries(ta=ta, fill_value=8.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    # extend after all time-series in the vector
    extended_tsvector = tsvector.extend_ts(extension)

    # assert first element
    for i in range(2*n):
        assert extended_tsvector[0](t0 + i*dt) == 1.0
    for i in range(n):
        assert math.isnan(extended_tsvector[0](t0 + (2*n + i)*dt))
    for i in range(2*n):
        assert extended_tsvector[0](t0 + (3*n + i)*dt) == 8.0

    # assert second element
    for i in range(2*n):
        assert extended_tsvector[1](t0 + (2*n + i)*dt) == 2.0
    for i in range(n):
        assert extended_tsvector[1](t0 + (4*n + i)*dt) == 8.0

    tsvector_2 = TsVector()
    tsvector_2.push_back(TimeSeries(
        ta=TimeAxisFixedDeltaT(t0 + 2*n*dt, dt, 4*n),
        fill_value=10.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE))
    tsvector_2.push_back(TimeSeries(
        ta=TimeAxisFixedDeltaT(t0 + 4*n*dt, dt, 4*n),
        fill_value=20.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE))

    # extend each element in tsvector by the corresponding element in tsvector_2
    extended_tsvector = tsvector.extend_ts(tsvector_2)

    # assert first element
    for i in range(2*n):
        assert extended_tsvector[0](t0 + i*dt) == 1.0
    for i in range(4*n):
        assert extended_tsvector[0](t0 + (2*n + i)*dt) == 10.0

    # assert second element
    for i in range(2*n):
        assert extended_tsvector[1](t0 + (2*n + i)*dt) == 2.0
    for i in range(4*n):
        assert extended_tsvector[1](t0 + (4*n + i)*dt) == 20.0


def test_vector_of_timeseries():
    u = TimeSeriesFixture()
    dv = np.arange(u.ta.size())
    v = DoubleVector.from_numpy(dv)
    tsf = TsFactory()
    tsa = tsf.create_point_ts(u.n, u.t, u.d, v)
    tsvector = TsVector()
    assert len(tsvector) == 0
    tsvector.push_back(tsa)
    assert len(tsvector) == 1
    tsvector.push_back(tsa)
    vv = tsvector.values_at_time(u.ta.time(3))  # verify it's easy to get out vectorized results at time t
    assert len(vv) == len(tsvector)
    assert round(abs(vv[0] - 3.0), 7) == 0
    assert round(abs(vv[1] - 3.0), 7) == 0
    ts_list = [tsa, tsa]
    vv = ts_vector_values_at_time(ts_list, u.ta.time(4))  # also check it work with list(TimeSeries)
    assert len(vv) == len(tsvector)
    assert round(abs(vv[0] - 4.0), 7) == 0
    assert round(abs(vv[1] - 4.0), 7) == 0


def test_timeseries_vector():
    t0 = utctime_now()
    dt = deltahours(1)
    n = 240
    ta = TimeAxisFixedDeltaT(t0, dt, n)

    a = TimeSeries(ta=ta, fill_value=3.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    b = TimeSeries(ta=ta, fill_value=2.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    v = TsVector()
    v.append(a)
    v.append(b)

    assert len(v) == 2
    assert round(abs(v[0].value(0) - 3.0), 7) == 0, "expect first ts to be 3.0"
    aa = TimeSeries(ta=a.time_axis, values=a.values,
                    point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)  # copy construct (really copy the values!)
    a.fill(1.0)
    assert round(abs(v[0].value(0) - 1.0), 7) == 0, "expect first ts to be 1.0, because the vector keeps a reference "
    assert round(abs(aa.value(0) - 3.0), 7) == 0


def test_timeseries_vector_logarithm():
    t0 = utctime_now()
    dt = deltahours(1)
    n = 240
    ta = TimeAxisFixedDeltaT(t0, dt, n)

    a = TimeSeries(ta=ta, fill_value=3.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    b = TimeSeries(ta=ta, fill_value=2.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    tsv = TsVector([a, b])
    tsv_log = tsv.log()

    assert len(tsv) == len(tsv_log)
    assert len(tsv[0]) == len(tsv_log[0])
    assert len(tsv[1]) == len(tsv_log[1])
    for i in range(n):
        assert abs(tsv_log[0].value(i) - np.log(3.0)) < 1e-5
        assert abs(tsv_log[1].value(i) - np.log(2.0)) < 1e-5


def test_tsvector_sum():
    time_axis = TimeAxis(time('2020-03-19T00:00:00Z'), time(3600), 24)
    v = TsVector([TimeSeries(time_axis, fill_value=float(i), point_fx=POINT_AVERAGE_VALUE) for i in range(20)])
    v_bin_ary_sum = reduce(add, v)  # this one is quite fast!
    v_n_ary_sum = v.sum()           # this one is even faster, especially when expressions, n, are high

    assert v_n_ary_sum == v_bin_ary_sum
    assert_array_almost_equal(v_n_ary_sum.values.to_numpy(), v_bin_ary_sum.values.to_numpy())

    # also test with expression time series
    w = TsVector([2.0 * vi for vi in v])
    w_bin_ary_sum = reduce(add, w)  # this one is quite fast!
    w_n_ary_sum = w.sum()  # this one is even faster, especially when expressions, n, are high

    assert w_n_ary_sum == w_bin_ary_sum
    assert_array_almost_equal(w_n_ary_sum.values.to_numpy(), w_bin_ary_sum.values.to_numpy())


def test_operations_on_ts_fixed():
    u = TimeSeriesFixture()
    dv = np.arange(u.ta.size())
    v = DoubleVector.from_numpy(dv)
    # test create
    tsa = TsFixed(u.ta, v, POINT_INSTANT_VALUE)
    # assert its contains time and values as expected.
    assert u.ta.total_period() == tsa.total_period()
    for i in range(u.ta.size()):
        assert round(abs(tsa.value(i) - v[i]), 7) == 0

    for i in range(u.ta.size()):
        assert tsa.time(i) == u.ta(i).start
    for i in range(u.ta.size()):
        assert round(abs(tsa.get(i).v - v[i]), 7) == 0
    # set one value
    v[0] = 122
    tsa.set(0, v[0])
    assert round(abs(v[0] - tsa.value(0)), 7) == 0
    # test fill with values
    for i in range(len(v)): v[i] = 123
    tsa.fill(v[0])
    for i in range(u.ta.size()):
        assert round(abs(tsa.get(i).v - v[i]), 7) == 0


def test_ts_fixed():
    u = TimeSeriesFixture()
    dv = np.arange(u.ta.size())
    v = DoubleVector.from_numpy(dv)
    xv = v.to_numpy()
    assert len(xv) == len(v)
    tsfixed = TsFixed(u.ta, v, POINT_AVERAGE_VALUE)
    assert tsfixed.size() == u.ta.size()
    assert round(abs(tsfixed.get(0).v - v[0]), 7) == 0
    vv = tsfixed.values.to_numpy()  # introduced .values for compatibility
    assert_array_almost_equal(dv, vv)
    tsfixed.values[0] = 10.0
    dv[0] = 10.0
    assert_array_almost_equal(dv, tsfixed.v.to_numpy())
    ts_ta = tsfixed.time_axis  # a TsFixed do have .time_axis and .values
    assert len(ts_ta) == len(u.ta)  # should have same length etc.

    # verify some simple core-ts to TimeSeries interoperability
    full_ts = tsfixed.TimeSeries  # returns a new TimeSeries as clone from tsfixed
    assert full_ts.size() == tsfixed.size()
    for i in range(tsfixed.size()):
        assert full_ts.time(i) == tsfixed.time(i)
        assert round(abs(full_ts.value(i) - tsfixed.value(i)), 5) == 0
    ns = tsfixed.nash_sutcliffe(full_ts)
    assert round(abs(ns - 1.0), 4) == 0
    kg = tsfixed.kling_gupta(full_ts, 1.0, 1.0, 1.0)
    assert round(abs(kg - 1.0), 4) == 0

    # u.assertAlmostEqual(v,vv)
    # some reference testing:
    ref_v = tsfixed.v
    del tsfixed
    assert_array_almost_equal(dv, ref_v.to_numpy())


def test_ts_point():
    u = TimeSeriesFixture()
    dv = np.arange(u.ta.size())
    v = DoubleVector.from_numpy(dv)
    t = UtcTimeVector()
    for i in range(u.ta.size()):
        t.push_back(u.ta(i).start)
    t.push_back(u.ta(u.ta.size() - 1).end)
    ta = TimeAxisByPoints(t)
    tspoint = TsPoint(ta, v, POINT_AVERAGE_VALUE)
    ts_ta = tspoint.time_axis  # a TsPoint do have .time_axis and .values
    assert len(ts_ta) == len(u.ta)  # should have same length etc.

    assert tspoint.size() == ta.size()
    assert round(abs(tspoint.get(0).v - v[0]), 7) == 0
    assert round(abs(tspoint.values[0] - v[0]), 7) == 0  # just to verfy compat .values works
    assert tspoint.get(0).t == ta(0).start
    # verify some simple core-ts to TimeSeries interoperability
    full_ts = tspoint.TimeSeries  # returns a new TimeSeries as clone from tsfixed
    assert full_ts.size() == tspoint.size()
    for i in range(tspoint.size()):
        assert full_ts.time(i) == tspoint.time(i)
        assert round(abs(full_ts.value(i) - tspoint.value(i)), 5) == 0
    ns = tspoint.nash_sutcliffe(full_ts)
    assert round(abs(ns - 1.0), 4) == 0
    kg = tspoint.kling_gupta(full_ts, 1.0, 1.0, 1.0)
    assert round(abs(kg - 1.0), 4) == 0


def test_ts_factory():
    u = TimeSeriesFixture()
    dv = np.arange(u.ta.size())
    v = DoubleVector.from_numpy(dv)
    t = UtcTimeVector()
    ti = Int64Vector()
    for i in range(u.ta.size()):
        t.push_back(u.ta(i).start)
        ti.append(u.ta(i).start.seconds)
    t.push_back(u.ta(u.ta.size() - 1).end)
    tsf = TsFactory()
    ts1 = tsf.create_point_ts(u.ta.size(), u.t, u.d, v)
    ts2 = tsf.create_time_point_ts(u.ta.total_period(), t, v)
    ts1i = tsf.create_point_ts(u.ta.size(), int(u.t.seconds), int(u.d.seconds), v, interpretation=POINT_AVERAGE_VALUE)
    ts2i = tsf.create_time_point_ts(u.ta.total_period(), times=UtcTimeVector(ti), values=v)  # TODO: remove requirement for UtcTimeVector, accept int's
    tslist = TsVector()
    tslist.push_back(ts1)
    tslist.push_back(ts2)
    assert tslist.size() == 2
    assert ts2 == ts2i
    assert not (ts2 != ts2i)


def test_average_accessor():
    u = TimeSeriesFixture()
    dv = np.arange(u.ta.size())
    v = DoubleVector.from_numpy(dv)
    t = UtcTimeVector()
    for i in range(u.ta.size()):
        t.push_back(u.ta(i).start)
    t.push_back(
        u.ta(u.ta.size() - 1).end)  # important! needs n+1 points to determine n periods in the timeaxis
    tsf = TsFactory()
    ts1 = tsf.create_point_ts(u.ta.size(), u.t, u.d, v)
    ts2 = tsf.create_time_point_ts(u.ta.total_period(), t, v)
    tax = TimeAxisFixedDeltaT(u.ta.total_period().start + deltaminutes(30), deltahours(1),
                              u.ta.size())
    avg1 = AverageAccessorTs(ts1, tax)
    assert avg1.size() == tax.size()
    assert ts2 is not None
