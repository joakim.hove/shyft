import numpy as np
from shyft.time_series import Calendar, deltahours, TimeAxis, DoubleVector, TimeSeries, point_interpretation_policy


def test_partition_by():
    """
    verify/demo exposure of the .partition_by function that can
    be used to produce yearly percentiles statistics for long historical
    time-series

    """
    c = Calendar()
    t0 = c.time(1930, 9, 1)
    dt = deltahours(1)
    n = c.diff_units(t0, c.time(2016, 9, 1), dt)

    ta = TimeAxis(t0, dt, n)
    pattern_values = DoubleVector.from_numpy(np.arange(len(ta)))  # increasing values

    src_ts = TimeSeries(ta=ta, values=pattern_values,
                        point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    partition_t0 = c.time(2016, 9, 1)
    n_partitions = 80
    partition_interval = Calendar.YEAR
    # get back TsVector,
    # where all TsVector[i].index_of(partition_t0)
    # is equal to the index ix for which the TsVector[i].value(ix) correspond to start value of that particular partition.
    ts_partitions = src_ts.partition_by(c, t0, partition_interval, n_partitions, partition_t0)
    assert len(ts_partitions) == n_partitions
    ty = t0
    for ts in ts_partitions:
        ix = ts.index_of(partition_t0)
        vix = ts.value(ix)
        expected_value = c.diff_units(t0, ty, dt)
        assert vix == expected_value
        ty = c.add(ty, partition_interval, 1)

    # Now finally, try percentiles on the partitions
    wanted_percentiles = [0, 10, 25, -1, 50, 75, 90, 100]
    ta_percentiles = TimeAxis(partition_t0, deltahours(24), 365)
    percentiles = ts_partitions.percentiles(ta_percentiles, wanted_percentiles)
    assert len(percentiles) == len(wanted_percentiles)
