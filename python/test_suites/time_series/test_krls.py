import numpy as np
from shyft.time_series import time, TimeAxis, deltahours, TimeSeries, POINT_INSTANT_VALUE


def test_krls_ts():
    t0 = time('2000-01-01T00:00:00Z')
    ta = TimeAxis(t0, deltahours(1), 30*24)
    data = np.sin(np.linspace(0, 2*np.pi, ta.size()))
    ts_data = TimeSeries(ta, data, POINT_INSTANT_VALUE)

    ts = TimeSeries("a")
    ts_krls = ts.krls_interpolation(deltahours(3))

    ts_krls_blob = ts_krls.serialize()
    ts2_krls = TimeSeries.deserialize(ts_krls_blob)

    assert ts2_krls.needs_bind()
    fbi = ts2_krls.find_ts_bind_info()
    assert len(fbi) == 1
    fbi[0].ts.bind(ts_data)
    ts2_krls.bind_done()
    assert not ts2_krls.needs_bind()

    assert len(ts2_krls) == len(ts_data)
    for i in range(len(ts2_krls)):
        assert round(abs(ts2_krls.values[i] - ts_data.values[i]), 1) == 0


def test_ts_get_krls_predictor():
    t0 = time('2000-01-01T00:00:00Z')
    ta = TimeAxis(t0, deltahours(1), 30*24)
    data = np.sin(np.linspace(0, 2*np.pi, ta.size()))
    ts_data = TimeSeries(ta, data, POINT_INSTANT_VALUE)

    ts = TimeSeries("a")

    try:
        ts.get_krls_predictor()
        assert False, "should not be able to get predictor for unbound"
    except:
        pass

    fbi = ts.find_ts_bind_info()
    fbi[0].ts.bind(ts_data)
    ts.bind_done()

    pred = ts.get_krls_predictor(deltahours(3))

    ts_krls = pred.predict(ta)
    assert len(ts_krls) == len(ts_data)
    ts_mse = pred.mse_ts(ts_data)
    assert len(ts_mse) == len(ts_data)
    for i in range(len(ts_krls)):
        assert round(abs(ts_krls.values[i] - ts_data.values[i]), 1) == 0
        assert round(abs(ts_mse.values[i] - 0), 2) == 0
    assert round(abs(pred.predictor_mse(ts_data) - 0), 2) == 0
