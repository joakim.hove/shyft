import pytest
from shyft.time_series import Calendar, TimeSeries, TimeAxis, time, POINT_AVERAGE_VALUE


def test_ts_compression():
    cal = Calendar()
    a = TimeSeries(TimeAxis(cal.time(2010, 1, 1), time(10), 4), [1.0, 1.1, 2.0, 1.9], POINT_AVERAGE_VALUE)
    b = a.compress(0.3)
    assert len(b) == 2
    assert a.compress_size(0.3) == 2
    assert a.compress_size(0.01) == 4
    assert b.values[0] == 1.0
    assert b.values[1] == 2.0
    c = (a*1.0).compress(0.2)  # concrete expressions works.
    assert c == b
    with pytest.raises(RuntimeError) as re:  # does not yet support lazy binding.
        TimeSeries('a').compress_size(0.1)
    with pytest.raises(RuntimeError) as re:
        TimeSeries('a').compress(0.1)
