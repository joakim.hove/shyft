from builtins import range
from shyft.time_series import (Calendar, TimeAxis, TimeAxisFixedDeltaT, deltahours, TimeAxisByPoints, UtcTimeVector, npos, time, utctime_now)
import numpy as np


class TimeAxisUtil:
    """Verify and illustrate TimeAxis
       defined as n periods non-overlapping ascending
        
     """

    def __init__(self):
        self.c = Calendar()
        self.d = deltahours(1)
        self.n = 24
        self.t = self.c.trim(self.c.time(1969, 12, 31, 0, 0, 0), self.d)
        self.ta = TimeAxis(self.t, self.d, self.n)


def test_index_of():
    u = TimeAxisUtil()
    assert u.ta.index_of(u.t) == 0
    assert u.ta.index_of(u.t, 0) == 0
    assert u.ta.index_of(u.t - 3600) == npos
    assert u.ta.open_range_index_of(u.t) == 0
    assert u.ta.open_range_index_of(u.t, 0) == 0
    assert u.ta.open_range_index_of(u.t - 3600) == npos


def test_create_timeaxis():
    u = TimeAxisUtil()
    assert u.ta.size() == u.n
    assert len(u.ta) == u.n
    assert u.ta(0).start == u.t
    assert u.ta(0).end == u.t + u.d
    assert u.ta(1).start == u.t + u.d
    assert u.ta.total_period().start == u.t
    va = np.array([86400, 3600, 3], dtype=np.int64)
    xta = TimeAxisFixedDeltaT(int(va[0]), int(va[1]), int(va[2]))
    assert xta.size() == 3


def test_iterate_timeaxis():
    u = TimeAxisUtil()
    tot_dt = 0
    for p in u.ta:
        tot_dt += p.timespan()
    assert tot_dt == u.n*u.d


def test_timeaxis_str():
    u = TimeAxisUtil()
    s = str(u.ta)
    assert len(s) > 10
    assert repr(u.ta) == str(u.ta)


def test_point_time_axis_():
    """
    A point time axis takes n+1 points do describe n-periods, where
    each period is defined as [ point_i .. point_i+1 >
    """
    u = TimeAxisUtil()
    all_points = UtcTimeVector([t for t in range(int(u.t), int(u.t + (u.n + 1)*u.d), int(u.d))])
    tap = TimeAxisByPoints(all_points)
    assert tap.size() == u.ta.size()
    for i in range(u.ta.size()):
        assert tap(i) == u.ta(i)
    assert tap.t_end == all_points[-1], "t_end should equal the n+1'th point if supplied"
    s = str(tap)
    assert len(s) > 0
    assert str(tap) == repr(tap)


def test_generic_time_axis():
    c = Calendar('Europe/Oslo')
    dt = deltahours(24)  # calendars with less resolution  gets simplified hmm.
    n = 240
    t0 = c.time(2016, 4, 10)

    tag1 = TimeAxis(t0, dt, n)
    assert len(tag1) == n
    assert tag1.time(0) == t0

    tag2 = TimeAxis(c, t0, dt, n)
    assert len(tag2) == n
    assert tag2.time(0) == t0
    assert tag2.calendar_dt.calendar is not None
    assert repr(tag1) == str(tag1)
    assert repr(tag2) == str(tag2)


def test_time_axis_time_points():
    c = Calendar('Europe/Oslo')
    dt = deltahours(1)
    n = 240
    t0 = c.time(2016, 4, 10)
    ta = TimeAxis(c, t0, dt, n)
    tp = ta.time_points
    assert tp is not None
    assert len(tp) == n + 1
    assert len(TimeAxis(c, t0, dt, 0).time_points) == 0


def test_time_axis_time_points_double():
    dt = 1.5
    n = 240
    t0 = 0
    ta = TimeAxis(t0, dt, n)
    tp = ta.time_points_double
    assert tp is not None
    assert len(tp) == n + 1
    assert len(TimeAxis(t0, dt, 0).time_points_double) == 0
    assert ta.time_points_double[1] == 1.5


def test_time_axis_merge():
    dt = time(1)
    a = TimeAxis(time(0), dt, 4)
    b = TimeAxis(time(2), dt, 4)
    assert a.merge(b) == TimeAxis(time(0), dt, 6)
    assert a.merge(TimeAxis(time(5), dt, 5)) == TimeAxis(time(0), dt, 10)
    assert a.merge(TimeAxis()) == a
    a = TimeAxis([1, 2, 3], 4)
    b = TimeAxis([6, 7], 8)
    m = a.merge(b)  # notice that we make fill-in period 4..6
    assert m == TimeAxis([1, 2, 3, 4, 6, 7], 8)
    assert TimeAxis().merge(TimeAxis()) == TimeAxis()

def test_time_axis_merge_different_dt():
    a = TimeAxis(time(0), time(3), 4)
    b = TimeAxis(time(2), time(5), 4)
    c= a.merge(b)
    assert c == TimeAxis([0,2,3,6,7,9,12,17,22])

def test_time_axis_merge_exact_joint():
    a = TimeAxis(time(0), time(1),1)
    b = TimeAxis(time(1), time(1), 1)
    c = a.merge(b)
    assert c==TimeAxis(time(0),time(1),2)

def test_time_axis_merge_disjoint():
    a = TimeAxis(time(0), time(1),1)
    b = TimeAxis(time(2), time(1), 1)
    c = a.merge(b)
    assert c == TimeAxis(time(0),time(1),3)

def test_time_axis_merge_performance():
    n= 7*1000*1000  # 7 million time-points
    dt=time(1)
    t0=time(0)
    a=TimeAxis(t0,dt,n)
    b=TimeAxis(t0-dt,dt,n+2)
    t=utctime_now()
    m=a.merge(b)
    t_used= utctime_now()-t
    perf= 2.*n/float(t_used)
    print(f'\nta-merge fixed dt performance {perf/1.e6:.1f} million points/sec')
    assert perf > 1000.0
    ax=TimeAxis(UtcTimeVector(a.time_points))
    bx=TimeAxis(UtcTimeVector(b.time_points))
    t=utctime_now()
    mx=ax.merge(bx)
    t_used= utctime_now()-t
    perf= 2.*n/float(t_used+0.000001)
    print(f'\nta-merge point dt performance {perf/1.e6:.1f} million points/sec')
    assert perf > 1000.0


