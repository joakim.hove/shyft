import tempfile
from pathlib import Path
from typing import List

# import numpy as np
import pytest
from shyft.time_series import (DtsServer, DtsClient, GeoMatrix, GeoTsMatrix, GeoEvalArgs, GeoQuery, GeoGridSpec,
                               GeoTimeSeriesConfiguration, GeoPoint, GeoPointVector, StringVector, time, UtcTimeVector, Calendar, GeoSlice, utctime_now,
                               TimeSeries, TimeAxis, POINT_AVERAGE_VALUE as stair_case,
                               time_axis_extract_time_points_as_utctime)


def test_geo_query():
    a = GeoQuery()
    assert a
    assert a.epsg == 0
    assert len(a.polygon) == 0
    points = GeoPointVector([GeoPoint(0, 0, 0), GeoPoint(0, 1000, 0), GeoPoint(1000, 0, 0)])
    b = GeoQuery(epsg=32633, points=points)
    assert b.epsg == 32633
    assert b.polygon == points
    ## just to show the exceptions
    with pytest.raises(RuntimeError) as x:
        c = GeoQuery(epsg=32633, points=GeoPointVector([GeoPoint(0, 0, 0), GeoPoint(0, 1, 2)]))
    assert len(str(x.value))
    with pytest.raises(RuntimeError) as x:
        c = GeoQuery(epsg=-32633, points=points)
    assert len(str(x.value))


def test_geo_grid_spec():
    a = GeoGridSpec()
    assert a
    assert a.epsg == 0
    assert len(a.points) == 0
    points = GeoPointVector([GeoPoint(0, 0, 0), GeoPoint(0, 1000, 0), GeoPoint(1000, 0, 0)])
    b = GeoGridSpec(epsg=32633, points=points)
    assert b.epsg == 32633
    assert b.points == points
    ## just to show the exceptions
    with pytest.raises(RuntimeError) as x:
        c = GeoGridSpec(epsg=-32633, points=points)
    assert len(str(x.value))


def test_geo_timeseries_configuration():
    # checkout we can create something empty
    a = GeoTimeSeriesConfiguration()
    assert a
    assert a.prefix == ""
    assert a.name == ""
    assert a.description == ""
    assert len(a.t0_times) == 0
    assert a.grid.epsg == 0
    assert len(a.grid.points) == 0
    assert len(a.variables) == 0
    # checkout we can create something useful
    utc = Calendar()
    prefix = "geo://"
    name = 'a'
    fc_t0 = UtcTimeVector([utc.time(2020, 1, 1) + 6*3600*i for i in range(4)])
    variables = StringVector(['p', 't', 'rh', 'r', 'w'])  # minimalistic size of keys
    n_ensembles = 1
    points = GeoPointVector([GeoPoint(x*1000, y*1000, 0) for x in range(3) for y in range(3)])
    grid = GeoGridSpec(epsg=32633, points=points)
    description = "some description"
    g = GeoTimeSeriesConfiguration(prefix, name, description, grid, fc_t0, 3600*36, n_ensembles, variables)
    assert g != a
    assert g.prefix == prefix
    assert g.name == name
    assert g.description == description
    assert g.t0_times == fc_t0
    assert g.grid.epsg == 32633
    assert g.grid == grid
    assert g.variables == variables
    # finally, test the find_ts_urls interface
    # geo_query = GeoQuery(epsg=32633,points=GeoPointVector([GeoPoint(0,0,0),GeoPoint(0,10*1000,0),GeoPoint(10*1000,10*1000,0),GeoPoint(10*1000,0,0)]))

    # f = g.find_ts_urls(variables=variables,ensembles=IntVector([0]),period=UtcPeriod(fc_t0[0],fc_t0[2]),geo_query=geo_query)
    # assert len(f)==2*len(variables)*len(points)


def test_geo_matrix():
    gs = GeoMatrix(n_t0=1, n_v=1, n_e=1, n_g=2)
    e_ts0 = TimeSeries(TimeAxis(time('2000-01-01T00:00:00Z'), time(3600), 24), fill_value=1.0, point_fx=stair_case)
    gs.set_ts(0, 0, 0, 0, e_ts0)
    ts0 = gs.get_ts(0, 0, 0, 0)
    ts1 = gs.get_ts(0, 0, 0, 1)
    shape = gs.shape
    assert e_ts0 == ts0


def test_geo_ts_matrix():
    gs = GeoTsMatrix(n_t0=1, n_v=1, n_e=1, n_g=2)
    e_ts0 = TimeSeries(TimeAxis(time('2000-01-01T00:00:00Z'), time(3600), 24), fill_value=1.0, point_fx=stair_case)
    gs.set_ts(0, 0, 0, 0, e_ts0)
    e_p0 = GeoPoint(1, 1, 2)
    gs.set_geo_point(0, 0, 0, 0, e_p0)
    ts0 = gs.get_ts(0, 0, 0, 0)
    p0 = gs.get_geo_point(0, 0, 0, 0)
    ts1 = gs.get_ts(0, 0, 0, 1)
    p1 = gs.get_geo_point(0, 0, 0, 1)
    shape = gs.shape
    assert e_p0 == p0
    assert e_ts0 == ts0


def test_geo_ts_matrix_concat():
    gs = GeoTsMatrix(n_t0=2, n_v=1, n_e=1, n_g=2)
    e_ts0 = TimeSeries(TimeAxis(time('2000-01-01T00:00:00Z'), time(3600), 48), fill_value=1.0, point_fx=stair_case)
    e_ts1 = TimeSeries(TimeAxis(time('2000-01-02T00:00:00Z'), time(3600), 48), fill_value=2.0, point_fx=stair_case)
    gs.set_ts(0, 0, 0, 0, e_ts0)
    gs.set_ts(1, 0, 0, 0, e_ts1)
    gs.set_geo_point(0, 0, 0, 0, GeoPoint(1, 1, 1))

    gs.set_ts(0, 0, 0, 1, e_ts0 + 10.0)
    gs.set_ts(1, 0, 0, 1, e_ts1 + 10.0)
    gs.set_geo_point(0, 0, 0, 1, GeoPoint(2, 2, 2))
    cc_gs = gs.concatenate(time(0), time(24*3600))
    assert cc_gs
    assert cc_gs.shape.n_t0 == 1
    assert cc_gs.get_ts(0, 0, 0, 0).time_axis == TimeAxis(time('2000-01-01T00:00:00Z'), time(3600), 24 + 48)


def test_geo_dtss_basics():
    with tempfile.TemporaryDirectory() as geo_root:
        srv = DtsServer()
        geo_container = "geo"
        srv.set_container(geo_container, str(geo_root))
        utc = Calendar()
        variables = StringVector(['p', 't', 'rh', 'rad', 'ws'])  # minimalistic size of keys
        n_ensembles = 1
        points = GeoPointVector([GeoPoint(x*1000, y*1000, 0) for x in range(3) for y in range(3)])
        grid = GeoGridSpec(epsg=32633, points=points)  # points are 3 x 3 grid with 1000 m spacing
        gdb = GeoTimeSeriesConfiguration('shyft://', geo_container, 'Basic test', grid, UtcTimeVector(), 3600*36,
                                         n_ensembles, variables)
        srv.set_geo_ts_db(gdb)
        port_no = srv.start_async()
        c = DtsClient(f'localhost:{port_no}')
        fc_dt = time(3600*6)  # like arome every 6th hour
        try:
            geo_infos = c.get_geo_db_ts_info()  # ensure it was able to register our only geo db
            assert geo_infos
            assert len(geo_infos) == 1
            assert geo_infos[0]
            assert geo_infos[0].name == geo_container
            fc_t0 = TimeAxis(utc.time(2020, 1, 1), fc_dt, 3)
            tp = time_axis_extract_time_points_as_utctime(fc_t0)[:-1]
            g_slice = GeoSlice(v=[0, 1, 2, 3, 4], e=[0], g=[gix for gix in range(len(gdb.grid.points))], t=tp, ts_dt=gdb.dt)
            assert g_slice

            bbox = gdb.bounding_box(g_slice)  # how to get axis aligned two point bbox
            assert len(bbox) == 2
            assert bbox == GeoPointVector([GeoPoint(0, 0, 0), GeoPoint(2000, 2000, 0)])
            conv_hull = gdb.convex_hull(g_slice)  # compute convex hull from the slice
            assert len(conv_hull) == 5
            assert conv_hull == GeoPointVector([GeoPoint(0, 0, 0), GeoPoint(0, 2000, 0), GeoPoint(2000, 2000, 0), GeoPoint(2000, 0, 0), GeoPoint(0, 0, 0)])
            fcm = gdb.create_ts_matrix(g_slice)  # create the structure for our forecasts
            assert fcm
            # now fill the forecast matrix, with forecast time-series
            # dimensions time, variables, ensembles,geopoints
            for t in range(fcm.shape.n_t0):
                for v in range(fcm.shape.n_v):
                    for e in range(fcm.shape.n_e):
                        for g in range(fcm.shape.n_g):
                            fc_ts = TimeSeries(TimeAxis(fc_t0.time(t), time(3600), 36), fill_value=t*10 + g,
                                               point_fx=stair_case)
                            fcm.set_ts(t, v, e, g, fc_ts)  # indicies happens to be correct here
            c.geo_store(geo_container, fcm, True, True)  # store to backend
            with pytest.raises(RuntimeError):
                c.geo_store("not_configured", fcm, True, True)  # ensure this gives user feedback
            geo_infos = c.get_geo_db_ts_info()
            assert geo_infos
            gtsm1 = c.geo_evaluate(geo_container, variables=gdb.variables, ensembles=[0], time_axis=fc_t0,
                                   ts_dt=gdb.dt,
                                   geo_range=GeoQuery(), concat=False, cc_dt0=time(0), use_cache=True,
                                   update_cache=False)
            with pytest.raises(RuntimeError):
                c.geo_evaluate("geo_not_configured", variables=gdb.variables, ensembles=[0], time_axis=fc_t0,
                               ts_dt=gdb.dt,
                               geo_range=GeoQuery(), concat=False, cc_dt0=time(0), use_cache=True,
                               update_cache=False)
            assert gtsm1
            ea = GeoEvalArgs(geo_ts_db_id=geo_container, variables=gdb.variables, ensembles=[0], time_axis=fc_t0,
                             ts_dt=0, geo_range=GeoQuery(), concat=False, cc_dt0=time(0))
            gtsm2 = c.geo_evaluate(eval_args=ea, use_cache=True, update_cache=False)

            assert gtsm1 == gtsm2
            ea_cc = GeoEvalArgs(geo_ts_db_id=geo_container, variables=gdb.variables, ensembles=[0], time_axis=fc_t0,
                                ts_dt=0, geo_range=GeoQuery(), concat=True, cc_dt0=time(0))
            gtsm2_cc = c.geo_evaluate(eval_args=ea_cc, use_cache=True, update_cache=True)
            assert gtsm2_cc
            assert gtsm2_cc.shape.n_t0 == 1
            expected_ts_0_0_0_0 = TimeSeries(TimeAxis(fc_t0.time(0), time(3600), 48), values=
            [0., 0., 0., 0., 0., 0., 10., 10., 10., 10., 10., 10., 20., 20., 20., 20., 20., 20.,
             20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20.,
             20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20.], point_fx=stair_case)
            assert gtsm2_cc.get_ts(0, 0, 0, 0) == expected_ts_0_0_0_0

        finally:
            c.close()
            srv.clear()


def test_geo_dtss_basics_external():
    """ same as internal test, but with geo-callbacks setup and external """
    # variables to keep record of callbacks
    tsm_stored: List[GeoMatrix] = []
    geo_store_gdb: List[GeoTimeSeriesConfiguration] = []
    replace_store: List[bool] = []
    geo_read_slices: List[GeoSlice] = []
    geo_read_gdb: List[GeoTimeSeriesConfiguration] = []

    def geo_ts_read(gdb: GeoTimeSeriesConfiguration, gs: GeoSlice) -> GeoMatrix:
        geo_read_slices.append(gs)
        geo_read_gdb.append(gdb)
        return tsm_stored[-1]

    def geo_ts_store(gdb: GeoTimeSeriesConfiguration, tsm: GeoMatrix, replace: bool):
        geo_store_gdb.append(gdb)
        replace_store.append(replace)
        tsm_stored.append(tsm)

    with tempfile.TemporaryDirectory() as geo_root:
        srv = DtsServer()
        srv.geo_ts_read_cb = geo_ts_read
        srv.geo_ts_store_cb = geo_ts_store

        geo_container = "geo"
        srv.set_container(geo_container, str(geo_root))
        utc = Calendar()
        variables = StringVector(['p', 't', 'rh', 'rad', 'ws'])  # minimalistic size of keys
        n_ensembles = 1
        points = GeoPointVector([GeoPoint(x*1000, y*1000, 0) for x in range(3) for y in range(3)])
        grid = GeoGridSpec(epsg=32633, points=points)  # points are 3 x 3 grid with 1000 m spacing
        gdb = GeoTimeSeriesConfiguration('geo://', geo_container, 'Basic test', grid, UtcTimeVector(), 3600*36,
                                         n_ensembles, variables)
        # srv.set_geo_ts_db(gdb)
        port_no = srv.start_async()
        c = DtsClient(f'localhost:{port_no}')
        fc_dt = time(3600*6)  # like arome every 6th hour
        try:
            c.add_geo_ts_db(gdb)
            geo_infos = c.get_geo_db_ts_info()  # ensure it was able to register our only geo db
            assert geo_infos
            assert len(geo_infos) == 1
            assert geo_infos[0]
            assert geo_infos[0].name == geo_container
            fc_t0 = TimeAxis(utc.time(2020, 1, 1), fc_dt, 3)
            tp = time_axis_extract_time_points_as_utctime(fc_t0)[:-1]
            g_slice = GeoSlice(v=[0, 1, 2, 3, 4], e=[0], g=[gix for gix in range(len(gdb.grid.points))], t=tp, ts_dt=gdb.dt)
            assert g_slice
            fcm = gdb.create_ts_matrix(g_slice)  # create the structure for our forecasts
            assert fcm
            # now fill the forecast matrix, with forecast time-series
            # dimensions time, variables, ensembles,geopoints
            for t in range(fcm.shape.n_t0):
                for v in range(fcm.shape.n_v):
                    for e in range(fcm.shape.n_e):
                        for g in range(fcm.shape.n_g):
                            fc_ts = TimeSeries(TimeAxis(fc_t0.time(t), time(3600), 36), fill_value=t*10 + g,
                                               point_fx=stair_case)
                            fcm.set_ts(t, v, e, g, fc_ts)  # indicies happens to be correct here
            c.geo_store(geo_container, fcm, True, False)  # store to backend
            geo_infos = c.get_geo_db_ts_info()
            assert geo_infos
            gtsm1 = c.geo_evaluate(geo_container, variables=gdb.variables, ensembles=[0], time_axis=fc_t0, ts_dt=gdb.dt,
                                   geo_range=GeoQuery(), concat=False, cc_dt0=time(0), use_cache=True,
                                   update_cache=True)
            assert gtsm1
            ea = GeoEvalArgs(geo_ts_db_id=geo_container, variables=gdb.variables, ensembles=[0], time_axis=fc_t0,
                             ts_dt=gdb.dt, geo_range=GeoQuery(), concat=False, cc_dt0=time(0))
            gtsm2 = c.geo_evaluate(eval_args=ea, use_cache=True, update_cache=False)

            assert gtsm1 == gtsm2
            ea_cc = GeoEvalArgs(geo_ts_db_id=geo_container, variables=gdb.variables, ensembles=[0], time_axis=fc_t0,
                                ts_dt=gdb.dt, geo_range=GeoQuery(), concat=True, cc_dt0=time(0))
            gtsm2_cc = c.geo_evaluate(eval_args=ea_cc, use_cache=True, update_cache=True)
            assert gtsm2_cc
            assert gtsm2_cc.shape.n_t0 == 1
            expected_ts_0_0_0_0 = TimeSeries(TimeAxis(fc_t0.time(0), time(3600), 48), values=
            [0., 0., 0., 0., 0., 0., 10., 10., 10., 10., 10., 10., 20., 20., 20., 20., 20., 20.,
             20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20.,
             20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20., 20.], point_fx=stair_case)
            assert gtsm2_cc.get_ts(0, 0, 0, 0) == expected_ts_0_0_0_0
            # also ensure we actually get called, not fooled by the cache
            assert len(geo_store_gdb) == 1
            assert len(tsm_stored) == 1
            assert len(replace_store) == 1
            assert len(geo_read_gdb) == 1
            assert len(geo_read_gdb) == 1
            c.remove_geo_ts_db(geo_container)
            geo_infos = c.get_geo_db_ts_info()
            assert len(geo_infos) == 0
        finally:
            c.close()
            srv.clear()


def test_geo_dtss_performance():
    n_x = 5
    n_y = 5
    t0_dt = time(3600*6)
    t0_y_dt = Calendar.YEAR
    fc_steps = 36
    fc_dt = time(3600)
    ta_t0 = TimeAxis(time('2020-09-01T00:00:00Z'), t0_dt, 4*365)
    ta_cc = TimeAxis(time('2020-09-01T00:00:00Z'), t0_y_dt, 1)

    with tempfile.TemporaryDirectory() as geo_root:
        srv = DtsServer()
        srv.cache_max_items = len(ta_t0)*n_x*n_y*5*2  # two times in cache, so we should be able to keep it
        geo_container = "geo"
        geo_cc_container = "gcc"
        srv.set_container(geo_container, str(Path(geo_root)/geo_container))
        srv.set_container(geo_cc_container, str(Path(geo_root)/geo_cc_container))
        utc = Calendar()
        variables = StringVector(['p', 't', 'rh', 'rad', 'ws'])  # minimalistic size of keys
        n_ensembles = 1
        points = GeoPointVector([GeoPoint(x*1000, y*1000, (x + y)*100) for x in range(n_x) for y in range(n_y)])
        grid = GeoGridSpec(epsg=32633, points=points)  # points are 3 x 3 grid with 1000 m spacing
        gdb = GeoTimeSeriesConfiguration('shyft://', geo_container, 'Performance test', grid, UtcTimeVector(), t0_dt,
                                         n_ensembles, variables)
        gcc = GeoTimeSeriesConfiguration('shyft://', geo_cc_container, 'concatenated forecasts test', grid,
                                         UtcTimeVector(), t0_y_dt,
                                         n_ensembles, variables)
        srv.set_geo_ts_db(gdb)
        # srv.set_geo_ts_db(gcc)
        port_no = srv.start_async()
        c = DtsClient(f'localhost:{port_no}')

        try:
            c.add_geo_ts_db(gcc)  # can be done by the client as long as containers are setup on server
            tp = time_axis_extract_time_points_as_utctime(ta_t0)[:-1]
            g_slice = GeoSlice(v=[0, 1, 2, 3, 4], e=[0], g=[gix for gix in range(len(gdb.grid.points))], t=tp, ts_dt=gdb.dt)
            fcm = gdb.create_ts_matrix(g_slice)  # create the structure for our forecasts
            # now fill the forecast matrix, with forecast time-series
            # dimensions time, variables, ensembles,geopoints
            for t in range(fcm.shape.n_t0):
                for v in range(fcm.shape.n_v):
                    for e in range(fcm.shape.n_e):
                        for g in range(fcm.shape.n_g):
                            fc_ts = TimeSeries(TimeAxis(ta_t0.time(t), fc_dt, fc_steps), fill_value=t*10 + g,
                                               point_fx=stair_case)
                            fcm.set_ts(t, v, e, g, fc_ts)  # indicies happens to be correct here
            tx1 = utctime_now()
            cc_fcm = fcm.concatenate(time(0), t0_dt)
            cc_used = utctime_now() - tx1
            tx1 = utctime_now()
            c.geo_store(geo_cc_container, cc_fcm, True, True)
            cc_store_used = utctime_now() - tx1
            c.geo_store(geo_container, fcm, True, True)  # store to backend
            store_used = utctime_now() - tx1

            geo_infos = c.get_geo_db_ts_info()
            assert geo_infos
            ea = GeoEvalArgs(geo_ts_db_id=geo_container, variables=gdb.variables, ensembles=[0], time_axis=ta_t0,
                             ts_dt=gdb.dt, geo_range=GeoQuery(), concat=True, cc_dt0=time(0))
            tx1 = utctime_now()
            gtsm2 = c.geo_evaluate(eval_args=ea, use_cache=True, update_cache=False)
            eval_used = utctime_now() - tx1
            assert gtsm2

            tx1 = utctime_now()
            gtsm2 = c.geo_evaluate(eval_args=ea, use_cache=False, update_cache=False)
            eval_uncached_used = utctime_now() - tx1
            assert gtsm2

            cc_ea = GeoEvalArgs(geo_ts_db_id=geo_cc_container, variables=gdb.variables, ensembles=[0], time_axis=ta_cc,
                                ts_dt=gcc.dt, geo_range=GeoQuery(), concat=True, cc_dt0=time(0))
            tx1 = utctime_now()
            gtsm3 = c.geo_evaluate(eval_args=cc_ea, use_cache=True, update_cache=False)
            eval_cc_used = utctime_now() - tx1
            assert gtsm3
            print(
                f'\n\ngeo dtss performance numbers from test-case {n_x} x {n_y} grid x {len(ta_t0)} forecasts x 5 variables x1 ens :')
            print(f'  Used {cc_used} in-memory concat client side')
            print(f'  Used {cc_store_used} to store already concatenated ')
            print(f'  Used {store_used} to store all forecasts')
            print(f'  Used {eval_used}  client read server-side concat with cache')
            print(f'  Used {eval_uncached_used}  client read server-side concat with no cache')
            print(f'  Used {eval_cc_used} client read already concatendated server forecasts')
        finally:
            c.close()
            srv.clear()
