from shyft.hydrology import GammaSnowParameter, GammaSnowState, GammaSnowCalculator, GammaSnowResponse
from shyft.time_series import deltahours
from shyft.time_series import Calendar


def test_gamma_snow_parameter():
    p = GammaSnowParameter(winter_end_day_of_year=100,
                           initial_bare_ground_fraction=0.0,
                           snow_cv=0.21, tx=-0.32, wind_scale=2.3,
                           wind_const=1.4, max_water=0.23,
                           surface_magnitude=30.0, max_albedo=0.73,
                           min_albedo=0.4, fast_albedo_decay_rate=5.1,
                           slow_albedo_decay_rate=5.3,
                           snowfall_reset_depth=5.12,
                           glacier_albedo=0.23)
    assert round(abs(p.winter_end_day_of_year - 100), 7) == 0
    assert round(abs(p.initial_bare_ground_fraction - 0.0), 7) == 0
    assert round(abs(p.snow_cv - 0.21), 7) == 0
    assert round(abs(p.tx - -0.32), 7) == 0
    assert round(abs(p.wind_scale - 2.3), 7) == 0
    assert round(abs(p.wind_const - 1.4), 7) == 0
    assert round(abs(p.max_water - 0.23), 7) == 0
    assert round(abs(p.surface_magnitude - 30.0), 7) == 0
    assert round(abs(p.max_albedo - 0.73), 7) == 0
    assert round(abs(p.min_albedo - 0.4), 7) == 0
    assert round(abs(p.fast_albedo_decay_rate - 5.1), 7) == 0
    assert round(abs(p.slow_albedo_decay_rate - 5.3), 7) == 0
    assert round(abs(p.snowfall_reset_depth - 5.12), 7) == 0
    assert round(abs(p.glacier_albedo - 0.23), 7) == 0


def test_gamma_snow_state():
    s = GammaSnowState(albedo=0.32, lwc=0.01, surface_heat=30003.0,
                       alpha=1.23, sdc_melt_mean=1.2, acc_melt=0.45,
                       iso_pot_energy=0.1, temp_swe=1.2)
    assert round(abs(s.albedo - 0.32), 7) == 0
    assert round(abs(s.lwc - 0.01), 7) == 0
    assert round(abs(s.surface_heat - 30003.0), 7) == 0
    assert round(abs(s.alpha - 1.23), 7) == 0
    assert round(abs(s.sdc_melt_mean - 1.2), 7) == 0
    assert round(abs(s.acc_melt - 0.45), 7) == 0
    assert round(abs(s.iso_pot_energy - 0.1), 7) == 0
    assert round(abs(s.temp_swe - 1.2), 7) == 0


def test_gamma_snow_step():
    utc = Calendar()
    s = GammaSnowState()
    p = GammaSnowParameter()
    r = GammaSnowResponse()
    calc = GammaSnowCalculator()
    t = utc.time(2016, 10, 1)
    dt = deltahours(1)
    temp = 0.4
    rad = 12.0
    prec_mm_h = 0.3
    wind_speed = 1.3
    rel_hum = 0.4
    forest_fraction = 0.2
    altitude = 100.0
    # Just check that we don't get an error when stepping
    calc.step(s, r, t, dt, p, temp, rad, prec_mm_h, wind_speed, rel_hum, forest_fraction, altitude)
