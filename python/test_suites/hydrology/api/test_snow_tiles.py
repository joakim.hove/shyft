from shyft.hydrology import SnowTilesParameter, SnowTilesState, SnowTilesCalculator, SnowTilesResponse
from shyft.time_series import deltahours
from shyft.time_series import Calendar


def test_snow_tiles_parameter_sig():
    # area_fracs = [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]
    area_fracs = [0.4, 0.3, 0.2, 0.1]
    p = SnowTilesParameter(shape=2.5, tx=0.5, cx=1.5, ts=0.1, lwmax=0.2, cfr=0.6, area_fractions=area_fracs)
    assert round(abs(p.shape - 2.5), 7) == 0
    assert round(abs(p.tx - 0.5), 7) == 0
    assert round(abs(p.cx - 1.5), 7) == 0
    assert round(abs(p.ts - 0.1), 7) == 0
    assert round(abs(p.lwmax - 0.2), 7) == 0
    assert round(abs(p.cfr - 0.6), 7) == 0
    for f1, f2 in zip(area_fracs, p.area_fractions.to_numpy()):
        assert round(abs(f1 - f2), 7) == 0


def test_snow_tiles_state():
    fw = [1, 2, 3, 4]
    lw = [5, 6, 7, 8]
    s = SnowTilesState(fw, lw)
    for i in range(len(fw)):
        assert round(abs(s.fw[i] - fw[i]), 7) == 0
        assert round(abs(s.lw[i] - lw[i]), 7) == 0


def test_snow_tiles_step():
    utc = Calendar()
    s = SnowTilesState()
    p = SnowTilesParameter()
    # s.distribute(p)
    r = SnowTilesResponse()
    calc = SnowTilesCalculator(p)
    t0 = utc.time(2016, 10, 1)
    t1 = utc.time(2016, 10, 2)
    dt = deltahours(1)
    temp = 0.4
    prec_mm_h = 0.3
    # Just check that we don't get an error when stepping
    calc.step(s, r, t0, t1, prec_mm_h, temp)
    assert True


def test_get_set_area_fractions():
    p = SnowTilesParameter()
    p.area_fractions = [0.5, 0.5]
    for i in range(len(p.area_fractions)):
        assert p.area_fractions[i] == 0.5


def test_get_set_shape():
    p = SnowTilesParameter()
    p.shape = 1.67
    assert round(p.shape, 7) == 1.67


def test_get_multiply():
    p = SnowTilesParameter()
    assert round(sum(p._multiply)/len(p._multiply), 7) == 1.0
