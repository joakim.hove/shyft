from shyft.hydrology import (GeoCellDataServer,GeoCellDataClient, GeoCellData, LandTypeFractions,GeoCellDataVector,GeoPoint,GeoCellDataModel)
from shyft.time_series import time, ModelInfo, Int64Vector


def mk_geo_cell_data_example(n: int = 1000)->GeoCellDataVector:
    return GeoCellDataVector([
        GeoCellData(  # describe personality of the cell.
            # TIN vertexes
            p1=GeoPoint(x=i*1000,y=i*1000,z=i),
            p2=GeoPoint(x=i*1000+1000,y=i*1000,z=i+100),
            p3=GeoPoint(x=i*1000,y=i*1000+1000,z=i+50),
            # the epsg of the tin coordinates
            epsg_id=32633, # utm 33n
            # catchment grouping id
            catchment_id=1,
            # and the landtypes/fractions
            land_type_fractions=LandTypeFractions(
                glacier=0.0,
                lake=0.1,
                reservoir=0.2,
                forest=0.3,
                unspecified=0.4
            )
        ) for i in range(n)])


def test_geo_cell_data_server_basics(tmpdir):
    model_dir = (tmpdir/"t_gcd_srv_basics")
    model_dir.mkdir()
    s = GeoCellDataServer(root_dir=str(model_dir))  # just tell the Server where to store it's model.
    try :
        #s.set_listening_port(10000) # you can specify the server port in real life, but in test, we autoallocate ports to avoid conflicts
        port = s.start_server()   # if you do not tell  the server, it will find a free port
        assert port > 0
        all_infos = Int64Vector()  # specify empty to get all infos
        c = GeoCellDataClient(host_port=f'localhost:{port}',timeout_ms=1000)
        m_infos=c.get_model_infos(all_infos)
        assert len(m_infos)==0
        m=GeoCellDataModel()
        m.geo_cell_data=mk_geo_cell_data_example(n=100)
        m_info=ModelInfo(id=0,name='example model',created=time.now(), json='{"descript":"any thing usefule for filtering goes here"}')
        mid=c.store_model(m,m_info)  # here we store the model, with the decorated m_info, that we can later use for filtering
        m_infos=c.get_model_infos(all_infos)  # we can specify spesific infos, or empty typed Int64Vector will give all
        assert len(m_infos) == 1
        assert m_infos[0].id == mid  # because that's the only model we have stored so far
        mx= c.read_model(mid=mid)
        mxv = c.read_models([mid])  # you can read many models in one go(save time!)
        assert len(mxv) == 1
        mx1 = mxv[0]
        assert mx.id == mx1.id  # should give same model in this case
        assert mx.geo_cell_data == m.geo_cell_data
        # update the model info (not the model, but associated info)
        m_info.json='{"labels":["operative","tin"]}'
        m_info.id=mid  # required, extra check, the mi.id must equal the mid=mid, below
        c.update_model_info(mid=mid,mi=m_info)
        m_infos=c.get_model_infos(all_infos)
        assert m_info.json == m_infos[0].json
        c.remove_model(mid=mid)
        assert len(c.get_model_infos(all_infos)) == 0  # now there should be zero left.
        c.close()
    finally:
        s.stop_server()
