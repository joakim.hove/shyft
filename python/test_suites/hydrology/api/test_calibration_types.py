﻿import math
import unittest

import numpy as np

from shyft import hydrology as api
from shyft.hydrology import hbv_stack
from shyft.hydrology import pt_gs_k
from shyft.hydrology import pt_hps_k
from shyft.hydrology import pt_hs_k
from shyft.hydrology import pt_ss_k
from shyft.hydrology import pt_st_k
from shyft.hydrology import r_pm_gs_k
from shyft.hydrology import r_pt_gs_k
from shyft.time_series import (Calendar, deltahours, TsFactory, TimeAxis, UtcPeriod,
                               TimeSeries, UtcTimeVector, point_interpretation_policy,
                               DoubleVector, IntVector,
                               POINT_AVERAGE_VALUE)

"""
Verify basic Shyft api calibration related functions and structures
"""


def verify_parameter_for_calibration(param, expected_size, valid_names, test_dict=None):
    min_p_value = -1e+10
    max_p_value = +1e+10
    test_dict = test_dict or dict()
    assert expected_size == param.size(), "expected parameter size changed"
    pv = DoubleVector([param.get(i) for i in range(param.size())])
    for i in range(param.size()):
        v = param.get(i)
        p_name = param.get_name(i)
        assert v > min_p_value and v < max_p_value
        if i not in test_dict:
            pv[i] = v*1.01
            param.set(pv)  # set the complete vector, only used during C++ calibration, but we verify it here
            x = param.get(i)
            assert round(abs(v*1.01 - x), 3) == 0, f"{p_name} :expect new value when setting value"
        else:

            pv[i] = test_dict[i]
            param.set(pv)
            x = param.get(i)
            assert round(abs(x - test_dict[i]), 1) == 0, "Expect new value when setting value"
        assert len(p_name) > 0, "parameter name should exist"
        assert valid_names[i] == p_name


def test_pt_hs_k_param():
    pthsk_size = 18
    pthsk = pt_hs_k.PTHSKParameter()
    assert pthsk is not None
    assert pthsk.size() == pthsk_size
    pthsk.hs.lw = 0.23
    assert round(abs(pthsk.hs.lw - 0.23), 7) == 0
    snow = api.HbvSnowParameter(
        tx=0.2)  # ordered .. keyword does work now
    assert snow is not None
    snow.lw = 0.2
    assert round(abs(snow.lw - 0.2), 7) == 0
    valid_names = [
        "kirchner.c1",
        "kirchner.c2",
        "kirchner.c3",
        "ae.ae_scale_factor",
        "hs.lw",
        "hs.tx",
        "hs.cx",
        "hs.ts",
        "hs.cfr",
        "gm.dtf",
        "p_corr.scale_factor",
        "pt.albedo",
        "pt.alpha",
        "routing.velocity",
        "routing.alpha",
        "routing.beta",
        "gm.direct_response",
        "msp.reservoir_direct_response_fraction"
    ]
    verify_parameter_for_calibration(pthsk, pthsk_size, valid_names)


def test_hbv_stack_param():
    hbv_size = 22
    hbv = hbv_stack.HbvParameter()
    assert hbv is not None
    assert hbv.size() == hbv_size
    valid_names = [
        "soil.fc",
        "soil.beta",
        "ae.lp",
        "tank.uz1",
        "tank.kuz2",
        "tank.kuz1",
        "tank.perc",
        "tank.klz",
        "hs.lw",
        "hs.tx",
        "hs.cx",
        "hs.ts",
        "hs.cfr",
        "p_corr.scale_factor",
        "pt.albedo",
        "pt.alpha",
        "gm.dtf",
        "routing.velocity",
        "routing.alpha",
        "routing.beta",
        "gm.direct_response",
        "msp.reservoir_direct_response_fraction"
    ]
    verify_parameter_for_calibration(hbv, hbv_size, valid_names)


def test_pt_gs_k_param():
    ptgsk_size = 31
    valid_names = [
        "kirchner.c1",
        "kirchner.c2",
        "kirchner.c3",
        "ae.ae_scale_factor",
        "gs.tx",
        "gs.wind_scale",
        "gs.max_water",
        "gs.wind_const",
        "gs.fast_albedo_decay_rate",
        "gs.slow_albedo_decay_rate",
        "gs.surface_magnitude",
        "gs.max_albedo",
        "gs.min_albedo",
        "gs.snowfall_reset_depth",
        "gs.snow_cv",
        "gs.glacier_albedo",
        "p_corr.scale_factor",
        "gs.snow_cv_forest_factor",
        "gs.snow_cv_altitude_factor",
        "pt.albedo",
        "pt.alpha",
        "gs.initial_bare_ground_fraction",
        "gs.winter_end_day_of_year",
        "gs.calculate_iso_pot_energy",
        "gm.dtf",
        "routing.velocity",
        "routing.alpha",
        "routing.beta",
        "gs.n_winter_days",
        "gm.direct_response",
        "msp.reservoir_direct_response_fraction"
    ]
    p = pt_gs_k.PTGSKParameter()
    special_values = {22: 130, 28: 221}
    verify_parameter_for_calibration(p, ptgsk_size, valid_names, special_values)
    # special verification of bool parameter
    p.gs.calculate_iso_pot_energy = True
    assert p.gs.calculate_iso_pot_energy
    assert abs(p.get(23) - 1.0) == 0.0
    p.gs.calculate_iso_pot_energy = False
    assert not p.gs.calculate_iso_pot_energy
    assert abs(p.get(23) - 0.0) == 0.0
    pv = DoubleVector.from_numpy([p.get(i) for i in range(p.size())])
    pv[23] = 1.0
    p.set(pv)
    assert p.gs.calculate_iso_pot_energy
    pv[23] = 0.0
    p.set(pv)
    assert not p.gs.calculate_iso_pot_energy
    # checkout new parameters for routing
    p.routing.velocity = 1/3600.0
    p.routing.alpha = 1.1
    p.routing.beta = 0.8
    assert round(abs(p.routing.velocity - 1/3600.0), 7) == 0
    assert round(abs(p.routing.alpha - 1.1), 7) == 0
    assert round(abs(p.routing.beta - 0.8), 7) == 0
    utc = Calendar()
    assert p.gs.is_snow_season(utc.time(2017, 1, 1))
    assert not p.gs.is_snow_season(utc.time(2017, 8, 1))
    p.gs.n_winter_days = 100
    assert not p.gs.is_snow_season(utc.time(2017, 11, 31))
    assert p.gs.is_snow_season(utc.time(2017, 2, 1))


def test_r_pm_gs_k_param():
    ptgsk_size = 33
    valid_names = [
        "kirchner.c1",
        "kirchner.c2",
        "kirchner.c3",
        "ae.ae_scale_factor",
        "gs.tx",
        "gs.wind_scale",
        "gs.max_water",
        "gs.wind_const",
        "gs.fast_albedo_decay_rate",
        "gs.slow_albedo_decay_rate",  # 9
        "gs.surface_magnitude",
        "gs.max_albedo",
        "gs.min_albedo",
        "gs.snowfall_reset_depth",
        "gs.snow_cv",
        "gs.glacier_albedo",
        "p_corr.scale_factor",
        "gs.snow_cv_forest_factor",
        "gs.snow_cv_altitude_factor",
        "rad.albedo",
        "rad.turbidity",
        "pm.height_veg",
        "gs.initial_bare_ground_fraction",
        "gs.winter_end_day_of_year",  # 24
        "gs.calculate_iso_pot_energy",
        "gm.dtf",
        "routing.velocity",
        "routing.alpha",
        "routing.beta",
        "gs.n_winter_days",  # 30
        "gm.direct_response",
        "msp.reservoir_direct_response_fraction",
        "pm.rl"
    ]
    p = r_pm_gs_k.RPMGSKParameter()
    ix_wedy = valid_names.index("gs.winter_end_day_of_year")
    ix_iso = valid_names.index("gs.calculate_iso_pot_energy")
    special_values = {ix_wedy: 130, valid_names.index("gs.n_winter_days"): 221}
    verify_parameter_for_calibration(p, ptgsk_size, valid_names, special_values)
    # special verification of bool parameter
    p.gs.calculate_iso_pot_energy = True
    assert p.gs.calculate_iso_pot_energy
    assert abs(p.get(ix_iso) - 1.0) == 0
    p.gs.calculate_iso_pot_energy = False
    assert not p.gs.calculate_iso_pot_energy
    assert abs(p.get(ix_iso) - 0.0) == 0
    pv = DoubleVector.from_numpy([p.get(i) for i in range(p.size())])
    pv[ix_iso] = 1.0
    p.set(pv)
    assert p.gs.calculate_iso_pot_energy
    pv[ix_iso] = 0.0
    p.set(pv)
    assert not p.gs.calculate_iso_pot_energy
    # checkout new parameters for routing
    p.routing.velocity = 1/3600.0
    p.routing.alpha = 1.1
    p.routing.beta = 0.8
    assert round(abs(p.routing.velocity - 1/3600.0), 7) == 0
    assert round(abs(p.routing.alpha - 1.1), 7) == 0
    assert round(abs(p.routing.beta - 0.8), 7) == 0
    utc = Calendar()
    assert p.gs.is_snow_season(utc.time(2017, 1, 1))
    assert not p.gs.is_snow_season(utc.time(2017, 8, 1))
    p.gs.n_winter_days = 100
    assert not p.gs.is_snow_season(utc.time(2017, 11, 31))
    assert p.gs.is_snow_season(utc.time(2017, 2, 1))
    blob = p.serialize()
    p2 = p.deserialize(blob)
    assert p == p2


def test_r_pt_gs_k_param():
    ptgsk_size = 33
    valid_names = [
        "kirchner.c1",
        "kirchner.c2",
        "kirchner.c3",
        "ae.ae_scale_factor",
        "gs.tx",
        "gs.wind_scale",
        "gs.max_water",
        "gs.wind_const",
        "gs.fast_albedo_decay_rate",
        "gs.slow_albedo_decay_rate",  # 9
        "gs.surface_magnitude",
        "gs.max_albedo",
        "gs.min_albedo",
        "gs.snowfall_reset_depth",
        "gs.snow_cv",
        "gs.glacier_albedo",
        "p_corr.scale_factor",
        "gs.snow_cv_forest_factor",
        "gs.snow_cv_altitude_factor",
        "rad.albedo",
        "rad.turbidity",
        "pt.albedo",
        "pt.alpha",
        "gs.initial_bare_ground_fraction",
        "gs.winter_end_day_of_year",  # 24
        "gs.calculate_iso_pot_energy",
        "gm.dtf",
        "routing.velocity",
        "routing.alpha",
        "routing.beta",
        "gs.n_winter_days",  # 30
        "gm.direct_response",
        "msp.reservoir_direct_response_fraction"
    ]
    p = r_pt_gs_k.RPTGSKParameter()
    ix_wedy = valid_names.index("gs.winter_end_day_of_year")
    ix_iso = valid_names.index("gs.calculate_iso_pot_energy")
    special_values = {ix_wedy: 130, valid_names.index("gs.n_winter_days"): 221}
    verify_parameter_for_calibration(p, ptgsk_size, valid_names, special_values)
    # special verification of bool parameter
    p.gs.calculate_iso_pot_energy = True
    assert p.gs.calculate_iso_pot_energy
    assert abs(p.get(ix_iso) - 1.0) == 0
    p.gs.calculate_iso_pot_energy = False
    assert not p.gs.calculate_iso_pot_energy
    assert abs(p.get(ix_iso) - 0.0) == 0
    pv = DoubleVector.from_numpy([p.get(i) for i in range(p.size())])
    pv[ix_iso] = 1.0
    p.set(pv)
    assert p.gs.calculate_iso_pot_energy
    pv[ix_iso] = 0.0
    p.set(pv)
    assert not p.gs.calculate_iso_pot_energy
    # checkout new parameters for routing
    p.routing.velocity = 1/3600.0
    p.routing.alpha = 1.1
    p.routing.beta = 0.8
    assert round(abs(p.routing.velocity - 1/3600.0), 7) == 0
    assert round(abs(p.routing.alpha - 1.1), 7) == 0
    assert round(abs(p.routing.beta - 0.8), 7) == 0
    utc = Calendar()
    assert p.gs.is_snow_season(utc.time(2017, 1, 1))
    assert not p.gs.is_snow_season(utc.time(2017, 8, 1))
    p.gs.n_winter_days = 100
    assert not p.gs.is_snow_season(utc.time(2017, 11, 31))
    assert p.gs.is_snow_season(utc.time(2017, 2, 1))
    blob = p.serialize()
    p2 = p.deserialize(blob)
    assert p == p2


def test_pt_hps_k_param():
    ptgsk_size = 24
    valid_names = [
        "kirchner.c1",
        "kirchner.c2",
        "kirchner.c3",
        "ae.ae_scale_factor",
        "hps.lw",
        "hps.tx",
        "hps.cfr",
        "hps.wind_scale",
        "hps.wind_const",
        "hps.surface_magnitude",
        "hps.max_albedo",
        "hps.min_albedo",
        "hps.fast_albedo_decay_rate",
        "hps.slow_albedo_decay_rate",
        "hps.snowfall_reset_depth",
        "hps.calculate_iso_pot_energy",
        "gm.dtf",
        "p_corr.scale_factor",
        "pt.albedo",
        "pt.alpha",
        "routing.velocity",
        "routing.alpha",
        "routing.beta",
        "msp.reservoir_direct_response_fraction"

    ]
    p = pt_hps_k.PTHPSKParameter()
    verify_parameter_for_calibration(p, ptgsk_size, valid_names)
    # special verification of bool parameter
    # p.us.calculate_iso_pot_energy = True
    # assertTrue(p.us.calculate_iso_pot_energy)
    # assertAlmostEqual(p.get(23), 1.0, 0.00001)
    # p.us.calculate_iso_pot_energy = False
    # assertFalse(p.us.calculate_iso_pot_energy)
    # assertAlmostEqual(p.get(23), 0.0, 0.00001)
    pv = DoubleVector.from_numpy([p.get(i) for i in range(p.size())])
    # pv[23] = 1.0
    # p.set(pv)
    # assertTrue(p.us.calculate_iso_pot_energy)
    # pv[23] = 0.0;
    p.set(pv)
    # assertFalse(p.us.calculate_iso_pot_energy)
    # checkout new parameters for routing
    p.routing.velocity = 1/3600.0
    p.routing.alpha = 1.1
    p.routing.beta = 0.8
    assert round(abs(p.routing.velocity - 1/3600.0), 7) == 0
    assert round(abs(p.routing.alpha - 1.1), 7) == 0
    assert round(abs(p.routing.beta - 0.8), 7) == 0


def test_pt_ss_k_param():
    ptssk_size = 21
    valid_names = [
        "kirchner.c1",
        "kirchner.c2",
        "kirchner.c3",
        "ae.ae_scale_factor",
        "ss.alpha_0",
        "ss.d_range",
        "ss.unit_size",
        "ss.max_water_fraction",
        "ss.tx",
        "ss.cx",
        "ss.ts",
        "ss.cfr",
        "p_corr.scale_factor",
        "pt.albedo",
        "pt.alpha",
        "gm.dtf",
        "routing.velocity",
        "routing.alpha",
        "routing.beta",
        "gm.direct_response",
        "msp.reservoir_direct_response_fraction"

    ]
    verify_parameter_for_calibration(pt_ss_k.PTSSKParameter(), ptssk_size, valid_names)
    p = pt_ss_k.PTSSKParameter()
    blob = p.serialize()
    p2 = p.deserialize(blob)
    assert p == p2


def test_pt_st_k_param():
    valid_names = [
        "kirchner.c1",
        "kirchner.c2",
        "kirchner.c3",
        "ae.ae_scale_factor",
        "st.shape",
        "st.tx",
        "st.cx",
        "st.ts",
        "st.lwmax",
        "st.cfr",
        "gm.dtf",
        "p_corr.scale_factor",
        "pt.albedo",
        "pt.alpha",
        "routing.velocity",
        "routing.alpha",
        "routing.beta",
        "gm.direct_response",
        "msp.reservoir_direct_response_fraction"
    ]
    ptstk_size = 19
    verify_parameter_for_calibration(pt_st_k.PTSTKParameter(), ptstk_size, valid_names)
    p = pt_st_k.PTSTKParameter()
    blob = p.serialize()
    p2 = p.deserialize(blob)
    assert p == p2


def _create_std_ptgsk_param():
    ptp = api.PriestleyTaylorParameter(albedo=0.85, alpha=1.23)
    ptp.albedo = 0.9
    ptp.alpha = 1.26
    aep = api.ActualEvapotranspirationParameter(ae_scale_factor=1.5)
    aep.ae_scale_factor = 1.1
    gsp = api.GammaSnowParameter(winter_end_day_of_year=99, initial_bare_ground_fraction=0.04, snow_cv=0.44,
                                 tx=-0.3, wind_scale=1.9, wind_const=0.9, max_water=0.11, surface_magnitude=33.0,
                                 max_albedo=0.88, min_albedo=0.55, fast_albedo_decay_rate=6.0,
                                 slow_albedo_decay_rate=4.0, snowfall_reset_depth=6.1, glacier_albedo=0.44
                                 )  # TODO: This does not work due to boost.python template arity of 15, )
    gsp.calculate_iso_pot_energy = False
    gsp.snow_cv = 0.5
    gsp.initial_bare_ground_fraction = 0.04
    kp = api.KirchnerParameter(c1=-2.55, c2=0.8, c3=-0.01)
    kp.c1 = 2.5
    kp.c2 = -0.9
    kp.c3 = 0.01
    spcp = api.PrecipitationCorrectionParameter(scale_factor=0.9)
    gm = api.GlacierMeltParameter(dtf=5.9)  # verify we can construct glacier parameter
    ptgsk_p = pt_gs_k.PTGSKParameter(ptp, gsp, aep, kp, spcp, gm)  # passing optional gm parameter here
    ptgsk_p.ae.ae_scale_factor = 1.2  # sih: just to demo ae scale_factor can be set directly
    return ptgsk_p


def test_precipitation_correction_constructor():
    spcp = api.PrecipitationCorrectionParameter(scale_factor=0.9)
    assert round(abs(0.9 - spcp.scale_factor), 7) == 0


def test_create_ptgsk_param():
    ptgsk_p = _create_std_ptgsk_param()
    copy_p = pt_gs_k.PTGSKParameter(ptgsk_p)
    assert ptgsk_p is not None, "should be possible to create a std param"
    assert copy_p is not None
    blob = ptgsk_p.serialize()
    p2 = pt_gs_k.PTGSKParameter.deserialize(blob)
    assert p2 == ptgsk_p


def _create_std_geo_cell_data():
    geo_point = api.GeoPoint(1, 2, 3)
    ltf = api.LandTypeFractions()
    ltf.set_fractions(0.2, 0.2, 0.1, 0.3)
    geo_cell_data = api.GeoCellData(geo_point, 1000.0**2, 0, 0.7, ltf)
    geo_cell_data.radiation_slope_factor = 0.7
    return geo_cell_data


def test_create_ptgsk_grid_cells():
    geo_cell_data = _create_std_geo_cell_data()
    param = _create_std_ptgsk_param()
    cell_ts = [pt_gs_k.PTGSKCellAll, pt_gs_k.PTGSKCellOpt]
    for cell_t in cell_ts:
        c = cell_t()
        c.geo = geo_cell_data
        c.set_parameter(param)
        m = c.mid_point()
        assert m is not None
        c.set_state_collection(True)


def test_create_region_environment():
    region_env = api.ARegionEnvironment()
    temp_vector = api.TemperatureSourceVector()
    region_env.temperature = temp_vector
    assert region_env is not None


def test_create_TargetSpecificationPts():
    t = api.TargetSpecificationPts()
    t.scale_factor = 1.0
    t.calc_mode = api.NASH_SUTCLIFFE
    t.calc_mode = api.KLING_GUPTA
    t.calc_mode = api.ABS_DIFF
    t.calc_mode = api.RMSE
    t.s_r = 1.0  # KGEs scale-factors
    t.s_a = 2.0
    t.s_b = 3.0
    assert t.uid is not None
    t.uid = 'test'
    assert t.uid == 'test'
    assert round(abs(t.scale_factor - 1.0), 7) == 0
    # create a ts with some points
    cal = Calendar()
    start = cal.time(2015, 1, 1, 0, 0, 0)
    dt = deltahours(1)
    tsf = TsFactory()
    times = UtcTimeVector()
    times.append(start + 1*dt)
    times.append(start + 3*dt)
    times.append(start + 4*dt)

    values = DoubleVector()
    values.push_back(1.0)
    values.push_back(3.0)
    values.push_back(np.nan)
    tsp = tsf.create_time_point_ts(UtcPeriod(start, start + 24*dt), times, values)
    # convert it from a time-point ts( as returned from current smgrepository) to a fixed interval with timeaxis, needed by calibration
    tst = api.TsTransform()
    tsa = tst.to_average(start, dt, 24, tsp)
    # tsa2 = tst.to_average(start,dt,24,tsp,False)
    # tsa_staircase = tst.to_average_staircase(start,dt,24,tsp,False) # nans infects the complete interval to nan
    # tsa_staircase2 = tst.to_average_staircase(start,dt,24,tsp,True) # skip nans, nans are 0
    # stuff it into the target spec.
    # also show how to specify snow-calibration
    cids = IntVector([0, 2, 3])
    t2 = api.TargetSpecificationPts(tsa, cids, 0.7, api.KLING_GUPTA, 1.0, 1.0, 1.0, api.SNOW_COVERED_AREA,
                                    'test_uid')
    assert t2.uid == 'test_uid'
    t2.catchment_property = api.SNOW_WATER_EQUIVALENT
    assert t2.catchment_property == api.SNOW_WATER_EQUIVALENT
    t2.catchment_property = api.CELL_CHARGE
    assert t2.catchment_property == api.CELL_CHARGE
    assert t2.catchment_indexes is not None
    for i in range(len(cids)):
        assert cids[i] == t2.catchment_indexes[i]
    t.ts = TimeSeries(tsa)  # target spec is now a regular TimeSeries
    tv = api.TargetSpecificationVector()
    tv[:] = [t, t2]
    # now verify we got something ok
    assert 2 == tv.size()
    assert round(abs(tv[0].ts.value(1) - 1.5), 7) == 0  # average value 0..1 ->0.5
    assert round(abs(tv[0].ts.value(2) - 2.5), 7) == 0  # average value 0..1 ->0.5
    # assertAlmostEqual(tv[0].ts.value(3), 3.0)  # original flat out at end, but now:
    assert math.isnan(tv[0].ts.value(3))  # strictly linear between points.
    # and that the target vector now have its own copy of ts
    tsa.set(1, 3.0)
    assert round(abs(tv[0].ts.value(1) - 1.5), 7) == 0  # make sure the ts passed onto target spec, is a copy
    assert round(abs(tsa.value(1) - 3.0), 7) == 0  # and that we really did change the source
    # Create a clone of target specification vector
    tv2 = api.TargetSpecificationVector(tv)
    assert 2 == tv2.size()
    assert round(abs(tv2[0].ts.value(1) - 1.5), 7) == 0  # average value 0..1 ->0.5
    assert round(abs(tv2[0].ts.value(2) - 2.5), 7) == 0  # average value 0..1 ->0.5
    assert math.isnan(tv2[0].ts.value(3))  # average value 0..1 ->0.5
    tv2[0].scale_factor = 10.0
    assert round(abs(tv[0].scale_factor - 1.0), 7) == 0
    assert round(abs(tv2[0].scale_factor - 10.0), 7) == 0
    # test we can create from breakpoint time-series
    ts_bp = TimeSeries(
        TimeAxis(UtcTimeVector([0, 25, 20]), 30),
        fill_value=2.0,
        point_fx=POINT_AVERAGE_VALUE
    )

    tspec_bp = api.TargetSpecificationPts(
        ts_bp,
        cids, 0.7, api.KLING_GUPTA, 1.0, 1.0, 1.0, api.CELL_CHARGE,
        'test_uid'
    )
    assert tspec_bp is not None


def test_create_target_spec_from_std_time_series():
    """
    Verify we can create target-spec giving ordinary ts,
    and that passing a non-fixed time-axis raises exception

    """
    cal = Calendar()
    ta = TimeAxis(cal.time(2017, 1, 1), deltahours(1), 24)
    ts = TimeSeries(ta, fill_value=3.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    cids = IntVector([0, 2, 3])
    t0 = api.TargetSpecificationPts(ts, cids, 0.7, api.KLING_GUPTA, 1.0, 1.0, 1.0, api.SNOW_COVERED_AREA,
                                    'test_uid')
    assert round(abs(t0.ts.value(0) - ts.value(0)), 7) == 0
    rid = 0
    t1 = api.TargetSpecificationPts(ts, rid, 0.7, api.KLING_GUPTA, 1.0, 1.0, 1.0, 'test_uid')
    assert round(abs(t1.ts.value(0) - ts.value(0)), 7) == 0
    tax = TimeAxis(UtcTimeVector.from_numpy(ta.time_points[:-1]), ta.total_period().end)
    tsx = TimeSeries(tax, fill_value=2.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    tx = api.TargetSpecificationPts(tsx, rid, 0.7, api.KLING_GUPTA, 1.0, 1.0, 1.0, 'test_uid')
    assert tx is not None


def test_IntVector():
    v1 = IntVector()  # empty
    v2 = IntVector([i for i in range(10)])  # by list
    v3 = IntVector([1, 2, 3])  # simple list
    assert v2.size() == 10
    assert v1.size() == 0
    assert len(v3) == 3


def test_DoubleVector():
    v1 = DoubleVector([i for i in range(10)])  #
    v2 = DoubleVector.FromNdArray(np.arange(0, 10.0, 0.5))
    v3 = DoubleVector(np.arange(0, 10.0, 0.5))
    assert len(v1) == 10
    assert len(v2) == 20
    assert len(v3) == 20
    assert round(abs(v2[3] - 1.5), 7) == 0


if __name__ == "__main__":
    unittest.main()
