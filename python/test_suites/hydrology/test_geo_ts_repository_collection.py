﻿"""
Tests GeoTsRepositoryCollection
"""
from os import path
from shyft.time_series import (Calendar, YMDhms, deltahours, UtcPeriod)
from shyft.hydrology import shyftdata_dir
from shyft.hydrology.repository.geo_ts_repository_collection import GeoTsRepositoryCollection
from shyft.hydrology.repository.geo_ts_repository_collection import GeoTsRepositoryCollectionError
from shyft.hydrology.repository.netcdf.met_netcdf_data_repository import MetNetcdfDataRepository
from shyft.hydrology.repository.netcdf.met_netcdf_data_repository import MetNetcdfDataRepositoryError
from shapely.geometry import box
import pytest


def arome_epsg_bbox():
    """A slice of test-data located in shyft-data repository/arome."""
    EPSG = 32632
    x0 = 436100.0  # lower left
    y0 = 6823000.0  # lower right
    nx = 74
    ny = 24
    dx = 1000.0
    dy = 1000.0
    return EPSG, ([x0, x0 + nx*dx, x0 + nx*dx, x0], [y0, y0, y0 + ny*dy, y0 + ny*dy]), box(x0, y0, x0 + dx*nx, y0 + dy*ny)


def test_get_timeseries_collection():
    tc = YMDhms(2015, 8, 24, 6)
    n_hours = 30
    dt = deltahours(1)
    utc = Calendar()  # No offset gives Utc
    t0 = utc.time(tc)
    period = UtcPeriod(t0, t0 + deltahours(n_hours))
    date_str = "{}{:02}{:02}_{:02}".format(tc.year, tc.month, tc.day, tc.hour)

    epsg, bbox, bpoly = arome_epsg_bbox()

    base_dir = path.join(shyftdata_dir, "repository", "arome_data_repository")
    f1 = "arome_metcoop_red_default2_5km_{}.nc".format(date_str)
    f2 = "arome_metcoop_red_test2_5km_{}.nc".format(date_str)

    ar1 = MetNetcdfDataRepository(epsg, base_dir, filename=f1, allow_subset=True)
    ar2 = MetNetcdfDataRepository(epsg, base_dir, filename=f2, elevation_file=f1, allow_subset=True)

    geo_ts_repository = GeoTsRepositoryCollection([ar1, ar1, ar2])
    sources_replace = geo_ts_repository.get_timeseries(("temperature", "radiation"),
                                                       period, geo_location_criteria=bpoly)

    with pytest.raises(GeoTsRepositoryCollectionError) as context:
        GeoTsRepositoryCollection([ar1, ar2], reduce_type="foo")

    geo_ts_repository = GeoTsRepositoryCollection([ar1, ar1, ar2], reduce_type="add")
    sources_add = geo_ts_repository.get_timeseries(("temperature", "radiation"),
                                                   period, geo_location_criteria=bpoly)
    assert len(sources_add["temperature"]) > len(sources_replace["temperature"])


def test_get_forecast_collection():
    n_hours = 30
    dt = deltahours(1)
    utc = Calendar()  # No offset gives Utc
    tc = YMDhms(2015, 8, 24, 6)
    t0 = utc.time(tc)
    period = UtcPeriod(t0, t0 + deltahours(n_hours))
    date_str = "{}{:02}{:02}_{:02}".format(tc.year, tc.month, tc.day, tc.hour)

    epsg, bbox, bpoly = arome_epsg_bbox()

    base_dir = path.join(shyftdata_dir, "repository", "arome_data_repository")
    f1_elev = "arome_metcoop_red_default2_5km_{}.nc".format(date_str)
    f1 = r"arome_metcoop_red_default2_5km_(\d{4})(\d{2})(\d{2})[T_](\d{2})Z?.nc$"
    # f2 = "arome_metcoop_red_test2_5km_{}.nc".format(date_str)
    f2 = r"arome_metcoop_red_test2_5km_(\d{4})(\d{2})(\d{2})[T_](\d{2})Z?.nc$"

    ar1 = MetNetcdfDataRepository(epsg, base_dir, filename=f1, allow_subset=True)
    ar2 = MetNetcdfDataRepository(epsg, base_dir, filename=f2, elevation_file=f1_elev, allow_subset=True)

    geo_ts_repository = GeoTsRepositoryCollection([ar1, ar2])
    source_names = ("temperature", "radiation")
    sources = geo_ts_repository.get_forecast(source_names, period, t0,
                                             geo_location_criteria=bpoly)
    assert all([x in source_names for x in sources])

    geo_ts_repository = GeoTsRepositoryCollection([ar1, ar2], reduce_type="add")
    with pytest.raises(GeoTsRepositoryCollectionError) as context:
        sources = geo_ts_repository.get_forecast(("temperature", "radiation"),
                                                 period, t0, geo_location_criteria=bpoly)


def test_get_ensemble_forecast_collection():
    EPSG = 32633
    upper_left_x = 436100.0
    upper_left_y = 7417800.0
    nx = 74
    ny = 94
    dx = 1000.0
    dy = 1000.0
    t0 = YMDhms(2015, 7, 26, 0)
    n_hours = 30
    utc = Calendar()  # No offset gives Utc
    period = UtcPeriod(utc.time(t0), utc.time(t0) + deltahours(n_hours))
    t_c = utc.time(t0) + deltahours(1)

    base_dir = path.join(shyftdata_dir, "netcdf", "arome")
    # pattern = "fc*.nc"
    pattern = r"fc_(\d{4})(\d{2})(\d{2})[T_](\d{2})Z?.nc$"
    bpoly = box(upper_left_x, upper_left_y - ny*dy, upper_left_x + nx*dx, upper_left_y)
    try:
        ar1 = MetNetcdfDataRepository(EPSG, base_dir, filename=pattern)
        ar2 = MetNetcdfDataRepository(EPSG, base_dir, filename=pattern)
        repos = GeoTsRepositoryCollection([ar1, ar2])
        data_names = ("temperature", "wind_speed", "relative_humidity")
        ensemble = repos.get_forecast_ensemble(data_names, period, t_c, None)
        assert isinstance(ensemble, list)
        assert len(ensemble) == 10
        with pytest.raises(GeoTsRepositoryCollectionError) as context:
            repos = GeoTsRepositoryCollection([ar1, ar2], reduce_type="add")
            repos.get_forecast_ensemble(data_names, period, t_c, geo_location_criteria=bpoly)
        assert "Only replace is supported yet" == context.exception.args[0]
    except MetNetcdfDataRepositoryError as adre:
        pytest.skip(f"(test inconclusive- missing arome-data {adre})")


