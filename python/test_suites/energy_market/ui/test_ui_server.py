
from shyft.energy_market import ui
from shyft import time_series as sa
import json


def generate_json(name: str, args: str):
    if name == "test":
        res = {}
        res["name"] = "generated_layout"
        res["widget"] = {"misc": "data"}
        return json.dumps(res)
    else:
        return ""


def test_server(tmpdir):
    root_dir = (tmpdir/"t_ui_srv")
    s = ui.LayoutServer(str(root_dir))
    s.fx = generate_json
    pn = s.start_server()
    c = ui.LayoutClient(host_port="localhost:" + str(pn), timeout_ms=1000)

    assert len(c.get_model_infos(sa.Int64Vector())) == 0
    # 1. Store a model:
    li = ui.LayoutInfo(1, "Laye oeuitte", "")
    mi = ui.ModelInfo(li.id, li.name, sa.utctime_now(), "")
    c.store_model(li, mi)
    assert len(c.get_model_infos(sa.Int64Vector())) == 1

    # 2. Read model
    li2 = c.read_model(1)
    assert li == li2

    # 4. Generate layout from fx
    li3 = c.read_model_with_args(-1, "test", "{misc.}", True)
    assert li3.id == 2
    assert li3.name == "test"
    assert li3.json == generate_json("test", "{misc.}")
    li4 = c.read_model_with_args(-1, "test", "{misc.}", False)
    assert li4.id == -1
    assert li4.name == li3.name
    assert li4.json == li3.json
    assert len(c.get_model_infos(sa.Int64Vector())) == 2

    # 3. Remove model
    c.remove_model(1)
    assert len(c.get_model_infos(sa.Int64Vector())) == 1

