from shyft.energy_market.stm import StmModelRef, ModelRefList,StmCase, StmTask
from shyft import time_series as sa


def test_model_ref():
    ri = StmModelRef("host", 123, 456, "key")
    assert ri == StmModelRef("host", 123, 456, "key")
    assert ri != StmModelRef("guest", 123, 456, "key")

    assert hasattr(ri, "host")
    assert ri.host == "host"
    assert hasattr(ri, "port_num")
    assert ri.port_num == 123
    assert ri.api_port_num == 456
    assert hasattr(ri, "model_key")
    assert ri.model_key == "key"

    ris = ModelRefList()
    assert len(ris) == 0
    ris.append(ri)
    assert len(ris) == 1
    assert ris[0] == ri


def test_stm_case():
    def verify_stm_case(run: StmCase):
        assert hasattr(run, "id")
        assert hasattr(run, "name")
        assert hasattr(run, "created")
        assert hasattr(run, "json")
        assert hasattr(run, "labels")
        assert hasattr(run, "model_refs")
    # 1. Default run:
    case1 = StmCase()
    verify_stm_case(case1)
    assert case1.id == 0
    assert case1.name == ""
    assert case1.json == ""
    assert case1.created == sa.no_utctime
    assert len(case1.labels) == 0
    assert len(case1.model_refs) == 0

    # 2. Add a model reference:
    mr = StmModelRef("host", 12, 34, "testkey")
    case1.add_model_ref(mr)
    assert len(case1.model_refs) == 1
    assert case1.model_refs[0] == mr
    assert case1.get_model_ref("testkey") == mr
    assert case1.get_model_ref("nonkey") is None
    assert not case1.remove_model_ref("nonkey")
    assert case1.remove_model_ref("testkey")
    assert len(case1.model_refs) == 0

    # 2. Constructor with default parameters:
    t0 = sa.utctime_now()
    case2 = StmCase(2, "testrun", t0)
    verify_stm_case(case2)
    assert case2.id == 2
    assert case2.name == "testrun"
    assert case2.created == t0
    assert len(case2.labels) == 0
    assert len(case2.model_refs) == 0
    case2.add_model_ref(StmModelRef())
    case2.add_model_ref(StmModelRef("host", 12, 34, "key"))
    assert len(case2.model_refs) == 2

    # 3. Constructor with non-default parameters:
    model_refs = ModelRefList()
    model_refs.append(StmModelRef())
    model_refs.append(StmModelRef("host", 123, 456, "key"))
    run3 = StmCase(3, "testcase2", t0, json="misc.", labels=["Tag1", "Tag2"], model_refs=model_refs)
    verify_stm_case(run3)
    assert run3.id == 3
    assert run3.name == "testcase2"
    assert run3.created == t0
    assert run3.json == "misc."
    assert len(run3.labels) == 2
    assert run3.labels[0] == "Tag1"
    assert run3.labels[1] == "Tag2"
    assert len(run3.model_refs) == 2
    assert run3.model_refs[0] == StmModelRef()
    assert run3.model_refs[1] == StmModelRef("host", 123, 456, "key")
    assert run3 != StmCase(3, "testcase2", t0, json="misc.")


def test_stm_task():
    # 0. Create a task:
    task = StmTask(1, "testtask", sa.utctime_now())
    assert hasattr(task, "id")
    assert hasattr(task, "name")
    assert hasattr(task, "created")
    assert hasattr(task, "json")
    assert hasattr(task, "labels")
    assert hasattr(task, "cases")
    assert hasattr(task, "base_model")
    assert hasattr(task, "task_name")

    assert task.id == 1
    assert task.name == "testtask"
    assert task.json == ""
    assert len(task.labels) == 0
    assert len(task.cases) == 0
    assert isinstance(task.base_model, StmModelRef)
    assert task.task_name == ""

    # We add a run:
    run = StmCase(2, "testrun", sa.utctime_now())
    task.add_case(run)
    assert len(task.cases) == 1
    assert run == task.cases[0]
    case2 = StmCase(3, "testcase2", sa.utctime_now())
    task.add_case(case2)
    assert len(task.cases) == 2
    assert case2 == task.cases[1]

    # Get run:
    assert run == task.get_case(2)
    assert task.get_case(4) is None
    assert case2 == task.get_case("testcase2")
    assert task.get_case("norun") is None

    # Update run
    run = task.get_case(2)
    run.name = "update_name"
    run.labels = ["1", "2"]
    run.json = "{'key': value}"
    task.update_case(run)
    updated_run = task.get_case(2)
    assert updated_run.name == "update_name"
    assert updated_run.labels == ["1", "2"]
    assert updated_run.json == "{'key': value}"


    # Remove runs using id or name:
    assert task.remove_case(2)
    assert len(task.cases) == 1
    assert not task.remove_case(2)
    assert task.remove_case("testcase2")
    assert len(task.cases) == 0
    assert not task.remove_case("not a run")

