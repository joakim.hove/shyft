import pytest
from pathlib import Path
from .models import create_test_hydro_power_system
from shyft.energy_market.stm.model_repository import ModelRepository
from shyft.energy_market.stm import StmSystem, MarketArea

from shyft.time_series import utctime_now, time, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE


def create_simple_stm_system(id: int, name: str, some_json: str):
    """ helper to create a complete system with market and stm hps"""
    a = StmSystem(id, name, some_json)
    a.hydro_power_systems.append(create_test_hydro_power_system(hps_id=1, name='ulla-førre'))
    no_1 = MarketArea(1, 'NO1', 'json{}', a)
    no_1.price.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=3.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=1300.0, point_fx=POINT_AVERAGE_VALUE)
    a.market_areas.append(no_1)
    return a


def test_create_read_delete(tmpdir):
    """
    Run a test-story where we use all the functions of the repository
    :return:
    """
    mdir = (tmpdir/"t_crd_stm")
    mdir.mkdir()
    # first check that zero models give zero back
    ms = ModelRepository(Path(mdir))
    model_list = ms.get_model_infos()
    assert model_list is not None
    assert len(model_list) == 0

    # create one model and verify we get it listed
    m = create_simple_stm_system(id=1, name='test_one', some_json='you decide')
    m_id = ms.save_model(m)
    assert m_id > 0
    model_list = ms.get_model_infos()
    assert len(model_list) == 1
    mi = model_list[0]
    assert mi.id == 1
    assert mi.name == 'test_one'
    assert abs(int(utctime_now()) - mi.created) < 100
    m2 = create_simple_stm_system(id=2, name='test_one', some_json='you decide')
    m_id2 = ms.save_model(m2)  # save another model
    assert m_id2 != m_id  # verify it's different id
    model_list = ms.get_model_infos()
    assert len(model_list) == 2, 'should have two models by now'
    # auto create id, if we pass 0
    m2.id = 0
    m_id3 = ms.save_model(m2)
    assert m_id3 == 3, 'expect 3 here'
    ms.delete_model(m_id3)
    ms.delete_model(m_id2)
    # now read the model and compare with original
    mr = ms.get_model(m_id)
    assert mr.id == mi.id
    assert mr.name == mi.name
    mr_list = ms.get_models([m_id])  # also test reading by list work
    assert len(mr_list) == 1

    # then remove the model and verify it's away
    ms.delete_model(m_id)
    model_list = ms.get_model_infos()
    assert len(model_list) == 0


def test_create_model_service(tmpdir):
    mdirx= (tmpdir/"t_cms_1")
    mdirx.mkdir()
    mdir = Path(mdirx)
    ms = ModelRepository(mdir)  # works on existing directory
    with pytest.raises(RuntimeError):
        x = ModelRepository(Path(r'dir_does_not_exist'))  # fail on non-existing
    with pytest.raises(RuntimeError):
        x = ModelRepository(Path(__file__))  # file should also fail
