from time import sleep

from shyft.energy_market.stm import DStmClient, DStmServer, ModelState
from shyft.time_series import (point_interpretation_policy as point_fx, Calendar,
    TsVector, TimeSeries, TimeAxis, time, deltahours, DtsServer, DtsClient
)

import pytest
shop = pytest.importorskip("shyft.energy_market.stm.shop")

if not shop.shyft_with_shop():
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)


def create_test_optimization_commands(run_id: int, write_files: bool) -> shop.ShopCommandList:
    r = shop.ShopCommandList()
    if write_files: r.append(
        shop.ShopCommand.log_file(f"shop_log_{run_id}.txt")
    )
    r.extend([
        shop.ShopCommand.set_method_primal(),
        shop.ShopCommand.set_code_full(),
        shop.ShopCommand.start_sim(3),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(3)
    ])
    if write_files: r.extend([
        shop.ShopCommand.return_simres(f"shop_results_{run_id}.txt"),
        shop.ShopCommand.save_series(f"shop_series_{run_id}.txt"),
        shop.ShopCommand.save_xmlseries(f"shop_series_{run_id}.xml"),
        shop.ShopCommand.return_simres_gen(f"shop_genres_{run_id}.xml")
    ])
    return r


def test_remote_shop_logging(port_no, web_api_port, system_to_optimize):
    assert port_no != web_api_port
    srv = DStmServer()
    srv.set_listening_port(port_no)
    srv.start_server()
    # Add a
    mid = "optimize"
    srv.do_add_model(mid, system_to_optimize)
    host = "127.0.0.1"
    client = DStmClient(f"{host}:{port_no}", 1000)
    try:
        assert len(client.get_model_ids()) == 1
        # optimize model remotely:
        t_begin = time('2018-10-17T10:00:00Z')
        t_end = time('2018-10-18T10:00:00Z')
        t_step = deltahours(1)
        n_steps = int((t_end - t_begin) / t_step)
        ta = TimeAxis(t_begin.seconds, t_step.seconds, n_steps)
        shop_commands = create_test_optimization_commands(1, False)
        assert len(client.get_log(mid)) == 0  # == "Unable to find log."
        assert client.optimize(mid, ta, shop_commands)
        while client.get_state(mid) != ModelState.FINISHED:  # poll until it's done (we resolve this later!)
            sleep(0.2)
        assert client.get_state(mid) == ModelState.FINISHED
        assert client.get_model(mid)  # This will not wait until optimization is complete
        assert len(client.get_log(mid)) > 200
    finally:
        client.close()
        srv.close()


def test_dstm_w_master_dtss(tmp_path, port_no, web_api_port, system_to_optimize):
    assert port_no != web_api_port

    dtss = DtsServer()  # start the master dtss keeping the external ts data
    dtss.set_container("test", str(tmp_path/"test"))
    dtss_port = dtss.start_async()

    # the main server
    srv = DStmServer()
    srv.set_listening_port(port_no)
    srv.start_server()

    # the dstm compute node server that does the actual optimization
    cn = DStmServer()
    cn_port = cn.start_server()  # start the compute node(random port), get the port
    cn_host_port = f'localhost:{cn_port}'
    srv.add_compute_node(cn_host_port)  # then register it for usage at the main server
    assert len(srv.compute_node_info()) == 1
    #
    # IMPORTANT: THIS IS HOW YOU CONNECT THE DSTM to the MASTER DTSS
    #
    srv.set_master_slave_mode(ip='localhost',    # set this dstm to use the dtss above for all io
                              port=dtss_port,
                              master_poll_time=0.01,  # maximum time to stay out of sync
                              unsubscribe_threshold=10,  # reduce dstm subscription when it lost interest in more than 10 timeseries
                              unsubscribe_max_delay=1.0  # or maximum this delay (even if you unsubscribe just one, dont wait more than this)
                              )

    # Add a
    mid = "optimize"
    host = "127.0.0.1"
    client = DStmClient(f"{host}:{port_no}", 1000)
    assert len(client.compute_node_info()) == 1

    t_begin = time('2018-10-17T10:00:00Z')
    t_end = time('2018-10-18T10:00:00Z')
    t_step = deltahours(1)
    n_steps = int((t_end - t_begin)/t_step)
    ta = TimeAxis(t_begin.seconds, t_step.seconds, n_steps)

    # Arrange the master dtss with the system price
    sys_price_ts_name = 'shyft://test/sys_price'
    orig_sys_price = system_to_optimize.market_areas[0].price.value
    tsv = TsVector([TimeSeries(sys_price_ts_name, orig_sys_price)])
    m = DtsClient(f'localhost:{dtss_port}')  # client to the dtss
    m.store_ts(tsv, True, True)
    system_to_optimize.market_areas[0].price.value = TimeSeries(sys_price_ts_name)  # set an unbound ts to the market
    srv.do_add_model(mid, system_to_optimize)
    try:
        assert len(client.get_model_ids()) == 1

        #
        # IMPORTANT: When you have UNBOUND parameterized models, you need to evaluate those
        #            expressions prior to optimization.
        #
        client.evaluate_model(mid,           # we have to evaluate/bind the model PRIOR to optimize
                              bind_period=ta.total_period(),
                              use_ts_cached_read=True,
                              update_ts_cache=True)
        # optimize model remotely:
        shop_commands = create_test_optimization_commands(1, False)
        assert len(client.get_log(mid)) == 0  # == "Unable to find log."
        assert client.optimize(mid, ta, shop_commands)
        while client.get_state(mid) == ModelState.RUNNING:  # poll until it's done (we resolve this later!)
            sleep(0.2)
        assert client.get_state(mid) == ModelState.FINISHED
        cn_info=client.compute_node_info()
        assert len(cn_info) == 1
        result_model = client.get_model(mid)  # This will not wait until optimization is complete
        assert result_model
        assert (orig_sys_price - result_model.market_areas[0].price.value).abs().values.to_numpy().sum() < 0.001  # the proof, the dstm talked to the master
        assert len(client.get_log(mid)) > 200
        summary=client.get_optimization_summary(mid)
        assert summary
        #
        # How to get time-series from a specific dstm model
        #
        ts_urls=system_to_optimize.result_ts_urls(f'dstm://M{mid}')  # this generate all time-series that could have results
        assert len(ts_urls)
        rts= client.get_ts(mid,ts_urls)  # this fetches the time-series back.
        assert len(rts) == len(ts_urls)
        # just verify we can maninpulate the state
        client.set_state(mid,ModelState.IDLE)
        assert client.get_state(mid) == ModelState.IDLE
        client.set_state(mid,ModelState.SETUP)
        assert client.get_state(mid) == ModelState.SETUP
        # verify we can remove compute node
        client.remove_compute_node(cn_host_port)
        assert len(srv.compute_node_info()) == 0
    finally:
        client.close()
        srv.close()
        cn.close()
        dtss.clear()
