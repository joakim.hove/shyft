from .utility import create_hydro_power_system
from shyft.energy_market.core import HydroPowerSystem, HydroPowerSystemBuilder
from shyft.energy_market.core import (
    Model, ModelBuilder, Gate,
    downstream_reservoirs, downstream_units, upstream_reservoirs, upstream_units,
)
import pytest


class TestWithHydroPowerSystem:
    def test_can_create_hps(self):
        a = create_hydro_power_system(hps_id=1, name='hps')
        assert a is not None
        assert len(a.reservoirs) > 0
        assert a.reservoirs[0].hps is not None
        assert a.reservoirs[0].hps.name == a.name
        assert a.units[0].power_plant
        mb = ModelBuilder(Model(1, "m1"))
        ma = mb.create_model_area(1, "a")
        ma.detailed_hydro = a
        assert ma.detailed_hydro is not None

    def test_robust_add_unit(self):
        s1 = HydroPowerSystem(1, 's1')
        s2 = HydroPowerSystem(2, 's2')
        pp = s1.create_power_plant(1, 'pp')
        u2 = s2.create_unit(2, 'u2')
        u1 = s1.create_unit(1, 'u1')
        with pytest.raises(RuntimeError):  # refuse to add objects from two different hps systems
            pp.add_unit(u2)
        pp.add_unit(u1)
        with pytest.raises(RuntimeError):  # refuse to add duplicate
            pp.add_unit(u1)
        p2 = s1.create_power_plant(2, 'p2')
        with pytest.raises(RuntimeError):  # refuse to add unit that is already owned by other
            p2.add_unit(u1)

    def test_robust_add_gate(self):
        s1 = HydroPowerSystem(1, 's1')
        s2 = HydroPowerSystem(2, 's2')
        b1 = HydroPowerSystemBuilder(s1)
        w1 = b1.create_waterway(1, 'w1', 'xx')
        assert not s1.equal_structure(s2)
        g1 = s1.create_gate(1, "Gate1", "{type:'binary'}")
        w1.add_gate(g1)
        assert g1 and g1.waterway
        with pytest.raises(RuntimeError):
            w1.add_gate(g1)

    def test_equal_structure_with_test_system(self):
        a = create_hydro_power_system(hps_id=1, name='a')
        b = create_hydro_power_system(hps_id=2, name='b')
        assert a.equal_structure(b)
        assert a.equal_content(b)

    def test_equal_structure_different_ids(self):
        """Create two systems each containing a single reservoir with different names and ids.
        Check that the two systems are structurally equal."""
        hps0 = HydroPowerSystem(0, 'hps 0')
        hps0.create_reservoir(1, 'rsv 0')

        hps1 = HydroPowerSystem(1, 'hps 1')
        hps1.create_reservoir(1, 'rsv 1')

        assert hps0.equal_structure(hps1)

    def test_equal_structure_permuted_components(self):
        """Create two systems containing the same components, but added to the system in different order.
        Check that the two systems are structurally equal."""
        hps0 = HydroPowerSystem(0, 'hps 0')
        hps0.create_reservoir(0, 'rsv 0')
        hps0.create_reservoir(1, 'rsv 1')

        hps1 = HydroPowerSystem(1, 'hps 1')
        hps1.create_reservoir(1, 'rsv 1')
        hps1.create_reservoir(0, 'rsv 0')

        assert hps0.equal_structure(hps1)
        assert hps0.equal_content(hps1)
        assert hps0 != hps1

    def test_equal_structure_missing_connection(self):
        """Create two systems having the same components, but only one has connected components.
        Check that the systems are not structurally equal"""

        hps0 = HydroPowerSystem(0, 'hps 0')
        hps0.create_reservoir(0, 'rsv 0')
        wv0 = hps0.create_tunnel(0, 'water 0')

        hps1 = HydroPowerSystem(1, 'hps 1')
        reservoir = hps1.create_reservoir(0, 'rsv 0')
        waterway = hps1.create_tunnel(0, 'water 0')
        reservoir.output_to(waterway)

        assert not hps0.equal_structure(hps1)
        assert not wv0.equal_structure(waterway)  # also possible to go into the details of the hydro components

    def test_equal_structure_reversed_connection(self):
        """Create two systems having the same components, but in one system a connection is reversed.
        Check that the systems are not structurally equal"""

        hps0 = HydroPowerSystem(0, 'hps 0')
        reservoir = hps0.create_reservoir(0, 'rsv 0')
        waterway = hps0.create_tunnel(0, 'water 0')
        waterway.output_to(reservoir)

        hps1 = HydroPowerSystem(1, 'hps 1')
        reservoir = hps1.create_reservoir(0, 'rsv 0')
        waterway = hps1.create_tunnel(0, 'water 0')
        reservoir.output_to(waterway)

        assert not hps0.equal_structure(hps1)

    def test_equal_structure_permuted_connections(self):
        """Create two systems having the same components and connections, but with connections added in
        a different order. Check that the systems are structurally equal."""
        hps0 = HydroPowerSystem(0, 'hps 0')
        rsv0 = hps0.create_reservoir(0, 'rsv 0')
        tun0 = hps0.create_tunnel(0, 'tun 0')
        tun1x = hps0.create_tunnel(1, 'tun 1')
        rsv0.output_to(tun0).output_to(tun1x)

        hps1 = HydroPowerSystem(1, 'hps 1')
        rsv0 = hps1.create_reservoir(0, 'rsv 0')
        tun0 = hps1.create_tunnel(0, 'tun 0')
        tun1 = hps1.create_tunnel(1, 'tun 1')
        rsv0.output_to(tun1).output_to(tun0)

        assert hps0.equal_structure(hps1)
        assert hps0.equal_content(hps1)
        tun1.json = 'a small diff'
        assert hps0.equal_structure(hps1)
        assert tun1.equal_structure(tun1x)
        assert tun1.equal_structure(tun1)
        assert not tun1.equal_structure(rsv0)  # because rsv0 is a diff component type

        assert not hps0.equal_content(hps1)

    def test_serialize_and_deserialize_xml(self):
        b = create_hydro_power_system(hps_id=1, name='hps')
        blob_of_b = b.to_blob()
        b_2 = HydroPowerSystem.from_blob(blob_of_b)
        assert b.equal_structure(b_2)


class TestGraph:

    @pytest.fixture(name='test_hps')
    def fixture_test_hps(self):
        hps = HydroPowerSystem(1, 'test')

        rsv_1 = hps.create_reservoir(1, 'rsv_1')
        rsv_2 = hps.create_reservoir(2, 'rsv_2')

        wtr_1 = hps.create_tunnel(1, 'wtr_1').input_from(rsv_1)
        wtr_2 = hps.create_tunnel(2, 'wtr_2').input_from(rsv_2)
        wtr_3 = hps.create_tunnel(3, 'wtr_3').input_from(wtr_1).input_from(wtr_2)
        wtr_4 = hps.create_tunnel(4, 'wtr_4').input_from(wtr_3)
        wtr_5 = hps.create_tunnel(5, 'wtr_5').input_from(wtr_3)

        unit_1 = hps.create_unit(1, 'unit_1').input_from(wtr_4)
        unit_2 = hps.create_unit(2, 'unit_2').input_from(wtr_5)

        wtr_6 = hps.create_tunnel(6, 'wtr_6').input_from(unit_1)
        wtr_7 = hps.create_tunnel(7, 'wtr_7').input_from(unit_2)

        rsv_3 = hps.create_reservoir(3, 'rsv_3').input_from(wtr_6).input_from(wtr_7)
        wtr_8 = hps.create_tunnel(8, 'wtr_8').input_from(rsv_3)

        return hps

    def test_issue(self):
        hps = HydroPowerSystem(1, 'test')

        rsv_1 = hps.create_reservoir(1, 'rsv_1')
        rsv_2 = hps.create_reservoir(2, 'rsv_2')

        wtr_1 = hps.create_tunnel(1, 'wtr_1').input_from(rsv_1)
        wtr_2 = hps.create_tunnel(2, 'wtr_2').input_from(wtr_1).output_to(rsv_2)

        downstream_of_rsv_1 = {r.name for r in downstream_reservoirs(rsv_1)}
        upstream_of_rsv_2 = {r.name for r in upstream_reservoirs(rsv_2)}

        assert downstream_of_rsv_1 == {'rsv_2'}
        assert upstream_of_rsv_2 == {'rsv_1'}

    def test_rsv_downstream_units(self, test_hps):
        rsv_1 = test_hps.find_reservoir_by_name('rsv_1')
        rsv_3 = test_hps.find_reservoir_by_name('rsv_3')
        wtr_8 = test_hps.find_waterway_by_name('wtr_8')

        downstream_of_rsv_1 = {u.name for u in downstream_units(rsv_1)}
        downstream_of_rsv_3 = {u.name for u in downstream_units(rsv_3)}
        downstream_of_wtr_8 = {u.name for u in downstream_units(wtr_8)}

        assert downstream_of_rsv_1 == {'unit_1', 'unit_2'}
        assert downstream_of_rsv_3 == set()
        assert downstream_of_wtr_8 == set()

    def test_rsv_upstream_units(self, test_hps):
        rsv_1 = test_hps.find_reservoir_by_id(1)
        rsv_3 = test_hps.find_reservoir_by_id(3)

        upstream_of_rsv_1 = {u.name for u in upstream_units(rsv_1)}
        upstream_of_rsv_3 = {u.name for u in upstream_units(rsv_3)}

        assert upstream_of_rsv_1 == set()
        assert upstream_of_rsv_3 == {'unit_1', 'unit_2'}

    def test_unit_downstream_reservoirs(self, test_hps):
        unit_1 = test_hps.find_unit_by_id(1)

        downstream_of_unit_1 = {u.name for u in downstream_reservoirs(unit_1)}

        assert downstream_of_unit_1 == {'rsv_3'}

    def test_unit_upstream_reservoirs(self, test_hps):
        unit_1 = test_hps.find_unit_by_id(1)

        upstream_of_unit_1 = {u.name for u in upstream_reservoirs(unit_1)}

        assert upstream_of_unit_1 == {'rsv_1', 'rsv_2'}


if __name__ == '__main__':
    t = TestGraph()
    t.test_issue()
