#from shyft.dashboard.test.time_series.test_time_series_fixtures import mock_parent

from bokeh.models import Selection

from shyft.dashboard.time_series.view import LegendItem, Line, FillInBetween
from shyft.dashboard.time_series.view_container.legend import Legend


def test_legend(mock_parent):
    # create Legend
    legend = Legend(viewer=mock_parent,
                    title="test_title",
                    width=200, height=300,
                    text_font_size=8,
                    text_font='monospace',
                    title_text_font_style ='bold',
                    title_text_font_size='10pt',
                    )

    # create views and legend items
    line1 = Line(color='blue', visible=True, label='Line 1', index=1, unit='MW', view_container=mock_parent)
    item1 = LegendItem(view_container=mock_parent, views=[line1], label="line 1")

    line2 = Line(color='blue', visible=True, label='Line 2', index=1, unit='MW', view_container=mock_parent)
    fib2 = FillInBetween(color='blue', visible=True, label='fib 2', indices=(1, 2),
                         unit='MW', view_container=mock_parent)
    item2 = LegendItem(view_container=mock_parent, views=[line2, fib2], label='')
    assert item2.label == ', '.join([line2.label, fib2.label])

    # ------------------------------------------
    # add view(s)
    legend.add_view(view=item1)

    assert len(legend.legend_items.keys()) == 1
    assert item1 in legend.legend_items.values()
    assert len(legend.ds.data['tag']) == 1
    assert legend.ds.data['tag'][0] == str(item1.uid)
    assert legend.layout
    assert len(legend.layout_components) == 2

    legend.add_view(view=item2)

    assert len(legend.legend_items.keys()) == 2
    assert item2 in legend.legend_items.values()
    assert len(legend.ds.data['tag']) == 3
    assert legend.ds.data['tag'][1] == str(item2.uid)


    # -----------------------------------------
    # visibility callback
    # assert that item 1 drawn as visible
    assert legend.alpha_visible == legend.ds.data['alpha'][0]
    # trigger callback
    line1.visible = False
    # assert that item 1 drawn as invisible
    assert legend.alpha_invisible == legend.ds.data['alpha'][0]

    # now check with 2 views
    assert legend.alpha_visible == legend.ds.data['alpha'][1]
    # trigger callback
    line2.visible = False
    # assert that item 2 drawn as visible since only line is invisible
    assert legend.alpha_visible == legend.ds.data['alpha'][1]
    # trigger callback
    fib2.visible = False
    # assert that item 2 drawn as visible since only line is invisible
    assert legend.alpha_invisible == legend.ds.data['alpha'][1]

    assert legend.visible_callback(obj=[], attr='foo', old_value='foo', new_value='bar') is None

    # -----------------------------------------
    # on change selected
    # select first item and check if it is visible again
    assert not line1.visible
    new = [0]
    legend.ds.selected.indices = new
    assert line1.visible

    # select second item and check if it is visible again
    assert not line2.visible
    assert not fib2.visible
    new = [1]
    legend.ds.selected.indices = new
    assert line2.visible
    assert fib2.visible

    # fire label callback (to get coverage at least)
    legend.label_callback(obj=item1,attr="foo",old_value='a',new_value='b')

    # -----------------------------------------
    # remove 1 specific view
    legend.clear_views(specific_views=[item1])

    assert len(legend.legend_items.keys()) == 1
    assert item2 in legend.legend_items.values()
    assert len(legend.ds.data['tag']) == 2
    assert legend.ds.data['tag'][0] == str(item2.uid)
    assert legend.ds.data['tag'][1] == str(item2.uid)

    #-- ensure it picks out both views of item2
    legend.clear_views(specific_views=[item2])
    assert len(legend.legend_items.keys()) == 0
    assert len(legend.ds.data['tag']) == 0

    #--re-add a view to ensure clear does work
    legend.add_view(view=item1)
    # -----------------------------------------
    # remove all views
    legend.clear_views()

    assert len(legend.legend_items.keys()) == 0
    assert len(legend.ds.data['tag']) == 0
