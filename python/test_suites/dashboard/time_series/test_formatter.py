import numpy as np
from shyft.time_series import Calendar, utctime_now
from shyft.dashboard.time_series.formatter import conditional_time_formatter, basic_time_formatter


def test_table_time_formatter():
    cal = Calendar()
    now = 1520871970

    # test not day, week, month, quater, year
    t_day = np.array([now, now+cal.HOUR, now+cal.HOUR*2])
    f_day = conditional_time_formatter(time_input=t_day)
    assert f_day == ['2018.03.12 16:26', '2018.03.12 17:26', '2018.03.12 18:26']

    # test day
    t_day = np.array([now, now + cal.DAY, now + cal.DAY*2])
    f_day = conditional_time_formatter(time_input=t_day)
    assert f_day == ['2018.03.12 (W.11)', '2018.03.13 (W.11)', '2018.03.14 (W.11)']

    # test Week
    t_day = np.array([now, now + cal.WEEK, now + cal.WEEK*2])
    f_day = conditional_time_formatter(time_input=t_day)
    assert f_day == ['2018 Week 11', '2018 Week 12', '2018 Week 13']

    # test month
    t_day = np.array([cal.trim(now, cal.MONTH),
                      cal.trim(now + cal.MONTH, cal.MONTH),
                      cal.trim(now + cal.MONTH*2, cal.MONTH)])
    f_day = conditional_time_formatter(time_input=t_day)
    assert f_day == ['2018 Mar', '2018 Apr', '2018 May']

    # test quarter
    t_day = np.array([cal.trim(now, cal.QUARTER),
                      cal.trim(now+cal.QUARTER, cal.QUARTER),
                      cal.trim(now+cal.QUARTER*2, cal.QUARTER)])
    f_day = conditional_time_formatter(time_input=t_day)
    assert f_day == ['2018 Q.1', '2018 Q.2', '2018 Q.3']

    # test year
    t_day = np.array([now, now + cal.YEAR, now + cal.YEAR*2])
    f_day = conditional_time_formatter(time_input=t_day)
    assert f_day == ['2018', '2019', '2020']


def test_basic_time_formatter():
    cal = Calendar()
    now = 1520871970

    # test not day, week, month, quater, year
    t_day = np.array([now, now + cal.HOUR, now + cal.HOUR*4, now + cal.WEEK])
    f_day = basic_time_formatter(time_input=t_day)
    assert f_day == ['2018.03.12 16:26', '2018.03.12 17:26', '2018.03.12 20:26', '2018.03.19 16:26']


def test_table_formatters_utc_offset():
    utc_now = Calendar(0).time(2019, 1, 1, 0, 0)
    t_day_utc = np.array([utc_now])

    time_zone = 'Africa/Algiers'  # Constant +1h offset from UTC

    conditional_format_tz = conditional_time_formatter(time_input=t_day_utc, time_zone=time_zone)
    assert conditional_format_tz == ['2019.01.01 01:00']

    basic_time_format_tz = basic_time_formatter(time_input=t_day_utc, time_zone=time_zone)
    assert basic_time_format_tz == ['2019.01.01 01:00']

