import numpy as np
import pytest

from shyft.dashboard.base.ports import States
from shyft.dashboard.time_series.axes_handler import DsViewTimeAxisType
from shyft.dashboard.time_series.ds_view_handle import DsViewHandle
from shyft.dashboard.time_series.sources.source import DataSource
from shyft.dashboard.time_series.ts_viewer import TsViewer
from shyft.dashboard.time_series.view import TableView
from shyft.dashboard.time_series.view_container.table import Table
from shyft.time_series import UtcPeriod


def test_table(mock_bokeh_document, test_ts_line, mock_ts_vector, logger_and_list_handler):
    logger, log_list = logger_and_list_handler
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    table_title = "test"
    table = Table(viewer=viewer, tools=[], title=table_title)
    table_view = TableView(view_container=table, columns={0: 'tull'}, label='bakvendtland', unit='MW')

    assert len(table.views) == 0
    table.add_view(view=table_view)
    assert len(table.views) == 1

    table.clear_views(specific_views=[table_view])
    assert len(table.views) == 0

    table.add_view(view=table_view)
    table.add_view(view=table_view)
    assert len(table.views) == 1

    table.clear()
    assert len(table.views) == 0
    table.add_view(view=table_view)

    time_range = UtcPeriod(0, 24*3600)
    data_source = DataSource(ts_adapter=test_ts_line, unit='MW',
                             request_time_axis_type=DsViewTimeAxisType.padded_view_time_axis,
                             time_range=time_range)

    da_view_handle = DsViewHandle(data_source=data_source, views=[table_view])

    viewer.add_ds_view_handles(ds_view_handles=[da_view_handle])

    table.update_view_data(view_data={table_view: mock_ts_vector})
    assert len(table.table_columns) and len(table.data) == 2
    assert [key for key in table.bokeh_data_source.data.keys()][0] == "Time"
    assert [key for key in table.bokeh_data_source.data.values()][0][0] == "Unit"

    table_view.visible = False
    assert len(table.table_columns) and len(table.data) == 1
    table_view.visible = True
    assert table.view_range_callback() is None
    new_title = "new title"
    table.update_title(new_title)
    assert table.bokeh_title_div.text == f"{table_title}: {new_title}"

    table.visible = True
    table.visible = False
    assert len(table.table_columns) and len(table.data) == 1
    table.visible = True
    assert len(table.table_columns) and len(table.data) == 2


@pytest.mark.parametrize("view_range, expected",
                         [
                             [UtcPeriod(1, 5), []],
                             [UtcPeriod(5, 20), []],
                             [UtcPeriod(5, 25), [0, 1]],
                             [UtcPeriod(5, 55), [0, 3]],
                             [UtcPeriod(22, 25), [0, 1]],
                             [UtcPeriod(22, 35), [0, 2]],
                             [UtcPeriod(50, 55), []],
                             [UtcPeriod(55, 60), []],
                         ]
                         )
def test_estimate_view_range_indices(view_range: UtcPeriod, expected,
                                     mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    viewer.view_time_axis.set_view_range(view_range, padding=False)

    table = Table(viewer=viewer, tools=[], title="test")
    table.aligned_time = np.array([20, 30, 40, 50], dtype=float)
    table.estimate_view_range_indices()

    assert table.view_range_indices == expected

def test_estimate_view_range_indices_no_effect(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    viewer.view_time_axis.set_view_range(UtcPeriod(5, 20), padding=False)
    table = Table(viewer=viewer, tools=[], title="test")

    # no effect (unsure, why no used reset to empy list []
    table.view_range_indices = [22, 25]
    table.aligned_time = None
    table.estimate_view_range_indices()
    assert table.view_range_indices == [22, 25]

    # empty time times for the vectors -> some kind of no input?
    table.aligned_time = np.array([], dtype=float)
    table.estimate_view_range_indices()
    assert table.view_range_indices == []


def test_table_timezone(mock_bokeh_document):
    time_zone = 'Europe/Oslo'
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer", time_zone=time_zone)
    table = Table(viewer=viewer)
    assert table.parent.time_zone == time_zone


def test_table_states(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    table_title = "test"
    table = Table(viewer=viewer, tools=[], title=table_title)
    assert table.visible
    assert table._visible_state
    assert table._receive_state(States.DEACTIVE) is None
    assert not table.visible
    assert table._visible_state
    assert table._receive_state(States.ACTIVE) is None
    assert table.visible


# TODO: editable table testing


