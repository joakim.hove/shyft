from typing import List

import shyft.time_series as sa
import pytest

import numpy as np
from shyft.time_series import Calendar, TimeAxis, utctime_now, time_axis_extract_time_points_as_utctime_tz, time_series_to_bokeh_plot_data, time, TimeSeries, POINT_AVERAGE_VALUE

from shyft.dashboard.time_series.data_utility import (data_to_patch_values,
                                                      merge_convert_ts_vectors_to_numpy,
                                                      find_nearest,
                                                      DataUtilError, calculate_dead_band_indices, convert_ts_to_numpy)

from shyft.dashboard.time_series.state import State, Quantity


def test_calculate_dead_band_indices():
    input = np.array([1, 1, 1, 2, 3, 3, 3, 3, 4, 3, 3, 3, 3, 3])
    multi_indices, scatter_indices = calculate_dead_band_indices(ts_input=input)
    expected_multi_indices = np.array([[0, 2],
                                       [4, 7],
                                       [9, 13]])
    expected_input = [3, 8]
    np.testing.assert_equal(multi_indices, expected_multi_indices)
    assert expected_input == scatter_indices


def test_data_to_patch_values():
    # without slice
    d1 = np.array([1, 2, 3, 4])
    d2 = np.array([-10, -11, -12, -13])
    resulting_patches = data_to_patch_values(d1, d2)
    assert len(resulting_patches) == 1
    np.testing.assert_allclose(resulting_patches[0], np.array([1, 2, 3, 4, -13, -12, -11, -10]))

    # two slices
    d1 = np.array([1, 2, 3, 4, np.nan, 10, 11, 12, 13])
    d2 = np.array([-10, -11, -12, -13, np.nan, 1, 2, 3, 4])
    non_nan_slices = np.ma.clump_unmasked(np.ma.masked_invalid(d1))
    resulting_patches = data_to_patch_values(d1, d2, non_nan_slices=non_nan_slices)
    assert len(resulting_patches) == 2
    np.testing.assert_allclose(resulting_patches[0], np.array([1, 2, 3, 4, -13, -12, -11, -10]))
    np.testing.assert_allclose(resulting_patches[1], np.array([10, 11, 12, 13, 4, 3, 2, 1]))


def test_step_post_interpolator():
    ts = TimeSeries(TimeAxis(time(1), time(1), 4), [10, 20, 30, 40], POINT_AVERAGE_VALUE)
    tv = time_series_to_bokeh_plot_data(ts=ts, calendar=Calendar(), time_scale=1.0, force_linear=False, crop_trailing_nans=True)
    np.testing.assert_allclose(tv[0].to_numpy(), [1, 2, 2, 3, 3, 4, 4, 5])
    np.testing.assert_allclose(tv[1].to_numpy(), [10, 10, 20, 20, 30, 30, 40, 40])


def test_convert_ts_to_numpy():
    # Empty time series
    tv = time_series_to_bokeh_plot_data(ts=TimeSeries(), calendar=Calendar(), time_scale=1.0, force_linear=False, crop_trailing_nans=True)
    assert len(tv[0]) == len(tv[1]) == 0

    # Nan time series
    nan = float('nan')
    ts = TimeSeries(TimeAxis(time(1), time(1), 4), [nan, nan, nan, nan], POINT_AVERAGE_VALUE)
    tv = time_series_to_bokeh_plot_data(ts=ts, calendar=Calendar(), time_scale=1.0, force_linear=False, crop_trailing_nans=True)
    assert len(tv[0]) == len(tv[1]) == 0

    # normal time series with crop nans, and PointAverageValue
    ts = TimeSeries(TimeAxis(time(1), time(1), 4), [10, 20, nan, nan], POINT_AVERAGE_VALUE)
    tv = time_series_to_bokeh_plot_data(ts=ts, calendar=Calendar(), time_scale=1.0, force_linear=False, crop_trailing_nans=True)

    np.testing.assert_allclose(tv[0].to_numpy(), [1, 2, 2, 3])
    np.testing.assert_allclose(tv[1].to_numpy(), [10, 10, 20, 20])

    # just to cover bwcompat function
    t, d = convert_ts_to_numpy(ts=ts)
    assert len(t) == len(d)

@pytest.mark.parametrize("time_scale", [1, 1.55])
def test_merge_convert_ts_vectors_to_numpy(time_scale):
    t1 = np.array([1, 2, 3, 4, 5, 6, 7, 8])
    d1 = np.array([10, 20, 30, 40, np.nan, np.nan, np.nan])
    t2 = np.array([4, 5, 6, 7, 8, 9, 10, 11])
    d2 = np.array([np.nan, np.nan, np.nan, 11, 21, 31, 41])
    t3 = np.array([4, 5, 6, 7, 8, 9, 10, 11]) - 1
    d3 = np.array([np.nan, np.nan, np.nan, 111, 121, 131, 141])

    time_axis1 = sa.TimeAxis(sa.UtcTimeVector.from_numpy(t1))
    time_series1 = sa.TimeSeries(time_axis1, sa.DoubleVector.from_numpy(d1),
                                 sa.point_interpretation_policy.POINT_AVERAGE_VALUE)
    time_axis2 = sa.TimeAxis(sa.UtcTimeVector.from_numpy(t2))
    time_series2 = sa.TimeSeries(time_axis2, sa.DoubleVector.from_numpy(d2),
                                 sa.point_interpretation_policy.POINT_AVERAGE_VALUE)
    time_axis3 = sa.TimeAxis(sa.UtcTimeVector.from_numpy(t3))
    time_series3 = sa.TimeSeries(time_axis3, sa.DoubleVector.from_numpy(d3),
                                 sa.point_interpretation_policy.POINT_AVERAGE_VALUE)

    ts_vector1 = sa.TsVector([time_series1])
    ts_vector2 = sa.TsVector([time_series2, time_series3])
    ts_vector1 = State.unit_registry.Quantity(ts_vector1, 'MW')
    ts_vector2 = State.unit_registry.Quantity(ts_vector2, 'MW')
    tsv_list = [ts_vector1, ts_vector2]

    t_new, d_new = merge_convert_ts_vectors_to_numpy(ts_vectors=tsv_list, time_scale=time_scale)
    assert len(t_new) == 11
    assert np.allclose(t_new, np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])*time_scale)
    assert len(d_new) == 2
    assert len(d_new[0][0]) == 10
    assert np.allclose(d_new[0][0], np.array([10, 20, 30, 40, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan]), equal_nan=True)
    assert len(d_new[1]) == 2
    assert np.allclose(d_new[1][0], np.array([np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, 11, 21, 31, 41]), equal_nan=True)
    assert np.allclose(d_new[1][1], np.array([np.nan, np.nan, np.nan, np.nan, np.nan, 111, 121, 131, 141, np.nan]), equal_nan=True)

    # work on empty list
    tsv_list = []
    res_t, res_d = merge_convert_ts_vectors_to_numpy(ts_vectors=tsv_list, time_scale=time_scale)
    np.testing.assert_equal(res_t, np.array([]))
    assert res_d == []

    # work on empty-tsv
    ts_vector1 = sa.TsVector()
    ts_vector1 = State.unit_registry.Quantity(ts_vector1, 'MW')
    tsv_list = [ts_vector1, ts_vector1]
    res_t, res_d = merge_convert_ts_vectors_to_numpy(ts_vectors=tsv_list, time_scale=time_scale)
    np.testing.assert_equal(res_t, np.array([]))
    assert res_d == []

    # work on empty-ts
    with pytest.raises(RuntimeError):
        ts_vector1 = sa.TsVector([TimeSeries()])
        ts_vector1 = State.unit_registry.Quantity(ts_vector1, 'MW')
        tsv_list = [ts_vector1, ts_vector1]
        merge_convert_ts_vectors_to_numpy(ts_vectors=tsv_list, time_scale=time_scale)


def test_merge_convert_ts_vectors_to_numpy_performance():
    n= 10000 # number of time-points
    nv = 10 # number of ts
    ta=sa.TimeAxis(time('2000-01-01T00:00:00Z'),time(3600),n)
    tsv=sa.TsVector([sa.TimeSeries(ta,fill_value=float(i),point_fx=sa.POINT_AVERAGE_VALUE) for i in range(nv)])
    t=sa.utctime_now()
    m=merge_convert_ts_vectors_to_numpy(ts_vectors=[State.unit_registry.Quantity(tsv,'MW')])
    t_used= sa.utctime_now()-t
    perf= n*nv/float(t_used)
    print(f"\nmerge_convert_ts_vectors_to_numpy performance {perf/1e6:.1f} million points/sec")
    assert perf > 10000.0


def test_find_nearest():
    a = np.asarray([0, 1, 2, 3])

    # Test inside
    for vtp in [float, sa.time]:
        assert find_nearest(array=a, input_value=vtp(0.0), smaller_equal=False) == 0
        assert find_nearest(array=a, input_value=vtp(0.0), smaller_equal=True) == 0
        assert find_nearest(array=a, input_value=vtp(1.0), smaller_equal=False) == 1
        assert find_nearest(array=a, input_value=vtp(1.0), smaller_equal=True) == 1
        assert find_nearest(array=a, input_value=vtp(1.2), smaller_equal=True) == 1
        assert find_nearest(array=a, input_value=vtp(1.2), smaller_equal=False) == 2
        assert find_nearest(array=a, input_value=vtp(0.5), smaller_equal=True) == 0
        assert find_nearest(array=a, input_value=vtp(0.5), smaller_equal=False) == 1
        assert find_nearest(array=a, input_value=vtp(2.5), smaller_equal=True) == 2
        assert find_nearest(array=a, input_value=vtp(2.5), smaller_equal=False) == 3
        assert find_nearest(array=a, input_value=vtp(3.0), smaller_equal=False) == 3
        assert find_nearest(array=a, input_value=vtp(3.0), smaller_equal=True) == 3

        # Test outside left
        assert find_nearest(array=a, input_value=vtp(-0.5), smaller_equal=False) == 0
        assert find_nearest(array=a, input_value=vtp(-0.5), smaller_equal=True) == 0

        # test outside right
        assert find_nearest(array=a, input_value=vtp(3.5), smaller_equal=True) == 3
        assert find_nearest(array=a, input_value=vtp(3.5), smaller_equal=False) == 3

        b = np.asarray([0])
        assert find_nearest(array=b, input_value=vtp(-0.5), smaller_equal=False) == 0
        assert find_nearest(array=b, input_value=vtp(-0.5), smaller_equal=True) == 0
        assert find_nearest(array=b, input_value=vtp(0.0), smaller_equal=False) == 0
        assert find_nearest(array=b, input_value=vtp(0.0), smaller_equal=True) == 0
        assert find_nearest(array=b, input_value=vtp(0.5), smaller_equal=False) == 0
        assert find_nearest(array=b, input_value=vtp(0.5), smaller_equal=True) == 0
        with pytest.raises(DataUtilError):
            find_nearest(array=np.asarray([]), input_value=1, smaller_equal=True)


def test_time_axis_extract_time_points_as_utctime_tz_performance():
    tz = 'Europe/Oslo'
    cal = Calendar(tz)
    ta = TimeAxis(cal.time(2000, 1, 1), 3600, 365*24*10)
    t0 = utctime_now()
    x = time_axis_extract_time_points_as_utctime_tz(ta, cal).to_numpy()
    t_used = utctime_now() - t0
    perf = len(ta)/t_used/1e6
    print(f'time_axis_extract_time_points_as_utctime_tz performance {perf} million points/sec ')
    assert perf > 0.1


def test_convert_ts_to_numpy_performance():
    ta = TimeAxis(time('2020-01-01T00:00:00Z'), time(3600), 10*365*24)
    ts = TimeSeries(ta, fill_value=1.0, point_fx=POINT_AVERAGE_VALUE)
    tz = 'Europe/Oslo'
    cal = Calendar(tz)
    t0 = utctime_now()
    # t,v = convert_ts_to_numpy(ts=ts,crop_nan=True,interpret_point_interpretation=True)
    # add_tz_utc_offset(t, time_zone=tz)
    tv = time_series_to_bokeh_plot_data(ts=ts, calendar=cal, time_scale=1000.0, force_linear=False, crop_trailing_nans=True)
    tv[0].to_numpy()
    tv[1].to_numpy()
    t1 = utctime_now()
    t_used = t1 - t0
    perf = len(ta)/t_used/1e6
    print(f'convert_ts_to_numpy performance {perf} million points/sec ')
    assert perf > 0.1
