import pytest

import bokeh.plotting as bokeh_plotting

from shyft.dashboard.maps.map_axes import MapAxesRanges
from shyft.dashboard.maps.map_viewer import MapViewer

from shyft.dashboard.time_series.bindable import BindableError
#from shyft.dashboard.test.maps.test_map_fixtures import basemap_factory


def test_MapAxesRanges(base_map):
    test_file = 'test_map_range.html'
    bokeh_plotting.output_file(test_file)
    ## ranges choosen that aspect ratio does not change anything

    map_ranges = MapAxesRanges(width=300, height=300)
    assert map_ranges.axes_bounds == (0, 0, 1, 1)

    map_ranges2 = MapAxesRanges(width=300, height=300)
    map_ranges2.set_axes_bounds(1, 2, 3, 4)
    assert map_ranges2.axes_bounds == (1, 2, 3, 4)

    map_ranges.set_axes_bounds_from_bounds_list([(0, 4, 3, 5), (1, 2, 2, 4)])
    assert map_ranges.axes_bounds == (0, 2, 3, 5)

    map_ranges.padding = 1
    assert map_ranges.axes_bounds == (-1, 1, 4, 6)

    # test bind
    map_viewer = MapViewer(width=300, height=300, base_map=base_map)
    map_ranges.bind(parent=map_viewer)

    assert map_viewer.bokeh_figure.x_range.start == -1
    assert map_viewer.bokeh_figure.x_range.end == 4

    assert map_viewer.bokeh_figure.y_range.start == 1
    assert map_viewer.bokeh_figure.y_range.end == 6

    # bindable error
    map_ranges3 = MapAxesRanges(width=300, height=150)
    with pytest.raises(BindableError):
        class Foo:
            def __init__(self):
                self.fig = 'blub'
        map_ranges3.bind(parent=Foo())

    # test aspect ratio calculations
    map_ranges = MapAxesRanges(width=300, height=150)
    map_ranges.set_axes_bounds(0, 0, 100, 100)
    # the smallest aspect range should be increased in this case the x axis by adding half the missing left and right
    assert map_ranges.axes_bounds == (-50, 0, 150, 100)

    map_ranges.set_axes_bounds(0, 0, 100, 25)
    # the smallest aspect range should be increased in this case the x axis by adding half the missing left and right
    assert map_ranges.axes_bounds == (0, -25, 100, 50)

    # map_viewer = MapViewer(width=300, height=300, base_map=basemap_factory())
    # map_ranges.bind(parent=map_viewer)
    # bokeh_plotting.show(map_viewer.layout)
