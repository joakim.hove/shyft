import pytest
from typing import List

import bokeh
import bokeh.models
from packaging import version

from shyft.dashboard.base.selector_views import (AutocompleteInput,
                                                 MultiSelect,
                                                 TwoSelect,
                                                 Select,
                                                 CheckboxButtonGroup,
                                                 CheckboxGroup,
                                                 RadioGroup,
                                                 RadioButtonGroup)
from shyft.dashboard.base.ports import Receiver, connect_ports, States


def get_bokeh_view_selection(view):
    if hasattr(view._bokeh_view, "value"):
        bokeh_value = view._bokeh_view.value
        if isinstance(bokeh_value, str):
            return bokeh_value
        elif isinstance(bokeh_value, list):
            if bokeh_value:
                return bokeh_value[0]
            return None
    if hasattr(view._bokeh_view, "active"):
        bokeh_value = view._bokeh_view.active
        if isinstance(bokeh_value, int):
            return view._bokeh_view.active
        elif isinstance(bokeh_value, list):
            return view._bokeh_view.active[0]


def test_selector_views_with_BokehViewBase(logger_and_list_handler):
    logger, log_list = logger_and_list_handler

    default = "default"
    options = [default, "A", "B", "C", "D", "E", "F", "G"]
    selected_option = "B"
    selected_values = ["B"]
    width = 123
    title = "test single select"

    # all with attribute value
    view_model = Select
    bokeh_model = bokeh.models.Select

    view = view_model(title=title, width=width, logger=logger)

    assert view.__repr__() == f"{bokeh_model.__name__} '{title}'"

    assert view.width == width
    view.width = 321
    assert view.width == 321

    # test setting of options
    view.receive_options(options)
    assert view._default == default

    # test setting of a value
    view.receive_selection(selected_values)
    bokeh_value = view._bokeh_view.value
    assert bokeh_value == selected_option

    view.receive_selection([])
    bokeh_value = view._bokeh_view.value
    assert bokeh_value == default

    # trigger selection of value and check if callback works
    call_count = [0]

    def assert_port(selected: List[str]):
        assert len(selected) > 0
        assert selected[0] == selected_option
        call_count[0] += 1

    receive_view_selection = Receiver(parent='', name='assert selection', signal_type=List[str], func=assert_port)
    connect_ports(view.send_selection, receive_view_selection)

    view._bokeh_view.value = selected_option

    assert call_count[0] == 1

    # layout components
    lc = view.layout_components
    assert lc["widgets"] and isinstance(lc["widgets"][0], bokeh_model)

    # test states
    view.state_ports.receive_state(States.LOADING)
    assert view.disabled
    assert view._bokeh_view.title == ': '.join([title, 'loading ...'])

    view.state_ports.receive_state(States.READY)
    assert not view.disabled
    assert view._bokeh_view.title == title

    view.state_ports.receive_state(States.PROCESSING)
    assert view.disabled
    assert view._bokeh_view.title == ': '.join([title, 'processing ...'])

    view.state_ports.receive_state(States.DEACTIVE)
    assert view.disabled
    assert view._bokeh_view.title == title

    view.state_ports.receive_state(States.INVALID)
    assert not view.disabled
    assert view._bokeh_view.title == ': '.join([title, 'invalid'])

    view.state_ports.receive_state(States.ACTIVE)
    assert not view.disabled
    assert view._bokeh_view.title == title

    state = "foobar party"
    view.state_ports.receive_state(state)
    assert log_list[-1] == f"ERROR: {view.__class__.__name__} - unknown state '{state}' received"


def test_composable_selector_views_multi_select(logger_and_list_handler):
    logger, log_list = logger_and_list_handler

    default = "default"
    options = [default, "A", "B", "C", "D", "E", "F", "G"]
    selected_option = "B"
    selected_values = ["B"]
    width = 123
    title = "test single select"

    # all with attribute value
    view_model = MultiSelect
    bokeh_model = bokeh.models.MultiSelect

    view = view_model(title=title, width=width, logger=logger)

    assert view.__repr__() == f"{bokeh_model.__name__} '{title}'"

    assert view.width == width
    view.width = 321
    assert view.width == 321

    # test setting of options
    view.receive_options(options)
    assert view._default == default

    # test setting of a value
    view.receive_selection(selected_values)
    bokeh_value = view._bokeh_view.value
    assert bokeh_value == selected_values

    view.receive_selection([])
    bokeh_value = view._bokeh_view.value
    assert bokeh_value == [default]

    # trigger selection of value and check if callback works
    call_count = [0]

    def assert_port(selected: List[str]):
        assert len(selected) > 0
        assert selected[0] == selected_option
        call_count[0] += 1

    receive_view_selection = Receiver(parent='', name='assert selection', signal_type=List[str], func=assert_port)
    connect_ports(view.send_selection, receive_view_selection)

    view._bokeh_view.value = selected_values

    assert call_count[0] == 1

    # layout components
    lc = view.layout_components
    assert lc["widgets"] and isinstance(lc["widgets"][0], bokeh_model)

    # test states
    view.state_ports.receive_state(States.LOADING)
    assert view.disabled
    assert view._bokeh_view.title == ': '.join([title, 'loading ...'])

    view.state_ports.receive_state(States.READY)
    assert not view.disabled
    assert view._bokeh_view.title == title

    view.state_ports.receive_state(States.PROCESSING)
    assert view.disabled
    assert view._bokeh_view.title == ': '.join([title, 'processing ...'])

    view.state_ports.receive_state(States.DEACTIVE)
    assert view.disabled
    assert view._bokeh_view.title == title

    view.state_ports.receive_state(States.INVALID)
    assert not view.disabled
    assert view._bokeh_view.title == ': '.join([title, 'invalid'])

    view.state_ports.receive_state(States.ACTIVE)
    assert not view.disabled
    assert view._bokeh_view.title == title

    state = "foobar party"
    view.state_ports.receive_state(state)
    assert log_list[-1] == f"ERROR: {view.__class__.__name__} - unknown state '{state}' received"


def test_composable_selector_view_autocomplete_input(logger_and_list_handler):
    logger, log_list = logger_and_list_handler

    default = "default"
    options = [default, "A", "B", "C", "D", "E", "F", "G"]
    selected_option = "B"
    selected_values = ["B"]
    width = 123
    title = "test single select"

    view = AutocompleteInput(title=title, width=width, logger=logger)
    bokeh_model = bokeh.models.AutocompleteInput

    assert view.__repr__() == f"{bokeh_model.__name__} '{title}'"

    assert view.width == width
    view.width = 321
    assert view.width == 321

    # test setting of options
    view.receive_options(options)
    assert view._default == default

    # test setting of a value
    view.receive_selection(selected_values)
    bokeh_value = view._bokeh_view.value
    assert bokeh_value == selected_option

    view.receive_selection([])
    bokeh_value = view._bokeh_view.value
    assert bokeh_value == default

    # trigger selection of value and check if callback works
    call_count = [0]

    def assert_port(selected: List[str]):
        assert len(selected) > 0
        assert selected[0] == selected_option
        call_count[0] += 1

    receive_view_selection = Receiver(parent='', name='assert selection', signal_type=List[str], func=assert_port)
    connect_ports(view.send_selection, receive_view_selection)

    view._bokeh_view.value = selected_option
    assert call_count[0] == 1
    view._bokeh_view.value = "nothing known"
    assert call_count[0] == 1

    # layout components
    lc = view.layout_components
    assert lc["widgets"] and isinstance(lc["widgets"][0], bokeh_model)

    # test states
    view.state_ports.receive_state(States.LOADING)
    assert view.disabled
    assert view._bokeh_view.title == ': '.join([title, 'loading ...'])

    view.state_ports.receive_state(States.READY)
    assert not view.disabled
    assert view._bokeh_view.title == title

    view.state_ports.receive_state(States.PROCESSING)
    assert view.disabled
    assert view._bokeh_view.title == ': '.join([title, 'processing ...'])

    view.state_ports.receive_state(States.DEACTIVE)
    assert view.disabled
    assert view._bokeh_view.title == title

    view.state_ports.receive_state(States.INVALID)
    assert not view.disabled
    assert view._bokeh_view.title == ': '.join([title, 'invalid'])

    view.state_ports.receive_state(States.ACTIVE)
    assert not view.disabled
    assert view._bokeh_view.title == title

    state = "foobar party"
    view.state_ports.receive_state(state)
    assert log_list[-1] == f"ERROR: {view.__class__.__name__} - unknown state '{state}' received"


def test_composable_selector_two_select(logger_and_list_handler):
    logger, log_list = logger_and_list_handler

    default = "default"
    options = [default, "A", "B", "C", "D", "E", "F", "G"]
    selected_values = ["B", "A"]
    width = 123
    title = "test single select"

    # all with attribute value
    bokeh_model = bokeh.models.Select
    view = TwoSelect(title=title, width=width, logger=logger)

    assert view.__repr__() == f"{view.__class__.__name__} '{title}'"

    assert view.width == width
    view.width = 321
    assert view.width == 321

    # test setting of options
    view.receive_options(options)
    assert view._default == default

    # test setting of a value
    view.receive_selection(selected_values)
    assert view._select1.value == selected_values[0]
    assert view._select2.value == selected_values[1]

    view.receive_selection([selected_values[0]])
    assert view._select1.value == selected_values[0]
    assert view._select2.value == default

    view.receive_selection([])
    assert view._select1.value == default
    assert view._select2.value == default

    view.receive_selection([selected_values[0], selected_values[0]])
    assert view._select1.value == selected_values[0]
    assert view._select2.value == default

    view.receive_selection([])
    assert view._select1.value == default
    assert view._select2.value == default

    # trigger selection of value and check if callback works
    call_count = [0]
    returned_selection = [None]

    def assert_port(selected: List[str]):
        assert len(selected) > 0
        assert len(set(selected)) == len(selected)
        returned_selection[0] = selected
        call_count[0] += 1

    receive_view_selection = Receiver(parent='', name='assert selection', signal_type=List[str], func=assert_port)
    connect_ports(view.send_selection, receive_view_selection)

    view._select1.value = selected_values[0]
    assert call_count[0] == 1
    assert returned_selection[0]
    assert returned_selection[0][0] == selected_values[0]

    view._select2.value = selected_values[1]
    assert call_count[0] == 2
    assert returned_selection[0]
    assert returned_selection[0][0] == selected_values[0]
    assert returned_selection[0][1] == selected_values[1]

    # layout components
    lc = view.layout_components
    assert lc["widgets"] and isinstance(lc["widgets"][0], bokeh_model)

    # test states
    view.state_ports.receive_state(States.LOADING)
    assert view.disabled
    assert view._two_select_title.text == ': '.join([title, 'loading ...'])

    view.state_ports.receive_state(States.READY)
    assert not view.disabled
    assert view._two_select_title.text == title

    view.state_ports.receive_state(States.PROCESSING)
    assert view.disabled
    assert view._two_select_title.text == ': '.join([title, 'processing ...'])

    view.state_ports.receive_state(States.DEACTIVE)
    assert view.disabled
    assert view._two_select_title.text == title

    view.state_ports.receive_state(States.INVALID)
    assert not view.disabled
    assert view._two_select_title.text == ': '.join([title, 'invalid'])

    view.state_ports.receive_state(States.ACTIVE)
    assert not view.disabled
    assert view._two_select_title.text == title

    state = "foobar party"
    view.state_ports.receive_state(state)
    assert log_list[-1] == f"ERROR: {view.__class__.__name__} - unknown state '{state}' received"


def test_composable_selector_view_checkboxbuttongroup(logger_and_list_handler):
    logger, log_list = logger_and_list_handler

    default = "default"
    options = [default, "A", "B", "C", "D", "E", "F", "G"]
    selection_index = [1]
    selected_option = "B"
    selected_values = ["B"]
    width = 123
    title = "test single select"

    view = CheckboxButtonGroup(title=title, width=width, logger=logger)
    bokeh_model = bokeh.models.CheckboxButtonGroup

    assert view.__repr__() == f"{bokeh_model.__name__} '{title}'"

    assert view.width == width
    view.width = 321
    assert view.width == 321

    # test setting of options
    view.receive_options(options)
    assert view._default == default

    # test setting of a value
    view.receive_selection(selected_values)
    bokeh_value = view._bokeh_view.active
    assert bokeh_value == selection_index

    view.receive_selection([])
    bokeh_value = view._bokeh_view.active
    assert not bokeh_value  # [] in 2.3, and None before

    # trigger selection of value and check if callback works
    call_count = [0]

    def assert_port(selected: List[str]):
        assert len(selected) > 0
        assert selected[0] == selected_option
        call_count[0] += 1

    receive_view_selection = Receiver(parent='', name='assert selection', signal_type=List[str], func=assert_port)
    connect_ports(view.send_selection, receive_view_selection)

    # Does not work with Bokeh version > 2.3
    # view._bokeh_view.active = selection_index
    # This should for older versions of Bokeh, as well
    view._on_change_select('active', 0, selection_index)
    assert call_count[0] == 1

    # layout components
    lc = view.layout_components
    assert lc["widgets"] and isinstance(lc["widgets"][0], bokeh.models.Div) and isinstance(lc["widgets"][1], bokeh_model)

    # test states
    view.state_ports.receive_state(States.LOADING)
    assert view.disabled
    assert view._title_div.text == ': '.join([title, 'loading ...'])

    view.state_ports.receive_state(States.READY)
    assert not view.disabled
    assert view._title_div.text == title

    view.state_ports.receive_state(States.PROCESSING)
    assert view.disabled
    assert view._title_div.text == ': '.join([title, 'processing ...'])

    view.state_ports.receive_state(States.DEACTIVE)
    assert view.disabled
    assert view._title_div.text == title

    view.state_ports.receive_state(States.INVALID)
    assert not view.disabled
    assert view._title_div.text == ': '.join([title, 'invalid'])

    view.state_ports.receive_state(States.ACTIVE)
    assert not view.disabled
    assert view._title_div.text == title

    state = "foobar party"
    view.state_ports.receive_state(state)
    assert log_list[-1] == f"ERROR: {view.__class__.__name__} - unknown state '{state}' received"


def test_composable_selector_view_checkboxgroup(logger_and_list_handler):
    logger, log_list = logger_and_list_handler

    default = "default"
    options = [default, "A", "B", "C", "D", "E", "F", "G"]
    selection_index = [1]
    selected_option = "B"
    selected_values = ["B"]
    width = 123
    title = "test single select"

    view = CheckboxGroup(title=title, width=width, logger=logger)
    bokeh_model = bokeh.models.CheckboxGroup

    assert view.__repr__() == f"{bokeh_model.__name__} '{title}'"

    assert view.width == width
    view.width = 321
    assert view.width == 321

    # test setting of options
    view.receive_options(options)
    assert view._default == default

    # test setting of a value
    view.receive_selection(selected_values)
    bokeh_value = view._bokeh_view.active
    assert bokeh_value == selection_index

    view.receive_selection([])
    bokeh_value = view._bokeh_view.active
    assert not bokeh_value  # [] in 2.3, and None before

    # trigger selection of value and check if callback works
    call_count = [0]

    def assert_port(selected: List[str]):
        assert len(selected) > 0
        assert selected[0] == selected_option
        call_count[0] += 1

    receive_view_selection = Receiver(parent='', name='assert selection', signal_type=List[str], func=assert_port)
    connect_ports(view.send_selection, receive_view_selection)

    # view._bokeh_view.active = selection_index
    view._on_change_select('active', 0, selection_index)
    assert call_count[0] == 1

    # layout components
    lc = view.layout_components
    assert lc["widgets"] and isinstance(lc["widgets"][0], bokeh.models.Div) and isinstance(lc["widgets"][1], bokeh_model)

    # test states
    view.state_ports.receive_state(States.LOADING)
    assert view.disabled
    assert view._title_div.text == ': '.join([title, 'loading ...'])

    view.state_ports.receive_state(States.READY)
    assert not view.disabled
    assert view._title_div.text == title

    view.state_ports.receive_state(States.PROCESSING)
    assert view.disabled
    assert view._title_div.text == ': '.join([title, 'processing ...'])

    view.state_ports.receive_state(States.DEACTIVE)
    assert view.disabled
    assert view._title_div.text == title

    view.state_ports.receive_state(States.INVALID)
    assert not view.disabled
    assert view._title_div.text == ': '.join([title, 'invalid'])

    view.state_ports.receive_state(States.ACTIVE)
    assert not view.disabled
    assert view._title_div.text == title

    state = "foobar party"
    view.state_ports.receive_state(state)
    assert log_list[-1] == f"ERROR: {view.__class__.__name__} - unknown state '{state}' received"


def test_composable_selector_view_radio_group(logger_and_list_handler):
    logger, log_list = logger_and_list_handler

    default = "default"
    options = [default, "A", "B", "C", "D", "E", "F", "G"]
    selection_index = 1
    selected_option = "B"
    selected_values = ["B"]
    width = 123
    title = "test single select"

    view = RadioGroup(title=title, width=width, logger=logger)
    bokeh_model = bokeh.models.RadioGroup

    assert view.__repr__() == f"{bokeh_model.__name__} '{title}'"

    assert view.width == width
    view.width = 321
    assert view.width == 321

    # test setting of options
    view.receive_options(options)
    assert view._default == default

    # test setting of a value
    view.receive_selection(selected_values)
    bokeh_value = view._bokeh_view.active
    assert bokeh_value == selection_index

    view.receive_selection([])
    bokeh_value = view._bokeh_view.active
    assert bokeh_value is None

    # trigger selection of value and check if callback works
    call_count = [0]

    def assert_port(selected: List[str]):
        assert len(selected) > 0
        assert selected[0] == selected_option
        call_count[0] += 1

    receive_view_selection = Receiver(parent='', name='assert selection', signal_type=List[str], func=assert_port)
    connect_ports(view.send_selection, receive_view_selection)

    view._bokeh_view.active = selection_index
    assert call_count[0] == 1

    # layout components
    lc = view.layout_components
    assert lc["widgets"] and isinstance(lc["widgets"][0], bokeh.models.Div) and isinstance(lc["widgets"][1], bokeh_model)

    # test states
    view.state_ports.receive_state(States.LOADING)
    assert view.disabled
    assert view._title_div.text == ': '.join([title, 'loading ...'])

    view.state_ports.receive_state(States.READY)
    assert not view.disabled
    assert view._title_div.text == title

    view.state_ports.receive_state(States.PROCESSING)
    assert view.disabled
    assert view._title_div.text == ': '.join([title, 'processing ...'])

    view.state_ports.receive_state(States.DEACTIVE)
    assert view.disabled
    assert view._title_div.text == title

    view.state_ports.receive_state(States.INVALID)
    assert not view.disabled
    assert view._title_div.text == ': '.join([title, 'invalid'])

    view.state_ports.receive_state(States.ACTIVE)
    assert not view.disabled
    assert view._title_div.text == title

    state = "foobar party"
    view.state_ports.receive_state(state)
    assert log_list[-1] == f"ERROR: {view.__class__.__name__} - unknown state '{state}' received"


def test_composable_selector_view_radio_button_group(logger_and_list_handler):
    logger, log_list = logger_and_list_handler

    default = "default"
    options = [default, "A", "B", "C", "D", "E", "F", "G"]
    selection_index = 1
    selected_option = "B"
    selected_values = ["B"]
    width = 123
    title = "test single select"

    view = RadioButtonGroup(title=title, width=width, logger=logger)
    bokeh_model = bokeh.models.RadioButtonGroup

    assert view.__repr__() == f"{bokeh_model.__name__} '{title}'"

    assert view.width == width
    view.width = 321
    assert view.width == 321

    # test setting of options
    view.receive_options(options)
    assert view._default == default

    # test setting of a value
    view.receive_selection(selected_values)
    bokeh_value = view._bokeh_view.active
    assert bokeh_value == selection_index

    view.receive_selection([])
    bokeh_value = view._bokeh_view.active
    assert bokeh_value is None

    # trigger selection of value and check if callback works
    call_count = [0]

    def assert_port(selected: List[str]):
        assert len(selected) > 0
        assert selected[0] == selected_option
        call_count[0] += 1

    receive_view_selection = Receiver(parent='', name='assert selection', signal_type=List[str], func=assert_port)
    connect_ports(view.send_selection, receive_view_selection)

    view._bokeh_view.active = selection_index
    assert call_count[0] == 1

    # layout components
    lc = view.layout_components
    assert lc["widgets"] and isinstance(lc["widgets"][0], bokeh.models.Div) and isinstance(lc["widgets"][1], bokeh_model)

    # test states
    view.state_ports.receive_state(States.LOADING)
    assert view.disabled
    assert view._title_div.text == ': '.join([title, 'loading ...'])

    view.state_ports.receive_state(States.READY)
    assert not view.disabled
    assert view._title_div.text == title

    view.state_ports.receive_state(States.PROCESSING)
    assert view.disabled
    assert view._title_div.text == ': '.join([title, 'processing ...'])

    view.state_ports.receive_state(States.DEACTIVE)
    assert view.disabled
    assert view._title_div.text == title

    view.state_ports.receive_state(States.INVALID)
    assert not view.disabled
    assert view._title_div.text == ': '.join([title, 'invalid'])

    view.state_ports.receive_state(States.ACTIVE)
    assert not view.disabled
    assert view._title_div.text == title

    state = "foobar party"
    view.state_ports.receive_state(state)
    assert log_list[-1] == f"ERROR: {view.__class__.__name__} - unknown state '{state}' received"
