from shyft.dashboard.base.ports import (Receiver, Sender, connect_ports)
from shyft.dashboard.base.gate_model import (SingleGate, GateState, AggregatedGate)


def test_single_gate_without_buffer():
    call_count = [0]

    send_10_port = Sender(parent='send_10', name='send 10 port', signal_type=int)

    def _receive_10(number: int) -> None:
        assert number == 10
        call_count[0] += 1

    receive_number = Receiver(parent=_receive_10, name='_recieve_10', func=_receive_10, signal_type=int)

    gate10 = SingleGate(sender=send_10_port, receiver=receive_number, connect_function=connect_ports,
                        buffer=False, initial_gate_state=GateState.Closed)

    # send a number
    send_10_port(10)
    # nothing should be received
    assert call_count[0] == 0
    # open the gate
    gate10.receive_gate_state(GateState.Open)
    # now number not be received since there is no use_buffer
    assert call_count[0] == 0
    # send a again
    send_10_port(10)
    # now number should be received
    assert call_count[0] == 1
    # close the gate again
    gate10.receive_gate_state(GateState.Closed)
    send_10_port(10)
    assert call_count[0] == 1


def test_single_gate_with_buffer():
    call_count = [0]
    send_10_port = Sender(parent='send_10', name='send 10 port', signal_type=int)

    def _receive_10(number: int) -> None:
        assert number == 10
        call_count[0] += 1

    receive_number = Receiver(parent=_receive_10, name='_recieve_10', func=_receive_10, signal_type=int)

    gate10 = SingleGate(sender=send_10_port, receiver=receive_number, connect_function=connect_ports, buffer=True,
                        initial_gate_state=GateState.Closed)

    # send a number
    send_10_port(10)
    # nothing should be received
    assert call_count[0] == 0
    # open the gate
    gate10.receive_gate_state(GateState.Open)
    # now number should be received
    assert call_count[0] == 1

    # open close gate, and buffered value should be send again
    gate10.receive_gate_state(GateState.Closed)
    gate10.receive_gate_state(GateState.Open)
    assert call_count[0] == 2


def test_single_gate_with_buffer_and_clear_buffer():
    call_count = [0]
    send_10_port = Sender(parent='send_10', name='send 10 port', signal_type=int)

    def _receive_10(number: int) -> None:
        assert number == 10
        call_count[0] += 1

    receive_number = Receiver(parent=_receive_10, name='_recieve_10', func=_receive_10, signal_type=int)

    gate10 = SingleGate(sender=send_10_port, receiver=receive_number, connect_function=connect_ports, buffer=True,
                        initial_gate_state=GateState.Closed, clear_buffer_after_send=True)

    # send a number
    send_10_port(10)
    # nothing should be received
    assert call_count[0] == 0
    # open the gate
    gate10.receive_gate_state(GateState.Open)
    # now number should be received since use_buffer is active
    assert call_count[0] == 1

    # open close gate, and buffered value should not be send again since use_buffer is cleared
    gate10.receive_gate_state(GateState.Closed)
    gate10.receive_gate_state(GateState.Open)
    assert call_count[0] == 1


def test_aggregated_gate():
    call_count_20 = [0]
    call_count_30 = [0]
    send_20_port = Sender(parent='send 20', name='send 20 port', signal_type=int)
    send_30_port = Sender(parent='send_30', name='send 30 port', signal_type=int)

    def _receive_20(number: int) -> None:
        assert number == 20
        call_count_20[0] += 1

    receive_20 = Receiver(parent=_receive_20, name='_recieve_20', func=_receive_20, signal_type=int)

    def _receive_30(number: int) -> None:
        assert number == 30
        call_count_30[0] += 1

    receive_30 = Receiver(parent=_receive_30, name='_recieve_30', func=_receive_30, signal_type=int)

    gate20 = SingleGate(sender=send_20_port, receiver=receive_20, buffer=False)
    gate30 = SingleGate(sender=send_30_port, receiver=receive_30, buffer=True)

    agg_gate = AggregatedGate(gates=[gate20, gate30], initial_gate_state=GateState.Closed)

    send_20_port(20)
    send_30_port(30)
    # tabs 20 should have received a value tabs 30 not
    assert call_count_20[0] == 0
    assert call_count_30[0] == 0
    # open agg gate
    agg_gate.open()
    # only 30 should have received a number since it has a use_buffer
    assert call_count_20[0] == 0
    assert call_count_30[0] == 1
    # send numbers
    send_20_port(20)
    send_30_port(30)
    assert call_count_20[0] == 1
    assert call_count_30[0] == 2
    # close gate, send again and check
    agg_gate.close()
    send_20_port(20)
    send_30_port(30)
    assert call_count_20[0] == 1
    assert call_count_30[0] == 2


def test_aggregated_gate_buffer_order():
    call_count_20 = [0]
    call_count_30 = [0]
    received_elements = []
    send_20_port = Sender(parent='send 20', name='send 20 port', signal_type=int)
    send_30_port = Sender(parent='send_30', name='send 30 port', signal_type=int)

    def _receive_20(number: int) -> None:
        assert number == 20
        received_elements.append(number)
        call_count_20[0] += 1

    receive_20 = Receiver(parent=_receive_20, name='_recieve_20', func=_receive_20, signal_type=int)

    def _receive_30(number: int) -> None:
        assert number == 30
        received_elements.append(number)
        call_count_30[0] += 1

    receive_30 = Receiver(parent=_receive_30, name='_recieve_30', func=_receive_30, signal_type=int)

    gate20 = SingleGate(sender=send_20_port, receiver=receive_20, buffer=True)
    gate30 = SingleGate(sender=send_30_port, receiver=receive_30, buffer=True)

    agg_gate = AggregatedGate(gates=[gate20, gate30], initial_gate_state=GateState.Closed)

    # send 30 then 20, opposite of definition
    send_30_port(30)
    send_20_port(20)
    # tabs 20 should have received a value tabs 30 not
    assert call_count_20[0] == 0
    assert call_count_30[0] == 0
    assert received_elements == []
    # open agg gate
    agg_gate.open()
    # only 30 should have received a number since it has a use_buffer
    assert call_count_20[0] == 1
    assert call_count_30[0] == 1
    assert received_elements == [30, 20]
