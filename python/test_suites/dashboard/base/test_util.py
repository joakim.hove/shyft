from shyft.dashboard.apps.water_route_app.simplified_water_route import SimplifiedWaterRouteGraph

from shyft.dashboard.apps.water_route_app.example_hps import get_example_hps
from shyft.util.layoutgraph import LayoutGraph, load_default_config_yaml
import tempfile
import yaml

from pathlib import Path


def test_water_route_graph_construct():
    wrg = SimplifiedWaterRouteGraph()
    assert wrg


def test_layout_graph_construct():
    some_obj = {'a': 1.0}
    lg = LayoutGraph(some_obj)
    assert lg


def test_load_default_config_yaml():
    x = {'a': True, 'b': [1, 2, 3]}
    with tempfile.TemporaryDirectory() as tmp_root:
        fn = Path(tmp_root)/"x.yml"
        with open(fn, mode='w') as f:
            f.write(yaml.safe_dump(x))

        cfg = load_default_config_yaml(filename=str(fn))
        assert cfg == x


def test_water_route_graph():
    hps = get_example_hps()
    water_route_graph_instance = SimplifiedWaterRouteGraph()
    water_route_graph_instance.generate_graph(hps)

    assert list(water_route_graph_instance.dh_tag_obj['reservoirs'].values()) == list(hps.reservoirs)
    assert list(water_route_graph_instance.dh_tag_obj['power_stations'].values()) == list(hps.power_stations)
    assert list(water_route_graph_instance.dh_tag_obj['main_water_routes'].values()) == \
           [ww for ww in hps.waterways if ww.upstream_role.name == 'main']
    assert list(water_route_graph_instance.dh_tag_obj['spill_routes'].values()) == \
           [ww for ww in hps.waterways if ww.upstream_role.name == 'flood']
    assert list(water_route_graph_instance.dh_tag_obj['bypass_routes'].values()) == \
           [ww for ww in hps.waterways if ww.upstream_role.name == 'bypass']

    assert len(water_route_graph_instance.main_water_route_beziers['tags']) == 8  # 8 main waterways
    for tag in water_route_graph_instance.main_water_route_beziers['tags']:
        assert tag in [ww.tag for ww in hps.waterways if ww.upstream_role.name == 'main']
    for tag in water_route_graph_instance.spill_routes_beziers['tags']:
        assert tag in [ww.tag for ww in hps.waterways if ww.upstream_role.name == 'flood']
    for tag in water_route_graph_instance.bypass_routes_beziers['tags']:
        assert tag in [ww.tag for ww in hps.waterways if ww.upstream_role.name == 'bypass']

    # 3 reservoirs + 3 power stations + 8 main water ways + 3 spill + 3 bypass + 5 oceans
    assert len(water_route_graph_instance.known_tags) == 25

