from bokeh.models import Toggle, LayoutDOM, PreText, ColumnDataSource, CustomJS
import pytest
from shyft.dashboard.base.app import LayoutComponents, Widget
#from .test_base_fixtures import logger_and_list_handler


def test_composable_app_update_value_factory(logger_and_list_handler):
    logger, log_list = logger_and_list_handler

    class MyApp(Widget):

        def __init__(self, logger):
            super().__init__(logger)
            # bokeh layout dom, widget
            self.toggle_button = Toggle(label='My toggle button')
            # add callback to toggel.active attribute
            self.toggle_button.on_click(self.on_click_toggle)
            # create update value factory which does not trigger the callback
            self.set_toggle_state = self.update_value_factory(self.toggle_button, 'active')
            # call count
            self.call_count = [0]

            # create update value factory which does not trigger the callback with attribute which does not exist
            with pytest.raises(AttributeError):
                self.set_toggle_state2 = self.update_value_factory(self.toggle_button, 'value')
            #
            # # test js property callbacks
            # javaScript = """
            # source.data['count'][0] = source.data['count'][0] + 1"""
            #
            # self.source = ColumnDataSource({'count': [0]})
            # self.pretext = PreText(text="")
            # pretext_callback = CustomJS(args=dict(source=self.source), code=javaScript)
            # self.pretext.js_on_change("text", pretext_callback)
            # self.set_text = self.update_value_factory(layout_dom_object=self.pretext,
            #                                           callback_attr='text')

        @property
        def layout(self) -> LayoutDOM:
            return self.toggle_button

        def layout_components(self) -> LayoutComponents:
            """
            Returns
            =======
            dict layout_components as:
                        {'widgets': [],
                         'figures': []}
            """
            return {'widgets': [self.toggle_button], 'figures': []}

        def on_click_toggle(self, new):
            self.call_count[0] += 1

    #Test the app
    my_app = MyApp(logger)
    # bokeh component triggered
    my_app.toggle_button.active = True
    assert my_app.call_count[0] == 1
    # update function triggered
    my_app.set_toggle_state(False)
    assert my_app.call_count[0] == 1
    # update function triggered
    my_app.set_toggle_state(True)
    assert my_app.call_count[0] == 1
    # bokeh component triggered
    my_app.toggle_button.active = False
    assert my_app.call_count[0] == 2

    with pytest.raises(ValueError):
        my_app.set_toggle_state("foo")
    assert "NoCallbackValueSetter Error" in log_list[-1]
        #,"NoCallbackValueSetter Error: : expected a value of type bool or bool_, got foo of type str"
