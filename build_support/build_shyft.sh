#!/bin/bash

set -e # exit on failure set -e 
set -o errexit 
set -o nounset # exit on undeclared vars set -u 
set -o pipefail # exit status of the last command that threw non-zero exit code returned

# Debugging set -x 
# set -o xtrace

exec 3>&1 4>&2
SHYFT_WORKSPACE=${SHYFT_WORKSPACE:=$(readlink --canonicalize --no-newline `dirname ${0}`/..)}
SHYFT_DEPENDENCIES_DIR=${SHYFT_DEPENDENCIES_DIR:=${SHYFT_WORKSPACE}/shyft_dependencies}
export LD_LIBRARY_PATH="${SHYFT_DEPENDENCIES_DIR}/lib:${LD_LIBRARY_PATH:+LD_LIBRARY_PATH:}"
pushd ${SHYFT_WORKSPACE}
cmake -B build -G Ninja
cmake --build build --config Release --target install
popd
