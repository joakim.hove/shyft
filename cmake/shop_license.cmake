include(FindPackageMessage)

#
# Function get_shop_license_file
#
# Find the Shop license file to be used
#
# On successful return, exports variable shop_license_file to parent scope.
#
# Value will be according to the following conditions in prioritized order:
# - If CMake variable SHOP_LICENSE is set, then write contents to a file in current
#   build directory and its path is returned.
# - If CMake variable SHOP_LICENSE_FILE is set, then its value is returned.
# - If environment variable SHOP_LICENSE is set, then write contents to a file in current
#   build directory and its path is returned.
# - If environment variable SHOP_LICENSE_FILE is set, then its value is returned.
# - If variable shop_api_LICENSE_FILE is set, set by the shop_api package if it includes
#   a license file, then its value is returned.
#
function(get_shop_license_file)
    if(DEFINED SHOP_LICENSE)
        message(VERBOSE "Shop license from CMake variable will be used in a temporary file")
        set(shop_license_file ${CMAKE_CURRENT_BINARY_DIR}/SHOP_license.dat)
        file(WRITE ${shop_license_file} "${SHOP_LICENSE}")
    elseif(DEFINED SHOP_LICENSE_FILE)
        message(VERBOSE "Shop license file from CMake variable will be used")
        set(shop_license_file ${SHOP_LICENSE_FILE})
    elseif(DEFINED ENV{SHOP_LICENSE})
        message(VERBOSE "Shop license from environment variable will be used in a temporary file")
        set(shop_license_file ${CMAKE_CURRENT_BINARY_DIR}/SHOP_license.dat)
        file(WRITE ${shop_license_file} "$ENV{SHOP_LICENSE}")
    elseif(DEFINED ENV{SHOP_LICENSE_FILE})
        message(VERBOSE "Shop license file from environment variable will be used")
        set(shop_license_file $ENV{SHOP_LICENSE_FILE})
    elseif(DEFINED shop_api_LICENSE_FILE)
        message(VERBOSE "Shop license file from shop_api package will be used")
        set(shop_license_file ${shop_api_LICENSE_FILE})
    endif()
    if(DEFINED shop_license_file)
        message(VERBOSE "Shop license file path: ${shop_license_file}")
        if (EXISTS ${shop_license_file})
            file(READ ${shop_license_file} shop_license_data LIMIT 64) # Reading just enough to expect finding "<datetime>#<licensee>#"
            string(FIND "${shop_license_data}" "#" pos)
            if (pos GREATER 0)
                string(SUBSTRING "${shop_license_data}" 0 ${pos} shop_license_date)
                math(EXPR pos "${pos} + 1")
                string(SUBSTRING "${shop_license_data}" ${pos} -1 shop_license_data)
                string(FIND "${shop_license_data}" "#" pos)
                if (pos GREATER 0)
                    string(SUBSTRING "${shop_license_data}" 0 ${pos} shop_licensee)
                    # message(STATUS "Shop license: ${shop_licensee} (issued ${shop_license_date})")
                    find_package_message(shop_license "Found Shop license: ${shop_licensee} (issued ${shop_license_date})" "[${shop_licensee}][${shop_license_date}]")
                else()
                    message(WARNING "Shop license file \"${shop_license_file}\" is not valid: ${SHOP_LICENSE_DATE}#${shop_license_data}")
                endif()
            else()
                message(WARNING "Shop license file \"${shop_license_file}\" is not valid: ${shop_license_data}")
            endif()
        else()
            message(WARNING "Shop license file \"${shop_license_file}\" does not exist")
        endif()
        set(shop_license_file ${shop_license_file} PARENT_SCOPE)
    else()
        find_package_message(shop_license "No Shop license: Only unlicensed features will be supported" "[]")
    endif()
endfunction()