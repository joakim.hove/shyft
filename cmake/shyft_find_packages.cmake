include_guard()
include(FindPackageMessage)

find_package(doctest REQUIRED)
if(doctest_FOUND)
    get_target_property(doctest_location doctest::doctest INTERFACE_INCLUDE_DIRECTORIES)
    find_package_message(doctest "Found doctest: ${doctest_location} (found version \"${doctest_VERSION}\")" "[${doctest_location}][v${doctest_VERSION}()]")
endif()

find_package(dlib REQUIRED)
if(dlib_FOUND)
    get_target_property(dlib_includes dlib::dlib INTERFACE_INCLUDE_DIRECTORIES)
    get_target_property(dlib_configs dlib::dlib IMPORTED_CONFIGURATIONS)
    foreach(dlib_config ${dlib_configs})
        get_target_property(dlib_location_${dlib_config} dlib::dlib IMPORTED_LOCATION_${dlib_config})
        list(APPEND dlib_locations ${dlib_location_${dlib_config}})
    endforeach()
    find_package_message(dlib "Found dlib: ${dlib_locations} (found version \"${dlib_VERSION}\")" "[${dlib_includes}][${dlib_locations}][v${dlib_VERSION}()]")
endif()

find_package(Armadillo REQUIRED)

if (NOT DEFINED OPENSSL_ROOT_DIR)
    if (DEFINED ENV{CONDA_PREFIX})
        # If conda, then try to use ssl from the conda env.
        set(OPENSSL_ROOT_DIR $ENV{CONDA_PREFIX})
    endif()
endif()
find_package(OpenSSL REQUIRED)

find_package(leveldb REQUIRED)
if(leveldb_FOUND)
    get_target_property(leveldb_includes leveldb::leveldb INTERFACE_INCLUDE_DIRECTORIES)
    get_target_property(leveldb_configs leveldb::leveldb IMPORTED_CONFIGURATIONS)
    foreach(leveldb_config ${leveldb_configs})
        get_target_property(leveldb_location_${leveldb_config} leveldb::leveldb IMPORTED_LOCATION_${leveldb_config})
        list(APPEND leveldb_locations ${leveldb_location_${leveldb_config}})
    endforeach()
    find_package_message(leveldb "Found LevelDB: ${leveldb_locations} (found version \"${leveldb_VERSION}\")" "[${leveldb_includes}][${leveldb_locations}][v${leveldb_VERSION}()]")
endif()

include(${CMAKE_CURRENT_LIST_DIR}/python_boost.cmake)

if (SHYFT_WITH_PYTHON)
    find_python_in_path()
    set(BOOST_PYTHON_VERSION ${python_version} CACHE INTERNAL "Version of Python that Boost.Python is built with")
endif()

set(BOOST_VERSION 1.75.0 CACHE STRING "Boost version to use")

find_boost_stuff()

if (MSVC)
    find_library(math_lib mkl_rt) # we use mkl, from conda pgk, ref. win_build_dependencies.sh
    if (math_lib)
        find_package_message(math_lib "Found Intel MKL: ${math_lib}" "[${math_lib}]")
    else()
        # for somewhat bw compat:
        find_library(math_lib libopenblas)
        if(math_lib)
            find_package_message(math_lib "Found OpenBLAS: ${math_lib}" "[${math_lib}]")
        else()
            message(WARNING "Did not find intel or openblas, incomplete dependencies for shyft on win")
        endif()
    endif()
endif()

set(arma_libs ${ARMADILLO_LIBRARIES} ${math_lib})

if(SHYFT_WITH_TESTS)
  find_package(doctest REQUIRED)
endif()

if(SHYFT_WITH_ENERGY_MARKET)
    # Optional Shop integration, with a specific version of shop_api package (must be compatible with existing generated proxy)
    set(SHOP_API_VERSION 13.5.5.6 CACHE STRING "The version of the package shop_api")
    if (NOT DEFINED SHYFT_WITH_SHOP)
        # Auto-detect: Do it if, and only if, shop_api package is found.
        find_package(shop_api ${SHOP_API_VERSION} EXACT CONFIG) # (not REQUIRED)
        set(SHYFT_WITH_SHOP ${shop_api_FOUND} CACHE BOOL "Build with Shop integration (requires package shop_api)")
        if(SHYFT_WITH_SHOP)
            find_package_message(shop_api "Found Shop API: Building with Shop integrations using shop_api version ${shop_api_SHOP_VERSION} since package was found (to disable set variable SHYFT_WITH_SHOP=FALSE)" "[${shop_api_INCLUDE_DIRS}][${shop_api_LIBRARIES}][v${shop_api_SHOP_VERSION}()]")
        else()
            find_package_message(shop_api "No Shop API: Building without Shop integrations due to missing shop_api package" "[]")
        endif()
    elseif(SHYFT_WITH_SHOP)
        # Forced: Fail if shop_api package cannot be found
        find_package(shop_api ${SHOP_API_VERSION} EXACT CONFIG REQUIRED)
        find_package_message(shop_api "Found Shop API: Building with Shop integrations using shop_api version ${shop_api_SHOP_VERSION} due to variable SHYFT_WITH_SHOP=${SHYFT_WITH_SHOP}" "[${shop_api_INCLUDE_DIRS}][${shop_api_LIBRARIES}][v${shop_api_SHOP_VERSION}()]")
    else()
        # Disabled: Ignore any shop_api package
        find_package_message(shop_api "No Shop API: Building without Shop integrations due to variable SHYFT_WITH_SHOP=${SHYFT_WITH_SHOP}" "[]")
    endif()
    if(SHYFT_WITH_SHOP)
        add_compile_definitions(SHYFT_WITH_SHOP SHOP_API_VERSION=${shop_api_SHOP_VERSION})
        # Handle optional Shop license
        include(${CMAKE_CURRENT_LIST_DIR}/shop_license.cmake)
        get_shop_license_file()
    endif()
    find_package(Qt5 COMPONENTS Widgets Charts REQUIRED HINTS /usr )
endif()
