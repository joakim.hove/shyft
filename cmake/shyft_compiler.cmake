
if(NOT MSVC)
    find_program(CCACHE_FOUND ccache)
    if(CCACHE_FOUND)
        set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
    endif(CCACHE_FOUND)
endif()

set(CMAKE_CXX_STANDARD 17)
# need some more work in code before we can set this
#set(CMAKE_CXX_VISIBILITY_PRESET hidden)

set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(THREADS_PREFER_PTHREAD_FLAG ON)




include_directories(${CMAKE_SOURCE_DIR}/cpp ${SHYFT_DEPENDENCIES}/include)

add_library(shyft_private_flags INTERFACE)
target_compile_features(shyft_private_flags INTERFACE cxx_std_${CMAKE_CXX_STANDARD})
target_compile_definitions(shyft_private_flags INTERFACE $<$<CONFIG:Debug>:SHYFT_DEBUG>)

add_library(shyft_public_flags INTERFACE)
target_compile_features(shyft_public_flags INTERFACE cxx_std_${CMAKE_CXX_STANDARD})
target_compile_definitions(shyft_public_flags INTERFACE $<$<CONFIG:Debug>:SHYFT_DEBUG>
        ARMA_USE_CXX11 ARMA_NO_DEBUG
        BOOST_ALLOW_DEPRECATED_HEADERS BOOST_PYTHON_PY_SIGNATURES_PROPER_INIT_SELF_TYPE 
        BOOST_BIND_GLOBAL_PLACEHOLDERS BOOST_BEAST_SEPARATE_COMPILATION 
        BOOST_ERROR_CODE_HEADER_ONLY BOOST_VARIANT_MINIMIZE_SIZE 
        BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS BOOST_MPL_LIMIT_LIST_SIZE=30 
        BOOST_MPL_LIMIT_VECTOR_SIZE=30)


if(MSVC)
    set(py_ext_suffix ".pyd") # Python extension use .pyd instead of .dll on Windows
    set(shyft_lib_type "STATIC")
    set(shyft_runtime "RUNTIME") #  when installing .dll/.pyd, requires special install to skip .lib on windows
    
    target_compile_options(shyft_public_flags INTERFACE -bigobj -MP4)
    target_compile_definitions(shyft_public_flags INTERFACE
        _CRT_SECURE_NO_WARNINGS _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS _WIN32_WINNT=0x0601 
        STRICT WIN32_LEAN_AND_MEAN NOMINMAX 
        ARMA_DONT_USE_WRAPPER ARMA_DONT_PRINT_CXX11_WARNING ARMA_DONT_PRINT_ERRORS
    )
    
else()
    set(py_ext_suffix ".so")  # linux/apple uses these
    set(shyft_runtime "")     # installing py runtime requires no special action on linux
    set(shyft_lib_type "SHARED")
    
    if ( NOT ${BOOST_VERSION} MATCHES "1.75.0" OR ${CMAKE_SYSTEM} MATCHES "el7.x86_64$" OR ${CMAKE_SYSTEM} MATCHES "el8.x86_64$" )        
        target_compile_options(shyft_public_flags INTERFACE -Wno-deprecated-declarations) # RHEL compilers are older and generates a lot of noise
    else()
        target_compile_options(shyft_public_flags INTERFACE -Wno-deprecated-declarations -Wall -Wextra)  # for fresh arch, this should work and keep us clean
    endif()
endif()


install(
  TARGETS shyft_public_flags shyft_private_flags
  EXPORT shyft-targets
  LIBRARY DESTINATION ${SHYFT_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${SHYFT_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${SHYFT_INSTALL_BINDIR})
