
#
# Function find_python_in_path
#
# execute python -c to find python, and extract variables using python modules
#
# on return export to parent scope:
#
# python_inlcude
# python_library
# python_library_path
# python_numpy_include
# python_version ex 3.6 , 3.7 etc..
# python_boost_module ex python36 or python37 corresponding to python version.
#
#
function(find_python_in_path)
    if (WIN32)
        # Executable name as used in conda.
        set(py_exe_name "python")
        # Get full path to executable by searching standard system environment variable
        # PATH, and only that, using find_program with all NO_... search options except
        # NO_SYSTEM_ENVIRONMENT_PATH.
        # (On Linux we can just call execute_process with executable name and it will
        # search PATH only, but on Windows it will search current application directory,
        # working directory, windows directories, and only as a last resort the PATH variable.
        # This means if there is a python.exe in the same directory as cmake.exe, which is
        # the case in a mingw environment, then execute_process with only the executable name
        # as command will always pick that one without ever considering PATH, and this could
        # be different from what will be used when running the executable outside of cmake.)
        find_program(py_exe_path ${py_exe_name} NO_PACKAGE_ROOT_PATH NO_CMAKE_PATH NO_CMAKE_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH NO_CMAKE_FIND_ROOT_PATH)
        if (NOT py_exe_path)
            message(FATAL_ERROR "Unable to find python executable in PATH")
        endif()
        # Move value from cache variable py_exe_path into normal variable py_exe so that a
        # a new search will be performed every time like in else case below (or could we
        # just as well keep it in cache?)
        set(py_exe ${py_exe_path})
        unset(py_exe_path CACHE)
    else()
        set(py_exe "python3")
    endif()

    execute_process(
        COMMAND ${py_exe} "-c" "from sysconfig import (get_paths,get_python_version);p=get_paths();import numpy as  np;cfg= ';'.join([p['include'],p['stdlib'],np.get_include(),get_python_version()]);print(cfg)"
        RESULT_VARIABLE rv
        OUTPUT_VARIABLE py_cfg
        ERROR_VARIABLE  py_cfg_error
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    if (rv)
        message(FATAL_ERROR "Errors occurred. trying to exec ${py_exe},  Leaving now!:${py_cfg_error}")
        return()
    endif()
    list(GET py_cfg 0 py_include)
    list(GET py_cfg 1 py_lib_path)
    list(GET py_cfg 2 py_numpy_include)
    list(GET py_cfg 3 py_version )

    #message(STATUS " py_cfg: " ${py_cfg})
    #message(STATUS " py_include: ${py_include}")
    #message(STATUS " py_lib_path: ${py_lib_path}")
    #message(STATUS " py_numpy_include: ${py_numpy_include}")
    #message(STATUS " py_version: ${py_version}")

    set(python_include ${py_include} PARENT_SCOPE)
    set(python_lib_path ${py_lib_path}  PARENT_SCOPE)
    set(python_numpy_include ${py_numpy_include}  PARENT_SCOPE)
    set(python_version ${py_version} PARENT_SCOPE)
    string(REPLACE "." "" py_ver "${py_version}")
    
    if (WIN32)
        set(python_lib "${py_lib_path}\\..\\libs\\python${py_ver}.lib"  PARENT_SCOPE)
    elseif (${CMAKE_SYSTEM_NAME} STREQUAL "Linux")
        if (EXISTS "${py_lib_path}/../libpython${py_version}m.so")
            set(python_lib "${py_lib_path}/../libpython${py_version}m.so"  PARENT_SCOPE)
        elseif ( EXISTS "${py_lib_path}/../libpython${py_version}.so")
            set(python_lib "${py_lib_path}/../libpython${py_version}.so"  PARENT_SCOPE)
        else()
            set(python_lib ""  PARENT_SCOPE)
        endif()
    elseif (${CMAKE_SYSTEM_NAME} STREQUAL "Darwin")
        set(python_lib "${py_lib_path}/../libpython${py_version}.dylib"  PARENT_SCOPE)

    endif()
    set(python_boost_module python${py_ver} PARENT_SCOPE)
endfunction()

#
# Function find_boost_stuff
#
# Try to find suitable boost libraries, using standard list,
#  pythonXY serialization filesystem system
#
# most important, the boost python library, must match with the
# active python version(XY), as found in find_python_in_path() above.
#
# e.g. if python is 3.6, the python36 is the name of the boost python module to load/link.
#
#  This is amazingly complex, due to bugs/shortcomings in both cmake and boost,
#  the latter migrating from bjam to cmake.
# Preconditions:
#
# BOOST_VERSION should be set to minimum required version. e.g. 1.71
# BOOST_PYTHON_VERSION should be set prior to calling this function, e.g. 3.7
# python_lib should point to the the python libaries that is needed, usually python36 (win), or libpython-....so (linux)
#
# After call, these variables are set (parent_scope):
# boost_link_libraries
# boost_py_link_libraries same as boost_link_libraries plus boost python and python lib.
#
macro(find_boost_stuff)

    if (MSVC)
        # Apply patch from CMake 3.21.3 to support VS2022 with latest toolset v143
        # (https://github.com/Kitware/CMake/blob/v3.21.3/Modules/Platform/Windows-MSVC.cmake#L70-L72).
        # Without this patch MSVC_TOOLSET_VERSION will have value 142 at this point,
        # and are simply overwritten with 143 - without considering if actually trying
        # to build using previous toolset version 142 with VS2022.
        # Note that the CMake version included in VS2022 version 17.0.0 is
        # "3.21.21080301-MSVC_2", it does not include the patch from CMake 3.21.3 but
        # the version number is not directly comparable to standard CMake version
        # numbers so currently includes the patch for any 3.21 versions of CMake with
        # the "-MSVC" suffix, in addition to any CMake versions older than 3.21.3.
        if (CMAKE_VERSION MATCHES "^3\.21\.[0-9]+-MSVC" OR CMAKE_VERSION VERSION_LESS "3.21.3")
            if (MSVC_TOOLSET_VERSION EQUAL 142 AND MSVC_VERSION GREATER_EQUAL 1930)
                message(WARNING "Patching MSVC_TOOLSET_VERSION to value 143 instead of 142 due to MSVC_VERSION ${MSVC_VERSION} and CMAKE_VERSION ${CMAKE_VERSION}")
                set(MSVC_TOOLSET_VERSION 143)
            endif()
        endif()
    endif()

    # Boost 1.70/1.71 provides package configuration files even though it was not built using the boost-cmake project, and
    # it seems to have some issues still (does not find boost_python36) so we stick with just the CMake built-in for now.
    set(Boost_NO_BOOST_CMAKE TRUE)  # Because https://gitlab.kitware.com/cmake/cmake/issues/19656

    set(BOOST_VERSION_MATCH ${BOOST_VERSION})
    string(REPLACE "." "" b_pylib "${BOOST_PYTHON_VERSION}" )
    set(BOOST_PYLIB  ${b_pylib} )
    if (SHYFT_WITH_PYTHON)
        set(boost_py_pkg python${b_pylib})
    endif()
    set(Boost_NO_WARN_NEW_VERSIONS ON)
    set(Boost_ADDITIONAL_VERSIONS  "1.74" "1.74.0" "1.75" "1.75.0" "1.76" "1.76.0" "1.77" "1.77.0" "1.78" "1.78.0") # Assuming minimum CMake version is 3.19, add support for Boost 1.74 (first known in CMake 3.19.2), 1.75 (CMake 3.19.5), 1.76 (CMake 3.20.3), 1.77 (CMake 3.22.1), 1.78 (unknown as of CMake 3.22.1)
    find_package(Boost ${BOOST_VERSION_MATCH} COMPONENTS ${boost_py_pkg} serialization filesystem system thread atomic REQUIRED)
    if (NOT Boost_FOUND)
        # FindBoost module does not generate fatal error that aborts the execution when boost is not found, so we generate one ourselves.
        message(FATAL_ERROR "Failed to find backage Boost ${BOOST_VERSION_MATCH} COMPONENTS python${BOOST_PYLIB} serialization filesystem system thread atomic")
    endif()
    if (NOT "${Boost_MAJOR_VERSION}.${Boost_MINOR_VERSION}.${Boost_SUBMINOR_VERSION}" VERSION_EQUAL "${BOOST_VERSION}")
        # FindBoost module supports specified Boost version so find_package was run without exact option, and it found a different version than expected.
        message(TRACE "Unable to find the requested Boost version ${BOOST_VERSION}, using ${Boost_MAJOR_VERSION}.${Boost_MINOR_VERSION}.${Boost_SUBMINOR_VERSION} instead.")
    endif()
    if (MSVC)
        # With MSVC link with optimized or debug variants of the libraries depending on build configuration
        set(boost_link_libraries ${Boost_SYSTEM_LIBRARY} ${Boost_FILESYSTEM_LIBRARY} ${Boost_SERIALIZATION_LIBRARY} ${Boost_THREAD_LIBRARY} ${Boost_ATOMIC_LIBRARY})
        set(boost_py_link_libraries ${boost_link_libraries} ${Boost_PYTHON${b_pylib}_LIBRARY} ${python_lib} )
    elseif (APPLE)
        # WITH CLANG, stdc++ is replaced with libc++, and filesystem is included by default.
        set(boost_link_libraries ${Boost_SYSTEM_LIBRARY_RELEASE} ${Boost_FILESYSTEM_LIBRARY_RELEASE} ${Boost_SERIALIZATION_LIBRARY_RELEASE} ${Boost_THREAD_LIBRARY_RELEASE} ${Boost_ATOMIC_LIBRARY})
        set(boost_py_link_libraries ${boost_link_libraries} ${Boost_PYTHON${b_pylib}_LIBRARY_RELEASE} ${python_lib} )
    else()
        # With GCC and other non-MSVC always link with release, and we also include stdc++fs
        set(boost_link_libraries ${Boost_SYSTEM_LIBRARY_RELEASE} ${Boost_FILESYSTEM_LIBRARY_RELEASE} ${Boost_SERIALIZATION_LIBRARY_RELEASE} ${Boost_THREAD_LIBRARY_RELEASE} ${Boost_ATOMIC_LIBRARY_RELEASE} stdc++fs)
        set(boost_py_link_libraries ${boost_link_libraries} ${Boost_PYTHON${b_pylib}_LIBRARY_RELEASE} ${python_lib} )
    endif()
    if (WIN32)
        # On Windows Boost libraries are built with "versioned" layout by default, and then the
        # include files are in version-specific subfolder so ${SHYFT_DEPENDENCIES}/include is not enough.
        # We also disable the auto-linking feature, since we are linking them explicitely.
        include_directories(${Boost_INCLUDE_DIRS})
        add_definitions(-DBOOST_ALL_NO_LIB)
    endif()
    #message(STATUS "boost libs:" ${boost_link_libraries})
    #message(STATUS "boost +py:" ${boost_py_link_libraries})

endmacro()
