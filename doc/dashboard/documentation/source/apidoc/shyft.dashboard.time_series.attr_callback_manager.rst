time\_series.attr\_callback\_manager
====================================

.. automodule:: shyft.dashboard.time_series.attr_callback_manager
   :members:
   :undoc-members:
   :show-inheritance:
