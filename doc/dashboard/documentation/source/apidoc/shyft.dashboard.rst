
API Documentation\:  Statkraft.bokeh
====================================


.. automodule:: shyft.dashboard
   :members:
   :undoc-members:
   :show-inheritance:

**Subpackages**

.. toctree::

   shyft.dashboard.apps
   shyft.dashboard.base
   shyft.dashboard.entry_points
   shyft.dashboard.examples
   shyft.dashboard.graph
   shyft.dashboard.maps
   shyft.dashboard.time_series
   shyft.dashboard.util
   shyft.dashboard.widgets
