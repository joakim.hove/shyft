from shyft.energy_market import ui
import functools
import random
import json
import copy
from PySide2.QtCore import Qt, QDate, QTime, QDateTime
from PySide2.QtGui import QFont
from PySide2.QtWidgets import QApplication, QWidget, QFrame, QLabel,\
    QPushButton, QVBoxLayout, QTableWidget,\
    QTableWidgetItem, QGridLayout, QMenu, QAction
from PySide2.QtCharts import QtCharts


def as_json_string(f):
    """
    Post-processing of Widget-generating function to return a JSON-string
    decoding its data.
    :param f: A function returning a QtWidget
    :return: A function returning the ui.export(QtWidget)
    """
    @functools.wraps(f) # To keep function name and documentation
    def wrapped_func(*args, **kwargs):
        qtobj = f(*args, **kwargs)
        return ui.export(qtobj)
    return wrapped_func


class CallbackArgs:
    def __init__(self, type):
        self.type = type


class Callback:

    def __init__(self, name: str, type: str):
        self.request_id = "fix-me"
        self.layout_id = 0
        self.name = name
        self.args = CallbackArgs(type)

    def to_json(self):
        
        if self.name and self.args.type:
            return f"read_layout {json.dumps(self.__dict__, default = lambda o: o.__dict__)}"
        else:
            return ""


class MenuItem:

    def __init__(self, id: int, text, icon: str = "", tag: str = "", callback = None, style = ""):
        self.id = id
        self.text = text
        self.icon = icon
        self.tag = tag
        self.callback = callback
        self.children = []
        self.window = None

    def build(self, window):
        
        self.window = window
        button = QPushButton(self.window)
        self._update(button, self)

        self._build_children(button, self.children)

        return button


    def _build_children(self, parent, children):
        
        if len(children) > 0:
            q_menu = QMenu(self.window)
            parent.setMenu(q_menu)

            for i in children:
                action = QAction(self.window)
                self._update(action, i)
                q_menu.addAction(action)

                if i.children:
                    self._build_children(action, i.children)
    
    def _update(self, component, props):
        component.setText(props.text)
        component.setProperty("id", props.id)
        component.setProperty("tag", props.tag)
        component.setProperty("callback", props.callback.to_json() if props.callback else '')
        component.setProperty("icon_name", props.icon)
        component.setProperty("view_identifier", 200 + props.id)   # 'front end only?'
        


class LayoutCallbacks:

    def __init__(self):
        pass

    def navigation_start_up(self, argmap):

        window = QWidget()
        layout = QVBoxLayout()
        window.setLayout(layout)

        # regions
        region = MenuItem(1, "select region", "expand_more", "region")
        norway = MenuItem(2, "Norway", "graphic_eq")
        sweden = MenuItem(3, "Sweden", "graphic_eq")
        
        korgen = MenuItem(4, "Korgen", "landscape", callback = Callback("box", "large_grid"))
        sauda = MenuItem(5, "Sauda", "landscape")
        se1 = MenuItem(6, "SE1", "adjust", callback = Callback("B", "jagged_grid"))
        region.children.extend((norway, sweden))
        norway.children.extend((korgen, sauda))
        sweden.children.append(se1)

        layout.addWidget(region.build(window))

        # users        
        user = MenuItem(7, "select user", "expand_more", "user")
        a_n = MenuItem(8, "a-n", "group")
        o_z = MenuItem(9, "o-z", "group")

        arne = MenuItem(10, "Arne", "account_circle", callback = Callback("box", "large_grid")) 
        lars = MenuItem(11, "Lars", "person", callback = Callback("box", "mega_grid"))
        ola = MenuItem(12, "Ola", "account_circle")
        randi = MenuItem(13, "Randi", "person")
        willhelm = MenuItem(14, "Willhelm", "smartphone")
        trude = MenuItem(15, "Trude", "smartphone", callback = Callback("box", "trude_grid"))

        user.children.extend((a_n, o_z))
        a_n.children.extend((arne, lars))
        o_z.children.extend((ola, randi, trude, willhelm))

        layout.addWidget(user.build(window))

        return window


    def large_grid(self, argmap):
        window = QWidget()
        mainGrid = QGridLayout()
        for i in range(3):
            for j in range(3):
                if j == 1 and i == 1:
                    subGrid = QGridLayout()
                    subGrid.addWidget(QLabel("A"))
                    subGrid.addWidget(QLabel("B"))
                    wm = QWidget()
                    wm.setLayout(subGrid)
                    mainGrid.addWidget(wm, i, j)
                else:
                    mainGrid.addWidget(QLabel(f"B({i}, {j})"), i, j)
        window.setLayout(mainGrid)
        return window
    

    def trude_grid(self, argmap):
        window = QWidget()
        mainGrid = QGridLayout()
        for i in range(2):
            for j in range(2):
                if j == 1 and i == 1:
                    subGrid = QGridLayout()
                    subGrid.addWidget(QLabel("A"))
                    subGrid.addWidget(QLabel("B"))
                    wm = QWidget()
                    wm.setLayout(subGrid)
                    mainGrid.addWidget(wm, i, j)
                else:
                    label = QLabel(f"B({i}, {j})")
                    label.setStyleSheet("material-theme { primary: pink, 500, 300, 800; accent: blue, 400, 200, 800; error: red }")
                    mainGrid.addWidget(label, i, j)
        window.setLayout(mainGrid)
        return window


    def mega_grid(self, argmap):
        window = QWidget()
        mainGrid = QGridLayout()
        for i in range(21):
            for j in range(21):
                if j == 1 and i == 1:
                    subGrid = QGridLayout()
                    subGrid.addWidget(QLabel("A"))
                    subGrid.addWidget(QLabel("B"))
                    wm = QWidget()
                    wm.setLayout(subGrid)
                    mainGrid.addWidget(wm, i, j)
                else:
                    mainGrid.addWidget(QLabel(f"B({i}, {j})"), i, j)
        window.setLayout(mainGrid)
        return window


    def jagged_grid(self, argmap):
        window = QWidget()
        mainGrid = QGridLayout()
        
        a = QLabel("AAA")
        b = QLabel("BBB")
        c = QLabel("CCC")
        d = QLabel("DDD")
        e = QLabel("EEE")
        f = QLabel("FFF")
        
        # , row-index, colum-index, row-span, col-span
        mainGrid.addWidget(a, 0, 0, 1, 1)
        mainGrid.addWidget(b, 0, 1, 1, 1)
        mainGrid.addWidget(c, 1, 0, 1, 2)
        mainGrid.addWidget(d, 2, 1, 1, 1)
        mainGrid.addWidget(e, 3, 0, 1, 2)
        mainGrid.addWidget(f, 4, 0, 1, 1)

        window.setLayout(mainGrid)

        return window

    
    @as_json_string
    def __call__(self, name: str, args: str):
        print(f"Trying to generate a new layout with name: {name}")
        try:
            argmap = json.loads(args)
        except json.JSONDecodeError:
            raise RuntimeError("Unable to parse args as a JSON-struct.")
        tpe = argmap.pop("type") # Should raise a KeyError if not present
        return getattr(self, tpe)(argmap)
        raise RuntimeError(f"Unknown widget type: '{tpe}'")


# if you get stuck, use this for dumping json to screen. Replace in region below with code that you want to use in your test case
if __name__ == "__main__":
    
    app = QApplication([])
    window = QWidget()
    layout = QVBoxLayout()

    window.setLayout(layout)

    # --- replace code from here: -----

    region = MenuItem(1, "select region", "expand_more", "region")
    norway = MenuItem(2, "Norway", "graphic_eq")
    sweden = MenuItem(3, "Sweden", "graphic_eq")

    korgen = MenuItem(4, "Korgen", "landscape", callback = Callback("A", "large_grid"))
    sauda = MenuItem(5, "Sauda", "landscape")
    se1 = MenuItem(6, "SE1", "adjust", callback = Callback("B", "jagged_grid")) #  'read_layout { "request_id": "fix-me", "layout_id": 0, "name": "box", "args": { "type": "jagged_grid" } }')
    region.children.extend((norway, sweden))
    norway.children.extend((korgen, sauda))
    sweden.children.append(se1)

    layout.addWidget(region.build(window))

    # --- to here: -----

    ui.export_print(window, True)
