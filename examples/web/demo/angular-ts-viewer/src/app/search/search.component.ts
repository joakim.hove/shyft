import { Component, OnInit } from '@angular/core';

import {TsInfo} from '../ts-info';
import {TimeSeriesService} from '../time-series.service';

import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  tsList: TsInfo[] = [];
  tsInfos: Observable<TsInfo[]>;
  selectedTsInfo:TsInfo = null; // For some reason that I don't understand, this has to be initialized with something1,
                               // And default constructors aren't a thing in javascript, it seems??? I don't know.
  private searchTerms = new Subject<string>();

  constructor(
    private timeSeriesService: TimeSeriesService
  ) { }

  ngOnInit(): void {
    this.searchTerms.pipe(
      // wait 300ms
      debounceTime(300),
      // ignore new term if same
      distinctUntilChanged(),
      // switch to new search observable each time the term changes
      switchMap((term: string) => this.timeSeriesService.findTimeSeries(term)),
    ).subscribe(tsList => {
      this.tsList = tsList.sort(
        (a, b) => {
          return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0);
        }
      );
    });
  }

  find(searchTerm: string): void {
    this.searchTerms.next(searchTerm);
  }

  onSelect(tsInfo: TsInfo): void {
    this.selectedTsInfo = tsInfo;
    this.timeSeriesService.selectedTsInfo.next(tsInfo); // will trigger tsgraph component fetch..
    // TODO: route directly to graph component instead.
}
}
